<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function checkAdministratorPermission ($request_permission, $require_permission_key){
    $codeigniter_instance =& get_instance();
    $administrator_permissions = $codeigniter_instance->session->userdata("__administrator_permissions");
    $administrator_group_permissions = $codeigniter_instance->session->userdata("__administrator_group_permissions");

    if(isset($administrator_permissions[$request_permission]) && in_array($require_permission_key, $administrator_permissions[$request_permission])){
        return true;
    }else if(isset($administrator_group_permissions[$request_permission]) && in_array($require_permission_key, $administrator_group_permissions[$request_permission])){
        return true;
    }else{
        return false;
    }
}
