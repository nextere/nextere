<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');


// MAIN ASSETS DIRECTORY
function convertToDateThai($date) {
    if(isset($date) && (DateTime::createFromFormat('Y-m-d H:i:s', $date) !== FALSE)){
        $ret = thaiDateFullMonth(strtotime($date));
        return $ret;
    }else{
        return "";
    }
}

function convertToYearThai($date) {
    if(isset($date)){
        $ret = date("d/m/", strtotime($date)).(date("Y", strtotime($date))+543);
        return $ret;
    }else{
        return "";
    }
}


function thaiDateTime($time){   // 19 ธันวาคม 2556 เวลา 10:10:43
    $codeigniter_instance =& get_instance();
    $dayTH = $codeigniter_instance->config->item('CONSTANT_DAYTH'); 
    $monthTH = $codeigniter_instance->config->item('CONSTANT_MONTHTH'); 
    $monthTH_brev = $codeigniter_instance->config->item('CONSTANT_MONTHTH_BREV'); 
    $thaiDateReturn = date("j",$time);   
    $thaiDateReturn.=" ".$monthTH[date("n",$time)];   
    $thaiDateReturn.= " ".(date("Y",$time)+543);   
    $thaiDateReturn.= " เวลา ".date("H:i:s",$time);
    return $thaiDateReturn;   
} 
function thaiDateTimeShort($time){   // 19  ธ.ค. 2556 10:10:4
    $codeigniter_instance =& get_instance();
    $dayTH = $codeigniter_instance->config->item('CONSTANT_DAYTH'); 
    $monthTH = $codeigniter_instance->config->item('CONSTANT_MONTHTH'); 
    $monthTH_brev = $codeigniter_instance->config->item('CONSTANT_MONTHTH_BREV'); 
    $thaiDateReturn = date("j",$time);   
    $thaiDateReturn.=" ".$monthTH_brev[date("n",$time)];   
    $thaiDateReturn.= " ".(date("Y",$time)+543);   
    $thaiDateReturn.= " ".date("H:i:s",$time);
    return $thaiDateReturn;   
} 
function thaiDateShort($time){   // 19  ธ.ค. 2556a
    $codeigniter_instance =& get_instance();
    $dayTH = $codeigniter_instance->config->item('CONSTANT_DAYTH'); 
    $monthTH = $codeigniter_instance->config->item('CONSTANT_MONTHTH'); 
    $monthTH_brev = $codeigniter_instance->config->item('CONSTANT_MONTHTH_BREV');   
    $thaiDateReturn = date("j",$time);   
    $thaiDateReturn.=" ".$monthTH_brev[date("n",$time)];   
    $thaiDateReturn.= " ".(date("Y",$time)+543);   
    return $thaiDateReturn;   
} 
function thaiDateFullMonth($time){   // 19 ธันวาคม 2556
    $codeigniter_instance =& get_instance();
    $dayTH = $codeigniter_instance->config->item('CONSTANT_DAYTH'); 
    $monthTH = $codeigniter_instance->config->item('CONSTANT_MONTHTH'); 
    $monthTH_brev = $codeigniter_instance->config->item('CONSTANT_MONTHTH_BREV'); 
    $thaiDateReturn = date("j",$time);   
    $thaiDateReturn.=" ".$monthTH[date("n",$time)];   
    $thaiDateReturn.= " ".(date("Y",$time)+543);   
    return $thaiDateReturn;   
} 
function thaiDateShortNumber($time){   // 19-12-56
    $codeigniter_instance =& get_instance();
    $dayTH = $codeigniter_instance->config->item('CONSTANT_DAYTH'); 
    $monthTH = $codeigniter_instance->config->item('CONSTANT_MONTHTH'); 
    $monthTH_brev = $codeigniter_instance->config->item('CONSTANT_MONTHTH_BREV'); 
    $thaiDateReturn = date("d",$time);   
    $thaiDateReturn.="-".date("m",$time);   
    $thaiDateReturn.= "-".substr((date("Y",$time)+543),-2);   
    return $thaiDateReturn;   
} 

function genNextRoundDate($purchase_date, $round)
{
    $day = date("d", strtotime($purchase_date));
    $month = date("m", strtotime($purchase_date));
    $year = date("Y", strtotime($purchase_date));
    $end_day_round = date("t", strtotime($year."-".$month."-1 +".($round-1)." Month"));
    if ($day <= $end_day_round) {
        return date("Y-m-d", strtotime($purchase_date." +".($round-1)." Month"));
    }
    else{
        return date("Y-m-t", strtotime($year."-".$month."-1 +".($round-1)." Month"));
    }
}

function monththaitoeng($date_string)
{
    $str_months = array(
        "มกราคม" => "january",
        "กุมภาพันธ์"=>'february', 
        "มีนาคม"=>'march',
        "เมษายน"=>'april',
        "พฤษภาคม"=>'may',
        "มิถุนายน"=>'june',
        "กรกฎาคม"=>'july',
        "สิงหาคม"=>'august',
        "กันยายน"=>'september',
        "ตุลาคม"=>'october', 
        "พฤศจิกายน"=>'november',
        "ธันวาคม"=>'december');

    return strtr(strtolower($date_string), $str_months);
}

function monthengtothai($date_string)
{
    $str_months = array(
        "january" =>"มกราคม",
        'february'=>"กุมภาพันธ์", 
        'march'=>"มีนาคม",
        'april'=>"เมษายน",
        'may'=>"พฤษภาคม",
        'june'=>"มิถุนายน",
        'july'=>"กรกฎาคม",
        'august'=>"สิงหาคม",
        'september'=>"กันยายน",
        'october'=>"ตุลาคม", 
        'november'=>"พฤศจิกายน",
        'december'=>"ธันวาคม");

    return strtr(strtolower($date_string), $str_months);
}

?>
