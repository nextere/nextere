<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function upload_path_url(){
	return base_url().'assets/upload/';
}

function upload_path(){
	return 'assets/upload/';
}

function front_path()
{
	return $_SERVER['DOCUMENT_ROOT'].'/rfDashboard/application/views/';
}

function asset_css()
{
    return base_url().'assets/css/';
}

function asset_js()
{
    return base_url().'assets/js/';
}

function asset_image()
{
    return base_url().'assets/img/';
}

function house_url()
{
	return "https://www.housesamyan.com/";
}


// MAIN ASSETS DIRECTORY
function assetsDirectory ($destination){
    return base_url().'assets/'.$destination;
}
function uploadsDirectory ($destination){
    return base_url().'assets/uploads/'.$destination;
}

function faviconDirectory ($destination){
    return base_url().'assets/website/Favicon/'.$destination;
}
function flagDirectory ($destination){
    return base_url().'assets/website/Flag/'.$destination;
}
function fontDirectory ($destination){
    return base_url().'assets/website/Font/'.$destination;
}
function logoDirectory ($destination){
    return base_url().'assets/img/'.$destination;
}

