<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function writelog_login($message){
    $savepath = FCPATH."/assets/uploads/logs/login/";
    if (!file_exists($savepath)) {
        mkdir($savepath, 0755, true);
    }
    $file = fopen($savepath.date("Ymd").".txt","a");
    fwrite($file,$message." \r\n");
    fclose($file);
}

function writelog_logout($message){
    $savepath = FCPATH."/assets/uploads/logs/logout/";
    if (!file_exists($savepath)) {
        mkdir($savepath, 0755, true);
    }
    $file = fopen($savepath.date("Ymd").".txt","a");
    fwrite($file,$message." \r\n");
    fclose($file);
}

function writelog_user($message){
    $savepath = FCPATH."/assets/uploads/logs/user/";
    if (!file_exists($savepath)) {
        mkdir($savepath, 0755, true);
    }
    $file = fopen($savepath.date("Ymd").".txt","a");
    fwrite($file,$message." \r\n");
    fclose($file);
}

function writelog_paymentCredit($message){
    $savepath = FCPATH."/assets/uploads/logs/paymentcredit/";
    if (!file_exists($savepath)) {
        mkdir($savepath, 0755, true);
    }
    $file = fopen($savepath.date("Ymd").".txt","a");
    fwrite($file,$message." \r\n");
    fclose($file);
}

function writelog_paymentInternetbanking($message){
    $savepath = FCPATH."/assets/uploads/logs/internetbanking/";
    if (!file_exists($savepath)) {
        mkdir($savepath, 0755, true);
    }
    $file = fopen($savepath.date("Ymd").".txt","a");
    fwrite($file,$message." \r\n");
    fclose($file);
}

function writelog_paymentQr($message){
    $savepath = FCPATH."/assets/uploads/logs/paymentqr/";
    if (!file_exists($savepath)) {
        mkdir($savepath, 0755, true);
    }
    $file = fopen($savepath.date("Ymd").".txt","a");
    fwrite($file,$message." \r\n");
    fclose($file);
}

function writelog_shipping($message){
    $savepath = FCPATH."/assets/uploads/logs/shipping/";
    if (!file_exists($savepath)) {
        mkdir($savepath, 0755, true);
    }
    $file = fopen($savepath.date("Ymd").".txt","a");
    fwrite($file,$message." \r\n");
    fclose($file);
}