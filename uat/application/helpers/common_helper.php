<?php 
function commonHelper_slugEncode($text) 
{
	$text = rawurlencode($text);
	
	return $text;
}

function commonHelper_slugDecode($text)
{
	$text = rawurldecode($text);
	 
	return $text;
}

function commonHelper_longDateFormat($date)
{
	$monthNameArray = array(
			'01'=>'January',
			'02'=>'February',
			'03'=>'March',
			'04'=>'April',
			'05'=>'May',
			'06'=>'June',
			'07'=>'July',
			'08'=>'August',
			'09'=>'September',
			'10'=>'October',
			'11'=>'November',
			'12'=>'December',
	);

	$strTime = strtotime($date);

	return date('j',$strTime).' '.$monthNameArray[date('m',$strTime)].' '. (date('Y',$strTime));
}

function commonHelper_shortDateFormat($date)
{
	$monthNameArray = array(
			'01'=>'Jan',
			'02'=>'Feb',
			'03'=>'Mar',
			'04'=>'Apr',
			'05'=>'May',
			'06'=>'Jun',
			'07'=>'Jul',
			'08'=>'Aug',
			'09'=>'Sep',
			'10'=>'Oct',
			'11'=>'Nov',
			'12'=>'Dec',
	);

	$strTime = strtotime($date);

	return date('j',$strTime).' '.$monthNameArray[date('m',$strTime)].' '. date('Y',$strTime);
}

function commonHelper_pathImage($product)
{
	$text = "";
	if($product->movieFlag==1) $text .= "uploads/movie/";
	if($product->seriesFlag==1) $text .= "uploads/series/";
	if($product->concertFlag==1) $text .= "uploads/concert/";
	if($product->liveFlag==1) $text .= "uploads/live/";
	if($product->livetvFlag==1) $text .= "uploads/livetv/";

	return $text;
}

function commonHelper_pathUriPopupDetail($product)
{
	$text = "";
	if($product->movieFlag==1) $text .= "movie/popup-detail/";
	if($product->seriesFlag==1) $text .= "series/popup-detail/";
	if($product->concertFlag==1) $text .= "concert/popup-detail/";
	if($product->liveFlag==1) $text .= "live/popup-detail/";
	if($product->livetvFlag==1) $text .= "live-tv/popup-detail/";

	return $text;
}


function endsWith($haystack, $needle) {
	return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
function get_client_mac(){
	ob_start(); // Turn on output buffering
	system('ipconfig /all'); //Execute external program to display output
	$mycom=ob_get_contents(); // Capture the output into a variable
	ob_clean(); // Clean (erase) the output buffer
	//echo $mycom;
	$findme = "Physical";
	$pmac = strpos($mycom, $findme); // Find the position of Physical text
	$mac=substr($mycom,($pmac+36),17); // Get Physical Address
	return trim($mac);
}
function DateEng($strDate,$onlyDate=false)
{
	$strYear = date("Y",strtotime($strDate));
	$strMonth= date("M",strtotime($strDate));
	$strMonthNum= date("n",strtotime($strDate));
	$strDay= date("d",strtotime($strDate));
	$strHour= date("H",strtotime($strDate));
	$strMinute= date("i",strtotime($strDate));
	$strSeconds= date("s",strtotime($strDate));
	$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
	$strMonthThai=$strMonthCut[$strMonthNum];
	if($onlyDate){
		return "$strDay $strMonth $strYear";
	}
	else{
		return "$strDay $strMonth $strYear | $strHour:$strMinute:$strSeconds";
	}
}
function DateThai($strDate,$onlyDate=false)
{
	$strYear = date("Y",strtotime($strDate))+543;
	$strMonth= date("n",strtotime($strDate));
	$strDay= date("d",strtotime($strDate));
	$strHour= date("H",strtotime($strDate));
	$strMinute= date("i",strtotime($strDate));
	$strSeconds= date("s",strtotime($strDate));
	$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
	$strMonthThai=$strMonthCut[$strMonth];
	if($onlyDate){
		return "$strDay $strMonthThai $strYear";
	}
	else{
		return "$strDay $strMonthThai $strYear | $strHour:$strMinute:$strSeconds";
	}
}