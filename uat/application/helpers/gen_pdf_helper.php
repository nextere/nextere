<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function geninvoicepdf($data, $remaining_amount, $next_round_date, $fine = null){
    $codeigniter_instance =& get_instance();

    $codeigniter_instance->load->library("fpdf_library");
    // GENERATE PDF
    $pdf = $codeigniter_instance->fpdf_library->get();
    //---------------------START COVER ---------------------------------------------------
    $pdf->AddPage("L", "A4");
    $pdf->AddFont('angsana','','angsa.php');
    $pdf->AddFont('angsana','B','angsab.php');
    $pdf->SetMargins(10, 20 ,10);
    $pdf->SetAutoPageBreak(false);
    $pdf->SetFont('angsana','B',18);
    $pdf->Image(base_url().'assets/images/logo.jpg',10,13,28);
    $pdf->Cell(30, 10, thai(""),0 , 0, "L");
    $pdf->Cell(110, 10, thai("บริษัท เออีซี โมบาย จำกัด"), 0, 0, "L");
    $pdf->Cell(100, 10, thai("ใบแจ้งหนี้"), 0 , 0, "L");
    $pdf->Ln();
    $pdf->SetFont('angsana','b',14);
    $pdf->Cell(30, 6, thai(""),0 , 0, "L");
    $pdf->Write(6, thai('ที่อยู่: '));
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai('1701/3 ถนนพหลโยธิน แขวงจตุจักร เขตจตุจักร กรุงเทพมหานคร 10900'));
    $pdf->Ln();
    $pdf->SetFont('angsana','b',14);
    $pdf->Cell(30, 6, thai(""),0 , 0, "L");
    $pdf->Write(6, thai('เบอร์โทร: '));
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai('02-1146165'));
    $pdf->Ln();
    $pdf->SetFont('angsana','b',14);
    $pdf->Cell(30, 6, thai(""),0 , 0, "L");
    $pdf->Write(6, thai('เลขประจำตัวผู้เสียภาษี: '));
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai('0105557108415'));
    $pdf->SetX(150);
    $pdf->SetFont('angsana','b',14);
    $pdf->Write(6, thai('วันที่ครบกำหนดชำระ'));
    $pdf->SetX(205);
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai(convertToDateThai($data->{"customer_car_insurance_payment::payment_due_date"})));
    $pdf->Ln();
    $pdf->SetFont('angsana','b',14);
    $pdf->Cell(30, 6, thai(""),0 , 0, "L");
    $pdf->Write(6, thai('สาขา: '));
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai('สำนักงานใหญ่'));
    $pdf->SetX(150);
    $pdf->SetFont('angsana','b',14);
    $pdf->Write(6, thai('เลขที่'));
    $pdf->SetX(170);
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai($data->{"customer_car_insurance_payment::payment_no"}));
    $pdf->Ln(8);
    $pdf->SetX(150);
    $pdf->SetFont('angsana','b',14);
    $pdf->Write(6, thai('รหัสลูกค้า'));
    $pdf->SetX(170);
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai(str_pad($data->{"customer::id"}, 10,"0",STR_PAD_LEFT)));
    $pdf->SetX(205);
    $pdf->SetFont('angsana','b',14);
    $pdf->Write(6, thai('เลขที่สัญญา'));
    $pdf->SetX(230);
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai(str_pad($data->{"customer_car_insurance::id"}, 15,"0",STR_PAD_LEFT)));
    $pdf->Ln();
    $pdf->SetX(150);
    $pdf->SetFont('angsana','b',14);
    $pdf->Write(6, thai('ชื่อ'));
    $pdf->SetX(170);
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai($data->{"customer::title"}.$data->{"customer::first_name"}.' '.$data->{"customer::last_name"}));
    $pdf->Ln();
    $pdf->SetX(150);
    $pdf->SetFont('angsana','b',14);
    $pdf->Write(6, thai('ที่อยู่'));
    $pdf->SetX(170);
    $pdf->SetFont('angsana','',14);
    $pdf->MultiCell(100,6,thai($data->{"customer::address"}.' '.$data->{"address_sub_district::name"}.' '.$data->{"address_district::name"}.' '.$data->{"address_province::name"}.' '.$data->{"customer::postcode"}));
    // $pdf->Write(6, thai($data->{"customer::address"}.' '.$data->{"address_sub_district::name"}.' '.$data->{"address_district::name"}.' '.$data->{"address_province::name"}.' '.$data->{"customer::postcode"}));

    $pdf->Ln();
    $pdf->SetFont('angsana','b',14);
    $pdf->Cell(30, 10, thai("วันที่"),1 , 0, "C");
    $pdf->Cell(200, 10, thai("รายละเอียดการชำระเงิน"),1 , 0, "C");
    $pdf->Cell(45, 10, thai("จำนวนเงิน (บาท)"),1 , 0, "C");

    $pdf->Ln();
    $pdf->SetFont('angsana','',14);
    $pdf->Cell(30, 7, thai(convertToYearThai($data->{"customer_car_insurance_payment::payment_due_date"})), "LR", 0, "C");
    $pdf->Cell(200, 7, thai("ค่าเบี้ยประกันผ่อนชำระงวดที่ ".$data->{"customer_car_insurance_payment::period_no"}.'/'.$data->{"product::total_round"}." (ทะเบียนรถ ".$data->{"customer_car::reg_number"}.")"), "LR", 0, "L");
    $pdf->Cell(45, 7, thai(number_format($data->{"customer_car_insurance_payment::amount"} ,2)), "LR", 0, "R");
    $pdf->Ln();
    $pdf->Cell(30, 5, thai(""), "LR", 0, "C");
    $pdf->Cell(200, 5, thai(""), "LR", 0, "L");
    $pdf->Cell(45, 5, thai(""), "LR", 0, "R");
    $pdf->Ln();
    $pdf->Cell(30, 7, thai(""), "LR", 0, "C");
    $pdf->Cell(30, 7, thai(""), "L", 0, "L");
    $pdf->Cell(100, 7, thai("จำนวนเงินคงเหลือผ่อนชำระ"), "", 0, "L");
    $pdf->Cell(40, 7, thai(number_format($remaining_amount ,2)), "", 0, "R");
    $pdf->Cell(30, 7, thai(""), "R", 0, "L");
    $pdf->Cell(45, 7, thai(""), "LR", 0, "R");
    if ($next_round_date != null) {
        $pdf->Ln();
        $pdf->Cell(30, 7, thai(""), "LR", 0, "C");
        $pdf->Cell(30, 7, thai(""), "L", 0, "L");
        $pdf->Cell(100, 7, thai("กำหนดชำระงวดต่อไป (".($data->{"customer_car_insurance_payment::period_no"}+1).'/'.$data->{"product::total_round"}.")  วันที่ ".convertToYearThai($next_round_date)."  จำนวนเงิน"), "", 0, "L");
        $pdf->Cell(40, 7, thai(number_format($data->{"customer_car_insurance_payment::amount"} ,2)), "", 0, "R");
        $pdf->Cell(30, 7, thai(""), "R", 0, "L");
        $pdf->Cell(45, 7, thai(""), "LR", 0, "R");
        $pdf->Ln();
        $pdf->Cell(30, 5, thai(""), "LR", 0, "C");
        $pdf->Cell(30, 5, thai(""), "L", 0, "L");
        $pdf->Cell(50, 5, thai(""), "", 0, "L");
        $pdf->Cell(30, 5, thai(""), "", 0, "R");
        $pdf->Cell(90, 5, thai(""), "R", 0, "L");
        $pdf->Cell(45, 5, thai(""), "LR", 0, "R");
    }
    else{
        $pdf->Ln();
        $pdf->Cell(30, 12, thai(""), "LR", 0, "C");
        $pdf->Cell(200, 12, thai(""), "LR", 0, "L");
        $pdf->Cell(45, 12, thai(""), "LR", 0, "R");
    }
    
    $pdf->Ln();
    $pdf->Cell(30, 5, thai(""), "LR", 0, "C");
    $pdf->Cell(200, 5, thai(""), "LR", 0, "L");
    $pdf->Cell(45, 5, thai(""), "LR", 0, "R");
    $pdf->Ln();
    $pdf->SetFont('angsana','B',14);
    $pdf->Cell(30, 7, thai(""), "LR", 0, "C");
    $pdf->Cell(120, 7, thai("รวมยอดสุทธิ"), "L", 0, "L");
    $pdf->Cell(80, 7, thai("(". convertToCurrencyThai($data->{"customer_car_insurance_payment::amount"}).")"), "R", 0, "L");
    $pdf->Cell(45, 7, thai(number_format($data->{"customer_car_insurance_payment::amount"} ,2)), "LR", 0, "R");
    $pdf->Ln();
    $pdf->Cell(30, 3, thai(""), "LRB", 0, "C");
    $pdf->Cell(200, 3, thai(""), "LRB", 0, "L");
    $pdf->Cell(45, 3, thai(""), "LRB", 0, "R");
    $pdf->Ln();
    $pdf->SetFont('angsana','B',14);
    $pdf->Cell(30, 8, thai("หมายเหตุ :"), "", 0, "L");
    $pdf->SetFont('angsana','',14);
    $pdf->Cell(200, 8, thai("โปรดตรวจสอบความถูกต้อง หากมีข้อสงสัยกรุณาติดต่อบริษัท ตามที่อยู่ข้างต้น ภายใน 7 วัน"), "", 0, "L");
    $pdf->Ln();
    $pdf->SetFont('angsana','B',14);
    $pdf->Cell(30, 8, thai("หมายเหตุ :"), "", 0, "L");
    $pdf->SetFont('angsana','',14);
    $pdf->Cell(200, 8, thai("กรุณาชำระค่างวดผ่อนชำระ ภายในระยะเวลาที่กำหนด ".(empty($fine) ? "" : "หากชำระล่าช้าจะมีค่าปรับ ".$fine." บาท/งวด")." ทางบริษัทขอสงวนสิทธิ์ในการยกเลิกกรมธรรม์"), "", 0, "L");
    $pdf->Ln(10);
    $pdf->SetFont('angsana','B',14);
    $pdf->Cell(0, 8, thai("ชำระผ่าน QR code/Bar code (ได้ทุกแบงค์):"), "TLR", 0, "L");
    $pdf->Ln();
    $current_X = $pdf->GetX();
    $current_Y = $pdf->GetY();
    $pdf->SetFont('angsana','',14);
    $pdf->Cell(0, 30, thai(""), "LR", 0, "L");
    $pdf->Image($data->{"customer_car_insurance_payment::barcode_path"},$current_X+10, $current_Y+10,100);
    $pdf->Image($data->{"customer_car_insurance_payment::qrcode_path"},$current_X+140, $current_Y,30);
    $pdf->Image(base_url().'assets/images/logo.jpg',$current_X+210, $current_Y,60);
    $pdf->Ln();
    $pdf->Cell(210, 8, thai(""), "LB", 0, "L");
    $pdf->Cell(67, 8, thai("Call Center: 02-1146165"), "RB", 0, "C");
    $pdf->Ln();
    $pdf->Cell(0, 8, thai("สอบถามรายละเอียดเพิ่มเติมติดต่อ AEC MOBILE CALL CENTER 02-1146165 เฉพาะวันทำการ (จันทร์-ศุกร์) เวลา 9.00 น.-18.00 น. https://www.banchainsurecash.com/"), "", 0, "L");

    $savepath = FCPATH."assets/uploads/invoice/";
    if (!file_exists($savepath)) {
        mkdir($savepath, 0777, true);
    }
    $file_name = uniqid();
    $pdffile = $savepath.$file_name.'.pdf';

    $pdf->Output("F",$pdffile, true);

    return base_url()."assets/uploads/invoice/".$file_name.'.pdf';
}


function genreceiptpdf($data, $remaining_amount, $next_round_date, $fine = null, $variable_config = null){
    $codeigniter_instance =& get_instance();

    $codeigniter_instance->load->library("fpdf_library");
    // GENERATE PDF
    $pdf = $codeigniter_instance->fpdf_library->get();
    //---------------------START COVER ---------------------------------------------------
    $pdf->AddPage("L", "A4");
    $pdf->AddFont('angsana','','angsa.php');
    $pdf->AddFont('angsana','B','angsab.php');
    $pdf->SetMargins(10, 20 ,10);
    $pdf->SetAutoPageBreak(false);
    $pdf->SetFont('angsana','B',18);
    $pdf->Image(base_url().'assets/images/logo.jpg',10,13,28);
    $pdf->Cell(30, 10, thai(""),0 , 0, "L");
    $pdf->Cell(110, 10, thai("บริษัท เออีซี โมบาย จำกัด"), 0, 0, "L");
    $pdf->Cell(100, 10, thai("ต้นฉบับใบรับเงิน"), 0 , 0, "L");
    $pdf->Ln();
    $pdf->SetFont('angsana','b',14);
    $pdf->Cell(30, 6, thai(""),0 , 0, "L");
    $pdf->Write(6, thai('ที่อยู่: '));
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai('1701/3 ถนนพหลโยธิน แขวงจตุจักร เขตจตุจักร กรุงเทพมหานคร 10900'));
    $pdf->Ln();
    $pdf->SetFont('angsana','b',14);
    $pdf->Cell(30, 6, thai(""),0 , 0, "L");
    $pdf->Write(6, thai('เบอร์โทร: '));
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai('02-1146165'));
    $pdf->Ln();
    $pdf->SetFont('angsana','b',14);
    $pdf->Cell(30, 6, thai(""),0 , 0, "L");
    $pdf->Write(6, thai('เลขประจำตัวผู้เสียภาษี: '));
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai('0105557108415'));
    $pdf->SetX(150);
    $pdf->SetFont('angsana','b',14);
    $pdf->Write(6, thai('วันที่'));
    $pdf->SetX(170);
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai(convertToDateThai($data->{"customer_car_insurance_payment::payment_received_date"})));
    $pdf->Ln();
    $pdf->SetFont('angsana','b',14);
    $pdf->Cell(30, 6, thai(""),0 , 0, "L");
    $pdf->Write(6, thai('สาขา: '));
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai('สำนักงานใหญ่'));
    $pdf->SetX(150);
    $pdf->SetFont('angsana','b',14);
    $pdf->Write(6, thai('เลขที่'));
    $pdf->SetX(170);
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai($data->{"customer_car_insurance_payment::payment_no"}));
    $pdf->Ln(8);
    $pdf->SetX(150);
    $pdf->SetFont('angsana','b',14);
    $pdf->Write(6, thai('รหัสลูกค้า'));
    $pdf->SetX(170);
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai(str_pad($data->{"customer::id"}, 10,"0",STR_PAD_LEFT)));
    $pdf->SetX(205);
    $pdf->SetFont('angsana','b',14);
    $pdf->Write(6, thai('เลขที่สัญญา'));
    $pdf->SetX(230);
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai(str_pad($data->{"customer_car_insurance::id"}, 15,"0",STR_PAD_LEFT)));
    $pdf->Ln();
    $pdf->SetX(150);
    $pdf->SetFont('angsana','b',14);
    $pdf->Write(6, thai('ชื่อ'));
    $pdf->SetX(170);
    $pdf->SetFont('angsana','',14);
    $pdf->Write(6, thai($data->{"customer::title"}.$data->{"customer::first_name"}.' '.$data->{"customer::last_name"}));
    $pdf->Ln();
    $pdf->SetX(150);
    $pdf->SetFont('angsana','b',14);
    $pdf->Write(6, thai('ที่อยู่'));
    $pdf->SetX(170);
    $pdf->SetFont('angsana','',14);
    $pdf->MultiCell(100,6,thai($data->{"customer::address"}.' '.$data->{"address_sub_district::name"}.' '.$data->{"address_district::name"}.' '.$data->{"address_province::name"}.' '.$data->{"customer::postcode"}));
    // $pdf->Write(6, thai($data->{"customer::address"}.' '.$data->{"address_sub_district::name"}.' '.$data->{"address_district::name"}.' '.$data->{"address_province::name"}.' '.$data->{"customer::postcode"}));

    $pdf->Ln();
    $pdf->SetFont('angsana','b',14);
    $pdf->Cell(30, 10, thai("วันที่"),1 , 0, "C");
    $pdf->Cell(200, 10, thai("รายละเอียดการชำระเงิน"),1 , 0, "C");
    $pdf->Cell(45, 10, thai("จำนวนเงิน (บาท)"),1 , 0, "C");

    $pdf->Ln();
    $pdf->SetFont('angsana','',14);
    $pdf->Cell(30, 7, thai(convertToYearThai($data->{"customer_car_insurance_payment::payment_received_date"})), "LR", 0, "C");
    $pdf->Cell(200, 7, thai("ค่าเบี้ยประกันผ่อนชำระงวดที่ ".$data->{"customer_car_insurance_payment::period_no"}.'/'.$data->{"product::total_round"}." (ทะเบียนรถ ".$data->{"customer_car::reg_number"}.")"), "LR", 0, "L");
    $pdf->Cell(45, 7, thai(number_format($data->{"customer_car_insurance_payment::amount"} ,2)), "LR", 0, "R");
    $pdf->Ln();
    $pdf->Cell(30, 5, thai(""), "LR", 0, "C");
    $pdf->Cell(200, 5, thai(""), "LR", 0, "L");
    $pdf->Cell(45, 5, thai(""), "LR", 0, "R");
    $pdf->Ln();
    $pdf->Cell(30, 7, thai(""), "LR", 0, "C");
    $pdf->Cell(30, 7, thai(""), "L", 0, "L");
    $pdf->Cell(100, 7, thai("จำนวนเงินคงเหลือผ่อนชำระ"), "", 0, "L");
    $pdf->Cell(40, 7, thai(number_format($remaining_amount ,2)), "", 0, "R");
    $pdf->Cell(30, 7, thai(""), "R", 0, "L");
    $pdf->Cell(45, 7, thai(""), "LR", 0, "R");
    if ($next_round_date != null) {
        $pdf->Ln();
        $pdf->Cell(30, 7, thai(""), "LR", 0, "C");
        $pdf->Cell(30, 7, thai(""), "L", 0, "L");
        $pdf->Cell(100, 7, thai("กำหนดชำระงวดต่อไป (".($data->{"customer_car_insurance_payment::period_no"}+1).'/'.$data->{"product::total_round"}.")  วันที่ ".convertToYearThai($next_round_date)."  จำนวนเงิน"), "", 0, "L");
        $pdf->Cell(40, 7, thai(number_format($data->{"customer_car_insurance_payment::amount"} ,2)), "", 0, "R");
        $pdf->Cell(30, 7, thai(""), "R", 0, "L");
        $pdf->Cell(45, 7, thai(""), "LR", 0, "R");
        $pdf->Ln();
        $pdf->Cell(30, 5, thai(""), "LR", 0, "C");
        $pdf->Cell(30, 5, thai(""), "L", 0, "L");
        $pdf->Cell(50, 5, thai(""), "", 0, "L");
        $pdf->Cell(30, 5, thai(""), "", 0, "R");
        $pdf->Cell(90, 5, thai(""), "R", 0, "L");
        $pdf->Cell(45, 5, thai(""), "LR", 0, "R");
    }
    else{
        $pdf->Ln();
        $pdf->Cell(30, 12, thai(""), "LR", 0, "C");
        $pdf->Cell(200, 12, thai(""), "LR", 0, "L");
        $pdf->Cell(45, 12, thai(""), "LR", 0, "R");
    }
    
    $pdf->Ln();
    $pdf->Cell(30, 5, thai(""), "LR", 0, "C");
    $pdf->Cell(200, 5, thai(""), "LR", 0, "L");
    $pdf->Cell(45, 5, thai(""), "LR", 0, "R");
    $pdf->Ln();
    $pdf->SetFont('angsana','B',14);
    $pdf->Cell(30, 7, thai(""), "LR", 0, "C");
    $pdf->Cell(120, 7, thai("รวมยอดสุทธิ"), "L", 0, "L");
    $pdf->Cell(80, 7, thai("(". convertToCurrencyThai($data->{"customer_car_insurance_payment::amount"}).")"), "R", 0, "L");
    $pdf->Cell(45, 7, thai(number_format($data->{"customer_car_insurance_payment::amount"} ,2)), "LR", 0, "R");
    $pdf->Ln();
    $pdf->Cell(30, 3, thai(""), "LRB", 0, "C");
    $pdf->Cell(200, 3, thai(""), "LRB", 0, "L");
    $pdf->Cell(45, 3, thai(""), "LRB", 0, "R");
    $pdf->Ln();
    $pdf->SetFont('angsana','B',14);
    $pdf->Cell(30, 8, thai("หมายเหตุ :"), "", 0, "L");
    $pdf->SetFont('angsana','',14);
    $pdf->Cell(200, 8, thai("โปรดตรวจสอบความถูกต้อง หากมีข้อสงสัยกรุณาติดต่อบริษัท ตามที่อยู่ข้างต้น ภายใน 7 วัน"), "", 0, "L");
    $pdf->Ln();
    $pdf->SetFont('angsana','B',14);
    $pdf->Cell(30, 8, thai("หมายเหตุ :"), "", 0, "L");
    $pdf->SetFont('angsana','',14);
    $pdf->Cell(200, 8, thai("กรุณาชำระค่างวดผ่อนชำระ ภายในระยะเวลาที่กำหนด ".(empty($fine) ? "" : "หากชำระล่าช้าจะมีค่าปรับ ".$fine." บาท/งวด")." ทางบริษัทขอสงวนสิทธิ์ในการยกเลิกกรมธรรม์"), "", 0, "L");
    $pdf->Ln(10);
    $current_X = $pdf->GetX();
    $current_Y = $pdf->GetY();
    $pdf->SetFont('angsana','',14);
    $pdf->Image(base_url().'assets/images/logo.jpg',$current_X, $current_Y,60);
    $pdf->Cell(185, 8, thai(""), "", 0, "L");
    $current_X = $pdf->GetX();
    $current_Y = $pdf->GetY();
    if (isset($variable_config->payee_sign)) {
        $pdf->Image(base_url().$variable_config->payee_sign,$current_X, $current_Y,20);
    }
    $pdf->Cell(40, 8, thai(""), "B", 0, "L");
    $pdf->Cell(10, 8, thai(""), "", 0, "L");
    $current_X = $pdf->GetX();
    $current_Y = $pdf->GetY();
    if (isset($variable_config->authorized_person_sign)) {
        $pdf->Image(base_url().$variable_config->authorized_person_sign,$current_X, $current_Y,20);
    }
    $pdf->Cell(40, 8, thai(""), "B", 0, "L");
    $pdf->Ln();
    $pdf->Cell(185, 8, thai(""), "", 0, "L");
    $pdf->Cell(2, 8, thai("("), "", 0, "L");
    $pdf->Cell(36, 8, thai(isset($variable_config->payee_fullname) ? $variable_config->payee_fullname : ""), "", 0, "C");
    $pdf->Cell(2, 8, thai(")"), "", 0, "L");
    $pdf->Cell(10, 8, thai(""), "", 0, "L");
    $pdf->Cell(2, 8, thai("("), "", 0, "L");
    $pdf->Cell(36, 8, thai(isset($variable_config->authorized_person_fullname) ? $variable_config->authorized_person_fullname : ""), "", 0, "C");
    $pdf->Cell(2, 8, thai(")"), "", 0, "L");
    $pdf->Ln();
    $pdf->Cell(185, 8, thai(""), "", 0, "L");
    $pdf->Cell(40, 8, thai("ผู้รับชำระเงิน"), "", 0, "C");
    $pdf->Cell(10, 8, thai(""), "", 0, "L");
    $pdf->Cell(40, 8, thai("ผู้มีอำนาจลงนาม"), "", 0, "C");
    $pdf->Ln();
    $pdf->Cell(60, 8, thai(""), "", 0, "L");
    $pdf->Cell(100, 8, thai("Call Center: 02-1146165"), "", 0, "L");
    $pdf->Ln();
    $pdf->Cell(0, 8, thai("สอบถามรายละเอียดเพิ่มเติมติดต่อ AEC MOBILE CALL CENTER 02-1146165 เฉพาะวันทำการ (จันทร์-ศุกร์) เวลา 9.00 น.-18.00 น. https://www.banchainsurecash.com/"), "", 0, "L");

    $savepath = FCPATH."assets/uploads/receipt/";
    if (!file_exists($savepath)) {
        mkdir($savepath, 0777, true);
    }
    $file_name = uniqid();
    $pdffile = $savepath.$file_name.'.pdf';

    $pdf->Output("F",$pdffile, true);

    return base_url()."assets/uploads/receipt/".$file_name.'.pdf';
}


function genconfirmsmspdf($customer_car_insurance, $customer_car_insurance_payments, $broker_product)
{
    $codeigniter_instance =& get_instance();

    $codeigniter_instance->load->library("fpdf_library");
    // GENERATE PDF
    $pdf = $codeigniter_instance->fpdf_library->get();
    //---------------------START COVER ---------------------------------------------------
    $pdf->AddPage("P", "A4");
    $pdf->AddFont('angsana','','angsa.php');
    $pdf->AddFont('angsana','B','angsab.php');
    $pdf->SetMargins(10, 20 ,10);
    $pdf->SetFont('angsana','B',18);

    $pdf->SetFont('angsana','B',20);
    $pdf->Cell(0, 20, thai("แบบฟอร์มการยืนยันข้อมูล"), "", 0, "C");
    $pdf->Ln();
    $pdf->SetFont('angsana','B',18);
    $pdf->Cell(0, 15, thai("คำขอแบ่งชำระเบี้ยประกันภัย"), "", 0, "L");
    $pdf->Ln();
    $pdf->SetFont('angsana','',16);
    $pdf->MultiCell(0, 8, thai("ข้าพเจ้า (นาย/นาง/นางสาว) ".$customer_car_insurance->{"customer::title"}.$customer_car_insurance->{"customer::first_name"}.' '.$customer_car_insurance->{"customer::last_name"}."  อายุ ".(isset($customer_car_insurance->{"customer::birth_date"}) ? date_diff(date_create($customer_car_insurance->{"customer::birth_date"}), date_create('today'))->y : "________")." ปี "."วัน/เดือน/ปีเกิด ".(isset($customer_car_insurance->{"customer::birth_date"}) ? date("d/m/Y", strtotime($customer_car_insurance->{"customer::birth_date"})) : "__________________")." "."เลขบัตรประชาชน ".$customer_car_insurance->{"customer::citizen_id"}), "", "L", false);
    $pdf->MultiCell(0, 8, thai("สถานที่ติดต่อ".$customer_car_insurance->{"customer::address"}." ".$customer_car_insurance->{"address_sub_district::name"}." ".$customer_car_insurance->{"address_district::name"}." ".$customer_car_insurance->{"address_province::name"}." ".$customer_car_insurance->{"customer::postcode"}." "."เบอร์โทรฯ ".$customer_car_insurance->{"customer::mobile_number"}." (ข้าพเจ้า)"), "", "L", false);
    $pdf->MultiCell(0, 8, thai(''), "", "L", false);
    $pdf->MultiCell(0, 8, thai('ขอทำคำขอแบ่งชำระเบี้ยประกันภัยฉบับนี้ ("คำขอ") ให้ไว้แก่บริษัท เออีซี โมบาย จำกัด ("บริษัทฯ") เพื่อเป็นหลักฐานแสดงว่า "ข้าพเจ้า"รับทราบ และตกลงให้"บริษัทฯ" เป็นผู้ชำระค่าเบี้ยประกันภัยให้แก่ตัวแทน/นายหน้าประกันภัย บริษัท '.$customer_car_insurance->{"broker::name"}.' จำกัด แทน"ข้าพเจ้า" เป็นจำนวนเงินทั้งสิ้น '.number_format($customer_car_insurance->{"customer_car_insurance::net_price"}).' บาท ('.ReadNumber($customer_car_insurance->{"customer_car_insurance::net_price"}).'บาทถ้วน) เพื่อชำระให้แก่บริษัทรับประกันภัย บริษัท '.$customer_car_insurance->{"insurance_company::name"}.' จำกัด(มหาชน) ตามกรมธรรม์ประกันภัย/เลขที่รับแจ้ง เลขที่ '.(!empty($customer_car_insurance->{"customer_car_insurance::insurance_number"}) ? $customer_car_insurance->{"customer_car_insurance::insurance_number"} :  $customer_car_insurance->{"customer_car_insurance::noti_number"}).' สำหรับ ทะเบียนรถ '.$customer_car_insurance->{"customer_car::reg_number"}.' รายละเอียดตามเอกสารแนบ (ถ้ามี)'), "", "L", false);
    $pdf->MultiCell(0, 15, thai('"ข้าพเจ้า"จึงตกลงผูกพันตนตามเงื่อนไข ดังต่อไปนี้'), "", "L", false);
    $pdf->MultiCell(0, 8, thai(' 1. "ข้าพเจ้า"ตกลงมอบกรมธรรม์ประกันภัยและต้นฉบับใบเสร็จรับเงินค่าเบี้ยประกันภัยดังกล่าวข้างต้น ให้"บริษัทฯ" เป็นผู้เก็บรักษาแทน"ข้าพเจ้า" จนกว่าจะมีการชำระค่าเบี้ยประกันภัยดังกล่าวข้างต้นคืนให้แก่"บริษัทฯ" จนครบถ้วน'), "", "L", false);
    $pdf->MultiCell(0, 8, thai(' 2. "ข้าพเจ้า"ตกลงแบ่งชำระเงินค่าเบี้ยประกันภัยดังกล่าวข้างต้นคืนให้แก่"บริษัทฯ" เป็นจำนวน '.$customer_car_insurance->{"customer_car_insurance::number_installment"}.' งวด ตามรายละเอียดดังนี้'), "", "L", false);
    $pdf->Cell(0,8,thai(""),"",1,"L");
    $pdf->Cell(10,8,thai(""),"",0,"L");
    $pdf->Cell(15,8,thai("งวดที่"),"LRTB",0,"C");
    $pdf->Cell(30,8,thai("วันที่"),"LRTB",0,"C");
    $pdf->Cell(125,8,thai("จำนวนเงิน (บาท)"),"LRTB",0,"C");
    $pdf->Cell(10,8,thai(""),"",0,"L");
    $pdf->Ln();
    $pdf->Cell(10,8,thai(""),"",0,"L");
    $pdf->Cell(15,8,thai("1"),"LRTB",0,"C");
    $pdf->Cell(30,8,thai(date("d/m/Y", strtotime($customer_car_insurance->{"customer_car_insurance::purchase_date"}))),"LRTB",0,"C");
    $pdf->Cell(125,8,thai(number_format($customer_car_insurance->{"customer_car_insurance::first_payment"},2).' (ได้ชำระ ณ วันที่ซื้อกรมธรรม์ข้างต้นเป็นที่เรียบร้อยแล้ว)'),"LRTB",0,"L");
    $pdf->Cell(10,8,thai(""),"",0,"L");
    $pdf->Ln();
    foreach ($customer_car_insurance_payments as $payment) {
        $pdf->Cell(10,8,thai(""),"",0,"L");
        $pdf->Cell(15,8,thai($payment->period_no),"LRTB",0,"C");
        $pdf->Cell(30,8,thai(date("d/m/Y", strtotime($payment->payment_due_date))),"LRTB",0,"C");
        $pdf->Cell(125,8,thai(number_format($payment->amount,2)),"LRTB",0,"L");
        $pdf->Cell(10,8,thai(""),"",0,"L");
        $pdf->Ln();
    }
    $pdf->Cell(0,8,thai(""),"",1,"L");
    $pdf->SetFont('angsana','B',16);
    $pdf->MultiCell(0, 8, thai('โดยผ่อนชำระเข้าบัญชีเลขที่ 074-3-67931-8 ชื่อบัญชี "บริษัท เออีซี โมบาย จำกัด" หรือ ผ่านระบบ QR Code/Barcode ของธนาคารกสิกรไทย จำกัด(มหาชน) หรือ ชำระโดยการสั่งจ่ายเช็คล่วงหน้าให้ไว้แก่"บริษัทฯ" เท่านั้น'), "", "L", false);
    $pdf->Cell(0,8,thai(""),"",1,"L");
    $pdf->SetFont('angsana','',16);
    $pdf->MultiCell(0, 8, thai('3. กรณีมีเหตุการณ์อย่างหนึ่งอย่างใดดังต่อไปนี้ ให้ถือว่า"ข้าพเจ้า"กระทำผิดเงื่อนไขข้อตกลงตามคำขอนี้ และให้"บริษัทฯ" หรือ ตัวแทน/นายหน้าประกันภัยของ"ข้าพเจ้า" ดำเนินการตามข้อ 4 และ ข้อ 5 ของคำขอนี้ได้ทันที'), "", "L", false);
    $pdf->MultiCell(0, 8, thai('      ก. "ข้าพเจ้า"ผิดนัด หรือ ไม่ชำระเงินจำนวนใดๆ ที่ถึงกำหนดชำระตามคำขอนี้  หรือไม่ปฏิบัติตามเงื่อนไขข้อหนึ่งข้อใดในคำขอนี้'), "", "L", false);
    $pdf->MultiCell(0, 8, thai('      ข. "ข้าพเจ้า"โอนกรรมสิทธิ์รถยนต์หรือทรัพย์สิน ที่เอาประกันภัยให้แก่บุคคลอื่น หรือเมื่อสิทธิตามกรมธรรม์ประกันภัยถูกโอน หรือตกแก่บุคคลอื่น'), "", "L", false);
    $pdf->MultiCell(0, 8, thai('      ค. "ข้าพเจ้า"ได้ทำการยกเลิกกรมธรรม์ประกันภัยตาม"คำขอ"นี้ โดยไม่ได้แจ้งให้แก่"บริษัทฯ" ทราบโดยทันที'), "", "L", false);
    $pdf->MultiCell(0, 8, thai('4. "ข้าพเจ้า"รับทราบ และ ตกลงยินยอมมอบสิทธิ์ให้นายหน้า/ตัวแทนประกันภัยของ"ข้าพเจ้า"ตามที่ระบุไว้ในกรมธรรม์ประกันภัยดังกล่าว หรือ "บริษัทฯ" ดำเนินการแจ้งยกเลิก หรือ ทำให้สิ้นผลผูกพันซึ่งกรมธรรม์ประกันภัยดังกล่าว โดยนายหน้า/ตัวแทนประกันภัย และ "บริษัทฯ" ต่างรับทราบถึงเงื่อนไข ภาระผูกพันของ"ข้าพเจ้า"ที่มีต่อ"บริษัทฯ" ตามคำขอนี้'), "", "L", false);
    $pdf->MultiCell(0, 8, thai('5. "ข้าพเจ้า"ได้ทำหนังสือขอยกเลิกกรมธรรม์ประกันภัย และการโอนสิทธิรับเงินจากการเรียกร้องค่าสินไหมทดแทน ค่าเสียหาย ค่ายกเลิกกรมธรรม์ประกันภัย ค่าเบี้ยประกันภัยที่จ่ายคืนตามที่ระบุไว้ในกรมธรรม์ประกันภัยดังกล่าวข้างต้น ให้แก่นายหน้า/ตัวแทนประกันภัยของ"ข้าพเจ้า"เป็นผู้รับเงินตามสิทธิดังกล่าว และนำเงินดังกล่าวทั้งหมดส่งให้"บริษัทฯ" เพื่อชำระค่าเบี้ยประกันภัย และ/หรือ ค่าใช้จ่ายดำเนินการตามคำขอนี้ของ"ข้าพเจ้า"ก่อน และหากมีเงินคงเหลือ ให้"บริษัทฯ"นำส่งคืนให้แก่"ข้าพเจ้า" แต่หากไม่เพียงพอต่อการชำระค่าเบี้ยประกัน และ/หรือ ค่าใช้จ่ายดำเนินการตามคำขอนี้ "ข้าพเจ้า"รับทราบและตกลงผูกพัน รับผิดชอบในภาระดังกล่าวส่วนที่ขาดนั้นให้แก่"บริษัทฯ"ทั้งจำนวนโดยครบถ้วน โดยที่ ตัวแทน/นายหน้าประกันภัย /"บริษัทฯ" ไม่ต้องรับผิดชอบต่อความเสียหายที่จะเกิดขึ้นภายหลังการยกเลิก หรือสิ้นผลความผูกพันของกรมธรรม์ประกันภัยดังกล่าวของ"ข้าพเจ้า"'), "", "L", false);
    $pdf->MultiCell(0, 8, thai('6. "ข้าพเจ้า"รับทราบและยอมรับ อัตราค่าปรับการติดตาม/ทวงถาม ในการชำระค่าเบี้ยประกันผ่อนชำระล่าช้าของ"ข้าพเจ้า" ในอัตรา '.$broker_product->fine.' บาท/ครั้ง(งวด)'), "", "L", false);
    $pdf->MultiCell(0, 8, thai('7. "ข้าพเจ้า"รับทราบดีว่า "บริษัทฯ" ได้สงวนสิทธิ์ในการเป็นผู้พิจารณาการยกเลิกกรมธรรม์ประกันภัยของ"ข้าพเจ้า" ตามข้อ 3 ของคำขอนี้ได้แต่เพียงผู้เดียว ภายใต้เงื่อนไขที่"บริษัทฯ" กำหนดเท่านั้น'), "", "L", false);
    $pdf->MultiCell(0, 8, thai('8. เมื่อ"บริษัทฯ" ส่งคำบอกกล่าวหรือหนังสือติดต่อใด ๆ ไปยังที่อยู่ หรือช่องทางติดต่อของ"ข้าพเจ้า" ตามที่ระบุไว้ข้างต้นของคำขอนี้ ไม่ว่าช่องทางหนึ่งช่องทางใด หรือที่อยู่ตามที่"ข้าพเจ้า"ได้แจ้งเปลี่ยนแปลงครั้งหลังสุดให้"บริษัทฯ" รับทราบ ให้ถือว่า"บริษัทฯ" ได้ส่งข้อความดังกล่าวให้แก่"ข้าพเจ้า" และ"ข้าพเจ้า"ได้รับข้อความนั้นโดยสมบูรณ์แล้ว'), "", "L", false);
    $pdf->MultiCell(0, 8, thai('9. "ข้าพเจ้า"ยินยอมให้"บริษัทฯ" จัดเก็บ ใช้ และเปิดเผยข้อมูลส่วนบุคคลของ"ข้าพเจ้า" เพื่อการปฎิบัติตามเงื่อนไข ข้อตกลง ตามวัตถุประสงค์ของคำขอนี้ และการเสนอผลิตภัณฑ์ และ/หรือ บริการอื่นใด ตามที่ทาง"บริษัทฯ" เห็นสมควร และเห็นว่าเป็นประโยชน์แก่"ข้าพเจ้า" ผ่านช่องทางการติดต่อสื่อสารใด ๆ ก็ตามที่"ข้าพเจ้า"ให้ข้อมูลไว้แก่"บริษัทฯ"'), "", "L", false);

    $pdf->SetFont('angsana','B',16);
    $pdf->Cell(0,15,thai('"ข้าพเจ้า"ได้อ่าน รับทราบ เข้าใจ และตกลงยินยอม ในรายละเอียดข้อความตามคำขอแบ่งชำระเบี้ยประกันภัยฉบับนี้ทุกประการ'),"",1,"L");

    $pdf->AddPage("P", "A4");
    $pdf->AddFont('angsana','','angsa.php');
    $pdf->AddFont('angsana','B','angsab.php');
    $pdf->SetMargins(10, 20 ,10);
    $pdf->SetFont('angsana','B',18);
    $pdf->Cell(0, 15, thai("คำขอยกเลิกกรมธรรม์ประกันภัย"), "", 1, "L");
    $pdf->SetFont('angsana','',16);
    $pdf->Cell(20,8, thai('เรื่อง'), "", 0, "L");
    $pdf->Cell(170,8, thai('ขอยกเลิกกรมธรรม์ประกันภัย และบอกกล่าวการโอนสิทธิการรับเงินจากการยกเลิกกรมธรรม์ประกันภัย'), "", 0, "L");
    $pdf->Ln();
    $pdf->Cell(20,8, thai('เรียน'), "", 0, "L");
    $pdf->Cell(170,8, thai('บริษัท '.$customer_car_insurance->{"insurance_company::name"}.' จำกัด (มหาชน)'), "", 0, "L");
    $pdf->Ln();
    $pdf->Cell(20,8, thai('อ้างอิง'), "", 0, "L");
    $pdf->Cell(170,8, thai('กรมธรรม์ประกันภัยเลขที่'.(empty($customer_car_insurance->{"customer_car_insurance::insurance_number"}) ? "__________________________________" : $customer_car_insurance->{"customer_car_insurance::insurance_number"})), "", 0, "L");
    $pdf->Ln();
    $pdf->Ln();
    $pdf->MultiCell(0, 8, thai('          "ข้าพเจ้า" "ผู้เอาประกันภัย"'), "", "L", false);
    $pdf->MultiCell(0, 8, thai('          (นาย/นาง/นางสาว) '.$customer_car_insurance->{"customer::title"}.$customer_car_insurance->{"customer::first_name"}.' '.$customer_car_insurance->{"customer::last_name"}), "", "L", false);
    $pdf->Ln();
    $pdf->MultiCell(0, 8, thai('          ได้ทำสัญญาประกันภัยไว้กับท่าน ซึ่งเป็นผู้รับประกันภัย ระยะเวลาความคุ้มครองเริ่มตั้งแต่วันที่ '.date("Y-m-d", strtotime($customer_car_insurance->{"customer_car_insurance::effective_date"}))." ถึงวันที่ ".date("Y-m-d", strtotime($customer_car_insurance->{"customer_car_insurance::expiry_date"})).' รายละเอียดปรากฏตามกรมธรรม์ประกันภัยที่อ้างถึงนั้น'), "", "L", false);
    $pdf->Ln();
    $pdf->MultiCell(0, 8, thai('          ข้าพเจ้า"มีความประสงค์ขอยกเลิกกรมธรรม์ประกันภัยดังกล่าว และตกลงโอนสิทธิการแจ้งยกเลิกกรมธรรม์ประกันภัย และการรับเงินเบี้ยประกันภัยคืนจากการยกเลิกกรมธรรม์ประกันภัยดังกล่าว ให้แก่นายหน้า หรือ ตัวแทนประกันภัยของ"ข้าพเจ้า" เพื่อนำเงินจำนวนดังกล่าว ชำระภาระหนี้และดำเนินการตามสัญญาให้แก่ "บริษัท เออีซี โมบาย จำกัด" ต่อไป'), "", "L", false);
    $pdf->Ln();
    $pdf->MultiCell(0, 8, thai('          จึงเรียนมาเพื่อทราบและโปรดดำเนินการ'), "", "L", false);
    $pdf->SetFont('angsana','B',16);
    $pdf->Cell(0,15,thai('"ข้าพเจ้า"ได้อ่าน รับทราบ เข้าใจ และตกลงยินยอม ในรายละเอียดข้อความตามคำขอแบ่งชำระเบี้ยประกันภัยฉบับนี้ทุกประการ'),"",1,"L");

    $savepath = FCPATH."assets/uploads/confirm_sms/";
    if (!file_exists($savepath)) {
        mkdir($savepath, 0777, true);
    }
    $file_name = uniqid();
    $pdffile = $savepath.$file_name.'.pdf';


    $pdf->Output("F",$pdffile, true);

    return base_url()."assets/uploads/confirm_sms/".$file_name.'.pdf';
}

function thai($string){
    if (function_exists('iconv')) {
        $string = iconv("UTF-8", "TIS-620//TRANSLIT//IGNORE", $string);
    }
    return $string;
}

?>