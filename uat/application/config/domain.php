<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

$server_addr = $_SERVER['SERVER_ADDR'];
$server_name = $_SERVER['SERVER_NAME'];
$copyright = true;

// check ip address and domain name of Server
if($server_addr=="x.x.x.x")
{
	if(preg_match('/darksite/', $server_name) || preg_match('/darksite/', $server_name)) $copyright = false;
}
else
{
	if(preg_match('/darksite/', $server_name) || preg_match('/darksite/', $server_name)) $copyright = false;
}



if($copyright)
{
	$config['website_name'] = 'Mazzflix';
	$config['website_logo'] = 'assets/emin/layout/img/logo-big.pngxxx';
	$config['website_copyright'] = true;
	$config['website_email'] = 'info@mazzflix.com';
	$config['website_template'] = 'content';
}
else 
{
	$config['website_name'] = 'Mazzflix No';
	$config['website_logo'] = 'assets/emin/layout/img/logo-big.pngxxx';
	$config['website_copyright'] = false;
	$config['website_email'] = 'info@mazzflix.com';
	$config['website_template'] = 'darksite';
}