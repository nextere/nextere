<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['WEBSITE_NAME'] = 'Nextere';
$config['WEBSITE_OWNER'] = '';
$config['WEBSITE_TEAM'] = '';
$config['WEBSITE_ADDRESS'] = 'บริษัท เนกซ์เทียร์จํากัด
<br>88/188 ถนน เทศบาลสงเคราะห์แขวง ลาดยาว เขตจตุจักร กรุงเทพมหานคร 10900
<br>เลขประจําตัวผู้เสียภาษี: 0105564101218 โทร 02-121-4645 
<br>(จันทร์-ศุกร์ยกเว้นวันหยุดนักขัตฤกษ์เวลาทําการ : 10.00 น. - 19.00 น.)';
$config['WEBSITE_FAVICON'] = 'favicon.ico';
$config['WEBSITE_COPYRIGHT'] = '';
$config['WEBSITE_REFERENCE'] = '';
$config['WEBSITE_LOGO'] = 'logo.png';
$config['WEBSITE_API_URL'] = 'https://www.nextere.space/';