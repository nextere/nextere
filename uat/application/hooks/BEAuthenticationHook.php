<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BEAuthenticationHook {
	
	public function initialize() 
	{
		$ci = &get_instance();
		$pathArr = array("be/","be/reports/"); 
		// check directory of class
		if(in_array($ci->router->directory, $pathArr))
		{
			if($ci->session->userdata('_beId')<0)
			{
				// check remember
				if($ci->input->cookie('_beId')>0)
				{
					$_beId = $ci->input->cookie('_beId');
					$_beUsername = $ci->input->cookie('_beUsername');
					$_beEmail = $ci->input->cookie('_beEmail');
					$_beToken = $ci->input->cookie('_beToken');
					 
					$ci->load->model('UserTable');
					$ci->load->model('RoleTable');
					
					$filter = $this->UsersTable;
					$filter->username = $username;
					$filter->status = 1;
					
					$admins = $this->UsersTable->getLogin($filter);
					$admin = $admins[0];
					 
					if(isset($admin) && $ci->UserTable->token($admin)==$_beToken)
					{
						$adminRole = $this->RoleTable->find($admin->roleId);
						$token = $this->UsersTable->token($admin);
				
						$item = array(
							'_beId' => $admin->id,
							'_beName' => $admin->name." ".$admin->surname,
							'_beUsername' => $admin->username,
							'_beEmail' => $admin->email,
							'_beRoleId' => $admin->roleId,
							'_beGroupName' => $adminRole->name,
							'_beToken' => $token,
						);
				
						$ci->session->set_userdata($item);
					}
				}
			}
	    	
	    	else if($ci->session->userdata('_beId')<=0 && !($ci->router->fetch_class()=="login" && $ci->router->fetch_method()=="index") )
	    	{
	    		$url = base_url(uri_string());
	    		if($ci->input->server('QUERY_STRING')!="")
	    		{
	    			$url .= "?".$ci->input->server('QUERY_STRING');
	    		}
	    		
	    		if($url!="")
	    		{
	    			redirect(base_url('be/login').'?url='.urlencode($url));
	    		}
	    		else 
	    		{
	    			redirect(base_url('be/login'));
	    		}
	    	}
			else if(!($ci->router->fetch_class()=="login")){ //save log
				$class =$ci->router->fetch_class();
				$url = $ci->uri->uri_string;
				$getArr = $ci->input->get();
				$postArr = $ci->input->post();
				$get = join(',',$getArr);
				$post = join(',',$postArr);
				$ci->load->model('LogBeTable');
				$obj = $ci->LogBeTable;
				$obj->userId = $ci->session->userdata('_beId');
				$obj->url = $url;
				$obj->class = $class;
				$obj->get = $get;
				$obj->post = $post;
				$ci->LogBeTable->save($obj);
			}
		}
		
	}
}