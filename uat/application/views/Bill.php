<?php include('header.php') ;
// echo '<pre>';
// print_r($order_limit[0]);
// exit();
?>
<!-- <link href="../assets/css/style-content.css" rel="stylesheet"> -->
<style>
    .bill .dashed {
        border: 1px dashed #BCBCBC;
        padding: 30px;
    }

    .bill .btn {
        border-radius: 20px;
        font-size: 12px;
    }
</style>

<body>
    <section class="bill">
        <div class="row ">
            <div class="pt-8 pb-8" style="background-color:#F0F0F0; text-align: center;">
                <h3><b><?php echo getWording('payment', 'payment_completed') ?></b></h3>
                <h3><b><?php echo getWording('quotation', 'thank') ?> Nextere</b></h3>
            </div>
            <div class="row justify-content-center pt-10">
                <div class="col-3" style="text-align: center;">
                </div>
                <div class="col-3">
                    <b><?php echo getWording('quotation', 'order_no') ?></b>
                </div>
                <div class="col-3" style="text-align: right;">
                    <b><?php echo $order_limit[0]->order_id ?></b>
                </div>
                <div class="col-3" style="text-align: center;">
                </div>
            </div>
            <div class="row justify-content-center pt-5">
                <div class="col-3" style="text-align: center;">
                </div>
                <div class="col-3">
                    <b><?php echo getWording('payment', 'order_date') ?></b>
                </div>
                <div class="col-3" style="text-align: right;">
                    <b><?php echo $order_limit[0]->created_date ?></b>
                </div>
                <div class="col-3" style="text-align: center;">
                </div>
            </div>
            <div class="row justify-content-center pt-5">
                <div class="col-3" style="text-align: center;">
                </div>
                <div class="col-3">
                    <b><?php echo getWording('payment', 'payment_type') ?></b>
                </div>
                <div class="col-3" style="text-align: right;">
                    <?php if ($order_limit[0]->payment_id == '1') { ?>
                        <b><?php echo getWording('profile', 'credit') ?></b>
                    <?php } else if ($order_limit[0]->payment_id == '2') { ?>
                        <b>Internet banking</b>
                    <?php } else if ($order_limit[0]->payment_id == '3') { ?>
                        <b>PromtPay</b>
                    <?php } else { ?>
                        <?php echo '-' ?>
                    <?php } ?>
                </div>
                <div class="col-3" style="text-align: center;">
                </div>
            </div>
            <hr style="width:50%; margin-left:25% !important; margin-right:25% !important; margin-top:15px !important;" />
            <div class="col-12 pt-4" style="text-align:center;">
                <h5><b> * <?php echo getWording('payment', 'sent_email') ?> <?php echo $user_info->email ?> <?php echo getWording('quotation', 'your') ?></b></h5>
            </div>
            <div class="col-12 pt-10" style="text-align:center;">
                <h5><b><?php echo getWording('payment', 'order_details') ?></b></h5>
            </div>
        </div>
        <div class="row justify-content-center mt-3">
            <div class="col-7 dashed">
                <div class="row">
                    <?php foreach ($order_limit[0]->purchase_transaction_detail as $key => $order_item) { ?>
                        <?php foreach ($order_product_history as $key => $order_product) { ?>
                            <?php if ($order_product->{'product::id'} == $order_item->product_id) { ?>
                                <div class="col-2">
                                    <?php if (empty($order_product->{'product::images'})) { ?>
                                        <img class="w-100" src="<?php echo base_url('/assets/img/no-images.png') ?>" alt="">
                                    <?php } else { ?>
                                        <img class="w-100" src="<?php echo $order_product->{'product::images'}[0] ?>" alt="">
                                    <?php } ?>
                                </div>
                                <div class="col-8" style="padding-left: 20px; align-self: center;">
                                    <h5 class="cart-item-title w-75"><?php echo getVariable($order_product, 'product::name')  ?></h5>
                                    <h5 class="cart-item-title w-75">Model : <?php echo getVariable($order_product, 'product::model_name')  ?></h5>
                                    <h5 class="cart-item-title w-75"><?php echo getWording('product', 'quantity') ?> : <?php echo $order_item->qty ?></h5>
                                </div>
                                <div class="col-2" style="text-align: right;  align-self: center;"><b><?php echo number_format($order_item->total_price + ($order_item->qty * $order_product->{'product::delivery_price'}))  ?> <?php echo getWording('index', 'baht') ?> </b></div>
                                <hr class="mt-3" style="color: #BCBCBC ;">
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </div>

            </div>

            <div class="col-6 pt-8 pb-10" style="text-align:right;">
                <a href="<?php echo base_url('') ?>"> <button type="button" class="btn btn-outline-dark pb-5" style="width: 250px; height: 40px; padding-top: 10px; border-radius: 50px;"> <b><?php echo getWording('quotation', 'home') ?></b></button></a>
            </div>
            <div class="col-6 pt-8 pb-10">
                <a href="<?php echo base_url('Account/order_history') ?>"><button type="button" class="btn btn-outline-dark pb-5" style="width: 250px; height: 40px; padding-top: 10px; background-color: #D3D3D3; border: 0px; border-radius: 50px;"> <b><?php echo getWording('payment', 'view') ?></b></button></a>
            </div>
        </div>
        <div class="mt-5"> <?php include('footer.php') ?></div>
    </section>
</body>

</html>