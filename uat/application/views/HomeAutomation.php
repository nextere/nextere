<?php include('header.php') ?>
<link href="../assets/css/style-content.css" rel="stylesheet">
<style>
    .subsolution_banner {
        position: relative;
        text-align: center;
        color: black;
    }

    .centered {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        /* color: black; */
    }

    .solution ul {
        column-count: 2;
        column-gap: 20px;
        list-style-type: none;
        margin: 0;
        padding: 0;
    }
</style>

<?php
// echo '<pre>';
// print_r($sub_category_list);
// exit();
?>

<body>
    <?php
    // echo '<pre>';
    // print_r($sub_category_list->data->submenu_category->banner_path);
    // exit();
    ?>

    <div class="subsolution_banner">
        <?php if ($sub_category_list->data->submenu_category->banner_path != '') { ?>
            <img class="w-100" src="<?php echo base_url($sub_category_list->data->submenu_category->banner_path) ?>" alt="">
            <div class="centered">
                <h3><b><?php echo getVariable($sub_category_list->data->submenu_category, 'name') ?></b></h3>
            </div>
        <?php } else { ?>
            <img class="w-100" src="<?php echo base_url('/assets/img/no-banner.png') ?>" alt="">
            <div class="centered">
                <h3><b style="color: black;"><?php echo getVariable($sub_category_list->data->submenu_category, 'name') ?></b></h3>
            </div>
        <?php } ?>
    </div>

    <div class="container mt-5 ">
        <div class="row">
            <?php if (!empty($sub_category_list->data->child_submenu_categories)) { ?>
                <?php foreach ($sub_category_list->data->child_submenu_categories as $key => $child_submenu_list) { ?>
                    <div class="col-sm-6 col-xl-4" style="text-align:left">
                        <div class="product-item">
                            <?php if ($child_submenu_list->banner_path != '') { ?>
                                <img class="w-100" src="<?php echo base_url($child_submenu_list->banner_path) ?>" alt="">
                            <?php } else { ?>
                                <img class="w-100" src="<?php echo base_url('/assets/img/image-none.png') ?>" alt="">
                            <?php } ?>
                            <div style="text-align:left">
                                <h5 class="mt-3"><b><?php echo getVariable($child_submenu_list, 'name') ?></b></h5>
                                <div class=" pb-3"><?php echo getVariable($child_submenu_list, 'description') ?></div>
                                <a href="<?php echo base_url('Solution/soluton_product_list/' . $child_submenu_list->id . '?type=child_category&submenu_id=' . $category_list->submenu_category_id . '&child_id=' . $child_submenu_list->id) ?>" class="btn btn-secondary " style="width: 150px; height: 40px; padding-top: 11px;border-radius: 25px;"><?php echo getWording('index', 'more') ?> <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <h3 class="mt-5 text-center"><?php echo getWording('product', 'no_product') ?></h3>
            <?php } ?>

        </div>

    </div>

    <?php if (!empty($sub_category_list->data->products)) { ?>
        <div class="row mt-5">
            <div class="col-sm-12 col-xl-12 " style=" text-align: center;">
                <h4><b><?php echo getWording('product', 'product') ?></b></h4>
            </div>
        </div>
        <div class="container">
            <div class="row " id="products">
                <?php foreach ($sub_category_list->data->products as $key => $product_child_list) { ?>
                    <div class="col-sm-6 col-xl-3 mt-4 item" style="text-align:left">
                        <div class="product-item">
                            <div class="border-images">
                                <?php if (property_exists($product_child_list, 'product::images')) { ?>
                                    <a href="<?php echo base_url('/Product/Details/' . $product_child_list->{'product::id'}) ?>">
                                        <img class="w-100" src="<?php echo $product_child_list->{'product::images'}[0]  ?>" alt="">
                                    </a>
                                <?php } else { ?>
                                    <a href="<?php echo base_url('/Product/Details/' . $product_child_list->{'product::id'}) ?>">
                                        <img class="w-100" src="<?php echo base_url('/assets/img/no-images.png') ?>" alt="">
                                    </a>
                                <?php } ?>
                            </div>
                            <h4 class="mt-3"><?php echo getVariable($product_child_list, 'product::name') ?></h4>

                            <?php if ($product_child_list->{'product::model_name_th'}  == '') { ?>
                                <span style="color:white;">...</span>
                            <?php } else if ($product_child_list->{'product::model_name_en'}  == '') { ?>
                                <span style="color:white;">...</span>
                            <?php } else { ?>
                                <span><?php echo getVariable($product_child_list, 'product::model_name') ?></span>
                            <?php } ?>
                            <?php if ($product_child_list->{'product::retail_price'} != null) { ?>
                                <?php if (empty($product_child_list->{'product::discount'})) { ?>
                                    <h4 class="mt-6">
                                        <?php echo number_format($product_child_list->{'product::retail_price'}); ?>
                                        <?php echo getWording('index', 'baht') ?>
                                    </h4>
                                <?php } else { ?>
                                    <h4 class="mt-1">
                                        <div class="row">
                                            <span class="col-6">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <?php if ($product_child_list->{'product::discount'}->discount_percent > "1") { ?>
                                                            <span class="discount-product">- <?php echo number_format($product_child_list->{'product::discount'}->discount_percent); ?> <?php echo getWording('index', 'baht') ?></span>
                                                        <?php } else { ?>
                                                            <span class="discount-product">- <?php echo ($product_child_list->{'product::discount'}->discount_percent * 100); ?>%</span>
                                                        <?php } ?>
                                                    </div>
                                                    <span class="strikethrough">
                                                        <?php echo number_format($product_child_list->{'product::retail_price'}); ?>
                                                        <?php echo getWording('index', 'baht') ?>
                                                    </span>
                                                </div>
                                            </span>
                                            <span class="col-6 mt-5" style="color: red;">
                                                <?php if ($product_child_list->{'product::discount'}->total_price > "1") { ?>
                                                    <?php echo number_format($product_child_list->{'product::discount'}->total_price); ?>
                                                <?php } else { ?>
                                                    0
                                                <?php } ?>
                                                <?php echo getWording('index', 'baht') ?>
                                            </span>
                                        </div>
                                    </h4>
                                <?php } ?>
                                <div class="row mt-5">
                                    <?php if ($token != '') { ?>
                                        <div class="col-8 pr-1">
                                            <button class="order_btn w-100" data-quantity="1" data-product-qty="<?php echo $product_child_list->{'product::in_stock_qty'} ?>" data-product-id="<?php echo $product_child_list->{'product::id'} ?>"><?php echo getWording('index', 'buy') ?></button>
                                        </div>
                                    <?php } else { ?>
                                        <div class="col-8 pr-1">
                                            <a href="<?php echo base_url('Account/login') ?>"><button class="w-100" ? style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"><?php echo getWording('index', 'buy') ?></button></a>
                                        </div>
                                    <?php } ?>

                                    <div class="col-4 pl-1">
                                        <button type="button" class="filter_btn w-100 p-0" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" data-compare="<?php echo $product_child_list->{'product::id'} ?>">
                                            <svg class="w-100">
                                                <use xlink:href="#icon-filter"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <h5 class="mt-6">
                                    <b><?php echo getWording('index', 'interested_product') ?></b>
                                </h5>
                                <div class="row mt-5">
                                    <div class="col-8 pr-1">
                                        <a href="<?php echo base_url('Contact') ?>"><button class="w-100" ? style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"><?php echo getWording('index', 'contact') ?></button></a>
                                    </div>
                                    <div class="col-4 pl-1">
                                        <button id="test-btn" type="button" class="filter_btn w-100 p-0" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" data-compare="<?php echo $product_child_list->{'product::id'} ?>">
                                            <svg class="w-100">
                                                <use xlink:href="#icon-filter"></use>
                                            </svg>
                                        </button>
                                    </div>

                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="row justify-content-center mt-6">
                <div class="col-6 col-xl-2">
                    <button class="see_all_btn w-100 py-2" style="width: 150px; height: 40px; padding-top: 11px;" onclick="location.href='<?php echo base_url('Solution/soluton_product_list/' . $sub_category_list->data->submenu_category_id . '?type=sub_solution') ?>'"><?php echo getWording('index', 'more') ?></button>
                </div>
            </div>
        </div>
    <?php } ?>



    <div class="mt-5"> <?php include('footer.php') ?></div>


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="vender/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
    <svg aria-hidden="true" style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="icon-filter" width="100%" viewBox="0 0 25 25">
                <g>
                    <path d="M1.75,7.75h6.6803589c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891   S13.9000854,9.0452271,14.2356567,7.75H24.25C24.6640625,7.75,25,7.4140625,25,7s-0.3359375-0.75-0.75-0.75H14.2356567   c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891S8.7659302,4.9547729,8.4303589,6.25H1.75   C1.3359375,6.25,1,6.5859375,1,7S1.3359375,7.75,1.75,7.75z M11.3330078,5.4912109   c0.8320313,0,1.5087891,0.6767578,1.5087891,1.5087891s-0.6767578,1.5087891-1.5087891,1.5087891S9.8242188,7.8320313,9.8242188,7   S10.5009766,5.4912109,11.3330078,5.4912109z" fill="#1D1D1B" />
                    <path d="M24.25,12.25h-1.6061401c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891   S17.1741333,10.9547729,16.838562,12.25H1.75C1.3359375,12.25,1,12.5859375,1,13s0.3359375,0.75,0.75,0.75h15.088562   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891s2.5670776-0.963562,2.9026489-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,12.25,24.25,12.25z M19.7412109,14.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891S21.25,12.1679688,21.25,13   S20.5732422,14.5087891,19.7412109,14.5087891z" fill="#1D1D1B" />
                    <path d="M24.25,18.25H9.7181396c-0.3355103-1.2952271-1.5037842-2.2587891-2.9017334-2.2587891   c-1.3987427,0-2.5670776,0.963562-2.9026489,2.2587891H1.75C1.3359375,18.25,1,18.5859375,1,19s0.3359375,0.75,0.75,0.75h2.1637573   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891c1.3979492,0,2.5662231-0.963562,2.9017334-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,18.25,24.25,18.25z M6.8164063,20.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891   c0.8310547,0,1.5078125,0.6767578,1.5078125,1.5087891S7.6474609,20.5087891,6.8164063,20.5087891z" fill="#1D1D1B" />
                </g>
            </symbol>
        </defs>
    </svg>
</body>

</html>