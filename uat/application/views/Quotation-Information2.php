<?php include('header.php') ?>

<style>
    input[type=text],
    select,
    textarea {
        padding: 12px;
        margin-top: 6px;
        margin-bottom: 16px;
        width: 100%;
        height: 45px;
        background: #FFFFFF;
        border: 1px solid #707070;
        border-radius: 5px;
    }

    input[type=tel],
    select,
    textarea {
        padding: 12px;
        margin-top: 6px;
        margin-bottom: 16px;
        width: 100%;
        height: 45px;
        background: #FFFFFF;
        border: 1px solid #707070;
        border-radius: 5px;
    }

    input[type=submit] {
        width: 179px;
        height: 45px;
        background: #7C7C7C;
        color: white;
        border-radius: 23px;
    }
</style>

<body>
    <div class="container" style="text-align: -webkit-center;">
        <div class="col-sm-6 col-xl-8">
            <div class="head25" style="text-align: center; padding-top: 69px;">
                <h2>ข้อมูลการขอใบเสนอราคา</h2>
            </div>
            <form id="dataForm">
                <hr style="border-width: 3px;">
                <div class="radio-page24" style="text-align: left;">
                    <div style="display: flex;">
                        <!-- <a href="http://localhost/nextere2/QuotationInformation"> -->
                        <label class="col-sm-12 col-xl-2 m-4">
                            <input type="radio" onclick="location.href='<?php echo base_url('Quotation') ?>'" name="type">ประเภทบุคคลธรรมดา
                        </label>
                        <!-- </a> -->
                        <!-- <a href="QuotationInformation/entity"> -->
                        <label class="col-sm-12 col-xl-2 mt-4 mt-4">
                            <input type="radio" onclick="location.href='<?php echo base_url('Quotation/entity') ?>'" name="type" checked="true">ประเภทนิติบุคคล
                        </label>
                        <!-- </a> -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-6 mt-3" style="text-align:left;">
                        <b>ชื่อ*</b>
                        <input type="text" id="name" name="name" placeholder="ชื่อ" required>

                    </div>

                    <div class="col-xl-6 mt-3" style="text-align:left;">
                        <b>นามสกุล*</b>
                        <input type="text" id="lastname" name="lastname" placeholder="นามสกุล" required>

                    </div>

                    <div class="col-xl-6 mt-2" style="text-align:left;">
                        <b>เบอร์โทรติดต่อ*</b>
                        <input type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" id="telephone" name="telephone" placeholder="เบอร์โทร" required>
                    </div>

                    <div class="col-xl-6 mt-2" style="text-align:left;">
                        <b>อีเมล*</b>
                        <input type="text" id="email" name="email" placeholder="อีเมล" required>

                    </div>

                    <div class="col-xl-6 mt-2" style="text-align:left;">
                        <b>ชื่อบริษัท*</b>
                        <input type="text" id="company" name="company" placeholder="ชื่อบริษัท" required>

                    </div>

                    <div class="col-xl-6 mt-2" style="text-align:left;">

                        <b>ประเภทธุรกิจ*</b>
                        <select id="category" name="category" style=" height: 48px;">
                            <option value="big">ประเภทธุรกิจ</option>
                            <option value="small">ธุรกิจขนาดเล็ก</option>
                        </select>

                    </div>

                    <div class="col-xl-6 mt-2" style="text-align:left;">
                        <b>เลขทะเบียนนิติบุคคล 13 หลัก*</b>
                        <input type="text" id="tax_id" name="tax_id" onKeyPress="return isNumber(event)" min="0" maxlength="13" placeholder="เลขทะเบียนนิติบุคคล 13 หลัก" required>

                    </div>

                    <div class="col-xl-12 mt-2" style="text-align:left;">
                        <b>ข้อความ*</b>
                        <textarea id="message" name="message" placeholder="ข้อความ.." style="width:  100%; height: 177px;"></textarea>

                    </div>
                </div>
                <button class="btn3 mt-3" style="width: 179px;
                    height: 45px; background: #7C7C7C; border: 0px; border-radius: 23px; font-weight: bold; color:white;">
                    <b>
                        ขอใบเสนอราคา
                    </b>
                </button>

                <div class="pt-1" style="text-align: left; padding-top: 80px;">
                    <p>หมายเหตุ</p>
                    <p>- รายการสินค้าดังกล่าวยังไม่รวมค่าขนส่งและค่าติดตั้ง</p>
                    <p>- Price Validity 7 days</p>

                </div>
            </form>
        </div>
    </div>


    <script>
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }

            return true;
        }
    </script>
    <script>
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }

            return true;
        }
    </script>

    <script>
        function phoneMask() {
            var num = $(this).val().replace(/\D/g, '');
            $(this).val(num.substring(0, 3) + '-' + num.substring(3, 6) + '-' + num.substring(6, 10));
        }

        $(document).ready(function() {
            $('#telephone').mask('000-000-0000');
            $("#dataForm").validate({
                rules: {
                    name: "required",
                    lastname: "required",

                    email: {
                        required: true,
                        email: true
                    },
                    telephone: {
                        required: true,
                        pattern: '\\d{3}-\\d{3}-\\d{4}',
                        minlength: 12,
                        maxlength: 12
                    },
                    company: "required",
                    message: "required"
                },
                messages: {
                    name: "Please enter your name",
                    lastname: "Please enter your lastname",
                    email: "Please enter a valid email address",
                    telephone: "Please correct telephone format",
                    company: "Please enter your company",
                    message: "Please enter your message",
                    tax_id: "Please enter your tax ID",
                },

                errorPlacement: function(error, element) {
                    error.insertBefore(element);
                }
            });
        });



        $("#dataForm").submit(function(e) {
            e.preventDefault();
            console.log('not submit!!');
            return false;
        });
    </script>

</body>
<div class="mt-5">
    <?php include('footer.php') ?>
</div>