<?php include('header.php') ?>
<style>
    .data ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 100%;
        background-color: white;
        border: 1px solid #BCBCBC;
        opacity: 1;
        font-size: 16px;
    }

    .data li a.active {
        background-color: #BCBCBC;
        color: white;
    }

    .data li a {
        display: block;
        color: #000;
        padding: 8px 12px;
        text-decoration: none;
        font-size: 14px;


    }

    .contact {
        list-style-type: none;
        width: 100%;

        padding: 20px;
        background-color: white;
        border: 1px solid #BCBCBC;
        opacity: 1;
        font-size: 16px;
    }

    .news {
        list-style-type: none;
        width: 100%;
        padding: 20px;
        background-color: white;
        border: 1px solid #BCBCBC;
        opacity: 1;
        font-size: 16px;
    }

    :root {
        --line-border-fill: #BCBCBC;
        --line-border-empty: #e0e0e0;
    }

    .progress-container::before {
        content: "";
        background: var(--line-border-empty);
        position: absolute;
        top: 50%;
        left: 0;
        transform: translateY(-50%);
        height: 4px;
        width: 100%;
        z-index: -1;
    }

    .progress-container {
        display: flex;
        justify-content: space-between;
        position: relative;
        margin-bottom: 30px;
        max-width: 100%;
        width: 350px;
    }

    .progress {
        background: var(--line-border-fill);
        position: absolute;
        top: 50%;
        left: 0;
        transform: translateY(-50%);
        height: 4px;
        width: 0%;
        z-index: -1;
        transition: 0.4s ease;
    }

    .circle {
        background: #fff;
        color: #999;
        border-radius: 50%;
        height: 50px;
        width: 50px;
        display: flex;
        align-items: center;
        justify-content: center;
        border: 3px solid var(--line-border-empty);
        transition: .4s ease;
    }

    .circle.active {
        border-color: var(--line-border-fill);
    }

    .button1 {
        background-color: var(--line-border-fill);
        color: #fff;
        cursor: pointer;
        font-family: inherit;
        border: 0;
        border-radius: 6px;
        padding: 8px 30px;
        margin: 5px;
        font-size: 14px;
    }

    .button1:active {
        transform: scale(0.98);
    }

    .button1:focus {
        outline: 0;
    }

    .button1:disabled {
        background-color: var(--line-border-empty);
        cursor: not-allowed;
    }
</style>
</head>
<?php
//  echo '<pre>';
//  print_r($order_limit);
//  exit();
?>

<body>
    <section class="data">
        <div class="container mt-5">
            <div class="row">
                <div class="col-sm-12 col-xl-12 pt-5 pb-5 pl-5 mt-5" style="background-color: lightgray; text-align:left">
                    <h4><b><?php echo getWording('profile', 'my_account') ?></b></h4>
                    <p><?php echo getWording('profile', 'hi') ?> <?php echo $user_info->fullname ?>, <?php echo getWording('profile', 'detail') ?></p>
                </div>
                <div class="col-sm-3 col-xl-3 mt-3 pl-0" style="text-align: left;">
                    <ul>
                        <li><a href="<?php echo base_url('Account/profile') ?>"> <img class="mr-3" style="width: 16px; height: 16px;" src="<?php echo base_url('/assets/img/icon/user.png') ?>"><?php echo getWording('profile', 'profile') ?></a></li>
                        <li><a class="active" href="<?php echo base_url('Account/order_history') ?>"><img class="mr-3" style="width: 20px; height: 18px;" src="<?php echo base_url('/assets/img/icon/shopping-cart-2.png') ?>"><?php echo getWording('profile', 'my_orders') ?></a></li>
                        <li><a href="<?php echo base_url('Account/quotation_request_history') ?>"><img class="mr-3" style="width: 18px; height: 18px;" src="<?php echo base_url('/assets/img/icon/Invoice.png') ?>"><?php echo getWording('profile', 'quotation') ?></a></li>
                        <li><a href="<?php echo base_url('Account/sending_address') ?>"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php echo base_url('/assets/img/icon/delivery-truck.png') ?>"><?php echo getWording('profile', 'shipping_address') ?></a></li>
                        <li><a href="<?php echo base_url('Account/tax_invoice_address') ?>"><img class="mr-3" style="width: 16px; height: 16px;" src="<?php echo base_url('/assets/img/icon/location-2.png') ?>"><?php echo getWording('profile', 'tax_invoice') ?></a></li>
                        <!-- <li><a href="<?php echo base_url('Account/payment') ?>"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php echo base_url('/assets/img/icon/credit-card.png') ?>"><?php echo getWording('profile', 'credit') ?></a></li> -->
                        <li><a href="<?php echo base_url('Account/datasheet') ?>"><img class="mr-3" style="width: 16px; height: 20px;" src="<?php echo base_url('/assets/img/icon/Datasheet.png') ?>"><?php echo getWording('profile', 'datasheet') ?></a></li>
                    </ul>
                </div>
                <div class="col-sm-10 col-xl-9">
                    <div class="row">
                        <div class="col-sm-12 col-xl-12 mt-3">
                            <h3 style="font-weight: bold"><?php echo getWording('profile', 'order_list') ?></h3>
                        </div>

                        <hr>
                        <div class="col-12">
                            <table class="table">
                                <thead class="thead-light">
                                    <tr class="table-active" style="text-align: center;">
                                        <!-- <th class="col-2"></th> -->
                                        <th class="col-4"><?php echo getWording('profile', 'product_list') ?></th>
                                        <th class="col-2"><?php echo getWording('product', 'quantity') ?></th>
                                        <th class="col-2"><?php echo getWording('index', 'total_price') ?></th>
                                        <th class="col-2"></th>
                                        <th class="col-2"></th>

                                    </tr>
                                </thead>
                                <tbody style="vertical-align: middle; border-bottom-width: 1px;">
                                    <?php foreach ($order_limit[0]->purchase_transaction_detail as $key => $order_item) { ?>

                                        <?php if (empty($order_product_history)) { ?>
                                            <?php echo null ?>
                                        <?php } else { ?>
                                            <?php foreach ($order_product_history as $key => $order_product) { ?>
                                                <?php if ($order_product->{'product::id'} == $order_item->product_id) { ?>
                                                    <tr>
                                                        <td>
                                                            <div class="row">
                                                                <div class="col-8  d-block pl-3" style="align-self: center;">
                                                                    <?php if (empty($order_product->{'product::images'})) { ?>
                                                                        <img class="w-75" src="<?php echo base_url('/assets/img/no-images.png') ?>" alt="">
                                                                    <?php } else { ?>
                                                                        <img class="w-75" src="<?php echo $order_product->{'product::images'}[0] ?>" alt="">
                                                                    <?php } ?>
                                                                </div>

                                                                <div class="col-4 d-block pl-3" style="align-self: center;">
                                                                    <p><?php echo getVariable($order_product, 'product::name')  ?></p>
                                                                    <p><?php echo getVariable($order_product, 'product::model_name')  ?></p>
                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td style="text-align: center; ;">
                                                            <p style="height:100%"><?php echo $order_item->qty ?></p>
                                                        </td>
                                                        <td style="text-align: center; ">
                                                            <p style=""><?php echo $order_item->total_price ?></p>
                                                        </td>
                                                        <td style="text-align: center; ">
                                                            <a href="<?php echo base_url('/Account/review/' . $order_limit[0]->id) ?>"><button type="button" class="btn btn-secondary align-self-center button1" style="width: 120px; height: 40px; padding-top: 8px;border-radius: 25px;"><?php echo getWording('profile', 'review') ?> </button>
                                                        </td>
                                                        <td style="text-align: center; ">
                                                            <a href="#">
                                                                <p style=""><?php echo getWording('profile', 'order_again') ?></p>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>

                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6 col-xl-5" style="text-align: left;">
                            <div class="col-12">
                                <b>
                                    <p><?php echo getWording('profile', 'order_information') ?></p>
                                </b>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-6">
                                            <div><?php echo getWording('profile', 'receive_date') ?></div>
                                            <div><?php echo getWording('profile', 'transaction_date') ?></div>
                                            <div><?php echo getWording('profile', 'payment_method') ?></div>
                                            <div><?php echo getWording('profile', 'order_status') ?></div>

                                        </div>
                                        <div class="col-6" style="text-align: right;">
                                            <div>-</div>
                                            <div><?php echo $order_limit[0]->created_date ?></div>
                                            <div>
                                                <?php if ($order_limit[0]->payment_id == 1) { ?>
                                                    <div><?php echo getWording('profile', 'credit') ?></div>
                                                <?php } elseif ($order_limit[0]->payment_id == 2) { ?>
                                                    <div>Internet Banking</div>
                                                <?php } else { ?>
                                                    <div>PromtPay</div>
                                                <?php } ?>
                                            </div>
                                            <div><?php echo $order_limit[0]->status ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="col-12 mt-3">
                                <b>
                                    <p><?php echo getWording('profile', 'delivery_address') ?></p>
                                </b>
                                <div>
                                    <?php if (!empty($address_send)) { ?>
                                        <span><b><?php echo $address_send[0]->first_name ?> <?php echo $address_send[0]->last_name ?></b></span><br>
                                        <span>
                                            <?php echo $address_send[0]->address ?>
                                            <?php echo $address_send[0]->sub_district->name ?>
                                            <?php echo $address_send[0]->district->name ?>
                                            <?php echo $address_send[0]->province->name ?>
                                            <?php echo $address_send[0]->sub_district->code ?>
                                        </span>
                                    <?php } else { ?>
                                        <span> - </span>
                                    <?php } ?>
                                </div>

                            </div>
                            <div class="col-12 mt-3">
                                <b>
                                    <p><?php echo getWording('profile', 'receipt') ?></p>
                                </b>
                                <div>
                                    <?php if (!empty($address_tax)) { ?>
                                        <span><b><?php echo $address_tax[0]->first_name ?> <?php echo $address_tax[0]->last_name ?></b></span><br>
                                        <span>
                                            <?php echo $address_tax[0]->address ?>
                                            <?php echo $address_tax[0]->sub_district->name ?>
                                            <?php echo $address_tax[0]->district->name ?>
                                            <?php echo $address_tax[0]->province->name ?>
                                            <?php echo $address_tax[0]->sub_district->code ?>
                                        </span>
                                    <?php } else { ?>
                                        <span> - </span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1 col-lg-2 ">
                        </div>
                        <div class="col-sm-5 col-xl-5  pl-5 " style="text-align: left;">
                            <div class="news col-sm-12 col-xl-12 mt-3 pl-5 ">
                                <?php foreach ($order_limit as $key => $order_detail) { ?>

                                    <div class="row">
                                        <div class="col-6">
                                            <h5 style="font-weight: bold;"><?php echo getWording('profile', 'product_price') ?></h5>
                                            <h5 style="font-weight: bold;"><?php echo getWording('product', 'shipping') ?></h5>
                                            <h5 style="font-weight: bold;"><?php echo getWording('product', 'discount') ?></h5>
                                        </div>
                                        <div class="col-6" style="text-align: right;">

                                            <h5><?php echo $order_detail->product_price ?> <?php echo getWording('index', 'baht') ?></h5>

                                            <h5><?php echo $order_detail->delivery_price ?> <?php echo getWording('index', 'baht') ?></h5>

                                            <?php if ($order_detail->discount == null) { ?>
                                                <?php echo getWording('profile', 'discount') ?>
                                            <?php } else { ?>
                                                <h5>- <?php echo $order_detail->discount ?> <?php echo getWording('index', 'baht') ?></h5>
                                            <?php } ?>

                                        </div>
                                        <hr>
                                        <div class="col-6">
                                            <h5 style="font-weight: bold;"><?php echo getWording('profile', 'net_price') ?></h5>
                                        </div>
                                        <div class="col-6" style="text-align: right;">
                                            <b>
                                                <h5><?php echo $order_detail->net_price ?> <?php echo getWording('index', 'baht') ?></h5>
                                            </b>

                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="contact col-sm-12 col-xl-12 mt-3 pl-5 ">
                                <b class="pb-2"><?php echo getWording('profile', 'problem_order') ?></b>
                                <p><?php echo ($contact->telephone) ?></p>
                                <b><?php echo getWording('login', 'or') ?></b>
                                <div class="mt-2"><a href="<?php echo base_url('Contact') ?>"><button type="button" class="btn btn-secondary align-self-center"><?php echo getWording('index', 'contact') ?> </button></a> </div>
                            </div>
                        </div>
                        <div class="p-10 " style="text-align:center; font-weight: bold">
                            <a href="<?php echo base_url('Account/order_history') ?>" style="text-decoration: underline;"><?php echo getWording('profile', 'back') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
    <div class="mt-5"> <?php include('footer.php') ?></div>

    <script>
        const progress = document.getElementById("progress");
        const stepCircles = document.querySelectorAll(".circle");
        let currentActive = 1;
        update(2);

        function update(currentActive) {
            stepCircles.forEach((circle, i) => {
                if (i < currentActive) {
                    circle.classList.add("active");
                } else {
                    circle.classList.remove("active");
                }
            });

            const activeCircles = document.querySelectorAll(".active");
            progress.style.width =
                ((activeCircles.length - 1) / (stepCircles.length - 1)) * 100 + "%";
        }
    </script>
</body>

</html>