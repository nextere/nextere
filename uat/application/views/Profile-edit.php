<?php include('header.php') ?>

<style>
    .profile ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 100%;
        background-color: white;
        border: 1px solid #BCBCBC;
        opacity: 1;
        font-size: 16px;
    }

    .profile li a.active {
        background-color: #BCBCBC;
        color: white;
    }

    .profile li a {
        display: block;
        color: #000;
        padding: 8px 12px;
        text-decoration: none;
        font-size: 14px;
    }

    .password a {
        color: #000000;
    }

    input[type=text],
    select,
    textarea {
        padding: 12px;
        margin-top: 6px;
        margin-bottom: 16px;
        width: 100%;
        height: 40px;
        border: 1px solid #DBDBDB;
        border-radius: 5px;
    }

    input[type=tel],
    select,
    textarea {
        padding: 12px;
        margin-top: 6px;
        margin-bottom: 16px;
        width: 100%;
        height: 40px;
        border: 1px solid #DBDBDB;
        border-radius: 5px;
    }
</style>

<body>
    <section class="profile">
        <div class="container mt-5">
            <div class="row">
                <div class="col-sm-12 col-xl-12 pt-5 pb-5 pl-5 mt-5" style="background-color: lightgray; text-align:left">
                    <h4><b><?php echo getWording('profile', 'my_account') ?></b></h4>
                    <p><?php echo getWording('profile', 'hi') ?> <?php echo $user_edit->fullname ?>, <?php echo getWording('profile', 'detail') ?></p>
                </div>
                <div class="col-sm-4 col-xl-3 mt-3 pl-0">
                    <ul>
                        <li><a class="active" href="<?php echo base_url('Account/profile') ?>"> <img class="mr-3" style="width: 16px; height: 16px;" src="<?php echo base_url('/assets/img/icon/user-White.png') ?>"><?php echo getWording('profile', 'profile') ?></a></li>
                        <li><a href="<?php echo base_url('Account/order_history') ?>"><img class="mr-3" style="width: 20px; height: 18px;" src="<?php echo base_url('/assets/img/icon/shopping-cart.png') ?>"><?php echo getWording('profile', 'my_orders') ?></a></li>
                        <li><a href="<?php echo base_url('Account/quotation_request_history') ?>"><img class="mr-3" style="width: 18px; height: 18px;" src="<?php echo base_url('/assets/img/icon/Invoice.png') ?>"><?php echo getWording('profile', 'quotation') ?></a></li>
                        <li><a href="<?php echo base_url('Account/sending_address') ?>"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php echo base_url('/assets/img/icon/delivery-truck.png') ?>"><?php echo getWording('profile', 'shipping_address') ?></a></li>
                        <li><a href="<?php echo base_url('Account/tax_invoice_address') ?>"><img class="mr-3" style="width: 16px; height: 16px;" src="<?php echo base_url('/assets/img/icon/location-2.png') ?>"><?php echo getWording('profile', 'tax_invoice') ?></a></li>
                        <!-- <li><a href="<?php echo base_url('Account/payment') ?>"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php echo base_url('/assets/img/icon/credit-card.png') ?>"><?php echo getWording('profile', 'credit') ?></a></li> -->
                        <li><a href="<?php echo base_url('Account/datasheet') ?>"><img class="mr-3" style="width: 16px; height: 20px;" src="<?php echo base_url('/assets/img/icon/Datasheet.png') ?>"><?php echo getWording('profile', 'datasheet') ?></a></li>
                    </ul>
                </div>
                <div class="col-sm-8 col-xl-9 mt-3">
                    <div class="head">
                        <h3 style="font-weight: bold"><?php echo getWording('profile', 'profile') ?></h3>
                    </div>
                    <hr style="border-width: 3px;">

                    <form id="dataForm">
                        <div class="col-sm-12 col-xl-12" style="border:solid #DBDBDB; width: auto; height: auto;">
                            <div class="row p-5" style="padding:10px">

                                <div class="col-sm-6 col-xl-6">
                                    <div class="col-xl-10 name pt-5">
                                        <b><?php echo getWording('profile', 'name') ?> *</b>
                                        <input type="text" id="name" name="name" value="<?php
                                                                                        $name = array();
                                                                                        $name = explode(" ", $user_edit->fullname);
                                                                                        echo $name[0];
                                                                                        ?>" placeholder="ชื่อ">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xl-6">
                                    <div class="col-xl-10 lastname pt-5">
                                        <b><?php echo getWording('profile', 'lastname') ?> *</b>
                                        <input type="text" id="lastname" name="lastname" value="<?php
                                                                                                if (count($name) >= 2) {
                                                                                                    echo $name[1];
                                                                                                } else {
                                                                                                    echo '-';
                                                                                                }
                                                                                                ?>" placeholder="นามสกุล">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xl-6 pt-5">
                                    <div class="gender col-xl-10 pt-2">
                                        <b><?php echo getWording('profile', 'gender') ?></b>
                                        <!-- <input type="text" id="gender" name="gender" value="<?php // echo $user_edit->gender 
                                                                                                    ?>" placeholder="เพศ"> -->
                                        <div class="pt-5">
                                            <?php if ($user_edit->gender == 'male') { ?>
                                                <input class="mr-2" type="radio" name="gender" value="male" id="male" checked><label for="male"><?php echo getWording('login', 'male') ?></label>
                                                  <input class="mr-2" type="radio" name="gender" value="female" id="female"><label for="female"><?php echo getWording('login', 'female') ?></label>
                                                  <input class="mr-2" type="radio" name="gender" value="LGBTQ" id="LGBTQ"><label for="LGBTQ">LGBTQ</label>
                                            <?php } else if ($user_edit->gender == 'female') { ?>
                                                <input class="mr-2" type="radio" name="gender" value="male" id="male"><label for="male"><?php echo getWording('login', 'male') ?></label>
                                                  <input class="mr-2" type="radio" name="gender" value="female" id="female" checked><label for="female"><?php echo getWording('login', 'female') ?></label>
                                                  <input class="mr-2" type="radio" name="gender" value="LGBTQ" id="LGBTQ"><label for="LGBTQ">LGBTQ</label>
                                            <?php } else if ($user_edit->gender == 'LGBTQ') { ?>
                                                <input class="mr-2" type="radio" name="gender" value="male" id="male"><label for="male"><?php echo getWording('login', 'male') ?></label>
                                                  <input class="mr-2" type="radio" name="gender" value="female" id="female"><label for="female"><?php echo getWording('login', 'female') ?></label>
                                                  <input class="mr-2" type="radio" name="gender" value="LGBTQ" id="LGBTQ" checked><label for="LGBTQ">LGBTQ</label>
                                            <?php } else { ?>
                                                <input class="mr-2" type="radio" name="gender" value="male" id="male"><label for="male"><?php echo getWording('login', 'male') ?></label>
                                                  <input class="mr-2" type="radio" name="gender" value="female" id="female"><label for="female"><?php echo getWording('login', 'female') ?></label>
                                                  <input class="mr-2" type="radio" name="gender" value="LGBTQ" id="LGBTQ"><label for="LGBTQ">LGBTQ</label>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xl-6 pt-5">
                                    <div class="birthday col-xl-10 pt-2">
                                        <b><?php echo getWording('profile', 'birthdate') ?></b>
                                        <input type="date" id="birthday" name="birthday" style="width: 100%; height:40px; padding: 12px; margin-top: 6px;" value="<?php echo date("Y-m-d", strtotime($user_edit->birth_date)) ?>">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xl-6 pt-5">
                                    <div class="tax col-xl-10 pt-2">
                                        <b><?php echo getWording('profile', 'tax') ?></b>
                                        <input type="text" id="tax" name="tax" value="<?php echo $user_edit->tax_id ?>" placeholder="เลขประจำตัวผู้เสียภาษี">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xl-6 pt-5">
                                    <div class="telephone col-xl-10 pt-2">
                                        <b><?php echo getWording('profile', 'phone') ?></b>
                                        <input type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" id="telephone" name="telephone" value="<?php echo $user_edit->telephone ?>">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xl-6 pt-5">
                                    <div class="email col-xl-10 pt-2">
                                        <b><?php echo getWording('profile', 'email') ?></b>
                                        <input type="text" id="email" name="email" value="<?php echo $user_edit->email ?>" placeholder="อีเมล">
                                    </div>
                                    <div class="pt-5">
                                        <!-- <label class="form-check-label"><input type="checkbox"> <?php echo getWording('profile', 'follow') ?></label> -->
                                        <div class="pt-1">
                                            <?php if (($user_edit->follow) == 'ACTIVATE') { ?>
                                                <input type="checkbox" name="is_default" id="is_default" value="1" checked="true"> <?php echo getWording('profile', 'follow') ?>
                                            <?php } else { ?>
                                                <input type="checkbox" name="is_default" id="is_default" value="0"> <?php echo getWording('profile', 'follow') ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <input style="display: none;" type="text" id="token_facebook" name="token_facebook" value="<?php echo $user_edit->token_facebook ?>">
                                    <input style="display: none;" type="text" id="token_google" name="token_google" value="<?php echo $user_edit->token_google ?>">
                                    <input style="display: none;" type="text" id="token_line" name="token_line" value="<?php echo $user_edit->token_line ?>">

                                </div>
                                <div class="col-sm-6 col-xl-6 pt-5">
                                    <div class="email col-xl-10 pt-2">
                                        <b><?php echo getWording('profile', 'password') ?></b>

                                        <input type="password" id="password" name="password" value="123456789" disabled style="width: 100%; height:40px;">
                                        <!-- <div style="width: 100%; height:40px; border:solid 1px">************</div> -->
                                    </div>
                                    <div class="password col-sm-6 col-xl-5 pt-5">
                                        <!-- <a href="#">ส่งคำขอเปลี่ยนรหัสผ่าน</a> -->
                                        <button data-bs-toggle="modal" data-bs-target="#myModal1" style="border: 0px; background-color:#FFFFFF">
                                            <?php echo getWording('profile', 'change_password') ?>
                                        </button>
                                    </div>
                                </div>

                            </div>
                            <div class="button4 pt-1" style="text-align: center;">
                                <div class="submit pt-4" style="text-align: center; padding-bottom: 20px;">
                                    <input id="submit-profile" type="submit" value="<?php echo getWording('profile', 'save') ?>" style="width: 208px; height:45px; border:solid 1px; border-radius: 23px; padding: 9px; margin:10px;background-color:#959595; color:#FFFFFF;">
                                </div>
                            </div>

                        </div>

                    </form>
                </div>
            </div>

        </div>

        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content modal-content-style">
                    <div class="modal-header modal-header-style">

                    </div>
                    <div class="modal-body">
                        <div class="btn-close-modal">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <h3 class="edit_response" style="text-align: center;"></h3>
                    </div>
                    <div class="modal-footer modal-footer-style edit_response_btn">
                        <button type="button" class="btn  register_success d-none" style="width: 142px;height: 40px; background-color: #AAAAAA; border-radius: 23px; padding: 10px; margin:10px;color:white;" onclick="location.href='<?php echo base_url() ?>'">Understood</button>
                        <button type="button" class="btn register_fail d-none" style="width: 142px;height: 40px; background-color: #AAAAAA; border-radius: 23px; padding: 10px; margin:10px;color:white;" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>



        <div class="modal fade" id="myModal1" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h5 class="modal-title" style="padding-left:20px"><?php echo getWording('profile', 'change_password') ?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <form id="change_password">
                                <div class="row">
                                    <div class="col-sm-12 col-xl-2"></div>
                                    <div class="col-sm-12 col-xl-8" style="text-align:left;"><b><?php echo getWording('profile', 'old_password') ?>*</b>
                                        <div class="old_password pt-2">
                                            <input type="password" id="old_password" name="old_password" placeholder="<?php echo getWording('profile', 'old_password') ?>" required style="width: 100%; height: 45px;
                        border: 1px solid #707070; border-radius: 5px;padding-left:15px ">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-xl-2"></div>

                                    <div class="col-sm-12 col-xl-2"></div>
                                    <div class="col-sm-12 col-xl-8 pt-4" style="text-align:left;"><b><?php echo getWording('profile', 'new_password') ?>*</b>
                                        <div class="new_password pt-2">
                                            <input type="password" id="new_password" name="new_password" placeholder="<?php echo getWording('profile', 'new_password') ?>" required style="width: 100%; height: 45px;
                        border: 1px solid #707070; border-radius: 5px;padding-left:15px">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-xl-2"></div>

                                    <div class="col-sm-12 col-xl-2"></div>
                                    <div class="col-sm-12 col-xl-8 pt-4" style="text-align:left;"><b><?php echo getWording('profile', 'confirm_password') ?>*</b>
                                        <div class="new_password pt-2">
                                            <input type="password" id="confirm_new_password" name="confirm_new_password" placeholder="<?php echo getWording('profile', 'confirm_password') ?>" required style="width: 100%; height: 45px;
                        border: 1px solid #707070; border-radius: 5px;padding-left:15px">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-xl-2"></div>
                                </div>
                                <div class="button4 pt-1" style="text-align: center;">
                                    <div class="submit pt-4" style="text-align: center; padding-bottom: 20px;">
                                        <input id="submit-change_password" type="submit" value="บันทึก" style="width: 208px; height:45px; border:solid 1px; border-radius: 23px; padding: 9px; margin:10px;background-color:#959595; color:#FFFFFF;">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(document).ready(function() {
                $('#telephone').mask('000-000-0000');
            });

            $("#dataForm").submit(function(e) {
                e.preventDefault();
                return false;
            });

            $('#is_default').change(function(e) {
                if (this.checked) {
                    $('#is_default').prop('checked', true)
                    $('#is_default').val('ACTIVATE');
                } else {
                    $('#is_default').prop('checked', false)
                    $('#is_default').val('NULL');
                }
            });

            $("#submit-profile").click(function() {

                $("#dataForm").validate({
                    rules: {
                        name: "required",
                        lastname: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        telephone: {
                            required: true,
                            pattern: '\\d{3}-\\d{3}-\\d{4}',
                            minlength: 12,
                            maxlength: 12
                        },
                        tax: "required"
                    },
                    messages: {
                        name: "Please enter your name",
                        lastname: "Please enter your lastname",
                        email: "Please enter a valid email address",
                        telephone: "Please correct telephone format",
                        tax: "Please enter your tax ID"
                    },

                    errorPlacement: function(error, element) {
                        error.insertBefore(element);
                    },
                    submitHandler: function() {
                        var fullname = $("#name").val() + " " + $("#lastname").val();

                        var data = {
                            "fullname": fullname,
                            "email": $("#email").val(),
                            "telephone": $("#telephone").val(),
                            "gender": $('input[name=gender]:checked', '#dataForm').val(),
                            "birthday": $("#birthday").val(),
                            "tax": $("#tax").val(),
                            "token_facebook": $("#token_facebook").val(),
                            "token_google": $("#token_google").val(),
                            "token_line": $("#token_line").val(),
                            "follow": $("#is_default").val(),
                        }

                        var send_data = {
                            "url": "<?php echo base_url('Account/profile_edit') ?>",
                            "method": "POST",
                            "data": data
                        }

                        $.ajax(send_data).done(function(response) {
                            if (response.code == "0x0000-00000") {
                                window.location.href = "<?php echo base_url('Account/profile') ?>";
                                $('.edit_response').text('Edit success');
                            } else {
                                $('.edit_success').addClass('d-none');
                                // $('#staticBackdrop').modal('show');
                                $('.edit_success').removeClass('d-block');
                                $('.edit_fail').addClass('d-block');
                                $('.edit_fail').removeClass('d-none');
                            }
                            $('#staticBackdrop').modal('show');
                        });
                    }
                });
            });

            $("#change_password").submit(function(e) {
                e.preventDefault();
                return false;
            });

            $("#submit-change_password").click(function() {

                $("#change_password").validate({
                    rules: {
                        old_password: {
                            required: true,
                            minlength: 5
                        },
                        new_password: {
                            required: true,
                            minlength: 5
                        },
                        confirm_new_password: {
                            required: true,
                            minlength: 5,
                            equalTo: "#new_password"
                        },
                    },
                    messages: {
                        old_password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long"
                        },
                        new_password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long"
                        },
                        confirm_new_password: {
                            required: "Please provide a password",
                            minlength: "Your password must be at least 5 characters long",
                            equalTo: "Please enter the same password as above"
                        },
                    },

                    errorPlacement: function(error, element) {
                        error.insertBefore(element);
                    },
                    submitHandler: function() {

                        var data = {
                            "old_password": $("#old_password").val(),
                            "confirm_new_password": $("#confirm_new_password").val(),
                        }

                        var send_data = {
                            "url": "<?php echo base_url('Account/change_password') ?>",
                            "method": "POST",
                            "data": data
                        }

                        $.ajax(send_data).done(function(response) {
                            if (response.code == "0x0000-00000") {
                                window.location.href = "<?php echo base_url('Account/profile_edit') ?>";
                                $('.edit_success').addClass('d-none');
                                $('.edit_success').removeClass('d-block');
                                $('.edit_fail').addClass('d-block');
                                $('.edit_fail').removeClass('d-none');
                                $(".edit_response").text(response.message);
                            } else if (response.code == "1x0000-00000") {
                                $('.edit_success').addClass('d-none');
                                $('.edit_success').removeClass('d-block');
                                $('.edit_fail').addClass('d-block');
                                $('.edit_fail').removeClass('d-none');
                                $(".edit_response").text(response.message);
                            }
                            $('#staticBackdrop').modal('show');
                        });
                    }
                });
            });
        </script>

    </section>
    <div class="mt-5">
        <?php include('footer.php') ?>
    </div>
</body>