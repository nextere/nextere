<?php
$codeigniter_instance = &get_instance();
$contactus_list = file_get_contents('http://nextere.space/api_area/home/contactus');
$contactus_list = json_decode($contactus_list);
$contactus_list =  $contactus_list->data->contactus;

$menu = file_get_contents('http://www.nextere.space/api_area/home/menu2');
$menu = json_decode($menu);
$menu = $menu->data->menu_categories;
function comparator_5($a, $b)
{
    return $a->order < $b->order ? -1 : 1;
};
usort($menu, "comparator_5");
?>

<style>
    .footer ul {
        -webkit-column-count: 2;
        -webkit-column-gap: 50%;
        column-count: 2;
        column-gap: 20px;
        list-style-type: none;
        margin: 0;
        padding: 0;
    }

    .footer .nav-link {
        display: block;
        padding: .5rem 1rem;
        color: lightgray;
        text-decoration: none;
        transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out;
        margin-bottom: 10px;
    }

    .footer a {
        color: #ffffff;
        text-decoration: none;
    }
</style>
</div>
<div class="footer">
    <div class="container">
        <div class="row pt-4">
            <div class="col-sm-6 col-lg-4 mt-2">
                <div><a class="navbar-brand" href="<?php echo base_url() ?>"><img class="w-20" src="<?php echo base_url('/assets/img/logo.png') ?>" alt=""></a></div>
                <p class="pt-3 w-75">
                    <?php echo getVariable($contactus_list, 'address') ?>
                </p>
                <div class="sociallist">
                    <a class="social facebook-icon" href="<?php echo ($contactus_list->facebook) ?>">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a class="social instagram-icon" href="<?php echo ($contactus_list->instagram) ?>">
                        <i class="fab fa-instagram"></i>
                    </a>
                    <a class="social line-icon" href="<?php echo ($contactus_list->line) ?>">
                        <img class="w-50" src="<?php echo base_url('/assets/img/icon/line.png') ?>">
                        <!-- <i class="fab fa-line"></i> -->

                    </a>
                </div>

            </div>

            <div class="col-sm-6 col-lg-3 mt-5">
                <div style="margin-bottom: 8px;"><b> <?php echo getWording('product', 'product') ?></b></div>
                <div>
                    <ul class="p-0">
                        <?php foreach ($menu as $key => $solution) { ?>

                            <li>
                                <?php if (!empty($solution)) { ?>
                                    <a class="nav-link p-0 " href="<?php echo base_url('Solution/detail/' . $solution->id . '?type=solution') ?>">
                                        <?php echo getVariable($solution, 'name') ?>
                                    </a>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="col-lg-1"></div>

            <div class="col-sm-6 col-lg-3 mt-5">

                <a href="<?php echo base_url('/Article') ?>">
                    <p style="margin-bottom: 8px;"><?php echo getWording('index', 'article') ?></p>
                </a>
                <a href="<?php echo base_url('/How_to') ?>">
                    <p style="margin-bottom: 8px;"><?php echo getWording('index', 'order') ?></p>
                </a>
                <a href="<?php echo base_url('/Contact') ?>">
                    <p style="margin-bottom: 8px;"><?php echo getWording('index', 'contact_us') ?></p>
                </a>
                <a href="<?php echo base_url('/About') ?>">
                    <p style="margin-bottom: 8px;"><?php echo getWording('index', 'about_us') ?></p>
                </a>
                <a href="<?php echo base_url('/How_to') ?>">
                    <p><?php echo getWording('index', 'help') ?></p>
                </a>
            </div>
            <div class="col-sm-12 mt-5" style="text-align: center;">
                <p>COPYRIGHT©2021 NEXTERE COMPANY LIMITED</p>

            </div>

        </div>
    </div>

</div>

<div class="collapse-bg d-none" id="collapse-bg" onclick="closeSol()"></div>


<!-- Modal -->
<div class="modal fade bd-example-modal-md" id='alert_full_compring' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-content-style">
            <div class="modal-header modal-header-style">
            </div>
            <div class="modal-body" style="text-align: -webkit-center;">
                <h2 class="compare_text text-center my-5"></h2>
                <button class="btn" type="button" data-bs-dismiss="modal" aria-label="Close" style="width: 142px;height: 40px; background-color: #AAAAAA; border-radius: 23px; padding: 10px; margin:10px;color:white;"><?php echo getWording('index', 'ok') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="compare_product_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table compare_product">
                                <thead>
                                    <tr class="compare_product_detail">
                                        <th scope="col">

                                        </th>

                                        <th scope="col">
                                            <div class="compare_title product_1">
                                                <!-- <img class="w-100" id="compare_img_1" src="" alt="">
                                                <span id="compare_category_1"></span>
                                                <p id="compare_title_1"></p>
                                                <a class="delete_compare_product" data-product_id="" href="" data-delete="1">ลบ <i class="far fa-trash-alt"></i></a> -->
                                            </div>
                                        </th>
                                        <th scope="col">
                                            <div class="compare_title product_2">
                                                <!-- <img class="w-100" id="compare_img_2" src="" alt="">
                                                <span id="compare_category_2"></span>
                                                <p id="compare_title_2"></p>
                                                <a class="delete_compare_product" href="" data-product_id="" data-delete="2">ลบ <i class="far fa-trash-alt"></i></a> -->
                                            </div>
                                        </th>
                                        <th scope="col">
                                            <div class="compare_title product_3">
                                                <!-- <img class="w-100" id="compare_img_3" src="" alt="">
                                                <span id="compare_category_3"></span>
                                                <p id="compare_title_3"></p>
                                                <a class="delete_compare_product" href="" data-product_id="" data-delete="3">ลบ <i class="far fa-trash-alt"></i></a> -->
                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="border-0">
                                    <tr class=" compare_price border-0">
                                        <th scope="row border-0"><?php echo getWording('product', 'price') ?></th>
                                        <td class="product_1 col-3 border-0"></td>
                                        <td class="product_2 col-3 border-0"></td>
                                        <td class="product_3 col-3 border-0"></td>
                                    </tr>
                                    <tr class="compart_table_bg compare_price_pro border-0">
                                        <th scope="row border-0"><?php echo getWording('product', 'promotion_price') ?></th>
                                        <td class="product_1 col-3 border-0"></td>
                                        <td class="product_2 col-3 border-0"></td>
                                        <td class="product_3 col-3 border-0"></td>
                                    </tr>
                                    <tr class="compare_model border-0">
                                        <th scope="row border-0"><?php echo getWording('product', 'model') ?></th>
                                        <td class="product_1 border-0"></td>
                                        <td class="product_2 border-0"></td>
                                        <td class="product_3 border-0"></td>
                                    </tr>
                                </tbody>
                                <tbody id="technical_spec">

                                </tbody>
                                <tbody>
                                    <tr class="compart_table_bg compare_insurance">
                                        <th scope="row"><?php echo getWording('product', 'product_warranty') ?></th>
                                        <td class="product_1"></td>
                                        <td class="product_2"></td>
                                        <td class="product_3"></td>
                                    </tr>
                                    <tr class="compare_addcart">
                                        <th scope="row"></th>
                                        <td class="product_buy_1">
                                            <?php if ($token != '') { ?>
                                                <button class="order_btn btn" data-quantity="1" data-product-id="" disabled><i class="fas fa-shopping-cart"></i> <?php echo getWording('product', 'place_order') ?></button>
                                            <?php } else { ?>
                                                <a href="<?php echo base_url('Account/login') ?>"> <button class="btn" disabled><i class="fas fa-shopping-cart"></i> <?php echo getWording('product', 'place_order') ?></button></a>
                                            <?php } ?>
                                        </td>
                                        <td class="product_buy_2">
                                            <?php if ($token != '') { ?>
                                                <button class="order_btn btn" data-quantity="1" data-product-id="" disabled><i class="fas fa-shopping-cart"></i> <?php echo getWording('product', 'place_order') ?></button>
                                            <?php } else { ?>
                                                <a href="<?php echo base_url('Account/login') ?>"> <button class="btn" disabled><i class="fas fa-shopping-cart"></i> <?php echo getWording('product', 'place_order') ?></button></a>
                                            <?php } ?>
                                        </td>
                                        <td class="product_buy_3">
                                            <?php if ($token != '') { ?>
                                                <button class="order_btn btn" data-quantity="1" data-product-id="" disabled><i class="fas fa-shopping-cart"></i> <?php echo getWording('product', 'place_order') ?></button>
                                            <?php } else { ?>
                                                <a href="<?php echo base_url('Account/login') ?>"> <button class="btn" disabled><i class="fas fa-shopping-cart"></i> <?php echo getWording('product', 'place_order') ?></button></a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('Livechat.php') ?>
<script src="<?php echo base_url('assets/js/vendor/validator.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendor/jquery-migrate-1.2.1.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendor/bootstrap-datepicker.js') ?>"></script>
<script src="<?php echo base_url('assets/js/vendor/bootstrap.bundle.min.js') ?>"></script>
<script src="<?php echo base_url('assets/vender/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/vendor/fontawesome.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/vendor/slick.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/assets/js/vendor/dropzone.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/vendor/video.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/vendor/jquery.mask.min.js') ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/js.geotools/dist/geotools.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/js.geotools/dist/geotools-swedish-zipcodes.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/moment@2.24.0/moment.min.js"></script>
<script src="<?php echo base_url('assets/js/fancyTable.js') ?>"></script>
<script src="<?php echo base_url('assets/js/cookie.js') ?>"></script>



<svg aria-hidden="true" style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <defs>
        <symbol id="icon-filter" width="100%" viewBox="0 0 25 25">
            <g>
                <path d="M1.75,7.75h6.6803589c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891   S13.9000854,9.0452271,14.2356567,7.75H24.25C24.6640625,7.75,25,7.4140625,25,7s-0.3359375-0.75-0.75-0.75H14.2356567   c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891S8.7659302,4.9547729,8.4303589,6.25H1.75   C1.3359375,6.25,1,6.5859375,1,7S1.3359375,7.75,1.75,7.75z M11.3330078,5.4912109   c0.8320313,0,1.5087891,0.6767578,1.5087891,1.5087891s-0.6767578,1.5087891-1.5087891,1.5087891S9.8242188,7.8320313,9.8242188,7   S10.5009766,5.4912109,11.3330078,5.4912109z" fill="#1D1D1B" />
                <path d="M24.25,12.25h-1.6061401c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891   S17.1741333,10.9547729,16.838562,12.25H1.75C1.3359375,12.25,1,12.5859375,1,13s0.3359375,0.75,0.75,0.75h15.088562   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891s2.5670776-0.963562,2.9026489-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,12.25,24.25,12.25z M19.7412109,14.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891S21.25,12.1679688,21.25,13   S20.5732422,14.5087891,19.7412109,14.5087891z" fill="#1D1D1B" />
                <path d="M24.25,18.25H9.7181396c-0.3355103-1.2952271-1.5037842-2.2587891-2.9017334-2.2587891   c-1.3987427,0-2.5670776,0.963562-2.9026489,2.2587891H1.75C1.3359375,18.25,1,18.5859375,1,19s0.3359375,0.75,0.75,0.75h2.1637573   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891c1.3979492,0,2.5662231-0.963562,2.9017334-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,18.25,24.25,18.25z M6.8164063,20.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891   c0.8310547,0,1.5078125,0.6767578,1.5078125,1.5087891S7.6474609,20.5087891,6.8164063,20.5087891z" fill="#1D1D1B" />
            </g>
        </symbol>
    </defs>
</svg>
<!-- Modal -->
<div class="modal fade" id="add-success" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-content-style">
            <div class="modal-header modal-header-style">
            </div>
            <div class="modal-body">
                <div class="btn-close-modal">
                    <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
                </div>
                <h3 style="text-align: center;"><?php echo getWording('product', 'add_product') ?></h3>
            </div>
            <div class="modal-footer modal-footer-style register_response_btn">

            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="product-outstock" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-content-style">
            <div class="modal-header modal-header-style">
            </div>
            <div class="modal-body" style="text-align: -webkit-center;">
                <div class="btn-close-modal">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <h3 style="text-align: center;"><?php echo getWording('product', 'out_of_stock') ?></h3>
                <button class="btn" type="button" data-bs-dismiss="modal" aria-label="Close" style="width: 142px;height: 40px; background-color: #AAAAAA; border-radius: 23px; padding: 10px; margin:10px;color:white;"><?php echo getWording('index', 'ok') ?></button>
            </div>
            <div class="modal-footer modal-footer-style register_response_btn">

            </div>
        </div>
    </div>
</div>


<!-- </body> -->

</html>

<script>
    // $('.filter_btn').tooltip('show');

    $(function() {
        $(document).tooltip();
    });

    function formatNumber(num) {
        return Number(num).toLocaleString(undefined, {
            minimumFractionDigits: 0,
            maximumFractionDigits: 0
        });
        // return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }
    var cart_item = parseFloat($('.cart_count').data('cart-items'));
    console.log(cart_item);
    $(document).ready(function() {

        $('.secd-menu .nav-item.dropdown').hover(function() {
            $('.sub_solution_header_thumbnail').stop();
            $('.childsub_solution_header_thumbnail').stop();
            $('.sub_solution_header_thumbnail').hide();
            $('.childsub_solution_header_thumbnail').hide();
            $('.solution_header_thumbnail').fadeIn(1300);
        });

        $('.solution-hover').hover(function() {
            $('.sub_solution_header_thumbnail').stop();
            $('.childsub_solution_header_thumbnail').stop();
            $('.solution_header_thumbnail').hide();
            $('.sub_solution_header_thumbnail').hide();
            $('.childsub_solution_header_thumbnail').hide();
            $('.sub_solution_header_thumbnail[data-thumbnail-traget=' + $(this).data('hover_thumbnail') + ']').fadeIn(1300);
        });

        $('.subsolution-hover').hover(function() {
            $('.sub_solution_header_thumbnail').stop();
            $('.childsub_solution_header_thumbnail').stop();
            $('.solution_header_thumbnail').hide();
            $('.sub_solution_header_thumbnail').hide();
            $('.childsub_solution_header_thumbnail').hide();
            $('.childsub_solution_header_thumbnail[data-thumbnail-traget=' + $(this).data('hover_thumbnail') + ']').fadeIn(1300);
        });

        $('.category-hover').hover(function() {
            $('.category_sub_thumbnail_hover').stop();
            $('.category_sub_thumbnail_hover').hide();
            $('.category_thumbnail_hover').stop();
            $('.category_thumbnail_hover').hide();
            $('.category_thumbnail_hover[data-thumbnail-traget=' + $(this).data('hover_thumbnail') + ']').fadeIn(1300);
        });
        $('.sub-category-hover').hover(function() {
            $('.category_thumbnail_hover').stop();
            $('.category_thumbnail_hover').hide();
            $('.category_sub_thumbnail_hover').stop();
            $('.category_sub_thumbnail_hover').hide();
            $('.category_sub_thumbnail_hover[data-thumbnail-traget=' + $(this).data('hover_thumbnail') + ']').fadeIn(1300);
        });

        // $(".secd-menu .nav-item.dropdown").mouseout(function() {
        //     console.log('mouse out!!!');
        //     $('.solution_header_thumbnail').hide();
        //     $('.sub_solution_header_thumbnail').hide();
        //     $('.childsub_solution_header_thumbnail').hide();
        // });

        // $(".subsolution-hover").mouseout(function() {
        //     $('.solution_header_thumbnail').fadeIn(1300);
        //     $('.sub_solution_header_thumbnail').hide();
        //     $('.childsub_solution_header_thumbnail').hide();
        // });
        // console.log(compare_list);
        // console.log(sessionStorage.getItem("compare_product"));
        var base_url = '<?php echo base_url() ?>';
        var compare_product_word = '<?php echo getWording('index', 'compare_product') ?>';
        var compare_limit_word = '<?php echo getWording('index', 'compare_limit') ?>';
        var compare_list_word = '<?php echo getWording('index', 'compare_list') ?>';
        if (sessionStorage.getItem("compare_product") != null) {
            // console.log(sessionStorage.getItem("compare_product"));
            // console.log('has compare!!');
            session_compare = sessionStorage.getItem("compare_product").split(",");
            var compare_list = session_compare;
            $("#compare").slideDown(500);
            $('.compare_amount').text(compare_product_word + ' (' + session_compare.length + '/3)');
        } else {
            var compare_list = [];
        }

        $(".profile_dropdown").click(function() {
            $(".dropdown-menu-profile").toggleClass("active");
        });

        crt_total();

        var member_id = '<?php echo $codeigniter_instance->session->userdata('member::id') != null ? $codeigniter_instance->session->userdata('member::id') : ''; ?>';
        var token = '<?php echo $codeigniter_instance->session->userdata('member::token') != null ? $codeigniter_instance->session->userdata('member::token') : ''; ?>';

        $(".order_btn").click(function() {
            if (token != "") {
                $('#add-success').modal('show');
                setTimeout(function() {
                    $('#add-success').modal('hide');
                }, 1000);
            }
        });

        $('.order_btn').click(function() {
            var $this = $(this);
            $this.toggleClass('SeeMore');
            if ($this.hasClass('.order_btn')) {
                $this.text('See More');
            } else {
                $this.text('เพิ่มสินค้าลงตะกร้า');
            }
        });

        var sum = 0;
        $('.crt_total').each(function() {
            sum += parseFloat($(this).data('product-sum')); // Or this.innerHTML, this.innerText
            $("#total_cart_price").text(formatNumber(sum));
        });

        $(document).on("click", ".filter_btn ", function() {
            event.preventDefault();
            // console.log(compare_list);
            // console.log(compare_list.includes($(this).data('compare')));
            if (compare_list.toString().includes($(this).data('compare')) == false) {
                // console.log(compare_list);
                $("#compare").slideDown(500);
                if (compare_list.length < 3) {
                    compare_list.push($(this).data('compare'));
                    $('.compare_amount').text(compare_product_word + ' (' + compare_list.length + '/3)');
                    $("#compare").slideDown(500);
                } else {
                    $('.compare_text').text(compare_limit_word);
                    $('#alert_full_compring').modal('show');
                }

            } else {
                $('.compare_text').text(compare_list_word);
                $('#alert_full_compring').modal('show');
            }

            compare_list.sort();

            sessionStorage.setItem("compare_product", compare_list);

            var data_token = {
                "product1": compare_list[0],
                "product2": compare_list[1],
                "product3": compare_list[2]
            }

            // console.log(sessionStorage.getItem("compare_product"));
        });

        $(document).on("click", "#submit_compare", function() {
            event.preventDefault();
            var product_compare_list = sessionStorage.getItem("compare_product");

            var data_token = {
                "product_compare_list": product_compare_list
            }

            var delete_word = '<?php echo getWording('product', 'delete') ?>';

            var compare_product = {
                "url": "<?php echo base_url('Product/compare_product') ?>",
                "method": "POST",
                "data": data_token
            }

            $.ajax(compare_product).done(function(response) {
                $(".compare_title ").empty();
                response.data.products.forEach((element, index, array) => {
                    // console.log(element);
                    if (typeof element['images'] !== 'undefined') {
                        product_compare_img = element['images'][0];
                        // console.log('have image!!');
                    } else {
                        product_compare_img = base_url + '/assets/img/no-images.png';
                        // console.log('not have image!!');
                    }
                    // console.log(product_compare_img);
                    var search_discount = Object.keys(element);
                    // console.log(search_discount.indexOf("product::discount"));
                    if (index == 0) {
                        if (search_discount.indexOf("product::discount") >= 0) {
                            // console.log(element['product::discount'].total_price);
                            $('.compare_title.product_1').append(
                                '<img class="w-100 border-images mb-3" id="compare_img_1" src="' + product_compare_img + '" alt="">' +
                                '<span id="compare_category_1" style="color: #8B8B8B;"></span>' +
                                '<p id="compare_title_1">' + element['product::name_th'] + '</p>' +
                                '<a class="delete_compare_product" data-product_id="' + element['product::id'] + '" href="" data-delete="1">' + delete_word + ' <i class="far fa-trash-alt"></i></a>');
                            // console.log(element['product::id']);
                            // $('#compare_img_1').attr('src', product_compare_img);
                            // $('#compare_title_1').text(element['product::name_th']);
                            // $('.compare_title.product_1 .delete_compare_product').attr('data-product_id', element['product::id']);
                            $('.compare_price .product_1').text(formatNumber(element['product::retail_price']));
                            $('.compare_price_pro .product_1').text(formatNumbe(element['product::discount'].total_price));
                            $('.compare_model .product_1').text(element['product::model_name_th']);
                            $('.compare_insurance .product_1').text(element['product::condition_warranty_th']);
                            $('.compare_addcart .product_buy_1 button').data('product-id', element['product::id']);
                            $('.compare_addcart .product_buy_1 button').prop("disabled", false);
                            // console.log($('#compare_img_1').attr('src'));
                        } else {
                            // console.log(element['product::discount'].total_price);
                            $('.compare_title.product_1').append(
                                '<img class="w-100 border-images mb-3" id="compare_img_1" src="' + product_compare_img + '" alt="">' +
                                '<span id="compare_category_1" style="color: #8B8B8B;"></span>' +
                                '<p id="compare_title_1">' + element['product::name_th'] + '</p>' +
                                '<a class="delete_compare_product" data-product_id="' + element['product::id'] + '" href="" data-delete="1">' + delete_word + ' <i class="far fa-trash-alt"></i></a>');
                            // console.log(element['product::id']);
                            // $('#compare_img_1').attr('src', product_compare_img);
                            // $('#compare_title_1').text(element['product::name_th']);
                            // $('.compare_title.product_1 .delete_compare_product').attr('data-product_id', element['product::id']);
                            $('.compare_price .product_1').text(formatNumber(element['product::retail_price']));
                            $('.compare_model .product_1').text(element['product::model_name_th']);
                            $('.compare_insurance .product_1').text(element['product::condition_warranty_th']);
                            $('.compare_addcart .product_buy_1 button').data('product-id', element['product::id']);
                            $('.compare_addcart .product_buy_1 button').prop("disabled", false);
                            // console.log($('#compare_img_1').attr('src'));
                        }

                    } else if (index == 1) {
                        if (search_discount.indexOf("product::discount") >= 0) {
                            // console.log(element['product::id']);
                            $('.compare_title.product_2').append(
                                '<img class="w-100 border-images mb-3" id="compare_img_2" src="' + product_compare_img + '" alt="">' +
                                '<span id="compare_category_2" style="color: #8B8B8B;"></span>' +
                                '<p id="compare_title_2">' + element['product::name_th'] + '</p>' +
                                '<a class="delete_compare_product" data-product_id="' + element['product::id'] + '" href="" data-delete="2">' + delete_word + ' <i class="far fa-trash-alt"></i></a>');
                            // $('#compare_img_2').attr('src', product_compare_img);
                            // $('#compare_title_2').text(element['product::name_th']);
                            // $('.compare_title.product_2 .delete_compare_product').attr('data-product_id', element['product::id']);
                            $('.compare_price .product_2').text(element['product::retail_price']);
                            $('.compare_price_pro .product_2').text(element['product::discount'].total_price);
                            $('.compare_model .product_2').text(element['product::model_name_th']);
                            $('.compare_insurance .product_2').text(element['product::condition_warranty_th']);
                            $('.compare_addcart .product_buy_2 button').data('product-id', element['product::id']);
                            $('.compare_addcart .product_buy_2 button').prop("disabled", false);
                            // console.log($('#compare_img_2').attr('src'));
                        } else {
                            $('.compare_title.product_2').append(
                                '<img class="w-100 border-images mb-3" id="compare_img_2" src="' + product_compare_img + '" alt="">' +
                                '<span id="compare_category_2" style="color: #8B8B8B;"></span>' +
                                '<p id="compare_title_2">' + element['product::name_th'] + '</p>' +
                                '<a class="delete_compare_product" data-product_id="' + element['product::id'] + '" href="" data-delete="2">' + delete_word + ' <i class="far fa-trash-alt"></i></a>');
                            // $('#compare_img_2').attr('src', product_compare_img);
                            // $('#compare_title_2').text(element['product::name_th']);
                            // $('.compare_title.product_2 .delete_compare_product').attr('data-product_id', element['product::id']);
                            $('.compare_price .product_2').text(element['product::retail_price']);
                            $('.compare_model .product_2').text(element['product::model_name_th']);
                            $('.compare_insurance .product_2').text(element['product::condition_warranty_th']);
                            $('.compare_addcart .product_buy_2 button').data('product-id', element['product::id']);
                            $('.compare_addcart .product_buy_2 button').prop("disabled", false);
                            // console.log($('#compare_img_2').attr('src'));
                        }
                    } else {
                        if (search_discount.indexOf("product::discount") >= 0) {
                            // console.log(element['product::id']);
                            $('.compare_title.product_3').append(
                                '<img class="w-100 border-images mb-3" id="compare_img_3" src="' + product_compare_img + '" alt="">' +
                                '<span id="compare_category_3" style="color: #8B8B8B;"></span>' +
                                '<p id="compare_title_3">' + element['product::name_th'] + '</p>' +
                                '<a class="delete_compare_product" data-product_id="' + element['product::id'] + '" href="" data-delete="3">' + delete_word + ' <i class="far fa-trash-alt"></i></a>');
                            // $('#compare_img_3').attr('src', product_compare_img);
                            // $('#compare_title_3').text(element['product::name_th']);
                            // $('.compare_title.product_3 .delete_compare_product').attr('data-product_id', element['product::id']);
                            $('.compare_price .product_3').text(element['product::retail_price']);
                            $('.compare_price_pro .product_3').text(element['product::discount'].total_price);
                            $('.compare_model .product_3').text(element['product::model_name_th']);
                            $('.compare_insurance .product_3').text(element['product::condition_warranty_th']);
                            $('.compare_addcart .product_buy_3 button').data('product-id', element['product::id']);
                            $('.compare_addcart .product_buy_3 button').prop("disabled", false);
                            // console.log($('#compare_img_3').attr('src'));
                        } else {
                            $('.compare_title.product_3').append(
                                '<img class="w-100 border-images mb-3" id="compare_img_3" src="' + product_compare_img + '" alt="">' +
                                '<span id="compare_category_3" style="color: #8B8B8B;"></span>' +
                                '<p id="compare_title_3">' + element['product::name_th'] + '</p>' +
                                '<a class="delete_compare_product" data-product_id="' + element['product::id'] + '" href="" data-delete="3">' + delete_word + ' <i class="far fa-trash-alt"></i></a>');
                            // $('#compare_img_3').attr('src', product_compare_img);
                            // $('#compare_title_3').text(element['product::name_th']);
                            // $('.compare_title.product_3 .delete_compare_product').attr('data-product_id', element['product::id']);
                            $('.compare_price .product_3').text(element['product::retail_price']);
                            $('.compare_model .product_3').text(element['product::model_name_th']);
                            $('.compare_insurance .product_3').text(element['product::condition_warranty_th']);
                            $('.compare_addcart .product_buy_3 button').data('product-id', element['product::id']);
                            $('.compare_addcart .product_buy_3 button').prop("disabled", false);
                        }
                    }
                })

                var row_index;
                var row_array;
                var row_sort = [];
                var row_sort_value = [];

                response.data.product_spec.forEach((element, index, array) => {
                    row_array = (element['product::technical_spec']);

                    if (index == 0) {
                        $('#compare_category_1').text(element['product_category::name_th'])
                    } else if (index == 1) {
                        $('#compare_category_2').text(element['product_category::name_th'])
                    } else {
                        $('#compare_category_3').text(element['product_category::name_th'])
                    }
                    $('#compare_product_modal').modal('show');
                })

                $(row_array).each(function() {
                    row_sort.push($(this)[0].technical_spec);
                    row_sort_value.push($(this)[0].value_spec);
                });

                row_sort = row_sort.sort((firstItem, secondItem) => firstItem.order - secondItem.order);

                var filtered_row_sort_value = row_sort_value.filter(function(el) {
                    return el != null;
                });

                row_sort_value = filtered_row_sort_value.sort((firstItem, secondItem) => firstItem.product_id - secondItem.product_id);

                $(row_sort).each(function(index) {
                    // console.log('technical_index', index);
                    $('.spec_' + $(this)[0].id).remove();
                    if (index % 2 != 0) {
                        $("#technical_spec").append('<tr class="compare_model spec_' + $(this)[0].id + '">' + '<th scope="row">' + $(this)[0].name_th + '</th>' + '</tr>');
                    } else {
                        $("#technical_spec").append('<tr class="compart_table_bg spec_' + $(this)[0].id + '">' + '<th scope="row">' + $(this)[0].name_th + '</th>' + '</tr>');
                    }

                    for (let i = 0; i < 3; i++) {
                        $(".spec_" + $(this)[0].id).append('<td class="product_' + (i + 1) + '"></td>');
                    }
                });

                var index_row_sort;
                var define_product_column = 0;
                row_sort_value.forEach((element, index, array) => {
                    // console.log(element);
                    if (element != null) {
                        if (index_row_sort != element.product_id) {
                            index_row_sort = element.product_id;
                            define_product_column = define_product_column + 1;
                        }
                    }

                    if (element != null) {
                        $('.spec_' + element.technical_spec_id + ' .product_' + define_product_column).text(element.value_th);
                    }
                })

                $('.compart_table_bg td').each(function(index) {
                    if ($(this).is(':empty')) {
                        $(this).text('-');
                    }
                });

            });
        });

        $(document).on("click", ".delete_compare_product ", function() {
            event.preventDefault();
            $(".product_" + $(this).data('delete') + "").empty();

            var removeItem = $(this).data('product_id');
            var compare_product = '<?php echo getWording('index', 'compare_product') ?>';

            compare_list = jQuery.grep(compare_list, function(value) {
                return value != removeItem;
            });
            // console.log(compare_list);
            sessionStorage.setItem("compare_product", compare_list);
            // console.log(sessionStorage.getItem("compare_product"));
            $('.compare_amount').text(compare_product + ' (' + compare_list.length + '/3)');
            $('.compare_addcart .product_buy_' + $(this).data('delete') + ' button').prop("disabled", true);
        });

        $(document).on("click", "#reset ", function() {
            var compare_word = '<?php echo getWording('index', 'compare_product') ?>';
            $(compare_list).each(function() {
                $('.product_' + $(this)[0]).remove();
            });
            $('#technical_spec').empty();
            compare_list = [];
            // console.log(compare_list);
            sessionStorage.removeItem('compare_product');
            // console.log(sessionStorage.getItem('compare_product'));

            $('.compare_amount').text(compare_word + ' (0/3)');
            $("#compare").slideUp(500);
        });

        $(document).on("click", ".order_btn ", function() {
            // console.log('order', member_id);
            event.preventDefault();
            // console.log($(this).data('product-qty'));
            var add_cart_word = '<?php echo getWording('index', 'add_cart') ?>';
            if ($(this).data('product-qty') != 0) {
                if (token != "") {
                    $('#add-success').modal('show');
                    setTimeout(function() {
                        $('#add-success').modal('hide');
                    }, 1000);
                }
                var $this = $(this);
                $this.toggleClass('SeeMore');
                if ($this.hasClass('.order_btn')) {
                    $this.text('See More');
                } else {
                    $this.text(add_cart_word);
                }
                if (member_id != "") {
                    // console.log('product')
                    var thisRemoveClass = $(this);
                    var data_token = {
                        "member_id": member_id,
                        "product_id": $(this).data('product-id'),
                        "quantity": $(this).data('quantity')
                    }

                    var add_cart = {
                        "url": "<?php echo base_url('Product/addCart') ?>",
                        "method": "POST",
                        "data": data_token
                    }
                    // console.log(data_token)

                    $.ajax(add_cart).done(function(response) {
                        console.log(response);
                        $(".cart_list_header").empty();
                        if (response.code == '0x0000-00000') {
                            $(".shopping_cart_count").load(document.URL + ' .shopping_cart_count');
                            $("#mobile_shopping_cart_count").load(document.URL + ' #mobile_shopping_cart_count');
                            $(".cart_list_header").load(document.URL + ' .cart_list_header', function() {
                                crt_total();
                            });
                        }
                    });
                }
            } else {
                if (token != "") {
                    $('#product-outstock').modal('show');
                }
            }
        });

        $(document).on("click", ".remove_cart_item ", function() {
            // var cart_item = parseFloat($('.cart_count').data('cart-items'));
            event.preventDefault();
            // console.log(member_id);
            if (member_id != "") {
                var thisRemoveClass = $(this);
                var data_token = {
                    "cart_id": $(this).data('remove')
                }

                var add_cart = {
                    "url": "<?php echo base_url('Product/removeCart') ?>",
                    "method": "POST",
                    "data": data_token
                }

                $.ajax(add_cart).done(function(response) {
                    console.log(cart_item);
                    $(".cart_list_header").empty();
                    // $('.cart_dropdown').remove($('.shopping_cart_count'));
                    $("#shopping_cart_list").empty();
                    $('input.form_payment').remove();
                    $('input.form_quotation').remove();
                    $('input.form_discount').remove();

                    if (response.code == '0x0000-00000') {
                        $("#mobile_shopping_cart_count").load(document.URL + ' #mobile_shopping_cart_count');
                        $(".shopping_cart_count").load(document.URL + ' .shopping_cart_count');
                        $(".cart_list_header").load(document.URL + ' .cart_list_header', function() {
                            crt_total();
                        });
                        $("#shopping_cart_list").load(document.URL + ' #shopping_cart_list tr', function() {
                            crt_total();
                            cart_crt_total();
                            delivery_cart_crt_total();
                            sum = 0;
                            var list_var = '<?php echo getWording('product', 'list') ?>';
                            sum_delivery = 0;
                            var product_count = 0;
                            $('#total_cart_price_page').text('0');
                            $('#delivery_total_cart_price_page').text('0');
                            $("#total_price").text('0');
                            $('#cart_count').text('0');
                            $('.shopping_cart_item td .cartlist-checkbox input').change(function() {
                                if ($(this).is(':checked')) {
                                    $('#product_discount').append('<input class="form_discount product_discount_' + $(this).data('product_id') + '" name="product_id[]" value="' + $(this).data('product_id') + '" type="hidden">');
                                    $('#product_discount').append('<input class="form_discount product_discount_' + $(this).data('product_id') + '" name="product_qty[]" value="' + $(this).data('qty') + '" type="hidden">');
                                    // console.log('checkbox');
                                    product_quantity = $(this).data('qty');
                                    product_minimum = $(this).data('minimum_product');
                                    // console.log('product_quantity', product_quantity);
                                    // console.log('product_minimum', product_minimum);

                                    if (product_quantity < product_minimum) {
                                        alert('กรุณเพิ่มจำนวนให้ถึงจำนวนสั่งซื้อขั้นต่ำ' + product_minimum + 'ชิ้น');
                                        $('.shopping_cart_item td .cartlist-checkbox input').prop("checked", false);

                                        sum = 0;
                                        $('#total_cart_price_page').text(formatNumber(sum));
                                        product_count = 0;
                                        $('#cart_count').text(product_count + ' ' + list_var);

                                        $('input.form_payment').remove();
                                        $('input.form_quotation').remove();
                                        $('input.form_discount').remove();

                                        sum_delivery = 0;
                                        $('#delivery_total_cart_price_page').text(formatNumber(sum_delivery));

                                        $('#delivery_total_cart_price_page').each(function() {
                                            delivery_total_price = 0;

                                        });
                                        if (delivery_total_price <= 0) {
                                            delivery_total_price = 0;
                                        }

                                        $("#delivery_total_cart_price_page").text(formatNumber(delivery_total_price).toString());

                                        $('#total_price').each(function() {
                                            total_price = 0;
                                        });

                                        if (total_price <= 0) {
                                            total_price = 0;
                                        }

                                        $("#total_price").text(formatNumber(total_price).toString());

                                    } else {
                                        // console.log('checkbox');
                                        sum = sum + parseFloat($(this).val());
                                        $('#total_cart_price_page').text(formatNumber(sum));
                                        product_count = parseFloat(product_count) + 1;
                                        $('#cart_count').text(product_count + ' ' + list_var);
                                        $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="product_id[]" id="id_check" value="' + $(this).data('product_id') + ' "type="hidden">');
                                        $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="product_retail_price[]" id="retail_price_check" value="' + $(this).data('retail_price') + ' "type="hidden">');
                                        $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="product_qty[]" id="qty_check" value="' + $(this).data('qty') + ' "type="hidden">');
                                        $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="product_total_price[]" id="total_price_check" value="' + $(this).val() + ' "type="hidden">');
                                        $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="delivery_price[]" id="delivery_price_check" value="' + $(this).data('delivery_price') + ' "type="hidden">');
                                        $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="minimum_product[]" id="minimum_product_check" value="' + $(this).data('minimum_product') + ' "type="hidden">');

                                        $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="product_id[]" value="' + $(this).data('product_id') + '" type="hidden">');
                                        $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="product_retail_price[]" value="' + $(this).data('retail_price') + '" type="hidden">');
                                        $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="product_qty[]" value="' + $(this).data('qty') + '" type="hidden">');
                                        $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="product_total_price[]" value="' + $(this).val() + '" type="hidden">');
                                        $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="delivery_price[]" value="' + $(this).data('delivery_price') + '" type="hidden">');
                                        $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="real_price[]" value="' + $(this).data('real_price') + '" type="hidden">');

                                        sum_delivery = sum_delivery + parseFloat($(this).data('delivery-sum'));
                                        $('#delivery_total_cart_price_page').text(formatNumber(sum_delivery));

                                        $('#delivery_total_cart_price_page').each(function() {
                                            delivery_total_price = sum_delivery - discount_delivery;
                                        });

                                        if (delivery_total_price <= 0) {
                                            delivery_total_price = 0;
                                        }

                                        $("#delivery_total_cart_price_page").text(formatNumber(delivery_total_price).toString());

                                        $('#total_price').each(function() {
                                            total_price = (sum + delivery_total_price) - discount_onlynormalprice;
                                        });

                                        if (total_price <= 0) {
                                            total_price = 0;
                                        }

                                        $("#total_price").text(formatNumber(total_price).toString());
                                    }

                                } else {
                                    sum = sum - parseFloat($(this).val());
                                    $('#total_cart_price_page').text(formatNumber(sum));
                                    product_count = parseFloat(product_count) - 1;
                                    $('#cart_count').text(product_count + ' ' + list_var);

                                    $('input.product_' + $(this).data('product_id') + '').remove();
                                    $('input.product_quotation_' + $(this).data('product_id') + '').remove();
                                    $('input.product_discount_' + $(this).data('product_id') + '').remove();

                                    sum_delivery = sum_delivery - parseFloat($(this).data('delivery-sum'));
                                    $('#delivery_total_cart_price_page').text(formatNumber(sum_delivery));

                                    $('#delivery_total_cart_price_page').each(function() {
                                        delivery_total_price = sum_delivery - discount_delivery;

                                    });
                                    if (delivery_total_price <= 0) {
                                        delivery_total_price = 0;
                                    }

                                    $("#delivery_total_cart_price_page").text(formatNumber(delivery_total_price).toString());

                                    $('#total_price').each(function() {
                                        total_price = (sum + delivery_total_price) - discount_onlynormalprice;
                                    });

                                    if (total_price <= 0) {
                                        total_price = 0;
                                    }

                                    $("#total_price").text(formatNumber(total_price).toString());
                                };
                            });
                        });
                    }
                });
            }
        });

        $(document).on("click", ".update_cart_item ", function() {
            event.preventDefault();
            // console.log(member_id);
            if (member_id != "") {
                var thisRemoveClass = $(this);
                var data_token = {
                    "member_id": member_id,
                    "cart_id": $(this).parent('.product-quantity').data('cart'),
                    "action": $(this).data('update_type'),
                }

                var add_cart = {
                    "url": "<?php echo base_url('Product/updateCart') ?>",
                    "method": "POST",
                    "data": data_token
                }

                $.ajax(add_cart).done(function(response) {
                    console.log(response);
                    $(".cart_list_header").empty();
                    $("#shopping_cart_list").empty();
                    $('input.form_payment').remove();
                    $('input.form_quotation').remove();
                    $('input.form_discount').remove();

                    if (response.code == '0x0000-00000') {
                        $("#mobile_shopping_cart_count").load(document.URL + ' #mobile_shopping_cart_count');
                        $(".shopping_cart_count").load(document.URL + ' .shopping_cart_count');
                        // $(".cart_list_header").load(document.URL + ' .cart_list_header');

                        $(".cart_list_header").load(document.URL + ' .cart_list_header', function() {
                            crt_total();
                        });
                        $("#shopping_cart_list").load(document.URL + ' #shopping_cart_list tr', function() {
                            crt_total();
                            cart_crt_total();
                            delivery_cart_crt_total();
                            sum = 0;
                            var list_var = '<?php echo getWording('product', 'list') ?>';
                            sum_delivery = 0;
                            var product_count = 0;
                            discount_onlynormalprice = 0;
                            $('#total_cart_price_page').text('0');
                            $('#delivery_total_cart_price_page').text('0');
                            $("#total_price").text('0');
                            $('#cart_count').text('0');
                            $('.shopping_cart_item td .cartlist-checkbox input').change(function() {
                                if ($(this).is(':checked')) {
                                    $('#product_discount').append('<input class="form_discount product_discount_' + $(this).data('product_id') + '" name="product_id[]" value="' + $(this).data('product_id') + '" type="hidden">');
                                    $('#product_discount').append('<input class="form_discount product_discount_' + $(this).data('product_id') + '" name="product_qty[]" value="' + $(this).data('qty') + '" type="hidden">');
                                    // console.log('checkbox');
                                    product_quantity = $(this).data('qty');
                                    product_minimum = $(this).data('minimum_product');
                                    // console.log('product_quantity', product_quantity);
                                    // console.log('product_minimum', product_minimum);

                                    if (product_quantity < product_minimum) {
                                        alert('กรุณเพิ่มจำนวนให้ถึงจำนวนสั่งซื้อขั้นต่ำ' + product_minimum + 'ชิ้น');
                                        $('.shopping_cart_item td .cartlist-checkbox input').prop("checked", false);

                                        sum = 0;
                                        $('#total_cart_price_page').text(formatNumber(sum));
                                        product_count = 0;
                                        $('#cart_count').text(product_count + ' ' + list_var);

                                        $('input.form_payment').remove();
                                        $('input.form_quotation').remove();
                                        $('input.form_discount').remove();

                                        sum_delivery = 0;
                                        $('#delivery_total_cart_price_page').text(formatNumber(sum_delivery));

                                        $('#delivery_total_cart_price_page').each(function() {
                                            delivery_total_price = 0;

                                        });
                                        if (delivery_total_price <= 0) {
                                            delivery_total_price = 0;
                                        }

                                        $("#delivery_total_cart_price_page").text(formatNumber(delivery_total_price).toString());

                                        $('#total_price').each(function() {
                                            total_price = 0;
                                        });

                                        if (total_price <= 0) {
                                            total_price = 0;
                                        }

                                        $("#total_price").text(formatNumber(total_price).toString());

                                    } else {
                                        // console.log('checkbox');
                                        sum = sum + parseFloat($(this).val());
                                        $('#total_cart_price_page').text(formatNumber(sum));
                                        product_count = parseFloat(product_count) + 1;
                                        $('#cart_count').text(product_count + ' ' + list_var);
                                        $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="product_id[]" id="id_check" value="' + $(this).data('product_id') + ' "type="hidden">');
                                        $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="product_retail_price[]" id="retail_price_check" value="' + $(this).data('retail_price') + ' "type="hidden">');
                                        $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="product_qty[]" id="qty_check" value="' + $(this).data('qty') + ' "type="hidden">');
                                        $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="product_total_price[]" id="total_price_check" value="' + $(this).val() + ' "type="hidden">');
                                        $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="delivery_price[]" id="delivery_price_check" value="' + $(this).data('delivery_price') + ' "type="hidden">');
                                        $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="minimum_product[]" id="minimum_product_check" value="' + $(this).data('minimum_product') + ' "type="hidden">');

                                        $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="product_id[]" value="' + $(this).data('product_id') + '" type="hidden">');
                                        $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="product_retail_price[]" value="' + $(this).data('retail_price') + '" type="hidden">');
                                        $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="product_qty[]" value="' + $(this).data('qty') + '" type="hidden">');
                                        $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="product_total_price[]" value="' + $(this).val() + '" type="hidden">');
                                        $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="delivery_price[]" value="' + $(this).data('delivery_price') + '" type="hidden">');
                                        $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="real_price[]" value="' + $(this).data('real_price') + '" type="hidden">');

                                        sum_delivery = sum_delivery + parseFloat($(this).data('delivery-sum'));
                                        $('#delivery_total_cart_price_page').text(formatNumber(sum_delivery));

                                        $('#delivery_total_cart_price_page').each(function() {
                                            delivery_total_price = sum_delivery - discount_delivery;
                                        });

                                        if (delivery_total_price <= 0) {
                                            delivery_total_price = 0;
                                        }

                                        $("#delivery_total_cart_price_page").text(formatNumber(delivery_total_price).toString());

                                        // console.log(discount_baht);
                                        $('#total_price').each(function() {
                                            total_price = (sum + delivery_total_price) - discount_onlynormalprice;
                                        });

                                        if (total_price <= 0) {
                                            total_price = 0;
                                        }

                                        $("#total_price").text(formatNumber(total_price).toString());
                                    }

                                } else {
                                    sum = sum - parseFloat($(this).val());
                                    $('#total_cart_price_page').text(formatNumber(sum));
                                    product_count = parseFloat(product_count) - 1;
                                    $('#cart_count').text(product_count + ' ' + list_var);

                                    // $(this).attr('name="product_id"')
                                    $('input.product_' + $(this).data('product_id') + '').remove();
                                    $('input.product_quotation_' + $(this).data('product_id') + '').remove();
                                    $('input.product_discount_' + $(this).data('product_id') + '').remove();

                                    sum_delivery = sum_delivery - parseFloat($(this).data('delivery-sum'));
                                    $('#delivery_total_cart_price_page').text(formatNumber(sum_delivery));
                                    // console.log("sum_delivery",sum_delivery);

                                    $('#delivery_total_cart_price_page').each(function() {
                                        delivery_total_price = sum_delivery - discount_delivery;

                                    });
                                    if (delivery_total_price <= 0) {
                                        delivery_total_price = 0;
                                    }

                                    $("#delivery_total_cart_price_page").text(formatNumber(delivery_total_price).toString());

                                    $('#total_price').each(function() {
                                        total_price = (sum + delivery_total_price) - discount_onlynormalprice;
                                    });

                                    if (total_price <= 0) {
                                        total_price = 0;
                                    }

                                    $("#total_price").text(formatNumber(total_price).toString());

                                };
                            });
                        });
                    }
                });
            }
        });
    });



    function crt_total() {
        // console.log('calculating!!!');
        var sum = 0;
        $('.crt_total').each(function() {
            sum += parseFloat($(this).data('product-sum'));
            // console.log('total_price', sum)
        });
        $("#total_cart_price").text(formatNumber(sum).toString());
        // console.log('total_price', $("#total_cart_price").text())
    }

    $("#cart_header_content").on('load', function() {
        var sum = 0;
        $('.crt_total').each(function() {
            sum += parseFloat($(this).data('product-sum'));

            $("#total_cart_price").text(formatNumber(sum));
            // cosole.log('total_price', sum)
        });
    });

    $("#bt-menu-mobile").click(function() {

        // $(this).toggleClass("close-bt-hamburguer");
        $(".naranja").toggleClass("abierto");
        $(".azul").toggleClass("abierto");

        return false;

    });

    function openSol() {
        // console.log("opennav");
        $("#bt-menu-mobile").toggleClass("close-bt-hamburguer");
        // $(".menu-mobile").addClass('d-none');
        // document.getElementById("img1").style.zIndex = "1";
        $(".menu-mobile").css("z-index", "2");
        $("#collapse-bg").removeClass("d-none");
        $("#collapse-bg").addClass("d-block");
        document.getElementById("mySideSol").style.width = "100%";
        // document.getElementById("main").style.marginLeft = "100%";
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    }

    /* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
    function closeSol() {
        // console.log("closenav");
        $("#bt-menu-mobile").removeClass("close-bt-hamburguer");
        // $(".menu-mobile").removeClass("d-none");
        $(".menu-mobile").css("z-index", "3");
        $("#collapse-bg").addClass("d-none");
        $("#collapse-bg").removeClass("d-block");
        document.getElementById("mySideSol").style.width = "0";
        // document.getElementById("main").style.marginLeft = "0";
        document.body.style.backgroundColor = "white";
    }

    function openMenu() {
        // console.log("opennav");
        $("#bt-mainmenu-mobile").toggleClass("close-bt-hamburguer");
        // $(".menu-mobile").addClass('d-none');
        // document.getElementById("img1").style.zIndex = "1";
        $(".mainmenu-mobile").css("z-index", "2");
        $("#collapse-bg").removeClass("d-none");
        $("#collapse-bg").addClass("d-block");
        document.getElementById("mySideMain").style.width = "100%";
        // document.getElementById("main").style.marginLeft = "100%";
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    }

    /* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
    function closeMenu() {
        // console.log("closenav");
        $("#bt-mainmenu-mobile").removeClass("close-bt-hamburguer");
        // $(".menu-mobile").removeClass("d-none");
        $(".mainmenu-mobile").css("z-index", "3");
        $("#collapse-bg").addClass("d-none");
        $("#collapse-bg").removeClass("d-block");
        document.getElementById("mySideMain").style.width = "0";
        // document.getElementById("main").style.marginLeft = "0";
        document.body.style.backgroundColor = "white";
    }

    function openAccount() {
        // console.log("opennav");
        $("#bt-mainmenu-mobile").toggleClass("close-bt-hamburguer");
        // $(".menu-mobile").addClass('d-none');
        // document.getElementById("img1").style.zIndex = "1";
        $(".mainmenu-mobile").css("z-index", "2");
        $("#collapse-bg").removeClass("d-none");
        $("#collapse-bg").addClass("d-block");
        document.getElementById("mySideAccount").style.width = "100%";
        // document.getElementById("main").style.marginLeft = "100%";
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    }

    /* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
    function closeAccount() {
        // console.log("closenav");
        $("#bt-mainmenu-mobile").removeClass("close-bt-hamburguer");
        // $(".menu-mobile").removeClass("d-none");
        $(".mainmenu-mobile").css("z-index", "3");
        $("#collapse-bg").addClass("d-none");
        $("#collapse-bg").removeClass("d-block");
        document.getElementById("mySideAccount").style.width = "0";
        // document.getElementById("main").style.marginLeft = "0";
        document.body.style.backgroundColor = "white";
    }

    var application
    $(".megamenu-list ul li").hover(
        function() {
            application = $(this).prop('id') || '';
            // console.log(application);
            var $trs = $('.sub-menumega[data-sub="' + application + '"]');

            // console.log($('.sub-menumega').data('sub'));
            // console.log(application);
            // console.log($('.sub-menumega').data('sub') != application);

            if ($trs != application) {
                $('.sub-menumega').removeClass('d-block');
                $('.sub-menumega').addClass('d-none');
                $trs.addClass('d-block');
                $trs.removeClass('d-none');
                // console.log('display-show', application);
            }
            // else {
            //     $trs.addClass('d-block');
            //     console.log('display', $application);
            // }

            // $trs.addClass('d-block');


            // console.log("add", $(this).attr('id'));
            // $(".sub-menumega").data($(this).attr('id')).addClass("d-block");
            // $(this).addClass("result_hover");
        }
        // function() {
        //     application = $(this).prop('id') || '';
        //     console.log(application);
        //     var $trs = $('.sub-menumega[data-sub="' + application + '"]');
        //     $trs.removeClass('d-block');
        //     // console.log("remove", $(this).attr('id'));
        //     // $(".sub-menumega").data($(this).attr('id')).removeClass("d-block");
        //     // $(this).removeClass("result_hover");
        // }
    );



    $('#play-button').click(function() {
        var mediaVideo = $("#my-video").get(0);
        if (mediaVideo.paused) {
            mediaVideo.play();
            $("#play-button").hide();
            $("#pause-button").fadeIn();
        }
    });

    $('#pause-button').click(function() {
        var mediaVideo = $("#my-video").get(0);
        if (mediaVideo.play) {
            mediaVideo.pause();
            $("#play-button").fadeIn();
            $("#pause-button").hide();
        }
    });

    $('#image_banner').click(function() {
        $('#exampleModal').modal('show');
    });

    $('#toggle-custom-size').click(function() {
        if ($("#toggle-custom-size").prop('checked') == true) {
            // console.log('burger on!!!');
            $(".secd-menu").addClass("active-secd-menu");
        } else {
            // console.log('burger off!!!');
            $(".secd-menu").removeClass("active-secd-menu");
        }
    });
    // $(".accordion-button.category").click(function() {
    //     var check_collaps = $(this).attr('aria-expanded');
    //     console.log(check_collaps);
    //     if(check_collaps == 'true'){
    //         $(this).attr('aria-expanded', false);
    //     }else{
    //         $(this).attr('aria-expanded', true);
    //     }
    // });
</script>