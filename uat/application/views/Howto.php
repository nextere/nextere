<?php include('header.php') ?>
<style>
    .nav {
        display: flex;
        flex-wrap: wrap;
        padding-left: 0;
        margin-bottom: 0;
        list-style: none;
        background-color: lightgray;
    }

    .nav-pills .nav-link {
        background: 0 0;
        border: 0;
        border-radius: .25rem;
        color: black;
        text-align: left;
        padding-left: 10%;
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .nav-pills .nav-link:hover {
        font-weight: bold;
    }

    .nav-pills .nav-link.active,
    .nav-pills .show>.nav-link {
        color: black;
        background-color: lightgray;
    }

    .tab-content>.active {
        display: block;
        padding-left: 5%;
    }

    .nav-link.active{
        font-weight: bold
    }

</style>

<body>
    <div class="col-xl-12 " style="background-color: #F0F0F0; text-align:center;padding:70px;">
        <h4><b><?php echo getWording('index', 'help') ?></b></h4>

    </div>
    <section class="Datasheet">
        <div class="container">
            <?php if (empty($conditions_list)) { ?>
                <?php echo null ?>
            <?php } else { ?>
                <div class="d-flex align-items-start mt-8 ">
                    <div class="nav flex-column nav-pills me-3 col-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <?php foreach ($conditions_list as $key => $conditions_item) { ?>
                            <?php if ($key == 0) { ?>
                                <button class="nav-link active" id="v-pills-home-tab" data-bs-toggle="pill" data-bs-target="#v-pills-home" type="button" role="tab" aria-controls="v-pills-home" aria-selected="true"> <?php echo getVariable($conditions_item, 'title') ?></button>
                            <?php } else { ?>
                                <button class="nav-link" id="v-pills-profile-tab-<?php echo ($conditions_item->id) ?>" data-bs-toggle="pill" data-bs-target="#v-pills-profile-<?php echo ($conditions_item->id) ?>" type="button" role="tab" aria-controls="v-pills-profile" aria-selected="false"><?php echo getVariable($conditions_item, 'title') ?></button>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="tab-content col-9" id="v-pills-tabContent">
                        <?php foreach ($conditions_list as $key => $conditions_item) { ?>
                            <?php if ($key == 0) { ?>
                                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab"><?php echo getVariable($conditions_item, 'detail') ?></div>
                            <?php } else { ?>
                                <div class="tab-pane fade" id="v-pills-profile-<?php echo ($conditions_item->id) ?>" role="tabpanel" aria-labelledby="v-pills-profile-tab"><?php echo getVariable($conditions_item, 'detail') ?></div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>

    </section>

    <div class="mt-5"> <?php include('footer.php') ?></div>

</body>

</html>