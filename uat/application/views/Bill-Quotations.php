<?php include('header.php') ?>
<style>
    .dashed {
        border: 1px dashed #BCBCBC;
        padding: 30px;
    }
</style>

<body>
    <div class="row ">
        <div class="pt-8 pb-8" style="background-color:#F0F0F0; text-align: center;">
            <h3><b><?php echo getWording('quotation', 'success') ?></b></h3>
            <h3><b><?php echo getWording('quotation', 'thank') ?> Nextere</b></h3>
        </div>
        <?php foreach ($quotation_detail as $key => $quotation) { ?>
            <div class="row justify-content-center pt-10">
                <div class="col-3" style="text-align: center;">
                </div>
                <div class="col-3">
                    <b><?php echo getWording('quotation', 'order_no') ?></b>
                </div>
                <div class="col-3" style="text-align: right;">
                    <b><?php echo $quotation->quotation_no ?></b>
                </div>
                <div class="col-3" style="text-align: center;">
                </div>
            </div>
            <div class="row justify-content-center pt-5">
                <div class="col-3" style="text-align: center;">
                </div>
                <div class="col-3">
                    <b><?php echo getWording('quotation', 'date') ?></b>
                </div>
                <div class="col-3" style="text-align: right;">
                    <b><?php echo $quotation->created_date ?></b>
                </div>
                <div class="col-3" style="text-align: center;">
                </div>
            </div>

            <hr style="width:50%; margin-left:25% !important; margin-right:25% !important; margin-top:15px !important;" />
            <div class="col-12 pt-4" style="text-align:center;">
                <h5><b> * <?php echo getWording('quotation', 'sent') ?> <?php echo $quotation->email ?> <?php echo getWording('quotation', 'your') ?></b></h5>
            </div>
            <div class="col-12 mt-3" style="text-align:center;">
                <a href="https://www.nextere.space/quotation/export_pdf/<?php echo $quotation->id ?>" target="_blank"><button type="button" class="btn" style="width: 250px; height: 40px; border: 1px solid; border-radius: 50px;"> <b><img class="mr-3" style="width: 20px; height: 18px;" src="<?php echo base_url('/assets/img/icon/Download.png') ?>"> <?php echo getWording('quotation', 'download') ?></b></button></a>
            </div>
        <?php } ?>
    </div>
    <div class="col-12 pt-10" style="text-align:center;">
        <h5><b> <?php echo getWording('quotation', 'detail') ?></b></h5>
    </div>
    <div class="row justify-content-center mt-3">
        <div class="col-7 dashed">
            <div class="row">
                <?php foreach ($quotation_detail[0]->quotation_details as $key => $quotation_item) { ?>
                    <?php
                    // echo '<pre>';
                    // print_r($quotation_item);
                    // exit(); 
                    ?>
                    <div class="col-2">
                        <img class="w-100" src="<?php echo $quotation_item->{'product::images'}[0] ?>" alt="">
                    </div>
                    <div class="col-8" style="padding-left: 20px; align-self: center;">
                        <h5 class="cart-item-title w-75"><?php echo getVariable($quotation_item, 'product::name') ?></h5>
                        <h5 class="cart-item-title w-75">Model : <?php echo getVariable($quotation_item, 'product::model_name') ?></h5>
                    </div>
                    <div class="col-2" style="text-align: right;  align-self: center;"><b><?php echo getWording('quotation', 'quantity') ?> : <?php echo $quotation_item->{'quotation_detail::quantity'} ?> </b></div>
                    <hr class="mt-3" style="color: #BCBCBC ;">
                <?php } ?>

            </div>

        </div>

        <div class="col-6 pt-8 pb-10" style="text-align:right;">
            <a href="<?php echo base_url('') ?>"> <button type="button" class="btn btn-outline-dark pb-5" style="width: 250px; height: 40px; padding-top: 10px; border-radius: 50px;"> <b><?php echo getWording('quotation', 'home') ?></b></button></a>
        </div>
        <div class="col-6 pt-8 pb-10">
            <a href="<?php echo base_url('Account/quotation_request_history') ?>"><button type="button" class="btn btn-outline-dark pb-5" style="width: 250px; height: 40px; padding-top: 10px; background-color: #D3D3D3; border: 0px; border-radius: 50px;"> <b><?php echo getWording('quotation', 'view') ?></b></button></a>
        </div>
    </div>


    <div class="mt-5"> <?php include('footer.php') ?></div>

</body>