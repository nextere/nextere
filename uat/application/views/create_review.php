<?php include("header.php") ?>
<?php
// echo "<pre>";
// print_r($order_limit);
// exit();
?>

<section class="create_review">
    <div class="container">
        <div class="row">
            <h3 class="text-center my-5"><?php echo getWording('product', 'product_review') ?></h3>
        </div>
        <!-- <form id="data"> -->
        <!-- <input type="file" id="test_file"> -->
        <?php foreach ($order_limit[0]->purchase_transaction_detail as $key => $order_item) { ?>
            <?php foreach ($order_product_history as $key => $order_product) { ?>
                <?php if ($order_product->{'product::id'} == $order_item->product_id) { ?>
                    <hr style="background-color:gray; border: 1px;">
                    <div class="row justify-content-center  mb-3">
                        <div class="col-12 col-md-6 col-lg-3">
                            <?php if (empty($order_product->{'product::images'})) { ?>
                                <img class="w-50" src="<?php echo base_url('/assets/img/no-images.png') ?>" alt="">
                            <?php } else { ?>
                                <img class="w-50" src="<?php echo $order_product->{'product::images'}[0] ?>" alt="">
                            <?php } ?>
                            <div class="product-rv-detail mt-5">
                                <span class="product-rv-number d-block"><b><?php echo getVariable($order_product, 'product::name')  ?></b></span>
                                <span class="product-rv-quantity d-block"><b><?php echo getWording('product', 'quantity') ?> <?php echo $order_item->qty ?> <?php echo getWording('product', 'piece') ?> </b></span>
                            </div>
                        </div>

                        <div class="col-12 col-md-6 col-lg-6">
                            <!-- <div class="d-flex"> -->

                            <ul class="p-0">
                                <li>
                                    <div class="row mb-5">
                                        <div class="col-3 align-self-center"><span>Rating*</span></div>
                                        <div class="col-9">
                                            <div class="d-flex">
                                                <div class="rating">
                                                    <input data-key="<?php echo $key ?>" type="checkbox" class="star star5" id="product_id_<?php echo $order_item->product_id ?>_star5" value="5">
                                                    <label for="product_id_<?php echo $order_item->product_id ?>_star5" title="5 Star"></label>
                                                    <input data-key="<?php echo $key ?>" type="checkbox" class="star star4" id="product_id_<?php echo $order_item->product_id ?>_star4" value="4">
                                                    <label for="product_id_<?php echo $order_item->product_id ?>_star4" title="4 Star"></label>
                                                    <input data-key="<?php echo $key ?>" type="checkbox" class="star star3" id="product_id_<?php echo $order_item->product_id ?>_star3" value="4">
                                                    <label for="product_id_<?php echo $order_item->product_id ?>_star3" title="3 Star"></label>
                                                    <input data-key="<?php echo $key ?>" type="checkbox" class="star star2" id="product_id_<?php echo $order_item->product_id ?>_star2" value="2">
                                                    <label for="product_id_<?php echo $order_item->product_id ?>_star2" title="2 Star"></label>
                                                    <input data-key="<?php echo $key ?>" type="checkbox" class="star star1" id="product_id_<?php echo $order_item->product_id ?>_star1" value="1">
                                                    <label for="product_id_<?php echo $order_item->product_id ?>_star1" title="1 Star"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="row mb-5">
                                        <div class="col-3">
                                            <span><?php echo getWording('product', 'review_text') ?> *</span>
                                        </div>
                                        <div class="col-9">
                                            <textarea data-key="<?php echo $key ?>" id="message" name="message[]" class="form-control" rows="3"></textarea>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="row mb-5">
                                        <div class="col-3">
                                            <span><?php echo getWording('product', 'image_video') ?> *</span>
                                        </div>
                                        <div class="col-9">

                                            <div id="dropzone" class="dropzone rounded" style="border-style: dashed" data-dropzone-num="<?php echo $key ?>">
                                                <div class="fallback">
                                                    <input name="file" type="file" multiple />
                                                </div>
                                                <div class="dz-message" data-dz-message><span>+ <?php echo getWording('product', 'insert') ?></span></div>
                                            </div>
                                            <div class="mt-5"><?php echo getWording('product', 'recommend_image') ?></div>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        <form id="dataform">
            <input style="display: none;" type="text" class="member_id" name="member_id" value="<?php echo $order_limit[0]->member_id ?>">
            <input style="display: none;" type="text" class="purchase_transaction_detail_id" name="purchase_transaction_detail_id" value="<?php echo $order_limit[0]->id ?>">
            <?php foreach ($order_limit[0]->purchase_transaction_detail as $key => $order_item) { ?>
                <input style="display: none;" type="text" id="product_id" name="product_id[]" value="<?php echo $order_item->product_id ?>">
            <?php } ?>
            <div id="my-dropzone"></div>
        </form>
        <div class="submit pt-4 pb-4" style=" text-align: center">
            <button id="submit-review" style="width: 151px; height: 45px; background: #7c7c7c; color:#FFFFFF; border: 0px; border-radius: 23px; font-weight: bold;"><?php echo getWording('product', 'confirm_review') ?></button>
        </div>
        <!-- </form> -->
    </div>
</section>

<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true" style="z-index: 1061;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h3 class="review_response"></h3>
            </div>
            <div class="modal-footer review_response_btn">
                <!-- <button type="button" class="btn btn-success register_success d-none" onclick="location.href='<?php // echo base_url() 
                                                                                                                    ?>'">Understood</button> -->
                <button type="button" class="btn btn-secondary review_fail d-none" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="mt-5"> <?php include('footer.php') ?></div>

<script src="<?php echo base_url('assets/js/validator.js') ?>"></script>

<script>
    Dropzone.autoDiscover = false;
    var review_img_array = [];
    $(document).ready(function() {

        $('input[name="product_id[]"]').each(function() {
            rating.push('');
        });



        $(".dropzone").dropzone({
            autoProcessQueue: false,
            url: 'http://localhost',
            // previewsContainer: false,
            // autoDiscover: false,
            paramName: "product_image",
            addRemoveLinks: true,

            init: function() {
                console.log(Dropzone.files);
                this.on("addedfile", function() {
                    this.on("addedfile", function() {
                        $('.dropzone').on("addedfile").each(function(index) {
                            console.log(index, $(this)[0].dropzone.files);
                            console.log(typeof($(this)[0].dropzone.files));
                            review_img_array.push($(this)[0].dropzone.files);
                        });
                        // $("#my-dropzone").append($('<input type="hidden" ' + 'name="product_images[]" ' + 'value="' + obj.file_name + '">'));
                    });

                });
            }
        });

    });

    var product_id = [];
    var message = [];
    var rating = [];
    var product = [];

    $(".rating input").click(function() {
        rating[$(this).data('key')] = $(this).val();
        // console.log(rating);
    });


    $("#submit-review").click(function(e) {
        $('textarea[name="message[]"]').each(function() {
            message.push($(this).val());
            // console.log(message);
        });



        $('input[name="product_id[]"]').each(function(key) {
            var data = new FormData();
            var length = $('input[name="product_id[]"]').length;
            // console.log('this', $(this).parent('.member_id'));
            // var obj = {
            //     "member_id": $(".member_id").val(),
            //     "purchase_transaction_detail_id": $(".purchase_transaction_detail_id").val(),
            //     "product_id": $(this).val(),
            //     "rating": rating[key],
            //     "message": message[key],
            //     "image": review_img_array[key]
            // }
            // product.push(obj);
            // console.log(product);
            // console.log(review_img_array[key]);

            data.append("member_id", $(".member_id").val());
            data.append("purchase_transaction_detail_id", $(".purchase_transaction_detail_id").val());
            data.append("product_id", $(this).val());
            data.append("rating", rating[key]);
            data.append("message", message[key]);
            if (review_img_array[key] != undefined) {
                for (var i = 0; i < review_img_array[key].length; i++) {
                    data.append("image_paths[" + i + "]", review_img_array[key][i]);
                }
            }

            var send_data = {
                url: "https://nextere.space/api_area/Product/review/",
                method: "POST",
                // dataType: 'json',
                // contentType: 'multipart/form-data',
                // async: true,
                contentType: false,
                processData: false,
                data: data
                // {
                //     'data': JSON.stringify(product)
                // }
            }

            // console.log(send_data);

            $.ajax(send_data).done(function(response) {
                console.log(response)
                if (length - 1 == key) {
                    // console.log('1234');
                    var error_word = '<?php echo getWording('product', 'error') ?>'
                    if (response.code == "0x0000-00000") {
                        $('.review_success').addClass('d-none');
                        $('.review_success').removeClass('d-block');
                        $('.review_fail').addClass('d-block');
                        $('.review_fail').removeClass('d-none');
                        $(".review_response").text(response.message);
                         window.location.href = "<?php echo base_url('/Account/order_details/' . $order_limit[0]->purchase_transaction_no) ?>";
                    } else {
                        $('.review_success').addClass('d-none');
                        $('#staticBackdrop').modal('show');
                        $('.review_success').removeClass('d-block');
                        $('.review_fail').addClass('d-block');
                        $('.review_fail').removeClass('d-none');
                        $(".review_response").text(error_word);
                        // window.location.href = "<?php echo base_url('/Account/review/' . $order_limit[0]->purchase_transaction_no) ?>";
                    }
                    $('#staticBackdrop').modal('show');
                }
            });
        });



        // for (var pair of data.entries()) {
        //     console.log(pair[0] + ', ' + pair[1]);
        // }
        // console.log(send_data);
        // console.log($('#test_file'));



        // e.preventDefault();
        // if ($("#data").valid() == true) {
        //     var data = {
        //         "member_id": $("#member_id").val(),
        //         "purchase_transaction_detail_id": $("#purchase_transaction_detail_id").val(),
        //         "product_id": product_id,
        //         "rating": rating,
        //         "message": message,
        //     }
        //     console.log('data :', data);
        //     console.log($("#data").valid());

        //     var send_data = {
        //         "url": "<?php // echo base_url('Account/create_review') ?>",
        //         "method": "POST",
        //         "data": data
        //     }

        //     $.ajax(send_data).done(function(response) {
        //         if (response.code == "0x0000-00000") {
        //             $('.review_success').addClass('d-none');
        //             $('.review_success').removeClass('d-block');
        //             $('.review_fail').addClass('d-block');
        //             $('.review_fail').removeClass('d-none');
        //             $(".review_response").text(response.message);
        //         } else {
        //             $('.review_success').addClass('d-none');
        //             // $('#staticBackdrop').modal('show');
        //             $('.review_success').removeClass('d-block');
        //             $('.review_fail').addClass('d-block');
        //             $('.review_fail').removeClass('d-none');
        //         }
        //         $('#staticBackdrop').modal('show');
        //     });
        // } else {
        //     console.log('not submit!!!');
        // }
    });
</script>
<script>
    // $('.dropzone').each(function() {
    //     Dropzone.autoDiscover = false;
    //     $(this).dropzone({
    //         autoProcessQueue: false,
    //         init: function() {
    //             this.on("addedfile", function(file) {
    //                 alert("Added file.");
    //             });
    //         }
    //         maxFiles: dropMaxFiles,
    //         paramName: dropParamName,
    //          maxFilesSize: dropMaxFileSize

    //          Rest of the configuration equal to all dropzones
    //     });

    // });
</script>
<script>
    // Dropzone.options.dropzone = {
    //   dictDefaultMessage: 'Drop file here or click to upload!!!!!!!!',
    //   addRemoveLinks: true,
    //   init: function () {
    //     this.on("addedfile", function(file) { alert("Added file."); });
    //   }
    // };
    // Dropzone.autoDiscover = false;
    // Dropzone.options.dropzone = {
    //     init: function() {
    //         Dropzone.options.previewaDropzone = false;
    //     }
    // };
    // Dropzone.options.dropzone = {

    // };
    // $(".dropzone").dropzone({
    // autoDiscover: false,
    // paramName: "product_image",
    // addRemoveLinks: true,
    // success: function(file, response) {
    //     var obj = JSON.parse(response);
    //     file.name = obj.file_name;
    //     if (obj.error == true) {
    //         file.previewElement.classList.add("dz-error");
    //     } else {
    //         file.previewElement.classList.add("dz-success");
    //         $("#my-dropzone").append($('<input type="hidden" ' + 'name="product_images[]" ' + 'value="' + obj.file_name + '">'));
    //     }
    // },
    // init: function() {
    //     Dropzone.options.previewaDropzone = false;
    // }

    // });
</script>