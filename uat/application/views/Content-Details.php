<?php include('header.php') ?>
<?php
// echo '<pre>';
// print_r($block_detail);
// exit();
$tag = array();
$tag = explode(",",  $block_detail->tags);
?>
<style>
    a {
        color: black;
        text-decoration: none;
    }

    a:hover {
        color: black;
        text-decoration: none;
    }

    .blog span {
        border: solid 1px;
        border-color: lightgray;
        padding: 2px 7px;
        background: lightgrey;
        margin-right: 10px;
        border-radius: 5px;
        font-size: .5rem;
    }

    .copy-link {
        cursor: pointer;
    }
</style>

<meta property="og:url" content="<?php echo base_url('Article/detail/' . $block_detail->id); ?>" />
<meta property="og:type" content="website" />
<meta property="og:title" content="<?php echo $block_detail->title_th ?>" />
<meta property="og:image" content="<?php echo base_url('/assets/img/1170-345.png') ?>" />

<body>
    <div class="row">
        <div class="col-12">
            <div class="w-100">
                <img class="w-100" src="<?php echo base_url($block_detail->banner_path) ?>" alt="">
            </div>
        </div>
    </div>
    <div class="container ">
        <div class="row">
            <div class="col-sm-12 col-lg-12 pt-5 pb-2" style="text-align:left;">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('') ?>"><?php echo getWording('index', 'home') ?> </a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url('Article') ?>"><?php echo getWording('index', 'article') ?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo getVariable($block_detail, 'title') ?></li>
                    </ol>
                </nav>
            </div>
            <div class="col-10">
                <h4><?php echo getVariable($block_detail, 'title') ?></h4>
                <p><?php $date = date_create($block_detail->created_date);
                    echo date_format($date, "d/m/y"); ?></p>
                <p><?php echo getVariable($block_detail, 'detail') ?></p>
            </div>
            <div class="col-1 " style="text-align: center;">
                <b><?php echo getWording('index', 'share') ?></b>
                <hr>
                <a href="#" id="fb-share-button" target="_blank"><i class="fab fa-facebook fa-2x pb-2 mt-2"></i>
                </a><br>
                <a target="_blank" data-lang="en" data-type="share-b" data-ver="3" href="https://social-plugins.line.me/lineit/share?url=<?php echo base_url('Article/detail/' . $block_detail->id); ?>">
                    <img class="mt-2"  style="width: 24px; height:24px;" src="<?php echo base_url('/assets/img/icon/line-black.png') ?>">
                </a> <br>
                <a href="https://twitter.com/intent/tweet?text= <?php echo base_url('Article/detail/' . $block_detail->id); ?>">
                    <i class="fab fa-twitter fa-2x pb-2 mt-2"></i>
                </a><br>
                <span class="copy-link" id="copy-link" onclick="copy_link()" href="<?php echo base_url('Article/detail/' . $block_detail->id); ?>" title="Link Copied!!!">
                    <img class="mt-2" style="width: 20px; height:20px;" src="<?php echo base_url('/assets/img/icon/link.png') ?>">
                </span>
            </div>

            <div class="col-12 mt-3">
                <b>TAG :
                    <?php foreach ($tag as $key => $block_tag) { ?>
                        <?php if ($block_tag != null) { ?>
                            <button type="button" class="btn btn-secondary"><?php echo ($block_tag) ?></button>
                        <?php } else { ?>
                          
                        <?php } ?>
                    <?php } ?>
            </div>
            <div class="product_heightlight mt-4 ">
                <h3 style="text-align: center;"><?php echo getWording('index', 'heightlight') ?></h3>
                <div class="product_heightlight_slider">
                    <?php if (!empty($article_relate)) { ?>
                        <?php foreach ($article_relate as $key => $product_item) { ?>
                            <div class="col-sm-6 col-xl-4 mt-4 item" style="text-align:left">
                                <div class="product-item">
                                    <div class="border-images mb-3">
                                        <a href="<?php echo base_url('/Product/Details/' . $product_item->{'product::id'}) ?>">
                                            <?php if (property_exists($product_item, 'product::images')) { ?>
                                                <img class="w-100" src="<?php echo $product_item->{'product::images'}[0] ?>" alt="">
                                            <?php } else { ?>
                                                <img class="w-100" src="<?php echo base_url('/assets/img/no-images.png') ?>" alt="">
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <span><?php echo getVariable($product_item, 'brand::name') ?></span>
                                    <h4><?php echo getVariable($product_item, 'product::name') ?></h4>
                                    <?php if ($product_item->{'product::retail_price'} != null) { ?>
                                        <?php if (empty($product_item->{'product::discount'})) { ?>
                                            <h4 class="mt-6">
                                                <?php echo number_format($product_item->{'product::retail_price'}); ?>
                                                <?php echo getWording('index', 'baht') ?>
                                            </h4>
                                        <?php } else { ?>
                                            <h4 class="mt-1">
                                                <div class="row">
                                                    <span class="col-6">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <span class="discount-product">- <?php echo ($product_item->{'product::discount'}->discount_percent * 100); ?>%</span>
                                                            </div>
                                                            <span class="strikethrough">
                                                                <?php echo number_format($product_item->{'product::retail_price'}); ?>
                                                                <?php echo getWording('index', 'baht') ?>
                                                            </span>
                                                        </div>
                                                    </span>
                                                    <span class="col-6 mt-5" style="color: red;">
                                                        <?php echo number_format($product_item->{'product::discount'}->total_price); ?>
                                                        <?php echo getWording('index', 'baht') ?>
                                                    </span>
                                                </div>
                                            </h4>
                                        <?php } ?>
                                        <div class="row mt-5">
                                            <?php if ($token != '') { ?>
                                                <div class="col-8 pr-1">
                                                    <button class="order_btn w-100" data-quantity="1" data-product-id="<?php echo $product_item->{'product::id'} ?>"><?php echo getWording('index', 'buy') ?></button>
                                                </div>
                                            <?php } else { ?>
                                                <div class="col-8 pr-1">
                                                    <a href="<?php echo base_url('Account/login') ?>"><button class="w-100" ? style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"><?php echo getWording('index', 'buy') ?></button></a>
                                                </div>
                                            <?php } ?>

                                            <div class="col-4 pl-1">
                                                <button id="test-btn" type="button" class="filter_btn w-100 p-0" data-compare="<?php echo $product_item->{'product::id'} ?>">
                                                    <svg class="w-100">
                                                        <use xlink:href="#icon-filter"></use>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <h5 class="mt-6">
                                            <b><?php echo getWording('index', 'interested_product') ?></b>
                                        </h5>
                                        <div class="row mt-5">
                                            <div class="col-8 pr-1">
                                                <a href="<?php echo base_url('Contact') ?>"><button class="w-100" ? style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"><?php echo getWording('index', 'contact') ?></button></a>
                                            </div>
                                            <div class="col-4 pl-1">
                                                <button id="test-btn" type="button" class="filter_btn w-100 p-0" data-compare="<?php echo $product_item->{'product::id'} ?>">
                                                    <svg class="w-100">
                                                        <use xlink:href="#icon-filter"></use>
                                                    </svg>
                                                </button>
                                            </div>

                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <h3 class="mt-5 text-center"><?php echo getWording('product', 'recommended') ?></h3>
                    <?php } ?>
                </div>
            </div>
            <hr class="mt-3">
            <h4 class="mt-4 mb-4" style="text-align: center;"><b><?php echo getWording('index', 'interesting_articles') ?></b></h4>
            <section class="blog">
                <div class="row">
                    <?php foreach ($block_limit as $key => $block_item) { ?>
                        <?php if ($block_item->tags == $block_detail->tags) { ?>
                            <div class="col-12 col-xl-6">
                                <a href="<?php echo base_url('Article/detail/' . $block_item->id) ?>">
                                    <div class="row my-3">
                                        <div class="col-12 col-xl-4">
                                            <img class="w-100" src="<?php echo base_url($block_item->thumbnail_path) ?>" alt="">
                                        </div>
                                        <div class="col-12 col-xl-8 align-self-center d-block pl-3">
                                            <?php
                                            $tag = array();
                                            $tag = explode(",", $block_item->tags);
                                            // print_r($tag)
                                            ?>
                                            <?php foreach ($tag as $key => $block_tag) { ?>
                                                <span>
                                                    <?php echo ($block_tag) ?>
                                                </span>
                                            <?php } ?>
                                            <h4 class="mt-2"><?php echo $block_item->title_th ?></h4>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </section>

        </div>
    </div>
    <div class="mt-5"> <?php include('footer.php') ?></div>
    <svg aria-hidden="true" style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="icon-filter" width="100%" viewBox="0 0 25 25">
                <g>
                    <path d="M1.75,7.75h6.6803589c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891   S13.9000854,9.0452271,14.2356567,7.75H24.25C24.6640625,7.75,25,7.4140625,25,7s-0.3359375-0.75-0.75-0.75H14.2356567   c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891S8.7659302,4.9547729,8.4303589,6.25H1.75   C1.3359375,6.25,1,6.5859375,1,7S1.3359375,7.75,1.75,7.75z M11.3330078,5.4912109   c0.8320313,0,1.5087891,0.6767578,1.5087891,1.5087891s-0.6767578,1.5087891-1.5087891,1.5087891S9.8242188,7.8320313,9.8242188,7   S10.5009766,5.4912109,11.3330078,5.4912109z" fill="#1D1D1B" />
                    <path d="M24.25,12.25h-1.6061401c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891   S17.1741333,10.9547729,16.838562,12.25H1.75C1.3359375,12.25,1,12.5859375,1,13s0.3359375,0.75,0.75,0.75h15.088562   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891s2.5670776-0.963562,2.9026489-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,12.25,24.25,12.25z M19.7412109,14.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891S21.25,12.1679688,21.25,13   S20.5732422,14.5087891,19.7412109,14.5087891z" fill="#1D1D1B" />
                    <path d="M24.25,18.25H9.7181396c-0.3355103-1.2952271-1.5037842-2.2587891-2.9017334-2.2587891   c-1.3987427,0-2.5670776,0.963562-2.9026489,2.2587891H1.75C1.3359375,18.25,1,18.5859375,1,19s0.3359375,0.75,0.75,0.75h2.1637573   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891c1.3979492,0,2.5662231-0.963562,2.9017334-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,18.25,24.25,18.25z M6.8164063,20.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891   c0.8310547,0,1.5078125,0.6767578,1.5078125,1.5087891S7.6474609,20.5087891,6.8164063,20.5087891z" fill="#1D1D1B" />
                </g>
            </symbol>
        </defs>
    </svg>


</body>

</html>

<script>
    function copy_link() {
        alert("copy link to clipboardx!");
    }

    $(document).ready(function() {
        $("#play-button").hide();
        $('.banner_slider').slick({
            dots: true,
            arrows: true,
            prevArrow: "<i class='slick-prev fas fa-chevron-left'></i>",
            nextArrow: "<i class='slick-next fas fa-chevron-right'></i>"
        });

        $('.product_heightlight_slider').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: "<i class='slick-prev fas fa-chevron-left'></i>",
            nextArrow: "<i class='slick-next fas fa-chevron-right'></i>"
        });

        $('.our_customer_slider').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            arrows: false
        });

    });

    $('.copy-link').on("click", function() {

        // evt.preventDefault();
        value = $(this).attr('href');
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(value).select();
        document.execCommand("copy");
        $temp.remove();
    });


    $('#play-button').click(function() {
        var mediaVideo = $("#my-video").get(0);
        if (mediaVideo.paused) {
            mediaVideo.play();
            $("#play-button").hide();
            $("#pause-button").fadeIn();
        }
    });

    $('#pause-button').click(function() {
        var mediaVideo = $("#my-video").get(0);
        if (mediaVideo.play) {
            mediaVideo.pause();
            $("#play-button").fadeIn();
            $("#pause-button").hide();
        }
    });

    $('#image_banner').click(function() {
        $('#exampleModal').modal('show');
    });

    $('#spinner-form2').click(function() {
        if ($("#spinner-form2").prop('checked') == true) {

            $(".secd-menu").addClass("active-secd-menu");
        } else {

            $(".secd-menu").removeClass("active-secd-menu");
        }
    });
</script>
<script>
    // $(document).ready(function() {
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    var fbButton = document.getElementById('fb-share-button');
    var url = window.location.href;


    fbButton.addEventListener('click', function() {
        window.open('https://www.facebook.com/sharer/sharer.php?u=' + url,
            'facebook-share-dialog',
            'width=800,height=600'
        );
        return false;
    });
    // });
</script>


<script>

</script>
<script type="text/javascript">
    LineIt.loadButton();
</script>