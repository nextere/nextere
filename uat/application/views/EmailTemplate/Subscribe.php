<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Choose a new password for <?php echo $this->config->item('WEBSITE_NAME');?></title>
    <style type="text/css" rel="stylesheet" media="all">
        /* Base ------------------------------ */
        *:not(br):not(tr):not(html) {
            font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }
        body {
            width: 100% !important;
            height: 100%;
            margin: 0;
            line-height: 1.4;
            background-color: #F2F4F6;
            color: #74787E;
            -webkit-text-size-adjust: none;
        }
        a {
            color: #3869D4;
        }
        /* Layout ------------------------------ */
        .email-wrapper {
            width: 100%;
            margin: 0;
            padding: 0;
            background-color: #F2F4F6;
        }
        .email-content {
            width: 100%;
            margin: 0;
            padding: 0;
        }
        /* Masthead ----------------------- */
        .email-masthead {
            padding: 25px 0;
            text-align: center;
        }
        .email-masthead_logo {
            max-width: 400px;
            border: 0;
        }
        .email-masthead_name {
            font-size: 16px;
            font-weight: bold;
            color: #bbbfc3;
            text-decoration: none;
            text-shadow: 0 1px 0 white;
        }
        /* Body ------------------------------ */
        .email-body {
            width: 100%;
            margin: 0;
            padding: 0;
            border-top: 1px solid #EDEFF2;
            border-bottom: 1px solid #EDEFF2;
            background-color: #FFF;
        }
        .email-body_inner {
            width: 570px;
            margin: 0 auto;
            padding: 0;
        }
        .email-footer {
            width: 570px;
            margin: 0 auto;
            padding: 0;
            text-align: center;
        }
        .email-footer p {
            color: #AEAEAE;
        }
        .body-action {
            width: 100%;
            margin: 30px auto;
            padding: 0;
            text-align: center;
        }
        .body-sub {
            margin-top: 25px;
            padding-top: 25px;
            border-top: 1px solid #EDEFF2;
        }
        .content-cell {
            padding: 35px;
        }
        .align-right {
            text-align: right;
        }
        /* Type ------------------------------ */
        h1 {
            margin-top: 0;
            color: #2F3133;
            font-size: 19px;
            font-weight: bold;
            text-align: left;
        }
        h2 {
            margin-top: 0;
            color: #2F3133;
            font-size: 16px;
            font-weight: bold;
            text-align: left;
        }
        h3 {
            margin-top: 0;
            color: #2F3133;
            font-size: 14px;
            font-weight: bold;
            text-align: left;
        }
        p {
            margin-top: 0;
            color: #74787E;
            font-size: 16px;
            line-height: 1.5em;
            text-align: left;
        }
        p.sub {
            font-size: 12px;
        }
        p.center {
            text-align: center;
        }
        /* Buttons ------------------------------ */
        .button {
            display: inline-block;
            width: 200px;
            background-color: #3869D4;
            border-radius: 3px;
            color: #ffffff;
            font-size: 15px;
            line-height: 45px;
            text-align: center;
            text-decoration: none;
            -webkit-text-size-adjust: none;
            mso-hide: all;
        }
        .button--red {
            background-color: #32c5d2;
        }
        /*Media Queries ------------------------------ */
        @media only screen and (max-width: 600px) {
            .email-body_inner,
            .email-footer {
                width: 100% !important;
            }
        }
        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
</head>
<body>
<table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table class="email-content" width="100%" cellpadding="0" cellspacing="0">
                <!-- Logo -->
                <tr>
                    <td class="email-masthead">
                        <table style="margin: 0 auto;">
                            <tr>
                                <td>
                                    
                                </td>
                                <td>
                                    <h1><a class="email-masthead_name" ><?php echo 'Nextere';?></a></h1>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- Email Body -->
                <tr>
                    <td class="email-body" width="100%">
                        <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
                            <!-- Body content -->
                            <tr>
                                <td class="content-cell">
                                    <h2>สวัสดี ,</h2><!-- 
                                    <p>You recently requested to reset your password for your <?php // echo $administrator_user->username;?> account. Click the button below to reset it.</p> -->
                                    <!-- Action -->
                                    <table class="body-action" align="center" width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="center">
                                                <div>
                                                    <p> ขอขอบคุณที่เลือกรับข่าวสารจาก <a href="<?php echo base_url();?>"><?php echo base_url();?></a>
                                                        <br> คุณจะได้รับข่าวสารและข้อมูลโปรโมชั่นสินค้าก่อนใคร โปรดติดตาม</p>
                                                    <p> ขอบคุณค่ะ</p>
                                                    <!-- <p> <a href="<?php echo base_url('member/verify').'/'.$member->token;?>"><?php echo base_url("member/verify/").$member->token;?></a></p> -->
                                                    <p> หากคุณต้องการยกเลิกการติดตามข่าวสารและโปรโมชั่นสุดพิเศษของเรา กดยกเลิกได้ <a href="<?php echo base_url('api_area/home/unSubscribe?code=').$subscribe->token;?>"> ที่นี่ </a></p>
                                                    <!-- <p> คุณสามารถติดตามข่าวสาร และโปรโมชั่นของ Nextere ได้ที่ Facebook:Nextere.store หรือ subscribe เพื่อรับอีเมลข่าวสารก่อนใครได้ที่ <?php echo base_url();?></p> -->
                                                    <p> ขอบคุณค่ะ <br> Nextere Team</p>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                  <!--   <p>If you did not request a password reset, please ignore this email or reply to let us know. This password reset is only valid for the next 30 minutes.</p> -->
                                    <!-- <p>Thanks,<br><?php // echo $this->config->item('WEBSITE_OWNER');?></p> -->
                                   <!--  <p><strong>P.S.</strong> We also love hearing from you and helping you with any issues you have. Please reply to this email if you want to ask a question or just say hi.</p> -->
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="content-cell">
                                    <p class="sub center">&copy; <?php echo date("Y");?> <?php echo $this->config->item('WEBSITE_NAME');?> . All rights reserved.</p>
                                    <p class="sub center">
                                        <?php echo $this->config->item('WEBSITE_NAME');?>
                                        <br><?php echo $this->config->item('WEBSITE_ADDRESS');?>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
