<?php include('header.php') ?>


<style>
    .delivery ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 100%;
        background-color: white;
        border: 1px solid #BCBCBC;
        opacity: 1;
        font-size: 16px;
    }

    .delivery li a.active {
        background-color: #BCBCBC;
        color: white;
    }

    .delivery li a {
        display: block;
        color: #000;
        padding: 8px 12px;
        text-decoration: none;
        font-size: 14px;
    }

    input[type=text],
    select,
    textarea {
        padding: 12px;
        margin-top: 6px;
        margin-bottom: 16px;
        width: 100%;
        height: 45px;
        border: 1px solid #707070;
        border-radius: 5px;
    }

    input[type=tel],
    select,
    textarea {
        padding: 12px;
        margin-top: 6px;
        margin-bottom: 16px;
        width: 100%;
        height: 45px;
        border: 1px solid #707070;
        border-radius: 5px;
    }

    /* .form-control {
        width: 100%;
        height: 45px;
        border: 1px solid #707070;
        border-radius: 5px;
    } */

    .delivery .select2 {
        padding: 12px;
        margin-top: 6px;
        margin-bottom: 16px;
        width: 100%;
        height: 45px;
        border: 1px solid #707070;
        border-radius: 5px;
    }
    .delivery .select2-container--default .select2-selection--single .select2-selection__clear {
        display: none;
    }
</style>




<body>
    <section class="delivery">
        <div class="container mt-5">
            <div class="row">
                <div class="col-sm-12 col-xl-12 pt-5 pb-5 pl-5 mt-5" style="background-color: lightgray; text-align:left">
                    <h4><b><?php echo getWording('profile', 'my_account') ?></b></h4>
                    <p><?php echo getWording('profile', 'hi') ?> <?php echo $user_info->fullname ?>, <?php echo getWording('profile', 'detail') ?></p>
                </div>
                <div class="col-sm-4 col-xl-3 mt-3 pl-0">
                    <ul>
                        <li><a href="<?php echo base_url('Account/profile') ?>"><i class="far fa-user pr-2"></i><?php echo getWording('profile', 'profile') ?></a></li>
                        <li><a href="<?php echo base_url('Account/order_history') ?>"><i class="fas fa-shopping-cart pr-2"></i><?php echo getWording('profile', 'my_orders') ?></a></li>
                        <li><a href="<?php echo base_url('Account/quotation_request_history') ?>"><i class="fas fa-file-alt pr-2"></i><?php echo getWording('profile', 'quotation') ?></a></li>
                        <li><a class="active" href="<?php echo base_url('Account/sending_address') ?>"><i class="fas fa-truck pr-2"></i><?php echo getWording('profile', 'shipping_address') ?></a></li>
                        <li><a href="<?php echo base_url('Account/tax_invoice_address') ?>"><i class="fas fa-map-marked-alt pr-2"></i><?php echo getWording('profile', 'tax_invoice') ?></a></li>
                        <!-- <li><a href="<?php echo base_url('Account/payment') ?>"><i class="far fa-credit-card pr-2"></i><?php echo getWording('profile', 'credit') ?></a></li> -->
                        <li><a href="<?php echo base_url('Account/datasheet') ?>"><i class="fas fa-file-alt pr-2"></i><?php echo getWording('profile', 'datasheet') ?></a></li>
                    </ul>
                </div>
                <div class="col-sm-8 col-xl-9 mt-3">

                    <div class="head">
                        <h3 style="font-weight: bold"><?php echo getWording('profile', 'add_address') ?></h3>
                    </div>
                    <hr style="border-width: 3px;">
                    <form id="dataForm">
                        <div class="row">
                            <div class="col-xl-6 name pt-5">
                                <b><?php echo getWording('profile', 'name') ?> *</b>
                                <input type="text" id="name" name="name" placeholder="<?php echo getWording('profile', 'name') ?>" required>
                            </div>

                            <div class="col-xl-6 lastname pt-5">
                                <b><?php echo getWording('profile', 'lastname') ?> *</b>
                                <input type="text" id="lastname" name="lastname" placeholder="<?php echo getWording('profile', 'lastname') ?>" required>
                            </div>

                            <div class="col-xl-6 phone pt-5">
                                <b><?php echo getWording('profile', 'phone') ?> *</b>
                                <input type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" id="telephone" name="telephone" placeholder="<?php echo getWording('profile', 'phone') ?>" required>
                            </div>

                            <div class="col-xl-6 email pt-5">
                                <b><?php echo getWording('profile', 'email') ?> *</b>
                                <input type="text" id="email" name="email" placeholder="<?php echo getWording('profile', 'email') ?>" required>
                            </div>

                            <div class="col-xl-6 company pt-5">
                                <b><?php echo getWording('profile', 'company') ?></b>
                                <input type="text" id="company" name="company" placeholder="<?php echo getWording('profile', 'company') ?>">
                            </div>

                            <div class="col-xl-6 branch pt-5">
                                <b><?php echo getWording('profile', 'branch') ?></b>
                                <input type="text" id="branch" name="branch" placeholder="<?php echo getWording('profile', 'branch') ?>">
                            </div>

                            <div class="col-xl-6 address pt-5">
                                <b><?php echo getWording('profile', 'address') ?> *</b>
                                <input type="text" id="address" name="address" placeholder="<?php echo getWording('profile', 'address') ?>">
                            </div>

                            <div class="col-xl-6 province pt-5">
                                <div class="form-group">
                                    <b><?php echo getWording('profile', 'province') ?> *</b>
                                    <!-- <input type="text" id="province" name="province" placeholder="จังหวัด"> -->

                                    <select class="form-control select select2" id="province" name="province">
                                        <?php
                                        if (isset($address_province->id)) {
                                        ?>
                                            <option value="<?php echo $address_province->id ?>" selected><?php echo $address_province->name ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xl-6 district pt-5">
                                <div class="form-group">
                                    <b><?php echo getWording('profile', 'district') ?> *</b>
                                    <!-- <input type="text" id="district" name="district" placeholder="อำเภอ/เขต"> -->
                                    <select class="form-control select select2" id="district" name="district">
                                        <?php
                                        if (isset($address_district->id)) {
                                        ?>
                                            <option value="<?php echo $address_district->id; ?>" selected><?php echo $address_district->name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xl-6 sub-district mt-5">
                                <div class="form-group">
                                    <b><?php echo getWording('profile', 'sub_district') ?> *</b>
                                    <!-- <input type="text" id="subdistrict" name="subdistrict" placeholder="ตำบล/แขวง"> -->

                                    <select class="form-control select select2" id="sub_district" name="sub_district">
                                        <?php
                                        if (isset($address_sub_district->id)) {
                                        ?>
                                            <option value="<?php echo $address_sub_district->id; ?>" selected><?php echo $address_sub_district->name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xl-6 postcode mt-5">
                                <b><?php echo getWording('profile', 'postcode') ?> *</b>
                                <input type="text" id="postcode" name="postcode" value="<?php echo isset($postcode) ?>" placeholder="<?php echo getWording('profile', 'postcode') ?>">
                            </div>

                            <div class="pt-1">
                                <label class="form-check-label"><input type="checkbox" id="is_default" value="0"> <?php echo getWording('profile', 'set_address') ?></label>
                            </div>


                            <div class="button4 pt-4" style="text-align: center;">
                                <div class="submit pt-4" style="text-align: center; padding-bottom: 40px;">
                                    <input id="submit-add" type="submit" value="<?php echo getWording('profile', 'save') ?>" style="width: 208px; height:45px; border:solid 0px; border-radius: 23px; padding: 9px; margin:10px; background-color:#AAAAAA; color:#FFFFFF;">
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>

        
        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content modal-content-style">
                    <div class="modal-header modal-header-style">

                    </div>
                    <div class="modal-body">
                        <div class="btn-close-modal">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <h3 class="add_response" style="text-align: center;"></h3>
                    </div>
                    <div class="modal-footer modal-footer-style edit_response_btn">
                        <button type="button" class="btn  register_success d-none" style="width: 142px;height: 40px; background-color: #AAAAAA; border-radius: 23px; padding: 10px; margin:10px;color:white;" onclick="location.href='<?php echo base_url() ?>'">Understood</button>
                        <button type="button" class="btn register_fail d-none" style="width: 142px;height: 40px; background-color: #AAAAAA; border-radius: 23px; padding: 10px; margin:10px;color:white;" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <script>
            // function isNumber(evt) {
            //     evt = (evt) ? evt : window.event;
            //     var charCode = (evt.which) ? evt.which : evt.keyCode;
            //     if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            //         return false;
            //     }

            //     return true;
            // }

            var url = "<?php echo $api_url; ?>";
            // console.log(url)

            $("#dataForm select[name='province']").select2({
                placeholder: "",
                width: null,
                allowClear: true,
                ajax: {
                    url: url + "api_area/home/select2/address_province",
                    dataType: 'json',
                    delay: 250,
                    cache: true,
                    data: function(params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data.data, function(obj) {
                                return {
                                    id: obj.id,
                                    text: obj.name
                                };
                            })
                        };
                    }
                }
            });

            $("#dataForm select[name='district']").select2({
                placeholder: "",
                width: null,
                allowClear: true,
                ajax: {
                    url: function() {
                        var query_object = {};
                        if ($("#dataForm select[name='province']").val() != null) {
                            query_object.address_province_id = $("#dataForm select[name='province']").val();
                        }
                        var query_string = jQuery.param(query_object);
                        return url + "api_area/home/select2/address_district/" + $("#dataForm select[name='province']").val();
                    },
                    dataType: 'json',
                    delay: 250,
                    cache: true,
                    data: function(params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data.data, function(obj) {
                                return {
                                    id: obj.id,
                                    text: obj.name
                                };
                            })
                        };
                    }
                }
            });

            $("#dataForm select[name='sub_district']").select2({
                placeholder: "",
                width: null,
                allowClear: true,
                ajax: {
                    url: function() {
                        var query_object = {};
                        if ($("#dataForm select[name='province']").val() != null) {
                            query_object.address_province_id = $("#dataForm select[name='province']").val();
                        }
                        if ($("#dataForm select[name='district']").val() != null) {
                            query_object.address_district_id = $("#dataForm select[name='district']").val();
                        }
                        var query_string = jQuery.param(query_object);
                        return url + "api_area/home/select2/address_sub_district/" + $("#dataForm select[name='district']").val();
                        // console.log('query_string');
                    },

                    dataType: 'json',
                    delay: 250,
                    cache: true,
                    data: function(params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data.data, function(obj) {
                                return {
                                    id: obj.id,
                                    text: obj.name,
                                };
                            })
                        };
                    }
                }
            });

            $("#dataForm select[name='province']").on("change", function() {
                $("#dataForm select[name='district']").select2("val", "");
                $("#dataForm select[name='sub_district']").select2("val", "");
            });

            $("#dataForm select[name='district']").on("change", function() {
                $("#dataForm select[name='sub_district']").select2("val", "");
            });

            $("#dataForm select[name='district']").on("change", function() {
                var data = {
                    "province_id": $("#dataForm select[name='province']").val(),
                    "district_name": $("#select2-district-container").attr('title'),
                }

                var send_data = {
                    "url": "<?php echo base_url('Account/get_postcode') ?>",
                    "method": "POST",
                    "data": data
                }

                $.ajax(send_data).done(function(response) {
                    // console.log(response);
                    $('#postcode').val(response.data.postcode);
                });
            });

            $(document).ready(function() {
                $('#telephone').mask('000-000-0000');
            });

            $("#dataForm").submit(function(e) {
                e.preventDefault();
                return false;
            });

            $('#is_default').change(function(e) {
                if (this.checked) {
                    $('#is_default').prop('checked', true)
                    $('#is_default').val('1');
                } else {
                    $('#is_default').prop('checked', false)
                    $('#is_default').val('0');
                }
            });

            $("#submit-add").click(function() {
                // console.log($('#name').val());

                $("#dataForm").validate({
                    rules: {
                        name: "required",
                        lastname: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        telephone: {
                            required: true,
                            pattern: '\\d{3}-\\d{3}-\\d{4}',
                            minlength: 12,
                            maxlength: 12
                        },
                        address: "required",
                        province: "required",
                        district: "required",
                        sub_district: "required",
                        postcode: "required",
                    },
                    messages: {
                        name: "Please enter your name",
                        lastname: "Please enter your lastname",
                        email: "Please enter a valid email address",
                        telephone: "Please correct telephone format",
                        address: "Please enter your address",
                        province: "Please enter your province",
                        district: "Please enter your district",
                        sub_district: "Please enter your sub-district",
                        postcode: "Please enter your postcode",
                    },

                    errorPlacement: function(error, element) {
                        error.insertBefore(element);
                    },
                    submitHandler: function() {
                        // console.log('validated');
                        var data = {
                            "name": $("#name").val(),
                            "lastname": $("#lastname").val(),
                            "telephone": $("#telephone").val(),
                            "email": $("#email").val(),
                            "company": $("#company").val(),
                            "branch": $("#branch").val(),
                            "address": $("#address").val(),
                            "province": $("#province").val(),
                            "district": $("#district").val(),
                            "sub_district": $("#sub_district").val(),
                            "postcode": $("#postcode").val(),
                            "is_default": $("#is_default").val(),
                        }

                        var send_data = {
                            "url": "<?php echo base_url('Account/sending_address_add') ?>",
                            "method": "POST",
                            "data": data
                        }

                        $.ajax(send_data).done(function(response) {
                            if (response.code == "0x0000-00000") {
                                window.location.href = "<?php echo base_url('Account/sending_address') ?>";
                                $('.add_response').text(response.message);
                            } else {
                                $('.add_response').addClass('d-none');
                                // $('#staticBackdrop').modal('show');
                                $('.add_response').removeClass('d-block');
                                $('.edit_fail').addClass('d-block');
                                $('.edit_fail').removeClass('d-none');
                            }
                            $('#staticBackdrop').modal('show');
                        });
                    }
                });
            });
        </script>
    </section>

    <div class="mt-5">
        <?php include('footer.php') ?>
    </div>
</body>