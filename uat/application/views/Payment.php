<?php include('header.php') ?>
<?php
$product_total_retail_price = 0;
$product_total_delivery_price = 0;
$product_total_price_all = 0;
$delivery_discount = 0;
$product_delivery_total_price = 0;
$total_price_all = 0;

$member_id = $codeigniter_instance->session->userdata('member::id');
// print_r($member_id);

if (!empty($product_list['product_id'])) {
    foreach ($product_list['delivery_price'] as $key => $product_delivery_price) {
        $product_delivery_total_price += (int)$product_delivery_price;
    }

    // echo $product_total_delivery_price;
    if (!empty($product_list['coupon_discount_delivery'])) {
        $delivery_discount = $product_delivery_total_price - $product_list['coupon_discount_delivery'];
        if ($delivery_discount <= 0) {
            $delivery_discount = 0;
        }
    } else {
        $delivery_discount = $product_delivery_total_price;
    }

    // product_total_price_all
    foreach ($product_list['product_total_price'] as $key => $product_total_price) {
        $total_price_all += (int)$product_total_price;
    }
    if (!empty($product_list['coupon_discount_onlynormalprice'])) {
        $product_total_price_all = $total_price_all - $product_list['coupon_discount_onlynormalprice'];
        if ($product_total_price_all <= 0) {
            $product_total_price_all = 0;
        }
    } else {
        $product_total_price_all = $total_price_all;
    }
} else {
    redirect(base_url('Product/shopping-cart'));
}


?>

<!-- <span><?php // echo number_format($delivery_discount) 
            ?> <?php // echo number_format($product_delivery_total_price) 
                ?> <?php // echo number_format($product_list['coupon_discount_delivery']) 
                    ?></span> -->

<style>
    .btn1 {
        width: 252px;
        height: 45px;
        background: #D3D3D3;
        color: #000000;
        border-radius: 23px;
        border: 0px;
        font-weight: bold;
    }

    .bg-accordion {
        background-color: #7C7C7C !important;
    }

    input[type=text],
    select,
    textarea {
        padding: 12px;
        width: 100%;
        height: 45px;
        background: #FFFFFF;
        border: 1px solid #707070;
        border-radius: 5px;
    }


    input[type=tel],
    select,
    textarea {
        padding: 12px;
        width: 100%;
        height: 45px;
        background: #FFFFFF;
        border: 1px solid #707070;
        border-radius: 5px;
    }

    .pay .accordion-button {
        padding-right: 30px;
    }

    .pay .accordion-button:not(.collapsed)::after {
        background-image: none !important;
        transform: none !important;
        content: none !important;
    }

    .pay .accordion-button::after {
        content: '<?php echo getWording('profile', 'edit') ?>';
        transform: none;
    }

    .pay .input-tax-invoice label {
        margin-bottom: .3rem;
    }

    .pay .accordion-button::after {
        background-image: none !important;
    }

    .payment_modal .select2 {
        padding: 12px !important;
        /* margin-top: 6px; */
        margin-bottom: 16px !important;
        width: 100% !important;
        height: 45px !important;
        border: 1px solid #707070 !important;
        border-radius: 5px !important;
    }

    .select2-dropdown {
        z-index: 1061 !important;
    }

    .select2-search__field {
        border-top-right-radius: 25px;
        border-top-left-radius: 25px;
    }

    .select_address.active {
        border: 1px solid gray !important;
    }

    .payment_modal .select2-container--default .select2-selection--single .select2-selection__clear {
        display: none;
    }
</style>

<section class="pay mb-5">
    <div class="container">
        <div class="head27 pt-5">
            <h2><b><?php echo getWording('payment', 'pay') ?></b></h2>
        </div>
        <div class="row">
            <div class="col-sm-8 col-xl-9" style="margin-bottom: auto; padding-bottom: auto;">

                <div class="accordion" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header " id="headingOne">
                            <button class="accordion-button bg-accordion text-light" style="background-color: #7C7C7C !important;" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                1. <?php echo getWording('payment', 'shipping') ?>
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <div class="d-block by_address">
                                    <input class="form-check-input mt-0" type="radio" name="shipping" checked="true" value="" id="by_address">
                                    <label for="#by_address" class="ml-3"><?php echo getWording('payment', 'delivery') ?></label>
                                </div>
                                <div class="d-block by_yourself">
                                    <!-- <input class="form-check-input mt-0" type="radio" name="shipping" value="" id="by_yourself" onclick="location.href='<?php // echo base_url('Payment/Not_delivery') 
                                                                                                                                                                ?>'"> -->
                                    <input class="form-check-input mt-0" type="radio" name="shipping" value="" id="by_yourself" data-bs-toggle="modal" data-bs-target="#myModal9">
                                    <label for="#by_yourself" class="ml-3"><?php echo getWording('payment', 'pick') ?></label>
                                </div>
                                <div class="d-flex justify-content-center">
                                    <button class="btn1" data-bs-toggle="collapse" data-bs-target="#collapseTwo"><?php echo getWording('product', 'next') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item mt-2">
                        <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button bg-accordion text-light collapsed" style="background-color: #7C7C7C !important;" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                2. <?php echo getWording('payment', 'company') ?>
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <!-- <div class="d-block">
                                    <input class="form-check-input mt-0" type="radio" name="delivery" checked="true" value="" id="kerry">
                                    <label for="#kerry" class="ml-3">Kerry Express</label>
                                </div> -->
                                <!-- <div class="d-block">
                                    <input class="form-check-input mt-0" type="radio" name="delivery" checked="true" value="FLE" id="flash_express" data-delivery-name="FlashExpress"><label for="flash_express" class="ml-3">Flash Express</label>
                                    <input class="form-check-input mt-0" type="radio" name="delivery" checked="true" value="NJV" id="ninja_van" data-delivery-name="NinjaVan"><label for="#ninja_van" class="ml-3">Ninja Van</label>
                                </div> -->

                                <div class="radio-delivery">
                                    <input class="delivery_type flash_express mr-2" type="radio" name="delivery" checked="true" value="FLE" id="flash_express" data-delivery-name="FlashExpress"><label for="flash_express">Flash Express</label>
                                    <br>
                                    <input class="delivery_type ninja_van mr-2 mt-2" type="radio" name="delivery" value="NJV" id="ninja_van" data-delivery-name="NinjaVan"><label for="ninja_van">Ninja Van</label>
                                    <br>
                                    <input class="delivery_type SCG mr-2 mt-2" type="radio" name="delivery" value="SCG" id="SCG" data-delivery-name="SCG"><label for="SCG">SCG</label>
                                </div>

                                <div class="d-flex justify-content-center">
                                    <button class="btn1" data-bs-toggle="collapse" data-bs-target="#collapseThree"><?php echo getWording('product', 'next') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item mt-2">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button bg-accordion text-light collapsed" style="background-color: #7C7C7C !important;" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                3. <?php echo getWording('payment', 'shipping_tax_info') ?>
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div class="accordion-body p-0">
                                <div class="address_list p-3">
                                    <h5><b><?php echo getWording('payment', 'shipping_info') ?></b></h5>
                                    <div class="address_list_detail p-3">
                                        <div>
                                            <?php if (empty($address)) { ?>
                                                <h3 style="margin-left: 30px;"><?php echo getWording('payment', 'please_add_address') ?></h3>
                                            <?php } else { ?>
                                                <div class="row" id="address_list" style="padding-bottom: 20px;">
                                                    <div class="address mt-2" id="shipping_info" style="width: 100%; height: auto; border: solid #DBDBDB;">
                                                        <div class="row address_main p-5">

                                                            <div class="col-xl-5">

                                                                <h5 class="address_name"><?php echo $address->first_name ?> <?php echo $address->last_name ?></h5>

                                                                <p class="address_address pt-2"><b><?php echo getWording('profile', 'address') ?> :</b> <?php echo $address->address ?></p>

                                                                <p class="address_district"><b><?php echo getWording('profile', 'sub_district') ?> :</b> <?php echo $address->sub_district->name ?> <b><?php echo getWording('profile', 'district') ?> :</b> <?php echo $address->district->name ?></p>

                                                                <p class="address_province"><b><?php echo getWording('profile', 'province') ?> :</b> <?php echo $address->province->name ?> <?php echo $address->postcode ?></p>

                                                                <?php if (empty($address->company_name)) { ?>
                                                                    <p class="address_company"></p>
                                                                <?php } else { ?>
                                                                    <p class="address_company"><b><?php echo getWording('profile', 'company') ?> :</b> <?php echo $address->company_name ?></p>
                                                                <?php } ?>

                                                                <?php if (empty($address->company_branch)) { ?>
                                                                    <p class="address_branch"></p>
                                                                <?php } else { ?>
                                                                    <p class="address_branch"><b><?php echo getWording('profile', 'branch') ?> :</b> <?php echo $address->company_branch ?></p>
                                                                <?php } ?>

                                                                <p class="address_email"><b><?php echo getWording('profile', 'email') ?> :</b> <?php echo $address->email ?></p>

                                                                <p class="address_phone"><b><?php echo getWording('profile', 'phone') ?> :</b> <?php echo $address->telephone ?></p>
                                                            </div>

                                                            <div class="col-xl-5"></div>
                                                            <div class="col-xl-2" style="text-align: right;">
                                                                <button id="edit_main_address" style="background-color: white; border: none;">
                                                                    <?php echo getWording('profile', 'edit') ?>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <input class="address_id" name="address_id" id="main_address_id" value="<?php echo $address->id ?>" hidden>

                                                        <?php if ($address->is_default == '1') { ?>
                                                            <div id="is_default_btn" class="default_address row p-4">
                                                                <div style="width: 123px; height: 34px; background: #E6E5E5; border-radius: 3px; border:0px; padding:3px; text-align: center;">
                                                                    <div style="padding-top: 4.5px;"><?php echo getWording('profile', 'main_address') ?></div>
                                                                </div>
                                                            </div>
                                                        <?php } else {
                                                            echo null;
                                                        } ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="row" id="button_address_list">
                                        <div class="col-sm-12 col-xl-3 mt-3 ">
                                        </div>
                                        <?php if ($address != null) { ?>
                                            <div class="col-sm-12 col-xl-3 mt-3 text-center">
                                                <button data-bs-toggle="modal" data-bs-target="#myModal1" style="background-color:#FFFFFF; border: 1px solid #555555; border-radius: 23px; width:200px; height:45px;">
                                                    <b><?php echo getWording('payment', 'choose_address') ?></b>
                                                </button>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-sm-12 col-xl-1 mt-3 " style="margin-right: 40px;">
                                            </div>
                                        <?php } ?>
                                        <div class="col-sm-12 col-xl-3 mt-3 text-center">
                                            <button data-bs-toggle="modal" data-bs-target="#myModal2" style="background-color:#FFFFFF; border: 1px solid #555555; border-radius: 23px; width:200px; height:45px;">
                                                <b>+ <?php echo getWording('payment', 'add_new_address') ?></b>
                                            </button>
                                        </div>
                                        <div class="col-sm-12 col-xl-3 mt-3 ">
                                        </div>
                                    </div>
                                </div>
                                <div class="border-top border-bottom p-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="need_tax_invoice">
                                        <label class="form-check-label" for="need_tax_invoice">
                                            <?php echo getWording('payment', 'need_tax_invoice') ?>
                                        </label>
                                        <div class="tax-invoive-info d-none">
                                            <div class="bg-light p-3">
                                                <div class="form-check">
                                                    <!-- <input class="form-check-input" type="checkbox" value="" id="same_deliver_address">
                                                    <label class="form-check-label" for="same_deliver_address">
                                                        <?php // echo getWording('payment', 'same_address') 
                                                        ?>
                                                    </label> -->
                                                    <div class="address-tax d-block">
                                                        <?php if (empty($tax_address)) { ?>
                                                            <h3><?php echo getWording('payment', 'please_add_address_tax') ?></h3>
                                                        <?php } else { ?>
                                                            <div class="row pr-5">
                                                                <div class="taxaddress mt-2" style="width: 100%; height: auto; border: solid #DBDBDB;">
                                                                    <div class="row tax_address_main p-5" id="tax_info">

                                                                        <div class="col-xl-5">
                                                                            <H5 class="tax_address_name"><?php echo $tax_address->first_name ?> <?php echo $tax_address->last_name ?></H5>

                                                                            <p class="tax_address_address pt-2"><b><?php echo getWording('profile', 'address') ?> :</b> <?php echo $tax_address->address ?></p>

                                                                            <p class="tax_address_district"><b><?php echo getWording('profile', 'sub_district') ?> :</b> <?php echo $tax_address->sub_district->name ?> <b><?php echo getWording('profile', 'district') ?> :</b> <?php echo $tax_address->district->name ?></p>

                                                                            <p class="tax_address_province"><b><?php echo getWording('profile', 'province') ?> :</b> <?php echo $tax_address->province->name ?> <?php echo $tax_address->postcode ?></p>

                                                                            <?php if (empty($tax_address->company_name)) { ?>
                                                                                <?php echo null ?>
                                                                            <?php } else { ?>
                                                                                <p class="tax_address_company"><b><?php echo getWording('profile', 'company') ?> :</b> <?php echo $tax_address->company_name ?></p>
                                                                            <?php } ?>

                                                                            <?php if (empty($tax_address->company_branch)) { ?>
                                                                                <?php echo null ?>
                                                                            <?php } else { ?>
                                                                                <p class="tax_address_branch"><b><?php echo getWording('profile', 'branch') ?> :</b> <?php echo $tax_address->company_branch ?></p>
                                                                            <?php } ?>

                                                                            <p class="tax_address_email"><b><?php echo getWording('profile', 'email') ?> :</b> <?php echo $tax_address->email ?></p>

                                                                            <p class="tax_address_phone"><b><?php echo getWording('profile', 'phone') ?> :</b> <?php echo $tax_address->telephone ?></p>

                                                                            <p class="tax_address_no"><b><?php echo getWording('profile', 'tax') ?> :</b> <?php echo $tax_address->tax_id ?></p>

                                                                        </div>
                                                                        <div class="col-xl-5"></div>
                                                                        <div class="col-xl-2" style="text-align: right;">
                                                                            <button id="edit_address" class="bg-light" style="border: none;" data-bs-toggle="modal" data-bs-target="#myModal7">
                                                                                <?php echo getWording('profile', 'edit') ?>
                                                                            </button>
                                                                        </div>
                                                                        <?php if (($tax_address->is_default) == '1') { ?>
                                                                            <div class="default_tax_address row p-4">
                                                                                <div style="width: 123px; height: 34px; background: #E6E5E5; border-radius: 3px; border:0px; padding:3px; text-align: center;">
                                                                                    <div style="padding-top: 4.5px;"><?php echo getWording('profile', 'main_address') ?></div>
                                                                                </div>
                                                                            </div>
                                                                        <?php } else {
                                                                            echo null;
                                                                        }
                                                                        ?>
                                                                        <input class="address_tax_id" name="address_tax_id" value="<?php echo $tax_address->id ?>" hidden>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 col-xl-3 mt-3 ">
                                                </div>
                                                <?php if ($tax_address != null) { ?>
                                                    <div class="col-sm-12 col-xl-3 mt-3 text-center">
                                                        <button data-bs-toggle="modal" data-bs-target="#myModal5" style="background-color:#FFFFFF; border: 1px solid #555555; border-radius: 23px; width:200px; height:45px;">
                                                            <b><?php echo getWording('payment', 'choose_address') ?></b>
                                                        </button>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="col-sm-12 col-xl-1 mr-7 mt-3 ">
                                                    </div>
                                                <?php } ?>
                                                <div class="col-sm-12 col-xl-3 mt-3 text-center">
                                                    <button data-bs-toggle="modal" data-bs-target="#myModal6" style="background-color:#FFFFFF; border: 1px solid #555555; border-radius: 23px; width:200px; height:45px;">
                                                        <b>+ <?php echo getWording('payment', 'add_new_address') ?></b>
                                                    </button>
                                                </div>
                                                <div class="col-sm-12 col-xl-3 mt-3 ">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn1 my-3" data-bs-toggle="collapse" data-bs-target="#collapseFour"><?php echo getWording('product', 'next') ?></button>
                            </div>
                        </div>
                    </div>


                    <div class="accordion-item mt-2">
                        <h2 class="accordion-header" id="headingFour">
                            <button class="accordion-button bg-accordion text-light collapsed" style="background-color: #7C7C7C !important;" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                4. <?php echo getWording('payment', 'payment_method') ?>
                            </button>
                        </h2>
                        <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <div class="radio-payment" style="text-align: left;">

                                    <input class="payment_type credit mr-2" type="radio" name="payment_id" value="1" id="credit"><label for="credit"><?php echo getWording('index', 'credit') ?></label>
                                    <div class="mt-5">
                                        <h5><?php echo getWording('payment', 'mplay') ?></h5>
                                    </div>

                                    <!-- <div class="form-check"> -->
                                    <input class="payment_type internet_banking mr-1 mt-2" type="radio" name="payment_id" value="2" id="internet_banking">
                                    <label for="internet_banking">
                                        Internet Banking
                                    </label>
                                    <div class="bank d-none">
                                        <div class="bg-light p-3">
                                            <div class="form-check">
                                                <input class="mr-2 mt-2" type="radio" name="bank_id" value="014" id="scb"><label for="scb">ธนาคารไทยพาณิชย์ (SCB)</label><br>
                                                <input class="mr-2 mt-2" type="radio" name="bank_id" value="002" id="bbl"><label for="bbl">ธนาคารกรุงเทพ (BBL)</label><br>
                                            </div>
                                        </div>
                                    </div><br>

                                    <input class="payment_type promtpay mr-2 mt-2" type="radio" name="payment_id" value="3" id="promtpay"><label for="promtpay">PromtPay</label>

                                </div>

                                <div class="ml-3 mt-5">
                                    <h5><?php echo getWording('payment', 'warranty') ?> <a data-bs-toggle="modal" disabled data-bs-target="#myModal8" style="text-decoration: underline; cursor: pointer;"><?php echo getWording('payment', 'here') ?></a> *</h5>
                                </div>
                                <!-- <div style="background-color: #F2F2F2; margin: 10px; padding: 15px;">
                                    <?php // echo getVariable($conditions_list[0], 'detail') 
                                    ?>
                                </div> -->
                                <div>
                                    <h5>
                                        <div class="accept_terms pt-2 pl-5">
                                            <input class="accept" type="checkbox" id="accept"><label class="ml-2" for="accept"> <?php echo getWording('payment', 'understand') ?></label>
                                        </div>
                                    </h5>
                                </div>

                                <div class="pt-5 pb-5" style="text-align: center;">
                                    <button type="button" id="confirm_modal" style="width: 252px; height: 45px; background-color: #D3D3D3; color: #000000; border: 0px; border-radius: 23px;">
                                        <b><?php echo getWording('payment', 'confirmation') ?></b>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-xl-3">
                <div class="order-summary" style="width:100%;">

                    <div class="header d-flex justify-content-between text-white">
                        <h5><?php echo getWording('payment', 'order_summary') ?></h5>
                        <?php if (empty($product_list['product_id'])) { ?>
                            <span> <?php echo getWording('payment', 'go_back') ?> </span>
                        <?php } else { ?>
                            <h5><?php echo count($product_list['product_id']) ?> <?php echo getWording('payment', 'list') ?></h5>
                        <?php } ?>
                    </div>
                    <div class="order-detail" style="background-color: white;">
                        <div class="product-price d-flex justify-content-between">
                            <span class="title"><?php echo getWording('profile', 'product_price') ?></span>
                            <?php if (empty($product_list['product_id'])) { ?>
                                <span> - </span>
                            <?php } else { ?>
                                <?php foreach ($product_list['product_total_price'] as $key => $product_retail_price) {
                                    $product_total_retail_price += (int)$product_retail_price;
                                } ?>
                                <span><?php echo number_format($product_total_retail_price) ?> <?php echo getWording('index', 'baht') ?></span>
                            <?php } ?>
                        </div>
                        <div class="tranport-price d-flex justify-content-between">
                            <span class="title"><?php echo getWording('product', 'shipping') ?></span>
                            <?php if (empty($product_list['product_id'])) { ?>
                                <span> - </span>
                            <?php } else { ?>
                                <span><?php echo number_format($delivery_discount) ?> <?php echo getWording('index', 'baht') ?></span>
                            <?php } ?>
                        </div>
                        <div class="discount d-flex justify-content-between">
                            <div>
                                <span class="title"><?php echo getWording('product', 'discount') ?></span>
                                <?php if (empty($product_list['coupon_name_submit'])) { ?>
                                    <span class="d-block">(<?php echo getWording('product', 'not') ?>)</span>
                                <?php } else { ?>
                                    <span class="d-block"><?php echo $product_list['coupon_name_submit'] ?></span>
                                <?php } ?>
                            </div>
                            <?php if (empty($product_list['coupon_name_submit'])) { ?>
                                <span><?php echo getWording('product', 'not') ?></span>
                            <?php } else if (!empty($product_list['coupon_discount_onlynormalprice'])) { ?>
                                <span><?php echo $product_list['coupon_discount_onlynormalprice'] ?> <?php echo getWording('index', 'baht') ?></span>
                            <?php } else if (!empty($product_list['coupon_discount_delivery'])) { ?>
                                <span><?php echo $product_list['coupon_discount_delivery'] ?> <?php echo getWording('index', 'baht') ?></span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="total-price d-flex justify-content-between" style="background-color: white;">
                        <div class="text-start">
                            <span class="title"><?php echo getWording('profile', 'net_price') ?></span>
                            <span class="d-block">(<?php echo getWording('product', 'vat') ?>)</span>
                        </div>
                        <?php if (empty($product_list['product_id'])) { ?>
                            <span> - </span>
                        <?php } else { ?>
                            <span><?php echo number_format($product_total_price_all + $delivery_discount) ?> <?php echo getWording('index', 'baht') ?></span>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="payment_modal">

    <div class="modal fade" id="myModal9" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-content-style">
                <div class="modal-header modal-header-style">
                </div>
                <div class="modal-body">
                    <div class="btn-close-modal">
                        <button type="button" class="btn-close" id="close_modal9" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <h3 style="text-align: center;"><?php echo getWording('payment', 'receive') ?></h3>
                </div>
                <div class="modal-footer modal-footer-style register_response_btn">

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-fail" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-content-style">
                <div class="modal-header modal-header-style">
                </div>
                <div class="modal-body">
                    <div class="btn-close-modal">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <h3 style="text-align: center;"><?php echo getWording('payment', 'select_product') ?></h3>
                </div>
                <div class="modal-footer modal-footer-style register_response_btn">

                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="myModal8">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" style="justify-content: center;">
            <div class="modal-content" style="min-inline-size: fit-content;">
                <!-- Modal Header -->
                <div class="modal-header" style="width:100% ">
                    <h5 class="modal-title" style="padding-left:20px"><?php echo getWording('payment', 'warranty_head') ?></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <!-- Modal body -->
                <div class="modal-body" style="width:auto">
                    <div style="background-color: #F2F2F2; margin: 10px; padding: 15px;">
                        <?php echo getVariable($conditions_list[1], 'detail')
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!------------------------------------------------------ edit tax address ---------------------------------------------------->
    <div class="modal" id="myModal7">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" style="justify-content: center;">
            <div class="modal-content" style="min-inline-size: fit-content;">

                <!-- Modal Header -->
                <div class="modal-header" style=" width: 100%">
                    <h5 class="modal-title" style="padding-left:20px"><?php echo getWording('payment', 'edit_address') ?></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <!-- Modal body -->

                <?php foreach ($tax_address_all as $key => $tax_address_item) { ?>
                    <?php if ($tax_address_item->id == $tax_address->id) { ?>
                        <form id="dataForm3">
                            <div class="row pl-8 pr-8">
                                <div class="col-xl-6 name pt-5">
                                    <b><?php echo getWording('profile', 'name') ?> *</b>
                                    <input type="text" id="name_address_tax_edit" name="name_address_tax_edit" value="<?php echo $tax_address_item->first_name ?>" placeholder="<?php echo getWording('profile', 'name') ?>" required>
                                </div>

                                <div class="col-xl-6 lastname pt-5">
                                    <b><?php echo getWording('profile', 'lastname') ?> *</b>
                                    <input type="text" id="lastname_address_tax_edit" name="lastname_address_tax_edit" value="<?php echo $tax_address_item->last_name ?>" placeholder="<?php echo getWording('profile', 'lastname') ?>" required>
                                </div>

                                <div class="col-xl-6 phone pt-5">
                                    <b><?php echo getWording('profile', 'phone') ?> *</b>
                                    <input type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" id="telephone_address_tax_edit" name="telephone_address_tax_edit" value="<?php echo $tax_address_item->telephone ?>" placeholder="<?php echo getWording('profile', 'phone') ?>" required>
                                </div>

                                <div class="col-xl-6 email pt-5">
                                    <b><?php echo getWording('profile', 'email') ?> *</b>
                                    <input type="text" id="email_address_tax_edit" name="email_address_tax_edit" value="<?php echo $tax_address_item->email ?>" placeholder="<?php echo getWording('profile', 'email') ?>" required>
                                </div>

                                <div class="col-xl-6 company pt-5">
                                    <b><?php echo getWording('profile', 'company') ?></b>
                                    <input type="text" id="company_address_tax_edit" name="company_address_tax_edit" value="<?php echo $tax_address_item->company_name ?>" placeholder="<?php echo getWording('profile', 'company') ?>">
                                </div>

                                <div class="col-xl-6 branch pt-5">
                                    <b><?php echo getWording('profile', 'branch') ?></b>
                                    <input type="text" id="branch_address_tax_edit" name="branch_address_tax_edit" value="<?php echo $tax_address_item->company_branch ?>" placeholder="<?php echo getWording('profile', 'branch') ?>">
                                </div>

                                <div class="col-xl-6 address pt-5">
                                    <b><?php echo getWording('profile', 'address') ?> *</b>
                                    <input type="text" id="address_address_tax_edit" name="address_address_tax_edit" value="<?php echo $tax_address_item->address ?>" placeholder="<?php echo getWording('profile', 'address') ?>">
                                </div>

                                <div class="col-xl-6 province pt-5">
                                    <div class="form-group">
                                        <b><?php echo getWording('profile', 'province') ?> *</b>
                                        <!-- <input type="text" id="province" name="province" placeholder="จังหวัด"> -->

                                        <select class="form-control select select2" id="province_address_tax_edit" name="province_address_tax_edit">
                                            <?php
                                            if (isset($address_province->id)) {
                                            ?>
                                                <option value="<?php echo $address_province->id ?>" selected><?php echo $address_province->name ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xl-6 district pt-5">
                                    <div class="form-group">
                                        <b><?php echo getWording('profile', 'district') ?> *</b>
                                        <!-- <input type="text" id="district" name="district" placeholder="อำเภอ/เขต"> -->
                                        <select class="form-control select select2" id="district_address_tax_edit" name="district_address_tax_edit">
                                            <?php
                                            if (isset($address_district->id)) {
                                            ?>
                                                <option value="<?php echo $address_district->id; ?>" selected><?php echo $address_district->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xl-6 sub-district mt-5">
                                    <div class="form-group">
                                        <b><?php echo getWording('profile', 'sub_district') ?></b>
                                        <!-- <input type="text" id="subdistrict" name="subdistrict" placeholder="ตำบล/แขวง"> -->

                                        <select class="form-control select select2" id="sub_district_address_tax_edit" name="sub_district_address_tax_edit">
                                            <?php
                                            if (isset($address_sub_district->id)) {
                                            ?>
                                                <option value="<?php echo $address_sub_district->id; ?>" selected><?php echo $address_sub_district->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xl-6 postcode mt-5">
                                    <b><?php echo getWording('profile', 'postcode') ?> *</b>
                                    <input type="text" id="postcode3" name="postcode3" value="<?php echo isset($postcode) ?>" placeholder="<?php echo getWording('profile', 'postcode') ?>">
                                </div>

                                <div class="col-xl-6 tax pt-5">
                                    <b><?php echo getWording('profile', 'tax') ?> *</b>
                                    <input type="text" id="tax_address_tax_edit" name="tax_address_tax_edit" value="<?php echo $tax_address_item->tax_id ?>" placeholder="<?php echo getWording('profile', 'tax') ?>">
                                </div>

                                <div class="pt-1">
                                    <?php if (($tax_address_item->is_default) == '1') { ?>
                                        <input type="checkbox" name="is_default_address_tax_edit" id="is_default_address_tax_edit" value="1" checked> <?php echo getWording('profile', 'set_address') ?>
                                    <?php } else { ?>
                                        <input type="checkbox" name="is_default_address_tax_edit" id="is_default_address_tax_edit" value="0"> <?php echo getWording('profile', 'set_address') ?>
                                    <?php } ?>
                                </div>

                                <input class="address_id_tax_edit" id="address_id_tax_edit" name="address_id_tax_edit" value="<?php echo $tax_address_item->id ?>" hidden>

                                <div class="button4 pt-4" style="text-align: center;">
                                    <div class="submit pt-4" style="text-align: center; padding-bottom: 40px;">
                                        <button id="submit-edit_tax" style="width: 208px; height:45px; border:solid 0px; border-radius: 23px; padding: 9px; margin:10px; background-color:#AAAAAA; color:#FFFFFF;"><?php echo getWording('profile', 'save') ?></button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    <?php } ?>
                <?php } ?>

            </div>
        </div>
    </div>

    <!------------------------------------------------------ add tax address ----------------------------------------------------->
    <div class="modal" id="myModal6">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" style="justify-content: center;">
            <div class="modal-content" style="min-inline-size: fit-content;">

                <!-- Modal Header -->
                <div class="modal-header" style=" width: 100%">
                    <h5 class="modal-title" style="padding-left:20px"><?php echo getWording('payment', 'add_new_address') ?></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <!-- Modal body -->
                <form id="dataForm2" class="modal-body pl-8 pr-8" style="width:100%">
                    <div class="row">
                        <div class="col-xl-6 name pt-5">
                            <b><?php echo getWording('profile', 'name') ?> *</b>
                            <input type="text" id="name" name="name" placeholder="<?php echo getWording('profile', 'name') ?>" required>
                        </div>

                        <div class="col-xl-6 lastname pt-5">
                            <b><?php echo getWording('profile', 'lastname') ?> *</b>
                            <input type="text" id="lastname" name="lastname" placeholder="<?php echo getWording('profile', 'lastname') ?>" required>
                        </div>

                        <div class="col-xl-6 phone pt-5">
                            <b><?php echo getWording('profile', 'phone') ?> *</b>
                            <input type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" id="telephone" name="telephone" placeholder="<?php echo getWording('profile', 'phone') ?>" required>
                        </div>

                        <div class="col-xl-6 email pt-5">
                            <b><?php echo getWording('profile', 'email') ?> *</b>
                            <input type="text" id="email" name="email" value="<?php echo $member_email ?>" placeholder="<?php echo getWording('profile', 'email') ?>" required>
                        </div>

                        <div class="col-xl-6 company pt-5">
                            <b><?php echo getWording('profile', 'company') ?></b>
                            <input type="text" id="company" name="company" placeholder="<?php echo getWording('profile', 'company') ?>">
                        </div>

                        <div class="col-xl-6 branch pt-5">
                            <b><?php echo getWording('profile', 'branch') ?></b>
                            <input type="text" id="branch" name="branch" placeholder="branch">
                        </div>

                        <div class="col-xl-6 address pt-5">
                            <b><?php echo getWording('profile', 'address') ?> *</b>
                            <input type="text" id="address" name="address" placeholder="<?php echo getWording('profile', 'address') ?>">
                        </div>

                        <div class="col-xl-6 province pt-5">
                            <div class="form-group">
                                <b><?php echo getWording('profile', 'province') ?> *</b>
                                <!-- <input type="text" id="province" name="province" placeholder="จังหวัด"> -->

                                <select class="form-control select select2" id="province_tax_add" name="province_tax_add">
                                    <?php
                                    if (isset($address_province->id)) {
                                    ?>
                                        <option value="<?php echo $address_province->id ?>" selected><?php echo $address_province->name ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-xl-6 district pt-5">
                            <div class="form-group">
                                <b><?php echo getWording('profile', 'district') ?> *</b>
                                <!-- <input type="text" id="district" name="district" placeholder="อำเภอ/เขต"> -->
                                <select class="form-control select select2" id="district_tax_add" name="district_tax_add">
                                    <?php
                                    if (isset($address_district->id)) {
                                    ?>
                                        <option value="<?php echo $address_district->id; ?>" selected><?php echo $address_district->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-xl-6 sub-district mt-5">
                            <div class="form-group">
                                <b><?php echo getWording('profile', 'sub_district') ?></b>
                                <!-- <input type="text" id="subdistrict" name="subdistrict" placeholder="ตำบล/แขวง"> -->

                                <select class="form-control select select2" id="sub_district_tax_add" name="sub_district_tax_add">
                                    <?php
                                    if (isset($address_sub_district->id)) {
                                    ?>
                                        <option value="<?php echo $address_sub_district->id; ?>" selected><?php echo $address_sub_district->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-xl-6 postcode mt-5">
                            <b><?php echo getWording('profile', 'postcode') ?> *</b>
                            <input type="text" id="postcode2" name="postcode2" value="<?php echo isset($postcode) ?>" placeholder="<?php echo getWording('profile', 'postcode') ?>">
                        </div>

                        <div class="col-xl-6 postal mt-5">
                            <b><?php echo getWording('profile', 'tax') ?> *</b>
                            <input type="text" id="tax" name="tax" placeholder="<?php echo getWording('profile', 'tax') ?>">
                        </div>

                        <div class="pt-1">
                            <label class="form-check-label"><input type="checkbox" id="is_default"> <?php echo getWording('profile', 'set_address') ?></label>
                        </div>

                        <div class="button4 pt-4" style="text-align: center;">
                            <div class="submit pt-4" style="text-align: center; padding-bottom: 40px;">
                                <input id="submit-add_tax" type="submit" value="<?php echo getWording('profile', 'save') ?>" style="width: 208px; height:45px; border:solid 0px; border-radius: 23px; padding: 9px; margin:10px; background-color:#AAAAAA; color:#FFFFFF;">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!---------------------------------------------------- choose tax address --------------------------------------------------->
    <div class="modal" id="myModal5">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" style="justify-content: center;">
            <div class="modal-content" style="min-inline-size: fit-content;">
                <!-- Modal Header -->
                <div class="modal-header" style="width:100% ">
                    <h5 class="modal-title" style="padding-left:20px"><?php echo getWording('payment', 'choose_address') ?></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <!-- Modal body -->
                <div class="modal-body" style="width:auto">
                    <!-- <form> -->
                    <?php if (empty($tax_address_all)) { ?>
                        <?php echo null ?>
                    <?php } else { ?>
                        <div class="row p-3" id="choose_tax_address">
                            <?php foreach ($tax_address_all as $key => $tax_address_all_item) { ?>
                                <!-- <button class="btn mt-3"> -->
                                <button class="select_tax_address btn mt-2" style="width: 100%; height: auto; border: solid #DBDBDB;">
                                    <div class="row tax_address_detail p-5">

                                        <div class="col-xl-5" style="text-align: left;">
                                            <H5 class="reciver_tax_name"><?php echo $tax_address_all_item->first_name ?> <?php echo $tax_address_all_item->last_name ?></H5>

                                            <p class="reciver_tax_address pt-2"><b><?php echo getWording('profile', 'address') ?> :</b> <?php echo $tax_address_all_item->address ?></p>

                                            <p class="reciver_tax_district"><b><?php echo getWording('profile', 'sub_district') ?> :</b> <?php echo $tax_address_all_item->sub_district->name ?> <b><?php echo getWording('profile', 'district') ?> :</b> <?php echo $tax_address_all_item->district->name ?></p>

                                            <p class="reciver_tax_province"><b><?php echo getWording('profile', 'province') ?> :</b> <?php echo $tax_address_all_item->province->name ?> <?php echo $tax_address_all_item->postcode ?></p>

                                            <?php if (empty($tax_address_all_item->company_name)) { ?>
                                                <p class="recive_taxr_company"></p>
                                            <?php } else { ?>
                                                <p class="reciver_tax_company"><b><?php echo getWording('profile', 'company') ?> :</b> <?php echo $tax_address_all_item->company_name ?></p>
                                            <?php } ?>

                                            <?php if (empty($tax_address_all_item->company_branch)) { ?>
                                                <p class="reciver_tax_branch"></p>
                                            <?php } else { ?>
                                                <p class="reciver_tax_branch"><b><?php echo getWording('profile', 'branch') ?> :</b> <?php echo $tax_address_all_item->company_branch ?></p>
                                            <?php } ?>

                                            <p class="reciver_tax_email"><b><?php echo getWording('profile', 'email') ?> :</b> <?php echo $tax_address_all_item->email ?></p>

                                            <p class="reciver_tax_phone"><b><?php echo getWording('profile', 'phone') ?> :</b> <?php echo $tax_address_all_item->telephone ?></p>

                                            <p class="reciver_tax_tax"><b><?php echo getWording('profile', 'tax') ?> :</b> <?php echo $tax_address_all_item->tax_id ?></p>

                                            <p class="reciver_tax_id" hidden><?php echo $tax_address_all_item->id ?></p>

                                            <p class="reciver_tax_default" hidden><?php echo $tax_address_all_item->is_default ?></p>
                                        </div>
                                    </div>
                                    <?php if (($tax_address_all_item->is_default) >= '1') { ?>
                                        <div class="row p-4">
                                            <div style="width: 123px; height: 34px; background: #E6E5E5; border-radius: 3px; border:0px; padding:3px; text-align: center;">
                                                <div style="padding-top: 4.5px;"><?php echo getWording('profile', 'main_address') ?></div>
                                            </div>
                                        </div>
                                    <?php } else {
                                        echo null;
                                    } ?>
                                </button>
                                <!-- </button> -->
                            <?php } ?>
                        </div>
                    <?php } ?>

                    <div style="text-align: center; padding-bottom: 40px;">
                        <div class="submit pt-4">
                            <input id="save_tax" type="submit" data-bs-dismiss="modal" value="<?php echo getWording('profile', 'save') ?>" style="width: 208px; height: 45px; background-color: #AAAAAA; color:#FFFFFF; border: 0px; border-radius: 23px;">
                        </div>
                    </div>

                    <!-- </form> -->
                </div>
            </div>
        </div>
    </div>

    <!------------------------------------------------------ confirm orders ----------------------------------------------------->
    <div class="modal" id="myModal4">
        <div id="confirm_order">
            <div class="modal-dialog modal-lg modal-dialog-scrollable" style="justify-content: center;">
                <div class="modal-content" style="min-inline-size: fit-content;">
                    <!-- Modal Header -->
                    <div class="modal-header" style="width: 100%;">
                        <h5 class="modal-title" style="padding-left:20px"><?php echo getWording('payment', 'order_confirmation') ?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body" id="confirm_order_detail" style="width: 100%; text-align: left;">
                        <div style="padding-left: 10px; padding-top: 15px; font-weight: bold;"><?php echo getWording('profile', 'shipping_address') ?></div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-12" style="padding-bottom: 10px; text-align: center;">
                                <?php if (empty($address)) { ?>
                                    <h3><?php echo getWording('payment', 'please_add_address') ?></h3>
                                <?php } else { ?>
                                    <div class="row p-4" id="confirm_order_address">
                                        <div class="address mt-2" style="width: 100%; height: auto; border: solid #DBDBDB; text-align:left;">
                                            <div class="row address_main p-5">

                                                <div class="col-xl-5">
                                                    <h5 class="address_name"><?php echo $address->first_name ?> <?php echo $address->last_name ?></h5>

                                                    <p class="address_address pt-2"><b><?php echo getWording('profile', 'address') ?> :</b> <?php echo $address->address ?></p>

                                                    <p class="address_district"><b><?php echo getWording('profile', 'sub_district') ?> :</b> <?php echo $address->sub_district->name ?> <b><?php echo getWording('profile', 'district') ?> :</b> <?php echo $address->district->name ?></p>

                                                    <p class="address_province"><b><?php echo getWording('profile', 'province') ?> :</b> <?php echo $address->province->name ?> <?php echo $address->postcode ?></p>

                                                    <?php if (empty($address->company_name)) { ?>
                                                        <p class="address_company"></p>
                                                    <?php } else { ?>
                                                        <p class="address_company"><b><?php echo getWording('profile', 'company') ?> :</b> <?php echo $address->company_name ?></p>
                                                    <?php } ?>

                                                    <?php if (empty($address->company_branch)) { ?>
                                                        <p class="address_branch"></p>
                                                    <?php } else { ?>
                                                        <p class="address_branch"><b><?php echo getWording('profile', 'branch') ?> :</b> <?php echo $address->company_branch ?></p>
                                                    <?php } ?>

                                                    <p class="address_email"><b><?php echo getWording('profile', 'email') ?> :</b> <?php echo $address->email ?></p>

                                                    <p class="address_phone"><b><?php echo getWording('profile', 'phone') ?> :</b> <?php echo $address->telephone ?></p>
                                                </div>

                                                <div class="col-xl-5"></div>
                                                <div class="col-xl-2"></div>
                                            </div>
                                            <input class="address_id_address_edit" name="address_id" value="<?php echo $address->id ?>" hidden>
                                            <input class="member_id" name="member_id" value="<?php echo $address->member_id ?>" hidden>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="taxaddress_submit" style="padding-left: 5px; font-weight: bold;"><?php echo getWording('index', 'tax_invoice') ?></div>
                        <div class="row" id="taxaddress_detail">
                            <div class="col-sm-12 col-xl-12 pl-5 pr-5" style="padding-bottom: 10px; text-align: center;">
                                <?php if (empty($tax_address)) { ?>
                                    <h3 class="please_add_address_tax" style="display: none;"><?php echo getWording('payment', 'please_add_address_tax') ?></h3>
                                <?php } else { ?>
                                    <div class="row p-4" id="confirm_order_tax">
                                        <div class="taxaddress_submit mt-2" style="width: 100%; height: auto; border: solid #DBDBDB; text-align: left;">

                                            <div class="row tax_address_main p-5">

                                                <div class="col-xl-5">
                                                    <H5 class="tax_address_name"><?php echo $tax_address->first_name ?> <?php echo $tax_address->last_name ?></H5>

                                                    <p class="tax_address_address pt-2"><b><?php echo getWording('profile', 'address') ?> :</b> <?php echo $tax_address->address ?></p>

                                                    <p class="tax_address_district"><b><?php echo getWording('profile', 'sub_district') ?> :</b> <?php echo $tax_address->sub_district->name ?> <b><?php echo getWording('profile', 'district') ?> :</b> <?php echo $tax_address->district->name ?></p>

                                                    <p class="tax_address_province"><b><?php echo getWording('profile', 'province') ?> :</b> <?php echo $tax_address->province->name ?> <?php echo $tax_address->postcode ?></p>

                                                    <?php if (empty($tax_address->company_name)) { ?>
                                                        <?php echo null ?>
                                                    <?php } else { ?>
                                                        <p class="tax_address_company"><b><?php echo getWording('profile', 'company') ?> :</b> <?php echo $tax_address->company_name ?></p>
                                                    <?php } ?>

                                                    <?php if (empty($tax_address->company_branch)) { ?>
                                                        <?php echo null ?>
                                                    <?php } else { ?>
                                                        <p class="tax_address_branch"><b><?php echo getWording('profile', 'branch') ?> :</b> <?php echo $tax_address->company_branch ?></p>
                                                    <?php } ?>

                                                    <p class="tax_address_email"><b><?php echo getWording('profile', 'email') ?> :</b> <?php echo $tax_address->email ?></p>

                                                    <p class="tax_address_phone"><b><?php echo getWording('profile', 'phone') ?> :</b> <?php echo $tax_address->telephone ?></p>

                                                    <p class="tax_address_no"><b><?php echo getWording('profile', 'tax') ?> :</b> <?php echo $tax_address->tax_id ?></p>

                                                </div>
                                                <div class="col-xl-5"></div>
                                                <div class="col-xl-2"></div>
                                            </div>

                                        </div>
                                    </div>
                                    <input class="address_tax_id" name="address_tax_id" value="<?php echo $tax_address->id ?>" hidden>
                                <?php } ?>
                            </div>
                        </div>

                        <div style="padding-left: 5px; font-weight: bold; padding-bottom: 10px;"><?php echo getWording('payment', 'payment_method') ?></div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-12 pl-5 pr-5" style="padding-bottom: 10px; text-align: center;">
                                <div class="pay mt-3" style="width: 100%; height: auto; border: solid #DBDBDB; text-align: left;">
                                    <div class="credit_submit col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        <?php echo getWording('profile', 'credit') ?>
                                    </div>
                                    <div class="internet_banking_submit col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        Internet Banking
                                    </div>
                                    <div class="promtpay_submit col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        PromptPay
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php if (empty($product_list['product_id'])) { ?>
                            <?php echo null ?>
                        <?php } else { ?>

                            <?php foreach ($product_list['product_id'] as $key => $product_id) { ?>
                                <input class="product_id" name="product_id" value="<?php echo $product_id ?>" hidden>
                            <?php } ?>

                            <?php foreach ($product_list['product_retail_price'] as $key => $product_retail_price) { ?>
                                <input class="product_retail_price" name="product_retail_price" value="<?php echo $product_retail_price ?>" hidden>
                            <?php } ?>

                            <?php foreach ($product_list['product_qty'] as $key => $product_qty) { ?>
                                <input class="product_qty" name="product_qty" value="<?php echo $product_qty ?>" hidden>
                            <?php } ?>

                            <?php foreach ($product_list['product_total_price'] as $key => $product_total_price) { ?>
                                <input class="product_total_price" name="product_total_price" value="<?php echo $product_total_price ?>" hidden>
                            <?php } ?>

                        <?php } ?>

                        <input class="net_price" name="net_price" value="<?php echo $product_total_price_all + $delivery_discount ?>" hidden>
                        <input class="delivery_price" name="delivery_price" value="<?php echo $delivery_discount ?>" hidden>
                        <input class="promotion_discount_id" name="promotion_discount_id" value="" hidden>
                        <input class="coupon_id" name="coupon_id" value="<?php echo $product_list['coupon_id_submit'] ?>" hidden>
                        <input class="product_price" name="product_price" value="<?php echo $product_total_retail_price ?>" hidden>
                        <input class="discount" name="discount" value="" hidden>
                        <div class="submit pt-5" style=" text-align: center">
                            <button id="submit-confirm_order" style="width: 252px; height: 45px; background-color: #D3D3D3; border: 0px; border-radius: 23px;"><?php echo getWording('payment', 'payment') ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!------------------------------------------------------- edit address ------------------------------------------------------>
    <div class="modal" id="myModal3">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" style="justify-content: center;">
            <div class="modal-content" style="min-inline-size: fit-content;">

                <!-- Modal Header -->
                <div class="modal-header" style=" width: 100%">
                    <h5 class="modal-title" style="padding-left:20px">
                        <?php echo getWording('payment', 'edit_address') ?>
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <!-- Modal body -->
                <form id="dataForm1" class="overflow-auto">
                    <?php foreach ($address_all as $key => $address_item) { ?>
                        <?php if ($address_item->id == $address->id) { ?>
                            <div class="row pl-8 pr-8">
                                <div class="col-xl-6 name pt-5">
                                    <b><?php echo getWording('profile', 'name') ?> *</b>
                                    <input type="text" id="name_address_edit" name="name_address_edit" value="<?php echo $address_item->first_name ?>" placeholder="<?php echo getWording('profile', 'name') ?>" required>
                                </div>

                                <div class="col-xl-6 lastname pt-5">
                                    <b><?php echo getWording('profile', 'lastname') ?> *</b>
                                    <input type="text" id="lastname_address_edit" name="lastname_address_edit" value="<?php echo $address_item->last_name ?>" placeholder="<?php echo getWording('profile', 'lastname') ?>" required>
                                </div>

                                <div class="col-xl-6 phone pt-5">
                                    <b><?php echo getWording('profile', 'phone') ?> *</b>
                                    <input type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" id="telephone_address_edit" name="telephone_address_edit" value="<?php echo $address_item->telephone ?>" placeholder="<?php echo getWording('profile', 'phone') ?>" required>
                                </div>

                                <div class="col-xl-6 email pt-5">
                                    <b><?php echo getWording('profile', 'email') ?> *</b>
                                    <input type="text" id="email_address_edit" name="email_address_edit" value="<?php echo $address_item->email ?>" placeholder="<?php echo getWording('profile', 'email') ?>" required>
                                </div>

                                <div class="col-xl-6 company pt-5">
                                    <b><?php echo getWording('profile', 'company') ?></b>
                                    <input type="text" id="company_address_edit" name="company_address_edit" value="<?php echo $address_item->company_name ?>" placeholder="<?php echo getWording('profile', 'company') ?>">
                                </div>

                                <div class="col-xl-6 branch pt-5">
                                    <b><?php echo getWording('profile', 'branch') ?></b>
                                    <input type="text" id="branch_address_edit" name="branch_address_edit" value="<?php echo $address_item->company_branch ?>" placeholder="<?php echo getWording('profile', 'branch') ?>">
                                </div>

                                <div class="col-xl-6 address pt-5">
                                    <b><?php echo getWording('profile', 'address') ?> *</b>
                                    <input type="text" id="address_address_edit" name="address_address_edit" value="<?php echo $address_item->address ?>" placeholder="<?php echo getWording('profile', 'address') ?>">
                                </div>

                                <div class="col-xl-6 province pt-5">
                                    <div class="form-group">
                                        <b><?php echo getWording('profile', 'province') ?> *</b>
                                        <!-- <input type="text" id="province" name="province" placeholder="จังหวัด"> -->

                                        <select class="form-control select select2" id="province_address_edit" name="province_address_edit">
                                            <?php
                                            if (isset($address_province->id)) {
                                            ?>
                                                <option value="<?php echo $address_province->id ?>" selected><?php echo $address_province->name ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xl-6 district pt-5">
                                    <div class="form-group">
                                        <b><?php echo getWording('profile', 'district') ?> *</b>
                                        <!-- <input type="text" id="district" name="district" placeholder="อำเภอ/เขต"> -->
                                        <select class="form-control select select2" id="district_address_edit" name="district_address_edit">
                                            <?php
                                            if (isset($address_district->id)) {
                                            ?>
                                                <option value="<?php echo $address_district->id; ?>" selected><?php echo $address_district->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xl-6 sub-district mt-5">
                                    <div class="form-group">
                                        <b><?php echo getWording('profile', 'sub_district') ?></b>
                                        <!-- <input type="text" id="subdistrict" name="subdistrict" placeholder="ตำบล/แขวง"> -->

                                        <select class="form-control select select2" id="sub_district_address_edit" name="sub_district_address_edit">
                                            <?php
                                            if (isset($address_sub_district->id)) {
                                            ?>
                                                <option value="<?php echo $address_sub_district->id; ?>" selected><?php echo $address_sub_district->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xl-6 postcode mt-5">
                                    <b><?php echo getWording('profile', 'postcode') ?> *</b>
                                    <input type="text" id="postcode1" name="postcode1" value="<?php echo isset($postcode) ?>" placeholder="<?php echo getWording('profile', 'postcode') ?>">
                                </div>

                                <div class="pt-1">
                                    <?php if (($address_item->is_default) == '1') { ?>
                                        <input type="checkbox" name="is_default_address_edit" id="is_default_address_edit" value="1" checked> <?php echo getWording('profile', 'set_address') ?>
                                    <?php } else { ?>
                                        <input type="checkbox" name="is_default_address_edit" id="is_default_address_edit" value="0"> <?php echo getWording('profile', 'set_address') ?>
                                    <?php } ?>
                                </div>

                                <input class="address_id_address_edit" id="address_id_address_edit" name="address_id_address_edit" value="<?php echo $address_item->id ?>" hidden>

                                <div class="button4 pt-4" style="text-align: center;">
                                    <div class="submit pt-4" style="text-align: center; padding-bottom: 40px;">
                                        <button id="submit-edit" style="width: 208px; height:45px; border:solid 0px; border-radius: 23px; padding: 9px; margin:10px; background-color:#AAAAAA; color:#FFFFFF;"><?php echo getWording('profile', 'save') ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>

    <!------------------------------------------------------ add new address ---------------------------------------------------->
    <div class="modal" id="myModal2">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" style="justify-content: center;">
            <div class="modal-content" style="min-inline-size: fit-content;">

                <!-- Modal Header -->
                <div class="modal-header" style=" width: 100%">
                    <h5 class="modal-title" style="padding-left:20px"><?php echo getWording('payment', 'add_new_address') ?></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <!-- Modal body -->
                <form id="dataForm" class="modal-body pl-8 pr-8" style="width:100%">
                    <div class="row">
                        <div class="col-xl-6 name pt-5">
                            <b><?php echo getWording('profile', 'name') ?> *</b>
                            <input type="text" id="name_address_add" name="name_address_add" placeholder="<?php echo getWording('profile', 'name') ?>" required>
                        </div>

                        <div class="col-xl-6 lastname pt-5">
                            <b><?php echo getWording('profile', 'lastname') ?> *</b>
                            <input type="text" id="lastname_address_add" name="lastname_address_add" placeholder="<?php echo getWording('profile', 'lastname') ?>" required>
                        </div>

                        <div class="col-xl-6 phone pt-5">
                            <b><?php echo getWording('profile', 'phone') ?> *</b>
                            <input type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" id="telephone_address_add" name="telephone_address_add" placeholder="<?php echo getWording('profile', 'phone') ?>" required>
                        </div>

                        <div class="col-xl-6 email pt-5">
                            <b><?php echo getWording('profile', 'email') ?> *</b>
                            <input type="text" id="email_address_add" name="email_address_add" value="<?php echo $member_email ?>" placeholder="<?php echo getWording('profile', 'email') ?>" required>
                        </div>

                        <div class="col-xl-6 company pt-5">
                            <b><?php echo getWording('profile', 'company') ?></b>
                            <input type="text" id="company_address_add" name="company_address_add" placeholder="<?php echo getWording('profile', 'company') ?>">
                        </div>

                        <div class="col-xl-6 branch pt-5">
                            <b><?php echo getWording('profile', 'branch') ?></b>
                            <input type="text" id="branch_address_add" name="branch_address_add" placeholder="<?php echo getWording('profile', 'branch') ?>">
                        </div>

                        <div class="col-xl-6 address pt-5">
                            <b><?php echo getWording('profile', 'address') ?> *</b>
                            <input type="text" id="address_address_add" name="address_address_add" placeholder="<?php echo getWording('profile', 'address') ?>">
                        </div>

                        <div class="col-xl-6 province pt-5">
                            <div class="form-group">
                                <b><?php echo getWording('profile', 'province') ?> *</b>
                                <select class="form-control select select2" id="province_address_add" name="province_address_add">
                                    <?php
                                    if (isset($address_province->id)) {
                                    ?>
                                        <option value="<?php echo $address_province->id ?>" selected><?php echo $address_province->name ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-xl-6 district pt-5">
                            <div class="form-group">
                                <b><?php echo getWording('profile', 'district') ?> *</b>
                                <select class="form-control select select2" id="district_address_add" name="district_address_add">
                                    <?php
                                    if (isset($address_district->id)) {
                                    ?>
                                        <option value="<?php echo $address_district->id; ?>" selected><?php echo $address_district->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-xl-6 sub-district mt-5">
                            <div class="form-group">
                                <b><?php echo getWording('profile', 'sub_district') ?></b>
                                <select class="form-control select select2" id="sub_district_address_add" name="sub_district_address_add">
                                    <?php
                                    if (isset($address_sub_district->id)) {
                                    ?>
                                        <option value="<?php echo $address_sub_district->id; ?>" selected><?php echo $address_sub_district->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-xl-6 postcode mt-5">
                            <b><?php echo getWording('profile', 'postcode') ?> *</b>
                            <input type="text" id="postcode" name="postcode" value="<?php echo isset($postcode) ?>" placeholder="<?php echo getWording('profile', 'postcode') ?>">
                        </div>

                        <div class="pt-1">
                            <label class="form-check-label"><input type="checkbox" id="is_default_address_add" value="0"> <?php echo getWording('profile', 'set_address') ?></label>
                        </div>

                        <div class="button4 pt-4" style="text-align: center;">
                            <div class="submit pt-4" style="text-align: center; padding-bottom: 40px;">
                                <button id="submit-add" style="width: 208px; height:45px; border:solid 0px; border-radius: 23px; padding: 9px; margin:10px; background-color:#AAAAAA; color:#FFFFFF;"><?php echo getWording('profile', 'save') ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!------------------------------------------------------ choose address ----------------------------------------------------->
    <div class="modal" id="myModal1">
        <div class="modal-dialog modal-lg modal-dialog-scrollable" style="justify-content: center;">
            <div class="modal-content" style="min-inline-size: fit-content;">
                <!-- Modal Header -->
                <div class="modal-header" style="width:100% ">
                    <h5 class="modal-title" style="padding-left:20px"><?php echo getWording('payment', 'choose_address') ?></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <!-- Modal body -->
                <div class="modal-body" style="width:auto">
                    <!-- <form> -->
                    <div class="row p-3" id="choose_address">
                        <?php if (empty($address_all)) { ?>
                            <?php echo null ?>
                        <?php } else { ?>
                            <?php foreach ($address_all as $key => $address_item) { ?>
                                <!-- <button class="btn mt-3"> -->
                                <button class="select_address btn mt-2">
                                    <div class="row address_detail p-5">

                                        <div class="col-xl-5" style="text-align: left;">
                                            <H5 class="reciver_name" data-first_name="<?php echo $address_item->first_name ?>" data-last_name="<?php echo $address_item->last_name ?>"><?php echo $address_item->first_name ?> <?php echo $address_item->last_name ?></H5>

                                            <p class="reciver_address pt-2" data-address="<?php echo $address_item->address ?>"><b><?php echo getWording('profile', 'address') ?> :</b> <?php echo $address_item->address ?></p>

                                            <p class="reciver_district" data-sub_district_name="<?php echo $address_item->sub_district->name ?>" data-district_name="<?php echo $address_item->district->name ?>">
                                                <b><?php echo getWording('profile', 'sub_district') ?> :</b> <?php echo $address_item->sub_district->name ?> <b><?php echo getWording('profile', 'district') ?> :</b> <?php echo $address_item->district->name ?>
                                            </p>
                                            <p class="reciver_province" data-province_name="<?php echo $address_item->postcode ?>" data-postcode="<?php echo $address_item->postcode ?>">
                                                <b><?php echo getWording('profile', 'province') ?> :</b>
                                                <?php echo $address_item->province->name ?>
                                                <?php echo $address_item->postcode ?>
                                            </p>

                                            <?php if (empty($address_item->company_name)) { ?>
                                                <p class="reciver_company" data-company_name=""></p>
                                            <?php } else { ?>
                                                <p class="reciver_company" data-company_name="<?php echo $address_item->company_name ?>">
                                                    <b><?php echo getWording('profile', 'company') ?> :</b> <?php echo $address_item->company_name ?>
                                                </p>
                                            <?php } ?>

                                            <?php if (empty($address_item->company_branch)) { ?>
                                                <p class="reciver_branch" data-reciver_branch=""></p>
                                            <?php } else { ?>
                                                <p class="reciver_branch" data-reciver_branch="<?php echo $address_item->company_branch ?>">
                                                    <b><?php echo getWording('profile', 'branch') ?> :</b> <?php echo $address_item->company_branch ?>
                                                </p>
                                            <?php } ?>

                                            <p class="reciver_email" data-email="<?php echo $address_item->email ?>">
                                                <b><?php echo getWording('profile', 'email') ?> :</b> <?php echo $address_item->email ?>
                                            </p>

                                            <p class="reciver_phone" data-telephone="<?php echo $address_item->telephone ?>">
                                                <b><?php echo getWording('profile', 'phone') ?> :</b> <?php echo $address_item->telephone ?>
                                            </p>

                                            <p class="reciver_id" data-id="<?php echo $address_item->id ?>" hidden>
                                                <?php echo $address_item->id ?>
                                            </p>

                                            <p class="reciver_default" data-is_default="<?php echo $address_item->is_default ?>" hidden>
                                                <?php echo $address_item->is_default ?>
                                            </p>
                                        </div>
                                        <div class="col-xl-5"></div>
                                        <div class="col-xl-2"></div>
                                    </div>
                                    <?php if (($address_item->is_default) >= '1') { ?>
                                        <div class="row p-4">
                                            <div style="width: 123px; height: 34px; background: #E6E5E5; border-radius: 3px; border:0px; padding:3px; text-align: center;">
                                                <div style="padding-top: 4.5px;"><?php echo getWording('profile', 'main_address') ?></div>
                                            </div>
                                        </div>
                                    <?php } else {
                                        echo null;
                                    } ?>
                                </button>
                                <!-- </button> -->
                            <?php } ?>
                        <?php } ?>
                    </div>

                    <div style="text-align: center; padding-bottom: 40px;">
                        <div class="submit pt-4">
                            <input id="save" type="submit" data-bs-dismiss="modal" value="<?php echo getWording('profile', 'save') ?>" style="width: 208px; height: 45px; background-color: #AAAAAA; color:#FFFFFF; border: 0px; border-radius: 23px;">
                        </div>
                    </div>

                    <!-- </form> -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true" style="z-index: 1061;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <h4 class="request_response" style="text-align: center;"></h4>
                    <div class="qr_image text-center"></div>
                    <div style="display: flex; justify-content: center;">
                        <div class="price"></div>
                        <div class="amount_net pl-1 pr-1"></div>
                        <div class="baht"></div>
                    </div>
                </div>
                <div class="modal-footer request_response_btn">
                    <button type="button" class="btn btn-secondary request_fail d-none" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="accept_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-content-style">
                <div class="modal-header modal-header-style">
                </div>
                <div class="modal-body" style="text-align: -webkit-center;">
                    <div class="btn-close-modal">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <h3 style="text-align: center;"><?php echo getWording('payment', 'accept') ?></h3>
                    <button class="btn" type="button" data-bs-dismiss="modal" aria-label="Close" style="width: 142px;height: 40px; background-color: #AAAAAA; border-radius: 23px; padding: 10px; margin:10px;color:white;"><?php echo getWording('index', 'ok') ?></button>
                </div>
                <div class="modal-footer modal-footer-style register_response_btn">

                </div>
            </div>
        </div>
    </div>

</section>

<?php include('footer.php') ?>

<script>
    var url = "<?php echo $api_url; ?>";
    var address_id = $(".address_id_address_edit").val();
    var address_default = '';
    // var address_tax_id = '';
    var payment_id = '';
    var bank_code = '';
    var tax_address_id = '';
    var delivery_name = $(".radio-delivery .delivery_type").data('delivery-name');
    var delivery_code = $(".radio-delivery .delivery_type").val();
    // $(this).val()
    // $(this).data('delivery-name')
    // console.log('tax_address_id',tax_address_id)
    $(document).ready(function() {
        // $('#telephone').mask('000-000-0000');
        var address_name = $('.address_main .address_name').text();
        var address_address = $('.address_main .address_address').text();
        var address_district = $('.address_main .address_district').text();
        var address_province = $('.address_main .address_province').text();
        var address_company = $('.address_main .address_company').text();
        var address_branch = $('.address_main .address_branch').text();
        var address_email = $('.address_main .address_email').text();
        var address_phone = $('.address_main .address_phone').text();

        var tax_address_name = $('.tax_address_main .tax_address_name').text();
        var tax_address_address = $('.tax_address_main .tax_address_address').text();
        var tax_address_district = $('.tax_address_main .tax_address_district').text();
        var tax_address_province = $('.tax_address_main .tax_address_province').text();
        var tax_address_company = $('.tax_address_main .tax_address_company').text();
        var tax_address_branch = $('.tax_address_main .tax_address_branch').text();
        var tax_address_email = $('.tax_address_main .tax_address_email').text();
        var tax_address_phone = $('.tax_address_main .tax_address_phone').text();
        var tax_address_tax = $('.tax_address_main .tax_address_no').text();

        $('#telephone').mask('000-000-0000');
        $('#telephone_address_edit').mask('000-000-0000');
        $('#telephone_address_add').mask('000-000-0000');
        $('#telephone_address_tax_edit').mask('000-000-0000');

        $('#is_default_address_add').val('0');
        $('#is_default').val('0');
        // $('#is_default_address_edit').val('0');
        $('#is_default_address_tax_edit').val('0');

        if ($("#need_tax_invoice").checked) {
            $('.taxaddress_submit').show();
            $('#taxaddress_detail').show();
        } else {
            $('.taxaddress_submit').hide();
            $('#taxaddress_detail').hide();
        }
        // var payment_id = $('input[name=payment_id]:checked').val()
    });

    $("#close_modal9").click(function() {
        // $('#product_minimum_word').empty();
        $(".by_address input").prop('checked', true);
        $(".by_yourself input").prop('checked', false);
    });

    $('body').on('click', '#choose_address .select_address', function() {
        // $("#choose_address .select_address").click(function() {
        address_name = $(this).find('.address_detail .reciver_name').text();
        address_address = $(this).find('.address_detail .reciver_address').text();
        address_district = $(this).find('.address_detail .reciver_district').text();
        address_province = $(this).find('.address_detail .reciver_province').text();
        address_company = $(this).find('.address_detail .reciver_company').text();
        address_branch = $(this).find('.address_detail .reciver_branch').text();
        address_email = $(this).find('.address_detail .reciver_email').text();
        address_phone = $(this).find('.address_detail .reciver_phone').text();
        address_default = $(this).find('.address_detail .reciver_default').text();
        address_id = $(this).find('.address_detail .reciver_id').text();
        $("#choose_address .select_address").removeClass('active');
        $(this).addClass('active');
        console.log(address_id);
    });

    $('body').on('click', '#save', function() {

        $('.address_main .address_name').text(address_name)
        $('.address_main .address_address').text(address_address)
        $('.address_main .address_district').text(address_district)
        $('.address_main .address_province').text(address_province)
        $('.address_main .address_company').text(address_company)
        $('.address_main .address_branch').text(address_branch)
        $('.address_main .address_email').text(address_email)
        $('.address_main .address_phone').text(address_phone)
        $("#main_address_id").val(address_id);

        var name_split = address_name.split(" ");
        var first_name = name_split[0];
        var last_name = name_split[1];

        var email_split = address_email.split(" : ");
        var email = email_split[1]

        var phone_split = address_phone.split(" : ");
        var phone = phone_split[1]

        var address_split = address_address.split(" : ");
        var address_split_edit = address_split[1]

        var company_split = address_company.split(" : ");
        var company_split_edit = company_split[1]

        var branch_split = address_branch.split(" : ");
        var branch_split_edit = branch_split[1]

        $('form#dataForm1 input#name_address_edit').val(first_name);
        $('form#dataForm1 input#lastname_address_edit').val(last_name);
        $('form#dataForm1 input#telephone_address_edit').val(phone);
        $('form#dataForm1 input#email_address_edit').val(email);
        $('form#dataForm1 input#company_address_edit').val(company_split_edit);
        $('form#dataForm1 input#branch_address_edit').val(branch_split_edit);
        $('form#dataForm1 input#address_address_edit').val(address_split_edit);
        $('form#dataForm1 input#address_id_address_edit').val(address_id);

        console.log(address_default);

        if (address_default == 1) {
            $('#is_default_btn').fadeIn();
        } else {
            $('#is_default_btn').fadeOut();
        };
    });

    $(".select_tax_address").click(function() {
        tax_address_name = $(this).find('.tax_address_detail .reciver_tax_name').text();
        tax_address_address = $(this).find('.tax_address_detail .reciver_tax_address').text();
        tax_address_district = $(this).find('.tax_address_detail .reciver_tax_district').text();
        tax_address_province = $(this).find('.tax_address_detail .reciver_tax_province').text();
        tax_address_company = $(this).find('.tax_address_detail .reciver_tax_company').text();
        tax_address_branch = $(this).find('.tax_address_detail .reciver_tax_branch').text();
        tax_address_email = $(this).find('.tax_address_detail .reciver_tax_email').text();
        tax_address_phone = $(this).find('.tax_address_detail .reciver_tax_phone').text();
        tax_address_tax = $(this).find('.tax_address_detail .reciver_tax_tax').text();
        tax_address_default = $(this).find('.tax_address_detail .reciver_tax_default').text();
        tax_address_id = $(this).find('.tax_address_detail .reciver_tax_id').text();
    });

    $("#save_tax").click(function() {
        $('.tax_address_main .tax_address_name').text(tax_address_name)
        $('.tax_address_main .tax_address_address').text(tax_address_address)
        $('.tax_address_main .tax_address_district').text(tax_address_district)
        $('.tax_address_main .tax_address_province').text(tax_address_province)
        $('.tax_address_main .tax_address_company').text(tax_address_company)
        $('.tax_address_main .tax_address_branch').text(tax_address_branch)
        $('.tax_address_main .tax_address_email').text(tax_address_email)
        $('.tax_address_main .tax_address_phone').text(tax_address_phone)
        $('.tax_address_main .tax_address_no').text(tax_address_tax)
        $(".address_tax_id").val(tax_address_id);

        var tax_name_split = tax_address_name.split(" ");
        var tax_first_name = tax_name_split[0];
        var tax_last_name = tax_name_split[1];

        var tax_email_split = tax_address_email.split(" : ");
        var tax_email = tax_email_split[1]

        var tax_phone_split = tax_address_phone.split(" : ");
        var tax_phone = tax_phone_split[1]

        var tax_address_split = tax_address_address.split(" : ");
        var tax_address_split_edit = tax_address_split[1]

        var tax_company_split = tax_address_company.split(" : ");
        var tax_company_split_edit = tax_company_split[1]

        var tax_branch_split = tax_address_branch.split(" : ");
        var tax_branch_split_edit = tax_branch_split[1]

        var tax_no_split = tax_address_tax.split(" : ");
        var tax_no_split_edit = tax_no_split[1]

        $('form#dataForm3 input#name_address_tax_edit').val(tax_first_name);
        $('form#dataForm3 input#lastname_address_tax_edit').val(tax_last_name);
        $('form#dataForm3 input#telephone_address_tax_edit').val(tax_phone);
        $('form#dataForm3 input#email_address_tax_edit').val(tax_email);
        $('form#dataForm3 input#company_address_tax_edit').val(tax_company_split_edit);
        $('form#dataForm3 input#branch_address_tax_edit').val(tax_branch_split_edit);
        $('form#dataForm3 input#address_address_tax_edit').val(tax_address_split_edit);
        $('form#dataForm3 input#tax_address_tax_edit').val(tax_no_split_edit);
        $('form#dataForm3 input#address_id_tax_edit').val(tax_address_id);

        // tax_address_tax_edit


        if (tax_address_default == '1') {
            $('.default_tax_address').fadeIn();
        } else {
            $('.default_tax_address').fadeOut();
        };
    });

    $(".radio-payment .payment_type").change(function() {
        payment_id = $(this).val();
        if (payment_id == '1') {
            $(".credit_submit").show();
            $(".internet_banking_submit").hide();
            $(".promtpay_submit").hide();
            $(".bank").removeClass('d-block');
            $(".bank").addClass('d-none');
            $(".bank input").prop('checked', false);
            // payment_id = $(this).val();
        } else if (payment_id == '2') {
            $(".credit_submit").hide();
            $(".internet_banking_submit").show();
            $(".promtpay_submit").hide();
            $(".bank").addClass('d-block');
            $(".bank").removeClass('d-none');
            $(".bank #scb").prop('checked', true);
        } else if (payment_id == '3') {
            $(".credit_submit").hide();
            $(".internet_banking_submit").hide();
            $(".promtpay_submit").show();
            $(".bank").removeClass('d-block');
            $(".bank").addClass('d-none');
            $(".bank input").prop('checked', false);
        }
    });

    $(".radio-delivery .delivery_type").change(function() {
        if (this.checked) {
            delivery_code = $(this).val();
            delivery_name = $(this).data('delivery-name');
        }
    });

    // $("#same_deliver_address").change(function() {
    //     if (this.checked) {
    //         $(".address-tax").removeClass('d-block');
    //         $(".address-tax").addClass('d-none');
    //     } else {
    //         $(".address-tax").addClass('d-block');
    //         $(".address-tax").removeClass('d-none');
    //     }
    // });

    $("#need_tax_invoice").change(function() {
        if (this.checked) {
            $(".tax-invoive-info").addClass('d-block');
            $(".tax-invoive-info").removeClass('d-none');
            tax_address_id = $(".address_tax_id").val();
            $('.taxaddress_submit').show();
            $('#taxaddress_detail').show();
            $('.please_add_address_tax').show();
        } else {
            $(".tax-invoive-info").removeClass('d-block');
            $(".tax-invoive-info").addClass('d-none');
            tax_address_id = '';
            $('.taxaddress_submit').hide();
            $('#taxaddress_detail').hide();
            $('.please_add_address_tax').hide();
        }
    });

    // $("#accept").change(function() {
    //     if (this.checked) {
    //         $('#confirm_modal').prop("disabled", false); // Element(s) are now enabled.
    //     } else {
    //         $('#confirm_modal').prop("disabled", true); // Element(s) are now enabled.
    //     }
    // });

    $("#confirm_modal").click(function() {
        var accept = $('#accept').prop('checked');
        // console.log(accept);
        if (accept == true) {
            $('#myModal4').modal('show');
        } else {
            $('#accept_modal').modal('show');
        }

    });



    $("#submit-confirm_order").click(function() {
        // validator.resetForm();
        var product_id = $('input.product_id').map(function() {
            return this.value;
        }).get();

        var product_retail_price = $('input.product_retail_price').map(function() {
            return this.value;
        }).get();

        var product_qty = $('input.product_qty').map(function() {
            return this.value;
        }).get();

        var product_total_price = $('input.product_total_price').map(function() {
            return this.value;
        }).get();

        var select_payment_word = '<?php echo getWording('payment', 'select_payment') ?>'

        bank_code = $('input[name=bank_id]:checked').val();
        // var bank_code = $('input[name=bank_id]:checked').val();
        $(".qr_image").empty();
        $(".amount_net").empty();
        $(".price").empty();
        $(".baht").empty();

        // if (!empty($(".address_id").val())) {
        if ($('.payment_type.credit').is(':checked')) {
            // console.log('credit');
            var send_data_url = '<?php echo base_url('Product/payment_creditcard') ?>';
            var data = {
                "member_id": $(".member_id").val(),
                "member_address_id": $(".address_id_address_edit").val(),
                "member_address_tax_id": tax_address_id,
                "payment_id": payment_id,
                "promotion_discount_id": $(".promotion_discount_id").val(),
                "coupon_id": $(".coupon_id").val(),
                "product_price": $(".product_price").val(),
                "delivery_price": $(".delivery_price").val(),
                "discount": $(".discount").val(),
                "net_price": $(".net_price").val(),
                "product_id": product_id,
                "product_retail_price": product_retail_price,
                "product_qty": product_qty,
                "product_total_price": product_total_price,
                "courier_name": delivery_name,
                "courier_code": delivery_code,
            }
            // console.log(data);
            send_data = {
                "url": send_data_url,
                "method": "POST",
                "data": data
            }
            // console.log('credit card', data);

            $.ajax(send_data).done(function(response) {
                // console.log(response);
                if (response.code == "0x0000-00000") {
                    // var myWindow = window.open(response.data.response.form_url, "myWindow", "width=800,height=600");
                    window.location.href = response.data.response.form_url;
                    $('.request_success').addClass('d-none');
                    $('.request_success').removeClass('d-block');
                    $('.request_fail').addClass('d-block');
                    $('.request_fail').removeClass('d-none');
                    $(".request_response").text(response.message);
                } else {
                    $('.request_success').addClass('d-none');
                    // $('#staticBackdrop').modal('show');
                    $('.request_success').removeClass('d-block');
                    $('.request_fail').addClass('d-block');
                    $('.request_fail').removeClass('d-none');
                }
                $('#staticBackdrop').modal('show');
            });
        } else if ($('.payment_type.internet_banking').is(':checked')) {
            // console.log('internet_banking');
            var send_data_url = '<?php echo base_url('Product/payment_internetbanking') ?>';
            var data = {
                "member_id": $(".member_id").val(),
                "member_address_id": $(".address_id_address_edit").val(),
                "member_address_tax_id": tax_address_id,
                "payment_id": payment_id,
                "bank_code": bank_code,
                "promotion_discount_id": $(".promotion_discount_id").val(),
                "coupon_id": $(".coupon_id").val(),
                "product_price": $(".product_price").val(),
                "delivery_price": $(".delivery_price").val(),
                "discount": $(".discount").val(),
                "net_price": $(".net_price").val(),
                "product_id": product_id,
                "product_retail_price": product_retail_price,
                "product_qty": product_qty,
                "product_total_price": product_total_price,
                "courier_name": delivery_name,
                "courier_code": delivery_code,
            }
            // console.log(data);
            send_data = {
                "url": send_data_url,
                "method": "POST",
                "data": data
            }
            // console.log('internet_banking', send_data);

            $.ajax(send_data).done(function(response) {
                // console.log(response);
                if (response.code == "0x0000-00000") {
                    // var myWindow = window.open(response.data.response.authorize_url, "myWindow", "width=800,height=600");
                    window.location.href = response.data.response.authorize_url;
                    $('.request_success').addClass('d-none');
                    $('.request_success').removeClass('d-block');
                    $('.request_fail').addClass('d-block');
                    $('.request_fail').removeClass('d-none');
                    $(".request_response").text(response.message);
                } else {
                    $('.request_success').addClass('d-none');
                    // $('#staticBackdrop').modal('show');
                    $('.request_success').removeClass('d-block');
                    $('.request_fail').addClass('d-block');
                    $('.request_fail').removeClass('d-none');
                }
                $('#staticBackdrop').modal('show');
            });
        } else if ($('.payment_type.promtpay').is(':checked')) {
            // console.log('QR');
            var send_data_url = '<?php echo base_url('Product/payment_qrcode') ?>';
            // console.log('validated');
            var data = {
                "member_id": $(".member_id").val(),
                "member_address_id": $(".address_id_address_edit").val(),
                "member_address_tax_id": tax_address_id,
                "payment_id": payment_id,
                "promotion_discount_id": $(".promotion_discount_id").val(),
                "coupon_id": $(".coupon_id").val(),
                "product_price": $(".product_price").val(),
                "delivery_price": $(".delivery_price").val(),
                "discount": $(".discount").val(),
                "net_price": $(".net_price").val(),
                "product_id": product_id,
                "product_retail_price": product_retail_price,
                "product_qty": product_qty,
                "product_total_price": product_total_price,
                "courier_name": delivery_name,
                "courier_code": delivery_code,
            }
            console.log('data', data);
            send_data = {
                "url": send_data_url,
                "method": "POST",
                "data": data
            }
            // console.log('QR code', send_data);

            $.ajax(send_data).done(function(response) {

                console.log('response', response);
                if (response.code == "0x0000-00000") {
                    // var qr_image = new Image();
                    // qr_image.src = atob(response.data.response.qr_img);
                    var image = new Image();
                    var baht_word = '<?php echo getWording('index', 'baht') ?>';
                    var price_word = '<?php echo getWording('product', 'price') ?>';
                    image.src = 'data:image/png;base64,' + response.data.response.qr_img;
                    $(".qr_image").append(image);
                    $(".amount_net").append(formatNumber(response.data.response.amount_net));
                    $(".price").append(price_word);
                    $(".baht").append(baht_word);
                    // $('.modal.fade').on('hidden.bs.modal', function() {
                    //     location.reload();
                    // })
                    // window.atob(response.data.response.qr_img);
                    // var myWindow = window.open(response.data.response.qr_img, "myWindow", "width=800,height=600");
                    $('.request_success').addClass('d-none');
                    $('.request_success').removeClass('d-block');
                    $('.request_fail').addClass('d-block');
                    $('.request_fail').removeClass('d-none');
                    $(".request_response").text('Please scan QR to pay your order.');
                } else {
                    $('.request_success').addClass('d-none');
                    // $('#staticBackdrop').modal('show');
                    $('.request_success').removeClass('d-block');
                    $('.request_fail').addClass('d-block');
                    $('.request_fail').removeClass('d-none');
                }
                $('#staticBackdrop').modal('show');
            });
        } else {
            alert(select_payment_word);
            return false;
        }

    });
    

    $("#dataForm select[name='province_address_add']").select2({
        placeholder: "",
        width: null,
        allowClear: true,
        ajax: {
            url: url + "api_area/home/select2/address_province",
            dataType: 'json',
            delay: 250,
            cache: true,
            data: function(params) {
                return {
                    search: params.term
                };
            },
            processResults: function(data) {
                return {
                    results: $.map(data.data, function(obj) {
                        return {
                            id: obj.id,
                            text: obj.name
                        };
                    })
                };
            }
        }
    });

    $("#dataForm select[name='district_address_add']").select2({
        placeholder: "",
        width: null,
        allowClear: true,
        ajax: {
            url: function() {
                var query_object = {};
                if ($("#dataForm select[name='province_address_add']").val() != null) {
                    query_object.address_province_id = $("#dataForm select[name='province_address_add']").val();
                }
                var query_string = jQuery.param(query_object);
                return url + "api_area/home/select2/address_district/" + $("#dataForm select[name='province_address_add']").val();
            },
            dataType: 'json',
            delay: 250,
            cache: true,
            data: function(params) {
                return {
                    search: params.term
                };
            },
            processResults: function(data) {
                return {
                    results: $.map(data.data, function(obj) {
                        return {
                            id: obj.id,
                            text: obj.name
                        };
                    })
                };
            }
        }
    });

    $("#dataForm select[name='sub_district_address_add']").select2({
        placeholder: "",
        width: null,
        allowClear: true,
        ajax: {
            url: function() {
                var query_object = {};
                if ($("#dataForm select[name='province_address_add']").val() != null) {
                    query_object.address_province_id = $("#dataForm select[name='province_address_add']").val();
                }
                if ($("#dataForm select[name='district_address_add']").val() != null) {
                    query_object.address_district_id = $("#dataForm select[name='district_address_add']").val();
                }
                var query_string = jQuery.param(query_object);
                return url + "api_area/home/select2/address_sub_district/" + $("#dataForm select[name='district_address_add']").val();
                // console.log('query_string');
            },

            dataType: 'json',
            delay: 250,
            cache: true,
            data: function(params) {
                return {
                    search: params.term
                };
            },
            processResults: function(data) {
                return {
                    results: $.map(data.data, function(obj) {
                        return {
                            id: obj.id,
                            text: obj.name,
                        };
                    })
                };
            }
        }
    });

    $("#dataForm select[name='province_address_add']").on("change", function() {
        $("#dataForm select[name='district_address_add']").select2("val", "");
        $("#dataForm select[name='sub_district_address_add']").select2("val", "");
    });

    $("#dataForm select[name='district_address_add']").on("change", function() {
        $("#dataForm select[name='sub_district_address_add']").select2("val", "");
    });

    $("#dataForm select[name='district_address_add']").on("change", function() {
        var data = {
            "province_id": $("#dataForm select[name='province_address_add']").val(),
            "district_name": $("#select2-district_address_add-container").attr('title'),
        }

        var send_data = {
            "url": "<?php echo base_url('Account/get_postcode') ?>",
            "method": "POST",
            "data": data
        }

        $.ajax(send_data).done(function(response) {
            // console.log(response);
            $('#postcode').val(response.data.postcode);
        });
    });

    // $('body').on('select', "#dataForm1 select[name='province_address_edit']", function() {
    $("#dataForm1 select[name='province_address_edit']").select2({
        placeholder: "",
        width: null,
        allowClear: true,
        ajax: {
            url: url + "api_area/home/select2/address_province",
            dataType: 'json',
            delay: 250,
            cache: true,
            data: function(params) {
                return {
                    search: params.term
                };
            },
            processResults: function(data) {
                return {
                    results: $.map(data.data, function(obj) {
                        return {
                            id: obj.id,
                            text: obj.name
                        };
                    })
                };
            }
        }
    });

    // $('body').on('shown.bs.modal', '#dataForm1 select[name='district_address_edit']', function() {
    $("#dataForm1 select[name='district_address_edit']").select2({
        placeholder: "",
        width: null,
        allowClear: true,
        ajax: {
            url: function() {
                var query_object = {};
                if ($("#dataForm1 select[name='province_address_edit']").val() != null) {
                    query_object.address_province_id = $("#dataForm1 select[name='province_address_edit']").val();
                }
                var query_string = jQuery.param(query_object);
                return url + "api_area/home/select2/address_district/" + $("#dataForm1 select[name='province_address_edit']").val();
            },
            dataType: 'json',
            delay: 250,
            cache: true,
            data: function(params) {
                return {
                    search: params.term
                };
            },
            processResults: function(data) {
                return {
                    results: $.map(data.data, function(obj) {
                        return {
                            id: obj.id,
                            text: obj.name
                        };
                    })
                };
            }
        }
    });

    $("#dataForm1 select[name='sub_district_address_edit']").select2({
        placeholder: "",
        width: null,
        allowClear: true,
        ajax: {
            url: function() {
                var query_object = {};
                if ($("#dataForm1 select[name='province_address_edit']").val() != null) {
                    query_object.address_province_id = $("#dataForm1 select[name='province_address_edit']").val();
                }
                if ($("#dataForm1 select[name='district_address_edit']").val() != null) {
                    query_object.address_district_id = $("#dataForm1 select[name='district_address_edit']").val();
                }
                var query_string = jQuery.param(query_object);
                return url + "api_area/home/select2/address_sub_district/" + $("#dataForm1 select[name='district_address_edit']").val();
            },
            dataType: 'json',
            delay: 250,
            cache: true,
            data: function(params) {
                return {
                    search: params.term
                };
            },
            processResults: function(data) {
                return {
                    results: $.map(data.data, function(obj) {
                        return {
                            id: obj.id,
                            text: obj.name
                        };
                    })
                };
            }
        }
    });

    $("#dataForm1 select[name='province_address_edit']").on("change", function() {
        $("#dataForm1 select[name='district_address_edit']").select2("val", "");
        $("#dataForm1 select[name='sub_district_address_edit']").select2("val", "");
    });

    $("#dataForm1 select[name='district_address_edit']").on("change", function() {
        $("#dataForm1 select[name='sub_district_address_edit']").select2("val", "");
    });

    $("#dataForm1 select[name='district_address_edit']").on("change", function() {
        // console.log($("#select2-district_address_edit-container").attr('title'));
        var data = {

            "province_id": $("#dataForm1 select[name='province_address_edit']").val(),
            "district_name": $("#select2-district_address_edit-container").attr('title'),
        }

        var send_data = {
            "url": "<?php echo base_url('Account/get_postcode') ?>",
            "method": "POST",
            "data": data
        }

        $.ajax(send_data).done(function(response) {
            // console.log(response);
            $('#postcode1').val(response.data.postcode);
        });
    });

    $("#dataForm2 select[name='province_tax_add']").select2({
        placeholder: "",
        width: null,
        allowClear: true,
        ajax: {
            url: url + "api_area/home/select2/address_province",
            dataType: 'json',
            delay: 250,
            cache: true,
            data: function(params) {
                return {
                    search: params.term
                };
            },
            processResults: function(data) {
                return {
                    results: $.map(data.data, function(obj) {
                        return {
                            id: obj.id,
                            text: obj.name
                        };
                    })
                };
            }
        }
    });

    $("#dataForm2 select[name='district_tax_add']").select2({
        placeholder: "",
        width: null,
        allowClear: true,
        ajax: {
            url: function() {
                var query_object = {};
                if ($("#dataForm2 select[name='province_tax_add']").val() != null) {
                    query_object.address_province_id = $("#dataForm2 select[name='province_tax_add']").val();
                }
                var query_string = jQuery.param(query_object);
                return url + "api_area/home/select2/address_district/" + $("#dataForm2 select[name='province_tax_add']").val();
            },
            dataType: 'json',
            delay: 250,
            cache: true,
            data: function(params) {
                return {
                    search: params.term
                };
            },
            processResults: function(data) {
                return {
                    results: $.map(data.data, function(obj) {
                        return {
                            id: obj.id,
                            text: obj.name
                        };
                    })
                };
            }
        }
    });

    $("#dataForm2 select[name='sub_district_tax_add']").select2({
        placeholder: "",
        width: null,
        allowClear: true,
        ajax: {
            url: function() {
                var query_object = {};
                if ($("#dataForm2 select[name='province_tax_add']").val() != null) {
                    query_object.address_province_id = $("#dataForm2 select[name='province_tax_add']").val();
                }
                if ($("#dataForm2 select[name='district_tax_add']").val() != null) {
                    query_object.address_district_id = $("#dataForm2 select[name='district_tax_add']").val();
                }
                var query_string = jQuery.param(query_object);
                return url + "api_area/home/select2/address_sub_district/" + $("#dataForm2 select[name='district_tax_add']").val();
            },
            dataType: 'json',
            delay: 250,
            cache: true,
            data: function(params) {
                return {
                    search: params.term
                };
            },
            processResults: function(data) {
                return {
                    results: $.map(data.data, function(obj) {
                        return {
                            id: obj.id,
                            text: obj.name
                        };
                    })
                };
            }
        }
    });

    $("#dataForm2 select[name='province_tax_add']").on("change", function() {
        $("#dataForm2 select[name='district_tax_add']").select2("val", "");
        $("#dataForm2 select[name='sub_district_tax_add']").select2("val", "");
    });

    $("#dataForm2 select[name='district_tax_add']").on("change", function() {
        $("#dataForm2 select[name='sub_district_tax_add']").select2("val", "");
    });

    $("#dataForm2 select[name='district_tax_add']").on("change", function() {
        var data = {
            "province_id": $("#dataForm2 select[name='province_tax_add']").val(),
            "district_name": $("#select2-district_tax_add-container").attr('title'),
        }

        var send_data = {
            "url": "<?php echo base_url('Account/get_postcode') ?>",
            "method": "POST",
            "data": data
        }

        $.ajax(send_data).done(function(response) {
            // console.log(response);
            $('#postcode2').val(response.data.postcode);
        });
    });

    $("#dataForm3 select[name='province_address_tax_edit']").select2({
        placeholder: "",
        width: null,
        allowClear: true,
        ajax: {
            url: url + "api_area/home/select2/address_province",
            dataType: 'json',
            delay: 250,
            cache: true,
            data: function(params) {
                return {
                    search: params.term
                };
            },
            processResults: function(data) {
                return {
                    results: $.map(data.data, function(obj) {
                        return {
                            id: obj.id,
                            text: obj.name
                        };
                    })
                };
            }
        }
    });

    $("#dataForm3 select[name='district_address_tax_edit']").select2({
        placeholder: "",
        width: null,
        allowClear: true,
        ajax: {
            url: function() {
                var query_object = {};
                if ($("#dataForm3 select[name='province_address_tax_edit']").val() != null) {
                    query_object.address_province_id = $("#dataForm3 select[name='province_address_tax_edit']").val();
                }
                var query_string = jQuery.param(query_object);
                return url + "api_area/home/select2/address_district/" + $("#dataForm3 select[name='province_address_tax_edit']").val();
            },
            dataType: 'json',
            delay: 250,
            cache: true,
            data: function(params) {
                return {
                    search: params.term
                };
            },
            processResults: function(data) {
                return {
                    results: $.map(data.data, function(obj) {
                        return {
                            id: obj.id,
                            text: obj.name
                        };
                    })
                };
            }
        }
    });

    $("#dataForm3 select[name='sub_district_address_tax_edit']").select2({
        placeholder: "",
        width: null,
        allowClear: true,
        ajax: {
            url: function() {
                var query_object = {};
                if ($("#dataForm3 select[name='province_address_tax_edit']").val() != null) {
                    query_object.address_province_id = $("#dataForm3 select[name='province_address_tax_edit']").val();
                }
                if ($("#dataForm3 select[name='district_address_tax_edit']").val() != null) {
                    query_object.address_district_id = $("#dataForm3 select[name='district_address_tax_edit']").val();
                }
                var query_string = jQuery.param(query_object);
                return url + "api_area/home/select2/address_sub_district/" + $("#dataForm3 select[name='district_address_tax_edit']").val();
            },
            dataType: 'json',
            delay: 250,
            cache: true,
            data: function(params) {
                return {
                    search: params.term
                };
            },
            processResults: function(data) {
                return {
                    results: $.map(data.data, function(obj) {
                        return {
                            id: obj.id,
                            text: obj.name
                        };
                    })
                };
            }
        }
    });

    $("#dataForm3 select[name='province_address_tax_edit']").on("change", function() {
        $("#dataForm3 select[name='district_address_tax_edit']").select2("val", "");
        $("#dataForm3 select[name='sub_district_address_tax_edit']").select2("val", "");
    });

    $("#dataForm3 select[name='district_address_tax_edit']").on("change", function() {
        $("#dataForm3 select[name='sub_district_address_tax_edit']").select2("val", "");
    });

    $("#dataForm3 select[name='district_address_tax_edit']").on("change", function() {
        var data = {
            "province_id": $("#dataForm3 select[name='province_address_tax_edit']").val(),
            "district_name": $("#select2-district_address_tax_edit-container").attr('title'),
        }

        var send_data = {
            "url": "<?php echo base_url('Account/get_postcode') ?>",
            "method": "POST",
            "data": data
        }

        $.ajax(send_data).done(function(response) {
            // console.log(response);
            $('#postcode3').val(response.data.postcode);
        });
    });

    $('#is_default').change(function(e) {
        if (this.checked) {
            $('#is_default').prop('checked', true)
            $('#is_default').val('1');
        } else {
            $('#is_default').prop('checked', false)
            $('#is_default').val('0');
        }
    });

    $('#is_default_address_add').change(function(e) {
        if (this.checked) {
            $('#is_default_address_add').prop('checked', true)
            $('#is_default_address_add').val('1');
        } else {
            $('#is_default_address_add').prop('checked', false)
            $('#is_default_address_add').val('0');
        }
    });

    $('#is_default_address_edit').change(function(e) {
        if (this.checked) {
            $('#is_default_address_edit').prop('checked', true)
            $('#is_default_address_edit').val('1');
        } else {
            $('#is_default_address_edit').prop('checked', false)
            $('#is_default_address_edit').val('0');
        }
    });

    $('#is_default_address_tax_edit').change(function(e) {
        if (this.checked) {
            $('#is_default_address_tax_edit').prop('checked', true)
            $('#is_default_address_tax_edit').val('1');
        } else {
            $('#is_default_address_tax_edit').prop('checked', false)
            $('#is_default_address_tax_edit').val('0');
        }
    });

    // var array_province = "";
    // var form = new FormData();
    // var settings = {
    //     "url": "<?php echo base_url('/api_area/home/select2/address_province/') ?>",
    //     "method": "GET",
    //     "timeout": 0,
    //     "headers": {
    //     },
    //     "processData": false,
    //     "mimeType": "multipart/form-data",
    //     "contentType": false,
    //     "data": form
    // };

    // $.ajax(settings).done(function (response) {
    //     const objProvince = JSON.parse(response);
    //     array_province = objProvince.data;
    // });

    // console.log(array_province);

    $("#submit-add").click(function() {
        // console.log($('#name').val());

        $("#dataForm").validate({
            rules: {
                name_address_add: "required",
                lastname_address_add: "required",
                email_address_add: {
                    required: true,
                    email: true
                },
                telephone_address_add: {
                    required: true,
                    pattern: '\\d{3}-\\d{3}-\\d{4}',
                    minlength: 12,
                    maxlength: 12
                },
                address_address_add: "required",
                province_address_add: "required",
                district_address_add: "required",
                sub_district_address_add: "required",
                postcode: "required",
                // company_address_add: "required",
                // branch_address_add: "required",
            },
            messages: {
                name_address_add: "Please enter name",
                lastname_address_add: "Please enter last name",
                email_address_add: "Please enter a valid email address",
                telephone_address_add: "Please enter phone number",
                address_address_add: "Please enter address",
                province_address_add: "Please enter province",
                district_address_add: "Please enter district",
                sub_district_address_add: "Please enter sub-district",
                postcode: "Please enter zip code",
                // company_address_add: "Please enter your company or -",
                // branch_address_add: "Please enter your branch or -",
            },

            errorPlacement: function(error, element) {
                error.insertBefore(element);
            },
            submitHandler: function() {
                // console.log('validated');
                var data = {
                    "name": $("#name_address_add").val(),
                    "lastname": $("#lastname_address_add").val(),
                    "telephone": $("#telephone_address_add").val(),
                    "email": $("#email_address_add").val(),
                    "company": $("#company_address_add").val(),
                    "branch": $("#branch_address_add").val(),
                    "address": $("#address_address_add").val(),
                    "province": $("#province_address_add").val(),
                    "district": $("#district_address_add").val(),
                    "sub_district": $("#sub_district_address_add").val(),
                    "postcode": $("#postcode").val(),
                    "is_default": $("#is_default_address_add").val(),
                }

                var send_data = {
                    "url": "<?php echo base_url('Account/sending_address_add') ?>",
                    "method": "POST",
                    "data": data
                }
                // console.log('data',data)
                $.ajax(send_data).done(function(response) {
                    // console.log('response',response)
                    if (response.code == "0x0000-00000") {

                        $('#choose_address').html('');

                        var member_id = <?php echo $codeigniter_instance->session->userdata('member::id') ?>;
                        console.log(response);
                        console.log(member_id);

                        var form = new FormData();
                        form.append("member_id", member_id);

                        var settings = {
                            "url": "https://nextere.space/api_area/member/address_all/" + "/" + member_id,
                            "method": "GET",
                            "timeout": 0,
                            "headers": {},
                            "processData": false,
                            "mimeType": "multipart/form-data",
                            "contentType": false,
                            "data": form
                        };

                        $.ajax(settings).done(function(response) {

                            var objJSON = JSON.parse(response);

                            if (objJSON.code == "0x0000-00000") {

                                const all_address = objJSON.data.member_address;
                                console.log('all_address', all_address);

                                if (all_address.length == 1) {
                                    $('#collapseThree .accordion-body .address_list_detail').html(null);
                                    $('#button_address_list').html(null);
                                    $('#collapseThree .accordion-body .address_list_detail').html(`<div class="row" id="address_list" style="padding-bottom: 20px;">` +
                                        `<div class="address mt-2" id="shipping_info" style="width: 100%; height: auto; border: solid #DBDBDB;">` +
                                        `<div class="row address_main p-5">` +

                                        `<div class="col-xl-5">` +
                                        `<h5 class="address_name">` + all_address[0].first_name + ` ` + all_address[0].last_name + `</h5>` +
                                        `<p class="address_address pt-2"><b><?php echo getWording('profile', 'address') ?> : </b>` +
                                        all_address[0].address +
                                        `</p>` +
                                        `<p class="address_district"><b><?php echo getWording('profile', 'sub_district') ?> :</b> ` +
                                        (all_address[0].address_sub_district.name != null ? all_address[0].address_sub_district.name : '') + ` ` + `<b><?php echo getWording('profile', 'district') ?> :</b> ` +
                                        all_address[0].address_district.name +
                                        `</p>` +
                                        `<p class="address_province"><b><?php echo getWording('profile', 'province') ?> : </b>` + all_address[0].address_province.name + ` ` + all_address[0].postcode +
                                        `<p class="address_company">` +
                                        (all_address[0].company_name != '' ? '<b><?php echo getWording('profile', 'company') ?> : </b> ' + all_address[0].company_name + '</p>' : '') +
                                        `</p>` +
                                        `<p class="address_branch">` +
                                        (all_address[0].company_branch != '' ? '<b><?php echo getWording('profile', 'company') ?> : </b> ' + all_address[0].company_branch + '</p>' : '') +
                                        `</p>` +
                                        `<p class="address_email"><b><?php echo getWording('profile', 'email') ?> : </b>` + all_address[0].email + `</p>` +
                                        `<p class="reciver_phone"><b><?php echo getWording('profile', 'phone') ?> : </b>  ` + all_address[0].telephone + `</p>` +
                                        `</div>` +
                                        `<div class="col-xl-5"></div>` +
                                        `<div class="col-xl-2" style="text-align: right;">
                                            <button id="edit_main_address"  style="background-color: white; border: none;">
                                                <?php echo getWording('profile', 'edit') ?>
                                            </button>
                                        </div>` +
                                        `</div>` +
                                        `</div>` +
                                        `<input class="address_id" name="address_id" id="main_address_id" value="` + all_address[0].id + `" hidden>` +
                                        (all_address[0].is_default == 1 ? `<div id="is_default_btn" class="default_address row p-4"><div style="width: 123px; height: 34px; background: #E6E5E5; border-radius: 3px; border:0px; padding:3px; text-align: center;"><div style="padding-top: 4.5px;"><?php echo getWording('profile', 'main_address') ?></div></div></div>` : ``) +
                                        `</div></div>` +
                                        ``
                                    );

                                    $('#button_address_list').html(`<div class="row" id="button_address_list">
                                        <div class="col-sm-12 col-xl-3 mt-3 ">
                                        </div>
                                        <div class="col-sm-12 col-xl-3 mt-3 text-center">
                                        <button data-bs-toggle="modal" data-bs-target="#myModal1" style="background-color:#FFFFFF; border: 1px solid #555555; border-radius: 23px; width:200px; height:45px;">
                                        <b><?php echo getWording('payment', 'choose_address') ?></b>
                                        </button>
                                        </div>
                                        <div class="col-sm-12 col-xl-3 mt-3 text-center">
                                        <button data-bs-toggle="modal" data-bs-target="#myModal2" style="background-color:#FFFFFF; border: 1px solid #555555; border-radius: 23px; width:200px; height:45px;">
                                        <b>+ <?php echo getWording('payment', 'add_new_address') ?></b>
                                        </button>
                                        </div>
                                        <div class="col-sm-12 col-xl-3 mt-3 ">
                                        </div>
                                        </div>`);

                                }


                                all_address.forEach(function callbackFn(element, index) {

                                    $('#choose_address').append(`<button class="select_address btn mt-2">` +
                                        `<div class="row address_detail p-5">` +
                                        `<div class="col-xl-5 text-start">` +
                                        `<H5 class="reciver_name">` + element.first_name + ` ` + element.last_name + `</H5>` +
                                        `<p class="reciver_address pt-2"><b><?php echo getWording('profile', 'address') ?> : </b>` + element.address +
                                        `</p>` +
                                        `<p class="reciver_district"><b><?php echo getWording('profile', 'sub_district') ?> : </b>` + element.address_sub_district.name + `<b class="ms-2"><?php echo getWording('profile', 'district') ?> : </b> ` + element.address_district.name + `</p>` +
                                        `<p class="reciver_province"><b><?php echo getWording('profile', 'province') ?> : </b>` + element.address_province.name + ` ` + element.postcode + `</p>` +

                                        `<p class="reciver_company">` +
                                        (element.company_name != '' ? '<b><?php echo getWording('profile', 'company') ?> : </b> ' + element.company_name + '</p>' : '') +
                                        `</p>` +
                                        `<p class="reciver_branch">` +
                                        (element.company_branch != '' ? '<b><?php echo getWording('profile', 'company') ?> : </b> ' + element.company_branch + '</p>' : '') +
                                        `</p>` +

                                        `<p class="reciver_email"><b><?php echo getWording('profile', 'email') ?> : </b>  ` + element.email + `</p>` +
                                        `<p class="reciver_phone"><b><?php echo getWording('profile', 'phone') ?> : </b>  ` + element.telephone + `</p>` +
                                        `<p class="reciver_id" hidden>` + element.id + `</p>` +
                                        `<p class="reciver_default" hidden>` + element.is_default + `</p>` +

                                        (element.is_default == 1 ? '<div style="width: 123px; height: 34px; background: #E6E5E5; border-radius: 3px; border:0px; padding:3px; text-align: center;"><div style="padding-top: 4.5px;"><?php echo getWording('profile', 'main_address') ?></div>' : '') +
                                        '</div>' +
                                        `</div>` +
                                        `</div>` +
                                        `</button>`
                                    );
                                });



                                $("#myModal2 .btn-close").click();
                                document.getElementById("dataForm").reset();
                            }
                        });


                        // location.reload();
                    } else {
                        $('.request_success').addClass('d-none');
                        $('#staticBackdrop').modal('show');
                        $('.request_success').removeClass('d-block');
                        $('.request_fail').addClass('d-block');
                        $('.request_fail').removeClass('d-none');
                    }
                });
            }
        });
    });

    $('body').on('click', '#is_default_address_edit', function() {
        // $('.myCheckbox').prop('checked');
        if ($('#is_default_address_edit').prop('checked') == true) {
            $(this).val(1);
        } else {
            $(this).val(0);
        }
    });

    $('body').on('click', '#submit-edit', function() {
        // console.log($('#name').val());

        $("#dataForm1").validate({
            rules: {
                name_address_edit: "required",
                lastname_address_edit: "required",
                email_address_edit: {
                    required: true,
                    email: true
                },
                telephone_address_edit: {
                    required: true,
                    pattern: '\\d{3}-\\d{3}-\\d{4}',
                    minlength: 12,
                    maxlength: 12
                },
                address_address_edit: "required",
                province_address_edit: "required",
                district_address_edit: "required",
                sub_district_address_edit: "required",
                postcode1_address_edit: "required",
            },
            messages: {
                name_address_edit: "Please enter name",
                lastname_address_edit: "Please enter last name",
                email_address_edit: "Please enter a valid email address",
                telephone_address_edit: "Please enter phone number",
                address_address_edit: "Please enter address",
                province_address_edit: "Please enter province",
                district_address_edit: "Please enter district",
                sub_district_address_edit: "Please enter sub-district",
                postcode1_address_edit: "Please enter zip code",
            },

            errorPlacement: function(error, element) {
                error.insertBefore(element);
            },
            submitHandler: function() {
                // console.log('validated');
                var data = {
                    "address_id": $("#address_id_address_edit").val(),
                    "name": $("#name_address_edit").val(),
                    "lastname": $("#lastname_address_edit").val(),
                    "telephone": $("#telephone_address_edit").val(),
                    "email": $("#email_address_edit").val(),
                    "company": $("#company_address_edit").val(),
                    "branch": $("#branch_address_edit").val(),
                    "address": $("#address_address_edit").val(),
                    "province": $("#province_address_edit").val(),
                    "district": $("#district_address_edit").val(),
                    "sub_district": $("#sub_district_address_edit").val(),
                    "postcode": $("#postcode1").val(),
                    "is_default": $("#is_default_address_edit").val(),
                }
                console.log(data);
                var send_data = {
                    "url": "<?php echo base_url('Account/sending_address_edit') ?>",
                    "method": "POST",
                    "data": data
                }

                $.ajax(send_data).done(function(response) {
                    // console.log(response)
                    if (response.code == "0x0000-00000") {
                        // location.reload();
                        $('#choose_address').html('');
                        // $('#shipping_info').html('');
                        var member_id = <?php echo $codeigniter_instance->session->userdata('member::id') ?>;

                        var form = new FormData();
                        form.append("member_id", member_id);

                        var settings = {
                            "url": "https://nextere.space/api_area/member/address_all/" + "/" + member_id,
                            "method": "GET",
                            "timeout": 0,
                            "headers": {},
                            "processData": false,
                            "mimeType": "multipart/form-data",
                            "contentType": false,
                            "data": form
                        };

                        $.ajax(settings).done(function(response) {

                            var main_address_word = '<?php echo getWording('profile', 'main_address') ?>';

                            var objJSON = JSON.parse(response);

                            if (objJSON.code == "0x0000-00000") {

                                const all_address = objJSON.data.member_address;

                                console.log('all_address', all_address);

                                all_address.forEach(function callbackFn(element, index) {

                                    $('#choose_address').append(`<button class="select_address btn mt-2">` +
                                        `<div class="row address_detail p-5">` +
                                        `<div class="col-xl-5 text-start">` +
                                        `<H5 class="reciver_name">` + element.first_name + ` ` + element.last_name + `</H5>` +
                                        `<p class="reciver_address pt-2"><b><?php echo getWording('profile', 'address') ?> : </b>` + element.address +
                                        `</p>` +
                                        `<p class="reciver_district"><b><?php echo getWording('profile', 'sub_district') ?> : </b>` + element.address_sub_district.name + `<b class="ms-2"><?php echo getWording('profile', 'district') ?> : </b> ` + element.address_district.name + `</p>` +
                                        `<p class="reciver_province"><b><?php echo getWording('profile', 'province') ?> : </b>` + element.address_province.name + ` ` + element.postcode + `</p>` +

                                        `<p class="reciver_company">` +
                                        (element.company_name != '' ? '<b><?php echo getWording('profile', 'company') ?> : </b> ' + element.company_name + '</p>' : '') +
                                        `</p>` +
                                        `<p class="reciver_branch">` +
                                        (element.company_branch != '' ? '<b><?php echo getWording('profile', 'company') ?> : </b> ' + element.company_branch + '</p>' : '') +
                                        `</p>` +

                                        `<p class="reciver_email"><b><?php echo getWording('profile', 'email') ?> : </b>  ` + element.email + `</p>` +
                                        `<p class="reciver_phone"><b><?php echo getWording('profile', 'phone') ?> : </b>  ` + element.telephone + `</p>` +
                                        `<p class="reciver_id" hidden>` + element.id + `</p>` +
                                        `<p class="reciver_default" hidden>` + element.is_default + `</p>` +

                                        (element.is_default == 1 ? '<div style="width: 123px; height: 34px; background: #E6E5E5; border-radius: 3px; border:0px; padding:3px; text-align: center;"><div style="padding-top: 4.5px;"><?php echo getWording('profile', 'main_address') ?></div>' : '') +
                                        '</div>' +
                                        `</div>` +
                                        `</div>` +
                                        `</button>`
                                    );

                                    if (data.address_id == element.id) {
                                        $('#collapseThree .accordion-body .address_list_detail').html(null);
                                        $('#collapseThree .accordion-body .address_list_detail').html(`<div class="row" id="address_list" style="padding-bottom: 20px;">` +
                                            `<div class="address mt-2" id="shipping_info" style="width: 100%; height: auto; border: solid #DBDBDB;">` +
                                            `<div class="row address_main p-5">` +

                                            `<div class="col-xl-5">` +
                                            `<h5 class="address_name">` + element.first_name + ` ` + element.first_name + `</h5>` +
                                            `<p class="address_address pt-2"><b><?php echo getWording('profile', 'address') ?> : </b>` +
                                            element.address +
                                            `</p>` +
                                            `<p class="address_district"><b><?php echo getWording('profile', 'sub_district') ?> :</b> ` +
                                            (element.address_sub_district.name != null ? element.address_sub_district.name : '') + ` ` + `<b><?php echo getWording('profile', 'district') ?> :</b> ` +
                                            element.address_district.name +
                                            `</p>` +
                                            `<p class="address_province"><b><?php echo getWording('profile', 'province') ?> : </b>` + element.address_province.name + ` ` + element.postcode +
                                            `<p class="address_company">` +
                                            (element.company_name != '' ? '<b><?php echo getWording('profile', 'company') ?> : </b> ' + element.company_name + '</p>' : '') +
                                            `</p>` +
                                            `<p class="address_branch">` +
                                            (element.company_branch != '' ? '<b><?php echo getWording('profile', 'company') ?> : </b> ' + element.company_branch + '</p>' : '') +
                                            `</p>` +
                                            `<p class="address_email"><b><?php echo getWording('profile', 'email') ?> : </b>` + element.email + `</p>` +
                                            `<p class="reciver_phone"><b><?php echo getWording('profile', 'phone') ?> : </b>  ` + element.telephone + `</p>` +
                                            `</div>` +
                                            `<div class="col-xl-5"></div>` +
                                            `<div class="col-xl-2" style="text-align: right;">
                                            <button id="edit_main_address" style="background-color: white; border: none;" data-bs-toggle="modal" data-bs-target="#myModal3">
                                            <?php echo getWording('profile', 'edit') ?>
                                            </button>
                                            </div>` +
                                            `</div>` +
                                            `</div>` +
                                            `<input class="address_id" name="address_id" id="main_address_id" value="` + element.id + `" hidden="">` +
                                            (element.is_default == 1 ? `<div id="is_default_btn" class="default_address row p-4"><div style="width: 123px; height: 34px; background: #E6E5E5; border-radius: 3px; border:0px; padding:3px; text-align: center;"><div style="padding-top: 4.5px;"><?php echo getWording('profile', 'main_address') ?></div></div></div>` : ``) +
                                            `</div></div>` +
                                            ``
                                        );
                                    }
                                })

                                $("#myModal3 .btn-close").click();
                                document.getElementById("dataForm1").reset();

                            }
                        });

                    } else {
                        $('.request_success').addClass('d-none');
                        $('#staticBackdrop').modal('show');
                        $('.request_success').removeClass('d-block');
                        $('.request_fail').addClass('d-block');
                        $('.request_fail').removeClass('d-none');
                    }
                });
            }
        });
    });

    $("#submit-add_tax").click(function() {
        // console.log($('#name').val());
        $("#dataForm2").validate({
            rules: {
                name: "required",
                lastname: "required",
                email: {
                    required: true,
                    email: true
                },
                telephone: {
                    required: true,
                    pattern: '\\d{3}-\\d{3}-\\d{4}',
                    minlength: 12,
                    maxlength: 12
                },
                address: "required",
                province: "required",
                district: "required",
                sub_district: "required",
                postcode2: "required",
                tax: "required"
            },
            messages: {
                name: "Please enter name",
                lastname: "Please enter last name",
                email: "Please enter a valid email address",
                telephone: "Please enter phone number",
                address: "Please enter address",
                province: "Please enter province",
                district: "Please enter district",
                sub_district: "Please enter sub-district",
                postcode2: "Please enter postal",
                tax: "Please enter tax ID",
            },

            errorPlacement: function(error, element) {
                error.insertBefore(element);
            },
            submitHandler: function() {
                var data = {
                    "name": $("#name").val(),
                    "lastname": $("#lastname").val(),
                    "telephone": $("#telephone").val(),
                    "email": $("#email").val(),
                    "company": $("#company").val(),
                    "branch": $("#branch").val(),
                    "address": $("#address").val(),
                    "province": $("#province_tax_add").val(),
                    "district": $("#district_tax_add").val(),
                    "sub_district": $("#sub_district_tax_add").val(),
                    "postcode": $("#postcode2").val(),
                    "tax": $("#tax").val(),
                    "is_default": $("#is_default").val(),
                }
                // console.log('data', data)
                var send_data = {
                    "url": "<?php echo base_url('Account/tax_invoice_address_add') ?>",
                    "method": "POST",
                    "data": data
                }

                $.ajax(send_data).done(function(response) {
                    // console.log(response)
                    if (response.code == "0x0000-00000") {
                        location.reload();
                    } else {
                        $('.request_success').addClass('d-none');
                        $('#staticBackdrop').modal('show');
                        $('.request_success').removeClass('d-block');
                        $('.request_fail').addClass('d-block');
                        $('.request_fail').removeClass('d-none');
                    }
                });
            }
        });
    });

    $('body').on('click', '#edit_main_address', function() {
        var main_address_id = $('#main_address_id').val();

        console.log(main_address_id);

        $('#myModal3').modal('show');

        var member_id = <?php echo $codeigniter_instance->session->userdata('member::id') ?>;

        var form = new FormData();
        form.append("member_id", member_id);

        var settings = {
            "url": "https://nextere.space/api_area/member/address_all/" + "/" + member_id,
            "method": "GET",
            "timeout": 0,
            "headers": {},
            "processData": false,
            "mimeType": "multipart/form-data",
            "contentType": false,
            "data": form
        };

        $.ajax(settings).done(function(response) {

            var main_address_word = '<?php echo getWording('profile', 'main_address') ?>';

            var objJSON = JSON.parse(response);

            if (objJSON.code == "0x0000-00000") {

                const all_address = objJSON.data.member_address;

                console.log('all_address', all_address);

                all_address.forEach(function callbackFn(element, index) {

                    if (main_address_id == element.id) {
                        console.log('FOUND !!');
                        $('#dataForm1').html(null);
                        $('#dataForm1').append(`<div class="row pl-8 pr-8">` +

                            `<div class="col-xl-6 name pt-5"><b><?php echo getWording('profile', 'name') ?> *</b>
                            <input type="text" id="name_address_edit" name="name_address_edit" value="` + element.first_name + `" placeholder="<?php echo getWording('profile', 'name') ?>" required>
                            </div>` +

                            `<div class="col-xl-6 lastname pt-5">
                            <b><?php echo getWording('profile', 'lastname') ?> *</b>
                            <input type="text" id="lastname_address_edit" name="lastname_address_edit" value="` + element.last_name + `" placeholder="<?php echo getWording('profile', 'lastname') ?>" required>
                            </div>` +

                            `<div class="col-xl-6 phone pt-5">
                            <b><?php echo getWording('profile', 'phone') ?> *</b>
                            <input type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" id="telephone_address_edit" name="telephone_address_edit" value="` + element.telephone + `" placeholder="<?php echo getWording('profile', 'phone') ?>" required>
                            </div>` +

                            `<div class="col-xl-6 email pt-5">
                            <b><?php echo getWording('profile', 'email') ?> *</b>
                            <input type="text" id="email_address_edit" name="email_address_edit" value="` + element.email + `" placeholder="<?php echo getWording('profile', 'email') ?>" required>
                            </div>` +

                            `<div class="col-xl-6 company pt-5">
                            <b><?php echo getWording('profile', 'company') ?></b>
                            <input type="text" id="company_address_edit" name="company_address_edit" value="` + element.company_name + `" placeholder="<?php echo getWording('profile', 'company') ?>">
                            </div>` +

                            `<div class="col-xl-6 branch pt-5">
                            <b><?php echo getWording('profile', 'branch') ?></b>
                            <input type="text" id="branch_address_edit" name="branch_address_edit" value="` + element.company_branch + `" placeholder="<?php echo getWording('profile', 'branch') ?>">
                            </div>` +

                            `<div class="col-xl-6 address pt-5">
                            <b><?php echo getWording('profile', 'address') ?> *</b>
                            <input type="text" id="address_address_edit" name="address_address_edit" value="` + element.address + `" placeholder="<?php echo getWording('profile', 'address') ?>">
                            </div>` +

                            `<div class="col-xl-6 province pt-5">
                            <div class="form-group">
                            <b><?php echo getWording('profile', 'province') ?> *</b>
                            <!-- <input type="text" id="province" name="province" placeholder="จังหวัด"> -->

                            <select class="form-control select select2" id="province_address_edit" name="province_address_edit">
                                <option value="` + element.address_province.id + `" selected>` + element.address_province.name + `</option>
                            </select>
                            </div>
                            </div>` +

                            `<div class="col-xl-6 district pt-5">
                            <div class="form-group">
                            <b><?php echo getWording('profile', 'district') ?> *</b>
                            <!-- <input type="text" id="district" name="district" placeholder="อำเภอ/เขต"> -->
                            <select class="form-control select select2" id="district_address_edit" name="district_address_edit">
                                <option value="` + element.address_district.id + `" selected>` + element.address_district.name + `</option>
                            </select>
                            </div>
                            </div>` +

                            `<div class="col-xl-6 sub-district mt-5">
                            <div class="form-group">
                            <b><?php echo getWording('profile', 'sub_district') ?></b>
                            <!-- <input type="text" id="subdistrict" name="subdistrict" placeholder="ตำบล/แขวง"> -->

                            <select class="form-control select select2" id="sub_district_address_edit" name="sub_district_address_edit">

                                <option value="` + element.address_sub_district.id + `" selected>` + element.address_sub_district.name + `</option>

                            </select>
                            </div>
                            </div>` +

                            `<div class="col-xl-6 postcode mt-5">
                            <b><?php echo getWording('profile', 'postcode') ?> *</b>
                            <input type="text" id="postcode1" name="postcode1" value="` + element.postcode + `" placeholder="<?php echo getWording('profile', 'postcode') ?>">
                            </div>` +

                            `<div class="pt-1">
                                <input type="checkbox" name="is_default_address_edit" id="is_default_address_edit" value="` + element.is_default + `" ` + (element.is_default == 1 ? `checked` : ``) + `> <?php echo getWording('profile', 'set_address') ?>
                            </div>` +

                            `<input class="address_id_address_edit" id="address_id_address_edit" name="address_id_address_edit" value="` + element.id + `" hidden>` +

                            `<div class="button4 pt-4" style="text-align: center;">
                            <div class="submit pt-4" style="text-align: center; padding-bottom: 40px;">
                            <button id="submit-edit" style="width: 208px; height:45px; border:solid 0px; border-radius: 23px; padding: 9px; margin:10px; background-color:#AAAAAA; color:#FFFFFF;"><?php echo getWording('profile', 'save') ?></button>
                            </div>
                            </div>` +

                            `</div>`
                        );

                        $('#myModal3').modal('show');
                    }
                })
            }
        });

    });

    $("#submit-edit_tax").click(function() {
        // console.log($('#name').val());

        $("#dataForm3").validate({
            rules: {
                name_address_tax_edit: "required",
                lastname_address_tax_edit: "required",
                email_address_tax_edit: {
                    required: true,
                    email: true
                },
                telephone_address_tax_edit: {
                    required: true,
                    pattern: '\\d{3}-\\d{3}-\\d{4}',
                    minlength: 12,
                    maxlength: 12
                },
                address_address_tax_edit: "required",
                province: "required",
                district: "required",
                sub_district: "required",
                postcode: "required",
                tax: "required",
            },
            messages: {
                name_address_tax_edit: "Please enter name",
                lastname_address_tax_edit: "Please enter last name",
                email_address_tax_edit: "Please enter a valid email address",
                telephone_address_tax_edit: "Please enter phone number",
                address_address_tax_edit: "Please enter address",
                province: "Please enter province",
                district: "Please enter district",
                sub_district: "Please enter sub-district",
                postcode: "Please enter zip code",
                tax: "Please enter tax ID",
            },

            errorPlacement: function(error, element) {
                error.insertBefore(element);
            },
            submitHandler: function() {
                // console.log('validated');
                var data = {
                    "address_id": $("#address_id_tax_edit").val(),
                    "name": $("#name_address_tax_edit").val(),
                    "lastname": $("#lastname_address_tax_edit").val(),
                    "telephone": $("#telephone_address_tax_edit").val(),
                    "email": $("#email_address_tax_edit").val(),
                    "company": $("#company_address_tax_edit").val(),
                    "branch": $("#branch_address_tax_edit").val(),
                    "address": $("#address_address_tax_edit").val(),
                    "province": $("#province_address_tax_edit").val(),
                    "district": $("#district_address_tax_edit").val(),
                    "sub_district": $("#sub_district_address_tax_edit").val(),
                    "postcode": $("#postcode3").val(),
                    "tax": $("#tax_address_tax_edit").val(),
                    "is_default": $("#is_default_address_tax_edit").val(),
                }

                var send_data = {
                    "url": "<?php echo base_url('Account/tax_invoice_address_edit') ?>",
                    "method": "POST",
                    "data": data
                }

                $.ajax(send_data).done(function(response) {
                    if (response.code == "0x0000-00000") {
                        location.reload();
                    } else {
                        $('.request_success').addClass('d-none');
                        $('#staticBackdrop').modal('show');
                        $('.request_success').removeClass('d-block');
                        $('.request_fail').addClass('d-block');
                        $('.request_fail').removeClass('d-none');
                    }
                });
            }
        });
    });
</script>

<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->