<?php include('header.php') ?>

<style>
    input[type=text],
    select,
    textarea {
        padding: 12px;
        margin-top: 6px;
        margin-bottom: 16px;
        width: 100%;
        height: 45px;
        border: 1px solid #707070;
        border-radius: 5px;
    }
</style>


<body>
    <div class="container">
        <div class="head76 mt-5">
            <h3 style="font-weight: bold; text-align:center">แจ้งโอนเงิน</h3>
        </div>


        <div class="col-sm-12 col-xl-12" style="border:solid 1px; width: auto; height: auto; margin:5%; padding:5%">
            <div class="row" style="padding:10px">
                <div class="col-sm-6 col-xl-6">
                    <b>เลขที่คำสั่งซื้อ</b>
                    <div class="OrderNo col-sm-6 col-xl-10 pt-2">
                        <input type="text" id="OrderNo" onKeyPress="return isNumber(event)" min="0" maxlength="" value="202006159" required>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-6">
                    <b>จำนวนเงินที่ชำระ</b>
                    <div class="Amount col-sm-6 col-xl-10 pt-2">
                        <input type="text" id="Amount" name="Amount" value="1590.00" required>
                    </div>
                </div>

            </div>

            <div class="row" style="padding:10px">
                <div class="col-sm-6 col-xl-6">
                    <b>โอนจากธนาคาร</b>
                    <div class="bank col-sm-6 col-xl-10 pt-2">
                        <select id="bank" name="bank">
                            <option value="KBANK">ธนาคารกสิกร</option>
                            <option value="BBL">ธนาคารกรุงเทพ</option>
                            <option value="KTB">ธนาคารกรุงไทย</option>
                            <option value="SCB">ธนาคารไทยพาณิชย์</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row" style="padding:10px">
                <div class="col-sm-6 col-xl-12">
                    <b>ไปยังธนาคาร</b>
                    <div class="pt-5">
                        <input type="radio" id="bank1" name="MyBank" value="bank1">
                        <label class="col-xl-12" for="bank1">
                            <div class="row">
                                <div class="col-sm-6 col-xl-1">

                                </div>
                                <div class="col-sm-6 col-xl-1">
                                    <!-- <div style="width: 50px; height:50px; border-radius: 50%; border:solid 1px">
                                        img
                                    </div> -->
                                </div>
                                <div class="col-sm-6 col-xl-5">
                                    <p>ธนาคารกสิกร สาขาใหญ่</p>
                                    <b>000-0-00000-0</b>
                                </div>
                                <div class="col-sm-6 col-xl-5">
                                    ชื่อบัญชี : บริษัท key2iot จำกัด
                                </div>
                            </div>
                        </label>
                    </div>
                    <div class="pt-5">
                        <input type="radio" id="bank2" name="MyBank" value="bank2">
                        <label class="col-xl-12" for="bank2">
                            <div class="row">
                                <div class="col-sm-6 col-xl-1">

                                </div>
                                <div class="col-sm-6 col-xl-1">
                                    <!-- <div style="width: 50px; height:50px; border-radius: 50%; border:solid 1px">
                                        img
                                    </div> -->
                                </div>
                                <div class="col-sm-6 col-xl-5">
                                    <p>ธนาคารกรุงเทพ สาขาใหญ่</p>
                                    <b>000-0-00000-0</b>
                                </div>
                                <div class="col-sm-6 col-xl-5">
                                    ชื่อบัญชี : บริษัท key2iot จำกัด
                                </div>
                            </div>
                        </label>
                    </div>
                </div>

            </div>

            <div class="row pt-5" style="padding:10px">
                <div class="col-sm-6 col-xl-6">
                    <b>วันที่ชำระ</b>
                    <div class="PayDate col-sm-6 col-xl-10 pt-2">
                        <input type="date" id="PayDate" name="PayDate" style="width: 100%; height:40px;">
                    </div>
                </div>
                <div class="col-sm-6 col-xl-6">
                    <b>เวลาที่ชำระ</b>
                    <div class="PayTime col-sm-6 col-xl-10 pt-2">
                        <input type="time" id="PayTime" name="PayTime" style="width: 100%; height:40px;">
                    </div>
                </div>
            </div>

            <div class="row pt-5" style="padding:10px">
                <div class="col-sm-6 col-xl-6">
                    <b>ไฟล์แนบ</b>
                    <div class="file col-sm-6 col-xl-10 pt-2">
                        <input type="file" id="file" name="file" size="60px">
                    </div>
                </div>
            </div>

            <div class="row pt-5" style="padding:10px">
                <div class="col-sm-6 col-xl-11">
                    <b>หมายเหตุ</b>
                    <div class="message pt-2">
                        <textarea id="message" name="message" style="width: 100%; height: 100px;"></textarea>
                    </div>
                </div>
            </div>
            <div class="button1 col-sm-2 col-xl-12" style="text-align: center;">
                <button class="facebook" style="width: 200px; border: 0px; background-color: #AAAAAA; border-radius: 23px; padding: 9px; margin:10px; color: #ffffff;">
                    แจ้งโอนเงิน
                </button>
            </div>
        </div>


    </div>



<!-- 
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/fontawesome.min.js" integrity="sha512-KCwrxBJebca0PPOaHELfqGtqkUlFUCuqCnmtydvBSTnJrBirJ55hRG5xcP4R9Rdx9Fz9IF3Yw6Rx40uhuAHR8Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://vjs.zencdn.net/7.11.4/video.min.js"></script> -->

    <script>
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }

            return true;
        }
    </script>

    <div class="mt-5">
        <?php include('footer.php') ?>
    </div>

</body>