<?php include("header.php") ?>


<section class="shopping-cart my-3">
    <div class="container">
        <div class="row">
            <?php if ($token != '') { ?>
                <div class="col-12 col-md-9">
                    <div class="col-md-11 d-flex justify-content-between align-items-center mt-5 ml-5">
                        <h2><b><?php echo getWording('product', 'cart') ?></b></h2>
                        <a href="<?php echo base_url('Product?type=all_product') ?>"><i class="fas fa-chevron-left"></i> <span> <b><?php echo getWording('product', 'view') ?></b></span></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-xl-9">
                        <div class="table-responsive">
                            <div class="nav-link" id="cart_header">
                                <span class="pr-2 pl-0 py-1 rounded cart_count" data-cart-items="<?php echo isset($product_cart) ? count($product_cart) : 0; ?>">
                                </span>
                                <div class="row" style="justify-content: center;">
                                    <img class="loading" style=" position: absolute; z-index: -1; width: 50px; padding-top: 80px;" src="<?php echo base_url('/assets/img/icon/loading.gif') ?>">
                                </div>
                                <table class="cart-table table">
                                    <thead>
                                        <tr style="background-color: #7C7C7C;">
                                            <th></th>
                                            <th style="color: white;"><?php echo getWording('product', 'product') ?></th>
                                            <th style="color: white;"><?php echo getWording('product', 'price') ?></th>
                                            <th style="color: white;"><?php echo getWording('product', 'quantity') ?></th>
                                            <th style="color: white;"><?php echo getWording('product', 'total_price') ?></th>
                                            <th></th>
                                        </tr>
                                    </thead>

                                    <tbody id="shopping_cart_list" style="background-color: #FFFFFF;">
                                        <?php if (!empty($product_cart)) { ?>
                                            <?php foreach ($product_cart as $key => $product_cart_list) { ?>
                                                <?php

                                                // $product_check = json_decode(file_get_contents(base_url() . 'Product/details/' . $product_cart_list->{'product::id'} . '/check'));

                                                if (empty($product_cart_list->{'product::discount'})) {
                                                    $retail_price = $product_cart_list->{'product::retail_price'};
                                                } else {
                                                    $retail_price = $product_cart_list->{'product::discount'}->total_price;
                                                }
                                                if ($product_cart_list->prdocut_price != "") {
                                                    foreach ($product_cart_list->prdocut_price as $key => $product_sale_discount) {
                                                        if ($product_cart_list->{'cart::qty'} >= $product_sale_discount->minimum_qty) {
                                                            $retail_price = $product_sale_discount->price;
                                                        }
                                                    }
                                                }

                                                ?>
                                                <tr class="shopping_cart_item">

                                                    <td>
                                                        <label class="container cartlist-checkbox">
                                                            <input type="checkbox" value="<?php echo $retail_price * $product_cart_list->{'cart::qty'} ?>" data-delivery-sum="<?php echo $product_cart_list->{'cart::qty'} * $product_cart_list->{'product::delivery_price'} ?>" data-product_id="<?php echo $product_cart_list->{'product::id'} ?>" data-retail_price="<?php echo $retail_price ?>" data-qty="<?php echo $product_cart_list->{'cart::qty'} ?>" data-delivery_price="<?php echo $product_cart_list->{'cart::qty'} *  $product_cart_list->{'product::delivery_price'} ?>" data-real_price="<?php echo $product_cart_list->{'product::retail_price'} ?>" data-minimum_product="<?php echo $product_cart_list->{'product::minimum_product'} ?>">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </td>

                                                    <td>
                                                        <div class="cart-thumb media align-items-center">
                                                            <img class="w-100" src="<?php echo $product_cart_list->{'product::images'}[0] ?>" alt="">

                                                            <div class="media-body">
                                                                <h5 class="cart-item-title w-100 pl-3"><?php echo getVariable($product_cart_list, 'product::model_name') ?></h5>
                                                            </div>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <h5 class="sum_product_price">
                                                            <?php echo number_format($retail_price); ?>
                                                            <?php echo getWording('index', 'baht') ?>
                                                        </h5>
                                                    </td>

                                                    <td>
                                                        <div class="d-flex align-items-center product-quantity" data-cart="<?php echo $product_cart_list->{'cart::id'} ?>">
                                                            <button class="btn-product btn-product-down update_cart_item" data-update_type="minus"> <i class="fas fa-minus"></i></button>
                                                            <input class="form-product" type="number" id="product_quantity" name="form-product" data-quantity="0" value="<?php echo $product_cart_list->{'cart::qty'} ?>" disabled="">
                                                            <button class="btn-product btn-product-up update_cart_item" data-update_type="plus"> <i class="fas fa-plus"></i></button>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <h5 class="cart_crt_total" id="product_cart_price" data-product-sum="<?php echo $retail_price * $product_cart_list->{'cart::qty'} ?>">
                                                            <?php echo number_format($product_cart_list->{'cart::qty'} * $retail_price); ?>

                                                            <?php echo getWording('index', 'baht') ?></h5>
                                                    </td>

                                                    <td>
                                                        <a class="remove_cart_item" href="#" data-remove="<?php echo $product_cart_list->{'cart::id'} ?>" location.reload();><span class="trash-btn"><i class="far fa-trash-alt"></i></span></a>
                                                    </td>
                                                    <td>
                                                        <h5 class="delivery_cart_crt_total" id="delivery_crt_total" data-delivery-sum="<?php echo $product_cart_list->{'cart::qty'} * $product_cart_list->{'product::delivery_price'} ?>" hidden><?php echo $product_cart_list->{'cart::qty'} * $product_cart_list->{'product::delivery_price'} ?> </h5>
                                                    </td>
                                                </tr>

                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td>

                                                </td>
                                                <td>
                                                    <h3 class="text-center my-4" style="color: black;"><?php echo getWording('index', 'cart') ?></h3>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>

                                </table>
                            </div>
                            <!-- <div style=" z-index: -1;"> -->

                            <!-- </div> -->
                        </div>
                    </div>
                    <div class="col-sm-4 col-xl-3">
                        <div class="order-summary" style="width:100%; margin-top: 25px;height: 305px;">
                            <div class="header d-flex justify-content-between text-white">
                                <h5><?php echo getWording('product', 'order') ?></h5>
                                <h5 id='cart_count'>0 <?php echo getWording('product', 'list') ?></h5>
                            </div>
                            <div class="order-detail" style="background-color: white;">
                                <div class="product-price d-flex justify-content-between">
                                    <span class="title"><?php echo getWording('product', 'price') ?></span>
                                    <span>
                                        <h5>
                                            <span class="d-inline" id="total_cart_price_page">0 </span>
                                            <?php echo getWording('index', 'baht') ?>
                                        </h5>
                                    </span>
                                </div>
                                <div class="tranport-price d-flex justify-content-between">
                                    <span class="title"><?php echo getWording('product', 'shipping') ?></span>
                                    <span>
                                        <h5><span class="d-inline" id="delivery_total_cart_price_page">0 </span>
                                            <?php echo getWording('index', 'baht') ?></h5>
                                    </span>
                                </div>
                                <div class="discount d-flex justify-content-between">
                                    <div>
                                        <span class="title"><?php echo getWording('product', 'discount') ?></span>
                                        <span class="d-block" id="coupon_name"><?php echo getWording('product', 'not') ?></span>
                                    </div>
                                    <h5><span id="discount_coupon">0 </span>
                                        <?php echo getWording('index', 'baht') ?></h5>
                                </div>
                            </div>
                            <div class="total-price d-flex justify-content-between" style="background-color: white;">
                                <div class="text-start">
                                    <h5><?php echo getWording('profile', 'net_price') ?></h5>
                                    <span class="d-block">(<?php echo getWording('product', 'vat') ?>)</span>
                                </div>
                                <span>
                                    <h5><span class="total_price" id="total_price">0 </span> <?php echo getWording('index', 'baht') ?></h5>
                                </span>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- --------------------------------------------------------------------------------------------- -->

                <div class="row my-3 mt-5">
                    <div class="col-6">
                        <div class="coupon d-flex ml-5" id="coupon_box">
                            <input class="pl-3" type="text" id="coupon" placeholder="<?php echo getWording('product', 'code') ?>">
                            <button class="btn-product" id="submit-coupon"><?php echo getWording('product', 'use') ?></button>
                            <h5 id="code_already" style="display: none;"><?php echo getWording('payment', 'code_already') ?></h5>
                        </div>
                    </div>
                    <div class="col-2">
                        <button class="btn-product update-cart radius w-100" id="update_cart" style="display: none;"><?php echo getWording('product', 'update') ?></button>
                    </div>
                </div>
                <div class="col-12 col-md-9">
                    <div class="col-11 row justify-content-center border-top py-3 my-3">
                        <div class="col-6 col-md-3 mt-3">
                            <button class="btn-product purchase radius w-100" id="submit_payment" disabled onclick="submit_payment()"><?php echo getWording('product', 'place') ?></button>
                        </div>
                        <div class="col-6 col-md-3 mt-3">
                            <button class="btn-product quotation radius w-100" id="submit_quotation" disabled onclick="submit_quotation()"><?php echo getWording('product', 'quotation') ?></button>
                        </div>
                    </div>
                </div>

            <?php } ?>
        </div>
    </div>

    <form id="product_list_form" action="<?php echo base_url('Product/payment') ?>" enctype="multipart/form-data" method="POST">
        <!-- <span class="d-block" id="coupon_name_submit"></span> -->
        <input name="coupon_name_submit" id="coupon_name_submit" value="" hidden>
        <input name="coupon_discount_onlynormalprice" id="coupon_discount_onlynormalprice" value="" hidden>
        <input name="coupon_discount_delivery" id="coupon_discount_delivery" value="" hidden>
        <input name="coupon_id_submit" id="coupon_id_submit" value="" hidden>
    </form>

    <form id="product_list_quotation" action="<?php echo base_url('Product/quotation') ?>" enctype="multipart/form-data" method="POST">

    </form>

    <div id="product_discount">

    </div>

    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true" style="z-index: 1061;">
        <div class="modal-dialog">
            <div class="modal-content modal-content-style">
                <div class="modal-header modal-header-style">
                </div>
                <div class="modal-body" style="text-align: center;">
                    <div class="btn-close-modal">
                        <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
                    </div>
                    <h3 class="coupon_response text-center"></h3>
                    <button class="btn" type="button" data-bs-dismiss="modal" aria-label="Close" style="width: 142px;height: 40px; background-color: #AAAAAA; border-radius: 23px; padding: 10px; margin:10px;color:white;"><?php echo getWording('index', 'ok') ?></button>
                </div>
                <div class="modal-footer coupon_response_btn">
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="add-fail" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-content-style">
                <div class="modal-header modal-header-style">
                </div>
                <div class="modal-body">
                    <div class="btn-close-modal">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <h3 style="text-align: center;"><?php echo getWording('payment', 'select_product') ?></h3>
                </div>
                <div class="modal-footer modal-footer-style register_response_btn">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="product_minimum_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-content-style">
                <div class="modal-header modal-header-style">
                </div>
                <div class="modal-body" style="text-align: -webkit-center;">
                    <div class="btn-close-modal">
                        <!-- <button type="button" class="btn-close" id="close_product_minimum_modal" data-bs-dismiss="modal" aria-label="Close"></button> -->
                    </div>
                    <h3 style="text-align: center;"> <span><?php echo getWording('product', 'increase') ?></span> <span id="product_minimum_word"></span> <span><?php echo getWording('product', 'piece') ?></span></h3>
                    <button class="btn" id="close_product_minimum_modal" type="button" data-bs-dismiss="modal" aria-label="Close" style="width: 142px;height: 40px; background-color: #AAAAAA; border-radius: 23px; padding: 10px; margin:10px;color:white;"><?php echo getWording('index', 'ok') ?></button>
                </div>
                <div class="modal-footer modal-footer-style register_response_btn">

                </div>
            </div>
        </div>
    </div>

</section>

<?php include("footer.php") ?>
<script>
    $(document).ready(function() {
        var product_list_id = [];
        var product_retail_price = [];
        var product_qty = [];
        var product_total_price = [];
        var delivery_price = [];
        var real_price = [];
        var product_id_coupon = [];
    });

    $("#shopping_cart_list").load(document.URL + ' #shopping_cart_list tr', function() {
        console.log('shopping_cart_list loaded');
        sum = 0;
        // formatNumber(sum);
        // var total_price = 0;
        var list_var = '<?php echo getWording('product', 'list') ?>';
        sum_delivery = 0;
        var product_count = 0;
        product_list_id = [];
        product_retail_price = [];
        product_qty = [];
        product_total_price = [];
        delivery_price = [];
        real_price = [];
        discount_onlynormalprice = 0;
        discount_delivery = 0;
        $('#submit_payment').prop("disabled", false);
        $('#submit_quotation').prop("disabled", false);
        // console.log(discount_baht);
        $('.shopping_cart_item td .cartlist-checkbox input').change(function() {
            if ($(this).is(':checked')) {
                $('#product_discount').append('<input class="form_discount product_discount_' + $(this).data('product_id') + '" name="product_id[]" value="' + $(this).data('product_id') + '" type="hidden">');
                $('#product_discount').append('<input class="form_discount product_discount_' + $(this).data('product_id') + '" name="product_qty[]" value="' + $(this).data('qty') + '" type="hidden">');
                // console.log('checkbox');
                product_quantity = $(this).data('qty');
                product_minimum = $(this).data('minimum_product');
                // console.log('product_quantity', product_quantity);
                // console.log('product_minimum', product_minimum);

                if (product_quantity < product_minimum) {
                    $('#product_minimum_modal').modal('show');
                    // alert_word = ('กรุณเพิ่มจำนวนให้ถึงจำนวนสั่งซื้อขั้นต่ำ' + product_minimum + 'ชิ้น');
                    $('#product_minimum_word').append(product_minimum);
                    $("#close_product_minimum_modal").click(function() {
                        $('#product_minimum_word').empty();
                    });

                    // setTimeout(function() {
                    //     $('#product_minimum_modal').modal('hide');
                    //     $('#product_minimum_word').empty();
                    // }, 5000);
                    // if ($('#product_minimum_modal').modal('hide')){
                    //     $('#product_minimum_word').empty();
                    // }

                    $('.shopping_cart_item td .cartlist-checkbox input').prop("checked", false);

                    sum = 0;
                    $('#total_cart_price_page').text(formatNumber(sum));
                    product_count = 0;
                    $('#cart_count').text(product_count + ' ' + list_var);

                    // $(this).attr('name="product_id"')
                    $('input.form_payment').remove();
                    $('input.form_quotation').remove();
                    $('input.form_discount').remove();


                    sum_delivery = 0;
                    $('#delivery_total_cart_price_page').text(formatNumber(sum_delivery));
                    // console.log("sum_delivery",sum_delivery);

                    $('#delivery_total_cart_price_page').each(function() {
                        delivery_total_price = 0;

                    });
                    if (delivery_total_price <= 0) {
                        delivery_total_price = 0;
                    }
                    // console.log("delivery_total_price", delivery_total_price);

                    $("#delivery_total_cart_price_page").text(formatNumber(delivery_total_price).toString());

                    // console.log(discount_baht);
                    $('#total_price').each(function() {
                        total_price = 0;
                    });

                    if (total_price <= 0) {
                        total_price = 0;
                    }

                    $("#total_price").text(formatNumber(total_price).toString());

                } else {
                    // console.log('checkbox product');
                    sum = sum + parseFloat($(this).val());
                    // console.log(formatNumber(sum));
                    $('#total_cart_price_page').text(formatNumber(sum));
                    product_count = parseFloat(product_count) + 1;
                    $('#cart_count').text(product_count + ' ' + list_var);
                    // product_list_id.push($(this).data('product_id'));
                    // product_retail_price.push($(this).data('retail_price'));
                    // product_qty.push($(this).data('qty'));
                    // product_total_price.push($(this).val());
                    $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="product_id[]" id="id_check" value="' + $(this).data('product_id') + ' "type="hidden">');
                    $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="product_retail_price[]" id="retail_price_check" value="' + $(this).data('retail_price') + ' "type="hidden">');
                    $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="product_qty[]" id="qty_check" value="' + $(this).data('qty') + ' "type="hidden">');
                    $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="product_total_price[]" id="total_price_check" value="' + $(this).val() + ' "type="hidden">');
                    $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="delivery_price[]" id="delivery_price_check" value="' + $(this).data('delivery_price') + ' "type="hidden">');
                    $('#product_list_form').append('<input class="form_payment product_' + $(this).data('product_id') + '" name="minimum_product[]" id="minimum_product_check" value="' + $(this).data('minimum_product') + ' "type="hidden">');
                    // $('#product_list_form').append('<input class="product_' + $(this).data('product_id') + '" name="coupon_name[]" value="' + 'test123' + ' "type="hidden">');

                    $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="product_id[]" value="' + $(this).data('product_id') + '" type="hidden">');
                    $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="product_retail_price[]" value="' + $(this).data('retail_price') + '" type="hidden">');
                    $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="product_qty[]" value="' + $(this).data('qty') + '" type="hidden">');
                    $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="product_total_price[]" value="' + $(this).val() + '" type="hidden">');
                    $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="delivery_price[]" value="' + $(this).data('delivery_price') + '" type="hidden">');
                    $('#product_list_quotation').append('<input class="form_quotation product_quotation_' + $(this).data('product_id') + '" name="real_price[]" value="' + $(this).data('real_price') + '" type="hidden">');

                    sum_delivery = sum_delivery + parseFloat($(this).data('delivery-sum'));
                    $('#delivery_total_cart_price_page').text(formatNumber(sum_delivery));
                    // console.log("sum_delivery",sum_delivery);

                    $('#delivery_total_cart_price_page').each(function() {
                        delivery_total_price = sum_delivery - discount_delivery;
                    });

                    if (delivery_total_price <= 0) {
                        delivery_total_price = 0;
                    }

                    $("#delivery_total_cart_price_page").text(formatNumber(delivery_total_price).toString());

                    // console.log(discount_baht);
                    $('#total_price').each(function() {
                        total_price = (sum + delivery_total_price) - discount_onlynormalprice;
                    });

                    if (total_price <= 0) {
                        total_price = 0;
                    }

                    $("#total_price").text(formatNumber(total_price).toString());
                }

            } else {
                sum = sum - parseFloat($(this).val());
                $('#total_cart_price_page').text(formatNumber(sum));
                product_count = parseFloat(product_count) - 1;
                $('#cart_count').text(product_count + ' ' + list_var);

                // $(this).attr('name="product_id"')
                $('input.product_' + $(this).data('product_id') + '').remove();
                $('input.product_quotation_' + $(this).data('product_id') + '').remove();
                $('input.product_discount_' + $(this).data('product_id') + '').remove();

                sum_delivery = sum_delivery - parseFloat($(this).data('delivery-sum'));
                $('#delivery_total_cart_price_page').text(formatNumber(sum_delivery));
                // console.log("sum_delivery not check",sum_delivery);

                $('#delivery_total_cart_price_page').each(function() {
                    delivery_total_price = sum_delivery - discount_delivery;

                });
                if (delivery_total_price <= 0) {
                    delivery_total_price = 0;
                }
                // console.log("delivery_total_price", delivery_total_price);

                $("#delivery_total_cart_price_page").text(formatNumber(delivery_total_price).toString());

                // console.log(discount_baht);
                $('#total_price').each(function() {
                    total_price = (sum + delivery_total_price) - discount_onlynormalprice;
                });

                if (total_price <= 0) {
                    total_price = 0;
                }

                $("#total_price").text(formatNumber(total_price).toString());

            };
        });
    });

    function isEmpty(el) {
        return !$.trim(el.html())
    }

    var select_product_word = '<?php echo getWording('payment', 'select_product') ?>'

    // function submit_payment() {
    //     if (isEmpty($('#product_list_form'))) {
    //         alert(select_product_word);
    //         return false;
    //     }
    //     $('#product_list_form').submit()
    // }

    function submit_payment() {
        if ($('.shopping_cart_item td .cartlist-checkbox input').is(':checked')) {
            $('#product_list_form').submit()
        } else {
            $('#add-fail').modal('show');
            return false;
        }
    }

    function submit_quotation() {
        // if (isEmpty($('#product_list_quotation'))) {
        if ($('.shopping_cart_item td .cartlist-checkbox input').is(':checked')) {
            $('#product_list_quotation').submit()
        } else {
            $('#add-fail').modal('show');
            return false;
        }

    }

    function cart_crt_total() {
        // console.log('calculating!!!');
        var sum = 0;

        $('.cart_crt_total').each(function() {
            sum += parseFloat($(this).data('product-sum'));
            // console.log('total_price', sum)
        });
        $("#total_cart_price_page").text(formatNumber(sum).toString());
        // console.count('total_price', sum)
        return sum;
    };

    // function check_crt_total() {
    //     // console.log('calculating!!!');
    //     var sum = 0;
    //     $('.cart_crt_total').each(function() {
    //         sum += parseFloat($(this).data('product-sum'));
    //         // console.log('total_price', sum)
    //     });
    //     $("#total_cart_price_page").text(formatNumber(sum).toString());
    //     // console.log('total_price', sum)
    // };

    function delivery_cart_crt_total() {
        // console.log('calculating!!!');
        var sum_delivery = 0;

        $('.delivery_cart_crt_total').each(function() {
            sum_delivery += parseFloat($(this).data('delivery-sum'));
            // console.log('total_price', sum)
        });
        $("#delivery_total_cart_price_page").text(formatNumber(sum_delivery).toString());
        // console.log('total_price', sum_delivery)
        return sum_delivery;
    };

    // function check_delivery_crt_total() {
    //     var sum_delivery = 0;
    //     $('.delivery_cart_crt_total').each(function() {
    //         sum_delivery += parseFloat($(this).data('delivery-sum'));
    //         // console.log('total_price', sum)
    //     });
    //     $("#delivery_total_cart_price_page").text(formatNumber(sum_delivery).toString());
    //     // console.log('total_price', sum)
    // };

    $("#update_cart").click(function() {
        location.reload();
    });

    $("#submit-coupon").click(function() {
        product_id_coupon = $.map($('#product_discount input[name="product_id[]"]'), function(el) {
            return el.value;
        }).join(',');
        console.log(product_id_coupon);
        var send_data_url = '<?php echo base_url('Product/coupon') ?>';
        var data = {
            "coupon_id": $("#coupon").val(),
            "product_coupon": product_id_coupon,
        }
        // console.log(data)
        send_data = {
            "url": send_data_url,
            "method": "POST",
            "data": data,
        }
        // console.log(send_data)

        $.ajax(send_data).done(function(response) {
            console.log(response);
            if (response.code == "0x0000-00000") {
                var is_delivery = response.data.coupon.is_delivery;
                var is_onlynormalprice = response.data.coupon.is_onlynormalprice;
                var code = response.data.coupon.code;
                var coupon_id = response.data.coupon.id;
                discount_baht = response.data.coupon.discount_baht;
                // console.log(is_delivery);
                // console.log(is_onlynormalprice);
                // console.log(code);
                // console.log(discount_baht);
                $("#coupon_name").empty();
                $("#coupon_name").append(code);
                $("#coupon_name_submit").val(code);
                $("#coupon_id_submit").val(coupon_id);
                $("#coupon").hide();
                $("#submit-coupon").hide();
                $("#code_already").show();
                $("#update_cart").show();
                if (is_onlynormalprice == '1') {
                    discount_onlynormalprice = discount_baht;
                    // console.log("discount_onlynormalprice", discount_onlynormalprice);
                    $("#discount_coupon").empty();
                    $("#discount_coupon").append(formatNumber(discount_onlynormalprice));
                    $("#coupon_discount_onlynormalprice").val(discount_onlynormalprice);
                    $('#total_price').each(function() {
                        total_price_discount = total_price - discount_onlynormalprice;
                        // console.log(total_price);
                    });
                    if (total_price_discount <= 0) {
                        total_price_discount = 0;
                    }
                    $("#total_price").empty();
                    $("#total_price").append(formatNumber(total_price_discount));
                    $('#submit-coupon').prop("hidden");
                } else if (is_delivery == '1') {
                    discount_delivery = discount_baht;
                    // console.log("discount_delivery", discount_delivery);
                    $("#discount_coupon").empty();
                    $("#discount_coupon").append(discount_delivery);
                    $("#coupon_discount_delivery").val(discount_delivery);
                    $('#delivery_total_cart_price_page').each(function() {
                        delivery_price_discount = sum_delivery - discount_delivery;
                    });
                    if (delivery_price_discount <= 0) {
                        delivery_price_discount = 0;
                    }
                    // $("#delivery_total_cart_price_page").text(formatNumber(delivery_total_cart_price_page).toString());
                    $("#delivery_total_cart_price_page").empty();
                    $("#delivery_total_cart_price_page").append(formatNumber(delivery_price_discount));

                    // console.log("delivery_price_discount", delivery_price_discount);
                    $('#total_price').each(function() {
                        total_price_discount = sum + delivery_price_discount;
                        // console.log("total_price_discount", total_price_discount);
                    });
                    if (total_price_discount <= 0) {
                        total_price_discount = 0;
                    }
                    $("#total_price").empty();
                    $("#total_price").append(formatNumber(total_price_discount));
                }
                $('.coupon_success').addClass('d-none');
                $('.coupon_success').removeClass('d-block');
                $('.coupon_fail').addClass('d-block');
                $('.coupon_fail').removeClass('d-none');
                $(".coupon_response").text('successfully update coupon.');
            } else {
                $('.coupon_success').addClass('d-none');
                // $('#staticBackdrop').modal('show');
                $('.coupon_success').removeClass('d-block');
                $('.coupon_fail').addClass('d-block');
                $('.coupon_fail').removeClass('d-none');
                $(".coupon_response").text(response.message);

                // $("#total_price").empty();
                // $("#discount_coupon").empty();
                // $("#delivery_total_cart_price_page").empty();
            }
            $('#staticBackdrop').modal('show');
        });
    });
</script>