<head>
    <title>page35</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link href="style-page30.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

</head>

<body>

    <div class="container">

        <!-- Button to Open the Modal -->
        <button type="button" class="btn1 btn-primary" data-toggle="modal" data-target="#myModal4" style="width: 200px;
        height: 45px;
        background-color: #D3D3D3;
        color: #000000;
        border-radius: 23px;">
            ยืนยันคำสั่งซื้อ
        </button>

        <!-- The Modal -->
        <div class="modal" id="myModal4">
            <div class="modal-dialog modal-dialog-scrollable" style="justify-content: center;">
                <div class="modal-content" style="min-inline-size: fit-content;">
                    <!-- Modal Header -->
                    <div class="modal-header" style="width: 100%;">
                        <h5 class="modal-title" style="padding-left:20px">ยืนยันคำสั่งซื้อก่อนชำระเงิน</h5>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body" style="width: 100%;">
                        <div style="padding-left: 5px; padding-top: 15px;">ที่อยู่จัดส่ง</div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-12" style="padding-bottom: 10px; text-align: center;">
                                <div class="address mt-3" style="width: auto; height: auto; background-color: #ffffff; border:solid 1px;">
                                    <div class="col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        กรกนก นามสกุล
                                    </div>
                                    <div class="col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        88/8 Lorem ipsum dolor sit amet, consectetur elit, sed
                                        do eiusmod tempor incididu 10090
                                    </div>
                                    <div class="col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        email@gmail.com <br>
                                        088-888-8888
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="padding-left: 5px; padding-top: 25px;">ที่อยู่ใบกำกับภาษี</div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-12" style="padding-bottom: 10px; text-align: center;">
                                <div class="vat mt-3" style="width: auto; height: auto; background-color: #ffffff; border:solid 1px;">
                                    <div class="col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        กรกนก นามสกุล
                                    </div>
                                    <div class="col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        88/8 Lorem ipsum dolor sit amet, consectetur elit, sed
                                        do eiusmod tempor incididu 10090
                                    </div>
                                    <div class="col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        email@gmail.com <br>
                                        088-888-8888
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="padding-left: 5px; padding-top: 25px;">ช่องทางการชำระเงิน</div>
                        <div class="row">
                            <div class="col-sm-12 col-xl-12" style="padding-bottom: 10px; text-align: center;">
                                <div class="pay mt-3" style="width: auto; height: auto; background-color: #ffffff; border:solid 1px;">
                                    <div class="col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        บัตรเครดิต/เดบิต
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="text-align: center; padding-bottom: 40px;">
                            <button class="btn3 mt-3" style="width: 252px; height: 45px; background-color: #D3D3D3; border: 0px; border-radius: 23px;">
                                ดำเนินการชำระเงิน
                            </button>
                        </div>


                    </div>




                </div>
            </div>
        </div>

    </div>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>