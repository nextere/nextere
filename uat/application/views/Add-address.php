<div class="container">

    <!-- Button to Open the Modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal5" style="width: 100%; height: 45px; background-color: #ffffff; color: #000000; border: solid 1px; margin-top:20px">
        + เพิ่มที่อยู่ใบกำกับภาษี
    </button>

    <!-- The Modal -->
    <div class="modal" id="myModal5">
        <div class="modal-dialog modal-dialog-scrollable" style="justify-content: center;">
            <div class="modal-content" style="min-inline-size: fit-content;">

                <!-- Modal Header -->
                <div class="modal-header" style="width: 100%">
                    <h5 class="modal-title" style="padding-left:20px">เพิ่มที่อยู่ใบกำกับภาษี</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body" style="width: 100%">

                    <div class="row">
                        <div class="col-sm-6 col-xl-12" style="text-align:left;">ชื่อ*
                            <div class="name pt-2">
                                <input type="text" id="name" name="name" placeholder="ชื่อ" required>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xl-12" style="text-align:left;">นามสกุล*
                            <div class="lname pt-2">
                                <input type="text" id="lname" name="lname" placeholder="นามสกุล" required>
                            </div>
                        </div>
                    </div>

                    <div class="row pt-3">
                        <div class="col-sm-6 col-xl-12" style="text-align:left;">บริษัท
                            <div class="company pt-2">
                                <input type="text" id="company" name="company" placeholder="บริษัท">
                            </div>
                        </div>

                        <div class="col-sm-6 col-xl-12" style="text-align:left;">สาขา
                            <div class="branch pt-2">
                                <input type="text" id="branch" name="branch" placeholder="สาขา" style="width: 100%; height: 45px;">
                            </div>
                        </div>
                    </div>

                    <div class="row pt-3">
                        <div class="col-sm-6 col-xl-12" style="text-align:left;">ที่อยู่ในการออกใบกำกับภาษี*
                            <div class="add pt-2">
                                <input type="text" id="add" name="add" placeholder="ที่อยู่" required style="width: 100%; height: 45px;">
                            </div>
                        </div>

                        <div class="col-sm-6 col-xl-12" style="text-align:left;">จังหวัด*
                            <div class="province pt-2">
                                <input type="text" id="province" name="province" placeholder="จังหวัด" required style="width: 100%; height: 45px;">
                            </div>
                        </div>
                    </div>

                    <div class="row pt-3">
                        <div class="col-sm-6 col-xl-12" style="text-align:left;">อำเภอ/เขต*
                            <div class="district pt-2">
                                <input type="text" id="district" name="district" placeholder="อำเภอ/เขต" style="width: 100%; height: 45px;">
                            </div>
                        </div>

                        <div class="col-sm-6 col-xl-12" style="text-align:left;">ตำบล/แขวง*
                            <div class="sub-district pt-2">
                                <input type="text" id="sub-district" name="sub-district" placeholder="ตำบล/แขวง" style="width: 100%; height: 45px;">
                            </div>
                        </div>
                    </div>

                    <div class="row pt-3">
                        <div class="col-sm-6 col-xl-12" style="text-align:left;">รหัสไปรษณีย์*
                            <div class="post pt-2">
                                <input type="text" id="post" name="post" placeholder="รหัสไปรษณีย์" style="width: 100%; height: 45px;">
                            </div>
                        </div>

                        <div class="col-sm-6 col-xl-12" style="text-align:left;">เลขประจำตัวผู้เสียภาษี*
                            <div class="vatNo pt-2">

                                <input type="text" id="vatNo" onKeyPress="return isNumber(event)" min="0" maxlength="13" placeholder="เลขประจำตัวผู้เสียภาษี" required style="width: 100%; height: 45px;">
                            </div>
                        </div>
                    </div>

                    <div style="text-align: center; padding-bottom: 40px;">
                        <button class="btn3 mt-3" style="width: 252px; height: 45px; background-color: #D3D3D3; border: 0px; border-radius: 23px;">
                            บันทึก
                        </button>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script>
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }

        return true;
    }
</script>