<?php include('header.php') ?>

<?php
$url = $actual_link;
$word_check   = 'subcategory';
$result_check = strpos($url, $word_check);

    if ($result_check === false) {
        // echo "NOT FOUND !!";
    } else {
        // echo "FOUND !";
        $index_1 = strpos($actual_link,"index/");
        $index_1 = $index_1+6;
        $get_sub_cat_id = substr($actual_link, $index_1, -17);

        foreach ($product_category as $key => $product_category_list) {
            if (!empty($product_category_list->product_sub_categories)) {
                foreach ($product_category_list->product_sub_categories as $sub_key => $sub_category_list) {
                    if($get_sub_cat_id == $sub_category_list->id){
                        $sub_c_id = $sub_category_list->id;
                        $main_c_id = $product_category_list->id;
                    }
                }
            }
        }
    }

// if(isset($sub_c_id) && isset($main_c_id)){
//     echo $main_c_id;
//     echo '<br>';
//     echo $sub_c_id;
// }
?>

<style>
    .accordion-item {
        border-right: none;
        border-left: none;
    }

    .list-group-item {
        border: none;
    }

    .accordion-item a {
        color: #000000;
        text-decoration: none;
    }

    .accordion-button:not(.collapsed) {
        color: #000000;
        background-color: #F2F2F2;
    }

    .border-bottom {
        border-bottom: groove;
    }
</style>

<body>
    <section class="all_product">
        <div class="col-12" style="background-color: #F0F0F0; text-align: center; padding: 70px;">
            <h4><b><?php echo getWording('product', 'all_product') ?></b></h4>
            <!-- <P>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</P> -->
        </div>
        <div class="d-flex">
            <div class="col-12  pt-2 pb-2 mt-3" style="background-color: lightgray; text-align:left;">
                <div class="container">
                    <a href="javascript:void(0)" class="btn btn-secondary buttomsale" onclick="openNavPrice()"><?php echo getWording('product', 'price') ?></a>
                    <?php if ($_GET['type'] == 'subcategory' && count($technical_list) > "1") { ?>
                        <a href="#" class="btn btn-secondary buttomsale technical_filter" onclick="openNav()"><?php echo getWording('product', 'usability') ?></a>
                    <?php } else { ?>
                        <a href="#" class="btn btn-secondary buttomsale technical_filter" onclick="openNav()" style="display: none;"><?php echo getWording('product', 'usability') ?></a>
                    <?php } ?>

                    <div id="mySidenavPrice" class="sidenav sidenav_1 ">
                        <div class="container">
                            <h4 class="ml-5 ml-md-0"><?php echo getWording('product', 'price') ?></h4>
                            <ul>
                                <?php foreach ($price_rate->price_rate as $key => $price_rate_list) { ?>
                                    <li>
                                        <label class="container cartlist-checkbox price_rate">
                                            <input type="checkbox" value="<?php echo $price_rate_list->id ?>">
                                            <span class="checkmark"></span>
                                            <span class="ml-3"><?php echo number_format($price_rate_list->price_min) ?> - <?php echo number_format($price_rate_list->price_max) ?> <?php echo getWording('index', 'baht') ?></span>
                                        </label>
                                    </li>
                                <?php } ?>
                            </ul>
                            <button class="btn reset_filter"><?php echo getWording('product', 'clear') ?></button>
                        </div>
                        <a href="javascript:void(0)" class="closebtn" onclick="closeNavPrice()">&times;</a>
                    </div>
                    <div id="mySidenav" class="sidenav ">
                        <div class="container">
                            <h4 class="ml-5 ml-md-0"><?php echo getWording('product', 'usability') ?></h4>
                            <ul class="technical_list_checkbox">
                                <?php foreach ($technical_list as $key => $technical) { ?>
                                    <li>
                                        <label class="container cartlist-checkbox technical">
                                            <input type="checkbox" value="<?php echo $technical->id ?>">
                                            <span class="checkmark ml-8 ml-md-5"></span>
                                            <span class="ml-3"><?php echo getVariable($technical, 'name') ?></span>
                                        </label>
                                    </li>
                                <?php } ?>


                            </ul>
                        </div>
                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    </div>

                    <!-- Use any element to open the sidenav -->
                    <!-- <span onclick="openNav()">Sidebar</span> -->
                </div>
            </div>
        </div>
        <div class="container ">
            <div class="row">
                <div class="col-sm-3 col-xl-3 mt-3 ">
                    <div class="pl-5 pb-1"><b> <?php echo getWording('product', 'category') ?></b></div>
                    <div class="accordion" id="accordionExample">
                        <?php
                        function comparator_10($a, $b)
                        {
                            return $a->order < $b->order ? -1 : 1;
                        };
                        usort($product_category, "comparator_10");
                        ?>
                        <?php foreach ($product_category as $key => $product_category_list) { ?>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="heading_<?php echo $product_category_list->id ?>"> </h2>
                                <button class="accordion-button collapsed category 
                                <?php if(isset($current_url_array[2])){if($current_url_array[2] == $product_category_list->id){echo('active');}} ?>
                                " id="reset" type="button" data-bs-toggle="collapse" data-bs-target="#<?php echo 'category_' . $product_category_list->id ?>" aria-expanded="false" aria-controls="<?php echo 'category_' . $product_category_list->id ?>">
                                    <a class="select_category" href="<?php echo base_url('Product/index/' . $product_category_list->id . '?type=category') ?>" data-category="<?php echo $product_category_list->id ?>">
                                        <p style="margin-bottom: 0rem;" class="select_category"> <?php echo getVariable($product_category_list, 'name') ?></p>
                                    </a>
                                </button>
                                <?php if (!empty($product_category_list->product_sub_categories)) { ?>
                                    <div id="<?php echo 'category_' . $product_category_list->id ?>" class="collapse 
                                        <?php if(isset($current_url_array[2])){if($current_url_array[2] == $product_category_list->id){echo('show');}} ?>
                                        <?php if(isset($sub_c_id) && isset($main_c_id)){if($main_c_id == $product_category_list->id){echo('show');}} ?>
                                        " aria-labelledby="heading_<?php echo $product_category_list->id ?>" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <ul class="list-group list-group-flush">
                                                <?php foreach ($product_category_list->product_sub_categories as $sub_key => $sub_category_list) { ?>
                                                    <li class="list-group-item">
                                                        <a class="select_subcategory <?php if(isset($sub_c_id) && isset($main_c_id)){if($sub_c_id == $sub_category_list->id){echo('active');}} ?>" href="<?php echo base_url('Product/index/' . $sub_category_list->id . '?type=subcategory') ?>" data-subcategory="<?php echo $sub_category_list->id ?>">
                                                            <?php echo getVariable($sub_category_list, 'name') ?>
                                                        </a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                <?php  } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-sm-9 col-xl-9">
                    <div class="row">
                        <div class="col-sm-3 col-xl-6 mt-3 ">
                            <a href="#" class="btn btn-outline-dark buttomsale" id="grid"><i class="fas fa-th-large "></i></a>
                            <a href="#" class="btn btn-outline-dark buttomsale" id="list"><i class="fas fa-list"></i></a>
                        </div>
                        <div class="col-sm-9 col-xl-6 mt-3 sorting" style="text-align: right;">
                            <?php echo getWording('product', 'filter') ?> :
                            <select class="product-fillter text-center" id="product-fillter" name="fillter" style="width: 150px;height: 30px;">
                                <option value=""><?php echo getWording('product', 'product_name') ?></option>
                                <option value="best_seller"><?php echo getWording('product', 'best_seller') ?></option>
                                <option value="sale"><?php echo getWording('product', 'sale') ?></option>
                            </select>

                            <?php echo getWording('product', 'show') ?> :
                            <select class="product_amount text-center p-0" id="product_amount" name="fillter" style="width: 50px;height: 30px;">
                                <option value="12">12</option>
                                <option value="21">21</option>
                                <option value="27">27</option>
                            </select>
                        </div>
                    </div>
                    <div class="row " id="products">
                        <?php if (!empty($product_list)) { ?>
                            <?php foreach ($product_list->product_mains as $key => $products) { ?>
                                <div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" id="border-bottom" style="text-align:left">
                                    <div class="product-item">
                                        <div id="border-images" class="border-images">
                                            <?php if (property_exists($products, 'product::images')) { ?>
                                                <a href="<?php echo base_url('/Product/Details/' . $products->{'product::id'}) ?>">
                                                    <img class="w-100" src="<?php echo $products->{'product::images'}[0] ?>" alt="">
                                                </a>
                                            <?php } else { ?>
                                                <a href="<?php echo base_url('/Product/Details/' . $products->{'product::id'}) ?>">
                                                    <img class="w-100" src="<?php echo base_url('/assets/img/no-images.png') ?>" alt="">
                                                </a>
                                            <?php } ?>
                                        </div>
                                        <h4 class="mt-3"><?php echo getVariable($products, 'product::name') ?></h4>

                                        <?php if ($products->{'product::model_name_th'} == '') { ?>
                                            <span style="color:white;">...</span>
                                        <?php } else if ($products->{'product::model_name_en'} == '') { ?>
                                            <span style="color:white;">...</span>
                                        <?php } else { ?>
                                            <span><?php echo getVariable($products, 'product::model_name') ?></span>
                                        <?php } ?>
                                        <?php if ($products->{'product::retail_price'} != null) { ?>
                                            <?php if (empty($products->{'product::discount'})) { ?>
                                                <h4 class="mt-6">
                                                    <?php echo number_format($products->{'product::retail_price'}); ?>
                                                    <?php echo getWording('index', 'baht') ?>
                                                </h4>
                                            <?php } else { ?>
                                                <h4 class="mt-1">
                                                    <div class="row">
                                                        <span class="col-6 ">
                                                            <div class="row ">
                                                                <div class="col-12">
                                                                    <?php if ($products->{'product::discount'}->discount_percent > "1") { ?>
                                                                        <span class="discount-product">- <?php echo number_format($products->{'product::discount'}->discount_percent); ?> <?php echo getWording('index', 'baht') ?></span>
                                                                    <?php } else { ?>
                                                                        <span class="discount-product">- <?php echo ($products->{'product::discount'}->discount_percent * 100); ?>%</span>
                                                                    <?php } ?>
                                                                </div>
                                                                <span class="strikethrough">
                                                                    <?php echo number_format($products->{'product::retail_price'}); ?>
                                                                    <?php echo getWording('index', 'baht') ?>
                                                                </span>
                                                            </div>
                                                        </span>
                                                        <span class="col-6 mt-5" style="color: red;">
                                                            <?php if ($products->{'product::discount'}->total_price > "1") { ?>
                                                                <?php echo number_format($products->{'product::discount'}->total_price); ?>
                                                            <?php } else { ?>
                                                                0
                                                            <?php } ?>
                                                            <?php echo getWording('index', 'baht') ?>
                                                        </span>
                                                    </div>
                                                </h4>
                                            <?php } ?>


                                            <div class="row mt-5">
                                                <?php if ($token != '') { ?>
                                                    <div class="col-8 pr-1">
                                                        <button class="order_btn w-100" data-quantity="1" data-product-qty="<?php echo $products->{'product::in_stock_qty'} ?>" data-product-id="<?php echo $products->{'product::id'} ?>"><?php echo getWording('index', 'buy') ?></button>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="col-8 pr-1">
                                                        <a href="<?php echo base_url('Account/login') ?>"><button class="w-100" ? style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"><?php echo getWording('index', 'buy') ?></button></a>
                                                    </div>
                                                <?php } ?>

                                                <!-- data-bs-toggle="modal" data-bs-target="#compare_product_modal" -->

                                                <div class="col-4 pl-1">
                                                    <button type="button" class="filter_btn w-100 p-0" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" data-compare="<?php echo $products->{'product::id'} ?>">
                                                        <svg class="w-100">
                                                            <use xlink:href="#icon-filter"></use>
                                                        </svg>
                                                    </button>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <h5 class="mt-6">
                                                <b><?php echo getWording('index', 'interested_product') ?></b>
                                            </h5>
                                            <div class="row mt-5">
                                                <div class="col-8 pr-1">
                                                    <a href="<?php echo base_url('Contact') ?>"><button class="w-100" ? style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"><?php echo getWording('index', 'contact') ?></button></a>
                                                </div>
                                                <div class="col-4 pl-1">
                                                    <button id="test-btn" type="button" class="filter_btn w-100 p-0" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" data-compare="<?php echo $products->{'product::id'} ?>">
                                                        <svg class="w-100">
                                                            <use xlink:href="#icon-filter"></use>
                                                        </svg>
                                                    </button>
                                                </div>

                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>

                        <?php } else { ?>
                            <h3 class="mt-5 text-center"><?php echo getWording('product', 'no_product') ?></h3>

                        <?php } ?>

                    </div>
                    <?php if (!empty($product_list)) { ?>
                        <?php
                        $page_max = $product_list->page_max;
                        $page =  $product_list->page;
                        ?>
                        <div class="row justify-content-center mt-3">
                            <div class="col-6">
                                <nav aria-label="Page navigation example">
                                    <ul class=" justify-content-center">
                                        <nav aria-label="Page navigation">
                                            <ul class="pagination">
                                                <?php
                                                if ($page != '1') {
                                                ?>
                                                    <li class="page-item mr-auto">
                                                        <div class="product_var_type" href="#" data-category="" data-subcategory="" data-page="<?php echo $page - 1 ?>"><span class="page-link"><?php echo getWording('product', 'previous') ?></span></div>
                                                    </li>
                                                <?php
                                                }
                                                if ($page == '1') {
                                                ?>
                                                    <li class="page-item mr-auto disabled">
                                                        <div class="product_var_type" href="#"><span class="page-link"><?php echo getWording('product', 'previous') ?></span></div>
                                                    </li>
                                                <?php
                                                }
                                                $page_array = array();
                                                for ($i = 1; $i <= $page_max; $i++) {

                                                    array_push($page_array, $i);
                                                ?>
                                                    <li class="page-item <?php if ($page == $i) echo 'active' ?>">
                                                        <div class="product_var_type" href="" data-category="" data-subcategory="" data-page="<?php echo $i ?>"><span class="page-link"><?php echo $i; ?></span></div>
                                                    </li>
                                                <?php
                                                }
                                                if ($page == $page_max) {
                                                ?>
                                                    <li class="page-item ml-auto disabled">
                                                        <div class="product_var_type" href="#"><span class="page-link"><?php echo getWording('product', 'next') ?></span></div>
                                                    </li>
                                                <?php
                                                }
                                                if ($page != $page_max) {
                                                ?>
                                                    <li class="page-item ml-auto">
                                                        <div class="product_var_type" href="#" data-category="" data-subcategory="" data-page="<?php echo $page + 1 ?>"><span class="page-link"><?php echo getWording('product', 'next') ?></span></div>
                                                    </li>
                                                <?php
                                                }
                                                ?>
                                            </ul>

                                        </nav>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="row justify-content-center mt-3">
                            <div class="col-6">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination justify-content-center">
                                        <nav aria-label="Page navigation">
                                            <ul class="pagination">
                                            </ul>
                                        </nav>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    <?php } ?>



                </div>
            </div>
        </div>


    </section>

    <div class="collapse-bg d-none" id="collapse-bg" onclick="closeNav()"></div>
    <div class="collapse-bg d-none" id="collapse-bg-price" onclick="closeNavPrice()"></div>

    <svg aria-hidden="true" style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="icon-filter" width="100%" viewBox="0 0 25 25">
                <g>
                    <path d="M1.75,7.75h6.6803589c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891   S13.9000854,9.0452271,14.2356567,7.75H24.25C24.6640625,7.75,25,7.4140625,25,7s-0.3359375-0.75-0.75-0.75H14.2356567   c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891S8.7659302,4.9547729,8.4303589,6.25H1.75   C1.3359375,6.25,1,6.5859375,1,7S1.3359375,7.75,1.75,7.75z M11.3330078,5.4912109   c0.8320313,0,1.5087891,0.6767578,1.5087891,1.5087891s-0.6767578,1.5087891-1.5087891,1.5087891S9.8242188,7.8320313,9.8242188,7   S10.5009766,5.4912109,11.3330078,5.4912109z" fill="#1D1D1B" />
                    <path d="M24.25,12.25h-1.6061401c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891   S17.1741333,10.9547729,16.838562,12.25H1.75C1.3359375,12.25,1,12.5859375,1,13s0.3359375,0.75,0.75,0.75h15.088562   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891s2.5670776-0.963562,2.9026489-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,12.25,24.25,12.25z M19.7412109,14.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891S21.25,12.1679688,21.25,13   S20.5732422,14.5087891,19.7412109,14.5087891z" fill="#1D1D1B" />
                    <path d="M24.25,18.25H9.7181396c-0.3355103-1.2952271-1.5037842-2.2587891-2.9017334-2.2587891   c-1.3987427,0-2.5670776,0.963562-2.9026489,2.2587891H1.75C1.3359375,18.25,1,18.5859375,1,19s0.3359375,0.75,0.75,0.75h2.1637573   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891c1.3979492,0,2.5662231-0.963562,2.9017334-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,18.25,24.25,18.25z M6.8164063,20.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891   c0.8310547,0,1.5078125,0.6767578,1.5078125,1.5087891S7.6474609,20.5087891,6.8164063,20.5087891z" fill="#1D1D1B" />
                </g>
            </symbol>
        </defs>
    </svg>

    <div class="mt-5"> <?php include('footer.php') ?></div>

    <script>
        var category = '<?php if (!empty($product_list)) {
                            echo $product_list->product_category_id;
                        } ?>';
        var subcategory = '<?php if (!empty($product_list)) {
                                echo $product_list->product_sub_category_id;
                            } ?>';
        var price_rate;


        $(document).ready(function() {

            $('#list').click(function(event) {
                event.preventDefault();
                $('#products .item').addClass('list-group-item');
                $('#border-images ').removeClass('border-images');
                $('#border-bottom ').addClass('border-bottom');
                $('.product-item .row .col-4').addClass('col-2')
                $('.product-item .row .col-4').removeClass('col-4');
                $('.product-item .row .col-6').addClass('col-3 mt-1')
                $('.product-item .row .col-6').removeClass('col-6');
                $('.product-item .row .col-8').addClass('col-4')
                $('.product-item .row .col-8').removeClass('col-8');
                $('.product-item img').removeClass('w-100');
                $('.product-item img').addClass('w-20');
                $('.product-item h4').addClass('pl-5 ');
                $('.product-item span').addClass('pl-5');
                $('div ').removeClass('mt-4');
                $('#products ').addClass('mt-2');


            });
            $('#grid').click(function(event) {
                event.preventDefault();
                $('#border-images ').addClass('border-images');
                $('#border-bottom ').removeClass('border-bottom');
                $('#products .item').removeClass('list-group-item');
                $('#products .item').addClass('grid-group-item');
                $('.product-item .row .col-4').addClass('col-8')
                $('.product-item .row .col-4').removeClass('col-4');
                $('.product-item .row .col-3').addClass('col-6');
                $('.product-item .row .col-3').removeClass('col-3 mt-1');
                $('.product-item .row .col-2').addClass('col-4')
                $('.product-item .row .col-2').removeClass('col-2');
                $('.product-item .row .col-8').removeClass('col-3');
                $('.product-item img').removeClass('w-20');
                $('.product-item img').addClass('w-100');
                $('.product-item h4').removeClass('pl-5 ');
                $('.product-item span').removeClass('pl-5');
                $('#products').addClass('mt-4')
                $('.product-item').addClass('mt-4');
            });

            $('.product_var_type').click(function() {
                event.preventDefault();
                page = $(this).data('page');

                console.log(category);
                console.log(subcategory);
                console.log(page);

                var data = {
                    "category": category,
                    "sub_category": subcategory,
                    "page": page,
                }

                onClick(data);
            });

        });
        // $('.product-fillter').click(function() {
        //     console.log('Test-fillter');
        // });
        $(".product-fillter").change(function() {

            console.log($(this).val());

            var data = {
                "category": category,
                "sub_category": subcategory,
                "page": '1',
                "price_rate_id": price_rate,
                "sale_type": $(this).val(),
                "show_item": $('#product_amount').val(),
            }

            onClick(data);
        });

        $('.select_category').click(function() {
            category = $(this).data('category');
            subcategory = $(this).data('subcategory');
            event.preventDefault();
            window.history.pushState("", "", $(this).attr('href'));

            var data = {
                "category": category,
                "sub_category": subcategory,
                "page": '1',
                "price_rate_id": price_rate,
                "sale_type": $('#product-fillter').val(),
                "show_item": $('#product_amount').val(),
            }

            console.log(data);

            var send_data = {
                "url": "<?php echo base_url('/Product') ?>",
                "method": "POST",
                "data": data
            }

            var base_url = 'location.href="' + '<?php echo base_url() ?>' + '"';

            $.ajax(send_data).done(function(response) {
                console.log(response);
                $("#products").empty();
                $(".pagination").empty();


                var html
                var pagination

                var baht_word = '<?php echo getWording('index', 'baht') ?>';
                var buy_word = '<?php echo getWording('index', 'buy') ?>'
                var interest_word = '<?php echo getWording('index', 'interested_product') ?>'
                var contact_word = '<?php echo getWording('index', 'contact') ?>'
                var no_product = '<?php echo getWording('product', 'no_product') ?>'

                if (response.code == "0x0000-00000") {
                    $('.btn.btn-secondary.buttomsale.technical_filter').fadeOut();
                    $.each(response.data.product_mains, function(key, value) {
                        console.log(response.data.product_mains);
                        var base_url = '<?php echo base_url() ?>';
                        var check_image = '';
                        // if (value['product::images']) {
                        //     check_image = value['product::images'][0];
                        // }
                        if (value.hasOwnProperty('product::images')) {
                            check_image =
                                '<a href="' + base_url + '/Product/Details/' + value['product::id'] + '">' +
                                '<img class="w-100" src="' + value['product::images'][0] + '" alt="">' +
                                '</a>'
                        } else {
                            check_image =
                                '<a href="' + base_url + '/Product/Details/' + value['product::id'] + '">' +
                                '<img class="w-100" src="' + base_url + '/assets/img/no-images.png" alt="">' +
                                '</a>'
                        }
                        var token_user = '<?php echo $codeigniter_instance->session->userdata('member::token') != null ? $codeigniter_instance->session->userdata('member::token') : ''; ?>';
                        console.log('token', token_user);
                        // console.log('value', value['product::name_th']);
                        var search_key = Object.keys(value);
                        if (value['product::retail_price'] != null) {
                            if (search_key.indexOf("product::discount") >= 0) {
                                var discount = ""
                                if (value['product::discount'].discount_percent < "1") {
                                    discount = value['product::discount'].discount_percent * 100;
                                    baht_word_1 = '<?php echo getWording('index', 'percent') ?>';
                                } else {
                                    discount = value['product::discount'].discount_percent;
                                    baht_word_1 = '<?php echo getWording('index', 'baht') ?>';
                                };
                                var discount_price = ""
                                if (value['product::discount'].total_price > "0") {
                                    discount_price = value['product::discount'].total_price;
                                } else {
                                    discount_price = "0";
                                };
                                if (token_user != "") {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-1"><div class="row"><span class="col-6"><div class="row"><div class="col-12">' +
                                        '<span class="discount-product col-4">- ' + discount + ' ' + baht_word_1 + '</span> </div></div>' +
                                        '<span class="strikethrough">' + formatNumber(value['product::retail_price']) + ' ' + baht_word + '</span></span>' +
                                        '<span class="col-6 mt-5" style="color: red;">' + formatNumber(discount_price) + ' ' + baht_word + '</span></div></h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<button class="order_btn w-100" data-quantity="1"  data-product-id="' + value['product::id'] + '"' + 'data-product-qty="' + value['product::in_stock_qty'] + '"  >' + buy_word + '</button>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                } else {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-1"><div class="row"><span class="col-6"><div class="row"><div class="col-12">' +
                                        '<span class="discount-product col-4">- ' + discount + ' ' + baht_word_1 + '</span> </div></div>' +
                                        '<span class="strikethrough">' + formatNumber(value['product::retail_price']) + ' ' + baht_word + '</span></span>' +
                                        '<span class="col-6 mt-5" style="color: red;">' + formatNumber(discount_price) + ' ' + baht_word + '</span></div></h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Account/login"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + buy_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                }
                            } else {
                                if (token_user != "") {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + formatNumber(value['product::retail_price']) + ' ' + baht_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<button class="order_btn w-100" data-quantity="1"  data-product-id="' + value['product::id'] + '"' + 'data-product-qty="' + value['product::in_stock_qty'] + '"  >' + buy_word + '</button>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);

                                } else {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + formatNumber(value['product::retail_price']) + ' ' + baht_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Account/login"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + buy_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                }
                            }
                        } else {
                            if (search_key.indexOf("product::discount") >= 0) {
                                var discount = ""
                                if (value['product::discount'].discount_percent < "1") {
                                    discount = value['product::discount'].discount_percent * 100;
                                    baht_word_1 = '<?php echo getWording('index', 'percent') ?>';
                                } else {
                                    discount = value['product::discount'].discount_percent;
                                    baht_word_1 = '<?php echo getWording('index', 'baht') ?>';
                                };
                                var discount_price = ""
                                if (value['product::discount'].total_price > "0") {
                                    discount_price = value['product::discount'].total_price;
                                } else {
                                    discount_price = "0";
                                };
                                if (token_user != "") {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + interest_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Contact"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + contact_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                } else {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + interest_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Contact"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + contact_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                }
                            } else {
                                if (token_user != "") {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + interest_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Contact"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + contact_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);

                                } else {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + interest_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Contact"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + contact_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                }
                            }
                        }
                    });


                    // <form>
                    //     <input name="cart_id[0]" value="1">
                    //     <input name="cart_id[1]" value="2">
                    //     <input name="total_price[0]" value="1">
                    //     <input name="total_price[1]" value="2">
                    // </form>

                    // $('form').append('<input name="cart_id[0]" value="1">')
                    // previouse_page = response.data.page-1;

                    createPagination(response);


                } else {
                    html = '<h3 class="mt-5 text-center">' + no_product + '</h3>'
                    $("#products").append(html);
                }
                // console.log(response);
            });
            // window.history.pushState("", "", $(this).attr('href'));
        });

        $('.select_subcategory').click(function() {
            event.preventDefault();
            window.history.pushState("", "", $(this).attr('href'));

            category = $(this).data('category');
            subcategory = $(this).data('subcategory');

            var data = {
                "category": category,
                "sub_category": subcategory,
                "page": '1',
                "price_rate_id": price_rate,
            }

            console.log(data);

            var send_data = {
                "url": "<?php echo base_url('/Product') ?>",
                "method": "POST",
                "data": data
            }

            var base_url = 'location.href="' + '<?php echo base_url() ?>' + '"';

            $.ajax(send_data).done(function(response) {
                // console.log(response);
                $("#products").empty();
                $(".pagination").empty();

                var html
                var pagination

                var baht_word = '<?php echo getWording('index', 'baht') ?>';
                var buy_word = '<?php echo getWording('index', 'buy') ?>'
                var interest_word = '<?php echo getWording('index', 'interested_product') ?>'
                var contact_word = '<?php echo getWording('index', 'contact') ?>'
                var no_product = '<?php echo getWording('product', 'no_product') ?>'

                if (response.code == "0x0000-00000") {
                    console.log(response);
                    // console.log(response.data.technical_list);
                    $.each(response.data.product_mains, function(key, value) {
                        var base_url = '<?php echo base_url() ?>';
                        var check_image = '';
                        if (value.hasOwnProperty('product::images')) {
                            check_image =
                                '<a href="' + base_url + '/Product/Details/' + value['product::id'] + '">' +
                                '<img class="w-100" src="' + value['product::images'][0] + '" alt="">' +
                                '</a>'
                        } else {
                            check_image =
                                '<a href="' + base_url + '/Product/Details/' + value['product::id'] + '">' +
                                '<img class="w-100" src="' + base_url + '/assets/img/no-images.png" alt="">' +
                                '</a>'
                        }
                        var token_user = '<?php echo $codeigniter_instance->session->userdata('member::token') != null ? $codeigniter_instance->session->userdata('member::token') : ''; ?>';
                        console.log('token', token_user);
                        var search_key = Object.keys(value);
                        if (value['product::retail_price'] != null) {
                            if (search_key.indexOf("product::discount") >= 0) {
                                var discount = ""
                                if (value['product::discount'].discount_percent < "1") {
                                    discount = value['product::discount'].discount_percent * 100;
                                    baht_word_1 = '<?php echo getWording('index', 'percent') ?>';
                                } else {
                                    discount = value['product::discount'].discount_percent;
                                    baht_word_1 = '<?php echo getWording('index', 'baht') ?>';
                                };
                                var discount_price = ""
                                if (value['product::discount'].total_price > "0") {
                                    discount_price = value['product::discount'].total_price;
                                } else {
                                    discount_price = "0";
                                };
                                if (token_user != "") {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-1"><div class="row"><span class="col-6"><div class="row"><div class="col-12">' +
                                        '<span class="discount-product col-4">- ' + discount + ' ' + baht_word_1 + '</span> </div></div>' +
                                        '<span class="strikethrough">' + formatNumber(value['product::retail_price']) + ' ' + baht_word + '</span></span>' +
                                        '<span class="col-6 mt-5" style="color: red;">' + formatNumber(discount_price) + ' ' + baht_word + '</span></div></h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<button class="order_btn w-100" data-quantity="1"  data-product-id="' + value['product::id'] + '"' + 'data-product-qty="' + value['product::in_stock_qty'] + '"  >' + buy_word + '</button>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                } else {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-1"><div class="row"><span class="col-6"><div class="row"><div class="col-12">' +
                                        '<span class="discount-product col-4">- ' + discount + ' ' + baht_word_1 + '</span> </div></div>' +
                                        '<span class="strikethrough">' + formatNumber(value['product::retail_price']) + ' ' + baht_word + '</span></span>' +
                                        '<span class="col-6 mt-5" style="color: red;">' + formatNumber(discount_price) + ' ' + baht_word + '</span></div></h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Account/login"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + buy_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                }
                            } else {
                                if (token_user != "") {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + formatNumber(value['product::retail_price']) + ' ' + baht_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<button class="order_btn w-100" data-quantity="1"  data-product-id="' + value['product::id'] + '"' + 'data-product-qty="' + value['product::in_stock_qty'] + '"  >' + buy_word + '</button>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button"  data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);

                                } else {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + formatNumber(value['product::retail_price']) + ' ' + baht_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Account/login"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + buy_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                }
                            }
                        } else {
                            if (search_key.indexOf("product::discount") >= 0) {
                                var discount = ""
                                if (value['product::discount'].discount_percent < "1") {
                                    discount = value['product::discount'].discount_percent * 100;
                                    baht_word_1 = '<?php echo getWording('index', 'percent') ?>';
                                } else {
                                    discount = value['product::discount'].discount_percent;
                                    baht_word_1 = '<?php echo getWording('index', 'baht') ?>';
                                };
                                var discount_price = ""
                                if (value['product::discount'].total_price > "0") {
                                    discount_price = value['product::discount'].total_price;
                                } else {
                                    discount_price = "0";
                                };
                                if (token_user != "") {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + interest_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Contact"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + contact_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                } else {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + interest_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Contact"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + contact_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                }
                            } else {
                                if (token_user != "") {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + interest_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Contact"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + contact_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);

                                } else {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + interest_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Contact"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + contact_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                }
                            }
                        }
                    });

                    $('.technical_list_checkbox').empty();

                    console.log(Object.keys(response.data).length);
                    if (Object.keys(response.data).length == 9) {
                        // console.log('have');
                        // $('.btn.btn-secondary.buttomsale.technical_filter').fadeIn();
                        if (response.data.technical_list.length < "1") {
                            $.each(response.data.technical_list, function(key, value) {
                                technical = '<li>' +
                                    '<label class="container cartlist-checkbox technical">' +
                                    '<input type="checkbox" value="' + value.id + '">' +
                                    '<span class="checkmark ml-8 ml-md-5"></span>' +
                                    '<span class="ml-3">' + value.name_th + '</span>' +
                                    '</label>' +
                                    '</li>'

                                $('.technical_list_checkbox').append(technical);
                            });
                            console.log(technical);
                            $('.btn.btn-secondary.buttomsale.technical_filter').fadeIn();
                        } else {
                            $('.btn.btn-secondary.buttomsale.technical_filter').fadeOut();
                        }
                    } else {
                        // console.log('not have');
                        $('.btn.btn-secondary.buttomsale.technical_filter').fadeOut();
                    }


                    // previouse_page = response.data.page-1;

                    createPagination(response);

                } else {
                    console.log(response);
                    html = '<h3 class="mt-5 text-center">' + no_product + '</h3>'
                    $("#products").append(html);
                    $('.btn.btn-secondary.buttomsale.technical_filter').fadeOut();
                }
                // console.log(response);
            });
        });

        $('.product_var_type').click(function() {
            console.log('change_page');
            event.preventDefault();
            // window.history.pushState("", "", $(this).attr('href'));

            // category = $(this).data('category');
            // subcategory = $(this).data('subcategory');
            page = $(this).data('page');

            console.log(category, subcategory, page);

            var data = {
                "category": category,
                "sub_category": subcategory,
                "page": page,
                "price_rate_id": price_rate
            }
        });

        $(".cartlist-checkbox.price_rate input").change(function() {

            $(".cartlist-checkbox input").prop('checked', false);
            $(this).prop('checked', true)
            price_rate = $(this).val();

            if (this.checked) {
                var data = {
                    "category": category,
                    "sub_category": subcategory,
                    "page": '1',
                    "price_rate_id": price_rate
                }

                console.log(data);

                var base_url = 'location.href="' + '<?php echo base_url() ?>' + '"';

                onClick(data);
            } else {
                var data = {
                    "category": category,
                    "sub_category": subcategory,
                    "page": '1',
                    "price_rate_id": price_rate
                }

                console.log(data);

                var base_url = 'location.href="' + '<?php echo base_url() ?>' + '"';

                onClick(data);
                console.log('unchecked!!!');
            }
        });

        var technical = [];
        $(".cartlist-checkbox.technical input").change(function() {

            // $(".cartlist-checkbox input").prop('checked', false);
            // $(this).prop('checked', true)
            // price_rate = $(this).val();
            // console.log(technical);
            if (this.checked) {
                technical.push($(this).val());
                console.log(technical);
                var data = {
                    "category": category,
                    "sub_category": subcategory,
                    "page": '1',
                    "price_rate_id": price_rate,
                    "technical_spec_id": technical
                }

                console.log(data);

                var base_url = 'location.href="' + '<?php echo base_url() ?>' + '"';

                onClick(data);
            } else {
                // $(this).prop('checked', false)
                var removeItem = $(this).val();
                technical = jQuery.grep(technical, function(value) {
                    return value != removeItem;
                });

                // console.log(technical);
                var data = {
                    "category": category,
                    "sub_category": subcategory,
                    "page": '1',
                    "price_rate_id": price_rate,
                    "technical_spec_id": technical
                }

                console.log(data);

                var base_url = 'location.href="' + '<?php echo base_url() ?>' + '"';

                onClick(data);
                // console.log('unchecked!!!');
            }
        });

        $('.reset_filter').click(function() {
            event.preventDefault();
            window.history.pushState("", "", $(this).attr('href'));

            category = $(this).data('category');
            subcategory = $(this).data('subcategory');

            var data = {
                "category": '',
                "sub_category": '',
                "page": '1',
                "price_rate_id": ''
            }

            console.log(data);

            var send_data = {
                "url": "<?php echo base_url('/Product') ?>",
                "method": "POST",
                "data": data
            }

            var base_url = 'location.href="' + '<?php echo base_url() ?>' + '"';

            $.ajax(send_data).done(function(response) {
                console.log(response);
                $("#products").empty();
                $(".pagination").empty();

                var html
                var pagination

                var baht_word = '<?php echo getWording('index', 'baht') ?>';
                var buy_word = '<?php echo getWording('index', 'buy') ?>'
                var interest_word = '<?php echo getWording('index', 'interested_product') ?>'
                var contact_word = '<?php echo getWording('index', 'contact') ?>'
                var no_product = '<?php echo getWording('product', 'no_product') ?>'

                if (response.code == "0x0000-00000") {
                    $.each(response.data.product_mains, function(key, value) {
                        $(".cartlist-checkbox input").prop('checked', false);
                        var base_url = '<?php echo base_url() ?>';
                        var check_image = '';
                        if (value.hasOwnProperty('product::images')) {
                            check_image =
                                '<a href="' + base_url + '/Product/Details/' + value['product::id'] + '">' +
                                '<img class="w-100" src="' + value['product::images'][0] + '" alt="">' +
                                '</a>'
                        } else {
                            check_image =
                                '<a href="' + base_url + '/Product/Details/' + value['product::id'] + '">' +
                                '<img class="w-100" src="' + base_url + '/assets/img/no-images.png" alt="">' +
                                '</a>'
                        }
                        var token_user = '<?php echo $codeigniter_instance->session->userdata('member::token') != null ? $codeigniter_instance->session->userdata('member::token') : ''; ?>';
                        console.log('token', token_user);
                        var search_key = Object.keys(value);
                        if (value['product::retail_price'] != null) {
                            if (search_key.indexOf("product::discount") >= 0) {
                                var discount = ""
                                if (value['product::discount'].discount_percent < "1") {
                                    discount = value['product::discount'].discount_percent * 100;
                                    baht_word_1 = '<?php echo getWording('index', 'percent') ?>';
                                } else {
                                    discount = value['product::discount'].discount_percent;
                                    baht_word_1 = '<?php echo getWording('index', 'baht') ?>';
                                };
                                var discount_price = ""
                                if (value['product::discount'].total_price > "0") {
                                    discount_price = value['product::discount'].total_price;
                                } else {
                                    discount_price = "0";
                                };
                                if (token_user != "") {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-1"><div class="row"><span class="col-6"><div class="row"><div class="col-12">' +
                                        '<span class="discount-product col-4">- ' + discount + ' ' + baht_word_1 + '</span> </div></div>' +
                                        '<span class="strikethrough">' + formatNumber(value['product::retail_price']) + ' ' + baht_word + '</span></span>' +
                                        '<span class="col-6 mt-5" style="color: red;">' + formatNumber(discount_price) + ' ' + baht_word + '</span></div></h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<button class="order_btn w-100" data-quantity="1"  data-product-id="' + value['product::id'] + '"' + 'data-product-qty="' + value['product::in_stock_qty'] + '"  >' + buy_word + '</button>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                } else {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-1"><div class="row"><span class="col-6"><div class="row"><div class="col-12">' +
                                        '<span class="discount-product col-4">- ' + discount + ' ' + baht_word_1 + '</span> </div></div>' +
                                        '<span class="strikethrough">' + formatNumber(value['product::retail_price']) + ' ' + baht_word + '</span></span>' +
                                        '<span class="col-6 mt-5" style="color: red;">' + formatNumber(discount_price) + ' ' + baht_word + '</span></div></h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Account/login"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + buy_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                }
                            } else {
                                if (token_user != "") {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + formatNumber(value['product::retail_price']) + ' ' + baht_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<button class="order_btn w-100" data-quantity="1"  data-product-id="' + value['product::id'] + '"' + 'data-product-qty="' + value['product::in_stock_qty'] + '"  >' + buy_word + '</button>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);

                                } else {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + formatNumber(value['product::retail_price']) + ' ' + baht_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Account/login"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + buy_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                }
                            }
                        } else {
                            if (search_key.indexOf("product::discount") >= 0) {
                                var discount = ""
                                if (value['product::discount'].discount_percent < "1") {
                                    discount = value['product::discount'].discount_percent * 100;
                                    baht_word_1 = '<?php echo getWording('index', 'percent') ?>';
                                } else {
                                    discount = value['product::discount'].discount_percent;
                                    baht_word_1 = '<?php echo getWording('index', 'baht') ?>';
                                };
                                var discount_price = ""
                                if (value['product::discount'].total_price > "0") {
                                    discount_price = value['product::discount'].total_price;
                                } else {
                                    discount_price = "0";
                                };
                                if (token_user != "") {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + interest_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Contact"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + contact_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                } else {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + interest_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Contact"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + contact_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                }
                            } else {
                                if (token_user != "") {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + interest_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Contact"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + contact_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);

                                } else {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + interest_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Contact"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + contact_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                }
                            }
                        }
                    });

                    // previouse_page = response.data.page-1;

                    createPagination(response);

                } else {
                    html = '<h3 class="mt-5 text-center">' + no_product + '</h3>'
                    $("#products").append(html);
                }
                // console.log(response);
            });
        });



        $('.product_amount').change(function() {

            console.log('change_page');
            event.preventDefault();
            // window.history.pushState("", "", $(this).attr('href'));

            // category = $(this).data('category');
            // subcategory = $(this).data('subcategory');
            // page = $(this).data('page');

            // console.log(category, subcategory, page);

            var data = {
                "category": category,
                "sub_category": subcategory,
                "page": '1',
                "sale_type": $("#product-fillter").val(),
                "show_item": $(this).val(),
                "price_rate_id": price_rate
            }

            onClick(data);

        });


        function createPagination(response) {
            console.log(response.data.page);
            if (response.data.page != '1') {
                previouse_page = '<li class="page-item mr-auto"><a class="product_var_type" href="#" data-category="" data-subcategory="" data-page="' + (parseInt(response.data.page) - 1) + '"><?php echo getWording('product', 'previous') ?></a></li>';
                $(".pagination").append(previouse_page);
            }

            if (response.data.page == '1') {
                previouse_page = '<li class="page-item mr-auto disabled"><a class="page-link" href="#"><?php echo getWording('product', 'previous') ?></a></li>';
                $(".pagination").append(previouse_page);
            }

            var page_array = [];

            for (i = 1; i <= response.data.page_max; i++) {
                page_array.push(i);
                if (response.data.page == i) {
                    page_list = '<li class="page-item active"><div class="product_var_type" href="" data-category="" data-subcategory="" data-page="' + i + '"><span class="page-link">' + i + '</span></div></li>';
                    $(".pagination").append(page_list);
                } else {
                    page_list = '<li class="page-item"><div class="product_var_type" href="" data-category="" data-subcategory="" data-page="' + i + '"><span class="page-link">' + i + '</span></div></li>';
                    $(".pagination").append(page_list);
                }
            }

            if (response.data.page == response.data.page_max) {
                next_page = '<li class="page-item ml-auto disabled"><a class="page-link" href="#"><?php echo getWording('product', 'next') ?></a></li>';
                $(".pagination").append(next_page);
            }

            if (response.data.page != response.data.page_max) {
                next_page = '<li class="page-item ml-auto"><a class="product_var_type" href="#" data-category="" data-subcategory="" data-page="' + (parseInt(response.data.page) + 1) + '"><?php echo getWording('product', 'next') ?></a></li>';
                $(".pagination").append(next_page);
            }


            $('.product_var_type').click(function() {
                event.preventDefault();
                // window.history.pushState("", "", $(this).attr('href'));

                // category = $(this).data('category');
                // subcategory = $(this).data('subcategory');
                page = $(this).data('page');

                console.log(category);
                console.log(subcategory);
                console.log(page);

                var data = {
                    "category": category,
                    "sub_category": subcategory,
                    "page": page,
                    "price_rate_id": price_rate
                }

                onClick(data);


            });
        }
        /* Set the width of the side navigation to 250px and the left margin of the page content to 250px and add a black background color to body */
        function openNav() {
            console.log("opennav");
            $("#collapse-bg").removeClass("d-none");
            $("#collapse-bg").addClass("d-block");
            document.getElementById("mySidenav").style.width = "400px";
            document.getElementById("main").style.marginLeft = "400px";
            document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
        }

        /* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
        function closeNav() {
            console.log("closenav");
            $("#collapse-bg").addClass("d-none");
            $("#collapse-bg").removeClass("d-block");
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("main").style.marginLeft = "0";
            document.body.style.backgroundColor = "white";
        }

        function openNavPrice() {
            console.log("opennav");
            $("#collapse-bg-price").removeClass("d-none");
            $("#collapse-bg-price").addClass("d-block");
            document.getElementById("mySidenavPrice").style.width = "400px";
            // document.getElementById("main").style.marginLeft = "400px";
            document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
        }

        /* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
        function closeNavPrice() {
            console.log("closenav");
            $("#collapse-bg-price").addClass("d-none");
            $("#collapse-bg-price").removeClass("d-block");
            document.getElementById("mySidenavPrice").style.width = "0";
            // document.getElementById("main").style.marginLeft = "0";
            document.body.style.backgroundColor = "white";
        }


        function onClick(data) {
            console.log(data);

            var send_data = {
                "url": "<?php echo base_url('/Product')  ?>",
                "method": "POST",
                "data": data
            }

            var base_url = 'location.href="' + '<?php echo base_url()  ?>' + '"';

            $.ajax(send_data).done(function(response) {
                console.log(response);
                $("#products").empty();
                $(".pagination").empty();

                var html
                var pagination

                var baht_word = '<?php echo getWording('index', 'baht') ?>';
                var buy_word = '<?php echo getWording('index', 'buy') ?>'
                var interest_word = '<?php echo getWording('index', 'interested_product') ?>'
                var contact_word = '<?php echo getWording('index', 'contact') ?>'
                var no_product = '<?php echo getWording('product', 'no_product') ?>'

                if (response.code == "0x0000-00000") {
                    $.each(response.data.product_mains, function(key, value) {
                        var base_url = '<?php echo base_url()    ?>';
                        var check_image = '';
                        if (value.hasOwnProperty('product::images')) {
                            check_image =
                                '<a href="' + base_url + '/Product/Details/' + value['product::id'] + '">' +
                                '<img class="w-100" src="' + value['product::images'][0] + '" alt="">' +
                                '</a>'
                        } else {
                            check_image =
                                '<a href="' + base_url + '/Product/Details/' + value['product::id'] + '">' +
                                '<img class="w-100" src="' + base_url + '/assets/img/no-images.png" alt="">' +
                                '</a>'
                        }
                        var token_user = '<?php echo $codeigniter_instance->session->userdata('member::token') != null ? $codeigniter_instance->session->userdata('member::token') : ''; ?>';
                        console.log('token', token_user);

                        var search_key = Object.keys(value);
                        if (value['product::retail_price'] != null) {
                            if (search_key.indexOf("product::discount") >= 0) {
                                var discount = ""
                                if (value['product::discount'].discount_percent < "1") {
                                    discount = value['product::discount'].discount_percent * 100;
                                    baht_word_1 = '<?php echo getWording('index', 'percent') ?>';
                                } else {
                                    discount = value['product::discount'].discount_percent;
                                    baht_word_1 = '<?php echo getWording('index', 'baht') ?>';
                                };
                                var discount_price = ""
                                if (value['product::discount'].total_price > "0") {
                                    discount_price = value['product::discount'].total_price;
                                } else {
                                    discount_price = "0";
                                };
                                if (token_user != "") {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-1"><div class="row"><span class="col-6"><div class="row"><div class="col-12">' +
                                        '<span class="discount-product col-4">- ' + discount + ' ' + baht_word_1 + '</span> </div></div>' +
                                        '<span class="strikethrough">' + formatNumber(value['product::retail_price']) + ' ' + baht_word + '</span></span>' +
                                        '<span class="col-6 mt-5" style="color: red;">' + formatNumber(discount_price) + ' ' + baht_word + '</span></div></h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<button class="order_btn w-100" data-quantity="1"  data-product-id="' + value['product::id'] + '"' + 'data-product-qty="' + value['product::in_stock_qty'] + '"  >' + buy_word + '</button>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                } else {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-1"><div class="row"><span class="col-6"><div class="row"><div class="col-12">' +
                                        '<span class="discount-product col-4">- ' + discount + ' ' + baht_word_1 + '</span> </div></div>' +
                                        '<span class="strikethrough">' + formatNumber(value['product::retail_price']) + ' ' + baht_word + '</span></span>' +
                                        '<span class="col-6 mt-5" style="color: red;">' + formatNumber(discount_price) + ' ' + baht_word + '</span></div></h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Account/login"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + buy_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                }
                            } else {
                                if (token_user != "") {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + formatNumber(value['product::retail_price']) + ' ' + baht_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<button class="order_btn w-100" data-quantity="1"  data-product-id="' + value['product::id'] + '"' + 'data-product-qty="' + value['product::in_stock_qty'] + '"  >' + buy_word + '</button>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);

                                } else {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + formatNumber(value['product::retail_price']) + ' ' + baht_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Account/login"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + buy_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                }
                            }
                        } else {
                            if (search_key.indexOf("product::discount") >= 0) {
                                var discount = ""
                                if (value['product::discount'].discount_percent < "1") {
                                    discount = value['product::discount'].discount_percent * 100;
                                    baht_word_1 = '<?php echo getWording('index', 'percent') ?>';
                                } else {
                                    discount = value['product::discount'].discount_percent;
                                    baht_word_1 = '<?php echo getWording('index', 'baht') ?>';
                                };
                                var discount_price = ""
                                if (value['product::discount'].total_price > "0") {
                                    discount_price = value['product::discount'].total_price;
                                } else {
                                    discount_price = "0";
                                };
                                if (token_user != "") {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + interest_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Contact"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + contact_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                } else {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + interest_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Contact"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + contact_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                }
                            } else {
                                if (token_user != "") {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + interest_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Contact"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + contact_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);

                                } else {
                                    html =
                                        '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                        '<div class="product-item">' +
                                        '<div class="border-images">' +
                                        check_image +
                                        '</div>' +
                                        '<h4 class="mt-3">' + value['product::name_th'] + '</h4>' +
                                        '<span>' + value['product::model_name_th'] + '</span>' +
                                        '<h4 class="mt-6">' + interest_word + '</h4>' +
                                        '<div class="row mt-5">' +
                                        '<div class="col-8 pr-1">' +
                                        '<a href="' + base_url + '/Contact"><button class=" w-100"style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"' + value['product::id'] + '" >' + contact_word + '</button></a>' +
                                        '</div>' +
                                        '<div class="col-4 pl-1">' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" class="filter_btn w-100 p-0" data-compare="' + value['product::id'] + '">' +
                                        '<svg class="w-100">' +
                                        '<use xlink:href="#icon-filter"></use>' +
                                        '</svg>' +
                                        '</button>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>' +
                                        '</div>'
                                    $("#products").append(html);
                                }
                            }
                        }
                    });
                    createPagination(response);

                } else {
                    html = '<h3 class="mt-5 text-center">' + no_product + '</h3>'
                    $("#products").append(html);
                }
            });
        }
    </script>


</body>

</html>