<?php include('header.php') ?>
<style>
    .accordion-item {
        border-right: none;
        border-left: none;
    }

    .list-group-item {
        border: none;
    }

    .accordion-item a {
        color: #000000;
        text-decoration: none;
    }

    .accordion-button:not(.collapsed) {
        color: #000000;
        background-color: #F2F2F2;
    }
</style>

<body>
    <section class="all_product">
        <div class="col-12" style="background-color: #F0F0F0; text-align: center; padding: 70px;">
            <h4><b><?php echo getWording('product', 'all_product') ?></b></h4>
            <!-- <P>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</P> -->
        </div>
        <div class="d-flex">
            <div class="col-12  pt-2 pb-2 mt-3" style="background-color: lightgray; text-align:left;">
                <div class="container">
                    <a href="javascript:void(0)" class="btn btn-secondary buttomsale" onclick="openNavPrice()"><?php echo getWording('product', 'price') ?></a>
                    <?php if ($_GET['type'] == 'subcategory' && count($technical_list) > 0) { ?>
                        <a href="#" class="btn btn-secondary buttomsale technical_filter" onclick="openNav()"><?php echo getWording('product', 'use') ?></a>
                    <?php } else { ?>
                        <?php if (count($technical_list) > 0) { ?>
                            <a href="#" class="btn btn-secondary buttomsale technical_filter" onclick="openNav()"><?php echo getWording('product', 'use') ?></a>
                        <?php } else { ?>
                            <a href="#" class="btn btn-secondary buttomsale technical_filter" onclick="openNav()" style="display: none;"><?php echo getWording('product', 'use') ?></a>
                        <?php } ?>
                    <?php } ?>
                    <div id="mySidenavPrice" class="sidenav sidenav_1 pl-3 ">
                        <div class="container">
                            <h4 class="ml-5 ml-md-0"><?php echo getWording('product', 'price') ?></h4>
                            <ul>
                                <?php foreach ($price_rate->price_rate as $key => $price_rate_list) { ?>
                                    <li>
                                        <label class="container cartlist-checkbox price_rate">
                                            <input type="checkbox" value="<?php echo $price_rate_list->id ?>">
                                            <span class="checkmark ml-8 ml-md-5"></span>
                                            <span class="ml-3"><?php echo $price_rate_list->price_min ?> - <?php echo $price_rate_list->price_max ?> <?php echo getWording('index', 'baht') ?></span>
                                        </label>
                                    </li>
                                <?php } ?>
                            </ul>
                            <button class="btn reset_filter"><?php echo getWording('product', 'clear') ?></button>
                        </div>
                        <a href="javascript:void(0)" class="closebtn" onclick="closeNavPrice()">&times;</a>
                    </div>
                    <div id="mySidenav" class="sidenav ">
                        <div class="container">
                            <h4 class="ml-5 ml-md-0"><?php echo getWording('product', 'use') ?></h4>
                            <ul class="technical_list_checkbox">
                                <?php foreach ($technical_list as $key => $technical) { ?>
                                    <li>
                                        <label class="container cartlist-checkbox technical">
                                            <input type="checkbox" value="<?php echo $technical->id ?>">
                                            <span class="checkmark ml-8 ml-md-5"></span>
                                            <span class="ml-3"><?php echo getVariable($technical, 'name') ?></span>
                                        </label>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    </div>

                    <!-- Use any element to open the sidenav -->
                    <!-- <span onclick="openNav()">Sidebar</span> -->
                </div>
            </div>
        </div>
        <div class="container ">
            <div class="row justify-content-center">
                <div class="col-sm-6 col-xl-9">
                    <div class="row">
                        <div class="col-sm-6 col-xl-6 mt-3 ">
                            <a href="#" class="btn btn-outline-dark buttomsale" id="grid"><i class="fas fa-th-large "></i></a>
                            <a href="#" class="btn btn-outline-dark buttomsale" id="list"><i class="fas fa-list"></i></a>
                        </div>
                        <div class="col-sm-6 col-xl-6 mt-3 " style="text-align: right;">
                            <?php echo getWording('product', 'filter') ?>:
                            <select id="fillter" name="fillter" style="width: 150px;height: 30px;">
                                <option value="popula"><?php echo getWording('product', 'best_seller') ?></option>
                                <option value="sale"><?php echo getWording('product', 'sale') ?></option>
                            </select>

                            <?php echo getWording('product', 'show') ?>:
                            <select class="product_amount" id="fillter" name="fillter" style="width: 50px;height: 30px;">
                                <option value="12">12</option>
                                <option value="21">21</option>
                                <option value="27">27</option>
                            </select>
                        </div>
                    </div>
                    <div class="row" id="products">
                        <?php if (!empty($solution_product->products)) { ?>
                            <?php foreach ($solution_product->products as $key => $products) { ?>
                                <div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">
                                    <div class="product-item">
                                        <div class="border-images">
                                            <?php if (property_exists($products, 'product::images')) { ?>
                                                <a href="<?php echo base_url('/Product/details/' . $products->{'product::id'}) ?>">
                                                    <img class="w-100" src="<?php echo $products->{'product::images'}[0] ?>" alt="">
                                                </a>
                                            <?php } else { ?>
                                                <a href="<?php echo base_url('/Product/details/' . $products->{'product::id'}) ?>">
                                                    <img class="w-100" src="<?php echo base_url('/assets/img/no-images.png') ?>" alt="">
                                                </a>
                                            <?php } ?>
                                        </div>
                                        <a href="<?php echo base_url('/Product/details/' . $products->{'product::id'}) ?>">
                                            <h4 class="mt-3"><?php echo getVariable($products, 'product::name')  ?></h4>
                                        </a>

                                        <?php if ($products->{'product::model_name_th'} == '') { ?>
                                            <span style="color:white;">...</span>
                                        <?php } else if ($products->{'product::model_name_en'} == '') { ?>
                                            <span style="color:white;">...</span>
                                        <?php } else { ?>
                                            <span><?php echo getVariable($products, 'product::model_name') ?></span>
                                        <?php } ?>
                                        <?php if ($products->{'product::retail_price'} != null) { ?>
                                            <?php if (empty($products->{'product::discount'})) { ?>
                                                <h4 class="mt-6">
                                                    <?php echo number_format($products->{'product::retail_price'}); ?>
                                                    <?php echo getWording('index', 'baht') ?>
                                                </h4>
                                            <?php } else { ?>
                                                <h4 class="mt-1">
                                                    <div class="row">
                                                        <span class="col-6">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <?php if ($products->{'product::discount'}->discount_percent > "1") { ?>
                                                                        <span class="discount-product">- <?php echo number_format($products->{'product::discount'}->discount_percent); ?> <?php echo getWording('index', 'baht') ?></span>
                                                                    <?php } else { ?>
                                                                        <span class="discount-product">- <?php echo ($products->{'product::discount'}->discount_percent * 100); ?>%</span>
                                                                    <?php } ?>
                                                                </div>
                                                                <span class="strikethrough">
                                                                    <?php echo number_format($products->{'product::retail_price'}); ?>
                                                                    <?php echo getWording('index', 'baht') ?>
                                                                </span>
                                                            </div>
                                                        </span>
                                                        <span class="col-6 mt-5" style="color: red;">
                                                            <?php if ($products->{'product::discount'}->total_price > "1") { ?>
                                                                <?php echo number_format($products->{'product::discount'}->total_price); ?>
                                                            <?php } else { ?>
                                                                0
                                                            <?php } ?>
                                                            <?php echo getWording('index', 'baht') ?>
                                                        </span>
                                                    </div>
                                                </h4>
                                            <?php } ?>
                                            <div class="row mt-5">
                                                <?php if ($token != '') { ?>
                                                    <div class="col-8 pr-1">
                                                        <button class="order_btn w-100" data-quantity="1" data-product-qty="<?php echo $products->{'product::in_stock_qty'} ?>" data-product-id="<?php echo $products->{'product::id'} ?>"><?php echo getWording('index', 'buy') ?></button>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="col-8 pr-1">
                                                        <a href="<?php echo base_url('Account/login') ?>"><button class="w-100" ? style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"><?php echo getWording('index', 'buy') ?></button></a>
                                                    </div>
                                                <?php } ?>

                                                <div class="col-4 pl-1">
                                                    <button type="button" class="filter_btn w-100 p-0" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" data-compare="<?php echo $products->{'product::id'} ?>">
                                                        <svg class="w-100">
                                                            <use xlink:href="#icon-filter"></use>
                                                        </svg>
                                                    </button>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <h5 class="mt-6">
                                                <b><?php echo getWording('index', 'interested_product') ?></b>
                                            </h5>
                                            <div class="row mt-5">
                                                <div class="col-8 pr-1">
                                                    <a href="<?php echo base_url('Contact') ?>"><button class="w-100" ? style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"><?php echo getWording('index', 'contact') ?></button></a>
                                                </div>
                                                <div class="col-4 pl-1">
                                                    <button id="test-btn" type="button" class="filter_btn w-100 p-0" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" data-compare="<?php echo $products->{'product::id'} ?>">
                                                        <svg class="w-100">
                                                            <use xlink:href="#icon-filter"></use>
                                                        </svg>
                                                    </button>
                                                </div>

                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <h3 class="mt-5 text-center"><?php echo getWording('product', 'no_product') ?></h3>
                        <?php } ?>
                    </div>
                    <?php if (!empty($solution_product)) { ?>
                        <?php
                        $page_max = $solution_product->page_max;
                        $page =  $solution_product->page;
                        ?>
                        <div class="row justify-content-center mt-3">
                            <div class="col-6">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination justify-content-center">
                                        <nav aria-label="Page navigation">
                                            <ul class="pagination">
                                                <?php
                                                if ($page != '1') {
                                                ?>
                                                    <li class="page-item mr-auto">
                                                        <div class="product_var_type" href="#" data-category="" data-subcategory="" data-page="<?php echo $page - 1 ?>"><span class="page-link"><?php echo getWording('product', 'previous') ?></span></div>
                                                    </li>
                                                <?php
                                                }
                                                if ($page == '1') {
                                                ?>
                                                    <li class="page-item mr-auto disabled">
                                                        <div class="product_var_type" href="#"><span class="page-link"><?php echo getWording('product', 'previous') ?></span></div>
                                                    </li>
                                                <?php
                                                }
                                                $page_array = array();
                                                for ($i = 1; $i <= $page_max; $i++) {

                                                    array_push($page_array, $i);
                                                ?>
                                                    <li class="page-item <?php if ($page == $i) echo 'active' ?>">
                                                        <div class="product_var_type" href="" data-category="" data-subcategory="" data-page="<?php echo $i ?>"><span class="page-link"><?php echo $i; ?></span></div>
                                                    </li>
                                                <?php
                                                }
                                                if ($page == $page_max) {
                                                ?>
                                                    <li class="page-item ml-auto disabled">
                                                        <div class="product_var_type" href="#"><span class="page-link"><?php echo getWording('product', 'next') ?></span></div>
                                                    </li>
                                                <?php
                                                }
                                                if ($page != $page_max) {
                                                ?>
                                                    <li class="page-item ml-auto">
                                                        <div class="product_var_type" href="#" data-category="" data-subcategory="" data-page="<?php echo $page + 1 ?>"><span class="page-link"><?php echo getWording('product', 'next') ?></span></div>
                                                    </li>
                                                <?php
                                                }
                                                ?>
                                            </ul>

                                        </nav>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="row justify-content-center mt-3">
                            <div class="col-6">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination justify-content-center">
                                        <nav aria-label="Page navigation">
                                            <ul class="pagination">
                                            </ul>
                                        </nav>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

    </section>



    <div class="collapse-bg d-none" id="collapse-bg" onclick="closeNav()"></div>
    <div class="collapse-bg d-none" id="collapse-bg-price" onclick="closeNavPrice()"></div>


    <div class="mt-5"> <?php include('footer.php') ?></div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <!-- <script src="vender/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->
    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
-->
    <svg aria-hidden="true" style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="icon-filter" width="100%" viewBox="0 0 25 25">
                <g>
                    <path d="M1.75,7.75h6.6803589c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891   S13.9000854,9.0452271,14.2356567,7.75H24.25C24.6640625,7.75,25,7.4140625,25,7s-0.3359375-0.75-0.75-0.75H14.2356567   c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891S8.7659302,4.9547729,8.4303589,6.25H1.75   C1.3359375,6.25,1,6.5859375,1,7S1.3359375,7.75,1.75,7.75z M11.3330078,5.4912109   c0.8320313,0,1.5087891,0.6767578,1.5087891,1.5087891s-0.6767578,1.5087891-1.5087891,1.5087891S9.8242188,7.8320313,9.8242188,7   S10.5009766,5.4912109,11.3330078,5.4912109z" fill="#1D1D1B" />
                    <path d="M24.25,12.25h-1.6061401c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891   S17.1741333,10.9547729,16.838562,12.25H1.75C1.3359375,12.25,1,12.5859375,1,13s0.3359375,0.75,0.75,0.75h15.088562   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891s2.5670776-0.963562,2.9026489-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,12.25,24.25,12.25z M19.7412109,14.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891S21.25,12.1679688,21.25,13   S20.5732422,14.5087891,19.7412109,14.5087891z" fill="#1D1D1B" />
                    <path d="M24.25,18.25H9.7181396c-0.3355103-1.2952271-1.5037842-2.2587891-2.9017334-2.2587891   c-1.3987427,0-2.5670776,0.963562-2.9026489,2.2587891H1.75C1.3359375,18.25,1,18.5859375,1,19s0.3359375,0.75,0.75,0.75h2.1637573   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891c1.3979492,0,2.5662231-0.963562,2.9017334-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,18.25,24.25,18.25z M6.8164063,20.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891   c0.8310547,0,1.5078125,0.6767578,1.5078125,1.5087891S7.6474609,20.5087891,6.8164063,20.5087891z" fill="#1D1D1B" />
                </g>
            </symbol>
        </defs>
    </svg>

    <!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> -->
    <!-- <script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'></script> -->
    <script>
        var category;
        var subcategory;
        var childsubmenu_category;
        var type;
        var price_rate;



        $(document).ready(function() {

            category = '<?php if ($solution_product->menu_category_id != '') {
                            echo $solution_product->menu_category_id;
                        } ?>';
            subcategory = '<?php if ($solution_product->submenu_category_id != '') {
                                echo $solution_product->submenu_category_id;
                            } ?>';
            childsubmenu_category = '<?php if ($solution_product->child_submenu_category_id != '') {
                                            echo $solution_product->child_submenu_category_id;
                                        } ?>';
            type = '<?php echo $_GET['type'] ?>';

            console.log(category);
            console.log(subcategory);
            console.log(childsubmenu_category);
            $('#list').click(function(event) {
                event.preventDefault();
                $('#products .item').addClass('list-group-item');
                $('.product-item .row .col-4').addClass('col-2')
                $('.product-item .row .col-4').removeClass('col-4');
                $('.product-item .row .col-8').addClass('col-3')
                $('.product-item .row .col-8').removeClass('col-8');
                $('.product-item img').removeClass('w-100');
                $('.product-item img').addClass('w-20');
                $('.product-item h4').addClass('pl-5 ');
                $('.product-item span').addClass('pl-5');
                $('div ').removeClass('mt-4');
                $('#products ').addClass('mt-2');
            });
            $('#grid').click(function(event) {
                event.preventDefault();
                $('#products .item').removeClass('list-group-item');
                $('#products .item').addClass('grid-group-item');
                $('.product-item .row .col-2').addClass('col-4')
                $('.product-item .row .col-2').removeClass('col-2');
                $('.product-item .row .col-3').addClass('col-8')
                $('.product-item .row .col-3').removeClass('col-3');
                $('.product-item img').removeClass('w-20');
                $('.product-item img').addClass('w-100');
                $('.product-item h4').removeClass('pl-5 ');
                $('.product-item span').removeClass('pl-5');
                $('#products').addClass('mt-4')
                $('.product-item').addClass('mt-4');
            });

            $('.product_var_type').click(function() {
                event.preventDefault();
                page = $(this).data('page');



                var data = {
                    "category": category,
                    "sub_category": subcategory,
                    "child_submenu_category": childsubmenu_category,
                    "page": page,
                    "type": type
                }

                onClick(data);
            });

        });

        $('.product_var_type').click(function() {

            event.preventDefault();
            // window.history.pushState("", "", $(this).attr('href'));

            // category = $(this).data('category');
            // subcategory = $(this).data('subcategory');
            page = $(this).data('page');



            var data = {
                "category": category,
                "sub_category": subcategory,
                "child_submenu_category": childsubmenu_category,
                "page": page,
                "type": type
            }
        });


        $('.product_amount').change(function() {

            event.preventDefault();
            // window.history.pushState("", "", $(this).attr('href'));

            // category = $(this).data('category');
            // subcategory = $(this).data('subcategory');
            // page = $(this).data('page');



            var data = {
                "category": category,
                "sub_category": subcategory,
                "child_submenu_category": childsubmenu_category,
                "page": '1',
                "type": type,
                "show_item": $(this).val()
            }

            onClick(data);

        });

        $(".cartlist-checkbox.price_rate input").change(function() {

            $(".cartlist-checkbox input").prop('checked', false);
            $(this).prop('checked', true)
            price_rate = $(this).val();

            if (this.checked) {
                var data = {
                    "category": category,
                    "sub_category": subcategory,
                    "child_submenu_category": childsubmenu_category,
                    "page": '1',
                    "price_rate_id": price_rate
                }



                var base_url = 'location.href="' + '<?php echo base_url() ?>' + '"';

                onClick(data);
            } else {
                var data = {
                    "category": category,
                    "sub_category": subcategory,
                    "child_submenu_category": childsubmenu_category,
                    "page": '1',
                    "price_rate_id": price_rate
                }



                var base_url = 'location.href="' + '<?php echo base_url() ?>' + '"';

                onClick(data);

            }
        });

        var technical = [];

        $(".cartlist-checkbox.technical input").change(function() {

            // $(".cartlist-checkbox.technical input").prop('checked', false);
            // $(this).prop('checked', true)
            // technical = $(this).val();

            if (this.checked) {
                technical.push($(this).val());
                var data = {
                    "category": category,
                    "sub_category": subcategory,
                    "child_submenu_category": childsubmenu_category,
                    "page": '1',
                    "price_rate_id": price_rate,
                    "technical_spec_id": technical
                }

                console.log(data);



                var base_url = 'location.href="' + '<?php echo base_url() ?>' + '"';

                onClick(data);
            } else {
                var removeItem = $(this).val();
                technical = jQuery.grep(technical, function(value) {
                    return value != removeItem;
                });

                var data = {
                    "category": category,
                    "sub_category": subcategory,
                    "child_submenu_category": childsubmenu_category,
                    "page": '1',
                    "price_rate_id": price_rate
                }

                console.log(data);

                var base_url = 'location.href="' + '<?php echo base_url() ?>' + '"';

                onClick(data);

            }
        });

        $('.reset_filter').click(function() {
            event.preventDefault();
            window.history.pushState("", "", $(this).attr('href'));

            // category = $(this).data('category');
            // subcategory = $(this).data('subcategory');

            var data = {
                "category": category,
                "sub_category": subcategory,
                "child_submenu_category": childsubmenu_category,
                "page": '1',
                "price_rate_id": ''
            }

            onClick(data);

        });



        function createPagination(response) {

            if (response.data.page != '1') {
                previouse_page = '<li class="page-item mr-auto"><a class="product_var_type" href="#" data-category="" data-subcategory="" data-page="' + (response.data.page - 1) + '"><?php echo getWording('product', 'previous') ?></a></li>';
                $(".pagination").append(previouse_page);
            }

            if (response.data.page == '1') {
                previouse_page = '<li class="page-item mr-auto disabled"><a class="page-link" href="#"><?php echo getWording('product', 'previous') ?></a></li>';
                $(".pagination").append(previouse_page);
            }

            var page_array = [];

            for (i = 1; i <= response.data.page_max; i++) {
                page_array.push(i);
                if (response.data.page == i) {
                    page_list = '<li class="page-item active"><div class="product_var_type" href="" data-category="" data-subcategory="" data-page="' + i + '"><span class="page-link">' + i + '</span></div></li>';
                    $(".pagination").append(page_list);
                } else {
                    page_list = '<li class="page-item"><div class="product_var_type" href="" data-category="" data-subcategory="" data-page="' + i + '"><span class="page-link">' + i + '</span></div></li>';
                    $(".pagination").append(page_list);
                }
            }

            if (response.data.page == response.data.page_max) {
                next_page = '<li class="page-item ml-auto disabled"><a class="page-link" href="#"><?php echo getWording('product', 'next') ?></a></li>';
                $(".pagination").append(next_page);
            }

            if (response.data.page != response.data.page_max) {
                next_page = '<li class="page-item ml-auto"><a class="product_var_type" href="#" data-category="" data-subcategory="" data-page="' + (parseInt(response.data.page) + 1) + '"><?php echo getWording('product', 'next') ?></a></li>';
                $(".pagination").append(next_page);
            }


            $('.product_var_type').click(function() {
                event.preventDefault();
                // window.history.pushState("", "", $(this).attr('href'));

                // category = $(this).data('category');
                // subcategory = $(this).data('subcategory');
                page = $(this).data('page');


                var data = {
                    "category": category,
                    "sub_category": subcategory,
                    "child_submenu_category": subcategory,
                    "page": page,
                    "type": type
                }

                onClick(data);


            });
        }
        /* Set the width of the side navigation to 250px and the left margin of the page content to 250px and add a black background color to body */
        function openNav() {

            $("#collapse-bg").removeClass("d-none");
            $("#collapse-bg").addClass("d-block");
            document.getElementById("mySidenav").style.width = "400px";
            document.getElementById("main").style.marginLeft = "400px";
            document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
        }

        /* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
        function closeNav() {

            $("#collapse-bg").addClass("d-none");
            $("#collapse-bg").removeClass("d-block");
            document.getElementById("mySidenav").style.width = "0";
            document.getElementById("main").style.marginLeft = "0";
            document.body.style.backgroundColor = "white";
        }

        function openNavPrice() {

            $("#collapse-bg-price").removeClass("d-none");
            $("#collapse-bg-price").addClass("d-block");
            document.getElementById("mySidenavPrice").style.width = "400px";
            document.getElementById("main").style.marginLeft = "400px";
            document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
        }

        /* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
        function closeNavPrice() {

            $("#collapse-bg-price").addClass("d-none");
            $("#collapse-bg-price").removeClass("d-block");
            document.getElementById("mySidenavPrice").style.width = "0";
            document.getElementById("main").style.marginLeft = "0";
            document.body.style.backgroundColor = "white";
        }


        function onClick(data) {

            var send_data = {
                "url": "<?php echo base_url('/Solution/soluton_product_list')  ?>",
                "method": "POST",
                "data": data
            }

            var base_url = 'location.href="' + '<?php echo base_url()  ?>' + '"';

            $.ajax(send_data).done(function(response) {
                console.log(response);
                $("#products").empty();
                $(".pagination").empty();

                var html
                var pagination

                var baht_word = '<?php echo getWording('index', 'baht') ?>';
                var buy_word = '<?php echo getWording('index', 'buy') ?>'
                var interest_word = '<?php echo getWording('index', 'interested_product') ?>'
                var contact_word = '<?php echo getWording('index', 'contact') ?>'
                var no_product = '<?php echo getWording('product', 'no_product') ?>'

                if (response.code == "0x0000-00000") {
                    // console.log(response.data.product_technical_spec);
                    if (response.data.product_technical_spec == 'true') {
                        $.each(response.data.products, function(key, value) {
                            if (value['product::images']) {
                                check_image = value['product::images'][0];
                            }
                            html =
                                '<div class="col-sm-6 col-xl-4 mt-4 item animate__animated animate__fadeInUp" style="text-align:left">' +
                                '<div class="product-item">' +
                                '<a href="' + base_url + '/Product/details/' + value['product::id'] + '"><img class="w-100" src="' + check_image + '" alt="">' +
                                '<h4 class="mt-3">' + value['product::name_th'] + '</h4></a>' +
                                '<span>' + value['product::model_name_th'] + '</span>' +
                                '<h4 class="price">' + value['product::retail_price'] + ' ' + baht_word + '</h4>' +
                                '<div class="row mt-5">' +
                                '<div class="col-8 pr-1">' +
                                '<a href="' + base_url + '/shopping_cart"> <button class="order_btn w-100">' + buy_word + '</button></a>' +
                                '</div>' +
                                '<div class="col-4 pl-1">' +
                                '<button type="button" class="filter_btn w-100 p-0" data-bs-toggle="modal" data-bs-target="#compare_product_modal">' +
                                '<svg class="w-100">' +
                                '<use xlink:href="#icon-filter"></use>' +
                                '</svg>' +
                                '</button>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>'

                            $("#products").append(html);



                            var base_url = '<?php echo base_url() ?>';
                            var check_image = '';

                        });
                        createPagination(response);
                    } else {
                        html = '<h3 class="mt-5 text-center">' + no_product + '</h3>'
                        $("#products").append(html);
                    }
                } else {
                    html = '<h3 class="mt-5 text-center">' + no_product + '</h3>'
                    $("#products").append(html);
                }
            });
        }
    </script>


</body>

</html>