<!-- ------------------------------------------------------------------ -->
<!-- PERMISSION CHECK IS BEGIN                                          -->
<!-- ------------------------------------------------------------------ -->
<?php
$codeigniter_instance =& get_instance();

$configurations = array(
  "PAGE_TITLE" => "Administrator System",
  "PAGE_HEADER" => array(
   "MAIN_TITLE" => "Member Management",
   "SUB_TITLE" => ""
  ),
  "PORTLET_HEADER" => array(
    "ICON" => "fa fa-bars",
    "TITLE" => "Member Detail"
  )
);
?>
<!-- ------------------------------------------------------------------ -->
<!-- HEADER IS BEGIN                                                    -->
<!-- ------------------------------------------------------------------ -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
  <head>
    <title><?php echo $this->config->item('WEBSITE_NAME');?> | Login System</title>
    <link href="<?php echo faviconDirectory($this->config->item('WEBSITE_FAVICON'));?>" rel="shortcut icon" />

    <!-- META TAG AREA -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- MANDATORY STYLE AREA -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/simple-line-icons/simple-line-icons.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-switch/css/bootstrap-switch.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- PLUGINS AREA -->
    <link href="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/clockface/css/clockface.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2-bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/jstree/dist/themes/default/style.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/dropzone/basic.min.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- THEME STYLE AREA -->
    <link href="<?php echo assetsDirectory("global/css/components-md.min.css");?>" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo assetsDirectory("global/css/plugins-md.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- LAYOUT AREA -->
    <link href="<?php echo assetsDirectory("layouts/layout/css/layout.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("layouts/layout/css/themes/light.min.css");?>" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?php echo assetsDirectory("layouts/layout/css/custom.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- CUSTOM AREA -->
    <style>
      .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice {
        color: #fff;
        background: #53c6d2;
        border: 1px solid #53c6d2;
        margin: 5px 0 0 6px;
        padding: 0 35px 0 6px;
      }
      .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice__remove {
        margin-right: 30px;
        color: #e7505a;
      }
      .product-image {
        height: 150px;
        margin-top: 10px;
      }
    </style>
  </head>

  <!-- ------------------------------------------------------------------ -->
  <!-- BODY IS BEGIN                                                      -->
  <!-- ------------------------------------------------------------------ -->
  <body class="page-header-fixed page-sidebar-closed-hide-logo page-md">
    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueHeader",array());?>
    <div class="page-container">
      <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueSideBar",array());?>
      <div class="page-content-wrapper">
        <div class="page-content">
          <h3 class="page-title">
              <?php echo $configurations["PAGE_HEADER"]["MAIN_TITLE"]?>
              <small>
                  <?php echo $configurations["PAGE_HEADER"]["SUB_TITLE"]?>
              </small>
          </h3>
          <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueBreadCrumb",array());?>

          <!-- ------------------------------------------------------------------ -->
          <!-- CONTENT IS BEGIN                                                   -->
          <!-- ------------------------------------------------------------------ -->
          
          <div class="row">
            <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light bordered">
                <div class="portlet-title">
                  <div class="caption font-green">
                    <i class="<?php echo $configurations["PORTLET_HEADER"]["ICON"]?> font-green"></i>
                    <span class="caption-subject bold uppercase">
                      <?php echo $configurations["PORTLET_HEADER"]["TITLE"]?>
                    </span>
                  </div>
                </div>
                <div class="portlet-body">
                  <form class="form-horizontal" role="form" method="post" id="main-form" enctype="multipart/form-data"> 
                    <input type="hidden" name="session_time" value="<?php echo date("YmdHis")?>">
                    <div class="form-group">
                      <label class="col-md-2 control-label">Email :</label>
                      <div class="col-md-3">
                        <p class="form-control-static"><?php echo $member->email ?></p>
                      </div>
                    </div>
                    <div class="form-group" >
                      <label class="col-md-2 control-label">Full name :</label>
                      <div class="col-md-3">
                        <p class="form-control-static">
                          <?php echo $member->fullname ?>
                        </p>
                      </div>
                    </div>
                    <div class="form-group" >
                      <label class="col-md-2 control-label">Gender :</label>
                      <div class="col-md-3">
                        <p class="form-control-static">
                          <?php echo $member->gender ?>
                        </p>
                      </div>
                      <label class="col-md-3 control-label">Birth Date :</label>
                      <div class="col-md-3">
                        <p class="form-control-static">
                          <?php echo isset($member->birth_date) ? date("d/M/Y", strtotime($member->birth_date)) : ''  ?>
                        </p>
                      </div>
                    </div>
                    <div class="form-group" >
                      <label class="col-md-2 control-label">Tax ID :</label>
                      <div class="col-md-3">
                        <p class="form-control-static">
                          <?php echo $member->tax_id ?>
                        </p>
                      </div>
                      <label class="col-md-3 control-label">Telephone :</label>
                      <div class="col-md-3">
                        <p class="form-control-static">
                          <?php echo $member->telephone ?>
                        </p>
                      </div>
                    </div>
                  </form>
                </div>
                <hr>
                <div class="portlet-title">
                  <div class="caption font-green">
                    <i class="fa fa-map-pin font-green"></i>
                    <span class="caption-subject bold uppercase">
                      Address
                    </span>
                  </div>
                </div>
                <div class="portlet-body">
                  <div class="form-horizontal">
                    <?php if($member_addresses != null && count($member_addresses) > 0) {
                      foreach ($member_addresses as $address) {
                    ?>
                        <div class="row" style="margin: 10px;border: 1px solid #eef1f5;">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-md-2 control-label">First Name:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo $address->{"member_address::first_name"} ?>
                                </p>
                              </div>
                              <label class="col-md-3 control-label">Last Name:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo $address->{"member_address::last_name"} ?>
                                </p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-2 control-label">telephone:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo $address->{"member_address::telephone"} ?>
                                </p>
                              </div>
                              <label class="col-md-3 control-label">Email:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo $address->{"member_address::email"} ?>
                                </p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-2 control-label">Company Name:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo $address->{"member_address::company_name"} ?>
                                </p>
                              </div>
                              <label class="col-md-3 control-label">Company Branch:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo $address->{"member_address::company_branch"} ?>
                                </p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-2 control-label">Address:</label>
                              <div class="col-md-9">
                                <p class="form-control-static">
                                  <?php echo $address->{"member_address::address"} ?>
                                </p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-2 control-label">Province:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo isset($address->{"address_province::name"}) ? $address->{"address_province::name"} : "" ?>
                                </p>
                              </div>
                              <label class="col-md-3 control-label">District:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo isset($address->{"address_district::name"}) ? $address->{"address_district::name"} : "" ?>
                                </p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-2 control-label">Sub District:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo isset($address->{"address_sub_district::name"}) ? $address->{"address_sub_district::name"} : "" ?>
                                </p>
                              </div>
                              <label class="col-md-3 control-label">Postcode:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo isset($address->{"member_address::postcode"}) ? $address->{"member_address::postcode"} : "" ?>
                                </p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-2 control-label">Is Default:</label>
                              <div class="col-md-3">
                                <div class="mt-checkbox-inline">
                                  <label class="mt-checkbox mt-checkbox-outline mt-checkbox-disabled">
                                    <input type="checkbox" name="is_default" disabled <?php if($address->{"member_address::is_default"} == 1) echo "checked";?> >
                                    <span></span>
                                  </label>
                                </div>
                              </div>
                            </div>


                          </div>
                        </div>
                    <?php
                      }
                    }
                    else{
                    ?>
                      <div class="form-group" >
                        <label class="col-md-2 control-label">No data</label>
                      </div>
                    <?php
                    }?>
                  </div>
                </div>
                <hr>
                <div class="portlet-title">
                  <div class="caption font-green">
                    <i class="fa fa-map-pin font-green"></i>
                    <span class="caption-subject bold uppercase">
                      Address Tax
                    </span>
                  </div>
                </div>
                <div class="portlet-body">
                  <div class="form-horizontal">
                    <?php if($member_address_taxs != null && count($member_address_taxs) > 0) {
                      foreach ($member_address_taxs as $address) {
                    ?>
                        <div class="row" style="margin: 10px;border: 1px solid #eef1f5;">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-md-2 control-label">First Name:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo $address->{"member_address_tax::first_name"} ?>
                                </p>
                              </div>
                              <label class="col-md-3 control-label">Last Name:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo $address->{"member_address_tax::last_name"} ?>
                                </p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-2 control-label">telephone:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo $address->{"member_address_tax::telephone"} ?>
                                </p>
                              </div>
                              <label class="col-md-3 control-label">Email:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo $address->{"member_address_tax::email"} ?>
                                </p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-2 control-label">Company Name:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo $address->{"member_address_tax::company_name"} ?>
                                </p>
                              </div>
                              <label class="col-md-3 control-label">Company Branch:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo $address->{"member_address_tax::company_branch"} ?>
                                </p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-2 control-label">Address:</label>
                              <div class="col-md-9">
                                <p class="form-control-static">
                                  <?php echo $address->{"member_address_tax::address"} ?>
                                </p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-2 control-label">Province:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo isset($address->{"address_province::name"}) ? $address->{"address_province::name"} : "" ?>
                                </p>
                              </div>
                              <label class="col-md-3 control-label">District:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo isset($address->{"address_district::name"}) ? $address->{"address_district::name"} : "" ?>
                                </p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-2 control-label">Sub District:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo isset($address->{"address_sub_district::name"}) ? $address->{"address_sub_district::name"} : "" ?>
                                </p>
                              </div>
                              <label class="col-md-3 control-label">Postcode:</label>
                              <div class="col-md-3">
                                <p class="form-control-static">
                                  <?php echo isset($address->{"member_address_tax::postcode"}) ? $address->{"member_address_tax::postcode"} : "" ?>
                                </p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-2 control-label">Is Default:</label>
                              <div class="col-md-3">
                                <div class="mt-checkbox-inline">
                                  <label class="mt-checkbox mt-checkbox-outline mt-checkbox-disabled">
                                    <input type="checkbox" name="is_default" disabled <?php if($address->{"member_address_tax::is_default"} == 1) echo "checked";?> >
                                    <span></span>
                                  </label>
                                </div>
                              </div>
                            </div>


                          </div>
                        </div>
                    <?php
                      }
                    }
                    else{
                    ?>
                      <div class="form-group" >
                        <label class="col-md-2 control-label">No data</label>
                      </div>
                    <?php
                    }?>
                  </div>
                </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueFooter",array());?>
  </body>

  <!-- ------------------------------------------------------------------ -->
  <!-- JAVASCRIPT IS BEGIN                                                -->
  <!-- ------------------------------------------------------------------ -->
  <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/respond.min.js');?>"></script> <![endif]-->
  <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/excanvas.min.js');?>"></script> <![endif]-->

  <!-- MANDATORY SCRIPT AREA -->
  <script src="<?php echo assetsDirectory("global/plugins/jquery.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap/js/bootstrap.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/js.cookie.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-slimscroll/jquery.slimscroll.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery.blockui.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-switch/js/bootstrap-switch.min.js");?>" type="text/javascript"></script>

  <!-- PLUGINS AREA -->
  <script src="<?php echo assetsDirectory("global/plugins/jquery-ui/jquery-ui.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/moment.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/clockface/js/clockface.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/select2/js/select2.full.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/jquery.validate.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/additional-methods.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.js");?>"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote-cleaner.js");?>"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jstree/dist/jstree.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/scripts/datatable.js");?>" type="text/javascript"></script>

  <script src="<?php echo assetsDirectory("global/plugins/backstretch/jquery.backstretch.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.js");?>" type="text/javascript"></script>
    

  <!-- THEME SCRIPT AREA -->
  <script src="<?php echo assetsDirectory("global/scripts/app.min.js");?>" type="text/javascript"></script>

  <!-- LAYOUT AREA -->
  <script src="<?php echo assetsDirectory("layouts/layout/scripts/layout.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("layouts/layout/scripts/demo.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("layouts/global/scripts/quick-sidebar.js");?>" type="text/javascript"></script>

  <!-- CUSTOM AREA -->
  <script>
    jQuery(document).ready(function() {
      
      //Price by Quantity
      $(".product-price-list").dataTable({
        processing: true,
        serverSide: true,
        pageLength: 25,
        searching: false,
        ajax:{
          url: "<?php echo base_url("administrator_area/product/datatables/index_product_price")?>",
          type: "POST",
          data:{
            product_id: $(".product-price-list").data("product_id")
          }
        },
        columns: [
          {
            data: 'minimum_qty'
          },
          {
            data: 'price'
          }
        ]
      });

      $(".menu-category-list").dataTable({
          processing: true,
          serverSide: true,
          pageLength: 25,
          searching: false,
          ajax:{
            url: "<?php echo base_url("administrator_area/product/datatables/index_menu_category")?>",
            type: "POST",
            data:{
              product_id: $(".menu-category-list").data("product_id")
            }
          },
          columns: [
            {
              data: 'menu_category_name'
            },
            {
              data: 'submenu_category_name'
            },
            {
              data: 'child_submenu_category_name'
            }
          ]
      });

      //Category
      $(".product-category-list").dataTable({
          processing: true,
          serverSide: true,
          pageLength: 25,
          searching: false,
          ajax:{
            url: "<?php echo base_url("administrator_area/product/datatables/index_product_category")?>",
            type: "POST",
            data:{
              product_id: $(".product-category-list").data("product_id")
            }
          },
          columns: [
            {
              data: 'product_category_name'
            },
            {
              data: 'product_sub_category_name'
            },
            {
              data: 'product_childsub_category_name'
            }
          ]
      });

    });
  </script>

  <!-- INCLUDE RAW SCRIPT AREA -->
  <?php $this->load->view("administrator_area/__scripts/Javascript_Modal",array());?>
  <?php $this->load->view("administrator_area/__scripts/Javascript_Select2",array());?>
</html>

<!-- ------------------------------------------------------------------ -->
<!-- MODAL IS BEGIN                                                     -->
<!-- ------------------------------------------------------------------ -->
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationCancel");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationDelete");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationRefresh");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationSave");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationSaveMultiform");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_PermissionDenied");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemDisconnected");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnError");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnFailed");?>
