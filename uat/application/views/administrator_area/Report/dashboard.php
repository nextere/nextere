<!-- ------------------------------------------------------------------ -->
<!-- PERMISSION CHECK IS BEGIN                                          -->
<!-- ------------------------------------------------------------------ -->
<?php
$codeigniter_instance =& get_instance();
 
$configurations = array(
    "PAGE_TITLE" => "Administrator System",
    "PAGE_HEADER" => array(
        "MAIN_TITLE" => "Report dashboard",
        "SUB_TITLE" => ""
    ),
    "PORTLET_HEADER" => array(
        "ICON" => "fa fa-bars",
        "TITLE" => "Report dashboard List"
    )
); 
?>
<!-- ------------------------------------------------------------------ -->
<!-- HEADER IS BEGIN                                                    -->
<!-- ------------------------------------------------------------------ -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <title><?php echo $this->config->item('WEBSITE_NAME');?> | Login System</title>
        <link href="<?php echo faviconDirectory($this->config->item('WEBSITE_FAVICON'));?>" rel="shortcut icon" />

        <!-- META TAG AREA -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

        <!-- MANDATORY STYLE AREA -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/simple-line-icons/simple-line-icons.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-switch/css/bootstrap-switch.min.css");?>" rel="stylesheet" type="text/css" />

        <!-- PLUGINS AREA -->
        <link href="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/clockface/css/clockface.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2-bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.css");?>" rel="stylesheet">
        <link href="<?php echo assetsDirectory("global/plugins/jstree/dist/themes/default/style.css");?>" rel="stylesheet">
        <link href="<?php echo assetsDirectory("global/plugins/dropzone/basic.min.css");?>" rel="stylesheet">
        <link href="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.css");?>" rel="stylesheet">

        <!-- THEME STYLE AREA -->
        <link href="<?php echo assetsDirectory("global/css/components-md.min.css");?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo assetsDirectory("global/css/plugins-md.min.css");?>" rel="stylesheet" type="text/css" />

        <!-- LAYOUT AREA -->
        <link href="<?php echo assetsDirectory("layouts/layout/css/layout.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("layouts/layout/css/themes/light.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("layouts/layout/css/custom.min.css");?>" rel="stylesheet" type="text/css" />

        <!-- CUSTOM AREA -->
        <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    </head>

    <!-- ------------------------------------------------------------------ -->
    <!-- BODY IS BEGIN                                                      -->
    <!-- ------------------------------------------------------------------ -->
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-md">
        <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueHeader",array());?>
        <div class="page-container">
            <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueSideBar",array());?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <h3 class="page-title">
                        <?php echo $configurations["PAGE_HEADER"]["MAIN_TITLE"]?>
                        <small>
                            <?php echo $configurations["PAGE_HEADER"]["SUB_TITLE"]?>
                        </small>
                    </h3>
                    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueBreadCrumb",array());?>

                    <!-- ------------------------------------------------------------------ -->
                    <!-- CONTENT IS BEGIN                                                   -->
                    <!-- ------------------------------------------------------------------ -->
                    <?php
                    if(checkAdministratorPermission("REPORT", "index")){
                        ?>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-green">
                                            <i class="<?php echo $configurations["PORTLET_HEADER"]["ICON"]?> font-green"></i>
                                            <span class="caption-subject bold uppercase"><?php echo $configurations["PORTLET_HEADER"]["TITLE"]?></span>
                                        </div>
                                    </div>
                                    <div class="portlet-body"> 
                                    <div class="row">
                                            <div class="col-xl-3 col-md-3">
                                            <div class="card card-stats">
                                                <!-- Card body -->
                                                <div class="card-body">
                                                <div class="row">
                                                    <div class="col" style="padding: 10px;">
                                                    <h5 class="card-title text-uppercase text-muted mb-0">Member / คน</h5>
                                                    <span class="h2 font-weight-bold mb-0"><?php echo number_format($sum_member->count) ?></span>
                                                    </div>
                                                    <div class="col-auto">
                                                    <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                                        <i class="ni ni-active-40"></i>
                                                    </div>
                                                    </div>
                                                </div>
                                                <!-- <p class="mt-3 mb-0 text-sm">
                                                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> </span>
                                                    <span class="text-nowrap"></span>
                                                </p> -->
                                                </div>
                                            </div>
                                            </div>
                                            <div class="col-xl-3 col-md-3">
                                            <div class="card card-stats">
                                                <!-- Card body -->
                                                <div class="card-body">
                                                <div class="row">
                                                    <div class="col " style="padding: 10px;">
                                                    <h5 class="card-title text-uppercase text-muted mb-0">Brand / ชนิด</h5>
                                                    <span class="h2 font-weight-bold mb-0"><?php echo number_format($sum_brand->count) ?></span>
                                                    </div>
                                                    <div class="col-auto">
                                                    <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                                        <i class="ni ni-chart-pie-35"></i>
                                                    </div>
                                                    </div>
                                                </div>
                                                <!-- <p class="mt-3 mb-0 text-sm">
                                                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> </span>
                                                    <span class="text-nowrap"></span>
                                                </p> -->
                                                </div>
                                            </div>
                                            </div>
                                            <div class="col-xl-3 col-md-3">
                                            <div class="card card-stats">
                                                <!-- Card body -->
                                                <div class="card-body">
                                                <div class="row">
                                                    <div class="col" style="padding: 10px;">
                                                    <h5 class="card-title text-uppercase text-muted mb-0">Product / ชิ้น</h5>
                                                    <span class="h2 font-weight-bold mb-0"><?php echo number_format($sum_product->count) ?></span>
                                                    </div>
                                                    <div class="col-auto">
                                                    <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                                        <i class="ni ni-chart-pie-35"></i>
                                                    </div>
                                                    </div>
                                                </div>
                                                <!-- <p class="mt-3 mb-0 text-sm">
                                                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> </span>
                                                    <span class="text-nowrap"></span>
                                                </p> -->
                                                </div>
                                            </div>
                                            </div>
                                            <div class="col-xl-3 col-md-3">
                                            <div class="card card-stats">
                                                <!-- Card body -->
                                                <div class="card-body">
                                                <div class="row">
                                                    <div class="col" style="padding: 10px;">
                                                    <h5 class="card-title text-uppercase text-muted mb-0">Order / ครั้ง</h5>
                                                    <span class="h2 font-weight-bold mb-0"><?php echo number_format($sum_purchase_transaction->count) ?></span>
                                                    </div>
                                                    <div class="col-auto">
                                                    <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                                        <i class="ni ni-money-coins"></i>
                                                    </div>
                                                    </div>
                                                </div>
                                                <!-- <p class="mt-3 mb-0 text-sm">
                                                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> </span>
                                                    <span class="text-nowrap"></span>
                                                </p> -->
                                                </div>
                                            </div>
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="col-md-12"><br><br>
                                            
                                            <div id="chart" style="height: 400px; width: 100%;"></div>
                                        </div>
                                        <h3>Latest orders</h3>
                                        <table class="table table-striped table-bordered table-hover order-column"  id="product_order">
                                            <thead>
                                            </thead>
                                            <tbody>
                                            <?php foreach($purchases as $key => $purchase){ ?>
                                                        <tr class="">
                                                        <th ><?php echo $purchase->{'purchase_transaction::purchase_transaction_no'} ?> </th>
                                                            <th ><?php echo $purchase->{'member::fullname'} ?> </th>
                                                            <th ><?php echo $purchase->{'member::email'} ?> </th>
                                                            <th > <?php echo  number_format($purchase->{'purchase_transaction::net_price'},2) ?> </th>
                                                            <?php if( $purchase->{'purchase_transaction::status'} == 'PAID'){ ?><th style="color:#2ECC71;"> <?php echo  $purchase->{'purchase_transaction::status'} ?> </th> 
                                                            <?php } 
                                                            else if($purchase->{'purchase_transaction::status'} == 'RESERVE'){?> <th style="color:#2980B9;"> <?php echo  $purchase->{'purchase_transaction::status'} ?> </th>
                                                            <?php } 
                                                            else if($purchase->{'purchase_transaction::status'} == 'CANCEL'){ ?> <th style="color:#E74C3C;"> <?php echo  $purchase->{'purchase_transaction::status'} ?> </th>
                                                            <?php } 
                                                            else{ ?><th style="color:#F1C40F;"> <?php echo  $purchase->{'purchase_transaction::status'} ?> </th>
                                                            <?php } ?>
                                                            
                                                            <th > <?php echo  $purchase->{'purchase_transaction::created_date'} ?> </th>
                                                            <th > <?php echo  $purchase->{'purchase_transaction::courier_name'} ?> </th>
                                                            
                                                            
                                                        </tr>
                                                
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                        <?php
                    }else{
                        ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div style="text-align: center;">
                                    <div class="row" style="padding-top: 50px;">
                                        <div class="page-spinner-bar-custom">
                                            <div class="bounce1"></div>
                                            <div class="bounce2"></div>
                                            <div class="bounce3"></div>
                                            <div class="bounce4"></div>
                                            <div class="bounce5"></div>
                                        </div>
                                    </div>
                                    <div class="caption font-red-thunderbird row" style="padding-top: 30px;padding-bottom: 50px;">
                                        <i class="fa fa-unlock-alt font-red-thunderbird"></i><br>
                                        <span class="caption-subject bold uppercase"> Permission Denied<br><small>You are not allow to use this function.</small></span><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>

        <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueFooter",array());?>
    </body>

    <!-- ------------------------------------------------------------------ -->
    <!-- JAVASCRIPT IS BEGIN                                                -->
    <!-- ------------------------------------------------------------------ -->
    <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/respond.min.js');?>"></script> <![endif]-->
    <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/excanvas.min.js');?>"></script> <![endif]-->

    <!-- MANDATORY SCRIPT AREA -->
    <script src="<?php echo assetsDirectory("global/plugins/jquery.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap/js/bootstrap.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/js.cookie.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery-slimscroll/jquery.slimscroll.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery.blockui.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-switch/js/bootstrap-switch.min.js");?>" type="text/javascript"></script>

    <!-- PLUGINS AREA -->
    <script src="<?php echo assetsDirectory("global/plugins/jquery-ui/jquery-ui.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/moment.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/clockface/js/clockface.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/select2/js/select2.full.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/jquery.validate.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/additional-methods.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.js");?>"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote-cleaner.js");?>"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jstree/dist/jstree.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/scripts/datatable.js");?>" type="text/javascript"></script>

    <script src="<?php echo assetsDirectory("global/plugins/backstretch/jquery.backstretch.min.js");?>" type="text/javascript"></script>

    <!-- THEME SCRIPT AREA -->
    <script src="<?php echo assetsDirectory("global/scripts/app.min.js");?>" type="text/javascript"></script>

    <!-- LAYOUT AREA -->
    <script src="<?php echo assetsDirectory("layouts/layout/scripts/layout.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("layouts/layout/scripts/demo.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("layouts/global/scripts/quick-sidebar.js");?>" type="text/javascript"></script>

    <!-- CUSTOM AREA -->
    <script>
        var result = <?= json_encode($chart) != "" ? json_encode($chart) : [] ?>;
        var price = [];
        for(var i = 0; i <= 12; i++)
        {
            for(var j = 0; j < result.length; j++){

                if(parseInt(result[j].month) == i){
                    price[i] = parseInt(result[j].price);
                    break;
                }else{
                    price[i] = 0;
                }
            }

        }
        price.splice(0,1);
        console.log(result);
        console.log(price);
        var options = {
            chart: {
                height: 350,
                type: "bar",
                stacked: false
            },
            dataLabels: {
                enabled: false
            },
            colors: ["#247BA0"],
            series: [
                {
                name: "sales",
                data: price
                }
            ],
            plotOptions: {
                bar: {
                columnWidth: "20%"
                }
            },
            xaxis: {
                categories: ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august','september','october','november','december']
            },
            tooltip: {
                shared: false,
                intersect: true,
                x: {
                show: false
                }
            },
            title: {
                text: 'Sales Statistics (<?php echo date('Y'); ?>) ',
                align: 'center'
            },
            };

var chart = new ApexCharts(document.querySelector("#chart"), options);
chart.render();
    </script>
    <script>
        jQuery(document).ready(function() {
            <?php
            if(checkAdministratorPermission("REPORT", "index")){
                ?>
                $(".date-picker").datepicker({
                        todayHighlight: true,
                        autoclose: true,
                        format: "dd MM yyyy",
                        altField  : ".date-picker",
                        altFormat: "yy-mm-dd"
                    });
                
                
                <?php
            }else{
                ?>
                $('#permission-denied').modal('show');
                <?php
            }
            ?>
        });
    </script>

    <!-- INCLUDE RAW SCRIPT AREA -->
    <?php $this->load->view("administrator_area/__scripts/Javascript_Datatables",array());?>
    <?php $this->load->view("administrator_area/__scripts/Javascript_Modal",array());?>
    <?php $this->load->view("administrator_area/__scripts/Javascript_Select2",array());?>
</html>

<!-- ------------------------------------------------------------------ -->
<!-- MODAL IS BEGIN                                                     -->
<!-- ------------------------------------------------------------------ -->
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationDelete");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_PermissionDenied");?>