<!-- ------------------------------------------------------------------ -->
<!-- PERMISSION CHECK IS BEGIN                                          -->
<!-- ------------------------------------------------------------------ -->
<?php
$codeigniter_instance =& get_instance();

$configurations = array(
    "PAGE_TITLE" => "Administrator System",
    "PAGE_HEADER" => array(
        "MAIN_TITLE" => "Report sale of product",
        "SUB_TITLE" => ""
    ),
    "PORTLET_HEADER" => array(
        "ICON" => "fa fa-bars",
        "TITLE" => "Report sale of product List"
    )
); 
?>
<!-- ------------------------------------------------------------------ -->
<!-- HEADER IS BEGIN                                                    -->
<!-- ------------------------------------------------------------------ -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <title><?php echo $this->config->item('WEBSITE_NAME');?> | Login System</title>
        <link href="<?php echo faviconDirectory($this->config->item('WEBSITE_FAVICON'));?>" rel="shortcut icon" />

        <!-- META TAG AREA -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

        <!-- MANDATORY STYLE AREA -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/simple-line-icons/simple-line-icons.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-switch/css/bootstrap-switch.min.css");?>" rel="stylesheet" type="text/css" />

        <!-- PLUGINS AREA -->
        <link href="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/clockface/css/clockface.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2-bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.css");?>" rel="stylesheet">
        <link href="<?php echo assetsDirectory("global/plugins/jstree/dist/themes/default/style.css");?>" rel="stylesheet">
        <link href="<?php echo assetsDirectory("global/plugins/dropzone/basic.min.css");?>" rel="stylesheet">
        <link href="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.css");?>" rel="stylesheet">

        <!-- THEME STYLE AREA -->
        <link href="<?php echo assetsDirectory("global/css/components-md.min.css");?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo assetsDirectory("global/css/plugins-md.min.css");?>" rel="stylesheet" type="text/css" />

        <!-- LAYOUT AREA -->
        <link href="<?php echo assetsDirectory("layouts/layout/css/layout.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("layouts/layout/css/themes/light.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("layouts/layout/css/custom.min.css");?>" rel="stylesheet" type="text/css" />

        <!-- CUSTOM AREA -->
    </head>

    <!-- ------------------------------------------------------------------ -->
    <!-- BODY IS BEGIN                                                      -->
    <!-- ------------------------------------------------------------------ -->
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-md">
        <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueHeader",array());?>
        <div class="page-container">
            <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueSideBar",array());?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <h3 class="page-title">
                        <?php echo $configurations["PAGE_HEADER"]["MAIN_TITLE"]?>
                        <small>
                            <?php echo $configurations["PAGE_HEADER"]["SUB_TITLE"]?>
                        </small>
                    </h3>
                    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueBreadCrumb",array());?>

                    <!-- ------------------------------------------------------------------ -->
                    <!-- CONTENT IS BEGIN                                                   -->
                    <!-- ------------------------------------------------------------------ -->
                    <?php
                    if(checkAdministratorPermission("REPORT", "index")){
                        ?>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-green">
                                            <i class="<?php echo $configurations["PORTLET_HEADER"]["ICON"]?> font-green"></i>
                                            <span class="caption-subject bold uppercase"><?php echo $configurations["PORTLET_HEADER"]["TITLE"]?></span>
                                        </div>
                                        <div class="actions">
                                        <div class="col-md-3"></div>
                                        <form class="col-md-4" target="_blank" method="POST" action="<?php echo base_url("administrator_area/report/export_product_sales");  ?>">
                                            <button id="exportExcel" class="btn blue-thunderbird"> <i class="fa fa-file-excel-o"></i> EXCEL </button>
                                            <textarea id="htmlExcel" name="htmlExcel" style="display: none;"></textarea>
                                        </form>
                                    </div>
                                    </div>
                                    <div class="portlet-body" ><br><br>
                                    <form class="form-horizontal" role="form" method="post" id="main-form">
                                    <div class="form-group">
                                    <label class="col-md-1 col-sm-12 control-label">Product</label>
                                    <div class="col-md-4">
                                        <select class="form-control select2" name="product_id">
                                        <?php
                                        if(isset($product->id)){
                                        ?>
                                            <option value="<?php echo $product->id ?>" selected><?php echo $product->name_en ?></option>
                                        <?php
                                        }
                                        ?>
                                        </select>
                                    </div>
                                    </div>
                                        <div class="form-group" style="margin-bottom: 0">
                                            <label class="col-md-1 col-sm-12 control-label"> Date</label>
                                            <div class="col-md-2 col-sm-12">
                                                <div class="input-group input-medium date date-picker" data-date-format="D-M-yyyy">
                                                    <input type="text" name="transaction_date" readonly class="form-control" value="<?php if(isset($search['transaction_date'])) echo $search['transaction_date']; ?>"> 
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                                <span class="help-block"> Select date start</span>
                                            </div>
                                            <label class="col-md-2 col-sm-12 control-label">To</label>
                                            <div class="col-md-2 col-sm-12">
                                                <div class="input-group input-medium date date-picker" data-date-format="D-M-yyyy">
                                                    <input type="text" name="transaction_date_end" readonly class="form-control"  value="<?php if(isset($search['transaction_date'])) echo $search['transaction_date_end']; ?>">
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                                <span class="help-block"> Select date stop</span>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <div class="col-md-offset-1 col-md-2 col-sm-12">
                                                <button type="submit" class="btn btn-block green"> Build Changes </button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="portlet-body export" >
                                    <table class="table table-striped table-bordered table-hover order-column" >
                                    <thead>
                                        <tr class="success">
                                            <th style="width: 20%; font-size: 14px;text-align: center;"> product name </th>
                                            <th style="width: 10%; font-size: 14px;text-align: center;"> qty </th>
                                            <th style="width: 10%; font-size: 14px;text-align: center;"> total price </th>
                                            <!-- <th style="width: 15%; font-size: 14px;text-align: center;"> courier name </th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(isset($results)){foreach($results as $result){ ?>
                                        <tr class="">
                                            <th style="text-align: inherit;"><?php echo $result->name_en ?> </th>
                                            <th style="text-align: center;"> <?php echo $result->qty ?> </th>
                                            <th style="text-align: center;"> <?php echo number_format($result->total_price,2) ?> </th>
                                            <!-- <th style="text-align: center;"> <?php echo $result->courier_name ?> </th> -->
                                        </tr>
                                        <?php } }?>
                                    </tbody>
                                    <tfoot>
                                        <tr class="success">
                                        <th style="text-align: center;"  id="total" colspan="2"><b>Total</b></th><br><br>
                                        <th style="text-align: center;" ><?php if(isset($sum))echo   number_format($sum->total_price,2); ?></th>
                                        <!-- <th style="text-align: center;" ><?php echo '';?></th> -->
                                        </tr>
                                    </tfoot>
                                        </table>
                                    </div>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                        <?php
                    }else{
                        ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div style="text-align: center;">
                                    <div class="row" style="padding-top: 50px;">
                                        <div class="page-spinner-bar-custom">
                                            <div class="bounce1"></div>
                                            <div class="bounce2"></div>
                                            <div class="bounce3"></div>
                                            <div class="bounce4"></div>
                                            <div class="bounce5"></div>
                                        </div>
                                    </div>
                                    <div class="caption font-red-thunderbird row" style="padding-top: 30px;padding-bottom: 50px;">
                                        <i class="fa fa-unlock-alt font-red-thunderbird"></i><br>
                                        <span class="caption-subject bold uppercase"> Permission Denied<br><small>You are not allow to use this function.</small></span><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>

        <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueFooter",array());?>
    </body>

    <!-- ------------------------------------------------------------------ -->
    <!-- JAVASCRIPT IS BEGIN                                                -->
    <!-- ------------------------------------------------------------------ -->
    <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/respond.min.js');?>"></script> <![endif]-->
    <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/excanvas.min.js');?>"></script> <![endif]-->

    <!-- MANDATORY SCRIPT AREA -->
    <script src="<?php echo assetsDirectory("global/plugins/jquery.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap/js/bootstrap.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/js.cookie.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery-slimscroll/jquery.slimscroll.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery.blockui.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-switch/js/bootstrap-switch.min.js");?>" type="text/javascript"></script>

    <!-- PLUGINS AREA -->
    <script src="<?php echo assetsDirectory("global/plugins/jquery-ui/jquery-ui.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/moment.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/clockface/js/clockface.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/select2/js/select2.full.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/jquery.validate.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/additional-methods.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.js");?>"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote-cleaner.js");?>"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jstree/dist/jstree.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/scripts/datatable.js");?>" type="text/javascript"></script>

    <script src="<?php echo assetsDirectory("global/plugins/backstretch/jquery.backstretch.min.js");?>" type="text/javascript"></script>

    <!-- THEME SCRIPT AREA -->
    <script src="<?php echo assetsDirectory("global/scripts/app.min.js");?>" type="text/javascript"></script>

    <!-- LAYOUT AREA -->
    <script src="<?php echo assetsDirectory("layouts/layout/scripts/layout.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("layouts/layout/scripts/demo.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("layouts/global/scripts/quick-sidebar.js");?>" type="text/javascript"></script>

    <!-- CUSTOM AREA -->
    <script>
        jQuery(document).ready(function() {
            <?php
            if(checkAdministratorPermission("REPORT", "index")){
                ?>
                var html = $('.export').html();
                $("#htmlExcel").text("");
                $("#htmlExcel").text(html);
                $(".date-picker").datepicker({
                        todayHighlight: true,
                        autoclose: true,
                        format: "dd MM yyyy",
                        altField  : ".date-picker",
                        altFormat: "yy-mm-dd"
                    });
                /** =================================================================== **/
                /** SELECT 2                                                            **/
                /** =================================================================== **/
                $("#main-form select[name='product_id']").select2({
                    placeholder: "Select...", width: null, allowClear: true,
                    ajax: {
                    url: "<?php echo base_url("administrator_area/report/select2/edit_product")?>",
                    dataType: 'json', delay: 250, cache: true,
                    data: function(params) {
                        return {
                        search: params.term
                        };
                    },
                    processResults: function(data) {
                        return {
                        results: $.map(data.data, function(obj) {
                            return { id: obj.id, text: obj.name_en };
                        })
                        };
                    }
                    }
                });
                /** =================================================================== **/
                /** DATA TABLES CONTROL                                                 **/
                /** =================================================================== **/
                // $("#main-table").dataTable({
                //     processing: true,
                //     serverSide: true,
                //     pageLength: 25,
                //     ajax:{
                //         url: "<?php echo base_url("administrator_area/banner/datatables/index")?>",
                //         type: "POST"
                //     },
                //     columns: [
                //         {
                //             data: 'title_th'
                //         },
                //         {
                //             data: 'title_en'
                //         },
                //         {
                //             "orderable": false,
                //             "className": "dt-center",
                //             render: function(data, type, row, meta){
                //                 var action_header =     '<img  style="width: 40%; height: auto"  src="<?php echo base_url() ?>' + '/' + row["banner_path"]+'">';
                //                 return action_header;
                //             }  
                //         },
                //         {
                //             render: function(data, type, row, meta){
                //                 var action_header =     '<div class="dropdown">'+
                //                     '<button class="btn btn-block green btn-xs dropdown-toggle" type="button" data-toggle="dropdown">Action Menu '+
                //                     '<span class="caret"></span></button>';
                //                     action_header += '<ul class="dropdown-menu dropdown-menu-right" style="left: auto;">';
                //                     action_header +=    '<li><a href="<?php echo base_url("administrator_area/banner/detail");?>/'+ row["id"] +'"><i class="fa fa-wrench"></i> Detail</a></li>';
                //                     <?php
                //                     if(checkAdministratorPermission("BANNER", "edit")){
                //                     ?>
                //                         action_header +=    '<li><a href="<?php echo base_url("administrator_area/banner/edit");?>/'+ row["id"] +'"><i class="fa fa-wrench"></i> Edit record</a></li>';
                //                     <?php
                //                     }else{
                //                     ?>
                //                         action_header +=    '<li><a onclick="$(\'#permission-denied\').modal(\'toggle\');"><i class="fa fa-wrench"></i> Edit record</a></li>';
                //                     <?php
                //                     }

                //                     if(checkAdministratorPermission("BANNER", "delete")){
                //                     ?>
                //                         action_header +=    '<li><a onclick="$(\'#confirmation-delete\').modal(\'toggle\');$(\'#confirmation-delete #delete-url\').val(\'<?php echo base_url("administrator_area/banner/delete");?>/'+row["id"]+'\');"><i class="fa fa-trash"></i> Delete record </a></li>';
                //                     <?php
                //                     }else{
                //                     ?>
                //                         action_header +=    '<li><a onclick="$(\'#permission-denied\').modal(\'toggle\');"><i class="fa fa-trash"></i> Delete record </a></li>';
                //                     <?php
                //                     }
                //                     ?>
                //                     action_header += '</ul>'+
                //                     '</div>';
                //                 return action_header;
                //             },
                //             searchable: false,
                //             orderable: false
                //         }
                //     ]
                // });
                <?php
            }else{
                ?>
                $('#permission-denied').modal('show');
                <?php
            }
            ?>
        });
    </script>

    <!-- INCLUDE RAW SCRIPT AREA -->
    <?php $this->load->view("administrator_area/__scripts/Javascript_Datatables",array());?>
    <?php $this->load->view("administrator_area/__scripts/Javascript_Modal",array());?>
    <?php $this->load->view("administrator_area/__scripts/Javascript_Select2",array());?>
</html>

<!-- ------------------------------------------------------------------ -->
<!-- MODAL IS BEGIN                                                     -->
<!-- ------------------------------------------------------------------ -->
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationDelete");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_PermissionDenied");?>