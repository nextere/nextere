<!-- ------------------------------------------------------------------ -->
<!-- PERMISSION CHECK IS BEGIN                                          -->
<!-- ------------------------------------------------------------------ -->
<?php
$codeigniter_instance =& get_instance();

$configurations = array(
  "PAGE_TITLE" => "Administrator System",
  "PAGE_HEADER" => array(
   "MAIN_TITLE" => "Promotion Discount",
   "SUB_TITLE" => ""
  ), 
  "PORTLET_HEADER" => array(
    "ICON" => "fa fa-bars",
    "TITLE" => "Promotion Discount Detail"
  )
);
?>
<!-- ------------------------------------------------------------------ -->
<!-- HEADER IS BEGIN                                                    -->
<!-- ------------------------------------------------------------------ -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
  <head>
    <title><?php echo $this->config->item('WEBSITE_NAME');?> | Login System</title>
    <link href="<?php echo faviconDirectory($this->config->item('WEBSITE_FAVICON'));?>" rel="shortcut icon" />

    <!-- META TAG AREA -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- MANDATORY STYLE AREA -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/simple-line-icons/simple-line-icons.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-switch/css/bootstrap-switch.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- PLUGINS AREA -->
    <link href="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/clockface/css/clockface.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2-bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/jstree/dist/themes/default/style.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/dropzone/basic.min.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- THEME STYLE AREA -->
    <link href="<?php echo assetsDirectory("global/css/components-md.min.css");?>" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo assetsDirectory("global/css/plugins-md.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- LAYOUT AREA -->
    <link href="<?php echo assetsDirectory("layouts/layout/css/layout.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("layouts/layout/css/themes/light.min.css");?>" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?php echo assetsDirectory("layouts/layout/css/custom.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- CUSTOM AREA -->
    <style>
      .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice {
        color: #fff;
        background: #53c6d2;
        border: 1px solid #53c6d2;
        margin: 5px 0 0 6px;
        padding: 0 35px 0 6px;
      }
      .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice__remove {
        margin-right: 30px;
        color: #e7505a;
      }
      .product-image {
        height: 150px;
        margin-top: 10px;
      }
    </style>
  </head>

  <!-- ------------------------------------------------------------------ -->
  <!-- BODY IS BEGIN                                                      -->
  <!-- ------------------------------------------------------------------ -->
  <body class="page-header-fixed page-sidebar-closed-hide-logo page-md">
    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueHeader",array());?>
    <div class="page-container">
      <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueSideBar",array());?>
      <div class="page-content-wrapper">
        <div class="page-content">
          <h3 class="page-title">
              <?php echo $configurations["PAGE_HEADER"]["MAIN_TITLE"]?>
              <small>
                  <?php echo $configurations["PAGE_HEADER"]["SUB_TITLE"]?>
              </small>
          </h3>
          <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueBreadCrumb",array());?>

          <!-- ------------------------------------------------------------------ -->
          <!-- CONTENT IS BEGIN                                                   -->
          <!-- ------------------------------------------------------------------ -->
          
          <div class="row">
            <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light bordered">
                <div class="portlet-title">
                  <div class="caption font-green">
                    <i class="<?php echo $configurations["PORTLET_HEADER"]["ICON"]?> font-green"></i>
                    <span class="caption-subject bold uppercase">
                      <?php echo $configurations["PORTLET_HEADER"]["TITLE"]?>
                    </span>
                  </div>
                </div>
                <div class="portlet-body">
                  <form class="form-horizontal" role="form" method="post" id="main-form" enctype="multipart/form-data"> 
                    <input type="hidden" name="session_time" value="<?php echo date("YmdHis")?>">
                    <div class="form-group">
                      <label class="col-md-2 control-label">Name (TH)<span class="required"> *</span></label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="name_th" autocomplete="off" placeholder="Name (TH)" value="<?php if(isset($promotion_discount->{"promotion_discount::name_th"}))  echo $promotion_discount->{"promotion_discount::name_th"}; ?>">
                      </div>
                    </div>
                    <div class="form-group"> 
                      <label class="col-md-2 control-label">Name (EN)<span class="required"> *</span></label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="name_en" autocomplete="off" placeholder="Name (EN)" value="<?php if(isset($promotion_discount->{"promotion_discount::name_en"}))  echo $promotion_discount->{"promotion_discount::name_en"}; ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">วัน/เวลาที่เริ่ม</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control datetime-picker" name="start_time" autocomplete="off" value="<?php if(isset($promotion_discount->{"promotion_discount::start_time"})) echo date("d F Y H:i", strtotime($promotion_discount->{"promotion_discount::start_time"}))?>" data-default="<?php if(isset($promotion_discount->{"promotion_discount::start_time"})) echo date("d F Y H:i", strtotime($promotion_discount->{"promotion_discount::start_time"}))?>">
                      </div>
                      <label class="col-md-3 control-label">ถึง</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control datetime-picker" name="end_time" autocomplete="off" value="<?php if(isset($promotion_discount->{"promotion_discount::end_time"})) echo date("d F Y H:i", strtotime($promotion_discount->{"promotion_discount::end_time"}))?>" data-default="<?php if(isset($promotion_discount->{"promotion_discount::end_time"})) echo date("d F Y H:i", strtotime($promotion_discount->{"promotion_discount::end_time"}))?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Discount(%)</label>
                      <div class="col-md-4">
                        <input type="text" class="form-control number" name="discount_percent" placeholder="0" autocomplete="off" value="<?php if(isset($promotion_discount->{"promotion_discount::discount_percent"}))  echo $promotion_discount->{"promotion_discount::discount_percent"}*100 ?>">
                        <small style="font-style: normal;color: lightslategray;">(ระบุเมื่อมีการลดแบบเปอร์เซนต์(%))</small>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Discount(Bant)</label>
                      <div class="col-md-4">
                        <input type="text" class="form-control number" name="discount_baht" placeholder="0" autocomplete="off" value="<?php if(isset($promotion_discount->{"promotion_discount::discount_baht"}))  echo $promotion_discount->{"promotion_discount::discount_baht"} ?>">
                        <small style="font-style: normal;color: lightslategray;">(ระบุเมื่อมีการลดบาท)</small>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Only Category</label>
                        <div class="col-md-4">
                          <select class="form-control select2" name="product_category_id">
                            <?php if (isset($promotion_discount->{"product_category::id"})){ ?>
                              <option value="<?php echo $promotion_discount->{"product_category::id"}?>">
                                <?php echo $promotion_discount->{"product_category::name_th"}; ?>
                              </option>
                            <?php }?>
                          </select>
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Status<span class="required"> *</span></label>
                      <div class="col-md-4">
                        <div class="mt-radio-inline">
                          <label class="mt-radio">
                            <input type="radio" name="status" value="ACTIVATE" <?php if(isset($promotion_discount->{"promotion_discount::status"}) && $promotion_discount->{"promotion_discount::status"} == "ACTIVATE")echo "checked"?>> Activate
                            <span></span>
                          </label>
                          <label class="mt-radio">
                            <input type="radio" name="status" value="SUSPEND" <?php if(!isset($promotion_discount->{"promotion_discount::status"}) || $promotion_discount->{"promotion_discount::status"} != "ACTIVATE")echo "checked"?>> Suspend
                            <span></span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                          <button type="button" class="btn green-jungle" data-toggle="modal" data-target="#confirmation-save" data-backdrop="static" data-keyboard="false">Save Changes</button>
                          <button type="button" class="btn red btn-outline" data-toggle="modal" data-target="#confirmation-cancel" data-backdrop="static" data-keyboard="false">Cancel</button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueFooter",array());?>
  </body>

  <!-- ------------------------------------------------------------------ -->
  <!-- JAVASCRIPT IS BEGIN                                                -->
  <!-- ------------------------------------------------------------------ -->
  <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/respond.min.js');?>"></script> <![endif]-->
  <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/excanvas.min.js');?>"></script> <![endif]-->

  <!-- MANDATORY SCRIPT AREA -->
  <script src="<?php echo assetsDirectory("global/plugins/jquery.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap/js/bootstrap.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/js.cookie.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-slimscroll/jquery.slimscroll.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery.blockui.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-switch/js/bootstrap-switch.min.js");?>" type="text/javascript"></script>

  <!-- PLUGINS AREA -->
  <script src="<?php echo assetsDirectory("global/plugins/jquery-ui/jquery-ui.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/moment.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/clockface/js/clockface.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/select2/js/select2.full.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/jquery.validate.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/additional-methods.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.js");?>"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote-cleaner.js");?>"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jstree/dist/jstree.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/scripts/datatable.js");?>" type="text/javascript"></script>

  <script src="<?php echo assetsDirectory("global/plugins/backstretch/jquery.backstretch.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.js");?>" type="text/javascript"></script>
    

  <!-- THEME SCRIPT AREA -->
  <script src="<?php echo assetsDirectory("global/scripts/app.min.js");?>" type="text/javascript"></script>

  <!-- LAYOUT AREA -->
  <script src="<?php echo assetsDirectory("layouts/layout/scripts/layout.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("layouts/layout/scripts/demo.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("layouts/global/scripts/quick-sidebar.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery.numeric.js");?>" type="text/javascript"></script>

  <!-- CUSTOM AREA -->
  <script>
    jQuery(document).ready(function() {
      $('.number').numeric({ decimal: false, negative: false });
      $('.pos-decimal').numeric({ negative: false });
      $(".datetime-picker").datetimepicker({
        todayHighlight: true,
        autoclose: true,
        format: "dd MM yyyy hh:ii",
        altField  : ".datetime-picker"
      });
      
      
      $("select[name='product_category_id']").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/promotion_discount/select2/edit_product_category")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_th };
              })
            };
          }
        }
      });
      /** =================================================================== **/
      /** FORM VALIDATE                                                       **/
      /** =================================================================== **/


      $("#main-form").validate({
        errorElement: 'span', errorClass: 'help-block', focusInvalid: false,
        rules: {
          name_en: {required: true},
          name_en: {required:true},
          status: {required: true}
        },
        highlight: function (element) { $(element).closest('.form-group').addClass('has-error'); },
        success: function (label) { label.closest('.form-group').removeClass('has-error'); label.remove(); },
        invalidHandler: function(form, validator) { $('.confirmation-save').modal('toggle'); },
        errorPlacement: function (error, element) { error.appendTo(element.closest('div')); },
        submitHandler: function() {
          App.blockUI({ target: 'body', animate: true });
          $.ajax({
            type: "POST",
            <?php
            $post_url = "";
            if(isset($promotion_discount->{"promotion_discount::id"})){
              $post_url = base_url("administrator_area/promotion_discount/edit/".$promotion_discount->{"promotion_discount::id"});
            }else{
              $post_url = base_url("administrator_area/promotion_discount/edit");
            }
            ?>
            url: "<?php echo $post_url?>",
            data: new FormData($('#main-form')[0]),
            cache: false,
            contentType: false,
            processData: false,
            success: function(response){
              try {
                if(response.code == "0x0000-00000") {
                  toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "positionClass": "toast-bottom-right",
                  "onclick": null,
                  "showDuration": "1000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                toastr.success('Update Completed', 'Success.');
                window.history.back();
                }else{
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch(e) {
                $("#system-return-failed").modal("toggle");
              }
              App.unblockUI('body');
            },
            error: function(){
              $("#system-disconnected").modal("toggle");
              App.unblockUI('body');
            }
          });
        }
      });


    });
  </script>

  <!-- INCLUDE RAW SCRIPT AREA -->
  <?php $this->load->view("administrator_area/__scripts/Javascript_Modal",array());?>
  <?php $this->load->view("administrator_area/__scripts/Javascript_Select2",array());?>
</html>

<!-- ------------------------------------------------------------------ -->
<!-- MODAL IS BEGIN                                                     -->
<!-- ------------------------------------------------------------------ -->
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationCancel");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationDelete");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationRefresh");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationSave");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationSaveMultiform");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_PermissionDenied");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemDisconnected");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnError");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnFailed");?>
