<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- <title>Quotation</title> -->
  <link href="<?php echo assetsDirectory("global/plugins/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet"
    type="text/css" />
  <link href="<?php echo assetsDirectory("global/plugins/simple-line-icons/simple-line-icons.min.css");?>"
    rel="stylesheet" type="text/css" />
  <link href="<?php echo assetsDirectory("global/plugins/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet"
    type="text/css" />
  <link href="<?php echo assetsDirectory("global/plugins/bootstrap-switch/css/bootstrap-switch.min.css");?>"
    rel="stylesheet" type="text/css" />

  <!-- PLUGINS AREA -->


  <!-- THEME STYLE AREA -->
  <link href="<?php echo assetsDirectory("global/css/components-md.min.css");?>" rel="stylesheet" id="style_components"
    type="text/css" />
  <link href="<?php echo assetsDirectory("global/css/plugins-md.min.css");?>" rel="stylesheet" type="text/css" />

  <!-- LAYOUT AREA -->
  <link href="<?php echo assetsDirectory("layouts/layout/css/layout.min.css");?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo assetsDirectory("layouts/layout/css/custom.min.css");?>" rel="stylesheet" type="text/css" />
</head>
<style>
@media print {
  @page {
    margin-top: 0;
    margin-bottom: 0;
  }
  body {
    padding-top: 72px;
    padding-bottom: 72px ;
  }
  .color_grey{
    color : #aaaaaa !important;
  }
  .mt-3{
    margin-top: 1.5rem !important;
  }

}
.footer {
 position: fixed;
 left: 0;
 bottom: 0;
}
.color_grey{
  color : #aaaaaa !important;
}
.mt-3{
  margin-top: 1.5rem !important;
}

</style>

<body>

  <table width="100%" border="0">
    <br><br>
    <tr>
      <td width="50%">
        <img src="<?php echo base_url($contact_us->logo_path) ?>" alt="" style="width: 164px;">
        <p style="margin: 5px;"><?php echo $contact_us->company_th ?></p>
        <p style="margin: 5px;"><?php echo $contact_us->address_th ?></p>
        <p style="margin: 5px;"><?php echo 'เลขประจำตัวผูเสียภาษี '.$contact_us->tax_identification_number ?></p>
        <p style="margin: 5px;"><?php echo 'โทร '.$contact_us->telephone ?></p>
        <p style="margin: 5px;"><?php echo 'เบอร์มือถือ '.$contact_us->phone ?></p>
        <p style="margin: 5px;"><?php echo base_url() ?></p>
      </td>
      <td>
        <h4 class="color_grey" style="white-space: nowrap; text-align:center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ใบส่งสินค้า/ใบแจ้งหนี้/ใบกำกับภาษี</h4>
        <h5 class="color_grey" style="white-space: nowrap; text-align:center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ต้นฉบับ (เอกสารออกเป็นชุด)</h5>
        <hr style="margin: 5px;">
        <p style="margin: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="color_grey">เลขที่</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php  echo $quotation->quotation_no ?></p>
        <p style="margin: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="color_grey">วันที่</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo date("d/m/Y"); ?></p>
        <p style="margin: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="color_grey">ผู้ขาย</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($administrator)) echo $administrator->first_name.' '.$administrator->last_name; ?></p>
        <p style="margin: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="color_grey">อ้างอิง</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($administrator)) echo $administrator->first_name.' '.$administrator->last_name; ?></p>

          <hr style="margin: 5px;">

          <p style="margin: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="color_grey">ชื่องาน</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php  echo $quotation->message; ?></p>
          <p class="mt-3" style="margin: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="color_grey">ผู้ติดต่อ</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $quotation->first_name.' '.$quotation->last_name; ?></p>
          <p style="margin: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="color_grey">เบอร์โทร</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php  echo $quotation->telephone; ?></p>
          <p style="margin: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="color_grey">อีเมล</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php  echo $quotation->telephone; ?></p>
      </td>
    </tr>
    <tr>
      <td>
        <h5 class="color_grey" style="margin: 0px;">ลูกค้า</h5>
        <p style="margin: 5px;">
          <?php echo $quotation->company_name ?><br>
          <?php echo 'เลขประจำตัวผูเสียภาษี '.$quotation->tax_id ?>
        </p>
      </td>
    </tr>
  </table>
  <br>
  <table width="100%" class="table table-bordered">
    <thead>
      <tr>
        <th style="text-align: center;" width="5%">#</th>
        <th style="text-align: center;" width="50%">รายละเอียด</th>
        <th style="text-align: center;" width="10%">จำนาน</th>
        <th style="text-align: center;" width="20%">ราคาต่อหน่วย</th>
        <th style="text-align: center;" width="20%">ยอดรวม</th>
      </tr>
    </thead>
    <tbody>
      <?php $total = 0;?>
      <?php foreach($quotation_details as $key => $quotation_detail){ $key++; ?>
      <tr>
        <td style="text-align: center;"><?php echo $key; ?></td>
        <td><?php echo $quotation_detail->{'product::name_th'}; ?></td>
        <td style="text-align: center;"><?php echo $quotation_detail->{'quotation_detail::quantity'}; ?></td>
        <td style="text-align: center;">
          <?php echo number_format($quotation_detail->{'quotation_detail::price_per_unit'},2); ?></td>
        <td style="text-align: center;">
          <?php echo number_format($quotation_detail->{'quotation_detail::total_price'},2); ?></td>
      </tr>

      <?php $total  += $quotation_detail->{'quotation_detail::total_price'}; ?>


      <?php } ?>

    </tbody>
  </table>
  <p style="text-align: end;margin: 5px;">
    <span class="color_grey">รวมเป็นเงิน</span>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <?php echo number_format($total,2); ?>
    บาท
  </p>
  <p style="text-align: end;margin: 5px;">
    <span class="color_grey">ส่วนลด 10%</span>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <?php echo number_format(($total*(7/100)),2); ?>
    บาท
  </p>
  <p style="text-align: end;margin: 5px;">
    <span class="color_grey">จำนวนเงินหลักหักส่วนลด</span>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <?php echo number_format(($total*(7/100)),2); ?>
    บาท
  </p>
  <p style="text-align: end;margin: 5px;">
    <span class="color_grey">ภาษีมูลค่าเพิ่ม 7%</span>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <?php echo number_format(($total*(7/100)),2); ?>
    บาท
  </p>
  <p style="text-align: end;margin: 5px;">
    <span class="color_grey">จำนานเงินรวมทั้งสิ้น</span>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <?php echo number_format($total+($total*(7/100)),2); ?>
    บาท
  </p>
  <p id="AMOUNT" style="text-align: center;font-size: 17px;"></p>
  <br><br><br><br><br><br><br>
  <!-- <table width="100%" border="0">
    <tr>
        <td width="65%">
        หมายเหตุ
        </td>
    </tr>
</table> -->
<div class="footer">
  <table width="100%" border="0">
    <tr>
      <td style="text-align: center;"width="10%">
        <hr>
        <p>ผู้สั่งซื้อสินค้า</p>
      </td>
      <td style="text-align: center;"width="1%">
        
      </td>
      <td style="text-align: center;"width="10%">
        <hr>
        <p>วันที่</p>
      </td>
      <td style="text-align: center;"width="10%">
        
      </td>
      <td style="text-align: center;"width="10%">
        <hr>
        <p>ผู้อนุมัติ</p>
      </td>
      <td style="text-align: center;"width="1%">
        
      </td>
      <td style="text-align: center;"width="10%">
        <hr>
        <p>วันที่</p>
      </td>
    </tr>
  </table>
  </div>
  


</body>
<script src="<?php echo assetsDirectory("global/plugins/jquery.min.js");?>" type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/bootstrap/js/bootstrap.min.js");?>" type="text/javascript">
</script>
<script src="<?php echo assetsDirectory("global/plugins/js.cookie.min.js");?>" type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js");?>"
  type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/jquery-slimscroll/jquery.slimscroll.min.js");?>"
  type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/jquery.blockui.min.js");?>" type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/bootstrap-switch/js/bootstrap-switch.min.js");?>"
  type="text/javascript"></script>

<!-- THEME SCRIPT AREA -->
<script src="<?php echo assetsDirectory("global/scripts/app.min.js");?>" type="text/javascript"></script>

<!-- LAYOUT AREA -->
<script src="<?php echo assetsDirectory("layouts/layout/scripts/layout.min.js");?>" type="text/javascript"></script>
<!-- CUSTOM AREA -->
<script src="<?php echo assetsDirectory("js/thaibath.js"); ?>"></script>
<script>
  var thaibath = ArabicNumberToText("<?php echo number_format($total+($total*(7/100)),2); ?>");
  $("#AMOUNT").html('(' + thaibath + ')');
</script>
<script>
    
    $(document).ready(function () {
    window.print();
});
</script>

</html>