<!-- ------------------------------------------------------------------ -->
<!-- PERMISSION CHECK IS BEGIN                                          -->
<!-- ------------------------------------------------------------------ -->
<?php
$codeigniter_instance =& get_instance();

$configurations = array(
    "PAGE_TITLE" => "Administrator System",
    "PAGE_HEADER" => array(
        "MAIN_TITLE" => "Coupon",
        "SUB_TITLE" => ""
    ),
    "PORTLET_HEADER" => array(
        "ICON" => "fa fa-bars",
        "TITLE" => "Coupon List"
    )
);
?>
<!-- ------------------------------------------------------------------ -->
<!-- HEADER IS BEGIN                                                    -->
<!-- ------------------------------------------------------------------ -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <title><?php echo $this->config->item('WEBSITE_NAME');?> | Login System</title>
        <link href="<?php echo faviconDirectory($this->config->item('WEBSITE_FAVICON'));?>" rel="shortcut icon" />

        <!-- META TAG AREA -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

        <!-- MANDATORY STYLE AREA -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/simple-line-icons/simple-line-icons.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-switch/css/bootstrap-switch.min.css");?>" rel="stylesheet" type="text/css" />

        <!-- PLUGINS AREA -->
        <link href="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/clockface/css/clockface.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2-bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.css");?>" rel="stylesheet">
        <link href="<?php echo assetsDirectory("global/plugins/jstree/dist/themes/default/style.css");?>" rel="stylesheet">
        <link href="<?php echo assetsDirectory("global/plugins/dropzone/basic.min.css");?>" rel="stylesheet">
        <link href="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.css");?>" rel="stylesheet">

        <!-- THEME STYLE AREA -->
        <link href="<?php echo assetsDirectory("global/css/components-md.min.css");?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo assetsDirectory("global/css/plugins-md.min.css");?>" rel="stylesheet" type="text/css" />

        <!-- LAYOUT AREA -->
        <link href="<?php echo assetsDirectory("layouts/layout/css/layout.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("layouts/layout/css/themes/light.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("layouts/layout/css/custom.min.css");?>" rel="stylesheet" type="text/css" />

        <!-- CUSTOM AREA -->
    </head>

    <!-- ------------------------------------------------------------------ -->
    <!-- BODY IS BEGIN                                                      -->
    <!-- ------------------------------------------------------------------ -->
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-md">
        <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueHeader",array());?>
        <div class="page-container">
            <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueSideBar",array());?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <h3 class="page-title">
                        <?php echo $configurations["PAGE_HEADER"]["MAIN_TITLE"]?>
                        <small>
                            <?php echo $configurations["PAGE_HEADER"]["SUB_TITLE"]?>
                        </small>
                    </h3>
                    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueBreadCrumb",array());?>

                    <!-- ------------------------------------------------------------------ -->
                    <!-- CONTENT IS BEGIN                                                   -->
                    <!-- ------------------------------------------------------------------ -->
                    <?php
                    if(checkAdministratorPermission("COUPON", "index")){
                        ?>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-green">
                                            <i class="<?php echo $configurations["PORTLET_HEADER"]["ICON"]?> font-green"></i>
                                            <span class="caption-subject bold uppercase"><?php echo $configurations["PORTLET_HEADER"]["TITLE"]?></span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <table class="table table-striped table-bordered table-hover order-column" id="main-table">
                                            <thead>
                                            <tr>
                                                <th style="padding: 5px;"><input type="text" class="form-control table-head-search" data-index="0" data-type="text" data-table="#main-table"></th>
                                                <th style="padding: 5px;"><input type="text" class="form-control table-head-search" data-index="1" data-type="text" data-table="#main-table"></th>
                                                <th style="padding: 5px;"><input type="text" class="form-control table-head-search" data-index="2" data-type="text" data-table="#main-table"></th>
                                                <th style="padding: 5px;"><input type="text" class="form-control table-head-search" data-index="3" data-type="text" data-table="#main-table"></th>
                                                <th style="padding: 5px;"><input type="text" class="form-control table-head-search" data-index="3" data-type="text" data-table="#main-table"></th>
                                                <th style="padding: 7px;">
                                                    <div class="dropdown">
                                                        <button class="btn green btn-sx dropdown-toggle col-md-7" type="button" data-toggle="dropdown">
                                                            Menu <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <?php
                                                            if(checkAdministratorPermission("COUPON", "edit")){
                                                                ?>
                                                                <li><a href="<?php echo base_url("administrator_area/coupon/edit");?>"><i class="fa fa-plus-circle"></i> Create</a></li>
                                                                <?php
                                                            }else{
                                                                ?>
                                                                <li><a onclick="$('#permission-denied').modal('toggle');"><i class="fa fa-plus-circle"></i> Create</a></li>
                                                                <?php
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                    <a class="btn green btn-sx col-md-4 table-head-refresh" data-table="#main-table">
                                                        <span class="md-click-circle md-click-animate"></span>
                                                        <i class="fa fa-refresh"></i>
                                                    </a>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th style="width:25%;"> Code </th>
                                                <th style="width:15%;"> Total Qty </th>
                                                <th style="width:15%;"> Remaining </th>
                                                <th style="width:15%;"> Discount(%) </th>
                                                <th style="width:15%;"> Discount(Baht) </th>
                                                <th style="width:15%;"> Action </th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                        <?php
                    }else{
                        ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div style="text-align: center;">
                                    <div class="row" style="padding-top: 50px;">
                                        <div class="page-spinner-bar-custom">
                                            <div class="bounce1"></div>
                                            <div class="bounce2"></div>
                                            <div class="bounce3"></div>
                                            <div class="bounce4"></div>
                                            <div class="bounce5"></div>
                                        </div>
                                    </div>
                                    <div class="caption font-red-thunderbird row" style="padding-top: 30px;padding-bottom: 50px;">
                                        <i class="fa fa-unlock-alt font-red-thunderbird"></i><br>
                                        <span class="caption-subject bold uppercase"> Permission Denied<br><small>You are not allow to use this function.</small></span><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>

        <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueFooter",array());?>
    </body>

    <!-- ------------------------------------------------------------------ -->
    <!-- JAVASCRIPT IS BEGIN                                                -->
    <!-- ------------------------------------------------------------------ -->
    <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/respond.min.js');?>"></script> <![endif]-->
    <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/excanvas.min.js');?>"></script> <![endif]-->

    <!-- MANDATORY SCRIPT AREA -->
    <script src="<?php echo assetsDirectory("global/plugins/jquery.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap/js/bootstrap.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/js.cookie.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery-slimscroll/jquery.slimscroll.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery.blockui.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-switch/js/bootstrap-switch.min.js");?>" type="text/javascript"></script>

    <!-- PLUGINS AREA -->
    <script src="<?php echo assetsDirectory("global/plugins/jquery-ui/jquery-ui.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/moment.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/clockface/js/clockface.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/select2/js/select2.full.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/jquery.validate.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/additional-methods.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.js");?>"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote-cleaner.js");?>"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jstree/dist/jstree.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/scripts/datatable.js");?>" type="text/javascript"></script>

    <script src="<?php echo assetsDirectory("global/plugins/backstretch/jquery.backstretch.min.js");?>" type="text/javascript"></script>

    <!-- THEME SCRIPT AREA -->
    <script src="<?php echo assetsDirectory("global/scripts/app.min.js");?>" type="text/javascript"></script>

    <!-- LAYOUT AREA -->
    <script src="<?php echo assetsDirectory("layouts/layout/scripts/layout.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("layouts/layout/scripts/demo.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("layouts/global/scripts/quick-sidebar.js");?>" type="text/javascript"></script>

    <!-- CUSTOM AREA -->
    <script>
        jQuery(document).ready(function() {
            <?php
            if(checkAdministratorPermission("COUPON", "index")){
                ?>
                /** =================================================================== **/
                /** DATA TABLES CONTROL                                                 **/
                /** =================================================================== **/
                $("#main-table").dataTable({
                    processing: true,
                    serverSide: true,
                    pageLength: 25,
                    ajax:{
                        url: "<?php echo base_url("administrator_area/coupon/datatables/index")?>",
                        type: "POST"
                    },
                    columns: [
                        {
                            data: 'code'
                        },
                        {
                            data: 'total_qty'
                        },
                        {
                            data: 'remaining_qty'
                        },
                        {
                            render: function(data, type, row, meta){
                                if(row["discount_percent"]!=null){
                                    return row["discount_percent"]*100;
                                }
                                else{
                                    return "";
                                }
                            },
                            data: 'discount_percent'
                        },
                        {
                            data: 'discount_baht'
                        },
                        {
                            render: function(data, type, row, meta){
                                var action_header =     '<div class="dropdown">'+
                                    '<button class="btn btn-block green btn-xs dropdown-toggle" type="button" data-toggle="dropdown">Action Menu '+
                                    '<span class="caret"></span></button>';
                                    action_header += '<ul class="dropdown-menu dropdown-menu-right" style="left: auto;">';
                                    action_header +=    '<li><a href="<?php echo base_url("administrator_area/coupon/detail");?>/'+ row["id"] +'"><i class="fa fa-wrench"></i> Detail</a></li>';
                                    <?php
                                    if(checkAdministratorPermission("COUPON", "edit")){
                                    ?>
                                        action_header +=    '<li><a href="<?php echo base_url("administrator_area/coupon/edit");?>/'+ row["id"] +'"><i class="fa fa-wrench"></i> Edit record</a></li>';
                                    <?php
                                    }else{
                                    ?>
                                        action_header +=    '<li><a onclick="$(\'#permission-denied\').modal(\'toggle\');"><i class="fa fa-wrench"></i> Edit record</a></li>';
                                    <?php
                                    }

                                    if(checkAdministratorPermission("COUPON", "delete")){
                                    ?>
                                        action_header +=    '<li><a onclick="$(\'#confirmation-delete\').modal(\'toggle\');$(\'#confirmation-delete #delete-url\').val(\'<?php echo base_url("administrator_area/coupon/delete");?>/'+row["id"]+'\');"><i class="fa fa-trash"></i> Delete record </a></li>';
                                    <?php
                                    }else{
                                    ?>
                                        action_header +=    '<li><a onclick="$(\'#permission-denied\').modal(\'toggle\');"><i class="fa fa-trash"></i> Delete record </a></li>';
                                    <?php
                                    }
                                    ?>
                                    action_header += '</ul>'+
                                    '</div>';
                                return action_header;
                            },
                            searchable: false,
                            orderable: false
                        }
                    ]
                });
                <?php
            }else{
                ?>
                $('#permission-denied').modal('show');
                <?php
            }
            ?>
        });
    </script>

    <!-- INCLUDE RAW SCRIPT AREA -->
    <?php $this->load->view("administrator_area/__scripts/Javascript_Datatables",array());?>
    <?php $this->load->view("administrator_area/__scripts/Javascript_Modal",array());?>
    <?php $this->load->view("administrator_area/__scripts/Javascript_Select2",array());?>
</html>

<!-- ------------------------------------------------------------------ -->
<!-- MODAL IS BEGIN                                                     -->
<!-- ------------------------------------------------------------------ -->
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationDelete");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_PermissionDenied");?>