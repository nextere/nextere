<?php
$codeigniter_instance =& get_instance();

$html = $_POST["htmlExcel"];

header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=ProductForm.xls");
header("Pragma: no-cache");
// header("Expires: 0");
// }
?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http://www.w3.org/TR/REC-html40">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
  <div class="container">
    <div class="row">
      <table x:str border=1 cellpadding=0 cellspacing=1 style="border-collapse:collapse">
        <tr>
          <th></th>
          <th>Item</th>
          <td></td>
        </tr>
        <tr>
          <th>1</th>
          <th>Product Solution Category (EN)</th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th>2</th>
          <th>Product Category (EN)</th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th></th>
          <th></th>
          <td></td>
        </tr>
        <tr>
          <th>3</th>
          <th>Product Type(ประเภทสินค้า) (EN)</th>
          <td></td>
        </tr>
        <tr>
          <th>4</th>
          <th>Product Name (EN)</th>
          <td></td>
        </tr>
        <tr>
          <th>5</th>
          <th>Product Name (TH)</th>
          <td></td>
        </tr>
        <tr>
          <th>6</th>
          <th>Code</th>
          <td></td>
        </tr>
        <tr>
          <th>7</th>
          <th>Brand (EN)</th>
          <td></td>
        </tr>
        <tr>
          <th>8</th>
          <th>Model (EN)</th>
          <td></td>
        </tr>
        <tr>
          <th>9</th>
          <th>Model (TH)</th>
          <td></td>
        </tr>
        <tr>
          <th>10</th>
          <th>Short Detail (EN)</th>
          <td></td>
        </tr>
        <tr>
          <th>11</th>
          <th>Short Detail (TH)</th>
          <td></td>
        </tr>
        <tr>
          <th>12</th>
          <th>Description (EN)</th>
          <td></td>
        </tr>
        <tr>
          <th>13</th>
          <th>Description (TH)</th>
          <td></td>
        </tr>
        <tr>
          <th>14</th>
          <th>Basic Quality (EN)</th>
          <td></td>
        </tr>
        <tr>
          <th>15</th>
          <th>Basic Quality (TH)</th>
          <td></td>
        </tr>
        <tr>
          <th>16</th>
          <th>ระยะเวลาที่รอสินค้า (กรณีไม่มีสินค้าในสต็อก)</th>
          <td></td>
        </tr>
        <tr>
          <th>17</th>
          <th>ราคาสินค้าปลีก</th>
          <td></td>
        </tr>
        <tr>
          <th>18</th>
          <th>จำนวนสินค้าขั้นต่ำ</th>
          <td></td>
        </tr>
        <tr>
          <th>19</th>
          <th>ค่าส่ง/ชิ้น</th>
          <td></td>
        </tr>
        <tr>
          <th>20</th>
          <th>ราคาสินค้าตามจำนวน</th>
          <td></td>
        </tr>
        <tr>
          <th>21</th>
          <th>เงื่อนไขสินค้า (EN)</th>
          <td></td>
        </tr>
        <tr>
          <th>22</th>
          <th>เงื่อนไขสินค้า (TH)</th>
          <td></td>
        </tr>
        <tr>
          <th>23</th>
          <th>เงื่อนไขการรับประกัน (EN)</th>
          <td></td>
        </tr>
        <tr>
          <th>24</th>
          <th>เงื่อนไขการรับประกัน (TH)</th>
          <td></td>
        </tr>
        <tr>
          <th>25</th>
          <th>In Stock Qty</th>
          <td></td>
        </tr>
        <tr>
          <th>26</th>
          <th>Safety Stock</th>
          <td></td>
        </tr>
        <tr>
          <th>27</th>
          <th>คำค้นหา</th>
          <td></td>
        </tr>
      </table>
    </div>
  </div>
</body>
</html>