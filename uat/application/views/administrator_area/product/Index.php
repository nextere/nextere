<!-- ------------------------------------------------------------------ -->
<!-- PERMISSION CHECK IS BEGIN                                          -->
<!-- ------------------------------------------------------------------ -->
<?php
$codeigniter_instance =& get_instance();

$configurations = array(
    "PAGE_TITLE" => "Administrator System",
    "PAGE_HEADER" => array(
        "MAIN_TITLE" => "Product Management",
        "SUB_TITLE" => ""
    ),
    "PORTLET_HEADER" => array(
        "ICON" => "fa fa-bars",
        "TITLE" => "Product List"
    )
);
?>
<!-- ------------------------------------------------------------------ -->
<!-- HEADER IS BEGIN                                                    -->
<!-- ------------------------------------------------------------------ -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <title><?php echo $this->config->item('WEBSITE_NAME');?> | Login System</title>
        <link href="<?php echo faviconDirectory($this->config->item('WEBSITE_FAVICON'));?>" rel="shortcut icon" />

        <!-- META TAG AREA -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

        <!-- MANDATORY STYLE AREA -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/simple-line-icons/simple-line-icons.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-switch/css/bootstrap-switch.min.css");?>" rel="stylesheet" type="text/css" />

        <!-- PLUGINS AREA -->
        <link href="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/clockface/css/clockface.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2-bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.css");?>" rel="stylesheet">
        <link href="<?php echo assetsDirectory("global/plugins/jstree/dist/themes/default/style.css");?>" rel="stylesheet">
        <link href="<?php echo assetsDirectory("global/plugins/dropzone/basic.min.css");?>" rel="stylesheet">
        <link href="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.css");?>" rel="stylesheet">
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.css");?>" rel="stylesheet" type="text/css" />

        <!-- THEME STYLE AREA -->
        <link href="<?php echo assetsDirectory("global/css/components-md.min.css");?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo assetsDirectory("global/css/plugins-md.min.css");?>" rel="stylesheet" type="text/css" />

        <!-- LAYOUT AREA -->
        <link href="<?php echo assetsDirectory("layouts/layout/css/layout.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("layouts/layout/css/themes/light.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("layouts/layout/css/custom.min.css");?>" rel="stylesheet" type="text/css" />

        <!-- CUSTOM AREA -->
    </head>

    <!-- ------------------------------------------------------------------ -->
    <!-- BODY IS BEGIN                                                      -->
    <!-- ------------------------------------------------------------------ -->
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-md">
        <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueHeader",array());?>
        <div class="page-container">
            <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueSideBar",array());?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <h3 class="page-title">
                        <?php echo $configurations["PAGE_HEADER"]["MAIN_TITLE"]?>
                        <small>
                            <?php echo $configurations["PAGE_HEADER"]["SUB_TITLE"]?>
                        </small>
                    </h3>
                    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueBreadCrumb",array());?>

                    <!-- ------------------------------------------------------------------ -->
                    <!-- CONTENT IS BEGIN                                                   -->
                    <!-- ------------------------------------------------------------------ -->
                    <?php
                    if(checkAdministratorPermission("PRODUCT_MANAGEMENT", "index")){
                        ?>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-green">
                                            <i class="<?php echo $configurations["PORTLET_HEADER"]["ICON"]?> font-green"></i>
                                            <span class="caption-subject bold uppercase"><?php echo $configurations["PORTLET_HEADER"]["TITLE"]?></span>
                                        </div>
                                        <div class="caption font-green  pull-right">
                                            <a id="export_form"  class="btn green btn-sx btn-block table-head-refresh" data-table="#main-table">
                                                <span class="md-click-circle md-click-animate"></span>
                                                <i class="fa fa-file-excel-o"></i>
                                              Export Form
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                    <form id="frm-table">
                                        <table class="table table-striped table-bordered table-hover order-column" id="main-table">
                                        <div class="mt-10 mb-5 collapse" id="kt_datatable_group_action_form">
                                        <div class="d-flex align-items-center">
                                            <div class="font-weight-bold text-danger mr-3">Selected
                                                <span id="kt_datatable_selected_records">0</span>records 
                                                <button class="btn btn-sm btn-success mr-2" type="button"
                                                id="kt_datatable_compare_multi" onclick="$('#confirmation-delete-all').modal('toggle');$('#confirmation-delete-all #delete-url-all').val('<?php echo base_url('administrator_area/product/delete_all'); ?>');"><i class="fa fa-trash"></i>Delete</button>
                                            </div>
                                        </div>
                                        </div>
                                            <thead>
                                            <tr>
                                                <th style="padding: 5px;"></th>
                                                <th style="padding: 5px;"><input type="text" class="form-control table-head-search" data-index="0" data-type="text" data-table="#main-table"></th>
                                                <th style="padding: 5px;"><input type="text" class="form-control table-head-search" data-index="1" data-type="text" data-table="#main-table"></th>
                                                <th style="padding: 5px;"><input type="text" class="form-control table-head-search" data-index="2" data-type="text" data-table="#main-table"></th>
                                                <th style="padding: 5px;"><input type="text" class="form-control table-head-search" data-index="3" data-type="text" data-table="#main-table"></th>
                                                <th style="padding: 5px;"><input type="text" class="form-control table-head-search" data-index="4" data-type="text" data-table="#main-table"></th>
                                                <th style="padding: 7px;">
                                                    <div class="dropdown">
                                                        <button class="btn green btn-sx dropdown-toggle col-md-7" type="button" data-toggle="dropdown">
                                                            Menu <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <?php
                                                            if(checkAdministratorPermission("PRODUCT_MANAGEMENT", "edit")){
                                                                ?>
                                                                <li><a href="<?php echo base_url("administrator_area/product/edit");?>"><i class="fa fa-plus-circle"></i> Create</a></li>
                                                                <li><a href="javascript:void(0)" class="open-import-file"><i class="fa fa-plus-circle"></i> Import Product</a></li>
                                                                <li><a href="javascript:void(0)" class="open-import-technical-file"><i class="fa fa-plus-circle"></i> Import Technical</a></li>
                                                            <?php
                                                            }else{
                                                                ?>
                                                                <li><a onclick="$('#permission-denied').modal('toggle');"><i class="fa fa-plus-circle"></i> Create</a></li>
                                                                <?php
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                    <a class="btn green btn-sx col-md-4 table-head-refresh" data-table="#main-table">
                                                        <span class="md-click-circle md-click-animate"></span>
                                                        <i class="fa fa-refresh"></i>
                                                    </a>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th style="width:1%;" > </th>
                                                <th style="width:15%;">Code</th>
                                                <th style="width:15%;">Brand</th>
                                                <th style="width:20%;">Name</th>
                                                <th style="width:20%;">Model</th>
                                                <th style="width:15%;">Type</th>
                                                <th style="width:15%;"> Action </th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                        </form>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>
                        <?php
                    }else{
                        ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div style="text-align: center;">
                                    <div class="row" style="padding-top: 50px;">
                                        <div class="page-spinner-bar-custom">
                                            <div class="bounce1"></div>
                                            <div class="bounce2"></div>
                                            <div class="bounce3"></div>
                                            <div class="bounce4"></div>
                                            <div class="bounce5"></div>
                                        </div>
                                    </div>
                                    <div class="caption font-red-thunderbird row" style="padding-top: 30px;padding-bottom: 50px;">
                                        <i class="fa fa-unlock-alt font-red-thunderbird"></i><br>
                                        <span class="caption-subject bold uppercase"> Permission Denied<br><small>You are not allow to use this function.</small></span><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>

        <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueFooter",array());?>
    </body>

    <!-- ------------------------------------------------------------------ -->
    <!-- JAVASCRIPT IS BEGIN                                                -->
    <!-- ------------------------------------------------------------------ -->
    <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/respond.min.js');?>"></script> <![endif]-->
    <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/excanvas.min.js');?>"></script> <![endif]-->

    <!-- MANDATORY SCRIPT AREA -->
    <script src="<?php echo assetsDirectory("global/plugins/jquery.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap/js/bootstrap.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/js.cookie.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery-slimscroll/jquery.slimscroll.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery.blockui.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-switch/js/bootstrap-switch.min.js");?>" type="text/javascript"></script>

    <!-- PLUGINS AREA -->
    <script src="<?php echo assetsDirectory("global/plugins/jquery-ui/jquery-ui.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/moment.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/clockface/js/clockface.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/select2/js/select2.full.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/jquery.validate.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/additional-methods.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.js");?>"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote-cleaner.js");?>"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jstree/dist/jstree.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/scripts/datatable.js");?>" type="text/javascript"></script>

    <script src="<?php echo assetsDirectory("global/plugins/backstretch/jquery.backstretch.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.js");?>" type="text/javascript"></script>

    <!-- THEME SCRIPT AREA -->
    <script src="<?php echo assetsDirectory("global/scripts/app.min.js");?>" type="text/javascript"></script>

    <!-- LAYOUT AREA -->
    <script src="<?php echo assetsDirectory("layouts/layout/scripts/layout.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("layouts/layout/scripts/demo.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("layouts/global/scripts/quick-sidebar.js");?>" type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <!-- CUSTOM AREA -->
    <script>
        jQuery(document).ready(function() {
            <?php
            if(checkAdministratorPermission("PRODUCT_MANAGEMENT", "index")){
                ?>
                $('#export_form').click( function() {
                    window.open('<?php  echo base_url("administrator_area/product/export_form"); ?>','_blank');
                    alert('ดำเนินการเรียบร้อย');
                });
                /** =================================================================== **/
                /** DATA TABLES CONTROL                                                 **/
                /** =================================================================== **/
                $("#main-table").dataTable({
                    processing: true,
                    serverSide: true,
                    pageLength: 25,
                    order : [[1, 'asc']],
                    ajax:{
                        url: "<?php echo base_url("administrator_area/product/datatables/index")?>",
                        type: "POST"
                    },
                    columns: [
                        {
                            'searchable': false,
                            'orderable': false,
                            'className': 'dt-body-center',
                            render: function(data, type, row, meta){
                                return '<input type="checkbox" name="id[]"  class="c" value="'+ row["id"] +'" onclick="updateCount()">';
                            }
                        },
                        {
                            data: 'code'
                        },
                        {
                            data: 'brand_name_en'
                        },
                        {
                            data: 'name_en'
                        },
                        {
                            data: 'model_name_en'
                        },
                        {
                            data: 'product_type_name_en'
                        },
                        {
                            render: function(data, type, row, meta){
                                var action_header =     '<div class="dropdown">'+
                                    '<button class="btn btn-block green btn-xs dropdown-toggle" type="button" data-toggle="dropdown">Action Menu '+
                                    '<span class="caret"></span></button>';
                                    action_header += '<ul class="dropdown-menu dropdown-menu-right" style="left: auto;">';
                                    action_header +=    '<li><a href="<?php echo base_url("administrator_area/product/review");?>/'+ row["id"] +'"><i class="fa fa-star"></i> Review</a></li>';
                                    action_header +=    '<li><a href="<?php echo base_url("administrator_area/product/detail");?>/'+ row["id"] +'"><i class="fa fa-wrench"></i> Detail</a></li>';
                                    action_header +=    '<li><a href="<?php echo base_url("administrator_area/product/detail_technical");?>/'+ row["id"] +'"><i class="fa fa-wrench"></i> Technical Spec</a></li>';
                                    <?php
                                    if(checkAdministratorPermission("PRODUCT_MANAGEMENT", "edit")){
                                    ?>
                                        action_header +=    '<li><a href="<?php echo base_url("administrator_area/product/edit");?>/'+ row["id"] +'"><i class="fa fa-wrench"></i> Edit record</a></li>';
                                        action_header +=    '<li><a href="<?php echo base_url("administrator_area/product/edit_technical");?>/'+ row["id"] +'"><i class="fa fa-wrench"></i> Edit Technical Spec</a></li>';
                                    <?php
                                    }else{
                                    ?>
                                        action_header +=    '<li><a onclick="$(\'#permission-denied\').modal(\'toggle\');"><i class="fa fa-wrench"></i> Edit record</a></li>';
                                    <?php
                                    }

                                    if(checkAdministratorPermission("PRODUCT_MANAGEMENT", "delete")){
                                    ?>
                                        action_header +=    '<li><a onclick="$(\'#confirmation-delete\').modal(\'toggle\');$(\'#confirmation-delete #delete-url\').val(\'<?php echo base_url("administrator_area/product/delete");?>/'+row["id"]+'\');"><i class="fa fa-trash"></i> Delete record </a></li>';
                                    <?php
                                    }else{
                                    ?>
                                        action_header +=    '<li><a onclick="$(\'#permission-denied\').modal(\'toggle\');"><i class="fa fa-trash"></i> Delete record </a></li>';
                                    <?php
                                    }
                                    ?>
                                    action_header += '</ul>'+
                                    '</div>';
                                return action_header;
                            },
                            searchable: false,
                            orderable: false
                        }
                    ]
                });
                $('#main-table tbody').on('change', 'input[type="checkbox"]', function(){
                    // If checkbox is not checked
                    if(!this.checked){
                        
                        var el = $('#select-all-checkbox').get(0);
                        // If "Select all" control is checked and has 'indeterminate' property
                        if(el && el.checked && ('indeterminate' in el)){
                            // Set visual state of "Select all" control
                            // as 'indeterminate'
                            el.indeterminate = true;
                        }
                    }
                });
                window.updateCount = function() {
                    var x = $(".c:checked").length;
                    if(x > 0){
                        $('#kt_datatable_selected_records').html(x);
                        $('#kt_datatable_group_action_form').show();  
                    }else{
                        $('#kt_datatable_group_action_form').hide(); 
                    }
                };
                <?php
            }else{
                ?>
                $('#permission-denied').modal('show');
                <?php
            }
            ?>

            $(document).on("click", ".open-import-file", function(){
                $("#import-model input[name=fileproduct]").val('');
                $('#import-model').modal('show');
            });

            $(document).on("click", ".import-button", function(){
                $('#import-model').modal('hide');
                App.blockUI('body');
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url("administrator_area/product/importproduct")?>",
                    data: new FormData($('#import-form')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(response){
                      try {
                        if(response.code == "0x0000-00000") {
                            var table = $("#main-table").DataTable();
                            table.columns().search("").draw();
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "positionClass": "toast-bottom-right",
                                "onclick": null,
                                "showDuration": "1000",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.success('Success', 'Success.');
                        }else{
                          $("#system-return-error .message").html(response.message);
                          $("#system-return-error").modal("toggle");
                        }
                      } catch(e) {
                        $("#system-return-failed").modal("toggle");
                      }
                      App.unblockUI('body');
                    },
                    error: function(){
                      $("#system-disconnected").modal("toggle");
                      App.unblockUI('body');
                    }
                });
            });

            $(document).on("click", ".open-import-technical-file", function(){
                $("#import-technical-model input[name=filetechnical]").val('');
                $('#import-technical-model').modal('show');
            });

            $(document).on("click", ".import-technical-button", function(){
                $('#import-technical-model').modal('hide');
                App.blockUI('body');
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url("administrator_area/product/importtechnical")?>",
                    data: new FormData($('#import-technical-form')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(response){
                      try {
                        if(response.code == "0x0000-00000") {
                            var table = $("#main-table").DataTable();
                            table.columns().search("").draw();
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "positionClass": "toast-bottom-right",
                                "onclick": null,
                                "showDuration": "1000",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            }
                            toastr.success('Correct:'+response.data.correct+"<br>Fail:"+response.data.fail, 'Success.');
                        }else{
                          $("#system-return-error .message").html(response.message);
                          $("#system-return-error").modal("toggle");
                        }
                      } catch(e) {
                        $("#system-return-failed").modal("toggle");
                      }
                      App.unblockUI('body');
                    },
                    error: function(){
                      $("#system-disconnected").modal("toggle");
                      App.unblockUI('body');
                    }
                });
            });
        });
    </script>

    <!-- INCLUDE RAW SCRIPT AREA -->
    <?php $this->load->view("administrator_area/__scripts/Javascript_Datatables",array());?>
    <?php $this->load->view("administrator_area/__scripts/Javascript_Modal",array());?>
    <?php $this->load->view("administrator_area/__scripts/Javascript_Select2",array());?>
</html>

<!-- ------------------------------------------------------------------ -->
<!-- MODAL IS BEGIN                                                     -->
<!-- ------------------------------------------------------------------ -->
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationDelete");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationDeleteAll");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_PermissionDenied");?>

<div class="modal fade" id="import-model" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green">
                        <i class="fa fa-pencil font-green" data-dismiss="modal"></i>
                        <span class="caption-subject bold uppercase">Import Product</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form class="form-horizontal" role="form" method="post" id="import-form" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-md-3 control-label">File<span class="required"> *</span><br>
                                <small style="font-style: normal;color: lightslategray;">(file .xlsx)</small>
                            </label>
                            <div class="col-md-5">
                                <input type="file" class="form-control" name="fileproduct" accept=".xls,.xlsx,.csv" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"></label>
                            <div class="col-md-5">
                                <button type="button"  class="btn green-jungle import-button">Save</button>
                                <button class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="import-technical-model" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green">
                        <i class="fa fa-pencil font-green" data-dismiss="modal"></i>
                        <span class="caption-subject bold uppercase">Import Technical Spec</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form class="form-horizontal" role="form" method="post" id="import-technical-form" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-md-3 control-label">File<span class="required"> *</span><br>
                                <small style="font-style: normal;color: lightslategray;">(file .xlsx)</small>
                            </label>
                            <div class="col-md-5">
                                <input type="file" class="form-control" name="filetechnical" accept=".xls,.xlsx,.csv" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"></label>
                            <div class="col-md-5">
                                <button type="button" class="btn green-jungle import-technical-button">Save</button>
                                <button class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>