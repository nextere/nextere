<!-- ------------------------------------------------------------------ -->
<!-- PERMISSION CHECK IS BEGIN                                          -->
<!-- ------------------------------------------------------------------ -->
<?php
$codeigniter_instance =& get_instance();

$configurations = array(
  "PAGE_TITLE" => "Administrator System",
  "PAGE_HEADER" => array(
   "MAIN_TITLE" => "Product Management",
   "SUB_TITLE" => ""
  ),
  "PORTLET_HEADER" => array(
    "ICON" => "fa fa-bars",
    "TITLE" => "Product Detail"
  )
);
?>
<!-- ------------------------------------------------------------------ -->
<!-- HEADER IS BEGIN                                                    -->
<!-- ------------------------------------------------------------------ -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
  <head>
    <title><?php echo $this->config->item('WEBSITE_NAME');?> | Login System</title>
    <link href="<?php echo faviconDirectory($this->config->item('WEBSITE_FAVICON'));?>" rel="shortcut icon" />

    <!-- META TAG AREA -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- MANDATORY STYLE AREA -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/simple-line-icons/simple-line-icons.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-switch/css/bootstrap-switch.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- PLUGINS AREA -->
    <link href="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/clockface/css/clockface.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2-bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/jstree/dist/themes/default/style.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/dropzone/basic.min.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- THEME STYLE AREA -->
    <link href="<?php echo assetsDirectory("global/css/components-md.min.css");?>" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo assetsDirectory("global/css/plugins-md.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- LAYOUT AREA -->
    <link href="<?php echo assetsDirectory("layouts/layout/css/layout.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("layouts/layout/css/themes/light.min.css");?>" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?php echo assetsDirectory("layouts/layout/css/custom.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- CUSTOM AREA -->
    <style>
      .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice {
        color: #fff;
        background: #53c6d2;
        border: 1px solid #53c6d2;
        margin: 5px 0 0 6px;
        padding: 0 35px 0 6px;
      }
      .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice__remove {
        margin-right: 30px;
        color: #e7505a;
      }
      .product-image {
        height: 150px;
        margin-top: 10px;
      }
    </style>
  </head>

  <!-- ------------------------------------------------------------------ -->
  <!-- BODY IS BEGIN                                                      -->
  <!-- ------------------------------------------------------------------ -->
  <body class="page-header-fixed page-sidebar-closed-hide-logo page-md">
    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueHeader",array());?>
    <div class="page-container">
      <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueSideBar",array());?>
      <div class="page-content-wrapper">
        <div class="page-content">
          <h3 class="page-title">
              <?php echo $configurations["PAGE_HEADER"]["MAIN_TITLE"]?>
              <small>
                  <?php echo $configurations["PAGE_HEADER"]["SUB_TITLE"]?>
              </small>
          </h3>
          <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueBreadCrumb",array());?>

          <!-- ------------------------------------------------------------------ -->
          <!-- CONTENT IS BEGIN                                                   -->
          <!-- ------------------------------------------------------------------ -->
          
          <div class="row">
            <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light bordered">
                <div class="portlet-title">
                  <div class="caption font-green">
                    <i class="<?php echo $configurations["PORTLET_HEADER"]["ICON"]?> font-green"></i>
                    <span class="caption-subject bold uppercase">
                      <?php echo $configurations["PORTLET_HEADER"]["TITLE"]?>
                    </span>
                  </div>
                  <?php if(isset($product->{"product::id"})){ ?>
                    <div class="actions">
                      <a href="<?php echo base_url("administrator_area/product/detail_technical/".$product->{"product::id"})?>" class="btn btn-sm btn-default">Technical Spec</a>
                      <?php if(checkAdministratorPermission("PRODUCT_MANAGEMENT", "edit")){
                      ?>
                        <a href="<?php echo base_url("administrator_area/product/edit/".$product->{"product::id"})?>" class="btn btn-sm btn-warning">Edit</a>
                      <?php
                      }
                      ?>
                    </div>
                  <?php } ?>

                </div>
                <div class="portlet-body">
                  <form class="form-horizontal" role="form" method="post" id="main-form" enctype="multipart/form-data"> 
                    <input type="hidden" name="session_time" value="<?php echo date("YmdHis")?>">
                    <div class="form-group">
                      <label class="col-md-2 control-label">Code :</label>
                      <div class="col-md-3">
                        <p class="form-control-static"><?php echo $product->{"product::code"} ?></p>
                      </div>
                      <label class="col-md-3 control-label">Brand :</label>
                      <div class="col-md-3">
                        <p class="form-control-static">
                          <?php if(isset($product->{"brand::id"})) echo $product->{"brand::name_en"} ?>
                        </p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Name (TH) :</label>
                      <div class="col-md-3">
                        <p class="form-control-static"><?php echo $product->{"product::name_th"} ?></p>
                      </div>
                      <label class="col-md-3 control-label">Name (EN) :</label>
                      <div class="col-md-3">
                        <p class="form-control-static"><?php echo $product->{"product::name_en"} ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Model (TH) :</label>
                      <div class="col-md-3">
                        <p class="form-control-static"><?php echo $product->{"product::model_name_th"} ?></p>
                      </div>
                      <label class="col-md-3 control-label">Model (EN) :</label>
                      <div class="col-md-3">
                        <p class="form-control-static"><?php echo $product->{"product::model_name_en"} ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Type :</label>
                      <div class="col-md-3">
                        <p class="form-control-static"><?php if(isset($product->{"product_type::id"})) echo $product->{"product_type::name_en"} ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Detail (TH) :</label>
                      <div class="col-md-9">
                        <p class="form-control-static"><?php echo $product->{"product::detail_th"} ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Detail (EN) :</label>
                      <div class="col-md-9">
                        <p class="form-control-static"><?php echo $product->{"product::detail_en"} ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Description (TH) :</label>
                      <div class="col-md-9">
                        <p class="form-control-static"><?php echo $product->{"product::description_th"} ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Description (EN) :</label>
                      <div class="col-md-9">
                        <p class="form-control-static"><?php echo $product->{"product::description_en"} ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Basic Quality (TH) :</label>
                      <div class="col-md-9">
                        <p class="form-control-static"><?php echo $product->{"product::basic_quality_th"} ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Basic Quality (EN) :</label>
                      <div class="col-md-9">
                        <p class="form-control-static"><?php echo $product->{"product::basic_quality_en"} ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Retail Price :</label>
                      <div class="col-md-3">
                        <p class="form-control-static"><?php echo $product->{"product::retail_price"} ?></p>
                      </div>
                      <label class="col-md-3 control-label">Waiting Day :<br>
                        <small style="font-style: normal;color: lightslategray;">(if product out of stock)</small>
                      </label>
                      <div class="col-md-3">
                        <p class="form-control-static"><?php echo $product->{"product::waiting_date"} ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Minimum Quantity/Order :</label>
                      <div class="col-md-3">
                        <p class="form-control-static"><?php echo $product->{"product::minimum_product"} ?></p>
                      </div>
                      <label class="col-md-3 control-label">Delivery Price/Qty :</label>
                      <div class="col-md-3">
                        <p class="form-control-static"><?php echo $product->{"product::delivery_price"} ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Condition of Product (TH) :</label>
                      <div class="col-md-9">
                        <p class="form-control-static"><?php echo $product->{"product::condition_product_th"} ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Condition of Product (EN) :</label>
                      <div class="col-md-9">
                        <p class="form-control-static"><?php echo $product->{"product::condition_product_en"} ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Condition of Warranty (TH) :</label>
                      <div class="col-md-9">
                        <p class="form-control-static"><?php echo $product->{"product::condition_warranty_th"} ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Condition of Warranty (EN) :</label>
                      <div class="col-md-9">
                        <p class="form-control-static"><?php echo $product->{"product::condition_warranty_en"} ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">In Stock Qty :</label>
                      <div class="col-md-3">
                        <p class="form-control-static"><?php echo $product->{"product::in_stock_qty"} ?></p>
                      </div>
                      <label class="col-md-3 control-label">Safety Stock :</label>
                      <div class="col-md-3">
                        <p class="form-control-static"><?php echo $product->{"product::safety_stock"} ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Search Keyword :<br>
                        <small style="font-style: normal;color: lightslategray;">Seperate by comma</small>
                      </label>
                      <div class="col-md-9">
                        <p class="form-control-static"><?php echo $product->{"product::search_keyword"} ?></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Image :</label>
                        <?php if($product_images != null && count($product_images) > 0) {
                          foreach ($product_images as $image) {
                        ?>
                            <div class="col-md-3 product-image" style="text-align: center;">
                              <img src="<?php echo uploadsDirectory("product/".$product->{"product::code"}."/".$image)?>"  style="max-width: 100%; max-height: 100%;" >
                            </div>
                        <?php 
                          }
                         } ?>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Recommend :</label>
                      <div class="col-md-3">
                        <div class="mt-checkbox-inline">
                          <label class="mt-checkbox mt-checkbox-outline ">
                            <input type="checkbox" name="is_recommend" disabled <?php if($product->{"product::is_recommend"} == 1) echo "checked";?> >
                            <span></span>
                          </label>
                        </div>
                      </div>
                      <label class="col-md-3 control-label">Best Seller :</label>
                      <div class="col-md-3">
                        <div class="mt-checkbox-inline">
                          <label class="mt-checkbox mt-checkbox-outline ">
                            <input type="checkbox" name="is_bestseller" disabled <?php if($product->{"product::is_bestseller"} == 1) echo "checked";?> >
                            <span></span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">New Product :</label>
                      <div class="col-md-3">
                        <div class="mt-checkbox-inline">
                          <label class="mt-checkbox mt-checkbox-outline ">
                            <input type="checkbox" name="is_new" disabled <?php if($product->{"product::is_new"} == 1) echo "checked";?> >
                            <span></span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Status :</label>
                      <div class="col-md-3">
                        <p class="form-control-static"><?php echo $product->{"product::status"} ?></p>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="portlet light bordered" >
                <div class="portlet-title">
                  <div class="caption font-green" >
                    <span class="caption-subject bold uppercase">
                      Product Feature
                    </span>
                  </div>
                </div>
                <div class="portlet-body">
                  <table class="table table-striped table-bordered table-hover order-column product-feature-list" data-product_id="<?php echo $product->{"product::id"}?>">
                    <thead>
                      <tr>
                        <th style="width:10%;"> Sort</th>
                        <th style="width:20%;"> Title(TH)</th>
                        <th style="width:20%;"> Title(EN)</th>
                        <th style="width:20%;"> Description(TH)</th>
                        <th style="width:20%;"> Description(EN)</th>
                        <th style="width:10%;"> Image </th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="portlet light bordered" >
                <div class="portlet-title">
                  <div class="caption font-green" >
                    <span class="caption-subject bold uppercase">
                      Product Tutorial
                    </span>
                  </div>
                </div>
                <div class="portlet-body">
                  <table class="table table-striped table-bordered table-hover order-column product-tutorial-list" data-product_id="<?php echo $product->{"product::id"}?>">
                    <thead>
                      <tr>
                        <th style="width:10%;"> Sort</th>
                        <th style="width:30%;"> Title(TH)</th>
                        <th style="width:30%;"> Title(EN)</th>
                        <th style="width:30%;"> Link Url</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="portlet light bordered" >
                <div class="portlet-title">
                  <div class="caption font-green" >
                    <span class="caption-subject bold uppercase">
                      Product DataSheet
                    </span>
                  </div>
                </div>
                <div class="portlet-body">
                  <table class="table table-striped table-bordered table-hover order-column product-datasheet-list" data-product_id="<?php echo $product->{"product::id"}?>">
                    <thead>
                      <tr>
                        <th style="width:10%;"> Sort</th>
                        <th style="width:80%;"> Name</th>
                        <th style="width:10%;"> File</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="portlet light bordered" >
                <div class="portlet-title">
                  <div class="caption font-green" >
                    <span class="caption-subject bold uppercase">
                      Price by Quantity
                    </span>
                  </div>
                </div>
                <div class="portlet-body">
                  <table class="table table-striped table-bordered table-hover order-column product-price-list" data-product_id="<?php echo $product->{"product::id"}?>">
                    <thead>
                      <tr>
                        <th style="width:50%;"> Minimun Quantity</th>
                        <th style="width:50%;"> Price </th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="portlet light bordered" >
                <div class="portlet-title">
                  <div class="caption font-green" >
                    <span class="caption-subject bold uppercase">
                      Category
                    </span>
                  </div>
                </div>
                <div class="portlet-body">
                  <table class="table table-striped table-bordered table-hover order-column product-category-list" data-product_id="<?php echo $product->{"product::id"}?>">
                    <thead>
                      <tr>
                        <th style="width:50%;"> Category</th>
                        <th style="width:50%;"> Sub Category </th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="portlet light bordered" >
                <div class="portlet-title">
                  <div class="caption font-green" >
                    <span class="caption-subject bold uppercase">
                      Menu (Solution)
                    </span>
                  </div>
                </div>
                <div class="portlet-body">
                  <table class="table table-striped table-bordered table-hover order-column menu-category-list" data-product_id="<?php echo $product->{"product::id"}?>">
                    <thead>
                      <tr>
                        <th style="width:35%;"> Menu</th>
                        <th style="width:35%;"> Sub Category </th>
                        <th> Child Category </th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="portlet light bordered" >
                <div class="portlet-title">
                  <div class="caption font-green" >
                    <span class="caption-subject bold uppercase">
                      Relate Product
                    </span>
                  </div>
                </div>
                <div class="portlet-body">
                  <table class="table table-striped table-bordered table-hover order-column product-relate-list" data-product_id="<?php echo $product->{"product::id"}?>">
                    <thead>
                      <tr>
                        <th style="width:35%;"> Name</th>
                        <th style="width:35%;"> Model </th>
                        <th> Code</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueFooter",array());?>
  </body>

  <!-- ------------------------------------------------------------------ -->
  <!-- JAVASCRIPT IS BEGIN                                                -->
  <!-- ------------------------------------------------------------------ -->
  <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/respond.min.js');?>"></script> <![endif]-->
  <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/excanvas.min.js');?>"></script> <![endif]-->

  <!-- MANDATORY SCRIPT AREA -->
  <script src="<?php echo assetsDirectory("global/plugins/jquery.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap/js/bootstrap.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/js.cookie.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-slimscroll/jquery.slimscroll.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery.blockui.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-switch/js/bootstrap-switch.min.js");?>" type="text/javascript"></script>

  <!-- PLUGINS AREA -->
  <script src="<?php echo assetsDirectory("global/plugins/jquery-ui/jquery-ui.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/moment.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/clockface/js/clockface.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/select2/js/select2.full.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/jquery.validate.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/additional-methods.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.js");?>"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote-cleaner.js");?>"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jstree/dist/jstree.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/scripts/datatable.js");?>" type="text/javascript"></script>

  <script src="<?php echo assetsDirectory("global/plugins/backstretch/jquery.backstretch.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.js");?>" type="text/javascript"></script>
    

  <!-- THEME SCRIPT AREA -->
  <script src="<?php echo assetsDirectory("global/scripts/app.min.js");?>" type="text/javascript"></script>

  <!-- LAYOUT AREA -->
  <script src="<?php echo assetsDirectory("layouts/layout/scripts/layout.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("layouts/layout/scripts/demo.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("layouts/global/scripts/quick-sidebar.js");?>" type="text/javascript"></script>

  <!-- CUSTOM AREA -->
  <script>
    jQuery(document).ready(function() {
      $(".product-feature-list").dataTable({
        processing: true,
        serverSide: true,
        pageLength: 25,
        searching: false,
        ajax:{
          url: "<?php echo base_url("administrator_area/product/datatables/index_product_feature")?>",
          type: "POST",
          data:{
            product_id: $(".product-feature-list").data("product_id")
          }
        },
        columns: [
          {
            data: 'sort'
          },
          {
            data: 'title_th'
          },
          {
            data: 'title_en'
          },
          {
            data: 'description_th'
          },
          {
            data: 'description_en'
          },
          {
            render: function(data, type, row, meta){
              var action_header = "";
              if(row['image_path'] != ''){
                action_header = "<img src='<?php echo base_url('')?>"+row['image_path']+"' style='max-width: 100%; max-height: 100%;' >";
              }
              return action_header;
            }
          }
        ]
      });

      $(".product-tutorial-list").dataTable({
        processing: true,
        serverSide: true,
        pageLength: 25,
        searching: false,
        ajax:{
          url: "<?php echo base_url("administrator_area/product/datatables/index_product_tutorial")?>",
          type: "POST",
          data:{
            product_id: $(".product-feature-list").data("product_id")
          }
        },
        columns: [
          {
            data: 'sort'
          },
          {
            data: 'title_th'
          },
          {
            data: 'title_en'
          },
          {
            render: function(data, type, row, meta){
              var action_header = "";
              if(row['link_url'] != ''){
                action_header = "<a href='"+row['link_url']+"' target='_blank' >"+row['link_url']+"</a>";
              }
              return action_header;
            }
          }
        ]
      });

      $(".product-datasheet-list").dataTable({
        processing: true,
        serverSide: true,
        pageLength: 25,
        searching: false,
        ajax:{
          url: "<?php echo base_url("administrator_area/product/datatables/index_product_datasheet")?>",
          type: "POST",
          data:{
            product_id: $(".product-datasheet-list").data("product_id")
          }
        },
        columns: [
          {
            data: 'sort'
          },
          {
            data: 'name'
          },
          {
            render: function(data, type, row, meta){
              var action_header = "";
              if(row['file_path'] != ''){
                action_header = "<a href='<?php echo base_url()?>"+row['file_path']+"' target='_blank' ><i class='fa fa-file'></i> file</a>";
              }
              return action_header;
            }
          }
        ]
      });
      
      //Price by Quantity
      $(".product-price-list").dataTable({
        processing: true,
        serverSide: true,
        pageLength: 25,
        searching: false,
        ajax:{
          url: "<?php echo base_url("administrator_area/product/datatables/index_product_price")?>",
          type: "POST",
          data:{
            product_id: $(".product-price-list").data("product_id")
          }
        },
        columns: [
          {
            data: 'minimum_qty'
          },
          {
            data: 'price'
          }
        ]
      });

      $(".menu-category-list").dataTable({
          processing: true,
          serverSide: true,
          pageLength: 25,
          searching: false,
          ajax:{
            url: "<?php echo base_url("administrator_area/product/datatables/index_menu_category")?>",
            type: "POST",
            data:{
              product_id: $(".menu-category-list").data("product_id")
            }
          },
          columns: [
            {
              data: 'menu_category_name'
            },
            {
              data: 'submenu_category_name'
            },
            {
              data: 'child_submenu_category_name'
            }
          ]
      });

      //Category
      $(".product-category-list").dataTable({
          processing: true,
          serverSide: true,
          pageLength: 25,
          searching: false,
          ajax:{
            url: "<?php echo base_url("administrator_area/product/datatables/index_product_category")?>",
            type: "POST",
            data:{
              product_id: $(".product-category-list").data("product_id")
            }
          },
          columns: [
            {
              data: 'product_category_name'
            },
            {
              data: 'product_sub_category_name'
            }
          ]
      });

      $(".product-relate-list").dataTable({
          processing: true,
          serverSide: true,
          pageLength: 25,
          searching: false,
          ajax:{
            url: "<?php echo base_url("administrator_area/product/datatables/index_product_relate")?>",
            type: "POST",
            data:{
              product_id: $(".product-relate-list").data("product_id")
            }
          },
          columns: [
            {
              data: 'product_name'
            },
            {
              data: 'product_model_name'
            },
            {
              data: 'product_code'
            }
          ]
      });


    });
  </script>

  <!-- INCLUDE RAW SCRIPT AREA -->
  <?php $this->load->view("administrator_area/__scripts/Javascript_Modal",array());?>
  <?php $this->load->view("administrator_area/__scripts/Javascript_Select2",array());?>
</html>

<!-- ------------------------------------------------------------------ -->
<!-- MODAL IS BEGIN                                                     -->
<!-- ------------------------------------------------------------------ -->
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationCancel");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationDelete");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationRefresh");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationSave");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationSaveMultiform");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_PermissionDenied");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemDisconnected");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnError");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnFailed");?>
