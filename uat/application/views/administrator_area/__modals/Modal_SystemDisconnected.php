<div class="modal fade" id="system-disconnected" tabindex="-1" data-width="400">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <div class="caption font-red-thunderbird">
                    <i class="fa fa-plug font-red-thunderbird"></i>
                    <span class="caption-subject bold uppercase"> Disconnected from server</span>
                </div>
            </div>
            <div class="modal-body">
                <div style="text-align: center;">
                    <div class="row" style="padding-top: 50px;">
                        <div class="page-spinner-bar-custom">
                            <div class="bounce1"></div>
                            <div class="bounce2"></div>
                            <div class="bounce3"></div>
                            <div class="bounce4"></div>
                            <div class="bounce5"></div>
                        </div>
                    </div>
                    <div class="caption font-red-thunderbird row" style="padding-top: 30px;padding-bottom: 50px;">
                        <i class="fa fa-plug font-red-thunderbird"></i><br>
                        <span class="caption-subject bold uppercase"> Disconnected. Please try again or<br><small>Contact Developer to solve problem</small></span><br><br>
                        <button type="button" class="btn red-thunderbird btn-xs btn-outline" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>