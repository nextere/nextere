<div class="modal fade" id="sendmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-envelope"></i> Sending Email</h5>
      </div>
      <div class="modal-body">
      <form  method="post" id="main-form">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">title:</label>
            <input type="text" name="title" class="form-control" id="title-text">
            <input type="hidden" name="id" class="form-control" id="id_contact" value="">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" name="message" id="message-text"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
      <div style="text-align: center;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="send-button">Send</button>
      </div>
      </div>
    </div>
  </div>
</div>
<script>
    jQuery(document).ready(function() {
        $("#send-button").attr("onclick","$('#main-form').submit();");
    });
</script>