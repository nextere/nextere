<?php
$codeigniter_instance =& get_instance();
?>

<div class="page-footer">
    <div class="page-footer-inner"> <?php echo date("Y")?> &copy; <?php echo $this->config->item('WEBSITE_NAME');?> By <?php echo $this->config->item('WEBSITE_TEAM');?>
        <a href="<?php echo $this->config->item('WEBSITE_REFERENCE');?>" title="<?php echo $this->config->item('WEBSITE_REFERENCE');?>" target="_blank">Join Us!</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>