<?php
$codeigniter_instance =& get_instance();

$administrator_information = $codeigniter_instance->session->userdata("__administrator_information");
?>

<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner ">
        <div class="page-logo">
            <a href="<?php echo base_url("administrator_area/")?>">
                <img src="<?php echo logoDirectory($this->config->item('WEBSITE_LOGO'));?>" alt="logo" class="logo-default swap-logo" style="margin: 5px 0 0;max-height: 40px;"/>
            </a>
            <div class="menu-toggler sidebar-toggler">
                <span></span>
            </div>
        </div>

        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
        </a>

        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="padding: 0px 6px 10px 8px;line-height: 16px;">
                        <span class="username username-hide-on-mobile" style="margin-top: 7px;">
                            @<?php echo $codeigniter_instance->session->userdata("__administrator::username")?> [<?php echo $codeigniter_instance->session->userdata("__administrator_group::name")?>] 
                        </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="<?php echo base_url("administrator_area/Authentication/Logout")?>">
                                <i class="icon-key"></i> Log Out
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="clearfix"> </div>