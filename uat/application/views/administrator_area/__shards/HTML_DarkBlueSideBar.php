<?php
$codeigniter_instance =& get_instance();

$directory = strtolower($codeigniter_instance->router->directory);
$controller = strtolower($codeigniter_instance->router->fetch_class());
$function = strtolower($codeigniter_instance->router->fetch_method());

?>

<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu page-header-fixed" data-keep-expanded="true" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>

            <?php
            /** ======================================================================================
             *      DASHBOARD
            ====================================================================================== **/
            ?>
            <?php
            $menu_active = "";
            $menu_open = "";
            if(in_array($controller, array("home")) && in_array($function, array("index"))){
                $menu_active = "active";
                $menu_open = "open";
            }
            ?>
            <li class="nav-item start <?php echo $menu_active ?> <?php echo $menu_open ?>">
                <a href="<?php echo base_url("administrator_area/home")?>" class="nav-link marquee-menu">
                    <i class="icon-home"></i>
                    <span class="title">Home</span>
                    <span class="selected"></span>
                </a>
            </li>
            <?php
            /** ======================================================================================
             *      ADMINISTRATOR ACCOUNT & PERMISSION
            ====================================================================================== **/
            if(checkAdministratorPermission("ADMINISTRATOR_MANAGEMENT", "index") || checkAdministratorPermission("ADMINISTRATOR_MANAGEMENT", "edit") ||
                checkAdministratorPermission("ADMINISTRATOR_GROUP_MANAGEMENT", "index") || 
                checkAdministratorPermission("ADMINISTRATOR_GROUP_MANAGEMENT", "edit")){
            ?>
            <li class="heading">
                <h3 class="uppercase"><b>Admin Management</b></h3>
            </li>
            <?php
                $header_menu_active = "";
                $header_menu_open = "";
                $header_menu_display = "";
                if((in_array($controller, array("administrator")) && in_array($function, array("index", "edit"))) ||(in_array($controller, array("administrator_group")) && in_array($function, array("index", "edit"))) ){
                    $header_menu_active = "active";
                    $header_menu_open = "open";
                    $header_menu_display = "display:block";
                }
            ?>
                <li class="nav-item <?php echo $header_menu_active?> <?php echo $header_menu_open?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon icon-user"></i>
                        <span class="title">Admin Permission</span>
                        <span class="selected"></span>
                        <span class="arrow <?php echo $header_menu_open?>"></span>
                    </a>
                    <ul class="sub-menu" style="<?php echo $header_menu_display?>">
                        <?php 
                        if(checkAdministratorPermission("ADMINISTRATOR_MANAGEMENT", "index") || checkAdministratorPermission("ADMINISTRATOR_MANAGEMENT", "edit")){
                        
                            if(in_array($controller, array("administrator")) && in_array($function, array("index", "edit"))){
                                $menu_active = "active";
                                $menu_open = "";
                            }
                            ?>
                            <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                                <a class="nav-link marquee-menu" href="<?php echo base_url("administrator_area/administrator")?>">
                                    <span class="title">User</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                        <?php
                        }

                        if(checkAdministratorPermission("ADMINISTRATOR_GROUP_MANAGEMENT", "index") || checkAdministratorPermission("ADMINISTRATOR_GROUP_MANAGEMENT", "edit")){
                            $menu_active = "";
                            if(in_array($controller, array("administrator_group")) && in_array($function, array("index", "edit"))){
                                $menu_active = "active";
                                $menu_open = "open";
                            }
                        ?>
                            <li class="nav-item start <?php echo $menu_active ?> <?php echo $menu_open ?>">
                                <a class="nav-link marquee-menu" href="<?php echo base_url("administrator_area/administrator_group")?>">
                                    <span class="title">Role&Permission</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                        <?php 
                        }
                        ?>
                    </ul>
                </li>

            <?php
            }
            ?>
            <?php 
            if(checkAdministratorPermission("REPORT", "index") || checkAdministratorPermission("REPORT", "edit")){
                $header_menu_active = "";
                $header_menu_open = "";
                $header_menu_display = "";
                if((in_array($controller, array("report")) && in_array($function, array("product_sales"))) ){
                    $header_menu_active = "active";
                    $header_menu_open = "open";
                    $header_menu_display = "display:block";
                }
            ?>
                <li class="heading">
                    <h3 class="uppercase"><b>Report</b></h3>
                </li>
                <?php 
                if(checkAdministratorPermission("REPORT", "index") || checkAdministratorPermission("REPORT", "edit")){
                    $menu_active = "";
                    $menu_open = "";
                    if(in_array($controller, array("report")) && in_array($function, array("dash"))){
                        $menu_active = "active";
                        $menu_open = "open";
                    }
                    ?>
                    <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                        <a href="<?php echo base_url("administrator_area/report/dash")?>" class="nav-link marquee-menu">
                            <i class="fa fa-bar-chart"></i>
                            <span class="title">Report dashboard</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                <?php 
                }
                ?>
                
                <?php 
                if(checkAdministratorPermission("REPORT", "index") || checkAdministratorPermission("REPORT", "edit")){
                    $menu_active = "";
                    $menu_open = "";
                    if(in_array($controller, array("report")) && in_array($function, array("product_sales"))){
                        $menu_active = "active";
                        $menu_open = "open";
                    }
                    ?>
                    <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                        <a href="<?php echo base_url("administrator_area/report/product_sales")?>" class="nav-link marquee-menu">
                            <i class="fa fa-bar-chart"></i>
                            <span class="title">Report sale of product</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                <?php 
                }
                ?>
                <?php 
                if(checkAdministratorPermission("REPORT", "index") || checkAdministratorPermission("REPORT", "edit")){
                    $menu_active = "";
                    $menu_open = "";
                    if(in_array($controller, array("report")) && in_array($function, array("product_order"))){
                        $menu_active = "active";
                        $menu_open = "open";
                    }
                    ?>
                    <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                        <a href="<?php echo base_url("administrator_area/report/product_order")?>" class="nav-link marquee-menu">
                            <i class="fa fa-bar-chart"></i>
                            <span class="title">Report order of product</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                <?php 
                }
                ?>

            <?php
            }
            ?>
            <li class="heading">
                <h3 class="uppercase"><b>Website Management</b></h3>
            </li>
            <li class="heading">
                <h3 class="uppercase"><b>Home Page</b></h3>
            </li>
            <?php
            if(checkAdministratorPermission("BANNER", "index") || checkAdministratorPermission("BANNER", "edit")){
                $menu_active = "";
                $menu_open = "";
                if(in_array($controller, array("banner")) && in_array($function, array("index", "edit", "detail"))){
                    $menu_active = "active";
                    $menu_open = "open";
                }
                ?>
                <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                    <a href="<?php echo base_url("administrator_area/banner")?>" class="nav-link marquee-menu">
                        <i class="fa fa-bars"></i>
                        <span class="title">Banner</span>
                        <span class="selected"></span>
                    </a>
                </li>
           <?php 
            }
            if(checkAdministratorPermission("PARTNER", "index") || checkAdministratorPermission("PARTNER", "edit")){
                $menu_active = "";
                $menu_open = "";
                if(in_array($controller, array("partner")) && in_array($function, array("index", "edit"))){
                    $menu_active = "active";
                    $menu_open = "open";
                }
                ?>
                <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                    <a href="<?php echo base_url("administrator_area/partner")?>" class="nav-link marquee-menu">
                        <i class="fa fa-bars"></i>
                        <span class="title">Partner</span>
                        <span class="selected"></span>
                    </a>
                </li>
           <?php 
            }
            if(checkAdministratorPermission("LANDING_PAGE", "index") || checkAdministratorPermission("LANDING_PAGE", "edit")){
                $menu_active = "";
                $menu_open = "";
                if(in_array($controller, array("landing_page")) && in_array($function, array( "edit"))){
                    $menu_active = "active";
                    $menu_open = "open";
                }
                ?>
                <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                    <a href="<?php echo base_url("administrator_area/landing_page")?>" class="nav-link marquee-menu">
                        <i class="fa fa-bars"></i>
                        <span class="title">Landing page</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php 
            }
            ?>
            <li class="heading">
                <h3 class="uppercase"><b>Other Page</b></h3>
            </li>
            <?php
            if(checkAdministratorPermission("ARTICLE", "index") || checkAdministratorPermission("ARTICLE", "edit")){
                $header_menu_active = "";
                $header_menu_open = "";
                $header_menu_display = "";
                if((in_array($controller, array("article")) && in_array($function, array("index", "edit", "detail"))) ||(in_array($controller, array("article_banner")) && in_array($function, array("index", "detail","edit"))) ){
                    $header_menu_active = "active";
                    $header_menu_open = "open";
                    $header_menu_display = "display:block";
                }
            ?>
                <li class="nav-item <?php echo $header_menu_active?> <?php echo $header_menu_open?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-bars"></i>
                        <span class="title">Article</span>
                        <span class="selected"></span>
                        <span class="arrow <?php echo $header_menu_open?>"></span>
                    </a>
                    <ul class="sub-menu" style="<?php echo $header_menu_display?>">
                        <?php if(checkAdministratorPermission("ARTICLE", "index") || checkAdministratorPermission("ARTICLE", "edit")){
                            $menu_active = "";
                            $menu_open = "";
                            if(in_array($controller, array("article")) && in_array($function, array("index", "edit", "detail"))){
                                $menu_active = "active";
                                $menu_open = "open";
                            }
                            ?>
                            <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                                <a href="<?php echo base_url("administrator_area/article")?>" class="nav-link marquee-menu">
                                    
                                    <span class="title">Article</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                       <?php 
                        }
                        if(checkAdministratorPermission("ARTICLE", "index") || checkAdministratorPermission("ARTICLE", "edit")){
                            $menu_active = "";
                            $menu_open = "";
                            if(in_array($controller, array("article_banner")) && in_array($function, array("index", "edit", "detail"))){
                                $menu_active = "active";
                                $menu_open = "open";
                            }
                            ?>
                            <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                                <a href="<?php echo base_url("administrator_area/article_banner")?>" class="nav-link marquee-menu">
                                    
                                    <span class="title">Article Banner</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                       <?php 
                        } ?>
                    </ul>
                </li>

            <?php
            }
            if(checkAdministratorPermission("CONTACTUS", "index") || checkAdministratorPermission("CONTACTUS", "edit") ||
                checkAdministratorPermission("WHYUS", "index") || checkAdministratorPermission("WHYUS", "edit")){
                $header_menu_active = "";
                $header_menu_open = "";
                $header_menu_display = "";
                if((in_array($controller, array("contact_us")) && in_array($function, array("index", "edit"))) ||(in_array($controller, array("whyus")) && in_array($function, array("index", "edit","detail"))) 
                    ||(in_array($controller, array("aboutus_banner")) && in_array($function, array("index", "edit","detail")))){
                    $header_menu_active = "active";
                    $header_menu_open = "open";
                    $header_menu_display = "display:block";
                }
            ?>
                <li class="nav-item <?php echo $header_menu_active?> <?php echo $header_menu_open?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-bars"></i>
                        <span class="title">About Us</span>
                        <span class="selected"></span>
                        <span class="arrow <?php echo $header_menu_open?>"></span>
                    </a>
                    <ul class="sub-menu" style="<?php echo $header_menu_display?>">
                        <?php 
                        if(checkAdministratorPermission("CONTACTUS", "index") || checkAdministratorPermission("CONTACTUS", "edit")){
                            $menu_active = "";
                            $menu_open = "";
                            if(in_array($controller, array("contact_us")) && in_array($function, array("index", "edit"))){
                                $menu_active = "active";
                                $menu_open = "open";
                            }
                            ?>
                            <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                                <a href="<?php echo base_url("administrator_area/contact_us")?>" class="nav-link marquee-menu">
                                    <span class="title">Contact Us</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                       <?php 
                        }
                        if(checkAdministratorPermission("ABOUTUS", "index") || checkAdministratorPermission("ABOUTUS", "edit")){
                            $menu_active = "";
                            $menu_open = "";
                            if(in_array($controller, array("aboutus_banner")) && in_array($function, array("index", "edit"))){
                                $menu_active = "active";
                                $menu_open = "open";
                            }
                            ?>
                            <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                                <a href="<?php echo base_url("administrator_area/aboutus_banner")?>" class="nav-link marquee-menu">
                                    <span class="title">Banner of About Us</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                       <?php 
                        }
                        if(checkAdministratorPermission("WHYUS", "index") || checkAdministratorPermission("WHYUS", "edit")){
                            $menu_active = "";
                            $menu_open = "";
                            if(in_array($controller, array("whyus")) && in_array($function, array("index", "edit","detail"))){
                                $menu_active = "active";
                                $menu_open = "open";
                            }
                            ?>
                            <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                                <a href="<?php echo base_url("administrator_area/whyus")?>" class="nav-link marquee-menu">
                                    <span class="title">Why Us</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                       <?php 
                        }
                        ?>
                    </ul>
                </li>
           <?php 
            }          
            if(checkAdministratorPermission("SUBSCRIBE", "index") || checkAdministratorPermission("SUBSCRIBE", "edit") ||
                checkAdministratorPermission("NEW", "index") || checkAdministratorPermission("NEW", "edit")){
                $header_menu_active = "";
                $header_menu_open = "";
                $header_menu_display = "";
                if((in_array($controller, array("subscribe")) && in_array($function, array("index", "edit"))) ||(in_array($controller, array("news")) && in_array($function, array("index", "edit"))) ){
                    $header_menu_active = "active";
                    $header_menu_open = "open";
                    $header_menu_display = "display:block";
                }
            ?>
                <li class="nav-item <?php echo $header_menu_active?> <?php echo $header_menu_open?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-bars"></i>
                        <span class="title">Subscribe</span>
                        <span class="selected"></span>
                        <span class="arrow <?php echo $header_menu_open?>"></span>
                    </a>
                    <ul class="sub-menu" style="<?php echo $header_menu_display?>">
                        <?php 
                        if(checkAdministratorPermission("SUBSCRIBE", "index") || checkAdministratorPermission("SUBSCRIBE", "edit")){
                        
                            if(in_array($controller, array("subscribe")) && in_array($function, array("index", "edit"))){
                                $menu_active = "active";
                                $menu_open = "";
                            }
                            ?>
                            <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                                <a class="nav-link marquee-menu" href="<?php echo base_url("administrator_area/subscribe")?>">
                                    <span class="title">Subscribe User</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                        <?php
                        }

                        if(checkAdministratorPermission("NEWS", "index") || checkAdministratorPermission("NEWS", "edit")){
                            $menu_active = "";
                            if(in_array($controller, array("news")) && in_array($function, array("index", "edit"))){
                                $menu_active = "active";
                                $menu_open = "open";
                            }
                        ?>
                            <li class="nav-item start <?php echo $menu_active ?> <?php echo $menu_open ?>">
                                <a class="nav-link marquee-menu" href="<?php echo base_url("administrator_area/news")?>">
                                    <span class="title">News</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                        <?php 
                        }
                        ?>
                    </ul>
                </li>
           <?php 
            } if(checkAdministratorPermission("BUYCONDITION", "index") || checkAdministratorPermission("BUYCONDITION", "edit")){
                $menu_active = "";
                $menu_open = "";
                if(in_array($controller, array("buy_condition")) && in_array($function, array("index", "edit","detail"))){
                    $menu_active = "active";
                    $menu_open = "open";
                }
                ?>
                <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                    <a href="<?php echo base_url("administrator_area/buy_condition")?>" class="nav-link marquee-menu">
                        <i class="fa fa-bars"></i>
                        <span class="title">Buy Condition</span>
                        <span class="selected"></span>
                    </a>
                </li>
           <?php 
            }
            ?>
            <?php 
            if(checkAdministratorPermission("LINK", "index") || checkAdministratorPermission("LINK", "edit")){
                $menu_active = "";
                $menu_open = "";
                if(in_array($controller, array("link")) && in_array($function, array("index", "edit","detail"))){
                    $menu_active = "active";
                    $menu_open = "open";
                }
                ?>
                <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                    <a href="<?php echo base_url("administrator_area/link")?>" class="nav-link marquee-menu">
                        <i class="fa fa-bars"></i>
                        <span class="title">Link</span>
                        <span class="selected"></span>
                    </a>
                </li>
           <?php 
            }
            ?>
            <li class="heading">
                <h3 class="uppercase"><b>Member</b></h3>
            </li>
            <?php
            if(checkAdministratorPermission("MEMBER_MENAGEMENT", "index") || checkAdministratorPermission("MEMBER_MENAGEMENT", "edit")){
                $menu_active = "";
                $menu_open = "";
                if(in_array($controller, array("member")) && in_array($function, array("index", "edit", "detail"))){
                    $menu_active = "active";
                    $menu_open = "open";
                }
                ?>
                <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                    <a href="<?php echo base_url("administrator_area/member")?>" class="nav-link marquee-menu">
                        <i class="fa fa-bars"></i>
                        <span class="title">Member</span>
                        <span class="selected"></span>
                    </a>
                </li>
           <?php 
            }
            if(checkAdministratorPermission("CONTACT_TITLE", "index") || checkAdministratorPermission("CONTACT_TITLE", "edit") ||
                checkAdministratorPermission("CONTACT_MANAGEMENT", "index") || checkAdministratorPermission("CONTACT_MANAGEMENT", "edit")){
                $header_menu_active = "";
                $header_menu_open = "";
                $header_menu_display = "";
                if((in_array($controller, array("contact_title")) && in_array($function, array("index", "edit"))) ||(in_array($controller, array("contact_management")) && in_array($function, array("index", "detail"))) ){
                    $header_menu_active = "active";
                    $header_menu_open = "open";
                    $header_menu_display = "display:block";
                }
            ?>
                <li class="nav-item <?php echo $header_menu_active?> <?php echo $header_menu_open?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-bars"></i>
                        <span class="title">Contact</span>
                        <span class="selected"></span>
                        <span class="arrow <?php echo $header_menu_open?>"></span>
                    </a>
                    <ul class="sub-menu" style="<?php echo $header_menu_display?>">
                        <?php 
                        if(checkAdministratorPermission("CONTACT_TITLE", "index") || checkAdministratorPermission("CONTACT_TITLE", "edit")){
                        
                            if(in_array($controller, array("contact_title")) && in_array($function, array("index", "edit"))){
                                $menu_active = "active";
                                $menu_open = "";
                            }
                            ?>
                            <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                                <a class="nav-link marquee-menu" href="<?php echo base_url("administrator_area/contact_title")?>">
                                    <span class="title">Contact Title</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                        <?php
                        }

                        if(checkAdministratorPermission("CONTACT_MANAGEMENT", "index") || checkAdministratorPermission("CONTACT_MANAGEMENT", "detail")){
                            $menu_active = "";
                            if(in_array($controller, array("contact_management")) && in_array($function, array("index", "detail"))){
                                $menu_active = "active";
                                $menu_open = "open";
                            }
                        ?>
                            <li class="nav-item start <?php echo $menu_active ?> <?php echo $menu_open ?>">
                                <a class="nav-link marquee-menu" href="<?php echo base_url("administrator_area/contact_management")?>">
                                    <span class="title">Contact Management</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                        <?php 
                        }
                        ?>
                    </ul>
                </li>

            <?php
            }
            if(checkAdministratorPermission("PURCHASE_TRANSATION", "index") || checkAdministratorPermission("PURCHASE_TRANSATION", "edit")){
                $menu_active = "";
                $menu_open = "";
                if(in_array($controller, array("purchase_transaction")) && in_array($function, array("index", "edit","detail"))){
                    $menu_active = "active";
                    $menu_open = "open";
                }
                ?>
                <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                    <a href="<?php echo base_url("administrator_area/purchase_transaction")?>" class="nav-link marquee-menu">
                        <i class="fa fa-bars"></i>
                        <span class="title">Purchase transaction</span>
                        <span class="selected"></span>
                    </a>
                </li>
           <?php 
            }
            if(checkAdministratorPermission("QUOTATION", "index") || checkAdministratorPermission("QUOTATION", "edit")){
                $menu_active = "";
                $menu_open = "";
                if(in_array($controller, array("quotation")) && in_array($function, array("index", "edit", "detail"))){
                    $menu_active = "active";
                    $menu_open = "open";
                }
                ?>
                <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                    <a href="<?php echo base_url("administrator_area/quotation")?>" class="nav-link marquee-menu">
                        <i class="fa fa-bars"></i>
                        <span class="title">Quotation</span>
                        <span class="selected"></span>
                    </a>
                </li>
           <?php 
            }
            ?>
            <li class="heading">
                <h3 class="uppercase"><b>Product</b></h3>
            </li>
            <?php
            if(checkAdministratorPermission("MENU_CATEGORY", "index") || checkAdministratorPermission("MENU_CATEGORY", "edit")){
                $menu_active = "";
                $menu_open = "";
                if(in_array($controller, array("menu_category")) && in_array($function, array("index", "edit", "submenu","submenu_edit", "childsubmenu","childsubmenu_edit"))){
                    $menu_active = "active";
                    $menu_open = "open";
                }
                ?>
                <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                    <a href="<?php echo base_url("administrator_area/menu_category")?>" class="nav-link marquee-menu">
                        <i class="fa fa-bars"></i>
                        <span class="title">Solution Category</span>
                        <span class="selected"></span>
                    </a>
                </li>
           <?php 
            } 
            if(checkAdministratorPermission("PRODUCT_CATEGORY", "index") || checkAdministratorPermission("PRODUCT_CATEGORY", "edit")){
                $menu_active = "";
                $menu_open = "";
                if(in_array($controller, array("product_category")) && in_array($function, array("index", "edit", "subcategory","subcategory_edit", "childsubcategory","childsubcategory_edit","technicalspec", "technicalspec_add"))){
                    $menu_active = "active";
                    $menu_open = "open";
                }
                ?>
                <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                    <a href="<?php echo base_url("administrator_area/product_category")?>" class="nav-link marquee-menu">
                        <i class="fa fa-bars"></i>
                        <span class="title">Product Category</span>
                        <span class="selected"></span>
                    </a>
                </li>
           <?php 
            } 
            if(checkAdministratorPermission("TECHNICAL_SPEC", "index") || checkAdministratorPermission("TECHNICAL_SPEC", "edit")){
                $menu_active = "";
                $menu_open = "";
                if(in_array($controller, array("technical_spec")) && in_array($function, array("index", "edit"))){
                    $menu_active = "active";
                    $menu_open = "open";
                }
                ?>
                <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                    <a href="<?php echo base_url("administrator_area/technical_spec")?>" class="nav-link marquee-menu">
                        <i class="fa fa-bars"></i>
                        <span class="title">Technical Spec</span>
                        <span class="selected"></span>
                    </a>
                </li>
           <?php 
            } 
            if(checkAdministratorPermission("PRODUCT_TYPE", "index") || checkAdministratorPermission("PRODUCT_TYPE", "edit")){
                $menu_active = "";
                $menu_open = "";
                if(in_array($controller, array("product_type")) && in_array($function, array("index", "edit"))){
                    $menu_active = "active";
                    $menu_open = "open";
                }
                ?>
                <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                    <a href="<?php echo base_url("administrator_area/product_type")?>" class="nav-link marquee-menu">
                        <i class="fa fa-bars"></i>
                        <span class="title">Product Type</span>
                        <span class="selected"></span>
                    </a>
                </li>
           <?php 
            } 
            if(checkAdministratorPermission("BRAND", "index") || checkAdministratorPermission("BRAND", "edit")){
                $menu_active = "";
                $menu_open = "";
                if(in_array($controller, array("brand")) && in_array($function, array("index", "edit"))){
                    $menu_active = "active";
                    $menu_open = "open";
                }
                ?>
                <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                    <a href="<?php echo base_url("administrator_area/brand")?>" class="nav-link marquee-menu">
                        <i class="fa fa-bars"></i>
                        <span class="title">Brand</span>
                        <span class="selected"></span>
                    </a>
                </li>
           <?php 
            } 
            if(checkAdministratorPermission("PRODUCT_MANAGEMENT", "index") || checkAdministratorPermission("PRODUCT_MANAGEMENT", "edit")){
                $menu_active = "";
                $menu_open = "";
                if(in_array($controller, array("product")) && in_array($function, array("index", "edit", "detail", "edit_techical", "detail_technical"))){
                    $menu_active = "active";
                    $menu_open = "open";
                }
                ?>
                <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                    <a href="<?php echo base_url("administrator_area/product")?>" class="nav-link marquee-menu">
                        <i class="fa fa-bars"></i>
                        <span class="title">Product</span>
                        <span class="selected"></span>
                    </a>
                </li>
           <?php 
            } 
            if(checkAdministratorPermission("BUSINESS_TYPE", "index") || checkAdministratorPermission("BUSINESS_TYPE", "edit")){
                $menu_active = "";
                $menu_open = "";
                if(in_array($controller, array("business_type")) && in_array($function, array("index", "edit", "detail"))){
                    $menu_active = "active";
                    $menu_open = "open";
                }
                ?>
                <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                    <a href="<?php echo base_url("administrator_area/business_type")?>" class="nav-link marquee-menu">
                        <i class="fa fa-bars"></i>
                        <span class="title">Business Type</span>
                        <span class="selected"></span>
                    </a>
                </li>
           <?php 
            }
            if(checkAdministratorPermission("PRICE_RATE", "index") || checkAdministratorPermission("PRICE_RATE", "edit")){
                $menu_active = "";
                $menu_open = "";
                if(in_array($controller, array("price_rate")) && in_array($function, array("index", "edit", "detail"))){
                    $menu_active = "active";
                    $menu_open = "open";
                }
                ?>
                <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                    <a href="<?php echo base_url("administrator_area/price_rate")?>" class="nav-link marquee-menu">
                        <i class="fa fa-bars"></i>
                        <span class="title">Price Rate</span>
                        <span class="selected"></span>
                    </a>
                </li>
            <?php 
            }
            if(checkAdministratorPermission("PROMOTION_DISCOUNT", "index") || checkAdministratorPermission("PROMOTION_DISCOUNT", "edit") ||
                checkAdministratorPermission("COUPON", "index") || checkAdministratorPermission("COUPON", "edit")){
                $header_menu_active = "";
                $header_menu_open = "";
                $header_menu_display = "";
                if((in_array($controller, array("promotion_discount")) && in_array($function, array("index", "edit","detail"))) ||(in_array($controller, array("coupon")) && in_array($function, array("index", "edit","detail"))) ){
                    $header_menu_active = "active";
                    $header_menu_open = "open";
                    $header_menu_display = "display:block";
                }
            ?>
                <li class="nav-item <?php echo $header_menu_active?> <?php echo $header_menu_open?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-bars"></i>
                        <span class="title">Discount</span>
                        <span class="selected"></span>
                        <span class="arrow <?php echo $header_menu_open?>"></span>
                    </a>
                    <ul class="sub-menu" style="<?php echo $header_menu_display?>">
                        <?php 
                        if(checkAdministratorPermission("PROMOTION_DISCOUNT", "index") || checkAdministratorPermission("PROMOTION_DISCOUNT", "edit")){
                            $menu_active = "";
                            $menu_open = "";
                            if(in_array($controller, array("promotion_discount")) && in_array($function, array("index", "edit","detail"))){
                                $menu_active = "active";
                                $menu_open = "open";
                            }
                            ?>
                            <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                                <a href="<?php echo base_url("administrator_area/promotion_discount")?>" class="nav-link marquee-menu">
                                    <span class="title">Promotion Discount</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                       <?php 
                        }
                        if(checkAdministratorPermission("COUPON", "index") || checkAdministratorPermission("COUPON", "edit")){
                            $menu_active = "";
                            $menu_open = "";
                            if(in_array($controller, array("coupon")) && in_array($function, array("index", "edit","detail"))){
                                $menu_active = "active";
                                $menu_open = "open";
                            }
                            ?>
                            <li class="nav-item <?php echo $menu_active ?> <?php echo $menu_open ?>">
                                <a href="<?php echo base_url("administrator_area/coupon")?>" class="nav-link marquee-menu">
                                    <span class="title">Coupon</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                       <?php 
                        }
                        ?>
                    </ul>
                </li>
           <?php 
            } 
            ?>
        </ul>
    </div>
</div>