<?php include('header.php') ?>


<style>
    .delivery ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 100%;
        background-color: white;
        border: 1px solid #BCBCBC;
        opacity: 1;
        font-size: 16px;
    }

    .delivery li a.active {
        background-color: #BCBCBC;
        color: white;
    }

    .delivery li a {
        display: block;
        color: #000;
        padding: 8px 12px;
        text-decoration: none;
        font-size: 14px;
    }

    input[type=text],
    select,
    textarea {
        padding: 12px;
        margin-top: 6px;
        margin-bottom: 16px;
        width: 100%;
        height: 45px;
        border: 1px solid #707070;
        border-radius: 5px;
    }

    input[type=tel],
    select,
    textarea {
        padding: 12px;
        margin-top: 6px;
        margin-bottom: 16px;
        width: 100%;
        height: 45px;
        border: 1px solid #707070;
        border-radius: 5px;
    }

    .form-control {
        width: 100%;
        height: 45px;
        border: 1px solid #707070;
        border-radius: 5px;
    }
</style>




<body>
    <section class="delivery">
        <div class="container mt-5">
            <div class="row">
                <div class="col-sm-12 col-xl-12 pt-5 pb-5 pl-5" style="background-color: lightgray; text-align:left">
                    <h4><b>บัญชีของฉัน</b></h4>
                    <p>สวัสดี Lorem Ipsum, คุณสามารถจัดการข้อมูลส่วนตัวและดูคําสั่งซื้อย้อนหลังได้</p>
                </div>
                <div class="col-sm-2 col-xl-2 mt-3 pl-0">
                    <ul>
                        <li><a href="<?php echo base_url('Account/profile') ?>"><i class="far fa-user pr-2"></i>โปรไฟล์ของฉัน</a></li>
                        <li><a href="<?php echo base_url('Account/order_history') ?>"><i class="fas fa-shopping-cart pr-2"></i>ประวัติการสั่งซื้อ</a></li>
                        <li><a href="<?php echo base_url('Account/quotation_request_history') ?>"><i class="fas fa-file-alt pr-2"></i>ประวัติการขอใบเสนอราคา</a></li>
                        <li><a class="active" href="<?php echo base_url('Account/sending_address') ?>"><i class="fas fa-truck pr-2"></i>ที่อยู่การสั่งสินค้า</a></li>
                        <li><a href="<?php echo base_url('Account/tax_invoice_address') ?>"><i class="fas fa-map-marked-alt pr-2"></i>ที่อยู่ใบกำกับภาษี</a></li>
                        <li><a href="<?php echo base_url('Account/payment') ?>"><i class="far fa-credit-card pr-2"></i>บัตรเครดิต/เดบิต</a></li>
                        <li><a href="<?php echo base_url('Account/datasheet') ?>"><i class="fas fa-file-alt pr-2"></i>รายการ Datasheet</a></li>
                    </ul>
                </div>
                <div class="col-sm-2 col-xl-10 mt-3">

                    <div class="head">
                        <h3 style="font-weight: bold">เพิ่มที่อยู่จัดส่งสินค้า</h3>
                    </div>
                    <hr style="border-width: 3px;">
                    <form id="dataForm">
                        <div class="row">
                            <div class="col-xl-6 name pt-5">
                                <b>ชื่อ *</b>
                                <input type="text" id="name" name="name" placeholder="ชื่อ" required>
                            </div>

                            <div class="col-xl-6 lastname pt-5">
                                <b>นามสกุล *</b>
                                <input type="text" id="lastname" name="lastname" placeholder="นามสกุล" required>
                            </div>

                            <div class="col-xl-6 phone pt-5">
                                <b>เบอร์โทร *</b>
                                <input type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" id="telephone" name="telephone" placeholder="เบอร์โทร" required>
                            </div>

                            <div class="col-xl-6 email pt-5">
                                <b>อีเมล *</b>
                                <input type="text" id="email" name="email" placeholder="อีเมล" required>
                            </div>

                            <div class="col-xl-6 company pt-5">
                                <b>บริษัท</b>
                                <input type="text" id="company" name="company" placeholder="บริษัท">
                            </div>

                            <div class="col-xl-6 branch pt-5">
                                <b>สาขา</b>
                                <input type="text" id="branch" name="branch" placeholder="สาขา">
                            </div>

                            <div class="col-xl-6 address pt-5">
                                <b>ที่อยู่ *</b>
                                <input type="text" id="address" name="address" placeholder="เลขที่อยู่/อาคาร/ถนน">
                            </div>

                            <div class="col-xl-6 province pt-5">
                                <b>จังหวัด *</b>
                                <!-- <input type="text" id="province" name="province" placeholder="จังหวัด"> -->

                                <select class="form-control select" name="province">
                                    <?php
                                    if (isset($address_province->id)) {
                                    ?>
                                        <option value="<?php echo $address_province->id ?>" selected><?php echo $address_province->name ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>

                                <select class="form-control select" name="province">
                                    <?php foreach ($address_province as $key => $address_province_item) { ?>
                                        <option value="<?php echo $address_province_item->id ?>" selected><?php echo $address_province_item->name ?></option>
                                    <?php } ?>
                                </select>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">จังหวัด<span class="required"> *</span></label>
                                    <div class="col-md-4">
                                        <select class="form-control select2" name="address_province_id">
                                            <?php
                                            if (isset($customer->{"customer::address_province_id"})) {
                                            ?>
                                                <option value="<?php echo $customer->{"address_province::id"} ?>" selected><?php echo $customer->{"address_province::name"} ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="col-xl-6 district pt-5">
                                <b>อำเภอ/เขต *</b>
                                <!-- <input type="text" id="district" name="district" placeholder="อำเภอ/เขต"> -->

                                <select class="form-control select" name="district">
                                    <?php foreach ($address_district as $key => $address_district_item) { ?>
                                        <option value="" selected><?php echo $address_district_item->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="col-xl-6 sub-district mt-5">
                                <b>ตำบล/แขวง *</b>
                                <!-- <input type="text" id="subdistrict" name="subdistrict" placeholder="ตำบล/แขวง"> -->

                                <select class="form-control select" name="sub_district">
                                    <?php foreach ($address_sub_district as $key => $address_sub_district_item) { ?>
                                        <option value="" selected><?php echo $address_sub_district_item->name ?></option>
                                    <?php } ?>
                                </select>

                            </div>

                            <div class="col-xl-6 postal mt-5">
                                <b>รหัสไปรษณีย์ *</b>
                                <input type="text" id="postal" name="postal" placeholder="รหัสไปรษณีย์">
                            </div>

                            <!-- <div class="col-sm-6 col-xl-6 mt-2" style="text-align:left;">
                                <div class="province pt-1">
                                    <label for="province"> <b>จังหวัด*</b></label>
                                    <select id="province" name="province">
                                        <option value="bangkok">กรุงเทพมหานคร</option>
                                        <option value="khonkaen">ขอนแก่น</option>
                                        <option value="chaiyaphum">ชัยภูมิ</option>
                                    </select>
                                </div>
                            </div> -->


                            <div class="pt-1">
                                <label class="form-check-label"><input type="checkbox"> ตั้งเป็นที่อยู่หลัก</label>
                            </div>


                            <div class="button4 pt-4" style="text-align: center;">
                                <div class="submit pt-4" style="text-align: center; padding-bottom: 40px;">
                                    <input type="submit" value="บันทึก" style="width: 208px; height:45px; border:solid 0px; border-radius: 23px; padding: 9px; margin:10px; background-color:#AAAAAA; color:#FFFFFF;">
                                </div>
                            </div>

                        </div>
                    </form>
                </div>



            </div>
        </div>

        <script>
            // function isNumber(evt) {
            //     evt = (evt) ? evt : window.event;
            //     var charCode = (evt.which) ? evt.which : evt.keyCode;
            //     if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            //         return false;
            //     }

            //     return true;
            // }

            $("#dataForm select[name='province']").select({
                placeholder: "",
                width: null,
                allowClear: true,
                ajax: {
                    url: "<?php echo base_url("https://nextere.space/api_area/home/select2/address_province/") ?>";,
                    dataType: 'json',
                    delay: 250,
                    cache: true,
                    data: function(params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data.data, function(obj) {
                                return {
                                    id: obj.id,
                                    text: obj.name
                                };
                            })
                        };
                    }
                }
            });

            $("#dataForm select[name='district']").select({
                placeholder: "",
                width: null,
                allowClear: true,
                ajax: {
                    url: "<?php echo base_url("https://nextere.space/api_area/home/select2/address_district/") ?>" + $("select[name=province]").val();,
                    dataType: 'json',
                    delay: 250,
                    cache: true,
                    data: function(params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data.data, function(obj) {
                                return {
                                    id: obj.id,
                                    text: obj.name
                                };
                            })
                        };
                    }
                }
            });

            $("#dataForm select[name='sub_district']").select({
                placeholder: "",
                width: null,
                allowClear: true,
                ajax: {
                    url: "<?php echo base_url("https://nextere.space/api_area/home/select2/address_sub_district/") ?>" + $("select[name=district]").val();,
                    dataType: 'json',
                    delay: 250,
                    cache: true,
                    data: function(params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data.data, function(obj) {
                                return {
                                    id: obj.id,
                                    text: obj.name
                                };
                            })
                        };
                    }
                }
            });

            $(document).ready(function() {
                $('#telephone').mask('000-000-0000');
                $("#dataForm").validate({
                    rules: {
                        name: "required",
                        lastname: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        telephone: {
                            required: true,
                            pattern: '\\d{3}-\\d{3}-\\d{4}',
                            minlength: 12,
                            maxlength: 12
                        },
                        address: "required",
                        province: "required",
                        district: "required",
                        subdistrict: "required",
                        postal: "required",
                    },
                    messages: {
                        name: "Please enter your name",
                        lastname: "Please enter your lastname",
                        email: "Please enter a valid email address",
                        telephone: "Please correct telephone format",
                        address: "Please enter your address",
                        province: "Please enter your province",
                        district: "Please enter your district",
                        subdistrict: "Please enter your sub-district",
                        postal: "Please enter your postal",
                    },

                    errorPlacement: function(error, element) {
                        error.insertBefore(element);
                    }
                });
            });

            $("#dataForm").submit(function(e) {
                e.preventDefault();
                console.log('not submit!!');
                return false;
            });
        </script>

    </section>
    <div class="mt-5">
        <?php include('footer.php') ?>
    </div>
</body>