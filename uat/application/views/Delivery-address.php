<?php include('header.php');
$codeigniter_instance = &get_instance();
?>


<style>
    .delivery ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 100%;
        background-color: white;
        border: 1px solid #BCBCBC;
        opacity: 1;
        font-size: 16px;
    }

    .delivery li a.active {
        background-color: #BCBCBC;
        color: white;
    }

    .delivery li a {
        display: block;
        color: #000;
        padding: 8px 12px;
        text-decoration: none;
        font-size: 14px;


    }
</style>



<body>
    <section class="delivery">
        <div class="container mt-5">
            <div class="row">
                <div class="col-sm-12 col-xl-12 pt-5 pb-5 pl-5 mt-5" style="background-color: lightgray; text-align:left">
                    <h4><b><?php echo getWording('profile', 'my_account') ?></b></h4>
                    <p><?php echo getWording('profile', 'hi') ?> <?php echo $user_info->fullname ?>, <?php echo getWording('profile', 'detail') ?></p>
                </div>
                <div class="col-sm-4 col-xl-3 mt-3 pl-0">
                    <ul>
                        <li><a href="<?php echo base_url('Account/profile') ?>"> <img class="mr-3" style="width: 16px; height: 16px;" src="<?php echo base_url('/assets/img/icon/user.png') ?>"><?php echo getWording('profile', 'profile') ?></a></li>
                        <li><a href="<?php echo base_url('Account/order_history') ?>"><img class="mr-3" style="width: 20px; height: 18px;" src="<?php echo base_url('/assets/img/icon/shopping-cart.png') ?>"><?php echo getWording('profile', 'my_orders') ?></a></li>
                        <li><a href="<?php echo base_url('Account/quotation_request_history') ?>"><img class="mr-3" style="width: 18px; height: 18px;" src="<?php echo base_url('/assets/img/icon/Invoice.png') ?>"><?php echo getWording('profile', 'quotation') ?></a></li>
                        <li><a class="active" href="<?php echo base_url('Account/sending_address') ?>"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php echo base_url('/assets/img/icon/delivery-truck-White.png') ?>"><?php echo getWording('profile', 'shipping_address') ?></a></li>
                        <li><a href="<?php echo base_url('Account/tax_invoice_address') ?>"><img class="mr-3" style="width: 16px; height: 16px;" src="<?php echo base_url('/assets/img/icon/location-2.png') ?>"><?php echo getWording('profile', 'tax_invoice') ?></a></li>
                        <!-- <li><a href="<?php echo base_url('Account/payment') ?>"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php echo base_url('/assets/img/icon/credit-card.png') ?>"><?php echo getWording('profile', 'credit') ?></a></li> -->
                        <li><a href="<?php echo base_url('Account/datasheet') ?>"><img class="mr-3" style="width: 16px; height: 20px;" src="<?php echo base_url('/assets/img/icon/Datasheet.png') ?>"><?php echo getWording('profile', 'datasheet') ?></a></li>
                    </ul>
                </div>
                <div class="col-sm-8 col-xl-9 mt-3">
                    <div class="head">
                        <h3 style="font-weight: bold"><?php echo getWording('profile', 'shipping_address') ?></h3>
                    </div>
                    <hr style="border-width: 3px;">

                    <?php if (empty($address)) { ?>
                        <?php echo null ?>
                    <?php } else { ?>

                        <div class="row" id="address_list" style="padding-bottom: 20px;">
                            <?php foreach ($address as $key => $address_item) { ?>
                                <div class="address mt-2" style="width: 100%; height: auto; border: solid #DBDBDB;">
                                    <div class="row p-5">

                                        <div class="col-sm-9 col-xl-10">
                                            <H5><?php echo $address_item->first_name ?> <?php echo $address_item->last_name ?></H5>

                                            <p class="pt-2"><b><?php echo getWording('profile', 'address') ?> :</b> <?php echo $address_item->address ?></p>

                                            <p><b><?php echo getWording('profile', 'sub_district') ?> :</b> <?php echo $address_item->sub_district->name ?> <b><?php echo getWording('profile', 'district') ?> :</b> <?php echo $address_item->district->name ?></p>

                                            <p><b><?php echo getWording('profile', 'province') ?> :</b> <?php echo $address_item->province->name ?> <?php echo $address_item->postcode ?></p>


                                            <?php if (empty($address_item->company_name)) { ?>
                                                <?php echo null ?>
                                            <?php } else { ?>
                                                <p><b><?php echo getWording('profile', 'company') ?> :</b> <?php echo $address_item->company_name ?></p>
                                            <?php } ?>

                                            <?php if (empty($address_item->company_branch)) { ?>
                                                <?php echo null ?>
                                            <?php } else { ?>
                                                <p><b><?php echo getWording('profile', 'branch') ?> :</b> <?php echo $address_item->company_branch ?></p>
                                            <?php } ?>

                                            <p><b><?php echo getWording('profile', 'email') ?> :</b> <?php echo $address_item->email ?></p>

                                            <p><b><?php echo getWording('profile', 'phone') ?> :</b> <?php echo $address_item->telephone ?></p>

                                        </div>
                                        <div class="col-sm-3 col-xl-2" style="text-align: right; padding-right: 30px;">
                                            <a href="<?php echo base_url('/Account/sending_address_edit/' . $address_item->id) ?>">
                                                <?php echo getWording('profile', 'edit') ?>
                                            </a>
                                            <a class="remove_address_item" href="#" data-remove="<?php echo $address_item->id ?>">
                                                <i class="fas fa-trash-alt ml-3"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <?php if (($address_item->is_default) >= '1') { ?>
                                        <div class="row p-4">
                                            <div style="width: 123px; height: 34px; background: #E6E5E5; border-radius: 3px; border:0px; padding:3px; text-align: center;">
                                                <div style="padding-top: 4.5px;"><?php echo getWording('profile', 'main_address') ?></div>
                                            </div>
                                        </div>
                                    <?php } else {
                                        echo null;
                                    }
                                    ?>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <div class="button4 pt-4">
                        <a href="<?php echo base_url('Account/sending_address_add') ?>">
                            <button class="edit" style="width: 199px; height:45px; border:solid 1px; border-radius: 23px; padding: 9px; margin:10px; background-color:#FFFFFF">
                                <?php echo getWording('profile', 'add_new_address') ?>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>



    
        
            <!-- Modal -->
            <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content modal-content-style">
                        <div class="modal-header modal-header-style">

                        </div>
                        <div class="modal-body" style="text-align: -webkit-center;">
                            <!-- <div class="btn-close-modal">
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div> -->
                            <h3 class="remove_response" style="text-align: center;"></h3>
                            <button class="btn" type="button" data-bs-dismiss="modal" aria-label="Close" style="width: 142px;height: 40px; background-color: #AAAAAA; border-radius: 23px; padding: 10px; margin:10px;color:white;"><?php echo getWording('index', 'ok') ?></button>
                        </div>
                    </div>
                </div>
            </div>

        <script>
            $(document).ready(function() {
                var member_id = '<?php echo $codeigniter_instance->session->userdata('member::id') != null ? $codeigniter_instance->session->userdata('member::id') : ''; ?>';
                // var data = {
                //  "address_id": '<?php // echo $address_item->id 
                                    ?>', };

                $(document).on("click", ".remove_address_item", function() {
                    event.preventDefault();
                    // console.log(member_id);
                    if (member_id != "") {
                        var thisRemoveClass = $(this);
                        var data_token = {
                            "id": $(this).data('remove')
                        }

                        var add_cart = {
                            "url": "<?php echo base_url('Account/remove_address') ?>",
                            "method": "POST",
                            "data": data_token
                        }

                        $.ajax(add_cart).done(function(response) {
                            // console.log(response);
                            if (response.code == '0x0000-00000') {
                                // $("#address_list").load(document.URL + ' #address_list');
                                $("#address_list").load(document.URL + ' #address_list', function() {
                                    $(".remove_response").text('Delete successfully');
                                    $("#staticBackdrop").modal("show");
                                });
                            } else {
                                $('.remove_success').addClass('d-none');
                                $('#staticBackdrop').modal('show');
                                $('.remove_success').removeClass('d-block');
                                $('.remove_fail').addClass('d-block');
                                $('.remove_fail').removeClass('d-none');
                                $("#staticBackdrop").modal("show");
                            }
                        });
                    }
                });
            });
        </script>

    </section>
    <div class="mt-5">
        <?php include('footer.php') ?>
    </div>
</body>