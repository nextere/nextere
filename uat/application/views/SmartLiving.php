<?php include('header.php') ?>
<?php
// echo '<pre>';
// print_r($category->data->products);
// exit();
?>
<style>
    .title {
        position: absolute;
        bottom: 0;
        color: #000;
        width: 100%;
        /* padding-left: 130px; */
        /* padding-bottom: 100px; */
        text-align: left;
        bottom: 15%;
        left: 0;
    }

    .title_2 {
        position: absolute;
        bottom: 0;
        color: white;
        width: 100%;
        /* padding-left: 130px; */
        /* padding-bottom: 100px; */
        text-align: left;
        bottom: 15%;
        left: 0;
    }
</style>

<body>
    <div class="pt-5 pb-5" style="background-color:white; margin-top: 140px; ">
        <h3 style="text-align: center;"><b><?php echo getVariable($category->data->menu_category, 'name') ?></b></h3>
    </div>
    <?php foreach ($category->data->submenu_categories as $key => $sub_category_list) { ?>
        <?php if ($key % 2) { ?>
            <div class="row">
                <div class="col-12">
                    <div class="article_banner w-100 banner_slider">
                        <div>
                            <img class="w-100" src="<?php echo base_url($sub_category_list->banner_path) ?>" alt="">
                            <div class="title_2">
                                <div class="container">
                                    <div class="row justify-content-end">
                                        <div class="col-12 col-xl-6" style="width: 40%;">
                                            <h3><b><?php echo getVariable($sub_category_list, 'name') ?></b></h3>
                                            <p><?php echo getVariable($sub_category_list, 'description') ?></p>
                                            <a href="<?php echo base_url('Solution/detail/' . $sub_category_list->id . '?type=sub_solution') ?>"><button type="button" class="btn" style="background :darkgray; color:black; width: 130px;height: 35px;border-radius: 25px;"><?php echo getWording('index', 'more') ?> <i class="fas fa-chevron-right"></i></button></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <div class="row">
                <div class="col-12">
                    <div class="article_banner w-100 banner_slider">
                        <div>
                            <img class="w-100" src="<?php echo base_url($sub_category_list->banner_path) ?>" alt="">
                            <div class="title">
                                <div class="container">
                                    <div class="row justify-content-start">
                                        <div class="col-12 col-xl-6">
                                            <h3><b><?php echo getVariable($sub_category_list, 'name') ?></b></h3>
                                            <p><?php echo getVariable($sub_category_list, 'description') ?></p>
                                            <a href="<?php echo base_url('Solution/detail/' . $sub_category_list->id . '?type=sub_solution') ?>"><button type="button" class="btn" style="background :darkgray; color:black; width: 130px;height: 35px;border-radius: 25px;"><?php echo getWording('index', 'more') ?> <i class="fas fa-chevron-right"></i></button></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php } ?>

    <?php if (!empty($category->data->products)) { ?>
        <div class="row mt-5">
            <div class="col-sm-12 col-xl-12 " style=" text-align: center;">
                <h4><b><?php echo getWording('product', 'product') ?></b></h4>
            </div>
        </div>
        <div class="container">
            <div class="row " id="products">
                <?php foreach ($category->data->products as $key => $product_category_list) { ?>
                    <?php if ($key <= 7) { ?>
                        <div class="col-sm-6 col-xl-3 mt-4 item" style="text-align:left">
                            <div class="product-item">
                                <div class="border-images">
                                    <?php if (property_exists($product_category_list, 'product::images')) { ?>
                                        <a href="<?php echo base_url('/Product/Details/' . $product_category_list->{'product::id'}) ?>">
                                            <img class="w-100" src="<?php echo $product_category_list->{'product::images'}[0] ?>" alt="">
                                        </a>
                                    <?php } else { ?>
                                        <a href="<?php echo base_url('/Product/Details/' . $product_category_list->{'product::id'}) ?>">
                                            <img class="w-100" src="<?php echo base_url('/assets/img/no-images.png') ?>" alt="">
                                        </a>
                                    <?php } ?>
                                    <!-- <a href="<?php echo base_url('/Product/Details/' . $product_category_list->{'product::id'}) ?>">
                                        <img class="w-100" src="<?php echo $product_category_list->{'product::images'}[0] ?>" alt="">
                                    </a> -->
                                </div>
                                <h4 class="mt-3"><?php echo getVariable($product_category_list, 'product::name'); ?></h4>

                                <?php if ($product_category_list->{'product::model_name_th'}  == '') { ?>
                                    <span style="color:white;">...</span>
                                <?php } else if ($product_category_list->{'product::model_name_en'}  == '') { ?>
                                    <span style="color:white;">...</span>
                                <?php } else { ?>
                                    <span><?php echo getVariable($product_category_list, 'product::model_name') ?></span>
                                <?php } ?>
                                <?php if ($product_category_list->{'product::retail_price'} != null) { ?>
                                    <?php if (empty($product_category_list->{'product::discount'})) { ?>
                                        <h4 class="mt-6">
                                            <?php echo number_format($product_category_list->{'product::retail_price'}); ?>
                                            <?php echo getWording('index', 'baht') ?>
                                        </h4>
                                    <?php } else { ?>
                                        <h4 class="mt-1">
                                            <div class="row">
                                                <span class="col-6">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <?php if ($product_category_list->{'product::discount'}->discount_percent > "1") { ?>
                                                                <span class="discount-product">- <?php echo number_format($product_category_list->{'product::discount'}->discount_percent); ?> <?php echo getWording('index', 'baht') ?></span>
                                                            <?php } else { ?>
                                                                <span class="discount-product">- <?php echo ($product_category_list->{'product::discount'}->discount_percent * 100); ?>%</span>
                                                            <?php } ?>
                                                        </div>
                                                        <span class="strikethrough">
                                                            <?php echo number_format($product_category_list->{'product::retail_price'}); ?>
                                                            <?php echo getWording('index', 'baht') ?>
                                                        </span>
                                                    </div>
                                                </span>
                                                <span class="col-6 mt-5" style="color: red;">
                                                    <?php if ($product_category_list->{'product::discount'}->total_price > "1") { ?>
                                                        <?php echo number_format($product_category_list->{'product::discount'}->total_price); ?>
                                                    <?php } else { ?>
                                                        0
                                                    <?php } ?>
                                                    <?php echo getWording('index', 'baht') ?>
                                                </span>
                                            </div>
                                        </h4>
                                    <?php } ?>
                                    <div class="row mt-5">
                                        <?php if ($token != '') { ?>
                                            <div class="col-8 pr-1">
                                                <button class="order_btn w-100" data-quantity="1" data-product-qty="<?php echo $product_category_list->{'product::in_stock_qty'} ?>" data-product-id="<?php echo $product_category_list->{'product::id'} ?>"><?php echo getWording('index', 'buy') ?></button>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-8 pr-1">
                                                <a href="<?php echo base_url('Account/login') ?>"><button class="w-100" ? style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"><?php echo getWording('index', 'buy') ?></button></a>
                                            </div>
                                        <?php } ?>
                                        <div class="col-4 pl-1">
                                            <button type="button" class="filter_btn w-100 p-0" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" data-compare="<?php echo $product_category_list->{'product::id'} ?>">
                                                <svg class="w-100">
                                                    <use xlink:href="#icon-filter"></use>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <h5 class="mt-6">
                                        <b>สนใจสินค้า กรุณาติดต่อเจ้าหน้าที่</b>
                                    </h5>
                                    <div class="row mt-5">
                                        <div class="col-8 pr-1">
                                            <a href="<?php echo base_url('Contact') ?>"><button class="w-100" ? style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;">ติดต่อเจ้าหน้าที่</button></a>
                                        </div>
                                        <div class="col-4 pl-1">
                                            <button id="test-btn" type="button" class="filter_btn w-100 p-0" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" data-compare="<?php echo $product_category_list->{'product::id'} ?>">
                                                <svg class="w-100">
                                                    <use xlink:href="#icon-filter"></use>
                                                </svg>
                                            </button>
                                        </div>

                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="row justify-content-center mt-6">
                <div class="col-2">
                    <button class="see_all_btn w-100 py-2" onclick="location.href='<?php echo base_url('Solution/soluton_product_list/' . $category->data->menu_category_id . '?type=solution') ?>'"><?php echo getWording('index', 'more') ?></button>
                </div>
            </div>
        </div>
    <?php } ?>



    <div class="mt-5">
        <?php include('footer.php') ?>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <!-- <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/fontawesome.min.js" integrity="sha512-KCwrxBJebca0PPOaHELfqGtqkUlFUCuqCnmtydvBSTnJrBirJ55hRG5xcP4R9Rdx9Fz9IF3Yw6Rx40uhuAHR8Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://vjs.zencdn.net/7.11.4/video.min.js"></script> -->
    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
    <svg aria-hidden="true" style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="icon-filter" width="100%" viewBox="0 0 25 25">
                <g>
                    <path d="M1.75,7.75h6.6803589c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891   S13.9000854,9.0452271,14.2356567,7.75H24.25C24.6640625,7.75,25,7.4140625,25,7s-0.3359375-0.75-0.75-0.75H14.2356567   c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891S8.7659302,4.9547729,8.4303589,6.25H1.75   C1.3359375,6.25,1,6.5859375,1,7S1.3359375,7.75,1.75,7.75z M11.3330078,5.4912109   c0.8320313,0,1.5087891,0.6767578,1.5087891,1.5087891s-0.6767578,1.5087891-1.5087891,1.5087891S9.8242188,7.8320313,9.8242188,7   S10.5009766,5.4912109,11.3330078,5.4912109z" fill="#1D1D1B" />
                    <path d="M24.25,12.25h-1.6061401c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891   S17.1741333,10.9547729,16.838562,12.25H1.75C1.3359375,12.25,1,12.5859375,1,13s0.3359375,0.75,0.75,0.75h15.088562   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891s2.5670776-0.963562,2.9026489-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,12.25,24.25,12.25z M19.7412109,14.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891S21.25,12.1679688,21.25,13   S20.5732422,14.5087891,19.7412109,14.5087891z" fill="#1D1D1B" />
                    <path d="M24.25,18.25H9.7181396c-0.3355103-1.2952271-1.5037842-2.2587891-2.9017334-2.2587891   c-1.3987427,0-2.5670776,0.963562-2.9026489,2.2587891H1.75C1.3359375,18.25,1,18.5859375,1,19s0.3359375,0.75,0.75,0.75h2.1637573   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891c1.3979492,0,2.5662231-0.963562,2.9017334-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,18.25,24.25,18.25z M6.8164063,20.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891   c0.8310547,0,1.5078125,0.6767578,1.5078125,1.5087891S7.6474609,20.5087891,6.8164063,20.5087891z" fill="#1D1D1B" />
                </g>
            </symbol>
        </defs>
    </svg>


</body>

</html>