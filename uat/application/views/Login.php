<?php include('header.php') ?>

<body style="background-color:#F8F8F8">
    <div class="container w-100" style="text-align: -webkit-center;">

        <div class="col-sm-12 col-xl-8" style="background-color:#FFFFFF; ">

            <div style="text-align: right; padding-bottom: 10px; padding-top: 20px;">

                <button class="btn3 mt-3" onclick="location.href='<?php echo base_url('Account/register') ?>'" style="width: 142px; height: 45px; background-color: #FFFFFF; border: 1px solid #707070; border-radius: 23px; margin-right: 20px">
                    <b>
                        <?php echo getWording('login', 'register') ?>
                    </b>
                </button>

            </div>

            <div class="login pt-4 w-75" action="<?php echo base_url('Account/profile') ?>">

                <div style="border-bottom: 2px solid #D6D6D6; padding-bottom:20px; margin-bottom:35px;width: auto;">
                    <h4>
                        <b>
                            <?php echo getWording('login', 'login') ?>
                        </b>
                    </h4>
                </div>

                <div style="width: auto; height:auto;">
                    <form id="login">
                        <div class="col-sm-12 col-xl-12" style="text-align:left;"><b><?php echo getWording('login', 'email') ?>*</b>
                            <div class="email pt-2">
                                <input type="email" id="email" name="email" placeholder="<?php echo getWording('login', 'email') ?>" required style="width: 100%; height: 45px;
                        border: 1px solid #707070; border-radius: 5px;padding-left:15px ">
                            </div>
                        </div>

                        <div class="col-sm-12 col-xl-12 pt-4" style="text-align:left;"><b><?php echo getWording('login', 'password') ?>*</b>
                            <div class="password pt-2">
                                <input type="password" id="password" name="password" placeholder="<?php echo getWording('login', 'password') ?>" required style="width: 100%; height: 45px;
                        border: 1px solid #707070; border-radius: 5px;padding-left:15px">
                            </div>
                        </div>

                        <div class="row ml-1 mr-1">
                            <div class="col-sm-6 col-xl-9 pt-2" style="text-align:left;">
                                <div class="remember pt-1">
                                    <label class="form-check-label"><input type="checkbox"> <?php echo getWording('login', 'remember') ?></label>
                                </div>
                            </div>

                            <div class="col-sm-6 col-xl-3 pt-2 forget" style="text-align: right;text-decoration: underline;">
                                <a class="forget pt-1" onclick="location.href='<?php echo base_url('Account/forget_password') ?>'" style="border: 0px; background-color:#FFFFFF; cursor:pointer">
                                    <?php echo getWording('login', 'forget') ?> ?
                                </a>
                            </div>
                        </div>

                        <div class="submit pt-4">
                            <a href="<?php echo base_url('Account/profile') ?>"> <input type="submit" value="<?php echo getWording('login', 'login') ?>" style="width: 50%;
                    height: 45px; background: #D3D3D3; border: 0px; border-radius: 23px; font-weight: bold;"></a>
                        </div>
                    </form>
                </div>
            </div>
            <!-- </form> -->

            <div style=" color:#9F9F9F">
                <hr>
                <?php echo getWording('login', 'or') ?>
                <hr>
            </div>

            <div class="row" style="justify-content: center; padding-bottom: 20px;">
                <button class="btn social facebook-icon-login " onClick="logInWithFacebook()" type="button" style="margin-right: 0px;"> <i class="fab fa-facebook-f"></i></button>
                <button class="btn social instagram-icon-login" id="customBtn" style="margin-right: 10px; margin-left:10px"><i class="fab fa-google"></i></button>
                <button class="btn social line-icon-login" onclick="logIn()" style="margin-right: 0px;"><i class="fab fa-line"></i></button>
            </div>

        </div>


    </div>

    <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-content-style">
                <div class="modal-header modal-header-style">

                </div>
                <div class="modal-body">
                    <div class="btn-close-modal">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <h3 class="register_response" style="text-align: center;"></h3>
                </div>
                <div class="modal-footer modal-footer-style register_response_btn">
                    <!-- <button type="button" class="btn btn-success register_success d-none" onclick="location.href='<?php // echo base_url() 
                                                                                                                        ?>'">Understood</button> -->
                    <button type="button" class="btn register_fail d-none" style="width: 142px;height: 40px; background-color: #AAAAAA; border-radius: 23px; padding: 10px; margin:10px;color:white;" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</body>
<?php include('footer.php') ?>
<script src="https://static.line-scdn.net/liff/edge/2/sdk.js"></script>

<script>
    function logOut() {
        liff.logout()
        window.location.reload()
    }

    function logIn() {
        liff.login({
            redirectUri: window.location.href
        })
    }
    async function getUserProfile() {
        var information_completely = '<?php echo getWording('login', 'information_completely') ?>';
        const profile = await liff.getProfile()
        console.log('profile', profile)
        line_token = profile.userId;
        $("#name").val(profile.displayName);
        $(".register_response").text(information_completely);
        $('#staticBackdrop').modal('show');
        $('#password_remove').hide();
        $('#confirm_password_remove').hide();
        // $('#social').hide();
        if (liff.isLoggedIn() == true) {
            logOut();
        }

        var data = {
            "line_token": line_token,
        }
        // console.log(data);

        // print_r(data);
        // exit();
        var send_data = {
            "url": "<?php echo base_url('/Account/line') ?>",
            "method": "POST",
            "data": data
        }

        var base_url = 'location.href="' + '<?php echo base_url() ?>' + '"';

        $.ajax(send_data).done(function(response) {
            // $(".register_response").text(response.message);
            if (response.code == "0x0000-00000") {
                window.location.href = "<?php echo base_url() ?>";
            } else if (response.code == "0x000A-AU001") {
                $('.register_success').addClass('d-none');
                $('.register_success').removeClass('d-block');
                $('.register_fail').addClass('d-block');
                $('.register_fail').removeClass('d-none');
                $(".register_response").text('Username or password is incorrect. Please retry.');
            }
            $('#staticBackdrop').modal('show');
            console.log(response);
        });
        // console.log('not submit!!');
        // console.log('birdth_date', $('#telephone').val());
        return false;

    }

    async function main() {
        await liff.init({
            liffId: "1656293084-01Ep9rqo"
        });
        if (liff.isInClient()) {
            getUserProfile();
        } else {
            if (liff.isLoggedIn() == false) {
                // getUserProfile();
                console.log('not logged in');
                console.log('liff_id', liff.init);
            } else {
                getUserProfile();
            }
        }
    }
    main()
    // console.log(login());
</script>


<meta name="google-signin-client_id" content="441404752301-1a5fd972o9ab8ps1960ksk63od66p2n8.apps.googleusercontent.com">
<meta name="google-signin-scope" content="profile email">
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://apis.google.com/js/api:client.js"></script>
<!-- ------------------------------------------------------------------------------------ -->

<script src="https://apis.google.com/js/api:client.js"></script>
<script>
    var googleUser = {};
    var startApp = function() {
        gapi.load('auth2', function() {
            auth2 = gapi.auth2.init({
                client_id: '441404752301-1a5fd972o9ab8ps1960ksk63od66p2n8.apps.googleusercontent.com',
                cookiepolicy: 'single_host_origin'
            });
            attachSignin(document.getElementById('customBtn'));
        });
    };

    function attachSignin(element) {
        // console.log(element.id);
        auth2.attachClickHandler(element, {},
            function(googleUser) {
                document.getElementById('name').innerText = "Signed in: "
                var profile = googleUser.getBasicProfile();
                console.log(profile);
                var email = profile.getEmail();
                var token = googleUser.getAuthResponse().id_token;
                var login_email = '<?php echo getWording('login', 'login_email') ?>';
                var login_facebook = '<?php echo getWording('login', 'login_facebook') ?>';
                // google_token = token
                var data2 = {
                    'email': email,
                    'token': profile.US
                };
                console.log(data2);
                var formData = new FormData();
                for (var key in data2) {
                    formData.append(key, data2[key]);
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('/Account/Google_login') ?>",
                    data: JSON.stringify(data2),
                    processData: false,
                    contentType: "application/json; charset=utf-8",
                }).done(function(data) {
                    console.log(data);
                    if (data.code == "0x0000-00000") {
                        window.location = '<?php echo base_url('') ?>';
                    } else if (data.code == "0x0API-00004") {
                        window.location = '<?php echo base_url('site/Authentication/input_phone_num') ?>';
                    } else if (data.code == "0x00GM-00003") {
                        alert(login_email);
                    } else if (data.code == "0x00FB-00003") {
                        alert(login_facebook);
                    }
                });
            }, );
    }
</script>
<script>

</script>
<div id="name"></div>
<script>
    startApp();
</script>

<!-- --------------------------------------------------------------------------------- -->
<script>
    jQuery(document).ready(function() {
        var facebook_name = "";
        var facebook_token;
        var google_token;
        var line_token;

        window.fbAsyncInit = function() {
            FB.init({
                appId: '295555285694750',
                cookie: true, // This is important, it's not enabled by default
                version: 'v2.5'
            });
        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    });
</script>

<script type="text/javascript">
    logInWithFacebook = function() {
        FB.login(function(response) {
            var authResponse = '';
            if (response.authResponse) {
                FB.getLoginStatus(function(response) {
                    authResponse = response.authResponse.accessToken;
                    fetch('https://graph.facebook.com/v2.5/me?fields=email&access_token=' + authResponse).then((resp) => console.log(resp.json().then(data => {
                        data.token = authResponse;
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url('/Account/Facebook_login') ?> ",
                            data: {
                                "data": data
                            },
                        }).done(function(data) {
                            // console.log(data);
                            if (data.code == "0x0000-00000") {
                                window.location = '<?php echo base_url('') ?>';
                            }
                        });
                    })))
                });
            } else {
                alert('User cancelled login or did not fully authorize.');
            }
        }, {
            scope: 'email'
        });
        return false;
    };
</script>

<script>
    // function phoneMask() {
    //     var num = $(this).val().replace(/\D/g, '');
    //     $(this).val(num.substring(0, 3) + '-' + num.substring(3, 6) + '-' + num.substring(6, 10));
    // }

    // $('[type="tel"]').keyup(phoneMask);

    // $('.gender input').change(function() {
    //     var selected_gender;

    // });

    $('#birthdate_input input').datepicker({
        format: "dd/mm/yyyy"
    });
    $('#dataForm input').keypress(function() {
        console.log('birdth_date', $('#birdth_date').val());
        console.log('birdth_date', $('#telephone').val());
    });

    $(document).ready(function() {
        $('#telephone').mask('000-000-0000');

        $("#login").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 5
                }
            },
            messages: {
                email: "Please enter a valid email address.",
                register_password: {
                    required: "Please provide a password.",
                    minlength: "Your password must be at least 5 characters long."
                }
            },

            errorPlacement: function(error, element) {
                error.insertBefore(element);
            }

        });

        jQuery.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                if (regexp.constructor != RegExp)
                    regexp = new RegExp(regexp);
                else if (regexp.global)
                    regexp.lastIndex = 0;
                return this.optional(element) || regexp.test(value);
            }, "erreur expression reguliere"
        );

    });



    $("#login").submit(function(e) {
        e.preventDefault();

        event.preventDefault();
        // console.log($("#subscribe").val());

        var data = {
            "email": $("#email").val(),
            "password": $("#password").val(),
        }

        // console.log(data);

        var send_data = {
            "url": "<?php echo base_url('/Account/login') ?>",
            "method": "POST",
            "data": data
        }

        var base_url = 'location.href="' + '<?php echo base_url() ?>' + '"';

        $.ajax(send_data).done(function(response) {

            // $(".register_response").text(response.message);
            if (response.code == "0x0000-00000") {
                window.location.href = "<?php echo base_url() ?>";
            } else if (response.code == "0x000A-AU001") {
                $('.register_success').addClass('d-none');
                $('.register_success').removeClass('d-block');
                $('.register_fail').addClass('d-block');
                $('.register_fail').removeClass('d-none');
                $(".register_response").text('Username or password is incorrect. Please retry.');
                $('#staticBackdrop').modal('show');
            }
           
            console.log(response);
        });
        // console.log('not submit!!');
        // console.log('birdth_date', $('#telephone').val());
        return false;
    });
</script>