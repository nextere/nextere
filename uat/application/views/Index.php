<?php // include("header.php")
$codeigniter_instance = &get_instance();
if (!empty($codeigniter_instance->session->userdata('member::token'))) {
    $token = $codeigniter_instance->session->userdata('member::token');
} else {
    $token = '';
}
?>
<style>
    .article a {
        color: black;
        text-decoration: none;

    }

    /* .article span {
        border: solid 1px;
        border-color: lightgray;
        padding: 2px 7px;
        background: lightgrey;
        margin-right: 10px;
        border-radius: 5px;
        font-size: .5rem;
    } */

    .article_1 {
        position: absolute;
        bottom: 0;
        color: white;
        /* padding-left: 130px; */
        /* padding-bottom: 100px; */
        text-align: left;
        bottom: 10%;
        left: 0;
    }

    .nav {
        display: flex;
        flex-wrap: wrap;
        padding-left: 0;
        margin-bottom: 0;
        list-style: none;
        background-color: none;

    }

    .nav-pills .nav-link {
        background: 0 0;
        border: 0;
        border-radius: .25rem;
        color: black;
        font-size: 18px;
    }

    .nav-pills .nav-link.active,
    .nav-pills .show>.nav-link {
        color: black;
        background-color: white;


    }

    .tab-content>.active {
        display: block;
        padding-left: 5%;
    }

    .living_tuumbnail_description h3 {
        word-wrap: break-word;
    }

    /* .ex2:hover,
    .ex2:active {
        font-size: 200%;
        font-weight: bold;
    } */
    .ex2 i {
        visibility: hidden;
    }

    .ex2.active i {
        visibility: visible;
    }

    /* .active {
        font-size: 200%;
        font-weight: bold;
    } */

    .solution_hover.active {
        font-size: 200%;
        font-weight: bold;
        transition: font-size .3s;
    }
</style>


<body>
    <div class="banner_slider">
        <?php foreach ($banner_list as $key => $banner_item) { ?>
            <?php
            $type = array();
            $type = explode(".", $banner_item->banner_path);
            // print_r($type);
            ?>
            <?php if ($type[1] == 'mp4') { ?>
                <div class="item">
                    <div class="video">
                        <video id="my-video" class="video-js" playsinline autoplay muted loop="loop">
                            <source src="<?php echo base_url($banner_item->banner_path) ?>" type="video/mp4" />
                        </video>
                        <div class="video_description">
                            <div class="video_wrapper">
                                <div class="container-fuid">
                                    <div class="row">
                                        <div class="col-12 col-xl-6">
                                            <h1><?php echo getVariable($banner_item, 'title') ?></h1>
                                            <p class="text-banner fs-4">
                                                <?php echo getVariable($banner_item, 'detail') ?></p>
                                            <a href="<?php echo ($banner_item->url) ?>"><button type="button" class="btn btn-secondary" style="width: 120px;height: 40px; border-radius: 25px;background-color: gray;"><?php echo getWording('index', 'more') ?><i class="fas fa-chevron-right"></i></button></a>
                                        </div>
                                        <div class="col-12 col-xl-6 text-center align-self-center">
                                            <!-- <div class="image_modal_banner">
                                                <a href="javascript:void(0);" id="image_banner"><i class="far fa-image"></i></a>
                                            </div> -->
                                            <div class="video_player">
                                                <i class="far fa-play-circle" id="play-button"></i>
                                                <i class="far fa-pause-circle" id="pause-button"></i>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="item">
                    <div class="video">
                        <div class="banner_index">
                            <img class="w-100" src="<?php echo base_url($banner_item->banner_path) ?>" alt="">
                        </div>
                        <div class="video_description">
                            <div class="video_wrapper">
                                <div class="container-fuid">
                                    <div class="row">
                                        <div class="col-12 col-xl-6">
                                            <h1><?php echo getVariable($banner_item, 'title') ?></h1>
                                            <p class="text-banner fs-4">
                                                <?php echo getVariable($banner_item, 'detail') ?></p>
                                            <a href="<?php echo ($banner_item->url) ?>"><button type="button" class="btn btn-secondary" style="width: 120px;height: 40px; border-radius: 25px;background-color: gray;"><?php echo getWording('index', 'more') ?><i class="fas fa-chevron-right"></i></button></a>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }  ?>
        <?php } ?>

    </div>

    <section class="index_iot_service my-8">
        <div class="container">
            <div class="text-center mb-10 mt-10">
                <h1><b>All Smart IoT Service</b></h1>
                <h4><span style="color: gray;">Nextere</span></h4>
            </div>

            <div class=" row align-items-start">
                <div class="solution_detail_list tab-content col-12 col-lg-6" id="v-pills-tabContent">
                    <?php foreach ($solution_list as $key => $solution_item) { ?>
                        <?php if ($key == 0) { ?>
                            <div class="solution_detail" data-id="solution_list_<?php echo $solution_item->id ?>">
                                <div class="living_tuumbnail w-100">
                                    <img src="<?php echo base_url($solution_item->thumbnail_path) ?>" alt="Avatar" class="image">
                                    <div class="living_tuumbnail_description">
                                        <h3><?php echo getVariable($solution_item, 'description') ?></h3>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="solution_detail" data-id="solution_list_<?php echo $solution_item->id ?>" style="display:none">
                                <div class="living_tuumbnail w-100">
                                    <img src="<?php echo base_url($solution_item->thumbnail_path) ?>" alt="Avatar" class="image">
                                    <div class="living_tuumbnail_description">
                                        <h3><?php echo getVariable($solution_item, 'description') ?></h3>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="nav-pills col-12 col-lg-6 align-items-start pl-10 mt-3" style=" align-self: center;" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <?php foreach ($solution_list as $key => $solution_item) { ?>
                        <?php if ($key == 0) { ?>
                            <button onclick="window.location.href='<?php echo base_url('Solution/detail/' . $solution_item->id . '?type=solution') ?>'" class="solution_hover nav-link ex2 active" id="solution_list_<?php echo $solution_item->id ?>"><?php echo getVariable($solution_item, 'name') ?> <i class="fas fa-arrow-right pl-2"></i></button>
                        <?php } else { ?>
                            <button onclick="window.location.href='<?php echo base_url('Solution/detail/' . $solution_item->id . '?type=solution') ?>'" class="solution_hover nav-link ex2 mt-3" id="solution_list_<?php echo $solution_item->id ?>"><?php echo getVariable($solution_item, 'name') ?> <i class="fas fa-arrow-right pl-2"></i></button>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>

        </div>
    </section>
    <section class="product_heightlight my-8">
        <div class="container">
            <div class="row">
                <h2 style="text-align: center;"><b><?php echo getWording('index', 'heightlight') ?></b></h2>
                <div class="product_heightlight_slider">
                    <?php foreach ($recommendt_list as $key => $recommendt_item) { ?>
                        <div class="col-sm-6 col-xl-3 mt-4 item" style="text-align:left">
                            <div class="product-item m-4">
                                <div class="border-images">
                                    <?php if (property_exists($recommendt_item, 'images')) { ?>
                                        <a href="<?php echo base_url('/Product/Details/' . $recommendt_item->id) ?>">
                                            <img class="w-100" src="<?php echo ($recommendt_item->images[0]) ?>" alt="">
                                        </a>
                                    <?php } else { ?>
                                        <a href="<?php echo base_url('/Product/Details/' . $recommendt_item->id) ?>">
                                            <img class="w-100" src="<?php echo base_url('/assets/img/no-images.png') ?>" alt="">
                                        </a>
                                    <?php } ?>
                                </div>
                                <h5 class="mt-3"><?php echo getVariable($recommendt_item, 'name'); ?></h5>
                                <?php if ($recommendt_item->model_name_th == '') { ?>
                                    <span style="color:white;">...</span>
                                <?php } else if ($recommendt_item->model_name_en == '') { ?>
                                    <span style="color:white;">...</span>
                                <?php } else { ?>
                                    <span><?php echo getVariable($recommendt_item, 'model_name') ?></span>
                                <?php } ?>
                                <?php if ($recommendt_item->retail_price != null) { ?>
                                    <?php if (empty($recommendt_item->discount)) { ?>
                                        <h4 class="mt-6">
                                            <?php echo number_format($recommendt_item->retail_price); ?>
                                            <?php echo getWording('index', 'baht') ?>
                                        </h4>
                                    <?php } else { ?>
                                        <h4 class="mt-1">
                                            <div class="row">
                                                <span class="col-6">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <?php if ($recommendt_item->discount->{'discount_percent'} > "1") { ?>
                                                                <span class="discount-product">- <?php echo number_format($recommendt_item->discount->{'discount_percent'}); ?> <?php echo getWording('index', 'baht') ?></span>
                                                            <?php } else { ?>
                                                                <span class="discount-product">- <?php echo ($recommendt_item->discount->{'discount_percent'} * 100); ?>%</span>
                                                            <?php } ?>
                                                        </div>
                                                        <span class="strikethrough">
                                                            <?php echo number_format($recommendt_item->retail_price); ?>
                                                            <?php echo getWording('index', 'baht') ?>
                                                        </span>
                                                    </div>
                                                </span>
                                                <span class="col-6 mt-5" style="color: red;">
                                                    <?php if ($recommendt_item->discount->{'total_price'} > "1") { ?>
                                                        <?php echo number_format($recommendt_item->discount->{'total_price'}); ?>
                                                    <?php } else { ?>
                                                        0
                                                    <?php } ?>
                                                    <?php echo getWording('index', 'baht') ?>
                                                </span>
                                            </div>
                                        </h4>
                                    <?php } ?>
                                    <div class="row mt-5">
                                        <?php if ($token != '') { ?>
                                            <div class="col-8 pr-1">
                                                <button class="order_btn w-100" data-quantity="1" data-product-qty="<?php echo $recommendt_item->in_stock_qty ?>" data-product-id="<?php echo $recommendt_item->id ?>"><?php echo getWording('index', 'buy') ?></button>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-8 pr-1">
                                                <a href="<?php echo base_url('Account/login') ?>"><button class="w-100" ? style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"><?php echo getWording('index', 'buy') ?></button></a>
                                            </div>
                                        <?php } ?>

                                        <div class="col-4 pl-1">
                                            <button id="test-btn" type="button" class="filter_btn w-100 p-0 " data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" data-compare="<?php echo $recommendt_item->id ?>">
                                                <svg class="w-100">
                                                    <use xlink:href="#icon-filter"></use>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <h5 class="mt-6">
                                        <b><?php echo getWording('index', 'interested_product') ?></b>
                                    </h5>
                                    <div class="row mt-5">
                                        <div class="col-8 pr-1">
                                            <a href="<?php echo base_url('Contact') ?>"><button class="w-100" ? style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"><?php echo getWording('index', 'contact') ?></button></a>
                                        </div>
                                        <div class="col-4 pl-1">
                                            <button id="test-btn" type="button" class="filter_btn w-100 p-0" data-toggle="tooltip" data-placement="top" title="เปรียบเทียบสินค้า" data-compare="<?php echo $recommendt_item->id ?>">
                                                <svg class="w-100">
                                                    <use xlink:href="#icon-filter"></use>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="row justify-content-center mt-6">
                    <div class="col-6 col-xl-2">
                        <a href="<?php echo base_url('Product?type=all_product') ?>"> <button class="see_all_btn w-100 py-2"><?php echo getWording('index', 'more') ?></button></a>
                    </div>
                </div>
            </div>
    </section>

    <section class="article my-8">
        <div class="container">
            <div class="row">
                <h1 class="text-center my-8"><?php echo getWording('index', 'article') ?></h1>
                <?php foreach ($block_list as $key => $block_item) { ?>
                    <?php if ($key == 0) { ?>
                        <a href="<?php echo base_url('Article/detail/' . $block_item->id) ?>">
                            <div class="article_banner">
                                <img class="w-100" src="<?php echo base_url($block_item->banner_path) ?>" alt="" style=" border-radius: 5px;">
                                <div class="article_1 col-12">
                                    <?php
                                    $tag = array();
                                    $tag = explode(",", $block_item->tags);
                                    // print_r($tag)
                                    ?>
                                    <?php foreach ($tag as $key => $block_tag) { ?>
                                        <span style="padding-left: 30px;">
                                            <?php echo ($block_tag) ?>
                                        </span>
                                    <?php } ?>
                                    <h4 style="padding-left: 30px;" class="mt-2"><?php echo getVariable($block_item, 'title') ?></h4>
                                </div>
                            </div>
                        </a>
                    <?php } else { ?>
                        <div class="col-6 col-xl-6 mt-5">
                            <a href="<?php echo base_url('Article/detail/' . $block_item->id) ?>" style=" border-radius: 5px;">
                                <div class="row my-3">
                                    <div class="col-12 col-xl-4">
                                        <img class="w-100" src="<?php echo base_url($block_item->thumbnail_path) ?>" alt="">
                                    </div>
                                    <div class="col-12 col-xl-8 align-self-center d-block pl-3 mt-5">
                                        <?php
                                        $tag = array();
                                        $tag = explode(",", $block_item->tags);
                                        // print_r($tag)
                                        ?>
                                        <?php foreach ($tag as $key => $block_tag) { ?>
                                            <span>
                                                <?php echo ($block_tag) ?>
                                            </span>
                                        <?php } ?>
                                        <h4 class="mt-2"><?php echo getVariable($block_item, 'title') ?></h4>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php } ?>
                <?php } ?>

                <div class="row justify-content-center mt-6">
                    <div class="col-6 col-xl-2">
                        <a href="<?php echo base_url('/Article') ?>"><button class="see_all_btn w-100 py-2"> <?php echo getWording('index', 'more') ?></button></a>
                    </div>
                </div>
            </div>
    </section>

    <section class="our_customer my-8">
        <div class="container">
            <div class="row justify-content-center">
                <h1 class="text-center my-8"><?php echo getWording('index', 'partner') ?></h1>
                <div class="col-10">
                    <div class="our_customer_slider">
                        <?php foreach ($partner_list as $key => $partner_item) { ?>
                            <div class="item">
                                <img class="w-100" src="<?php echo base_url($partner_item->thumbnail_path) ?>" alt="">
                            </div>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="subscribe my-8">
        <div class="container subscribe_bg p-8">
            <div class="row justify-content-end">
                <div class="col-xl-6">
                    <img class="w-75" src="<?php echo base_url('/assets/img/sub_nextere.png') ?>" ?>
                </div>
                <div class="col-12 col-xl-6 pt-3">
                    <h4><?php echo getWording('index', 'follow') ?><br><?php echo getWording('index', 'subscribe') ?></h4>
                    <div class="pt-3">
                        <form class="subscribe-form w-100" id="subscribe_form">
                            <div class="input_box d-flex">
                                <input class="py-3 px-2 w-100" type="text" placeholder="<?php echo getWording('index', 'email') ?>" id="subscribe" name="subscribe">
                                <button class="w-40" id="submit-subscribe" type="submit"><?php echo getWording('index', 'sub') ?></button>
                            </div>
                            <label id="subscribe-error" class="error" for="subscribe"></label>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                ...
            </div>
        </div>
    </div>
    <?php if ($langding_page != "") { ?>
        <div class="modal fade" id="popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal_popup">
                    <div id="popup">
                        <button id="btn-close" type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                        <img class="w-100" src="<?php echo base_url($langding_page->banner_path) ?>" alt="">
                        <div style=" position: absolute; top: 60%;left: 50%;transform: translate(-50%, -50%);">
                            <h3>
                                <?php echo $langding_page->message ?>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="modal fade banner_modal" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <img class="w-100" src="<?php echo base_url('/assets/img/nature-wallpaper-full-hd-1920_1080-0722.jpg') ?>" alt="">
                <!-- <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>

                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div> -->
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-content-style">
                <div class="modal-header modal-header-style">

                </div>
                <div class="modal-body">
                    <div class="btn-close-modal">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <h3 class="subscribe_response" style="text-align: center;"></h3>
                </div>
                <div class="modal-footer modal-footer-style subscribe_response_btn">
                    <!-- <button type="button" class="btn btn-success register_success d-none" onclick="location.href='<?php // echo base_url() 
                                                                                                                        ?>'">Understood</button> -->
                    <button type="button" class="btn register_fail d-none" style="width: 142px;height: 40px; background-color: #AAAAAA; border-radius: 23px; padding: 10px; margin:10px;color:white;" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>



    <?php include("footer.php") ?>

    <script>
        $(window).ready(function() {
            setTimeout(function() {
                $('#popup').modal("show")
            }, 500)
        })
        $(document).ready(function() {
            $("#play-button").hide();
            $('.banner_slider').slick({
                dots: true,
                arrows: true,
                prevArrow: "<i class='slick-prev fas fa-chevron-left'></i>",
                nextArrow: "<i class='slick-next fas fa-chevron-right'></i>",
            });

            $('.product_heightlight_slider').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                arrows: true,
                prevArrow: "<i class='slick-prev fas fa-chevron-left'></i>",
                nextArrow: "<i class='slick-next fas fa-chevron-right'></i>",
                responsive: [{
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 1008,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 800,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 700,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }

                ]
            });

            $('.our_customer_slider').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                arrows: false,
                autoplay: true,
                autoplaySpeed: 1000,
                prevArrow: "<i class='slick-prev fas fa-chevron-left'></i>",
                nextArrow: "<i class='slick-next fas fa-chevron-right'></i>",
                responsive: [{
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 5,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 1008,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 800,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 300,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }

                ]
            });
        });

        $(".solution_hover").mouseover(function() {
            if ($(this).hasClass('active') == false) {
                $(this).addClass('active');
                $(this).siblings().removeClass('active');
                var id = $(this).attr('id');

                $('.solution_detail').stop();
                $('.solution_detail').hide();
                $('.solution_detail[data-id=' + id + ']').fadeIn(1300);
                // $(traget).fadeIn();
            }

        });



        // $("#submit-subscribe").click(function() {
        //     console.log("test_1");
        //     event.preventDefault();
        //     $("#subscribe_form").validate({

        //         rules: {
        //             subscribe: {
        //                 required: true,
        //                 email: true
        //             }
        //         },
        //         messages: {
        //             email: "Please enter a valid email address",
        //         },
        //         errorPlacement: function(error, element) {
        //             error.insertBefore(element);
        //         },
        //         submitHandler: function() {
        //             var data = {
        //                 "email": $("#subscribe").val()
        //             }
        //             console.log("test_2");

        //             var send_data = {
        //                 "url": "<?php echo base_url() ?>",
        //                 "method": "POST",
        //                 "data": data
        //             }

        //             $.ajax(send_data).done(function(response) {
        //                 console.log(response);
        //             });
        //         }
        //     });
        // });

        $("#submit-subscribe").click(function(e) {
            e.preventDefault();
            event.preventDefault();

            // console.log($("#subscribe").val());
            var data = {
                "email": $("#subscribe").val()
            }

            console.log(data);

            var send_data = {
                "url": "<?php echo base_url('') ?>",
                "method": "POST",
                "data": data
            }

            $.ajax(send_data).done(function(response) {
                // $(".register_response").text(response.message);
                if (response.code == "0x0000-00000") {
                    window.location.href = "<?php echo base_url() ?>";
                    $(".subscribe_response").text('สมัครรับข่าวสารสำเร็จ');
                    $('#staticBackdrop').modal('show');
                } else {
                    $('.register_success').addClass('d-none');
                    $('.register_success').removeClass('d-block');
                    $('.register_fail').addClass('d-block');
                    $('.register_fail').removeClass('d-none');
                    $(".subscribe_response").text('คุณมีการสมัครรับข่าวสารแล้ว');
                }
                $('#staticBackdrop').modal('show');

                console.log(response);
            });
            // console.log('not submit!!');
            // console.log('birdth_date', $('#telephone').val());
            return false;
        });

        // jQuery.validator.setDefaults({
        //     debug: true,
        //     success: "valid"
        // });
    </script>