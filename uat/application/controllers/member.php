<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member extends CI_Controller
{
    var $api_url;
    function __construct()
    {

        parent::__construct();
        $this->api_url = $this->config->item('WEBSITE_API_URL');
    }

    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function confirm_password()
    {
        $this->load->library("encryption_library");
        $code = $this->input->get("token");

        if ($this->input->post()) 
        {
            // print_r($this->input->post());
            $code = $this->input->post("code");
            $token = $this->input->post("token");

            // $member = new stdClass();

            // $member->email = $this->encryption_library->decryptString($token,"token");

            // print_r($member);
            // exit();

            $postdata = http_build_query(
                array(
                    'token' => $this->input->post('token'),
                    'password' => $this->input->post('confirm_new_password')
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            // print_r($postdata);
            // exit();

            $context = stream_context_create($opts);

            $confirm_password = file_get_contents($this->api_url . "api_area/member/confrim_rememberPassword/", false, $context);

            $confirm_password = json_decode($confirm_password);

            // print_r($confirm_password);
            // exit();

            $this->load->library("response_library");

            $this->response_library->responseJSON($confirm_password->code, $confirm_password->message, $confirm_password->data);
        }
        $this->load->view("confirm_password", compact('code'));

    }
    public function verify_member()
    {
        // if ($_SESSION['user_token'] != "") {
        //     redirect(
        //         base_url('account/profile')
        //     );
        // } else {
        $code = $this->input->get("token");
        // print_r($code);
        // exit();

        // echo '<pre>';
        // print_r('test');
        // exit();

        if ($this->input->post()) {

            // echo 'pre';
            // print_r($this->input->post());
            // exit();

            $postdata = http_build_query(
                array(
                    'token' => $this->input->post('code'),
                    'password' => $this->input->post('confirm_new_password'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $confirm_password = file_get_contents($this->api_url . "api_area/member/confrim_rememberPassword/", false, $context);

            $confirm_password = json_decode($confirm_password);

            $this->load->library("response_library");

            $this->response_library->responseJSON($confirm_password->code, $confirm_password->message, $confirm_password->data);
        }
        $this->load->view("verify_member", compact('code'));
        // }
    }
}
