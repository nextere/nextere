<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Quotation extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->api_url = $this->config->item('WEBSITE_API_URL');
    }

    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("Quotation-Information1");
    }

    public function entity()
    {
        $this->load->view("Quotation-Information2");
    }

    public function Bill_Quotation()
    {
        $this->load->view("Bill-Quotations");
    }
    public function export_pdf($id = null)
    {
        $this->load->model("contact_us_model");
        $this->load->model("quotation_model");
        $this->load->model("quotation_detail_model");
        $this->load->model("administrator_model");
        $this->load->model("member_model");

        $contact_us = $this->contact_us_model->find(1);
        $quotation = $this->quotation_model->find($id);
        $administrator = $this->administrator_model->find($quotation->created_by);
        $member = $this->member_model->find($quotation->created_by);

        $model_filter = new stdClass();
        $model_filter->where["quotation_detail.quotation_id"] = $id;
        $model_filter->where["quotation_detail.status"] = "ACTIVATE";
        $model_filter->join = array(
            "product" => "quotation_detail.product_id = product.id"
        );
        $quotation_details = $this->quotation_detail_model->search($model_filter);

        
        // $mpdf = new \Mpdf\Mpdf();
        // $mpdf->WriteHTML('<h1>Hello world!</h1>');
        // $mpdf->Output();

        $data = array(
            "contact_us" => $contact_us,
            "quotation" => $quotation,
            "administrator" => $administrator,
            "member" => $member,
            "quotation_details" => $quotation_details,
        );

        // echo('<pre>');
        // print_r($data);
        // die;

        $this->load->view("administrator_area/quotation",$data);

    }
     public function invoice($id = null)
    {
        $this->load->model("contact_us_model");
        $this->load->model("quotation_model");
        $this->load->model("quotation_detail_model");
        $this->load->model("administrator_model");
        $this->load->model("member_model");

        $contact_us = $this->contact_us_model->find(1);
        $quotation = $this->quotation_model->find($id);
        $administrator = $this->administrator_model->find($quotation->created_by);

        $member = $this->member_model->find($quotation->created_by);

        $model_filter = new stdClass();
        $model_filter->where["quotation_detail.quotation_id"] = $id;
        $model_filter->where["quotation_detail.status"] = "ACTIVATE";
        $model_filter->join = array(
            "product" => "quotation_detail.product_id = product.id"
        );
        $quotation_details = $this->quotation_detail_model->search($model_filter);

        $order_detail = file_get_contents($this->api_url . "api_area/member/product_history/" . $member->id);
        $order_detail = json_decode($order_detail);
        $order_detail = $order_detail->data->purchase_transactions;

        
        // $mpdf = new \Mpdf\Mpdf();
        // $mpdf->WriteHTML('<h1>Hello world!</h1>');
        // $mpdf->Output();

        $data = array(
            "contact_us" => $contact_us,
            "quotation" => $quotation,
            "administrator" => $administrator,
            "member" => $member,
            "quotation_details" => $quotation_details,
            "order_detail" => $order_detail,
        );

        echo('<pre>');
        print_r($data);
        die;

        $this->load->view("administrator_area/invoice",$data);

    }
}
