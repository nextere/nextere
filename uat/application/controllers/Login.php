<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("Login");
    }

    function Register()
    {
        $this->load->view("Register");
    }
    function ForgetPass()
    {
        $this->load->view("ForgetPass");
    }
}
