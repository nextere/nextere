<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends CI_Controller
{
    var $api_url;
    function __construct()
    {

        parent::__construct();
        $this->api_url = $this->config->item('WEBSITE_API_URL');
    }


    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index($category_id = null, $page = '1')
    {
        if ($this->input->post()) {

            if ($this->input->post('page') == "") {
                $page = 1;
            } else {
                $page = $this->input->post('page');
            }

            // if($this->input->post('category')){
            //     $category
            // }

            $postdata = http_build_query(
                array(
                    'product_category_id' => $this->input->post('category'),
                    'product_sub_category_id' => $this->input->post('sub_category'),
                    'page' => $page,
                    'show_item' => $this->input->post('show_item'),
                    'price_rate_id' => $this->input->post('price_rate_id'),
                    'technical_spec_id' => $this->input->post('technical_spec_id'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $product_searched = file_get_contents($this->api_url . "api_area/product/product_list/", false, $context);
            $product_searched = json_decode($product_searched);
            // $product_searched = $product_searched->data;


            $technical_list = array();

            if ($product_searched->code == '0x0000-00000') {
                if (!empty($this->input->post('sub_category'))) {
                    $technical_list = file_get_contents($this->api_url . "api_area/Home/filter_category/" . $this->input->post('sub_category'), false, $context);
                    $technical_list = json_decode($technical_list);
                    // $technical_list = $technical_list->filter_category;
                    if (count($technical_list->data->filter_category) > 0) {
                        $technical_list = $technical_list->data->filter_category;
                        // print_r($technical_list);
                        $product_searched->data->technical_list = $technical_list;
                    }
                }
            }

            // echo '<pre>';
            // print_r($product_searched);
            // exit();



            // echo '<pre>';
            // print_r($product_searched);
            // exit();

            $this->load->library("response_library");
            $this->response_library->responseJSON($product_searched->code, $product_searched->message, $product_searched->data);
        }
        // echo '<pre>';
        // print_r($category_id);
        // exit();

        $type = $_GET["type"];

        if ($type == 'category') {
            $postdata = http_build_query(
                array(
                    'product_category_id' => $category_id,
                    'page' => '1'
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $product_list = file_get_contents($this->api_url . "api_area/product/product_list/", false, $context);
            $product_list = json_decode($product_list);
            $product_list = $product_list->data;

            $price_rate = file_get_contents($this->api_url . "api_area/product/price_rate");

            $price_rate = json_decode($price_rate);

            $price_rate = $price_rate->data;



            // echo '<pre>';
            // print_r($technical_list);
            // exit();

            $this->load->view("Allproduct", compact('product_list', 'price_rate'));
        } else if ($type == "subcategory") {

            $postdata = http_build_query(
                array(
                    'product_sub_category_id' => $category_id,
                    'page' => '1'
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $product_list = file_get_contents($this->api_url . "api_area/product/product_list/", false, $context);
            $product_list = json_decode($product_list);
            $product_list = $product_list->data;

            $price_rate = file_get_contents($this->api_url . "api_area/product/price_rate");

            $price_rate = json_decode($price_rate);

            $price_rate = $price_rate->data;

            $product_category_search = file_get_contents($this->api_url . "api_area/product/product_category_all", false, $context);
            $product_category_search = json_decode($product_category_search);

            $technical_list = file_get_contents($this->api_url . "api_area/Home/filter_category/" . $category_id, false, $context);
            $technical_list = json_decode($technical_list);
            $technical_list = $technical_list->data->filter_category;

            // echo '<pre>';
            // print_r($product_list);
            // exit();

            $this->load->view("Allproduct", compact('product_list', 'price_rate', 'technical_list'));
        } else {

            $postdata = http_build_query(
                array(
                    'page' => $page,
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);


            $product_list = file_get_contents($this->api_url . "api_area/product/product_list/", false, $context);
            $product_list = json_decode($product_list);
            $product_list = $product_list->data;

            $price_rate = file_get_contents($this->api_url . "api_area/product/price_rate");
            $price_rate = json_decode($price_rate);
            $price_rate = $price_rate->data;

            $product_searched_list = array();

            $page = isset($_GET['page']) && $_GET['page']   != null ? $_GET['page'] : 1;

            // echo '<pre>';
            // print_r($product_list);
            // exit();

            $this->load->view("Allproduct", compact('product_list', 'price_rate'));
        }
    }
    public function details($id = null, $check = null)
    {
        $product_list = file_get_contents($this->api_url . 'api_area/product/product_detail/' . $id);
        $product_list = json_decode($product_list);
        $product_list = $product_list->data->product_main;

        $product_relate = file_get_contents($this->api_url . 'api_area/product/product_detail/' . $id);
        $product_relate = json_decode($product_relate);
        $product_relate = $product_relate->data->product_relate_alls;

        $Solution_id = $product_list->{'product_detail_categorys'}[0]->{'product_sub_category::product_category_id'};
        $submenu_id = "";
        $child_id = "";
        if (isset($_SERVER['HTTP_REFERER'])) {
            $url = $_SERVER['HTTP_REFERER'];

            $url_components = parse_url($url);

            if (array_key_exists('query', $url_components)) {
                parse_str($url_components['query'], $params);
                // echo '<pre>';
                // print_r($params);
                // exit();
                if (isset($params['submenu_id'])) {

                    $submenu_id = $params['submenu_id'];
                    $child_id = $params['child_id'];
                    $postdata = http_build_query(
                        array(
                            'menu_category_id' => $Solution_id,
                            'submenu_category_id' => $submenu_id,
                            'child_submenu_category_id' => $child_id,
                        )
                    );

                    $opts = array(
                        'http' =>
                        array(
                            'method' => 'POST',
                            'header' => array(
                                'Content-type: application/x-www-form-urlencoded'
                            ),
                            'content' => $postdata
                        )
                    );

                    $context = stream_context_create($opts);


                    $breadcrumbs_search = file_get_contents($this->api_url . "api_area/home/product_detail_menu/", false, $context);
                    $breadcrumbs_search = json_decode($breadcrumbs_search);
                    $breadcrumbs_search = $breadcrumbs_search->data;
                } else {
                    $breadcrumbs_search = '';
                }
            } else {
                $breadcrumbs_search = '';
            }
        } else {
            $breadcrumbs_search = '';
        }

        // echo '<pre>';
        // print_r($product_list);
        // exit();

        // echo '<pre>';
        // print_r($breadcrumbs_search);
        // exit();

        // if ($params['submenu_id'] != "") {
        // }

        // echo '<pre>';
        // print_r($breadcrumbs_search);
        // exit();

        $product_limit = array();
        if (!empty($product_relate)) {
            foreach ($product_relate as $key => $product_relate) {
                if (count($product_limit) <= 7 && $product_relate->{'product::id'} != $id) {
                    array_push($product_limit, $product_relate);
                }
            }
        }

        if (!empty($check)) {
            // echo '<pre>';
            // print_r($product_list);
            // exit();
            echo json_encode($product_list);
        } else {
            $this->load->view("Details", compact('product_list', 'product_limit', 'breadcrumbs_search'));
        }
    }

    public function addCart()
    {
        // echo '<pre>';
        // print_r($this->input->post());
        // exit();

        $cart_list = file_get_contents($this->api_url . 'api_area/product/get_cart/' . $this->input->post('member_id'));
        $cart_list = json_decode($cart_list);

        if ($cart_list->message == 'The server respone success.') {
            $cart_list = $cart_list->data->product_carts;
            $search_result = array_search($this->input->post('product_id'), array_column($cart_list, 'product::id'));
            // echo '<pre>';
            // print_r('search key ' . $search_result);
            // exit();
        } else {
            $search_result = '';
        }

        if (strval($search_result) == "") {

            // print_r(' add_cart');
            // exit();

            $postdata = http_build_query(
                array(
                    'member_id' => $this->input->post('member_id'),
                    'product_id' => $this->input->post('product_id'),
                    'qty' => $this->input->post('quantity')
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);


            $add_cart = file_get_contents("https://nextere.space/api_area/product/add_cart", false, $context);
            $add_cart = json_decode($add_cart);

            // echo '<pre>';
            // print_r($add_cart);
            // exit();

            $this->load->library("response_library");
            $this->response_library->responseJSON($add_cart->code, $add_cart->message, $add_cart->data);
        } else {

            foreach ($cart_list as $key => $check_cart) {
                if ($check_cart->{'product::id'} == $this->input->post('product_id')) {
                    $get_cart_id = $check_cart->{'cart::id'};
                }
            }
            // print_r($cart_list);
            // exit();

            $postdata = http_build_query(
                array(
                    'member_id' => $this->input->post('member_id'),
                    'cart_id' => $get_cart_id,
                    'action' => 'plus'
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $update_cart = file_get_contents("https://nextere.space/api_area/product/update_cart", false, $context);
            $update_cart = json_decode($update_cart);

            // echo '<pre>';
            // print_r($update_cart);
            // exit();

            $this->load->library("response_library");
            $this->response_library->responseJSON($update_cart->code, $update_cart->message, $update_cart->data);
        }
    }

    public function removeCart()
    {
        $remove_cart = file_get_contents('http://nextere.space/api_area/product/delete_cart/' . $this->input->post('cart_id'));
        $remove_cart = json_decode($remove_cart);

        $this->load->library("response_library");
        $this->response_library->responseJSON($remove_cart->code, $remove_cart->message, $remove_cart->data);
    }

    public function updateCart()
    {
        $cart_list = file_get_contents($this->api_url . 'api_area/product/get_cart/' . $this->input->post('member_id'));
        $cart_list = json_decode($cart_list);

        if ($cart_list->message == 'The server respone success.') {
            $cart_list = $cart_list->data->product_carts;
            $search_result = array_search($this->input->post('product_id'), array_column($cart_list, 'product::id'));
        } else {
            $search_result = '';
        }



        // $cart_list = $cart_list->data->product_carts;
        $product_item_amount = '';


        foreach ($cart_list as $key => $search_cart) {

            if ($search_cart->{'cart::id'} == $this->input->post('cart_id')) {

                $product_item_amount = $search_cart->{'cart::qty'};
            }
        }
        // echo '<pre>';
        // print_r($this->input->post('action'));
        // exit();
        //  echo '<pre>';
        // print_r($product_item_amount);
        //  exit();

        if ($product_item_amount == '1') {
            if ($this->input->post('action') == 'minus') {
                $update_cart = file_get_contents('http://nextere.space/api_area/product/delete_cart/' . $this->input->post('cart_id'));
                $update_cart = json_decode($update_cart);
            } else {
                $postdata = http_build_query(
                    array(
                        'member_id' => $this->input->post('member_id'),
                        'cart_id' => $this->input->post('cart_id'),
                        'action' => $this->input->post('action')
                    )
                );

                $opts = array(
                    'http' =>
                    array(
                        'method' => 'POST',
                        'header' => array(
                            'Content-type: application/x-www-form-urlencoded'
                        ),
                        'content' => $postdata
                    )
                );

                $context = stream_context_create($opts);

                $update_cart = file_get_contents("https://nextere.space/api_area/product/update_cart", false, $context);
                $update_cart = json_decode($update_cart);
            }
            $this->load->library("response_library");
            $this->response_library->responseJSON($update_cart->code, $update_cart->message, $update_cart->data);
        } else {
            $postdata = http_build_query(
                array(
                    'member_id' => $this->input->post('member_id'),
                    'cart_id' => $this->input->post('cart_id'),
                    'action' => $this->input->post('action')
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $update_cart = file_get_contents("https://nextere.space/api_area/product/update_cart", false, $context);
            $update_cart = json_decode($update_cart);
        }

        $this->load->library("response_library");
        $this->response_library->responseJSON($update_cart->code, $update_cart->message, $update_cart->data);
    }

    public function compare_product()
    {
        if ($this->input->post()) {
            // echo '<pre>';
            // print_r($this->input->post('product_compare_list'));
            // exit();

            $product_compare_list = explode(',', $this->input->post('product_compare_list'));

            if (count($product_compare_list) == 1) {
                $product1 = $product_compare_list[0];
                $product2 = '';
                $product3 = '';
            } else if (count($product_compare_list) == 2) {
                $product1 = $product_compare_list[0];
                $product2 = $product_compare_list[1];
                $product3 = '';
            } else {
                $product1 = $product_compare_list[0];
                $product2 = $product_compare_list[1];
                $product3 = $product_compare_list[2];
            }




            $postdata = http_build_query(
                array(
                    'product1' => $product1,
                    'product2' => $product2,
                    'product3' => $product3,
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $compare_product = file_get_contents($this->api_url . "api_area/product/product_compare/", false, $context);
            $compare_product = json_decode($compare_product);

            $this->load->library("response_library");
            $this->response_library->responseJSON($compare_product->code, $compare_product->message, $compare_product->data);
        }
    }

    public function quotation()
    {
        if ($this->input->post()) {

            // echo '<pre>';
            // print_r($this->input->post());
            // exit();

            $product_list = $this->input->post();



            // if ($product_list == null) {
            //     redirect(base_url('Product/shopping-cart'));
            // }

            $business_type = file_get_contents($this->api_url . "api_area/home/business_type");

            $business_type = json_decode($business_type);

            $business_type = $business_type->data->business_types;

            // echo '<pre>';
            // print_r($business_type);
            // exit();

            $this->load->view("Quotation-Information", compact('product_list', 'business_type'));
        }
    }


    public function request_quotation()
    {
        if ($this->session->userdata('member::token') != "") {

            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );



            $context = stream_context_create($opts);

            $id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $id = json_decode($id);

            $id =  $id->data->member->id;



            if ($this->input->post()) {


                $postdata = http_build_query(
                    array(
                        'member_id' => $id,
                        'first_name' => $this->input->post('name'),
                        'last_name' => $this->input->post('lastname'),
                        'email' => $this->input->post('email'),
                        'telephone' => $this->input->post('telephone'),
                        'message' => $this->input->post('message'),
                        'product_id' => $this->input->post('product_id'),
                        'quantity' => $this->input->post('quantity'),
                        'price_per_unit' => $this->input->post('price_per_unit'),
                        'real_price' => $this->input->post('real_price'),
                        'total_price' => $this->input->post('total_price'),
                        'tax_id' => $this->input->post('tax_id'),
                        'company' => $this->input->post('company'),
                        'business_type' => $this->input->post('business_type'),
                        'type' => $this->input->post('type'),
                    )
                );

                $opts = array(
                    'http' =>
                    array(
                        'method' => 'POST',
                        'header' => array(
                            'Content-type: application/x-www-form-urlencoded'
                        ),
                        'content' => $postdata
                    )
                );

                // echo "<pre>";
                // print_r($postdata);
                // exit();

                $context = stream_context_create($opts);

                $quotation = file_get_contents($this->api_url . "api_area/product/quotation/", false, $context);

                $quotation = json_decode($quotation);

                // echo '<pre>';
                // print_r($quotation);
                // exit();

                $this->load->library("response_library");

                $this->response_library->responseJSON($quotation->code, $quotation->message, $quotation->data);

                // $this->load->view("Quotation-Information");
            }
        } else {
            redirect(base_url('account/login'));
        }
    }

    public function bill_quotation($quotation_id = null)
    {
        // echo '<pre>';
        // print_r($quotation_id);
        // exit();

        $postdata = http_build_query(
            array(
                'token' => $this->session->userdata('member::token'),
            )
        );

        $opts = array(
            'http' =>
            array(
                'method' => 'POST',
                'header' => array(
                    'Content-type: application/x-www-form-urlencoded'
                ),
                'content' => $postdata
            )
        );

        $context = stream_context_create($opts);

        $id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

        $id = json_decode($id);

        $id =  $id->data->member->id;

        // echo '<pre>';
        // print_r($id);
        // exit();



        $bill_quotation = file_get_contents($this->api_url . "api_area/product/get_quotation/" . $id);

        $bill_quotation = json_decode($bill_quotation);

        $bill_quotation = $bill_quotation->data->quotations;

        $quotation_detail = array();

        foreach ($bill_quotation as $key => $quotation) {
            if ($quotation_id == $quotation->id) {
                array_push($quotation_detail, $quotation);
            }
        }

        // echo '<pre>';
        // print_r($quotation_detail);
        // exit();

        $this->load->view("Bill-Quotations", compact('quotation_detail'));
    }

    public function coupon()
    {
        if ($this->input->post()) {

            // echo 'pre';
            // print_r($this->input->post());
            // exit();

            $postdata = http_build_query(
                array(
                    'code' => $this->input->post('coupon_id'),
                    'products' => $this->input->post('product_coupon'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            // echo "<pre>";
            // print_r($postdata);
            // exit();

            $context = stream_context_create($opts);

            $coupon = file_get_contents($this->api_url . "api_area/Product/check_coupon", false, $context);

            $coupon = json_decode($coupon);

            // echo "<pre>";
            // print_r($coupon);
            // exit();

            $this->load->library("response_library");

            $this->response_library->responseJSON($coupon->code, $coupon->message, $coupon->data);
        }
    }

    public function shopping_cart()
    {
        $this->load->view("shopping-cart");
    }

    public function data_address()
    {
        if ($this->input->post()) {

            // echo '<pre>';
            // print_r($this->input->post());
            // exit();

            $member_id = $this->input->post('id');

            $data_address = file_get_contents($this->api_url . "api_area/member/address_all/" . $member_id);
            $data_address = json_decode($data_address);
            if ($data_address->data->member_address == null) {
                $data_address = $data_address->data->member_address;
            } else {
                $data_address = $data_address->data->member_address[0];

                if ($member_id == $data_address->member_id) {

                    $address_province = $this->get_province($data_address->address_province_id);
                    $data_address->province = $address_province;
                }

                if ($member_id == $data_address->member_id) {

                    $address_district = $this->get_district($data_address->address_district_id, $data_address->address_province_id);
                    $data_address->district = $address_district;
                }

                if ($member_id == $data_address->member_id) {

                    $address_sub_district = $this->get_sub_district($data_address->address_sub_district_id, $data_address->address_district_id);
                    $data_address->sub_district = $address_sub_district;
                }
            }

            // echo "<pre>";
            // print_r($data_address);
            // exit();
            // $test = 'abcdefghij';

            $this->load->library("response_library");

            $this->response_library->responseJSON('0x0000-00000', 'respone success', $data_address);
        }
    }

    public function data_address_all()
    {
        if ($this->input->post()) {

            // echo '<pre>';
            // print_r($this->input->post());
            // exit();

            $member_id = $this->input->post('id');

            $address_all = file_get_contents($this->api_url . "api_area/member/address_all/" . $member_id);
            $address_all = json_decode($address_all);
            $address_all =  $address_all->data->member_address;

            foreach ($address_all as $key => $address_detail) {
                if ($member_id == $address_detail->member_id) {
                    $address_province = $this->get_province($address_detail->address_province_id);
                    $address_detail->province = $address_province;
                }
            }

            foreach ($address_all as $key => $address_detail) {
                if ($member_id == $address_detail->member_id) {
                    $address_district = $this->get_district($address_detail->address_district_id, $address_detail->address_province_id);
                    $address_detail->district = $address_district;
                }
            }

            foreach ($address_all as $key => $address_detail) {
                if ($member_id == $address_detail->member_id) {

                    $address_sub_district = $this->get_sub_district($address_detail->address_sub_district_id, $address_detail->address_district_id);
                    $address_detail->sub_district = $address_sub_district;
                    // array_push($address_limit, $address);
                }
            }

            // echo "<pre>";
            // print_r($data_address);
            // exit();
            // $test = 'abcdefghij';

            $this->load->library("response_library");

            $this->response_library->responseJSON('0x0000-00000', 'respone success', $address_all);
        }
    }

    public function payment()
    {
        if (!empty($this->session->userdata('member::token'))) {

            if ($this->input->post()) {
                // echo '<pre>';
                // print_r($this->input->post());
                // exit();

                $product_list = $this->input->post();

                // echo '<pre>';
                // print_r($product_list);
                // exit();

                $postdata = http_build_query(
                    array(
                        'token' => $this->session->userdata('member::token'),
                    )
                );

                $opts = array(
                    'http' =>
                    array(
                        'method' => 'POST',
                        'header' => array(
                            'Content-type: application/x-www-form-urlencoded'
                        ),
                        'content' => $postdata
                    )
                );

                $context = stream_context_create($opts);

                $id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);
                $id = json_decode($id);
                $id =  $id->data->member->id;

                $conditions_list = file_get_contents('http://nextere.space/api_area/home/conditions');
                $conditions_list = json_decode($conditions_list);
                $conditions_list = $conditions_list->data->conditions;

                // echo "<pre>";
                // print_r($id);
                // exit();

                $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);
                $user_info = json_decode($user_info);
                $user_info =  $user_info->data->member;

                $address = file_get_contents($this->api_url . "api_area/member/address_all/" . $id);
                $address = json_decode($address);
                if ($address->data->member_address == null) {
                    $address = $address->data->member_address;
                } else {
                    $address = $address->data->member_address[0];

                    if ($id == $address->member_id) {

                        $address_province = $this->get_province($address->address_province_id);
                        $address->province = $address_province;
                    }

                    if ($id == $address->member_id) {

                        $address_district = $this->get_district($address->address_district_id, $address->address_province_id);
                        $address->district = $address_district;
                    }

                    if ($id == $address->member_id) {

                        $address_sub_district = $this->get_sub_district($address->address_sub_district_id, $address->address_district_id);
                        $address->sub_district = $address_sub_district;
                    }
                }

                $address_all = file_get_contents($this->api_url . "api_area/member/address_all/" . $id);
                $address_all = json_decode($address_all);
                $address_all =  $address_all->data->member_address;

                foreach ($address_all as $key => $address_detail) {
                    if ($id == $address_detail->member_id) {
                        $address_province = $this->get_province($address_detail->address_province_id);
                        $address_detail->province = $address_province;
                    }
                }

                foreach ($address_all as $key => $address_detail) {
                    if ($id == $address_detail->member_id) {
                        $address_district = $this->get_district($address_detail->address_district_id, $address_detail->address_province_id);
                        $address_detail->district = $address_district;
                    }
                }

                foreach ($address_all as $key => $address_detail) {
                    if ($id == $address_detail->member_id) {

                        $address_sub_district = $this->get_sub_district($address_detail->address_sub_district_id, $address_detail->address_district_id);
                        $address_detail->sub_district = $address_sub_district;
                        // array_push($address_limit, $address);
                    }
                }

                $tax_address = file_get_contents($this->api_url . "api_area/member/address_tax_all/" . $id);
                $tax_address = json_decode($tax_address);
                if ($tax_address->data->member_address == null) {
                    $tax_address =  $tax_address->data->member_address;
                } else {
                    $tax_address = $tax_address->data->member_address[0];

                    if ($id == $tax_address->member_id) {
                        $address_province = $this->get_province($tax_address->address_province_id);
                        $tax_address->province = $address_province;
                    }
                    if ($id == $tax_address->member_id) {
                        $address_district = $this->get_district($tax_address->address_district_id, $tax_address->address_province_id);
                        $tax_address->district = $address_district;
                    }

                    if ($id == $tax_address->member_id) {
                        $address_sub_district = $this->get_sub_district($tax_address->address_sub_district_id, $tax_address->address_district_id);
                        $tax_address->sub_district = $address_sub_district;
                    }
                }

                $tax_address_all = file_get_contents($this->api_url . "api_area/member/address_tax_all/" . $id);
                $tax_address_all = json_decode($tax_address_all);
                $tax_address_all =  $tax_address_all->data->member_address;

                foreach ($tax_address_all as $key => $tax_address_all_detail) {
                    if ($id == $tax_address_all_detail->member_id) {
                        $address_province = $this->get_province($tax_address_all_detail->address_province_id);
                        $tax_address_all_detail->province = $address_province;
                    }
                }

                foreach ($tax_address_all as $key => $tax_address_all_detail) {
                    if ($id == $tax_address_all_detail->member_id) {
                        $address_district = $this->get_district($tax_address_all_detail->address_district_id, $tax_address_all_detail->address_province_id);
                        $tax_address_all_detail->district = $address_district;
                    }
                }

                foreach ($tax_address_all as $key => $tax_address_all_detail) {
                    if ($id == $tax_address_all_detail->member_id) {
                        $address_sub_district = $this->get_sub_district($tax_address_all_detail->address_sub_district_id, $tax_address_all_detail->address_district_id);
                        $tax_address_all_detail->sub_district = $address_sub_district;
                    }
                }
                // echo "<pre>";
                // print_r($tax_address);
                // exit();

                $api_url =  $this->api_url;

                // echo "<pre>";
                // print_r($address);
                // exit();

                $this->load->view("Payment", compact('address', 'tax_address', 'product_list', 'address_all', 'api_url', 'conditions_list', 'tax_address_all'));
            }
        } else {
            redirect(base_url('account/login'));
        }
    }

    public function payment_creditcard()
    {
        if ($this->input->post()) {

            // echo 'pre';
            // print_r($this->input->post());
            // exit();

            $postdata = http_build_query(
                array(
                    // 'member_id' => $id,
                    'member_id' => $this->input->post('member_id'),
                    'member_address_id' => $this->input->post('member_address_id'),
                    'member_address_tax_id' => $this->input->post('member_address_tax_id'),
                    'payment_id' => $this->input->post('payment_id'),
                    'promotion_discount_id' => $this->input->post('promotion_discount_id'),
                    'coupon_id' => $this->input->post('coupon_id'),
                    'product_price' => $this->input->post('product_price'),
                    'delivery_price' => $this->input->post('delivery_price'),
                    'discount' => $this->input->post('discount'),
                    'net_price' => $this->input->post('net_price'),
                    'product_id' => $this->input->post('product_id'),
                    'product_retail_price' => $this->input->post('product_retail_price'),
                    'product_qty' => $this->input->post('product_qty'),
                    'product_total_price' => $this->input->post('product_total_price'),
                    'courier_name' => $this->input->post('courier_name'),
                    'courier_code' => $this->input->post('courier_code'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );


            $context = stream_context_create($opts);

            $payment = file_get_contents($this->api_url . "api_area/Payment/CreditCardPayment", false, $context);

            $payment = json_decode($payment);

            // echo "<pre>";
            // print_r($address_add);
            // exit();

            $this->load->library("response_library");

            $this->response_library->responseJSON($payment->code, $payment->message, $payment->data);

            // $this->load->view("submit_payment");
        }
    }

    public function payment_internetbanking()
    {

        if ($this->input->post()) {

            // echo 'pre';
            // print_r($this->input->post());
            // exit();

            $postdata = http_build_query(
                array(
                    // 'member_id' => $id,
                    'member_id' => $this->input->post('member_id'),
                    'member_address_id' => $this->input->post('member_address_id'),
                    'member_address_tax_id' => $this->input->post('member_address_tax_id'),
                    'payment_id' => $this->input->post('payment_id'),
                    'bank_code' => $this->input->post('bank_code'),
                    'promotion_discount_id' => $this->input->post('promotion_discount_id'),
                    'coupon_id' => $this->input->post('coupon_id'),
                    'product_price' => $this->input->post('product_price'),
                    'delivery_price' => $this->input->post('delivery_price'),
                    'discount' => $this->input->post('discount'),
                    'net_price' => $this->input->post('net_price'),
                    'product_id' => $this->input->post('product_id'),
                    'product_retail_price' => $this->input->post('product_retail_price'),
                    'product_qty' => $this->input->post('product_qty'),
                    'product_total_price' => $this->input->post('product_total_price'),
                    'courier_name' => $this->input->post('courier_name'),
                    'courier_code' => $this->input->post('courier_code'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );


            $context = stream_context_create($opts);

            $payment = file_get_contents($this->api_url . "api_area/Payment/InternetbankingPayment", false, $context);

            $payment = json_decode($payment);

            // echo "<pre>";
            // print_r($address_add);
            // exit();

            $this->load->library("response_library");

            $this->response_library->responseJSON($payment->code, $payment->message, $payment->data);

            // $this->load->view("submit_payment");
        }
    }

    public function payment_qrcode()
    {
        if ($this->input->post()) {

            // echo 'pre';
            // print_r($this->input->post());
            // exit();

            $postdata = http_build_query(
                array(
                    // 'member_id' => $id,
                    'member_id' => $this->input->post('member_id'),
                    'member_address_id' => $this->input->post('member_address_id'),
                    'member_address_tax_id' => $this->input->post('member_address_tax_id'),
                    'payment_id' => $this->input->post('payment_id'),
                    'promotion_discount_id' => $this->input->post('promotion_discount_id'),
                    'coupon_id' => $this->input->post('coupon_id'),
                    'product_price' => $this->input->post('product_price'),
                    'delivery_price' => $this->input->post('delivery_price'),
                    'discount' => $this->input->post('discount'),
                    'net_price' => $this->input->post('net_price'),
                    'product_id' => $this->input->post('product_id'),
                    'product_retail_price' => $this->input->post('product_retail_price'),
                    'product_qty' => $this->input->post('product_qty'),
                    'product_total_price' => $this->input->post('product_total_price'),
                    'courier_name' => $this->input->post('courier_name'),
                    'courier_code' => $this->input->post('courier_code'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );


            $context = stream_context_create($opts);

            $payment = file_get_contents($this->api_url . "api_area/Payment/qrPayment", false, $context);

            $payment = json_decode($payment);

            // echo "<pre>";
            // print_r($address_add);
            // exit();

            $this->load->library("response_library");

            $this->response_library->responseJSON($payment->code, $payment->message, $payment->data);

            // $this->load->view("submit_payment");
        }
    }

    public function bill()
    {
        if ($this->session->userdata('member::token') != "") {
            $txn_id = $this->input->get("transactionId");
            // echo '<pre>';
            // print_r($txn_id);
            // exit();

            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $user_info = json_decode($user_info);

            $user_info =  $user_info->data->member;

            $member_id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $member_id = json_decode($member_id);

            $member_id =  $member_id->data->member->id;

            $order_detail = file_get_contents($this->api_url . "api_area/member/product_history/" . $member_id);
            $order_detail = json_decode($order_detail);
            $order_detail = $order_detail->data->purchase_transactions;

            $order_limit = array();

            foreach ($order_detail as $key => $order_detail) {
                if ($txn_id == $order_detail->txn_id) {
                    array_push($order_limit, $order_detail);
                }
            }

            // echo "<pre>";
            // print_r($user_info);
            // exit();

            $order_product_history = array();

            foreach ($order_detail->purchase_transaction_detail as $key => $order_product_search) {
                $product_list = file_get_contents($this->api_url . 'api_area/product/product_detail/' . $order_product_search->product_id);
                $product_list = json_decode($product_list);
                // echo "<pre>";
                // print_r($order_product_search->product_id);
                // exit();
                $product_list = $product_list->data->product_main;
                array_push($order_product_history, $product_list);
            }

            // echo "<pre>";
            // print_r($order_product_history);
            // exit();

            $this->load->view("Bill", compact('order_limit', 'order_product_history', 'user_info'));
        } else {
            redirect(base_url('account/login'));
        }
    }

    public function bill_fail()
    {
        $this->load->view("Bill_fail");
    }

    function get_province($id = null)
    {
        if (!empty($id)) {
            $address_province = file_get_contents($this->api_url . "api_area/home/select2/address_province");

            $address_province = json_decode($address_province);

            $address_province =  $address_province->data;

            foreach ($address_province as $key => $value) {
                if ($value->id == $id) {
                    return $value;
                }
            }
        } else {
            return new stdClass();
        }
    }

    function get_district($id = null, $province_id = null)
    {
        if (!empty($id)) {
            $address_district = file_get_contents($this->api_url . "api_area/home/select2/address_district/" . $province_id);

            $address_district = json_decode($address_district);

            $address_district =  $address_district->data;

            foreach ($address_district as $key => $value) {
                if ($value->id == $id) {
                    return $value;
                }
            }
        } else {
            return new stdClass();
        }
    }

    function get_sub_district($id = null, $district_id = null)
    {
        if (!empty($id)) {
            $address_sub_district = file_get_contents($this->api_url . "api_area/home/select2/address_sub_district/" . $district_id);

            $address_sub_district = json_decode($address_sub_district);

            $address_sub_district =  $address_sub_district->data;

            foreach ($address_sub_district as $key => $value) {
                if ($value->id == $id) {
                    return $value;
                }
            }
        } else {
            return new stdClass();
        }
    }

    public function get_postcode()
    {
        if ($this->input->post()) {

            $postcode = file_get_contents($this->api_url . "api_area/home/select2/address_district/" . $this->input->post('province_id') . '?search=' . $this->input->post('district_name'));

            $postcode = json_decode($postcode);

            // $postcode = $postcode->data[0];

            // echo "<pre>";
            // print_r($postcode);
            // exit();

            $this->load->library("response_library");

            $this->response_library->responseJSON($postcode->code, $postcode->message, $postcode->data[0]);
        }
    }
}
// base_url/product/detail/product_id