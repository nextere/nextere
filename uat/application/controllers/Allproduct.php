<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Allproduct extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("Allproduct");
    }
}
