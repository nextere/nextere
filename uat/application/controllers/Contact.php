<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Contact extends CI_Controller
{
    var $api_url;
    function __construct()
    {

        parent::__construct();
        $this->api_url = $this->config->item('WEBSITE_API_URL');
    }

    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        if ($this->input->post()) {

            // echo 'pre';
            // print_r($this->input->post());
            // exit();

            $postdata = http_build_query(
                array(
                    'fullname' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'telephone' => $this->input->post('telephone'),
                    'company' => $this->input->post('company'),
                    'message' => $this->input->post('message'),
                    'contact_title_id' => $this->input->post('topic'),
                    'is_reply' => 0
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $contact = file_get_contents($this->api_url . "api_area/home/contact_form", false, $context);

            $contact = json_decode($contact);

            // echo "<pre>";
            // print_r($this->input->post());
            // exit();

            $this->load->library("response_library");

            $this->response_library->responseJSON($contact->code, $contact->message, $contact->data);
        } else {
            // $this->load->view("Contact-us");

            $contact_title = file_get_contents($this->api_url . "api_area/home/contact_title");

            $contact_title = json_decode($contact_title);

            $contact_title = $contact_title->data->contact_titles;
            function comparator_1($a, $b)
            {
                return $a->name_th < $b->name_th ? -1 : 1;
            };
            usort($contact_title, "comparator_1");

            // echo '<pre>';
            // print_r($contact_title);
            // exit();

            // $this->load->view("Contact-us", compact());
        }

        $contactus_list = file_get_contents($this->api_url . "api_area/home/contactus");
        $contactus_list = json_decode($contactus_list);
        $contactus_list =  $contactus_list->data->contactus;
        // echo '<pre>';
        // print_r($contactus_list);
        // exit();
        $this->load->view("Contact-us", compact('contactus_list', 'contact_title'));
    }
}
