<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Doorlock extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("Doorlock");
    }
}
