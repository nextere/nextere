<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Index extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        // echo '<pre>';
        // print_r($this->input->post());
        // exit();
        if ($this->input->post()) {
            $postdata = http_build_query(
                array(
                    'email' => $this->input->post('email')
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $subscribe = file_get_contents("https://www.nextere.space/api_area/home/subscribe", false, $context);

            $subscribe = json_decode($subscribe);
            // echo "<pre>";
            // print_r($subscribe);
            // exit();

            $this->load->library("response_library");
            $this->response_library->responseJSON($subscribe->code, $subscribe->message, $subscribe->data);
        } else {
            $submenu = file_get_contents('https://www.nextere.space/api_area/home/menu2');
            $index_article_search = file_get_contents('https://www.nextere.space/api_area/home/article_list');

            $submenu = json_decode($submenu);
            $submenu = $submenu->data;

            $index_article_search = json_decode($index_article_search);
            $index_article_search = $index_article_search->data->articles;

            $index_article = array();

            foreach ($index_article_search as $key => $index_limited) {
                if ($key <= 4) {
                    array_push($index_article, $index_limited);
                }
            }

            function sorting_date($a, $b)
            {
                return strtotime($a->modified_date) - strtotime($b->modified_date);
            }

            usort($index_article, 'sorting_date');


            // echo '<pre>';
            // print_r($product_category);
            // exit();


            $this->load->view("header");


            // $this->load->view("footer");
        }
        $block_list = file_get_contents('https://www.nextere.space/api_area/home/article_list');
        $block_list = json_decode($block_list);
        // echo '<pre>';
        // print_r($block_list);
        // exit();
        $block_list = $block_list->data->articles;
        $block_limit = array();
        foreach ($block_list as $key => $block_show_list) {
            if (count($block_limit) <= 4 && $block_show_list->id) {
                array_push($block_limit, $block_show_list);
            }
        }
        $block_list = $block_limit;
        // echo '<pre>';
        // print_r($block_list);
        // exit();

        $partner_list = file_get_contents('http://nextere.space/api_area/home/partner');
        $partner_list = json_decode($partner_list);
        // echo '<pre>';
        // print_r($block_list);
        // exit();
        $partner_list = $partner_list->data->partners;

        $solution_list = file_get_contents('http://www.nextere.space/api_area/home/menu2');
        $solution_list = json_decode($solution_list);
        // echo '<pre>';
        // print_r($solution_list);
        // exit();
        $solution_list = $solution_list->data->menu_categories;

        $recommendt_list = file_get_contents('https://www.nextere.space/api_area/home/product_outstanding');
        $recommendt_list = json_decode($recommendt_list);
        $recommendt_list = $recommendt_list->data->recommendProducts;
        // echo '<pre>';
        // print_r($recommendt_list);
        // exit();

        $banner_list = file_get_contents('https://www.nextere.space/api_area/home/banner');
        $banner_list = json_decode($banner_list);
        $banner_list = $banner_list->data->banners;
        function comparator_3($a, $b)
        {
            return $a->sort < $b->sort ? -1 : 1;
        };
        usort($banner_list, "comparator_3");
        // echo '<pre>';
        // print_r($banner_list);
        // exit();

        $langding_page = file_get_contents('https://www.nextere.space/api_area/home/langding_page');
        $langding_page = json_decode($langding_page);
        $langding_page =  $langding_page->data->langding_page;
        // echo '<pre>';
        // print_r($langding_page);
        // exit();
        $this->load->view("Index", compact('block_list', 'submenu', 'index_article', 'partner_list', 'solution_list', 'recommendt_list', 'banner_list', 'langding_page'));
    }
}
