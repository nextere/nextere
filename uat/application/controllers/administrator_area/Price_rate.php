<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Price_rate extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/price_rate/Index");
    }

    public function edit($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("price_rate_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $price_rate = $this->price_rate_model->find($id);
        if($this->input->post())
        {
            $price_min = $this->input->post("price_min");
            $price_max = $this->input->post("price_max");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["price_min"] = $price_min;
            $dup_filter->where["price_max"] = $price_max;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->price_rate_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "Duplicate data");
            }


            if($id != null)
            {
                $price_rate = $this->price_rate_model->find($id);
                $price_rate->modified_date = date("Y-m-d h:i:s");
                $price_rate->modified_by = $this->session->userdata("__administrator::id");
            }
            else
            {
                $price_rate = new stdClass();
                $price_rate->created_date = date("Y-m-d h:i:s");
                $price_rate->created_by = $this->session->userdata("__administrator::id");
            }

            $price_rate->price_min = $price_min;
            $price_rate->price_max = $price_max;
            $price_rate->status = $this->input->post("status");
            
            $id = $this->price_rate_model->save($price_rate);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "price_rate" => $price_rate
        );
        $this->load->view("administrator_area/price_rate/Edit",$data);
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("price_rate_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $price_rate = $this->price_rate_model->find($id);
        $price_rate->modified_date = date("Y-m-d h:i:s");
        $price_rate->modified_by = $this->session->userdata("__administrator::administrator_id");
        $price_rate->status = "REMOVED";

        $this->price_rate_model->save($price_rate);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }



    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "price_rate";
            $default_filter = "price_rate.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "price_rate.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "price_rate.price_min"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "price_min", "type" => "TEXT"),
                "price_rate.price_max"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "price_max", "type" => "TEXT")
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        
    }
}