<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends CI_Controller
{
    function __construct()
	{
		parent::__construct();     
        $this->ci = & get_instance ();
	}

	public function login()
	{
		/** ------------------------------------------------------- **
		 **           CONSTRUCTION PHASE
		/** ------------------------------------------------------- **/
		$this->load->model("administrator_model");
        $this->load->model("administrator_group_permission_model");
		$this->load->library("encryption_library");
		$this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        if($this->input->post())
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');
            
            $model_filter = new stdClass();
            $model_filter->join = array(
                "administrator_group" => "administrator.administrator_group_id = administrator_group.id"
            );
            $model_filter->custom_where = "(administrator.username = '".$username."' OR administrator.email = '".$username."') AND administrator.status = 'ACTIVATE' AND administrator_group.status = 'ACTIVATE'";

            $model_filter->get_first = true;

            $administrator = $this->administrator_model->search($model_filter);
            if($administrator != null)
            {
                $check_password = false;
                if($this->encryption_library->verifyPassword($password, $administrator->{"administrator::password"}))
                {
                    $check_password = true;
                }

                if($check_password == true)
                {

                    // COLLECT USER & USER GROUP DATA
                    $this->session->set_userdata(array("__administrator::id" => $administrator->{"administrator::id"}));
                    $this->session->set_userdata(array("__administrator::username" => $administrator->{"administrator::username"}));
                    $this->session->set_userdata(array("__administrator::full_name" => $administrator->{"administrator::first_name"}." ".$administrator->{"administrator::last_name"}));
                    $this->session->set_userdata(array("__administrator_group::id" => $administrator->{"administrator_group::id"}));
                    $this->session->set_userdata(array("__administrator_group::name" => $administrator->{"administrator_group::name"}));

                    // COLLECT USER GROUP PERMISSION
                    $administrator_group_permissions = $this->administrator_group_permission_model->getAdministratorGroupPermissionsByAdministratorGroupId($administrator->{"administrator_group::id"});
                    $this->session->set_userdata(array("__administrator_group_permissions" => $administrator_group_permissions));


                    $this->response_library->responseJSON("0x0000-00000", "Login Success. Session was created.");
                }
                else
                {
                    $this->response_library->responseJSON("0x000A-AU001", "Username or Password incorrect.");
                }
            }
            else
            {
                $this->response_library->responseJSON("0x000A-AU001", "Username or Password incorrect.");
            }
            
		}

		$this->load->view('administrator_area/authentication/Login');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url("administrator_area/authentication/Login"));
	}
}