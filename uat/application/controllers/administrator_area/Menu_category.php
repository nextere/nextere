<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_category extends CI_Controller
{
    function __construct()
    { 
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/menu_category/Index");
    }

    public function edit($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("menu_category_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $menu_category = $this->menu_category_model->find($id);
        if($this->input->post())
        {
            $name_th = $this->input->post("name_th");
            $name_en = $this->input->post("name_en");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["name_th"] = $name_th;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->menu_category_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "มีการใช้ชื่อนี้แล้ว");
            }
            
            $thumbnail_part = '';
            if (!file_exists(FCPATH.'/assets/uploads/menu_catogory/thumbnail_file/')) {
                mkdir(FCPATH.'/assets/uploads/menu_catogory/thumbnail_file/', 0777, true);
            }
            if (!empty($_FILES['thumbnail_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = FCPATH.'/assets/uploads/menu_catogory/thumbnail_file/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("thumbnail_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $thumbnail_part = '/assets/uploads/menu_catogory/thumbnail_file/'.$upload_data['file_name'];
                }
                else
                {
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            }

            $banner_path = '';
            if (!file_exists(FCPATH.'/assets/uploads/menu_catogory/banner_file/')) {
                mkdir(FCPATH.'/assets/uploads/menu_catogory/banner_file/', 0777, true);
            }
            if (!empty($_FILES['banner_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = FCPATH.'/assets/uploads/menu_catogory/banner_file/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("banner_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $banner_path = '/assets/uploads/menu_catogory/banner_file/'.$upload_data['file_name'];
                }
                else
                {
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            }


            if($id != null)
            {
                $menu_category = $this->menu_category_model->find($id);
                $menu_category->modified_date = date("Y-m-d h:i:s");
                $menu_category->modified_by = $this->session->userdata("__administrator::id");

                
            }
            else
            {
                $menu_category = new stdClass();
                $menu_category->created_date = date("Y-m-d h:i:s");
                $menu_category->created_by = $this->session->userdata("__administrator::id");

                $sort_filter = new stdClass();
                $sort_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
                $counts = $this->menu_category_model->search($sort_filter);
                $count = count($counts);
                $menu_category->order = $count+1;
            }

            $menu_category->name_th = $name_th;
            $menu_category->name_en = $name_en;
            $menu_category->description_th = $this->input->post("description_th");
            $menu_category->description_en = $this->input->post("description_en");
            if(!empty($thumbnail_part))
                $menu_category->thumbnail_path = $thumbnail_part;
            if(!empty($banner_path))
                $menu_category->banner_path = $banner_path;
            $menu_category->status = $this->input->post("status");
            
            $id = $this->menu_category_model->save($menu_category);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "menu_category" => $menu_category
        );
        $this->load->view("administrator_area/menu_category/Edit",$data);
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("menu_category_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $menu_category = $this->menu_category_model->find($id);
        $menu_category->modified_date = date("Y-m-d h:i:s");
        $menu_category->modified_by = $this->session->userdata("__administrator::administrator_id");
        $menu_category->status = "REMOVED";

        $this->menu_category_model->save($menu_category);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    public function submenu($menu_category_id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("menu_category_model");
        $this->load->model("submenu_category_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $menu_category = $this->menu_category_model->find($menu_category_id);
        

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "menu_category" => $menu_category
        );
        $this->load->view("administrator_area/menu_category/Submenu",$data);
    }


    public function submenu_edit($menu_category_id = null, $id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("menu_category_model");
        $this->load->model("submenu_category_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $menu_category = $this->menu_category_model->find($menu_category_id);
        $submenu_category = $this->submenu_category_model->find($id);
        if($this->input->post())
        {
            $name_th = $this->input->post("name_th");
            $name_en = $this->input->post("name_en");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["name_th"] = $name_th;
            $dup_filter->where["menu_category_id"]  = $menu_category_id;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->submenu_category_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "มีการใช้ชื่อนี้แล้ว");
            }

            $thumbnail_part = '';
            if (!file_exists(FCPATH.'/assets/uploads/submenu_catogory/thumbnail_file/')) {
                mkdir(FCPATH.'/assets/uploads/submenu_catogory/thumbnail_file/', 0777, true);
            }
            if (!empty($_FILES['thumbnail_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = FCPATH.'/assets/uploads/submenu_catogory/thumbnail_file/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("thumbnail_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $thumbnail_part = '/assets/uploads/submenu_catogory/thumbnail_file/'.$upload_data['file_name'];
                }
                else
                {
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            }

            $banner_path = '';
            if (!file_exists(FCPATH.'/assets/uploads/submenu_catogory/banner_file/')) {
                mkdir(FCPATH.'/assets/uploads/submenu_catogory/banner_file/', 0777, true);
            }
            if (!empty($_FILES['banner_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = FCPATH.'/assets/uploads/submenu_catogory/banner_file/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("banner_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $banner_path = '/assets/uploads/submenu_catogory/banner_file/'.$upload_data['file_name'];
                }
                else
                { 
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            }

            if($id != null)
            {
                $submenu_category = $this->submenu_category_model->find($id);
                $submenu_category->modified_date = date("Y-m-d h:i:s");
                $submenu_category->modified_by = $this->session->userdata("__administrator::id");

                
            }
            else
            {
                $submenu_category = new stdClass();
                $submenu_category->created_date = date("Y-m-d h:i:s");
                $submenu_category->created_by = $this->session->userdata("__administrator::id");

                $sort_filter = new stdClass();
                $sort_filter->where["menu_category_id"]  = $menu_category_id;
                $sort_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
                $counts = $this->submenu_category_model->search($sort_filter);
                $count = count($counts);
                $submenu_category->order = $count+1;
            }

            $submenu_category->menu_category_id = $menu_category_id;
            $submenu_category->name_th = $name_th;
            $submenu_category->name_en = $name_en;
            if(!empty($thumbnail_part))
                $submenu_category->thumbnail_path = $thumbnail_part;
            if(!empty($banner_path))
                $submenu_category->banner_path = $banner_path;
            $submenu_category->status = $this->input->post("status");
            
            $id = $this->submenu_category_model->save($submenu_category);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "menu_category" => $menu_category,
            "submenu_category" => $submenu_category
        );
        $this->load->view("administrator_area/menu_category/Submenu_edit",$data);
    }


    public function submenu_delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("submenu_category_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $submenu_category = $this->submenu_category_model->find($id);
        $submenu_category->modified_date = date("Y-m-d h:i:s");
        $submenu_category->modified_by = $this->session->userdata("__administrator::administrator_id");
        $submenu_category->status = "REMOVED";

        $this->submenu_category_model->save($submenu_category);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    public function childsubmenu($menu_category_id = null, $submenu_category_id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("menu_category_model");
        $this->load->model("submenu_category_model");
        

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $menu_category = $this->menu_category_model->find($menu_category_id);
        $submenu_category = $this->submenu_category_model->find($submenu_category_id);
        

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "menu_category" => $menu_category,
            "submenu_category" => $submenu_category
        );
        $this->load->view("administrator_area/menu_category/Childsubmenu",$data);
    }

    public function childsubmenu_edit($submenu_category_id = null, $id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("menu_category_model");
        $this->load->model("submenu_category_model");
        $this->load->model("child_submenu_category_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $model_filter = new stdClass();
        $model_filter->where["submenu_category.id"] = $submenu_category_id;
        $model_filter->join = array(
            "menu_category" => "submenu_category.menu_category_id = menu_category.id"
        );
        $model_filter->get_first = true;
        $submenu_category = $this->submenu_category_model->search($model_filter);
        $child_submenu_category = $this->child_submenu_category_model->find($id);

        if($this->input->post())
        {
            $name_th = $this->input->post("name_th");
            $name_en = $this->input->post("name_en");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["name_th"] = $name_th;
            $dup_filter->where["submenu_category_id"]  = $submenu_category_id;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->child_submenu_category_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "มีการใช้ชื่อนี้แล้ว");
            }
            $thumbnail_part = '';
            if (!file_exists(FCPATH.'/assets/uploads/childsubmenu_catogory/thumbnail_file/')) {
                mkdir(FCPATH.'/assets/uploads/childsubmenu_catogory/thumbnail_file/', 0777, true);
            }
            if (!empty($_FILES['thumbnail_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = FCPATH.'/assets/uploads/childsubmenu_catogory/thumbnail_file/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("thumbnail_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $thumbnail_part = '/assets/uploads/childsubmenu_catogory/thumbnail_file/'.$upload_data['file_name'];
                }
                else
                {
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            }

            $banner_path = '';
            if (!file_exists(FCPATH.'/assets/uploads/childsubmenu_catogory/banner_file/')) {
                mkdir(FCPATH.'/assets/uploads/childsubmenu_catogory/banner_file/', 0777, true);
            }
            if (!empty($_FILES['banner_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = FCPATH.'/assets/uploads/childsubmenu_catogory/banner_file/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("banner_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $banner_path = '/assets/uploads/childsubmenu_catogory/banner_file/'.$upload_data['file_name'];
                }
                else
                {  
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            }

            if($id != null)
            {
                $child_submenu_category = $this->child_submenu_category_model->find($id);
                $child_submenu_category->modified_date = date("Y-m-d h:i:s");
                $child_submenu_category->modified_by = $this->session->userdata("__administrator::id");

                
            }
            else
            {
                $child_submenu_category = new stdClass();
                $child_submenu_category->created_date = date("Y-m-d h:i:s");
                $child_submenu_category->created_by = $this->session->userdata("__administrator::id");

                $sort_filter = new stdClass();
                $sort_filter->where["submenu_category_id"]  = $submenu_category_id;
                $sort_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
                $counts = $this->child_submenu_category_model->search($sort_filter);
                $count = count($counts);
                $child_submenu_category->order = $count+1;
            }

            $child_submenu_category->submenu_category_id = $submenu_category_id;
            $child_submenu_category->name_th = $name_th;
            $child_submenu_category->name_en = $name_en;
            if(!empty($thumbnail_part))
                $child_submenu_category->thumbnail_path = $thumbnail_part;
            if(!empty($banner_path))
                $child_submenu_category->banner_path = $banner_path;
            $child_submenu_category->status = $this->input->post("status");
            
            $id = $this->child_submenu_category_model->save($child_submenu_category);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "submenu_category" => $submenu_category,
            "child_submenu_category" => $child_submenu_category
        );
        $this->load->view("administrator_area/menu_category/Childsubmenu_edit",$data);
    }

    public function childsubmenu_delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("child_submenu_category_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $child_submenu_category = $this->child_submenu_category_model->find($id);
        $child_submenu_category->modified_date = date("Y-m-d h:i:s");
        $child_submenu_category->modified_by = $this->session->userdata("__administrator::administrator_id");
        $child_submenu_category->status = "REMOVED";

        $this->child_submenu_category_model->save($child_submenu_category);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }
    public function technicalspec($menu_category_id = null, $submenu_category_id = null, $menu_childsub_category_id = null)
    {
        $this->load->model("menu_category_model");
        $this->load->model("submenu_category_model");
        $this->load->model("child_submenu_category_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");


        $menu_category = $this->menu_category_model->find($menu_category_id);
        $submenu_category = $this->submenu_category_model->find($submenu_category_id);
        $child_submenu_category = $this->child_submenu_category_model->find($menu_childsub_category_id);
        

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "menu_category" => $menu_category,
            "submenu_category" => $submenu_category,
            "child_submenu_category" => $child_submenu_category
        );
        $this->load->view("administrator_area/menu_category/Technicalspec",$data);

    }

    public function technicalspec_add($menu_category_id = null, $submenu_category_id = null, $menu_childsub_category_id = null)
    {
        $this->load->model("menu_category_model");
        $this->load->model("submenu_category_model");
        $this->load->model("child_submenu_category_model");
        $this->load->model("technical_spec_solution_model");
        $this->load->model("technical_spec_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");


        $menu_category = $this->menu_category_model->find($menu_category_id);
        $submenu_category = $this->submenu_category_model->find($submenu_category_id);
        $child_submenu_category = $this->child_submenu_category_model->find($menu_childsub_category_id);
        
        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        if ($this->input->post()) {


            if(!empty($this->input->post("technical_spec_id[]"))){

                $technical_spec_id = $this->input->post("technical_spec_id[]");
                $count = count($technical_spec_id);
                for($i=0; $i<$count; $i++) 
                {
                    if($this->input->post("technical_spec_id[".$i."]") != "")
                    {
                        $dup_filter = new stdClass();
                        $dup_filter->where["status"] = "ACTIVATE";
                        $dup_filter->where["technical_spec_id"] = $technical_spec_id[$i];
                        $dup_filter->where["menu_category_id"] = $this->input->post("menu_category_id");
                        $dup_filter->get_first = true;
                        $dup_data = $this->technical_spec_solution_model->search($dup_filter);

                        if ($dup_data == null) {

                            $technical_spec_solution = new stdClass();
                            $technical_spec_solution->technical_spec_id = $technical_spec_id[$i];
                            $technical_spec_solution->menu_category_id = $this->input->post("menu_category_id");
                            $technical_spec_solution->submenu_category_id = empty($this->input->post("submenu_category_id")) ? null : $this->input->post("submenu_category_id");
                            $technical_spec_solution->menu_childsub_category_id = empty($this->input->post("menu_childsub_category_id")) ? null : $this->input->post("menu_childsub_category_id");
                            $technical_spec_solution->status = "ACTIVATE";
                            $technical_spec_solution->created_date = date("Y-m-d h:i:s");
                            $technical_spec_solution->created_by = $this->session->userdata("__administrator::id");
                            $this->technical_spec_solution_model->save($technical_spec_solution);
                            
                        }
                    }
                }

            }
            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        $technical = new stdClass();
        $technical->where["status"] = "ACTIVATE";
        $technical_spec = $this->technical_spec_model->search($technical);


        $data = array(
            "menu_category" => $menu_category,
            "submenu_category" => $submenu_category,
            "child_submenu_category" => $child_submenu_category,
            "technical_spec" => $technical_spec
        );
        $this->load->view("administrator_area/menu_category/Technicalspec_add",$data); 

    }

    public function technicalspec_delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("technical_spec_solution_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $technical_spec_solution = $this->technical_spec_solution_model->find($id);
        $technical_spec_solution->modified_date = date("Y-m-d h:i:s");
        $technical_spec_solution->modified_by = $this->session->userdata("__administrator::administrator_id");
        $technical_spec_solution->status = "REMOVED";

        $this->technical_spec_solution_model->save($technical_spec_solution);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }


    public function technicalspec_category_delete($technical_spec_id = null, $menu_category_id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("technical_spec_solution_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $update_filter = new stdClass();
        $update_filter->where["technical_spec_id"] = $technical_spec_id;
        $update_filter->where["menu_category_id"] = $menu_category_id;
        $update_filter->where["status"] = "ACTIVATE";

        $update_data = array(
            "status" => "REMOVED",
            "modified_date" => date("Y-m-d h:i:s"),
            "modified_by" => $this->session->userdata("__administrator::administrator_id")
        );

        $this->technical_spec_solution_model->update($update_filter, $update_data);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }



    public function filter_solution($menu_category_id = null, $submenu_category_id = null, $menu_childsub_category_id = null)
    {
        $this->load->model("menu_category_model");
        $this->load->model("submenu_category_model");
        $this->load->model("child_submenu_category_model");
        $this->load->model("technical_spec_solution_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");


        $menu_category = $this->menu_category_model->find($menu_category_id);
        $submenu_category = $this->submenu_category_model->find($submenu_category_id);
        $child_submenu_category = $this->child_submenu_category_model->find($menu_childsub_category_id);

        $model_filter = new stdClass();
        $model_filter->where["technical_spec_solution.status"] = "ACTIVATE";
        $model_filter->where_in["technical_spec.status"] = array('ACTIVATE','SUSPEND');
        $model_filter->where_in["technical_spec_solution.menu_category_id"] =  $menu_category_id;
        $model_filter->custom_where = "(technical_spec_solution.menu_childsub_category_id is null OR technical_spec_solution.menu_childsub_category_id = ".$menu_childsub_category_id.")";
        $model_filter->join  = array(
            "technical_spec" => "technical_spec_solution.technical_spec_id = technical_spec.id",
            "filter_solution" => "technical_spec_solution.technical_spec_id = filter_solution.technical_spec_id AND filter_solution.child_submenu_category_id = ".$menu_childsub_category_id." AND filter_solution.status = 'ACTIVATE'"
        );
        $technical_specs = $this->technical_spec_solution_model->search($model_filter);


        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "menu_category" => $menu_category,
            "submenu_category" => $submenu_category,
            "child_submenu_category" => $child_submenu_category,
            "technical_specs" => $technical_specs
        );
        $this->load->view("administrator_area/menu_category/Filter_solution",$data);

    }

    public function edit_filter_solution($child_submenu_category_id)
    {
        $this->load->model("child_submenu_category_model");
        $this->load->model("filter_solution_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $update_filter = new stdClass();
        $update_filter->where["child_submenu_category_id"] = $child_submenu_category_id;
        $update_filter->where["status"] = "ACTIVATE";

        $update_data = array(
            "status" => "REMOVED"
        );
        $this->filter_solution_model->update($update_filter, $update_data);

        $technical_specids = $this->input->post("technical_specid");
        if ($technical_specids != null && count($technical_specids) > 0) {
            foreach ($technical_specids as $technical_spec_id) {
                $filter_solution = new stdClass();
                $filter_solution->child_submenu_category_id = $child_submenu_category_id;
                $filter_solution->technical_spec_id = $technical_spec_id;
                $filter_solution->status = "ACTIVATE";
                $filter_solution->created_date = date("Y-m-d h:i:s");
                $filter_solution->created_by = $this->session->userdata("__administrator::administrator_id");
                $this->filter_solution_model->save($filter_solution);
            }
        }
        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");

    }

    public function sort_menu()
    {
        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $this->load->model("menu_category_model");

        if($this->input->post())
        { 

            $num = $this->input->post("id[]");
            $ids = $this->input->post("id[]");

            $min = min($ids);
            foreach($ids as $key => $id)
            {

                $menu = $this->menu_category_model->find($key);
                $menu->modified_date = date("Y-m-d H:i:s");
                $menu->modified_by = $this->session->userdata("__administrator::administrator_id");
                $menu->order = $min;
                $this->menu_category_model->save($menu);

                $min++;
            }

            $menu_validated = array("code" => "0x0000-00000", "message" => "Server response success. Request process complete with no error.");

            if($menu_validated["code"] == "0x0000-00000")
            {

                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }
            else
            {
                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }

        }
    }
    public function sort_submenu()
    {
        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $this->load->model("menu_category_model");
        $this->load->model("submenu_category_model");

        if($this->input->post())
        { 

            $num = $this->input->post("id[]");
            $ids = $this->input->post("id[]");

            $min = min($ids);
            foreach($ids as $key => $id)
            {

                $menu = $this->submenu_category_model->find($key);
                $menu->modified_date = date("Y-m-d H:i:s");
                $menu->modified_by = $this->session->userdata("__administrator::administrator_id");
                $menu->order = $min;
                $this->submenu_category_model->save($menu);

                $min++;
            }

            $menu_validated = array("code" => "0x0000-00000", "message" => "Server response success. Request process complete with no error.");

            if($menu_validated["code"] == "0x0000-00000")
            {

                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }
            else
            {
                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }

        }
    }
    public function sort_chid_submenu()
    {
        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $this->load->model("menu_category_model");
        $this->load->model("submenu_category_model");
        $this->load->model("child_submenu_category_model");

        if($this->input->post())
        { 

            $num = $this->input->post("id[]");
            $ids = $this->input->post("id[]");

            $min = min($ids);
            foreach($ids as $key => $id)
            {

                $menu = $this->child_submenu_category_model->find($key);
                $menu->modified_date = date("Y-m-d H:i:s");
                $menu->modified_by = $this->session->userdata("__administrator::administrator_id");
                $menu->order = $min;
                $this->child_submenu_category_model->save($menu);

                $min++;
            }

            $menu_validated = array("code" => "0x0000-00000", "message" => "Server response success. Request process complete with no error.");

            if($menu_validated["code"] == "0x0000-00000")
            {

                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }
            else
            {
                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }

        }
    }



    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "menu_category";
            $default_filter = "menu_category.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "menu_category.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "menu_category.order"       => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "order", "type" => "TEXT"),
                "menu_category.name_th"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "name_th", "type" => "TEXT"),
                "menu_category.name_en"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "name_en", "type" => "TEXT")
                
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
        else if($function == "index_submenu")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "submenu_category";
            $default_filter = "submenu_category.status IN ('ACTIVATE','SUSPEND') AND submenu_category.menu_category_id = ".$this->input->post("menu_category_id");
            $request_filter = array(
                "submenu_category.id"           => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "submenu_category.menu_category_id" => array("method" => "LIKE", "value" => null, "alias" => "menu_category_id" , "type" => "TEXT"),
                "submenu_category.order"        => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "order", "type" => "TEXT"),
                "submenu_category.name_th"      => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "name_th", "type" => "TEXT"),
                "submenu_category.name_en"      => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "name_en", "type" => "TEXT")
                
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
        else if($function == "index_childsubmenu")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "child_submenu_category";
            $default_filter = "child_submenu_category.status IN ('ACTIVATE','SUSPEND') AND child_submenu_category.submenu_category_id = ".$this->input->post("submenu_category_id");
            $request_filter = array(
                "child_submenu_category.id"         => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "submenu_category.menu_category_id" => array("method" => "LIKE", "value" => null, "alias" => "menu_category_id" , "type" => "TEXT"),
                "child_submenu_category.submenu_category_id" => array("method" => "LIKE", "value" => null, "alias" => "submenu_category_id" , "type" => "TEXT"),
                "child_submenu_category.order"      => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "order", "type" => "TEXT"),
                "child_submenu_category.name_th"    => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "name_th", "type" => "TEXT"),
                "child_submenu_category.name_en"    => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "name_en", "type" => "TEXT"),
                
            );
            $join = array(
                "submenu_category" => "child_submenu_category.submenu_category_id = submenu_category.id"
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
        else if($function == "index_technicalspec")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "technical_spec_solution";
            $default_group = "";
            $default_filter = "technical_spec_solution.status = 'ACTIVATE' AND technical_spec.status IN ('ACTIVATE','SUSPEND') AND technical_spec_solution.menu_category_id = ".$this->input->post("menu_category_id");
            if (!empty($this->input->post('submenu_category_id'))) {
                $default_filter.=" AND (technical_spec_solution.submenu_category_id is null OR technical_spec_solution.submenu_category_id = ".$this->input->post('submenu_category_id').")";
                $request_filter = array(
                    "technical_spec_solution.id"    => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                    "technical_spec_solution.technical_spec_id"    => array("method" => "LIKE", "value" => null, "alias" => "technical_spec_id" , "type" => "TEXT"),
                    "technical_spec_solution.menu_category_id"    => array("method" => "LIKE", "value" => null, "alias" => "menu_category_id" , "type" => "TEXT"),
                    "technical_spec_solution.submenu_category_id"    => array("method" => "LIKE", "value" => null, "alias" => "submenu_category_id" , "type" => "TEXT"),
                    "technical_spec_solution.menu_childsub_category_id"    => array("method" => "LIKE", "value" => null, "alias" => "menu_childsub_category_id" , "type" => "TEXT"),
                    "technical_spec.name_th"        => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "name_th", "type" => "TEXT"),
                    "technical_spec.name_en"        => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "name_en", "type" => "TEXT")
                );
            }
            else{
                $request_filter = array(
                    "technical_spec_solution.technical_spec_id"    => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                    "technical_spec_solution.technical_spec_id"    => array("method" => "LIKE", "value" => null, "alias" => "technical_spec_id" , "type" => "TEXT"),
                    "technical_spec_solution.menu_category_id"    => array("method" => "LIKE", "value" => null, "alias" => "menu_category_id" , "type" => "TEXT"),
                    "technical_spec_solution.submenu_category_id"    => array("method" => "LIKE", "value" => null, "alias" => "submenu_category_id" , "type" => "TEXT"),
                    "technical_spec_solution.menu_childsub_category_id"    => array("method" => "LIKE", "value" => null, "alias" => "menu_childsub_category_id" , "type" => "TEXT"),
                    "technical_spec.name_th"        => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "name_th", "type" => "TEXT"),
                    "technical_spec.name_en"        => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "name_en", "type" => "TEXT")
                );
                $default_group = "technical_spec_solution.technical_spec_id,technical_spec_solution.menu_category_id ";
            }


            
            $join = array(
                "technical_spec" => "technical_spec_solution.technical_spec_id = technical_spec.id"
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join,$default_group);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join, $default_group);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join,$default_group);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        
    }
}