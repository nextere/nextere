<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/partner/Index");
    } 


    public function detail($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("partner_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $partner = $this->partner_model->find($id);
        

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "partner" => $partner
        );
        $this->load->view("administrator_area/partner/Detail",$data);
    }

    public function edit($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("partner_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $partner = $this->partner_model->find($id);
        if($this->input->post())
        {
            $name_th = $this->input->post("name_th");
            $name_en = $this->input->post("name_en");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["name_th"] = $name_th;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->partner_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "Duplicate data");
            }

            $partner_path = '';
            if (!file_exists(FCPATH.'/assets/uploads/partner/')) {
                mkdir(FCPATH.'/assets/uploads/partner/', 0777, true);
            }
            if (!empty($_FILES['thumbnail_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = FCPATH.'/assets/uploads/partner/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("thumbnail_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $partner_path = '/assets/uploads/partner/'.$upload_data['file_name'];
                }
                else
                {
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            }


            if($id != null)
            {
                $partner = $this->partner_model->find($id);
                $partner->modified_date = date("Y-m-d h:i:s");
                $partner->modified_by = $this->session->userdata("__administrator::id");

            }
            else
            {
                $partner = new stdClass();
                $partner->created_date = date("Y-m-d h:i:s");
                $partner->created_by = $this->session->userdata("__administrator::id");

                $sort_filter = new stdClass();
                $sort_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
                $counts = $this->partner_model->search($sort_filter);
                $count = count($counts);
                $partner->order = $count+1;
            }

            $partner->name_th = $name_th;
            $partner->name_en = $name_en;
            if(!empty($partner_path))
                $partner->thumbnail_path = $partner_path;
            $partner->status = $this->input->post("status");
            
            $id = $this->partner_model->save($partner);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "partner" => $partner
        );
        $this->load->view("administrator_area/partner/Edit",$data);
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("partner_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $partner = $this->partner_model->find($id);
        $partner->modified_date = date("Y-m-d h:i:s");
        $partner->modified_by = $this->session->userdata("__administrator::administrator_id");
        $partner->status = "REMOVED";

        $this->partner_model->save($partner);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    public function sort()
    {
        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $this->load->model("partner_model");

        if($this->input->post())
        { 

            $num = $this->input->post("id[]");
            $ids = $this->input->post("id[]");

            $min = min($ids);
            foreach($ids as $key => $id)
            {

                $menu = $this->partner_model->find($key);
                $menu->modified_date = date("Y-m-d H:i:s");
                $menu->modified_by = $this->session->userdata("__administrator::administrator_id");
                $menu->order = $min;
                $this->partner_model->save($menu);

                $min++;
            }

            $menu_validated = array("code" => "0x0000-00000", "message" => "Server response success. Request process complete with no error.");

            if($menu_validated["code"] == "0x0000-00000")
            {

                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }
            else
            {
                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }

        }
    }



    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "partner";
            $default_filter = "partner.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "partner.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "partner.order"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "order", "type" => "TEXT"),
                "partner.name_th"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "name_th", "type" => "TEXT"),
                "partner.name_en"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "name_en", "type" => "TEXT"),
                "partner.status"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["3"]["search"]["value"]  , "alias" => "status", "type" => "TEXT")
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        
    }
}