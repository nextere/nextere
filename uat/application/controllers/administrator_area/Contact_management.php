<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_management extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    } 
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/contact_management/Index");
    }

    public function detail($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("contact_form_model");
        $this->load->model("administrator_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $model_filter = new stdClass();
        $model_filter->where["contact_form.id"] = $id;
        $model_filter->join = array(
            "contact_title" => "contact_form.contact_title_id = contact_title.id"
        );
        $model_filter->get_first = true;
        $contact_form = $this->contact_form_model->search($model_filter);

        $administrator = $this->administrator_model->find($contact_form->{'contact_form::sendmail_by'});

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "contact_form" => $contact_form,
            "administrator" =>$administrator

        );
        $this->load->view("administrator_area/contact_management/Detail",$data);
    }

    public function confirm_reply($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("contact_form_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $contact_form = $this->contact_form_model->find($id);
        $contact_form->modified_date = date("Y-m-d h:i:s");
        $contact_form->modified_by = $this->session->userdata("__administrator::administrator_id");
        $contact_form->is_reply = 1;

        $this->contact_form_model->save($contact_form);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }




    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "contact_form";
            $default_filter = "contact_form.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "contact_form.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "contact_form.contact_title_id"          => array("method" => "LIKE", "value" => null, "alias" => "contact_title_id" , "type" => "TEXT"),
                "contact_title.name_en"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "contact_title_name", "type" => "TEXT"),
                "contact_form.fullname"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "fullname", "type" => "TEXT"),
                "contact_form.email"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "email", "type" => "TEXT"),
                "contact_form.telephone"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["3"]["search"]["value"]  , "alias" => "telephone", "type" => "TEXT"),
                "contact_form.message"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["4"]["search"]["value"]  , "alias" => "message", "type" => "TEXT"),
                "contact_title.created_date"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["5"]["search"]["value"]  , "alias" => "created_date", "type" => "SHORT_MONTH_DATETIME"),
                "contact_form.is_reply"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["4"]["search"]["value"]  , "alias" => "is_reply", "type" => "TEXT"),
            );
            $join = array(
                "contact_title" => "contact_form.contact_title_id = contact_title.id"
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        if($function == "edit_contact_title")
        {
            $this->load->model("contact_title_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_en"] = $this->input->get("search");
            }
            $contact_titles = $this->contact_title_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $contact_titles);
        }
    }
    public function sendmail(){

        $this->load->library("response_library");
        $this->load->library("encryption_library");
        $this->load->model("contact_form_model");
        $this->load->model("Contact_us_model");

        //***************** GET INPUT ***************************//
        $title = $this->input->post("title");
        $message = $this->input->post("message");
        $id = $this->input->post("id");


        $member = $this->contact_form_model->find($id);
        $contact = $this->Contact_us_model->find(1);
        if ($member == null) {
            $this->response_library->responseJSON("0x0000-00001","ไม่พบข้อมูล");
            exit();
        }

        $subject_email = "Nextere";
        $headers = "From: ".$contact->email."\r\n"."MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $email = $member->email;

        $html = "
            <html>
            <head>
            <title></title>
            </head>
            <body>
            <p>".$title."</p>
            <p>".$message."</p>
            <br>
            <br>
            </body>
            </html>
            ";

        mail($email, $subject_email, $html, $headers); 

        $member->sendmail_date = date("Y-m-d h:i:s");
        $member->sendmail_by = $this->session->userdata("__administrator::id");
        $member->sendmail_title = $title;
        $member->sendmail_message = $message;
        $member->is_reply = 1;

         $this->contact_form_model->save($member); 

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }
}