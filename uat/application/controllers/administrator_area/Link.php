<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Link extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/link/Index");
    }


    public function detail($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("link_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $link = $this->link_model->find($id);
        

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "link" => $link
        );
        $this->load->view("administrator_area/link/Detail",$data);
    }

    public function edit($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("link_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $link = $this->link_model->find($id);
        if($this->input->post())
        {
            $title_th = $this->input->post("title_th");
            $title_en = $this->input->post("title_en");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["title_th"] = $title_th;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->link_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "Duplicate data");
            }



            if($id != null)
            {
                $link = $this->link_model->find($id);
                $link->modified_date = date("Y-m-d h:i:s");
                $link->modified_by = $this->session->userdata("__administrator::id");

                


            }
            else
            {
                $link = new stdClass();
                $link->created_date = date("Y-m-d h:i:s");
                $link->created_by = $this->session->userdata("__administrator::id");

                $sort_filter = new stdClass();
                $sort_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
                $counts = $this->link_model->search($sort_filter);
                $count = count($counts);
                $link->sort = $count+1;
            }

            $link->title_th = $title_th;
            $link->title_en = $title_en;
            $link->detail_th = $this->input->post("detail_th");
            $link->detail_en = $this->input->post("detail_en");
            $link->url = $this->input->post("url");
            $link->status = $this->input->post("status");
            
            $id = $this->link_model->save($link);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        } 

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "link" => $link
        );
        $this->load->view("administrator_area/link/Edit",$data);
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("link_model");

        $this->load->library("response_library"); 

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $link = $this->link_model->find($id);
        $link->modified_date = date("Y-m-d h:i:s");
        $link->modified_by = $this->session->userdata("__administrator::administrator_id");
        $link->status = "REMOVED";

        $this->link_model->save($link);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    public function sort()
    {
        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $this->load->model("link_model");

        if($this->input->post())
        { 

            $num = $this->input->post("id[]");
            $ids = $this->input->post("id[]");

            $min = min($ids);
            foreach($ids as $key => $id)
            {

                $menu = $this->link_model->find($key);
                $menu->modified_date = date("Y-m-d H:i:s");
                $menu->modified_by = $this->session->userdata("__administrator::administrator_id");
                $menu->sort = $min;
                $this->link_model->save($menu);

                $min++;
            }

            $menu_validated = array("code" => "0x0000-00000", "message" => "Server response success. Request process complete with no error.");

            if($menu_validated["code"] == "0x0000-00000")
            {

                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }
            else
            {
                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }

        }
    }



    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "link";
            $default_filter = "link.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "link.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "link.sort"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "sort", "type" => "TEXT"),
                "link.title_th"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "title_th", "type" => "TEXT"),
                "link.title_en"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "title_en", "type" => "TEXT"),
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        
    }
}