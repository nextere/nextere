<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator_group_permission_key extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
	public function index()
	{
		if ($this->session->userdata("__administrator_group::id") != 1) {
			redirect(base_url()."home/Index");
		}
		
        $this->load->view("administrator_area/administrator_group_permission_key/Index");
	}

	public function edit($id = null)
	{
		/** ------------------------------------------------------- **
		 **           CONSTRUCT PHASE
		/** ------------------------------------------------------- **/
		$this->load->model("administrator_group_permission_key_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

		/** ------------------------------------------------------- **
		 **           PROCESS PHASE
		/** ------------------------------------------------------- **/

        $administrator_group_permission_key = $this->administrator_group_permission_key_model->find($id);
        if($this->input->post())
        {
            if($id != null)
            {
                $administrator_group_permission_key = $this->administrator_group_permission_key_model->find($id);
            }
            else
            {
                $administrator_group_permission_key = new stdClass();
            }

            $administrator_group_permission_key->key = $this->input->post("key");
            $administrator_group_permission_key->name = $this->input->post("name");
            $administrator_group_permission_key->possible_permission = $this->input->post("possible_permission");
            $administrator_group_permission_key->order = $this->input->post("order");
            $administrator_group_permission_key->status = 'ACTIVATE';

            $administrator_group_permission_key_id = $this->administrator_group_permission_key_model->save($administrator_group_permission_key);


            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

		/** ------------------------------------------------------- **
		 **           RENDER VIEW PHASE
		/** ------------------------------------------------------- **/
        $data = array(
            "administrator_group_permission_key" => $administrator_group_permission_key
        );
        $this->load->view("administrator_area/administrator_group_permission_key/Edit",$data);
	}

	public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("administrator_group_permission_key_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $administrator_group_permission_key = $this->administrator_group_permission_key_model->find($id);
        $administrator_group_permission_key->modified_date = date("Y-m-d h:i:s");
        $administrator_group_permission_key->modified_by = $this->session->userdata("__administrator::id");
        $administrator_group_permission_key->status = "REMOVED";

        $this->administrator_group_permission_key_model->save($administrator_group_permission_key);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

	public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "administrator_group_permission_key";
            $default_filter = "administrator_group_permission_key.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "administrator_group_permission_key.id"     => array("method" => "LIKE", "value" => null, "alias" => "id"  , "type" => "TEXT"),
                "administrator_group_permission_key.key"    => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "key", "type" => "TEXT"),
                "administrator_group_permission_key.name"   => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "name", "type" => "TEXT"),
                "administrator_group_permission_key.order"  => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "order", "type" => "TEXT"),
                "administrator_group_permission_key.status" => array("method" => "LIKE", "value" => $datatables_request["columns"]["3"]["search"]["value"]  , "alias" => "status", "type" => "TEXT")
            );
            $join = array( );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }
}