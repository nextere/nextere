<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_type extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/product_type/Index");
    }

    public function edit($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_type_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $product_type = $this->product_type_model->find($id);
        if($this->input->post())
        {
            $name_th = $this->input->post("name_th");
            $name_en = $this->input->post("name_en");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["name_th"] = $name_th;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->product_type_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "Duplicate data");
            }


            if($id != null)
            {
                $product_type = $this->product_type_model->find($id);
                $product_type->modified_date = date("Y-m-d h:i:s");
                $product_type->modified_by = $this->session->userdata("__administrator::id");
            }
            else
            {
                $product_type = new stdClass();
                $product_type->created_date = date("Y-m-d h:i:s");
                $product_type->created_by = $this->session->userdata("__administrator::id");
            }

            $product_type->name_th = $name_th;
            $product_type->name_en = $name_en;
            $product_type->status = $this->input->post("status");
            
            $id = $this->product_type_model->save($product_type);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "product_type" => $product_type
        );
        $this->load->view("administrator_area/product_type/Edit",$data);
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_type_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $product_type = $this->product_type_model->find($id);
        $product_type->modified_date = date("Y-m-d h:i:s");
        $product_type->modified_by = $this->session->userdata("__administrator::administrator_id");
        $product_type->status = "REMOVED";

        $this->product_type_model->save($product_type);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }



    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "product_type";
            $default_filter = "product_type.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "product_type.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "product_type.name_th"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "name_th", "type" => "TEXT"),
                "product_type.name_en"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "name_en", "type" => "TEXT")
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        
    }
}