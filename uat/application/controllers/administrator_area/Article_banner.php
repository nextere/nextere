<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article_banner extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    } 
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/article_banner/Index");
    }


    public function detail($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("article_banner_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $banner = $this->article_banner_model->find($id);
        

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "banner" => $banner
        );
        $this->load->view("administrator_area/article_banner/Detail",$data);
    }

    public function edit($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("article_banner_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $banner = $this->article_banner_model->find($id);
        if($this->input->post())
        {
            $title_th = $this->input->post("title_th");
            $title_en = $this->input->post("title_en");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["title_th"] = $title_th;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->article_banner_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "Duplicate data");
            }

            $banner_path = '';
            if (!file_exists(FCPATH.'/assets/uploads/articlebanner/')) {
                mkdir(FCPATH.'/assets/uploads/articlebanner/', 0777, true);
            }
            if (!empty($_FILES['banner_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = FCPATH.'/assets/uploads/articlebanner/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("banner_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $banner_path = '/assets/uploads/articlebanner/'.$upload_data['file_name'];
                }
                else
                {
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            }


            if($id != null)
            {
                $banner = $this->article_banner_model->find($id);
                $banner->modified_date = date("Y-m-d h:i:s");
                $banner->modified_by = $this->session->userdata("__administrator::id");

                
            }
            else
            {
                $banner = new stdClass();
                $banner->created_date = date("Y-m-d h:i:s");
                $banner->created_by = $this->session->userdata("__administrator::id");

                $sort_filter = new stdClass();
                $sort_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
                $counts = $this->article_banner_model->search($sort_filter);
                $count = count($counts);
                $banner->sort = $count+1;
            }

            $banner->title_th = $title_th;
            $banner->title_en = $title_en;
            $banner->detail_th = $this->input->post("detail_th");
            $banner->detail_en = $this->input->post("detail_en");
            $banner->url = $this->input->post("url");
            if(!empty($banner_path))
                $banner->banner_path = $banner_path;
            $banner->status = $this->input->post("status");
            
            $id = $this->article_banner_model->save($banner);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "banner" => $banner
        );
        $this->load->view("administrator_area/article_banner/Edit",$data);
    }

    public function sort()
    {
        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $this->load->model("article_banner_model");

        if($this->input->post())
        { 

            $num = $this->input->post("id[]");
            $ids = $this->input->post("id[]");

            $min = min($ids);
            foreach($ids as $key => $id)
            {

                $menu = $this->article_banner_model->find($key);
                $menu->modified_date = date("Y-m-d H:i:s");
                $menu->modified_by = $this->session->userdata("__administrator::administrator_id");
                $menu->sort = $min;
                $this->article_banner_model->save($menu);

                $min++;
            }

            $menu_validated = array("code" => "0x0000-00000", "message" => "Server response success. Request process complete with no error.");

            if($menu_validated["code"] == "0x0000-00000")
            {

                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }
            else
            {
                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }

        }
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("article_banner_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $banner = $this->article_banner_model->find($id);
        $banner->modified_date = date("Y-m-d h:i:s");
        $banner->modified_by = $this->session->userdata("__administrator::administrator_id");
        $banner->status = "REMOVED";

        $this->article_banner_model->save($banner);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }



    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "article_banner";
            $default_filter = "article_banner.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "article_banner.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "article_banner.sort"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "sort", "type" => "TEXT"),
                "article_banner.title_th"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "title_th", "type" => "TEXT"),
                "article_banner.title_en"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "title_en", "type" => "TEXT"),
                "article_banner.banner_path"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["3"]["search"]["value"]  , "alias" => "banner_path", "type" => "TEXT"),
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        
    }
}