<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_transaction extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/purchase_transaction/Index");
    }


    public function detail($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("promotion_discount_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $model_filter = new stdClass();
        $model_filter->where["promotion_discount.id"] = $id;
        $model_filter->join = array(
            "product_category" => "promotion_discount.product_category_id = product_category.id"
        );
        $model_filter->get_first = true;
        $promotion_discount = $this->promotion_discount_model->search($model_filter);
        

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "promotion_discount" => $promotion_discount
        );
        $this->load->view("administrator_area/promotion_discount/Detail",$data);
    }




    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "purchase_transaction";
            $default_filter = "";
            $request_filter = array(
                "purchase_transaction.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "purchase_transaction.purchase_transaction_no"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "purchase_transaction_no", "type" => "TEXT"),
                "member.fullname"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "fullname", "type" => "TEXT"),
                "member.email"  => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "email", "type" => "TEXT"),
                "purchase_transaction.net_price"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["3"]["search"]["value"]  , "alias" => "net_price", "type" => "TEXT"),
                "purchase_transaction.status"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["4"]["search"]["value"]  , "alias" => "status", "type" => "TEXT"),
                "purchase_transaction.created_date"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["5"]["search"]["value"]  , "alias" => "created_date", "type" => "SHORT_MONTH_DATETIME")
            );
            $join = array(
                "member" => "purchase_transaction.member_id = member.id"
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

    }
}