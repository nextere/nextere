<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    } 
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->model("contact_us_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $contact_us = $this->contact_us_model->find(1);
        if($this->input->post())
        {

            $id = $this->input->post("id");
            $company_th = $this->input->post("company_th");
            $company_en = $this->input->post("company_en");
            $tax_identification_number = $this->input->post("tax_identification_number");
            $address_th = $this->input->post("address_th");
            $address_en = $this->input->post("address_en");
            $map = $this->input->post("map");
            $work_time_th = $this->input->post("work_time_th");
            $work_time_en = $this->input->post("work_time_en");
            $email = $this->input->post("email");
            $line = $this->input->post("line");
            $facebook = $this->input->post("facebook");
            $instagram = $this->input->post("instagram");
            $telephone = $this->input->post("telephone");
            $fackbook_messenger = $this->input->post("fackbook_messenger");
            $twitter = $this->input->post("twitter");
            $phone = $this->input->post("phone");
            $email_sender = $this->input->post("email_sender");
            $email_stock = $this->input->post("email_stock");
            $vision = $this->input->post("vision");
            $mission = $this->input->post("mission");

            $company_path = '';
            if (!file_exists(FCPATH.'/assets/uploads/company/')) {
                mkdir(FCPATH.'/assets/uploads/company/', 0777, true);
            }
            if (!empty($_FILES['logo_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = FCPATH.'/assets/uploads/company/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("logo_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $company_path = '/assets/uploads/company/'.$upload_data['file_name'];
                }
                else
                {
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            }
            
            if($id != null)
            {
                $contact_us = $this->contact_us_model->find($id);
                $contact_us->update_date = date("Y-m-d h:i:s");
                $contact_us->update_by = $this->session->userdata("__administrator::id");
            }

            $contact_us->company_th = $company_th;
            $contact_us->company_en = $company_en;
            $contact_us->tax_identification_number = $tax_identification_number;
            $contact_us->address_th = $address_th;
            $contact_us->address_en = $address_en;
            $contact_us->map = $map;
            $contact_us->work_time_th = $work_time_th;
            $contact_us->work_time_en = $work_time_en;
            $contact_us->email = $email;
            $contact_us->line = $line;
            $contact_us->facebook = $facebook;
            $contact_us->instagram = $instagram;
            $contact_us->telephone = $telephone;
            $contact_us->fackbook_messenger = $fackbook_messenger;
            $contact_us->twitter = $twitter;
            $contact_us->phone = $phone;
            $contact_us->email_sender = $email_sender;
            $contact_us->email_stock = $email_stock;
            $contact_us->vision = $vision;
            $contact_us->mission = $mission;
            if(!empty($company_path))
                $contact_us->logo_path = $company_path;
            
            $id = $this->contact_us_model->save($contact_us);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        $data = array(
            "contact_us" => $contact_us
        );

        $this->load->view("administrator_area/contact_us/Edit",$data);
    }


}