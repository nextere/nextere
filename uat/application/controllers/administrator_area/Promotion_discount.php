<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promotion_discount extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/promotion_discount/Index");
    }


    public function detail($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("promotion_discount_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $model_filter = new stdClass();
        $model_filter->where["promotion_discount.id"] = $id;
        $model_filter->join = array(
            "product_category" => "promotion_discount.product_category_id = product_category.id"
        );
        $model_filter->get_first = true;
        $promotion_discount = $this->promotion_discount_model->search($model_filter);
        

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "promotion_discount" => $promotion_discount
        );
        $this->load->view("administrator_area/promotion_discount/Detail",$data);
    }

    public function edit($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("promotion_discount_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $promotion_discount = $this->promotion_discount_model->find($id);
        if($this->input->post())
        {
            $name_th = $this->input->post("name_th");
            $name_en = $this->input->post("name_en");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["name_th"] = $name_th;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->promotion_discount_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "Duplicate data");
            }


            if($id != null)
            {
                $promotion_discount = $this->promotion_discount_model->find($id);
                $promotion_discount->modified_date = date("Y-m-d h:i:s");
                $promotion_discount->modified_by = $this->session->userdata("__administrator::id");
            }
            else
            {
                $promotion_discount = new stdClass();
                $promotion_discount->created_date = date("Y-m-d h:i:s");
                $promotion_discount->created_by = $this->session->userdata("__administrator::id");
            }

            $start_time = $this->input->post("start_time");
            $end_time = $this->input->post("end_time");
            $promotion_discount->name_th = $name_th;
            $promotion_discount->name_en = $name_en;
            $promotion_discount->discount_percent = empty($this->input->post("discount_percent")) ? null: $this->input->post("discount_percent")/100;
            $promotion_discount->discount_baht = empty($this->input->post("discount_baht")) ? null: $this->input->post("discount_baht");
            $promotion_discount->start_time = empty($start_time) ? null: date("Y-m-d", strtotime($start_time));
            $promotion_discount->end_time = empty($end_time) ? null: date("Y-m-d", strtotime($end_time));
            $promotion_discount->product_category_id = empty($this->input->post("product_category_id")) ? null: $this->input->post("product_category_id");
            $promotion_discount->status = $this->input->post("status");
            
            $id = $this->promotion_discount_model->save($promotion_discount);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $model_filter = new stdClass();
        $model_filter->where["promotion_discount.id"] = $id;
        $model_filter->join = array(
            "product_category" => "promotion_discount.product_category_id = product_category.id"
        );
        $model_filter->get_first = true;
        $promotion_discount = $this->promotion_discount_model->search($model_filter);
        $data = array(
            "promotion_discount" => $promotion_discount
        );
        $this->load->view("administrator_area/promotion_discount/Edit",$data);
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("promotion_discount_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $promotion_discount = $this->promotion_discount_model->find($id);
        $promotion_discount->modified_date = date("Y-m-d h:i:s");
        $promotion_discount->modified_by = $this->session->userdata("__administrator::administrator_id");
        $promotion_discount->status = "REMOVED";

        $this->promotion_discount_model->save($promotion_discount);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }



    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "promotion_discount";
            $default_filter = "promotion_discount.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "promotion_discount.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "promotion_discount.name_th"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "name_th", "type" => "TEXT"),
                "promotion_discount.name_en"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "name_en", "type" => "TEXT"),
                "promotion_discount.discount_percent"  => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "discount_percent", "type" => "TEXT"),
                "promotion_discount.discount_baht"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["3"]["search"]["value"]  , "alias" => "discount_baht", "type" => "TEXT")
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        if($function == "edit_product_category")
        {
            $this->load->model("product_category_model");
            $model_filter = new stdClass();
            $model_filter->where_in["status"] = array("ACTIVATE","ALL");
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_th"] = $this->input->get("search");
            }
            $product_categorys = $this->product_category_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $product_categorys);
        }
    }
}