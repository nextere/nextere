<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    } 
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/article/Index");
    }


    public function detail($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("article_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $article = $this->article_model->find($id);
        

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "article" => $article
        );
        $this->load->view("administrator_area/article/Detail",$data);
    }

    public function edit($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("article_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $article = $this->article_model->find($id);
        if($this->input->post())
        {
            $title_th = $this->input->post("title_th");
            $title_en = $this->input->post("title_en");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["title_th"] = $title_th;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->article_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "Duplicate data");
            }

            $thumbnail_path = '';
            if (!file_exists(FCPATH.'/assets/uploads/article/thumbnail/')) {
                mkdir(FCPATH.'/assets/uploads/article/thumbnail/', 0777, true);
            }
            if (!empty($_FILES['thumbnail_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = FCPATH.'/assets/uploads/article/thumbnail/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("thumbnail_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $thumbnail_path = '/assets/uploads/article/thumbnail/'.$upload_data['file_name'];
                }
                else
                {
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            }
            $banner_path = '';
            if (!file_exists(FCPATH.'/assets/uploads/article/banner/')) {
                mkdir(FCPATH.'/assets/uploads/article/banner/', 0777, true);
            }
            if (!empty($_FILES['banner_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = FCPATH.'/assets/uploads/article/banner/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("banner_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $banner_path = '/assets/uploads/article/banner/'.$upload_data['file_name'];
                }
                else
                {
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            }


            if($id != null)
            {
                $article = $this->article_model->find($id);
                $article->modified_date = date("Y-m-d h:i:s");
                $article->modified_by = $this->session->userdata("__administrator::id");

                
            }
            else
            {
                $article = new stdClass();
                $article->created_date = date("Y-m-d h:i:s");
                $article->created_by = $this->session->userdata("__administrator::id");

                $sort_filter = new stdClass();
                $sort_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
                $counts = $this->article_model->search($sort_filter);
                $count = count($counts);
                $article->sort = $count+1;
            }

            $article->title_th = $title_th;
            $article->title_en = $title_en;
            $article->detail_th = $this->input->post("detail_th");
            $article->detail_en = $this->input->post("detail_en");
            $article->tags = $this->input->post("tags");
            if(!empty($thumbnail_path))
                $article->thumbnail_path = $thumbnail_path;
            if(!empty($banner_path))
                $article->banner_path = $banner_path;
            $article->status = $this->input->post("status");

            $id = $this->article_model->save($article);     
            
            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "article" => $article
        );
        $this->load->view("administrator_area/article/Edit",$data);
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("article_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $article = $this->article_model->find($id);
        $article->modified_date = date("Y-m-d h:i:s");
        $article->modified_by = $this->session->userdata("__administrator::administrator_id");
        $article->status = "REMOVED";

        $this->article_model->save($article);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }
    public function sort()
    {
        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $this->load->model("article_model");

        if($this->input->post())
        { 

            $num = $this->input->post("id[]");
            $ids = $this->input->post("id[]");

            $min = min($ids);
            foreach($ids as $key => $id)
            {

                $menu = $this->article_model->find($key);
                $menu->modified_date = date("Y-m-d H:i:s");
                $menu->modified_by = $this->session->userdata("__administrator::administrator_id");
                $menu->sort = $min;
                $this->article_model->save($menu);

                $min++;
            }

            $menu_validated = array("code" => "0x0000-00000", "message" => "Server response success. Request process complete with no error.");

            if($menu_validated["code"] == "0x0000-00000")
            {

                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }
            else
            {
                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }

        }
    }

    public function edit_product_article()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("article_product_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           POST PHASE
        /** ------------------------------------------------------- **/
        if($this->input->post())
        {
            $id = $this->input->post("article_id");
            $product_id = $this->input->post("product_id");
            if (empty($product_id)) {
                $this->response_library->responseJSON("0x0API-00001", "Please input data");
            }

            $dup_filter = new stdClass();
            $dup_filter->where["article_id"] = $id;
            $dup_filter->where["product_id"] = $product_id;
            $dup_filter->where["status"] = "ACTIVATE";
            $dup_filter->get_first = true;
            $dup_data = $this->article_product_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x0API-00002", "Duplicate data");
            }


            $product_article = new stdClass();
            $product_article->created_date = date("Y-m-d h:i:s");
            $product_article->created_by = $this->session->userdata("__administrator::id");
            $product_article->status = "ACTIVATE";
            $product_article->article_id = $id;
            $product_article->product_id = $product_id;
            
            $product_article_id = $this->article_product_model->save($product_article);

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }
    }

    public function delete_product_article()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("article_product_model");
        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $id = $this->input->post("id");
        $product_detail_category = $this->article_product_model->find($id);
        $product_detail_category->modified_date = date("Y-m-d h:i:s");
        $product_detail_category->modified_by = $this->session->userdata("__administrator::id");
        $product_detail_category->status = "REMOVED";

        $this->article_product_model->save($product_detail_category);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }



    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "article";
            $default_filter = "article.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "article.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "article.sort"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "sort", "type" => "TEXT"),
                "article.title_th"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "title_th", "type" => "TEXT"),
                "article.title_en"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "title_en", "type" => "TEXT")
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
        else if($function == "index_product_article")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post(); 

            $table = "article_product";
            $default_filter = "article_product.status like 'ACTIVATE'";            
            $default_filter .= " AND article_product.article_id = ".$this->input->post("article_id");
            $request_filter = array(
                "article_product.id"              => array("method" => "LIKE", "value" => null, "alias" => "id"  , "type" => "TEXT"),
                "product.code"      => array("method" => "LIKE", "value" => null, "alias" => "code"  , "type" => "TEXT"),
                "product.name_en" => array("method" => "LIKE", "value" => null  , "alias" => "name_en", "type" => "TEXT"),
                "product.model_name_en"           => array("method" => "LIKE", "value" => null  , "alias" => "model_name_en", "type" => "TEXT"),
            );
            $join = array(
                "product" => "article_product.product_id = product.id",
             );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        if($function == "edit_product")
        {
            $this->load->model("product_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            if($this->input->get("search") != "")
            {
                $model_filter->like["code"] = $this->input->get("search");
            }
            $brands = $this->product_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $brands);
        }
        
    }
}