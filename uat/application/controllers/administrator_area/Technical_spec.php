<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ob_start();

class Technical_spec extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/technical_spec/Index");
    }

    public function edit($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("technical_spec_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $technical_spec = $this->technical_spec_model->find($id);
        if($this->input->post())
        {
            $name_th = $this->input->post("name_th");
            $name_en = $this->input->post("name_en");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["name_th"] = $name_th;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->technical_spec_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "มีการใช้ชื่อนี้แล้ว");
            }


            if($id != null) 
            {
                $technical_spec = $this->technical_spec_model->find($id);
                $technical_spec->modified_date = date("Y-m-d h:i:s");
                $technical_spec->modified_by = $this->session->userdata("__administrator::id");

                $technical_spec->order = $this->input->post("order");

            }
            else
            {
                $technical_spec = new stdClass();
                $technical_spec->created_date = date("Y-m-d h:i:s");
                $technical_spec->created_by = $this->session->userdata("__administrator::id");

                $sort_filter = new stdClass();
                $sort_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
                $counts = $this->technical_spec_model->search($sort_filter);
                $count = count($counts);
                $technical_spec->order = $count+1;
            }

            $technical_spec->name_th = $name_th;
            $technical_spec->name_en = $name_en;
            $technical_spec->status = $this->input->post("status");
            
            $id = $this->technical_spec_model->save($technical_spec);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "technical_spec" => $technical_spec
        );
        $this->load->view("administrator_area/technical_spec/Edit",$data);
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("technical_spec_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $technical_spec = $this->technical_spec_model->find($id);
        $technical_spec->modified_date = date("Y-m-d h:i:s");
        $technical_spec->modified_by = $this->session->userdata("__administrator::administrator_id");
        $technical_spec->status = "REMOVED";

        $this->technical_spec_model->save($technical_spec);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    public function sort()
    {
        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $this->load->model("technical_spec_model");

        if($this->input->post())
        { 

            $num = $this->input->post("id[]");
            $ids = $this->input->post("id[]");

            $min = min($ids);
            foreach($ids as $key => $id)
            {

                $menu = $this->technical_spec_model->find($key);
                $menu->modified_date = date("Y-m-d H:i:s");
                $menu->modified_by = $this->session->userdata("__administrator::administrator_id");
                $menu->order = $min;
                $this->technical_spec_model->save($menu);

                $min++;
            }

            $menu_validated = array("code" => "0x0000-00000", "message" => "Server response success. Request process complete with no error.");

            if($menu_validated["code"] == "0x0000-00000")
            {

                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }
            else
            {
                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }

        }
    }


    public function export_form()
    {

        $this->load->model("technical_spec_model");
        $this->load->model("product_model");

        $this->load->library('excel');

        $model_filter = new stdClass();
        $model_filter->where["status"] = "ACTIVATE";
        $technical_specs = $this->technical_spec_model->search($model_filter);

        $model_filter = new stdClass();
        $model_filter->where["status"] = "ACTIVATE";
        $model_filter->order_by = "product_type_id asc, name_en asc, model_name_en asc";
        $products = $this->product_model->search($model_filter);

        $this->load->view("administrator_area/exportexcel/Technical_spec_form", compact("technical_specs","products"));
        
    }

   
    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "technical_spec";
            $default_filter = "technical_spec.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "technical_spec.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "technical_spec.order"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "order", "type" => "TEXT"),
                "technical_spec.name_th"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "name_th", "type" => "TEXT"),
                "technical_spec.name_en"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "name_en", "type" => "TEXT"),
                "technical_spec.status"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["3"]["search"]["value"]  , "alias" => "status", "type" => "TEXT")
                
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        
    }
}