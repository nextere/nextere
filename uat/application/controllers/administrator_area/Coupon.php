<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coupon extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/coupon/Index");
    }


    public function detail($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("coupon_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $model_filter = new stdClass();
        $model_filter->where["id"] = $id;
        $model_filter->get_first = true;
        $coupon = $this->coupon_model->search($model_filter);
        

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "coupon" => $coupon
        );
        $this->load->view("administrator_area/coupon/Detail",$data);
    }

    public function edit($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("coupon_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $coupon = $this->coupon_model->find($id);
        if($this->input->post())
        {
            $code = $this->input->post("code");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["code"] = $code;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->coupon_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "Duplicate data");
            }


            if($id != null)
            {
                $coupon = $this->coupon_model->find($id);
                $coupon->modified_date = date("Y-m-d h:i:s");
                $coupon->modified_by = $this->session->userdata("__administrator::id");
                if($coupon->total_qty != $this->input->post("total_qty")){
                    $coupon->remaining_qty = $coupon->remaining_qty + ($this->input->post("total_qty") - $coupon->total_qty);
                }
            }
            else
            {
                $coupon = new stdClass();
                $coupon->created_date = date("Y-m-d h:i:s");
                $coupon->created_by = $this->session->userdata("__administrator::id");
                $coupon->total_qty = $this->input->post("total_qty");
                $coupon->remaining_qty = $coupon->total_qty;
            }
            $dates = new stdClass();
            $dates->start_time = $this->input->post("start_time");
            $dates->end_time = $this->input->post("end_time");
            $start_time = $this->input->post("start_time");
            $end_time = $this->input->post("end_time");

            $coupon->code = $code;
            $coupon->discount_percent = empty($this->input->post("discount_percent")) ? null: $this->input->post("discount_percent")/100;
            $coupon->discount_baht = empty($this->input->post("discount_baht")) ? null: $this->input->post("discount_baht");
            $coupon->start_time = empty($start_time) ? null: date("Y-m-d H:i:s", strtotime($start_time));
            $coupon->end_time = empty($end_time) ? null: date("Y-m-d H:i:s", strtotime($end_time));

           

            $coupon->is_delivery = $this->input->post("is_delivery");
            $coupon->is_onlynormalprice = $this->input->post("is_onlynormalprice")==1? 1 : 0;
            $coupon->status = $this->input->post("status");

            $id = $this->coupon_model->save($coupon);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.",$id);
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        
        $data = array(
            "coupon" => $coupon
        );
        $this->load->view("administrator_area/coupon/Edit",$data);
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("coupon_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $coupon = $this->coupon_model->find($id);
        $coupon->modified_date = date("Y-m-d h:i:s");
        $coupon->modified_by = $this->session->userdata("__administrator::administrator_id");
        $coupon->status = "REMOVED";

        $this->coupon_model->save($coupon);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }


    public function edit_expept_category()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("coupon_except_category_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           POST PHASE
        /** ------------------------------------------------------- **/
        if($this->input->post())
        {
            $coupon_id = $this->input->post("coupon_id");
            $product_category_id = $this->input->post("product_category_id");
            if (empty($coupon_id) || empty($product_category_id)) {
                $this->response_library->responseJSON("0x0API-00001", "Please input data");
            }

            $dup_filter = new stdClass();
            $dup_filter->where["coupon_id"] = $coupon_id;
            $dup_filter->where["product_category_id"] = $product_category_id;
            $dup_filter->where["status"] = "ACTIVATE";
            $dup_filter->get_first = true;
            $dup_data = $this->coupon_except_category_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x0API-00002", "Duplicate data");
            }

            $coupon_except_category = new stdClass();
            $coupon_except_category->created_date = date("Y-m-d h:i:s");
            $coupon_except_category->created_by = $this->session->userdata("__administrator::id");
            $coupon_except_category->status = "ACTIVATE";
            $coupon_except_category->coupon_id = $coupon_id;
            $coupon_except_category->product_category_id = $product_category_id;
            
            $coupon_except_category_id = $this->coupon_except_category_model->save($coupon_except_category);

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }
    }

    public function delete_expept_category()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("coupon_except_category_model");
        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $id = $this->input->post("id");
        $coupon_except_category = $this->coupon_except_category_model->find($id);
        $coupon_except_category->modified_date = date("Y-m-d h:i:s");
        $coupon_except_category->modified_by = $this->session->userdata("__administrator::id");
        $coupon_except_category->status = "REMOVED";

        $this->coupon_except_category_model->save($coupon_except_category);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    public function edit_expept_product()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("coupon_except_product_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           POST PHASE
        /** ------------------------------------------------------- **/
        if($this->input->post())
        {
            $coupon_id = $this->input->post("coupon_id");
            $product_id = $this->input->post("product_id");
            if (empty($coupon_id) || empty($product_id)) {
                $this->response_library->responseJSON("0x0API-00001", "Please input data");
            }

            $dup_filter = new stdClass();
            $dup_filter->where["coupon_id"] = $coupon_id;
            $dup_filter->where["product_id"] = $product_id;
            $dup_filter->where["status"] = "ACTIVATE";
            $dup_filter->get_first = true;
            $dup_data = $this->coupon_except_product_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x0API-00002", "Duplicate data");
            }

            $coupon_except_product = new stdClass();
            $coupon_except_product->created_date = date("Y-m-d h:i:s");
            $coupon_except_product->created_by = $this->session->userdata("__administrator::id");
            $coupon_except_product->status = "ACTIVATE";
            $coupon_except_product->coupon_id = $coupon_id;
            $coupon_except_product->product_id = $product_id;
            
            $coupon_except_product_id = $this->coupon_except_product_model->save($coupon_except_product);

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }
    }

    public function delete_expept_product()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("coupon_except_product_model");
        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $id = $this->input->post("id");
        $coupon_except_product = $this->coupon_except_product_model->find($id);
        $coupon_except_product->modified_date = date("Y-m-d h:i:s");
        $coupon_except_product->modified_by = $this->session->userdata("__administrator::id");
        $coupon_except_product->status = "REMOVED";

        $this->coupon_except_product_model->save($coupon_except_product);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }


    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "coupon";
            $default_filter = "coupon.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "coupon.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "coupon.code"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "code", "type" => "TEXT"),
                "coupon.total_qty"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "total_qty", "type" => "TEXT"),
                "coupon.remaining_qty"  => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "remaining_qty", "type" => "TEXT"),
                "coupon.discount_percent"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["3"]["search"]["value"]  , "alias" => "discount_percent", "type" => "TEXT"),
                "coupon.discount_baht"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["4"]["search"]["value"]  , "alias" => "discount_baht", "type" => "TEXT")
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
        else if ($function == "index_except_product_category") {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "coupon_except_category";
            $default_filter = "coupon_except_category.status IN ('ACTIVATE','SUSPEND')";
            $default_filter.= " AND coupon_except_category.coupon_id = ".$this->input->post("coupon_id");
            $request_filter = array(
                "coupon_except_category.id" => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "coupon_except_category.product_category_id" => array("method" => "LIKE", "value" => null, "alias" => "product_category_id" , "type" => "TEXT"),
                "product_category.name_th"  => array("method" => "LIKE", "value" => null , "alias" => "product_category_name", "type" => "TEXT")
            );
            $join = array(
                "product_category" => "coupon_except_category.product_category_id = product_category.id"
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
        else if ($function == "index_except_product") {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "coupon_except_product";
            $default_filter = "coupon_except_product.status IN ('ACTIVATE','SUSPEND')";
            $default_filter.= " AND coupon_except_product.coupon_id = ".$this->input->post("coupon_id");
            $request_filter = array(
                "coupon_except_product.id" => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "coupon_except_product.product_id" => array("method" => "LIKE", "value" => null, "alias" => "product_id" , "type" => "TEXT"),
                "product.name_th"  => array("method" => "LIKE", "value" => null , "alias" => "product_name", "type" => "TEXT"),
                "product.model_name_th"  => array("method" => "LIKE", "value" => null , "alias" => "model_name", "type" => "TEXT"),
                "product.code"  => array("method" => "LIKE", "value" => null , "alias" => "code", "type" => "TEXT")
            );
            $join = array(
                "product" => "coupon_except_product.product_id = product.id"
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        if($function == "edit_product_category")
        {
            $this->load->model("product_category_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_th"] = $this->input->get("search");
            }
            $product_categorys = $this->product_category_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $product_categorys);
        }
        else if($function == "edit_product")
        {
            $this->load->model("product_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_th"] = $this->input->get("search");
            }
            $products = $this->product_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $products);
        }
    }
}