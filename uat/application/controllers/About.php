<?php
defined('BASEPATH') or exit('No direct script access allowed');

class About extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $about_list = file_get_contents('https://nextere.space/api_area/home/aboutus_banner');
        $about_list = json_decode($about_list);
        $about_list =  $about_list->data->aboutus_banner;
        // echo '<pre>';
        // print_r( $about_list);
        // exit();
        $whyus_list = file_get_contents('https://nextere.space/api_area/home/whyus_list');
        $whyus_list = json_decode($whyus_list);
        $whyus_list =  $whyus_list->data->whyus_list;
        // echo '<pre>';
        // print_r($whyus_list);
        // exit();
        $contactus_list = file_get_contents('https://nextere.space/api_area/home/contactus');
        $contactus_list = json_decode($contactus_list);
        $contactus_list =  $contactus_list->data->contactus;
        // echo '<pre>';
        // print_r($contactus_list);
        // exit();
        $this->load->view("about", compact('about_list','whyus_list','contactus_list'));
    }
    public function social()
    {
        $this->load->view("social_login");
    }
    
}
