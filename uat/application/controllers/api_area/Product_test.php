<?php
 defined('BASEPATH') OR exit('No direct script access allowed');

 class Product extends CI_Controller
 {
    function __construct()
    {
        parent::__construct();
    }

    private $product_path = FCPATH.'/assets/uploads/product/';


    public function product_list($page = null)
    {
        $this->load->library("response_library");


        $this->load->model("product_relate_model");
        $this->load->model("product_model");
        $this->load->model("product_detail_category_model");
        $this->load->model("product_detail_menu_model");
        $this->load->model("price_rate_model");
        $this->load->model("price_technical_spec_model");

        if($this->input->post())
        {
            $product_category_id = $this->input->post("product_category_id");
            $product_sub_category_id = $this->input->post("product_sub_category_id");
            $price_rate_id = $this->input->post("price_rate_id");
            $show_item = $this->input->post("show_item");
            $page = $this->input->post("page");
            $technical_spec_id = $this->input->post("technical_spec_id");

            if(isset($technical_spec_id))
            {
                $model_filter = new stdClass();
                $model_filter->where["status"] = 'ACTIVATE';
                $model_filter->where["technical_spec_id"] = $technical_spec_id;
                $product_technical_specs = $this->price_technical_spec_model->search($model_filter);

                $product_technical_spec_array = array();
                foreach ($product_technical_specs as $key => $product_technical_spec) {
                    $product_technical_spec_array[] = $product_technical_spec->product_id;
                }
            }
            if(!isset($show_item))
            {
                $show_item = 12;
            }
            if(!isset($page))
            {
                $page = 1;
            }

            $price_rate = $this->price_rate_model->find($price_rate_id);

            $between = '';
            if(isset($price_rate))
            {
                $between = $price_rate->price_min . ' AND ' . $price_rate->price_max;
            }

            $model_filter = new stdClass();
            $model_filter->where["status"] = 'ACTIVATE';
            if(isset($product_category_id) && $product_category_id != '')
            {
                $model_filter->where["product_category_id"] = $product_category_id;
            }
            if(isset($product_sub_category_id) && $product_sub_category_id != '')
            {
                $model_filter->where["product_sub_category_id"] = $product_sub_category_id;
            }
            if(isset($product_technical_spec_array))
            {
                $model_filter->where_in["product_id"] = $product_technical_spec_array;
            }
            $model_filter->group_by = 'product_id';
            // $model_filter->get_query = true;
            $product_detail_categories = $this->product_detail_category_model->search($model_filter);

            if(count($product_detail_categories) > 0)
            {

                $product_array = array();
                foreach ($product_detail_categories as $key => $product_detail_category) {
                    $product_array[] = $product_detail_category->product_id;
                }

                // print_r($product_array);
                // exit();

                $page_start = ($page-1)*$show_item;

                $model_filter = new stdClass();
                $model_filter->where["product.status"] = 'ACTIVATE';
                $model_filter->where_in["product.id"] = $product_array;
                $model_filter->join = array("product_type" => "product.product_type_id = product_type.id","brand" => "product.brand_id = brand.id");
                if(isset($price_rate_id) && $price_rate_id != '')
                {
                    $model_filter->custom_where = 'product.retail_price BETWEEN '.$between.' ';
                }
                // $model_filter->limit = $show_item;
                // $model_filter->limit_start = $page_start;
                // $model_filter->get_query = true;
                $product_mains_all = $this->product_model->search($model_filter);
               
                $model_filter = new stdClass();
                $model_filter->where["product.status"] = 'ACTIVATE';
                $model_filter->where_in["product.id"] = $product_array;
                if(isset($price_rate_id) && $price_rate_id != '')
                {
                    $model_filter->custom_where = 'product.retail_price BETWEEN '.$between.' ';
                }
                $model_filter->join = array("product_type" => "product.product_type_id = product_type.id","brand" => "product.brand_id = brand.id");
                $model_filter->limit = $show_item;
                $model_filter->limit_start = $page_start;
                // $model_filter->get_query = true;
                $product_mains = $this->product_model->search($model_filter);

                if(count($product_mains) > 0)
                {
                    foreach($product_mains as $product_relate_all)
                    {
                        $image = array();
                        $dir = $this->product_path.$product_relate_all->{"product::code"};
                        if (is_dir($dir))
                        {
                            $product_images = array_diff(scandir($dir), array('.', '..')); 
                        }
                        if(count($product_images) > 0)
                        {
                            foreach ($product_images as $product_image) 
                            {
                                $image[] = base_url("assets/uploads/product").'/'.$product_relate_all->{"product::code"}.'/'.$product_image;
                            }
                            $product_relate_all->{"product::images"} = $image;
                        }
                    }
                }

                $item_max = count($product_mains_all);

                $page_max = ceil(count($product_mains_all)/$show_item);

                $this->response_library->responseJSON("0x0000-00000","The server respone success.", array("page_max" => $page_max,"page" => $page,"show_item" => $show_item,"item_max" => $item_max,"product_category_id" => $product_category_id,"product_sub_category_id" => $product_sub_category_id,"price_rate_id" => $price_rate_id,"product_mains" => $product_mains));
            }
            else
            {
                $this->response_library->responseJSON("1x0000-00000","The server respone Fails. No item.", );
            }
        }
        // else
        // {
        //     $show_item = 12;

        //     $model_filter = new stdClass();
        //     $model_filter->where["status"] = 'ACTIVATE';
        //     $model_filter->group_by = 'product_id';
        //     $product_detail_menus = $this->product_detail_menu_model->search($model_filter);

        //     $product_array = array();
        //     foreach ($product_detail_menus as $key => $product_detail_menu) {
        //         $product_array[] = $product_detail_menu->product_id;
        //     }

        //     // print_r($product_detail_menus);
        //     // exit();

        //     $page_start = ($page-1)*$show_item;

        //     $model_filter = new stdClass();
        //     $model_filter->where["product.status"] = 'ACTIVATE';
        //     $model_filter->where_in["product.id"] = $product_array;
        //     $model_filter->join = array("product_type" => "product.product_type_id = product_type.id","brand" => "product.brand_id = brand.id");
        //     // $model_filter->limit = $show_item;
        //     // $model_filter->limit_start = $page_start;
        //     // $model_filter->get_query = true;
        //     $product_mains_all = $this->product_model->search($model_filter);

        //     $model_filter = new stdClass();
        //     $model_filter->where["product.status"] = 'ACTIVATE';
        //     $model_filter->where_in["product.id"] = $product_array;
        //     $model_filter->join = array("product_type" => "product.product_type_id = product_type.id","brand" => "product.brand_id = brand.id");
        //     $model_filter->limit = $show_item;
        //     $model_filter->limit_start = $page_start;
        //     // $model_filter->get_query = true;
        //     $product_mains = $this->product_model->search($model_filter);

        //     // print_r($product_mains);
        //     // exit();           

        //     $item_max = count($product_mains_all);

        //     $page_max = ceil(count($product_mains_all)/$show_item);

        //     // print_r($page_max);
        //     // exit();


        //     if(count($product_mains) > 0)
        //     {
        //         foreach($product_mains as $product_relate_all)
        //         {
        //             $image = array();
        //             $dir = $this->product_path.$product_relate_all->{"product::code"};
        //             if (is_dir($dir))
        //             {
        //                 $product_images = array_diff(scandir($dir), array('.', '..')); 
        //             }
        //             if(count($product_images) > 0)
        //             {
        //                 foreach ($product_images as $product_image) 
        //                 {
        //                     $image[] = base_url("assets/uploads/product").'/'.$product_relate_all->{"product::code"}.'/'.$product_image;
        //                 }
        //                 $product_relate_all->{"product::images"} = $image;
        //             }
        //         }
        //     }

        //     $this->response_library->responseJSON("0x0000-00000","The server respone success.", array("page_max" => $page_max,"page" => $page,"show_item" => $show_item,"item_max" => $item_max,"product_mains" => $product_mains));
        // }

    }
}