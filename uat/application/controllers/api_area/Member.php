 <?php
 defined('BASEPATH') OR exit('No direct script access allowed');

 class Member extends CI_Controller
 {
    function __construct()
    {
        parent::__construct();
    }

    public function decode()
    {
        $this->load->library("encryption_library");

        if($this->input->post())
        {
            $code = $this->input->post("code");

            $password = $this->encryption_library->decryptString('$2y$10$SPj3OzeY2U6.FoVGay2Ts.pGORxhlASwkyhDMcyCtkDnJ0EKku.pe');

            echo ($password);
            exit();
        }
    }

    public function login()
    {
        $this->load->library("response_library");
        $this->load->library("encryption_library");

        $this->load->model("member_model");
        // $this->load->model("product_sub_category_model");
        // $this->load->model("child_submenu_category_model");

        if($this->input->post())
        {
            $email = $this->input->post("email");
            $password = $this->input->post("password");
            $token_line = $this->input->post("token_line");
            $token_facebook = $this->input->post("token_facebook");
            $token_google = $this->input->post("token_google");

            if(isset($email) && isset($password) && $password != ''  && $password != null)
            {
                $model_filter = new stdClass();
                $model_filter->where["status"] = 'ACTIVATE';
                $model_filter->where["email"] = $email;
                $model_filter->get_first = true;

                $member = $this->member_model->search($model_filter);

                if($member != null)
                {
                    $check_password = false;
                    
                        // $token = $this->encryption_library->encryptPassword($password);
                        // print_r($token);
                        // exit();

                    if($this->encryption_library->verifyPassword($password, $member->password))
                    {
                        $check_password = true;
                    }

                    if($check_password == true)
                    {
                        $token = $this->encryption_library->encryptString($member->id,"p@ssw0rd_smartclick");

                    // COLLECT USER & USER GROUP DATA
                        $this->session->set_userdata(array("__member::id" => $member->id));
                        $this->session->set_userdata(array("__member::email" => $member->email));
                        $this->session->set_userdata(array("__member::fullname" => $member->fullname));
                        $this->session->set_userdata(array("__member::gender" => $member->gender));
                        $this->session->set_userdata(array("__member::birth_date" => $member->birth_date));
                        $this->session->set_userdata(array("__member::tax_id" => $member->tax_id));
                        $this->session->set_userdata(array("__member::telephone" => $member->telephone));
                        $this->session->set_userdata(array("__member::status" => $member->status));


                        $this->response_library->responseJSON("0x0000-00000", "Login Success. Session was created.", array("token" => $token, "member_information" => $member));
                    }
                    else
                    {
                        $this->response_library->responseJSON("0x000A-AU001", "Username or Password incorrect.");
                    }
                }
                else
                {
                    $this->response_library->responseJSON("0x000A-AU001", "Username or Password incorrect.");
                }

            }
            if(isset($email) && isset($token_line) && $token_line != '' && $token_line != null)
            {

                $model_filter = new stdClass();
                $model_filter->where["status"] = 'ACTIVATE';
                // $model_filter->where["email"] = $email;
                $model_filter->where["token_line"] = $token_line;
                $model_filter->get_first = true;

                $member = $this->member_model->search($model_filter);

                if($member != null)
                {
                    $check_password = false;
                    if($member->token_line == $token_line)
                    {
                        $check_password = true;
                    }

                    if($check_password == true)
                    {
                        $token = $this->encryption_library->encryptString($member->id,"p@ssw0rd_smartclick");

                    // COLLECT USER & USER GROUP DATA
                        $this->session->set_userdata(array("__member::id" => $member->id));
                        $this->session->set_userdata(array("__member::email" => $member->email));
                        $this->session->set_userdata(array("__member::fullname" => $member->fullname));
                        $this->session->set_userdata(array("__member::gender" => $member->gender));
                        $this->session->set_userdata(array("__member::birth_date" => $member->birth_date));
                        $this->session->set_userdata(array("__member::tax_id" => $member->tax_id));
                        $this->session->set_userdata(array("__member::telephone" => $member->telephone));
                        $this->session->set_userdata(array("__member::status" => $member->status));


                        $this->response_library->responseJSON("0x0000-00000", "Login Success. Session was created.", array("token" => $token, "member_information" => $member));
                    }
                    else
                    {
                        $this->response_library->responseJSON("0x000A-AU001", "Username or Password incorrect.");
                    }
                }
                else
                {
                    $this->response_library->responseJSON("0x000A-AU001", "Username or Password incorrect.");
                }

            }
            if(isset($email) && isset($token_facebook) && $token_facebook != '' && $token_facebook != null)
            {

                $model_filter = new stdClass();
                $model_filter->where["status"] = 'ACTIVATE';
                // $model_filter->where["email"] = $email;
                $model_filter->where["token_facebook"] = $token_facebook;
                $model_filter->get_first = true;

                $member = $this->member_model->search($model_filter);

                if($member != null)
                {
                    $check_password = false;
                    if($member->token_facebook == $token_facebook)
                    {
                        $check_password = true;
                    }

                    if($check_password == true)
                    {
                        $token = $this->encryption_library->encryptString($member->id,"p@ssw0rd_smartclick");

                    // COLLECT USER & USER GROUP DATA
                        $this->session->set_userdata(array("__member::id" => $member->id));
                        $this->session->set_userdata(array("__member::email" => $member->email));
                        $this->session->set_userdata(array("__member::fullname" => $member->fullname));
                        $this->session->set_userdata(array("__member::gender" => $member->gender));
                        $this->session->set_userdata(array("__member::birth_date" => $member->birth_date));
                        $this->session->set_userdata(array("__member::tax_id" => $member->tax_id));
                        $this->session->set_userdata(array("__member::telephone" => $member->telephone));
                        $this->session->set_userdata(array("__member::status" => $member->status));


                        $this->response_library->responseJSON("0x0000-00000", "Login Success. Session was created.", array("token" => $token, "member_information" => $member));
                    }
                    else
                    {
                        $this->response_library->responseJSON("0x000A-AU001", "Username or Password incorrect.");
                    }
                }
                else
                {
                    $this->response_library->responseJSON("0x000A-AU001", "Username or Password incorrect.");
                }

            }
            if(isset($email) && isset($token_google) && $token_google != '' && $token_google != null)
            {

                $model_filter = new stdClass();
                $model_filter->where["status"] = 'ACTIVATE';
                // $model_filter->where["email"] = $email;
                $model_filter->where["token_google"] = $token_google;
                $model_filter->get_first = true;

                $member = $this->member_model->search($model_filter);

                if($member != null)
                {
                    $check_password = false;
                    if($member->token_google == $token_google)
                    {
                        $check_password = true;
                    }

                    if($check_password == true)
                    {
                        $token = $this->encryption_library->encryptString($member->id,"p@ssw0rd_smartclick");

                    // COLLECT USER & USER GROUP DATA
                        $this->session->set_userdata(array("__member::id" => $member->id));
                        $this->session->set_userdata(array("__member::email" => $member->email));
                        $this->session->set_userdata(array("__member::fullname" => $member->fullname));
                        $this->session->set_userdata(array("__member::gender" => $member->gender));
                        $this->session->set_userdata(array("__member::birth_date" => $member->birth_date));
                        $this->session->set_userdata(array("__member::tax_id" => $member->tax_id));
                        $this->session->set_userdata(array("__member::telephone" => $member->telephone));
                        $this->session->set_userdata(array("__member::status" => $member->status));


                        $this->response_library->responseJSON("0x0000-00000", "Login Success. Session was created.", array("token" => $token, "member_information" => $member));
                    }
                    else
                    {
                        $this->response_library->responseJSON("0x000A-AU001", "Username or Password incorrect.");
                    }
                }
                else
                {
                    $this->response_library->responseJSON("0x000A-AU001", "Username or Password incorrect.");
                }

            }
            else
            {
                $this->response_library->responseJSON("1x0000-00000","The server respone Fails."); 
            }
        }
    }

    public function register()
    {
        $this->load->library("response_library");
        $this->load->library("encryption_library");
        // $this->load->library("jwt_library");

        $this->load->model("member_model");

        // print_r($code);
        // exit();

        if($this->input->post())
        {
            $email = $this->input->post("email");

            $model_filter = new stdClass();
            $model_filter->where_in["status"] = array("ACTIVATE","CONFRIM");
            $model_filter->where["email"] = $email;

            $ck_duplicate = $this->member_model->search($model_filter);

            if(count($ck_duplicate) > 0)
            {
                $this->response_library->responseJSON("0x000A-AC001", "Email incorrect or Email Duplicate.");
            }
            else
            {

                $member = new stdClass();
                $member->fullname = $this->input->post("fullname");
                $member->gender = $this->input->post("gender");
                $member->email = $email;
                $member->password = $this->encryption_library->encryptPassword($this->input->post("password"));
                $member->birth_date = $this->input->post("birth_date");
                $member->tax_id = $this->input->post("tax_id");
                $member->telephone = $this->input->post("telephone");
                $member->token_facebook = $this->input->post("token_facebook");
                $member->token_google = $this->input->post("token_google");
                $member->token_line = $this->input->post("token_line");
                $member->status = 'CONFRIM';
                $member->created_date = date("Y-m-d h:i:s");

                $member->follow = $this->input->post("follow");
                if($member->follow == '' || empty($member->follow) || $member->follow == null)
                {
                    $member->follow = "SUSPEND";
                }

                $member_id = $this->member_model->save($member);

                $this->load->library('email');

                if(isset($member->token_facebook) || isset($member->token_google) || isset($member->token_line))
                {
                    $to = $member->email; // note the comma

            // Subject
                    $subject = 'Please verify your email address.';

                    $token = $this->encryption_library->encryptString($email,"token");
                    $member->token = $token;


            // Message
                    $message = $this->load->view("EmailTemplate/ForgotPassword",compact('member'),true);

                    // print_r($message);
                    // exit();

            // To send HTML mail, the Content-type header must be set
                    $headers[] = 'MIME-Version: 1.0';
                    $headers[] = 'Content-type: text/html; charset=utf-8';

            // Additional headers
                    // $headers[] = 'To: Mary <mary@example.com>, Kelly <kelly@example.com>';
                    $headers[] = 'From: Nextere <info@nextere.co.th>';
                    // $headers[] = 'Cc: birthdayarchive@example.com';
                    // $headers[] = 'Bcc: birthdaycheck@example.com';

            // Mail it
                    mail($to, $subject, $message, implode("\r\n", $headers));
                }

                else if(isset($member->password))
                {
                    $model_filter = new stdClass();
                    $model_filter->where["email"] = $email;
                    // $model_filter->where["status"] = 'CONFRIM';
                    $model_filter->get_first = true;

                    $member = $this->member_model->search($model_filter);

                    if($member != null)
                    {
                        $this->load->library('email');

                        $token = $this->encryption_library->encryptString($email,"token");
                        $member->token = $token;

                        $to = $email; // note the comma

                // Subject
                        $subject = 'Please confirm your change password.';

                // Message
                        $message = $this->load->view("EmailTemplate/ForgotPassword",compact('member'),true);

                        // print_r($message);
                        // exit();

                // To send HTML mail, the Content-type header must be set
                        $headers[] = 'MIME-Version: 1.0';
                        $headers[] = 'Content-type: text/html; charset=utf-8';

                // Additional headers
                        // $headers[] = 'To: Mary <mary@example.com>, Kelly <kelly@example.com>';
                        $headers[] = 'From: Nextere <info@nextere.com>';
                        // $headers[] = 'Cc: birthdayarchive@example.com';
                        // $headers[] = 'Bcc: birthdaycheck@example.com';

                // Mail it
                        mail($to, $subject, $message, implode("\r\n", $headers));

                        // $this->response_library->responseJSON("0x0000-00000", "Send mail complete.");
                        // redirect(base_url("login"));
                    }
                }

                $this->response_library->responseJSON("0x0000-00000", "Create Success.");
            }
        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The server respone Fails."); 
        }

    }

    public function test_email()
    {
        $this->load->library('email_library');
        $this->load->library('email');

        $to = 'adisorn.charoen@gmail.com, soullinke@outlook.com, adisorn@smartclick.co.th'; // note the comma

// Subject
        $subject = 'Please verify your email address.';

        $member = new stdClass();
        $member->token = 'GHAS2BNMCVBFDXCVASDF115TDVBS9ASC8';
        $member->email = 'adisorn.charoen@gmail.com';

// Message
        $message = $this->load->view("EmailTemplate/ConfirmEmail",compact('member'),true);

        // print_r($message);
        // exit();

// To send HTML mail, the Content-type header must be set
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=utf-8';

// Additional headers
        // $headers[] = 'To: Mary <mary@example.com>, Kelly <kelly@example.com>';
        $headers[] = 'From: Nextere <info@nextere.com>';
        // $headers[] = 'Cc: birthdayarchive@example.com';
        // $headers[] = 'Bcc: birthdaycheck@example.com';

// Mail it
        mail($to, $subject, $message, implode("\r\n", $headers));

        echo 'send complete';
        // if()
        // {

        // }
        // else
        // {
        //     // echo $this->Email_library->print_debugger();
        // }
    }

    public function edit_profile($member_id)
    {
        $this->load->library("response_library");
        $this->load->library("encryption_library");
        // $this->load->library("jwt_library");

        $this->load->model("member_model");

        if($this->input->post())
        {
            $member = $this->member_model->find($member_id);

            $member->email = $this->input->post("email");
            $member->fullname = $this->input->post("fullname");
            $member->gender = $this->input->post("gender");
            $member->birth_date = $this->input->post("birth_date");
            $member->tax_id = $this->input->post("tax_id");
            $member->telephone = $this->input->post("telephone");
            $member->token_facebook = $this->input->post("token_facebook");
            $member->token_google = $this->input->post("token_google");
            $member->token_line = $this->input->post("token_line");
            $member->modified_by = $member_id;
            $member->modified_date = date("Y-m-d h:i:s");

            $member->follow = $this->input->post("follow");

            $this->member_model->save($member);

            $this->response_library->responseJSON("0x0000-00000", "The server respone edit Success.");

        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The server respone Fails."); 
        }

    }


    public function change_password($member_id)
    {
        $this->load->library("response_library");
        $this->load->library("encryption_library");
        // $this->load->library("jwt_library");

        $this->load->model("member_model");

        if($this->input->post())
        {
            $password = $this->input->post("old_password");
            $new_password = $this->input->post("new_password");

            $member = $this->member_model->find($member_id);

            if(isset($member_id))
            {
                $member = $this->member_model->find($member_id);

                if($member != null)
                {
                    $check_password = false;
                    if($this->encryption_library->verifyPassword($password, $member->password))
                    {
                        $check_password = true;
                    }

                    if($check_password == true)
                    {
                        $enew_password = $this->encryption_library->encryptPassword($new_password);

                        $member->password = $enew_password;
                        $member->modified_by = $member_id;
                        $member->modified_date = date("Y-m-d h:i:s");

                        $this->member_model->save($member);

                        $this->response_library->responseJSON("0x0000-00000", "The server respone edit Success.");

                    }
                    else
                    {
                        $this->response_library->responseJSON("1x0000-00000","The server respone old password Fails."); 
                    }
                }
                else
                {
                    $this->response_library->responseJSON("1x0000-00000","The server respone not already."); 
                }
            }

        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The server respone Fails3."); 
        }

    }

    public function information()
    {
        $this->load->library("response_library");
        $this->load->library("encryption_library");
        // $this->load->library("jwt_library");

        $this->load->model("member_model");
        $this->load->model("member_address_model");
        $this->load->model("member_address_tax_model");
        $this->load->model("creditcard_model");

        if($this->input->post())
        {
            $token = $this->input->post("token");
            $member_id = $this->encryption_library->decryptString($token, "p@ssw0rd_smartclick");

            $member = $this->member_model->find($member_id);
            
            $model_filter = new stdClass();
            $model_filter->where["member_id"] = $member_id;
            $model_filter->where["status"] = 'ACTIVATE';
            $model_filter->get_first = true;

            $member_address = $this->member_address_model->search($model_filter);

            $model_filter = new stdClass();
            $model_filter->where["member_id"] = $member_id;
            $model_filter->where["status"] = 'ACTIVATE';
            $model_filter->get_first = true;

            $member_address_tax = $this->member_address_tax_model->search($model_filter);

            $model_filter = new stdClass();
            $model_filter->where["member_id"] = $member_id;
            $model_filter->where["is_remember"] = '1';
            $model_filter->where["status"] = 'ACTIVATE';
            $model_filter->get_first = true;

            $credit_card = $this->creditcard_model->search($model_filter);

            $this->response_library->responseJSON("0x0000-00000", "The server respone Success.", array("member" => $member, "member_address" => $member_address, "member_address_tax" => $member_address_tax, "credit_card" => $credit_card));

        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The server respone Fails."); 
        }

    }

    public function address_all($member_id = null)
    {
        $this->load->library("response_library");
        $this->load->library("encryption_library");
        // $this->load->library("jwt_library");

        $this->load->model("member_model");
        $this->load->model("member_address_model");

        $this->load->model("address_province_model");
        $this->load->model("address_district_model");
        $this->load->model("address_sub_district_model");


        $model_filter = new stdClass();
        $model_filter->where["member_id"] = $member_id;
        $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->order_by = 'is_default DESC';
        // $model_filter->get_first = true;

        $member_address = $this->member_address_model->search($model_filter);

        foreach($member_address as $member_addres)
        {
            if($member_addres->address_province_id != '' && !empty($member_addres->address_province_id))
            {
                $address_province = $this->address_province_model->find($member_addres->address_province_id);

                $member_addres->address_province = $address_province;
            }

            if($member_addres->address_district_id != '' && !empty($member_addres->address_district_id))
            {
                $address_district = $this->address_district_model->find($member_addres->address_district_id);

                $member_addres->address_district = $address_district;                
            }

            if($member_addres->address_sub_district_id != '' && !empty($member_addres->address_sub_district_id))
            {
                $address_sub_district = $this->address_sub_district_model->find($member_addres->address_sub_district_id);

                $member_addres->address_sub_district = $address_sub_district;                
            }
        }

        $this->response_library->responseJSON("0x0000-00000", "The server respone Success.", array("member_address" => $member_address));
    }

    public function address_tax_all($member_id = null)
    {
        $this->load->library("response_library");
        $this->load->library("encryption_library");
        // $this->load->library("jwt_library");

        $this->load->model("member_model");
        $this->load->model("member_address_tax_model");

        $this->load->model("address_province_model");
        $this->load->model("address_district_model");
        $this->load->model("address_sub_district_model");

        $model_filter = new stdClass();
        $model_filter->where["member_id"] = $member_id;
        $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->order_by = 'is_default DESC';
        // $model_filter->get_first = true;

        $member_address = $this->member_address_tax_model->search($model_filter);

        foreach($member_address as $member_addres)
        {
            if($member_addres->address_province_id != '' && !empty($member_addres->address_province_id))
            {
                $address_province = $this->address_province_model->find($member_addres->address_province_id);

                $member_addres->address_province = $address_province;
            }

            if($member_addres->address_district_id != '' && !empty($member_addres->address_district_id))
            {
                $address_district = $this->address_district_model->find($member_addres->address_district_id);

                $member_addres->address_district = $address_district;                
            }

            if($member_addres->address_sub_district_id != '' && !empty($member_addres->address_sub_district_id))
            {
                $address_sub_district = $this->address_sub_district_model->find($member_addres->address_sub_district_id);

                $member_addres->address_sub_district = $address_sub_district;                
            }
        }

        $this->response_library->responseJSON("0x0000-00000", "The server respone Success.", array("member_address" => $member_address));
    }

    public function add_address($member_address_id = null)
    {

        $this->load->library("response_library");
        $this->load->library("encryption_library");
        // $this->load->library("jwt_library");

        $this->load->model("member_model");
        $this->load->model("member_address_model");
        $this->load->model("member_address_tax_model");

        if($this->input->post())
        {
            $is_default = $this->input->post("is_default");
            $member_id = $this->input->post("member_id");

            if($is_default == 1 && $member_id != null)
            {
                $model_filter = new stdClass();
                $model_filter->where["member_id"] =$member_id;

                $addesss = $this->member_address_model->search($model_filter);

                if(count($addesss) > 0)
                {
                    $this->member_address_model->rawModify("UPDATE member_address SET is_default = 0 WHERE member_address.member_id = ".$member_id.";");
                }
            }

            if($member_address_id != null)
            {
                $address = $this->member_address_model->find($member_address_id);

                $address->modified_by = $this->input->post("member_id");
                $address->modified_date = date("Y-m-d h:i:s");
            }
            else
            {

                $address = new stdClass();

                $address->created_by = $this->input->post("member_id");
                $address->created_date = date("Y-m-d h:i:s");
            }

            $address->member_id = $member_id;
            $address->first_name = $this->input->post("first_name");
            $address->last_name = $this->input->post("last_name");
            $address->telephone = $this->input->post("telephone");
            $address->email = $this->input->post("email");
            $address->company_name = $this->input->post("company_name");
            $address->company_branch = $this->input->post("company_branch");
            $address->address = $this->input->post("address");
            $address->address_province_id = $this->input->post("address_province_id");
            $address->address_district_id = $this->input->post("address_district_id");
            $address->address_sub_district_id = $this->input->post("address_sub_district_id");
            $address->postcode = $this->input->post("postcode");
            $address->is_default = $this->input->post("is_default");
            $address->status = 'ACTIVATE';

            $address_id = $this->member_address_model->save($address);

            $this->response_library->responseJSON("0x0000-00000", "Create Success.");

        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The server respone Fails."); 
        }
    }

    public function remove_address($member_address_id)
    {

        $this->load->library("response_library");
        $this->load->library("encryption_library");
        // $this->load->library("jwt_library");

        $this->load->model("member_model");
        $this->load->model("member_address_model");
        $this->load->model("member_address_tax_model");

        $member_address = $this->member_address_model->find($member_address_id);

        $member_address->status = 'REMOVED';
        $member_address->modified_date = date("Y-m-d h:i:s");

        $this->member_address_model->save($member_address);

        $this->response_library->responseJSON("0x0000-00000","The server respone delete success.");
    }

    public function add_address_tax($member_address_tax_id = null)
    {

        $this->load->library("response_library");
        $this->load->library("encryption_library");
        // $this->load->library("jwt_library");

        $this->load->model("member_model");
        $this->load->model("member_address_model");
        $this->load->model("member_address_tax_model");

        if($this->input->post())
        {
            $is_default = $this->input->post("is_default");
            $member_id = $this->input->post("member_id");

            if($is_default == 1 && $member_id != null)
            {
                $model_filter = new stdClass();
                $model_filter->where["member_id"] =$member_id;

                $addesss = $this->member_address_tax_model->search($model_filter);

                if(count($addesss) > 0)
                {
                    $this->member_address_tax_model->rawModify("UPDATE member_address_tax SET is_default = 0 WHERE member_address_tax.member_id = ".$member_id.";");
                }
            }

            if($member_address_tax_id != null)
            {
                $address = $this->member_address_tax_model->find($member_address_tax_id);

                $address->modified_by = $this->input->post("member_id");
                $address->modified_date = date("Y-m-d h:i:s");
            }
            else
            {

                $address = new stdClass();

                $address->created_by = $this->input->post("member_id");
                $address->created_date = date("Y-m-d h:i:s");
            }

            $address->member_id = $member_id;
            $address->first_name = $this->input->post("first_name");
            $address->last_name = $this->input->post("last_name");
            $address->telephone = $this->input->post("telephone");
            $address->email = $this->input->post("email");
            $address->company_name = $this->input->post("company_name");
            $address->company_branch = $this->input->post("company_branch");
            $address->address = $this->input->post("address");
            $address->address_province_id = $this->input->post("address_province_id");
            $address->address_district_id = $this->input->post("address_district_id");
            $address->address_sub_district_id = $this->input->post("address_sub_district_id");
            $address->postcode = $this->input->post("postcode");
            $address->tax_id = $this->input->post("tax_id");
            $address->is_default = $this->input->post("is_default");
            $address->status = 'ACTIVATE';

            $address_id = $this->member_address_tax_model->save($address);

            $this->response_library->responseJSON("0x0000-00000", "Create Success.");

        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The server respone Fails."); 
        }
    }

    public function remove_address_tax($member_address_tax_id)
    {

        $this->load->library("response_library");
        $this->load->library("encryption_library");
        // $this->load->library("jwt_library");

        $this->load->model("member_model");
        $this->load->model("member_address_model");
        $this->load->model("member_address_tax_model");

        $member_address = $this->member_address_tax_model->find($member_address_tax_id);

        $member_address->status = 'REMOVED';
        $member_address->modified_date = date("Y-m-d h:i:s");

        $this->member_address_tax_model->save($member_address);

        $this->response_library->responseJSON("0x0000-00000","The server respone delete success.");
    }


    // public function verify_member()
    // {
    //     $this->load->library("response_library");
    //     $this->load->library("encryption_library");
    //     // $this->load->library("jwt_library");

    //     $this->load->model("member_model");

    //     $code = $this->input->get("code");

    //     // print_r($code);
    //     // exit();

    //     if(isset($code) && $code != null)
    //     {
    //         $email = $this->encryption_library->decryptString($code);


    //         $model_filter = new stdClass();
    //         $model_filter->where["email"] = $email;
    //         $model_filter->where["status"] = 'CONFRIM';
    //         $model_filter->get_first = true;

    //         $member = $this->member_model->search($model_filter);

    //         if($member != null)
    //         {
    //             $member->status = 'ACTIVATE';
    //             $member->modified_date = date("Y-m-d h:i:s");
    //             // $member->modified_by = $this->session->userdata("__administrator::id");

    //             $this->member_model->save($member);

    //             // $this->response_library->responseJSON("0x0000-00000", "Update Success.");
    //             redirect(base_url("login"));
    //         }
    //         else
    //         {
    //             $this->response_library->responseJSON("0x000A-AC001", "Code incorrect.");
    //         }
    //     }
    //     else
    //     {
    //         $this->response_library->responseJSON("1x0000-00000","The server respone Fails."); 
    //     }

    // }


    public function send_rememberPassword()
    {
        $this->load->library("response_library");
        $this->load->library("encryption_library");
        // $this->load->library("jwt_library");

        $this->load->model("member_model");

        if($this->input->post())
        {
            $email = $this->input->post("email");



            $model_filter = new stdClass();
            $model_filter->where["email"] = $email;
            // $model_filter->where["status"] = 'CONFRIM';
            $model_filter->get_first = true;

            $member = $this->member_model->search($model_filter);

            if($member != null)
            {
                $this->load->library('email');

                $token = $this->encryption_library->encryptString($email,"token");
                $member->token = $token;

                $to = $email; // note the comma

        // Subject
                $subject = 'Please confirm your change password.';

        // Message
                $message = $this->load->view("EmailTemplate/ForgotPassword",compact('member'),true);

                // print_r($message);
                // exit();

        // To send HTML mail, the Content-type header must be set
                $headers[] = 'MIME-Version: 1.0';
                $headers[] = 'Content-type: text/html; charset=utf-8';

        // Additional headers
                // $headers[] = 'To: Mary <mary@example.com>, Kelly <kelly@example.com>';
                $headers[] = 'From: Nextere <info@nextere.com>';
                // $headers[] = 'Cc: birthdayarchive@example.com';
                // $headers[] = 'Bcc: birthdaycheck@example.com';

        // Mail it
                mail($to, $subject, $message, implode("\r\n", $headers));

                $this->response_library->responseJSON("0x0000-00000", "Send mail complete.");
                // redirect(base_url("login"));
            }
            else
            {
                $this->response_library->responseJSON("0x000A-AC001", "email incorrect.");
            }
        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The server respone Fails."); 
        }

    }

    public function confrim_rememberPassword()
    {
        $this->load->library("response_library");
        $this->load->library("encryption_library");
        // $this->load->library("jwt_library");

        $this->load->model("member_model");

        if($this->input->post())
        {
            $token = $this->input->post("token");
            $password = $this->input->post("password");

            $email = $this->encryption_library->decryptString($token,"token");

            // echo "<pre>";
            // echo $email;
            // echo $password;
            // exit();


            $model_filter = new stdClass();
            $model_filter->where["email"] = $email;
            // $model_filter->where["status"] = 'CONFRIM';
            $model_filter->get_first = true;

            $member = $this->member_model->search($model_filter);

            if($member != null)
            {
                $member->password = $this->encryption_library->encryptPassword($password);
                $member->status = 'ACTIVATE';
                $member->modified_date = date("Y-m-d h:i:s");

                $this->member_model->save($member);

                $this->response_library->responseJSON("0x0000-00000", "The server update Success.");
                // redirect(base_url("login"));
            }
            else
            {
                $this->response_library->responseJSON("0x000A-AC001", "Token incorrect.");
            }
        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The server respone Fails."); 
        }

    }

    public function product_history($member_id = null)
    {
        $this->load->library("response_library");
        $this->load->library("encryption_library");
        // $this->load->library("jwt_library");

        $this->load->model("purchase_transaction_model");
        $this->load->model("purchase_transaction_detail_model");

        if(isset($member_id) && $member_id != null)
        {

            $model_filter = new stdClass();
            $model_filter->where["member_id"] = $member_id;

            $purchase_transactions = $this->purchase_transaction_model->search($model_filter);

            if(count($purchase_transactions) > 0)
            {
                foreach($purchase_transactions as $purchase_transaction)
                {
                    $model_filter = new stdClass();
                    $model_filter->where["purchase_transaction_id"] = $purchase_transaction->id;
                    $purchase_transaction_details = $this->purchase_transaction_detail_model->search($model_filter);

                    $purchase_transaction->purchase_transaction_detail = $purchase_transaction_details;
                }
            }

            $this->response_library->responseJSON("0x0000-00000", "The server respone Success.", array("purchase_transactions" => $purchase_transactions));
        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The server respone Fails."); 
        }
    }
}