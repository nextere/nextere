<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Language extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
  }

  public function change($Language)
  {
   
    if ($Language == "th") {
      $this->session->set_userdata("CURRENT_LANGUAGE", "th");
    } else {
      $this->session->set_userdata("CURRENT_LANGUAGE", "en");
    }

    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $url = $this->input->get("url");

   
    
    redirect($url);
  }
}
