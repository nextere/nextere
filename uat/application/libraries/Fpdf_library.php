<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fpdf_library
{
    public function __construct()
    {
        require_once APPPATH.'third_party/fpdf/custom_fpdf.php';
    }

    public function get()
    {
        $pdf = new custom_fpdf();
        return $pdf;
    }
}

/*
$pdf->Cell(30, 8, $this->thai(
    "10"."       ".
    "20"."       ".
    "30"."       ".
    "40"."       ".
    "50"."        ".
    "60"."        ".
    "70"."        ".
    "80"."        ".
    "90"."        ".
    "100"."    ".
    "110"."     ".
    "120"."      ".
    "130"."      ".
    "140"."       ".
    "150"."     ".
    "160"."     ".
    "170"."     ".
    "180"."     ".
    "190"."     ".
    "200"
), "", 0, "L");
$pdf->Line(5,10,10,10);
$pdf->Line(15,10,20,10);
$pdf->Line(25,10,30,10);
$pdf->Line(35,10,40,10);
$pdf->Line(45,10,50,10);
$pdf->Line(55,10,60,10);
$pdf->Line(65,10,70,10);
$pdf->Line(75,10,80,10);
$pdf->Line(85,10,90,10);
$pdf->Line(95,10,100,10);
$pdf->Line(105,10,110,10);
$pdf->Line(115,10,120,10);
$pdf->Line(125,10,130,10);
$pdf->Line(135,10,140,10);
$pdf->Line(145,10,150,10);
$pdf->Line(155,10,160,10);
$pdf->Line(165,10,170,10);
$pdf->Line(175,10,180,10);
$pdf->Line(185,10,190,10);
$pdf->Line(195,10,200,10);
$pdf->Line(205,10,210,10);
$pdf->Line(215,10,220,10);
$pdf->Line(225,10,230,10);
*/