<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	public function change($Language)
	{

    if($Language == "th")
    {
      $this->session->set_userdata("CURRENT_LANGUAGE","th");
    }
    else 
    {
      $this->session->set_userdata("CURRENT_LANGUAGE","en");
    }
    $url = $this->input->get("url");
    redirect($url);

  }
}