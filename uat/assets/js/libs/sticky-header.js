// Sticky Header
$(window).scroll(function() {
    if(window.screen.width > 767){
      if ($(window).scrollTop()) {
        $('.main_h').addClass('sticky');
        $(".navigation-extra").css('display', 'none');
        } else {
            $('.main_h').removeClass('sticky');
            $(".navigation-extra").css('display', 'block');
        }
    }
});

// $(window).scroll(function() {
//     if(window.screen.width > 767){
//       if ($(window).scrollTop()) {
//         $('.main_h').addClass('sticky');
//         $(".navigation-inner").css('display', 'none');
//         } else {
//             $('.main_h').removeClass('sticky');
//             $(".navigation-inner").css('display', 'block');
//         }
//     }
// });

if (window.screen.width <= 767) {
     $('.main_h').addClass('sticky');
 }


// Mobile Navigation
$('.mobile-toggle').click(function() {
    if ($('.main_h').hasClass('open-nav')) {
        $('.main_h').removeClass('open-nav');
    } else {
        $('.main_h').addClass('open-nav');
    }
});
$('.main_h li a').click(function() {
    if ($('.main_h').hasClass('open-nav')) {
        $('.navigation').removeClass('open-nav');
        $('.main_h').removeClass('open-nav');
    }
});

// Navigation Scroll - ljepo radi materem
$('nav a').click(function(event) {
    var id = $(this).attr("href");
    var offset = 70;
    var target = $(id).offset().top - offset;
    $('html, body').animate({
        scrollTop: target
    }, 500);
    event.preventDefault();
});