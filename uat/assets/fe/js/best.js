function getServerTime(url){
	var serverTime = "";
	$.ajax({
		 url: url,
		 type: "GET",
		 contentType: "application/json",
		 dataType: "json",
		 async: false,
		 cache: false,
		 success: function(data) {
			var dateTime = new Date(data['servertime']);
			var dd = dateTime.getDate();
			var mm = dateTime.getMonth()+1;
			var yy = dateTime.getFullYear();
			var hh = dateTime.getHours();
			if(dd<10){
				dd='0'+dd;
			} 
			if(mm<10){
				mm='0'+mm;
			}
			yy = (""+yy).substr(2,4);
			if(hh<10){
				hh='0'+hh;
			}
			var dateStr = ""+dd+mm+yy+hh;
			var res = CryptoJS.MD5('nekcihc'+dateStr);
			serverTime = res+"";
		 }
	});
	return serverTime;
}