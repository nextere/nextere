<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model { 
    function __construct()
    {
        parent::__construct();
        //$this->db_master = $this->load->database('default', TRUE);
		$this->load->database();
		$this->db->cache_off();
    }
} 