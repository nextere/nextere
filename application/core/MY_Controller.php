<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller { 
    function __construct()
    {
        parent::__construct();     
    	$this->ci = & get_instance ();
		if ($_SERVER['SERVER_ADDR'] != $_SERVER['REMOTE_ADDR']){
		  $this->output->set_status_header(400, 'No Remote Access Allowed');
		  exit();
		}
    }
} 