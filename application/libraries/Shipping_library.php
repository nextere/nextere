<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shipping_library
{
    private $API_KEY = '';
    
    public function checkprice() {
        $array_order = array();
        $array_order[0] = array(
            'from'=>array(
                'name'=>'ชื่อผู้ส่ง นามสกุล',
                'address'=>'522 ซอยรัชดาภิเษก 26 ถนนรัชดาภิเษก  แขวงสามเสนนอก เขตห้วยขวาง กรุงเทพมหานคร ',
                'district'=>'-',
                'state'=>'-',
                'province'=>'-',
                'postcode'=>'10310',
                'tel'=>'0929053355'
            ),
            'to'=>array(
                'name'=>'ชื่อผู้รับ นามสกุล',
                'address'=>'บ้านแสงจันทร์ เลขที่ 4 ซอยลาดพร้าว 40 ถนนลาดพร้าว แขวงจันทรเกษม เขตจตุจักร กรุงเทพมหานคร',
                'district'=>'-',
                'state'=>'-',
                'province'=>'-',
                'postcode'=>'10900',
                'tel'=>'0929053355'
            ),
            'parcel'=>array(
                'name'=>'-',
                'weight'=> 1,
                'width'=> 1,
                'length'=> 1,
                'height'=> 1
            ),
            'showall'=>true
        );
        $array_order[1] = array(
            'from'=>array(
                'name'=>'ชื่อผู้ส่ง นามสกุล',
                'address'=>'522 ซอยรัชดาภิเษก 26 ถนนรัชดาภิเษก  แขวงสามเสนนอก เขตห้วยขวาง กรุงเทพมหานคร ',
                'district'=>'-',
                'state'=>'-',
                'province'=>'-',
                'postcode'=>'10310',
                'tel'=>'0929053355'
            ),
            'to'=>array(
                'name'=>'ชื่อผู้รับ นามสกุล',
                'address'=>'บ้านแสงจันทร์ เลขที่ 4 ซอยลาดพร้าว 40 ถนนลาดพร้าว แขวงจันทรเกษม เขตจตุจักร กรุงเทพมหานคร',
                'district'=>'-',
                'state'=>'-',
                'province'=>'-',
                'postcode'=>'10900',
                'tel'=>'0929053355'
            ),
            'parcel'=>array(
                'name'=>'-',
                'weight'=> 1,
                'width'=> 1,
                'length'=> 1,
                'height'=> 1
            ),
            'showall'=>true
        );

        $post_data = array(
            'api_key'=>$API_KEY,
            'data' => array()
        );
        $post_data['data'] = $array_order;

        $post_data = http_build_query($post_data);
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL, 'https://mkpservice.shippop.dev/pricelist/');
        curl_setopt($curl,CURLOPT_POST, sizeof($post_data));
        curl_setopt($curl,CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

}