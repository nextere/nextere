<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpay_library
{
	public function GenSignature($guid, $requestBody)
	{
		$codeigniter_instance =& get_instance();
		$channelSecret =  $codeigniter_instance->config->item('MPAY_CHANNEL_SECRET');
		$signature = hash_hmac('sha256', $requestBody.$guid, $channelSecret);

		return $signature;
	}

	public function PaymentCreditCard($requestBody)
	{
		$codeigniter_instance =& get_instance();

		/** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        try{
    		$url = $codeigniter_instance->config->item('MPAY_ENDPOINT')."/service-txn-gateway/v1/cc/txns/payment_order";
    		$merchant_id = $codeigniter_instance->config->item('MPAY_MERCHANT_ID');
    		$guid = uniqid();
    		$signature = $this->GenSignature($guid, $requestBody);

    		$ch = curl_init($url);
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);

			// Set HTTP Header for POST request 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			    'Content-Type: application/json; charset=UTF-8',
			    'X-sdpg-nonce: '.$guid,
			    'X-sdpg-merchant-id: '.$merchant_id,
			    'X-sdpg-signature: '.$signature
				)
			);
			// Submit the POST request
			$result = curl_exec($ch);

			// Close cURL session handle
			curl_close($ch);
			writelog_paymentCredit(date("Y-m-d H:i:s")."||create||input=".$requestBody);
			writelog_paymentCredit(date("Y-m-d H:i:s")."||create||output=".$result);
			return json_decode($result);
		}
		catch(Exception $e){
			writelog_paymentCredit(date("Y-m-d H:i:s")."||create||error=".$e->getMessage());
			return json_decode($result);
		}
	}

	public function PaymentInternetBanking($requestBody)
	{
		$codeigniter_instance =& get_instance();

		/** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        try{
    		$url = $codeigniter_instance->config->item('MPAY_ENDPOINT')."/service-txn-gateway/v1/ib/payment_order";
    		$merchant_id = $codeigniter_instance->config->item('MPAY_MERCHANT_ID');
    		$guid = uniqid();
    		$signature = $this->GenSignature($guid, $requestBody);

    		$ch = curl_init($url);
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);

			// Set HTTP Header for POST request 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			    'Content-Type: application/json; charset=UTF-8',
			    'X-sdpg-nonce: '.$guid,
			    'X-sdpg-merchant-id: '.$merchant_id,
			    'X-sdpg-signature: '.$signature
				)
			);
			// Submit the POST request
			$result = curl_exec($ch);

			// Close cURL session handle
			curl_close($ch);
			writelog_paymentInternetbanking(date("Y-m-d H:i:s")."||create||input=".$requestBody);
			writelog_paymentInternetbanking(date("Y-m-d H:i:s")."||create||output=".$result);
			return json_decode($result);
		}
		catch(Exception $e){
			writelog_paymentInternetbanking(date("Y-m-d H:i:s")."||create||error=".$e->getMessage());
		}
	}

	public function PaymentQR($requestBody)
	{
		$codeigniter_instance =& get_instance();

		/** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        try{
    		$url = $codeigniter_instance->config->item('MPAY_ENDPOINT')."/service-txn-gateway/v1/qr";
    		$merchant_id = $codeigniter_instance->config->item('MPAY_MERCHANT_ID');
    		$guid = uniqid();
    		$signature = $this->GenSignature($guid, $requestBody);

    		$ch = curl_init($url);
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);

			// Set HTTP Header for POST request 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			    'Content-Type: application/json; charset=UTF-8',
			    'X-sdpg-nonce: '.$guid,
			    'X-sdpg-merchant-id: '.$merchant_id,
			    'X-sdpg-signature: '.$signature
				)
			);
			// Submit the POST request
			$result = curl_exec($ch);

			// Close cURL session handle
			curl_close($ch);
			writelog_paymentQr(date("Y-m-d H:i:s")."||create||input=".$requestBody);
			writelog_paymentQr(date("Y-m-d H:i:s")."||create||output=".$result);
			return json_decode($result);
		}
		catch(Exception $e){
			writelog_paymentQr(date("Y-m-d H:i:s")."||create||error=".$e->getMessage());
		}
	}

	public function PaymentRabbitLinePay($requestBody)
	{
		$codeigniter_instance =& get_instance();

		/** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        try{
    		$url = $codeigniter_instance->config->item('MPAY_ENDPOINT')."/service-txn-gateway/v1/rlp/txns/payment";
    		$merchant_id = $codeigniter_instance->config->item('MPAY_MERCHANT_ID');
    		$guid = uniqid();
    		$signature = $this->GenSignature($guid, $requestBody);

    		$ch = curl_init($url);
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);

			// Set HTTP Header for POST request 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			    'Content-Type: application/json; charset=UTF-8',
			    'X-sdpg-nonce: '.$guid,
			    'X-sdpg-merchant-id: '.$merchant_id,
			    'X-sdpg-signature: '.$signature
				)
			);
			// Submit the POST request
			$result = curl_exec($ch);

			// Close cURL session handle
			curl_close($ch);
			writelog_paymentCredit(date("Y-m-d H:i:s")."||create||input=".$requestBody);
			writelog_paymentCredit(date("Y-m-d H:i:s")."||create||output=".$result);
			return json_decode($result);
		}
		catch(Exception $e){
			writelog_paymentCredit(date("Y-m-d H:i:s")."||create||error=".$e->getMessage());
		}
	}

	public function PaymentAlipay($requestBody)
	{
		$codeigniter_instance =& get_instance();

		/** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        try{
    		$url = $codeigniter_instance->config->item('MPAY_ENDPOINT')."/v1/alipay/txns/order_apply";
    		$merchant_id = $codeigniter_instance->config->item('MPAY_MERCHANT_ID');
    		$guid = uniqid();
    		$signature = $this->GenSignature($guid, $requestBody);

    		$ch = curl_init($url);
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);

			// Set HTTP Header for POST request 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			    'Content-Type: application/json; charset=UTF-8',
			    'X-sdpg-nonce: '.$guid,
			    'X-sdpg-merchant-id: '.$merchant_id,
			    'X-sdpg-signature: '.$signature
				)
			);
			// Submit the POST request
			$result = curl_exec($ch);

			// Close cURL session handle
			curl_close($ch);
			writelog_paymentCredit(date("Y-m-d H:i:s")."||create||input=".$requestBody);
			writelog_paymentCredit(date("Y-m-d H:i:s")."||create||output=".$result);
			return json_decode($result);
		}
		catch(Exception $e){
			writelog_paymentCredit(date("Y-m-d H:i:s")."||create||error=".$e->getMessage());
		}
	}


}