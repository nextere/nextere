<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Language
{
    public static function setlanguage($lang = 'th')
    {
        $ci = get_instance();
        if($lang!='en' ) {
            $ci->config->set_item('HOME', 'หน้าแรก');
            $ci->config->set_item('OURSERVICES', 'บริการของเรา');
            $ci->config->set_item('GALLERY', 'ภาพหลังผ่าตัด');
            $ci->config->set_item('TESTIMONIALS', 'คำตอบรับจากลูกค้า');
            $ci->config->set_item('BLOG', 'บล็อก');
            $ci->config->set_item('FAQ', 'คำถามที่พบบ่อย');
            $ci->config->set_item('ABOUTUS', 'เกี่ยวกับเรา');
            $ci->config->set_item('ABOUTCLINIC', 'ดีเฮชที คลินิก');
            $ci->config->set_item('DOCTORS', 'แพทย์');
            $ci->config->set_item('CONTACTUS', 'ติดต่อเรา');
            $ci->config->set_item('TEAM', 'บุคลากร');
            $ci->config->set_item('STAFF', 'ทีม');
            $ci->config->set_item('ONLINECONSULT', 'ปรึกษาออนไลน์');

            $ci->config->set_item('DETAILS', 'รายละเอียด');

            $ci->config->set_item('HOME_PROFILE', 'ได้รับ “รางวัลเส้นผมทองคำ” ในด้านศัลยกรรมปลูกผมยอดเยี่ยม ปี พ.ศ.2553 จากสมาคมศัลยแพทย์ปลูกผมนานาชาติ');

            $ci->config->set_item('DAMKERNG_FULLNAME', 'นพ.ดำเกิง ปฐมวาณิชย์');
            $ci->config->set_item('TRAVELING', 'การเดินทาง มายัง DHT Clinic');

        }
        else{
            $ci->config->set_item('HOME', 'Home');
            $ci->config->set_item('OURSERVICES', 'Our Services');
            $ci->config->set_item('GALLERY', 'Gallery');
            $ci->config->set_item('TESTIMONIALS', 'Testimonials');
            $ci->config->set_item('BLOG', 'Blogs');
            $ci->config->set_item('FAQ', 'FAQ');
            $ci->config->set_item('ABOUTUS', 'About us');
            $ci->config->set_item('ABOUTCLINIC', 'About Clinic');
            $ci->config->set_item('DOCTORS', 'Doctors');
            $ci->config->set_item('CONTACTUS', 'Contact Us');
            $ci->config->set_item('TEAM', 'Team');
            $ci->config->set_item('STAFF', 'Staff');
            $ci->config->set_item('ONLINECONSULT', 'Online Consultation');

            $ci->config->set_item('DETAILS', 'Details');
            $ci->config->set_item('HOME_PROFILE', '" 2010 ISHRS Golden Follicle Award 
                                For Outstanding and Significant 
                                Clinical Contributions Related to 
                            Hair Restoration Surgery ”');

            $ci->config->set_item('DAMKERNG_FULLNAME', 'Dr. Damkerng Pathomvanich, MD,');
            $ci->config->set_item('TRAVELING', 'How to visit us');
        }
    }
}