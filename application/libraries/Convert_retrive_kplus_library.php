<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Convert_retrive_kplus_library
{ 


public function converttojson($text) { 
    $text = str_replace("PMGWRESP2=","",$text);
    $data = new StdClass();
    $data->TransCode = $this->start_end_position($text, 1, 4); 
    $data->MerchantID  = $this->start_end_position($text, 5, 15); 
    $data->BankReserve1   = $this->start_end_position($text, 20, 8); 
    $data->ShopNo    = $this->start_end_position($text, 28, 2); 
    // 764 = THB เสมอ
    $data->CurrencyCode     = $this->start_end_position($text, 30, 3); 
    $data->InvoiceNo      =  $this->start_end_position($text, 33, 12); 
    //DDMMYYYY
    $data->Date      = $this->convert_to_date($this->start_end_position($text, 45, 8)); 
    //HHMMSS
    $data->Time      = $this->start_end_position($text, 53, 6); 
    $data->MobileNo       = $this->start_end_position($text, 59, 10); 
    $data->TransactionTimeOut       = $this->start_end_position($text, 69, 17); 
    $data->TransAmount       = $this->convert_to_currency($this->start_end_position($text, 86, 12)); 
    $data->ResponseCode        = $this->start_end_position($text, 98, 2); 
    $data->BankReserve2        = $this->start_end_position($text, 100, 9); 
    $data->Reference1        =  $this->start_end_position($text, 109, 18); 
    $data->BankReserve3        = $this->start_end_position($text, 129, 42); 
    $data->Reference2        =  $this->start_end_position($text, 169, 18);
    $data->BankReserve4         = $this->start_end_position($text, 189, 120); 
    $data->Description         = $this->start_end_position($text, 309, 150); 
    $data->PayerIPAddress         = $this->start_end_position($text, 459, 18); 
    $data->BankReserve5          = $this->start_end_position($text, 477, 40); 
    $data->Info1          = $this->start_end_position($text, 517, 30); 
    $data->BankReserve6           = $this->start_end_position($text, 547, 70); 
    $data->Info2           = $this->start_end_position($text, 617, 30); 
    $data->BankReserve7 = $this->start_end_position($text, 647, 170); 
    return $data;
}



public function start_end_position($text,$start,$length){
    $result = new StdClass();
    $result->start = $start-1;
    $data = substr($text, $result->start, $length); 
    $data = $this->remove_x( $data);
    return $data;
}

public function convert_to_currency($data){
    $result = "";
    if( strlen($data)==12){
        $result =    Intval(substr($data,0,10)).'.'.substr($data,-2); 
    }
    return  $result;

}

public function convert_to_date($data){
  $dd =   substr($data, 0, 2);
  $mm =   substr($data, 2, 2);
  $yy =   substr($data, 4, 4);

  return date('Y-m-d', strtotime($yy.'-'.$mm.'-'.$dd)); 
}

public function remove_x($data){
   return str_replace("X","",$data);
}


public function get_response_code($response){
          
   
    switch ($response) {
    case  "00":
        return "Payment Success";
        break;
    case  "01":
        return "Payment Reject";
        break;
    case  "02":
        return "Transaction is awaiting confirmation";
        break;
    case  "03":
        return "Payment Failure";
        break;
    case  "04":
        return "Transaction expired";
        break;
    case  "05":
        return "Transaction cannot be processed";
        break;
    case  "06":
        return "RTP Sending Fail (KBank)";
        break;
    case  "07":
        return "RTP Sending Fail (ITMX)";
        break;
    default:
        return "Transaction Timeout";
        break;
    }

}




}
?>