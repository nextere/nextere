<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Notification_firebase_library
{
	public function SendNotification($sendto, $body, $purchase_transaction_id = null)
	{
		/** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        /** @CODEIGNITER_INSTANCE  $codeigniterInstance */
		$codeigniter_instance =& get_instance();

		/** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $url = $codeigniter_instance->config->item('FIREBASE_NOTIFICATION_URL');
        $key = $codeigniter_instance->config->item('FIREBASE_NOTIFICATION_KEY');
        $title = $codeigniter_instance->config->item('FIREBASE_NOTIFICATION_TITLE');
        $data = array(
        	"to" => $sendto,
        	"notification" => array(
                "title" => $title,
        		"body" => $body,
                "sound" => "default"
        	),
            "priority" => "high"
        );
        if($purchase_transaction_id != null){
            $data["data"] = array(
                "purchase_transaction_id" => $purchase_transaction_id
            );
        }
        
        $postdata = json_encode($data);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
		// Set HTTP Header for POST request 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'Content-Type: application/json',
		    'Authorization: key='.$key,
		    'Content-Length: ' . strlen($postdata))
		);

		// Submit the POST request
		$result = curl_exec($ch);

		// Close cURL session handle
		curl_close($ch);

	  	return json_decode($result);

	}
}