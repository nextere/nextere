<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authentication{
    
    public static function checkkey($key, $masster_key){
        /** @CODEIGNITER_INSTANCE  $codeigniterInstance */
        $codeigniter_instance =& get_instance();

        if (empty($key)) {
             redirect(base_url()."/Index/notfound");
        }
        else{
            if ($masster_key != $key) {
                redirect(base_url()."/Index/notfound");
            }
            else {
                $now = date("Y-m-d H:i:s");
                if($now >= "2020-12-30 20:00:00" && $now < "2021-01-04 09:00:00"){
                    redirect(base_url()."/Index/freezsystem");
                }
            }
        }
    }

}
