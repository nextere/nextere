<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Authentication {

    public function verifySession()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
         * /** ------------------------------------------------------- **/
        $codeigniter_instance =& get_instance();

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
         * /** ------------------------------------------------------- **/
        $directory = strtolower($codeigniter_instance->router->directory);
        $controller = strtolower($codeigniter_instance->router->fetch_class());
        $function = strtolower($codeigniter_instance->router->fetch_method());

        if($controller != "authentication" && $function != "login")
        {
            if(strpos($directory, 'administrator_area/') !== false)
            {
                $administrator_id = $codeigniter_instance->session->userdata("__administrator::id");

                if($administrator_id == null)
                {
                    $codeigniter_instance->session->sess_destroy();
                    redirect(base_url("administrator_area/authentication/login"));
                }
            }
        }
    }
}