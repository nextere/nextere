<?php
defined('BASEPATH') or exit('No direct script access allowed');

class How_to extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/

    public function index()
    {
        $conditions_list = file_get_contents(base_url('/api_area/home/conditions'));
        $conditions_list = json_decode($conditions_list);
        $conditions_list = $conditions_list->data->conditions;

        $this->sortArrayByKey($conditions_list, "order", false, false);

        $this->load->view("Howto", compact('conditions_list'));
    }


    function sortArrayByKey($array, $key, $string = false, $asc = true)
    {
        if ($string) {
            usort($array, function ($a, $b) use ($key, $asc) {
                if ($asc)    return strcmp(strtolower($a->$key), strtolower($b->$key));
                else        return strcmp(strtolower($b->$key), strtolower($a->$key));
            });
        } else {
            usort($array, function ($a, $b) use ($key, $asc) {
                if ($a->$key == $b->$key) {
                    return 0;
                }
                if ($asc) return ($a->$key < $b->$key) ? -1 : 1;
                else     return ($a->$key > $b->$key) ? -1 : 1;
            });
        }
    }
}
