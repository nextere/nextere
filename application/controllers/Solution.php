<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Solution extends CI_Controller
{
    var $api_url;
    function __construct()
    {

        parent::__construct();
        $this->api_url = $this->config->item('WEBSITE_API_URL');
    }

    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/

    public function detail($category_id = null)
    {
        if (!empty($category_id)) {

            if (!empty($_GET["type"])) {
                $type = $_GET["type"];

                // echo '<pre>';
                // print_r($type);
                // exit();

                if ($type == 'solution') {

                    $postdata = http_build_query(
                        array(
                            'menu_category_id' => $category_id
                        )
                    );

                    $opts = array(
                        'http' =>
                        array(
                            'method' => 'POST',
                            'header' => array(
                                'Content-type: application/x-www-form-urlencoded'
                            ),
                            'content' => $postdata
                        )
                    );

                    $context = stream_context_create($opts);

                    $category = file_get_contents($this->api_url . "/api_area/home/product_detail_menu", false, $context);

                    $category = json_decode($category);

                    // echo '<pre>';
                    // print_r($category);
                    // exit();

                    $this->load->view("SmartLiving", compact('category'));
                } else {

                    $postdata = http_build_query(
                        array(
                            'submenu_category_id' => $category_id
                        )
                    );

                    $opts = array(
                        'http' =>
                        array(
                            'method' => 'POST',
                            'header' => array(
                                'Content-type: application/x-www-form-urlencoded'
                            ),
                            'content' => $postdata
                        )
                    );

                    $context = stream_context_create($opts);

                    $sub_category = file_get_contents($this->api_url . "/api_area/home/product_detail_menu", false, $context);

                    $sub_category_list = json_decode($sub_category);

                    $category = file_get_contents($this->api_url . "/api_area/home/product_detail_menu", false, $context);
                    $category_list = json_decode($category);
                    $category_list =  $category_list->data;
                    // echo '<pre>';
                    // print_r($category_list);
                    // exit();

                    $this->load->view("HomeAutomation", compact('sub_category_list', 'category_list'));
                }
            } else {
                redirect(base_url(''));
            }
        }
    }

    public function soluton_product_list($category_id = null)
    {
        if ($this->input->post()) {
            // echo '<pre>';
            // print_r($this->input->post('type'));
            // exit();

            if ($this->input->post('page') == "") {
                $page = 1;
            } else {
                $page = $this->input->post('page');
            }

            $postdata = http_build_query(
                array(
                    'child_submenu_category_id' => $this->input->post('child_submenu_category'),
                    'page' => $page,
                    'show_item' => $this->input->post('show_item'),
                    'price_rate_id' => $this->input->post('price_rate_id'),
                    'technical_spec_id' => $this->input->post('technical_spec_id')
                )
            );
            // echo '<pre>';
            // print_r($this->input->post());
            // exit();



            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $product_searched = file_get_contents($this->api_url . "api_area/home/product_detail_menu/", false, $context);
            $product_searched = json_decode($product_searched);

            // $product_searched = $product_searched->data;

            // echo '<pre>';
            // print_r($this->input->post());
            // print_r($postdata);
            // print_r($product_searched);
            // exit();

            $this->load->library("response_library");
            $this->response_library->responseJSON($product_searched->code, $product_searched->message, $product_searched->data);
        } else {
            if (!empty($_GET["type"])) {
                $type = $_GET["type"];
                if ($type == 'solution') {
                    $postdata = http_build_query(
                        array(
                            'menu_category_id' => $category_id,
                            'show_item' => '12',
                            'page' => '1'
                        )
                    );
                } else if ($type == 'sub_solution') {
                    $postdata = http_build_query(
                        array(
                            'submenu_category_id' => $category_id,
                            'show_item' => '12',
                            'page' => '1'
                        )
                    );
                } else {
                    $postdata = http_build_query(
                        array(
                            'child_submenu_category_id' => $category_id,
                            'show_item' => '12',
                            'page' => '1'
                        )
                    );
                }



                $opts = array(
                    'http' =>
                    array(
                        'method' => 'POST',
                        'header' => array(
                            'Content-type: application/x-www-form-urlencoded'
                        ),
                        'content' => $postdata
                    )
                );

                $context = stream_context_create($opts);

                $solution_product = file_get_contents($this->api_url . "api_area/home/product_detail_menu", false, $context);

                $solution_product = json_decode($solution_product);

                $solution_product = $solution_product->data;


                $price_rate = file_get_contents($this->api_url . "api_area/product/price_rate", false, $context);

                $price_rate = json_decode($price_rate);

                $price_rate = $price_rate->data;

                $technical_list = file_get_contents($this->api_url . "api_area/Home/filter_solution/" . $category_id, false, $context);
                $technical_list = json_decode($technical_list);
                $technical_list = $technical_list->data->filter_solution;

                // echo '<pre>';
                // print_r($technical_list);
                // exit();

                $this->load->view("solution-product-list", compact('solution_product', 'price_rate', 'technical_list'));
            } else {
                redirect(base_url(''));
            }
        }
    }
}
