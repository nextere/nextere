<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Footer extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $submenu = file_get_contents(base_url('/api_area/home/menu2'));
        $submenu = json_decode($submenu);
        $submenu = $submenu->data;
        //  echo '<pre>';
        // print_r($submenu);
        // exit();
        $contactus_list = file_get_contents(base_url('/api_area/home/contactus'));
        $contactus_list = json_decode($contactus_list);
        $contactus_list =  $contactus_list->data->contactus;
        // echo '<pre>';
        // print_r($contactus_list);
        // exit();
        
      
        $this->load->view("footer", compact('submenu', 'contactus_list'));
    }
}
