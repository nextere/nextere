<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_category extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/product_category/Index");
    }

    public function edit($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_category_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $product_category = $this->product_category_model->find($id);
        if($this->input->post())
        {
            $name_th = $this->input->post("name_th");
            $name_en = $this->input->post("name_en");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["name_th"] = $name_th;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->product_category_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "มีการใช้ชื่อนี้แล้ว");
            }
            $thumbnail_path = '';
            if (!file_exists(FCPATH.'/assets/uploads/product_category/thumbnail/')) {
                mkdir(FCPATH.'/assets/uploads/product_category/thumbnail/', 0777, true);
            }
            if (!empty($_FILES['thumbnail_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = FCPATH.'/assets/uploads/product_category/thumbnail/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("thumbnail_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $thumbnail_path = '/assets/uploads/product_category/thumbnail/'.$upload_data['file_name'];
                }
                else
                {
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            }

            $banner_path = '';
            if (!file_exists(FCPATH.'/assets/uploads/product_category/banner/')) {
                mkdir(FCPATH.'/assets/uploads/product_category/banner/', 0777, true);
            }
            if (!empty($_FILES['banner_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = FCPATH.'/assets/uploads/product_category/banner/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("banner_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $banner_path = '/assets/uploads/product_category/banner/'.$upload_data['file_name'];
                }
                else
                {
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            }

 
            if($id != null)
            {
                $product_category = $this->product_category_model->find($id);
                $product_category->modified_date = date("Y-m-d h:i:s");
                $product_category->modified_by = $this->session->userdata("__administrator::id");

            }
            else
            {
                $product_category = new stdClass();
                $product_category->created_date = date("Y-m-d h:i:s");
                $product_category->created_by = $this->session->userdata("__administrator::id");

                $sort_filter = new stdClass();
                $sort_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
                $counts = $this->product_category_model->search($sort_filter);
                $count = count($counts);
                $product_category->order = $count+1;
            }

            $product_category->name_th = $name_th;
            $product_category->name_en = $name_en;
            $product_category->description_th = $this->input->post("description_th");
            $product_category->description_en = $this->input->post("description_en");
            if(!empty($thumbnail_path))
                $product_category->thumbnail_path = $thumbnail_path;
            if(!empty($banner_path))
                $product_category->banner_path = $banner_path;
            $product_category->status = $this->input->post("status");
            
            $id = $this->product_category_model->save($product_category);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "product_category" => $product_category
        );
        $this->load->view("administrator_area/product_category/Edit",$data);
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_category_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $product_category = $this->product_category_model->find($id);
        $product_category->modified_date = date("Y-m-d h:i:s");
        $product_category->modified_by = $this->session->userdata("__administrator::administrator_id");
        $product_category->status = "REMOVED";

        $this->product_category_model->save($product_category);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    public function subcategory($product_category_id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_category_model");
        $this->load->model("product_sub_category_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $product_category = $this->product_category_model->find($product_category_id);
        

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "product_category" => $product_category
        );
        $this->load->view("administrator_area/product_category/Subcategory",$data);
    }


    public function subcategory_edit($product_category_id = null, $id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_category_model");
        $this->load->model("product_sub_category_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $product_category = $this->product_category_model->find($product_category_id);
        $product_sub_category = $this->product_sub_category_model->find($id);
        if($this->input->post())
        {
            $name_th = $this->input->post("name_th");
            $name_en = $this->input->post("name_en");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["name_th"] = $name_th;
            $dup_filter->where["product_category_id"]  = $product_category_id;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->product_sub_category_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "มีการใช้ชื่อนี้แล้ว");
            }

            if($id != null)
            {
                $product_sub_category = $this->product_sub_category_model->find($id);
                $product_sub_category->modified_date = date("Y-m-d h:i:s");
                $product_sub_category->modified_by = $this->session->userdata("__administrator::id");


            }
            else
            {
                $product_sub_category = new stdClass();
                $product_sub_category->created_date = date("Y-m-d h:i:s");
                $product_sub_category->created_by = $this->session->userdata("__administrator::id");

                $sort_filter = new stdClass();
                $sort_filter->where["product_category_id"]  = $product_category_id;
                $sort_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
                $counts = $this->product_sub_category_model->search($sort_filter);
                $count = count($counts);
                $product_sub_category->order = $count+1;
            }

            $product_sub_category->product_category_id = $product_category_id;
            $product_sub_category->name_th = $name_th;
            $product_sub_category->name_en = $name_en;
            $product_sub_category->status = $this->input->post("status");
            
            $id = $this->product_sub_category_model->save($product_sub_category);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "product_category" => $product_category,
            "product_sub_category" => $product_sub_category
        );
        $this->load->view("administrator_area/product_category/Subcategory_edit",$data);
    }


    public function subcategory_delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_sub_category_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $product_sub_category = $this->product_sub_category_model->find($id);
        $product_sub_category->modified_date = date("Y-m-d h:i:s");
        $product_sub_category->modified_by = $this->session->userdata("__administrator::administrator_id");
        $product_sub_category->status = "REMOVED";

        $this->product_sub_category_model->save($product_sub_category);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    public function childsubcategory($product_category_id = null, $product_sub_category_id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_category_model");
        $this->load->model("product_sub_category_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $product_category = $this->product_category_model->find($product_category_id);
        $product_sub_category = $this->product_sub_category_model->find($product_sub_category_id);
        

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "product_category" => $product_category,
            "product_sub_category" => $product_sub_category
        );
        $this->load->view("administrator_area/product_category/Childsubcategory",$data);
    }

    public function childsubcategory_edit($product_sub_category_id = null, $id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_category_model");
        $this->load->model("product_sub_category_model");
        $this->load->model("product_childsub_category_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $model_filter = new stdClass();
        $model_filter->where["product_sub_category.id"] = $product_sub_category_id;
        $model_filter->join = array(
            "product_category" => "product_sub_category.product_category_id = product_category.id"
        );
        $model_filter->get_first = true;
        $product_sub_category = $this->product_sub_category_model->search($model_filter);
        $product_childsub_category = $this->product_childsub_category_model->find($id);

        if($this->input->post())
        {
            $name_th = $this->input->post("name_th");
            $name_en = $this->input->post("name_en");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["name_th"] = $name_th;
            $dup_filter->where["product_sub_category_id"]  = $product_sub_category_id;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->product_childsub_category_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "มีการใช้ชื่อนี้แล้ว");
            }

            if($id != null)
            {
                $product_childsub_category = $this->product_childsub_category_model->find($id);
                $product_childsub_category->modified_date = date("Y-m-d h:i:s");
                $product_childsub_category->modified_by = $this->session->userdata("__administrator::id");

                
            }
            else
            {
                $product_childsub_category = new stdClass();
                $product_childsub_category->created_date = date("Y-m-d h:i:s");
                $product_childsub_category->created_by = $this->session->userdata("__administrator::id");

                $sort_filter = new stdClass();
                $sort_filter->where["product_sub_category_id"]  = $product_sub_category_id;
                $sort_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
                $counts = $this->product_childsub_category_model->search($sort_filter);
                $count = count($counts);
                $product_childsub_category->order = $count+1;
            }

            $product_childsub_category->product_sub_category_id = $product_sub_category_id;
            $product_childsub_category->name_th = $name_th;
            $product_childsub_category->name_en = $name_en;
            $product_childsub_category->status = $this->input->post("status");
            
            $id = $this->product_childsub_category_model->save($product_childsub_category);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "product_sub_category" => $product_sub_category,
            "product_childsub_category" => $product_childsub_category
        );
        $this->load->view("administrator_area/product_category/Childsubcategory_edit",$data);
    }

    public function childsubcategory_delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_childsub_category_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $product_childsub_category = $this->product_childsub_category_model->find($id);
        $product_childsub_category->modified_date = date("Y-m-d h:i:s");
        $product_childsub_category->modified_by = $this->session->userdata("__administrator::administrator_id");
        $product_childsub_category->status = "REMOVED";

        $this->product_childsub_category_model->save($product_childsub_category);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }


    public function technicalspec($product_category_id = null, $product_sub_category_id = null, $product_childsub_category_id = null)
    {
        $this->load->model("product_category_model");
        $this->load->model("product_sub_category_model");
        $this->load->model("product_childsub_category_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");


        $product_category = $this->product_category_model->find($product_category_id);
        $product_sub_category = $this->product_sub_category_model->find($product_sub_category_id);
        $product_childsub_category = $this->product_childsub_category_model->find($product_childsub_category_id);
        

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "product_category" => $product_category,
            "product_sub_category" => $product_sub_category,
            "product_childsub_category" => $product_childsub_category
        );
        $this->load->view("administrator_area/product_category/Technicalspec",$data);

    }

    public function technicalspec_add($product_category_id = null, $product_sub_category_id = null, $product_childsub_category_id = null)
    {
        $this->load->model("product_category_model");
        $this->load->model("product_sub_category_model");
        $this->load->model("product_childsub_category_model");
        $this->load->model("technical_spec_category_model");
        $this->load->model("technical_spec_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");


        $product_category = $this->product_category_model->find($product_category_id);
        $product_sub_category = $this->product_sub_category_model->find($product_sub_category_id);
        $product_childsub_category = $this->product_childsub_category_model->find($product_childsub_category_id);
        
        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        if ($this->input->post()) {


            if(!empty($this->input->post("technical_spec_id[]"))){

                $technical_spec_id = $this->input->post("technical_spec_id[]");
                $count = count($technical_spec_id);
                for($i=0; $i<$count; $i++) 
                {
                    if($this->input->post("technical_spec_id[".$i."]") != "")
                    {
                        $dup_filter = new stdClass();
                        $dup_filter->where["status"] = "ACTIVATE";
                        $dup_filter->where["technical_spec_id"] = $technical_spec_id[$i];
                        $dup_filter->where["product_category_id"] = $this->input->post("product_category_id");
                        $dup_filter->get_first = true;
                        $dup_data = $this->technical_spec_category_model->search($dup_filter);

                        if ($dup_data == null) {

                            $technical_spec_category = new stdClass();
                            $technical_spec_category->technical_spec_id = $technical_spec_id[$i];
                            $technical_spec_category->product_category_id = $this->input->post("product_category_id");
                            $technical_spec_category->product_sub_category_id = empty($this->input->post("product_sub_category_id")) ? null : $this->input->post("product_sub_category_id");
                            $technical_spec_category->product_childsub_category_id = empty($this->input->post("product_childsub_category_id")) ? null : $this->input->post("product_childsub_category_id");
                            $technical_spec_category->status = "ACTIVATE";
                            $technical_spec_category->created_date = date("Y-m-d h:i:s");
                            $technical_spec_category->created_by = $this->session->userdata("__administrator::id");
                            $this->technical_spec_category_model->save($technical_spec_category);
                            
                        }
                    }
                }

            }
            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        $technical = new stdClass();
        $technical->where["status"] = "ACTIVATE";
        $technical_spec = $this->technical_spec_model->search($technical);


        $data = array(
            "product_category" => $product_category,
            "product_sub_category" => $product_sub_category,
            "product_childsub_category" => $product_childsub_category,
            "technical_spec" => $technical_spec
        );
        $this->load->view("administrator_area/product_category/Technicalspec_add",$data); 

    }

    public function technicalspec_delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("technical_spec_category_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $technical_spec_category = $this->technical_spec_category_model->find($id);
        $technical_spec_category->modified_date = date("Y-m-d h:i:s");
        $technical_spec_category->modified_by = $this->session->userdata("__administrator::administrator_id");
        $technical_spec_category->status = "REMOVED";

        $this->technical_spec_category_model->save($technical_spec_category);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }


    public function technicalspec_category_delete($technical_spec_id = null, $product_category_id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("technical_spec_category_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $update_filter = new stdClass();
        $update_filter->where["technical_spec_id"] = $technical_spec_id;
        $update_filter->where["product_category_id"] = $product_category_id;
        $update_filter->where["status"] = "ACTIVATE";

        $update_data = array(
            "status" => "REMOVED",
            "modified_date" => date("Y-m-d h:i:s"),
            "modified_by" => $this->session->userdata("__administrator::administrator_id")
        );

        $this->technical_spec_category_model->update($update_filter, $update_data);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }



    public function filter_category($product_category_id = null, $product_sub_category_id = null)
    {
        $this->load->model("product_category_model");
        $this->load->model("product_sub_category_model");
        $this->load->model("technical_spec_category_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");


        $product_category = $this->product_category_model->find($product_category_id);
        $product_sub_category = $this->product_sub_category_model->find($product_sub_category_id);

        $model_filter = new stdClass();
        $model_filter->where["technical_spec_category.status"] = "ACTIVATE";
        $model_filter->where_in["technical_spec.status"] = array('ACTIVATE','SUSPEND');
        $model_filter->where_in["technical_spec_category.product_category_id"] =  $product_category_id;
        $model_filter->custom_where = "(technical_spec_category.product_sub_category_id is null OR technical_spec_category.product_sub_category_id = ".$product_sub_category_id.")";
        $model_filter->join  = array(
            "technical_spec" => "technical_spec_category.technical_spec_id = technical_spec.id",
            "filter_category" => "technical_spec_category.technical_spec_id = filter_category.technical_spec_id AND filter_category.product_sub_category_id = ".$product_sub_category_id." AND filter_category.status = 'ACTIVATE'"
        );
        $technical_specs = $this->technical_spec_category_model->search($model_filter);


        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "product_category" => $product_category,
            "product_sub_category" => $product_sub_category,
            "technical_specs" => $technical_specs
        );
        $this->load->view("administrator_area/product_category/Filter_category",$data);

    }

    public function edit_filter_category($product_sub_category_id)
    {
        $this->load->model("product_sub_category_model");
        $this->load->model("filter_category_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $update_filter = new stdClass();
        $update_filter->where["product_sub_category_id"] = $product_sub_category_id;
        $update_filter->where["status"] = "ACTIVATE";

        $update_data = array(
            "status" => "REMOVED"
        );
        $this->filter_category_model->update($update_filter, $update_data);

        $technical_specids = $this->input->post("technical_specid");
        if ($technical_specids != null && count($technical_specids) > 0) {
            foreach ($technical_specids as $technical_spec_id) {
                $filter_category = new stdClass();
                $filter_category->product_sub_category_id = $product_sub_category_id;
                $filter_category->technical_spec_id = $technical_spec_id;
                $filter_category->status = "ACTIVATE";
                $filter_category->created_date = date("Y-m-d h:i:s");
                $filter_category->created_by = $this->session->userdata("__administrator::administrator_id");
                $this->filter_category_model->save($filter_category);
            }
        }
        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");

    }


    public function sort_category()
    {
        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $this->load->model("product_category_model");

        if($this->input->post())
        { 

            $num = $this->input->post("id[]");
            $ids = $this->input->post("id[]");

            $min = min($ids);
            foreach($ids as $key => $id)
            {

                $menu = $this->product_category_model->find($key);
                $menu->modified_date = date("Y-m-d H:i:s");
                $menu->modified_by = $this->session->userdata("__administrator::administrator_id");
                $menu->order = $min;
                $this->product_category_model->save($menu);

                $min++;
            }

            $menu_validated = array("code" => "0x0000-00000", "message" => "Server response success. Request process complete with no error.");

            if($menu_validated["code"] == "0x0000-00000")
            {

                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }
            else
            {
                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }

        }
    }
    public function sort_subcategory()
    {
        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $this->load->model("product_category_model");
        $this->load->model("product_sub_category_model");

        if($this->input->post())
        { 

            $num = $this->input->post("id[]");
            $ids = $this->input->post("id[]");

            $min = min($ids);
            foreach($ids as $key => $id)
            {

                $menu = $this->product_sub_category_model->find($key);
                $menu->modified_date = date("Y-m-d H:i:s");
                $menu->modified_by = $this->session->userdata("__administrator::administrator_id");
                $menu->order = $min;
                $this->product_sub_category_model->save($menu);

                $min++;
            }

            $menu_validated = array("code" => "0x0000-00000", "message" => "Server response success. Request process complete with no error.");

            if($menu_validated["code"] == "0x0000-00000")
            {

                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }
            else
            {
                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }

        }
    }
    public function sort_childsubcategory()
    {
        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $this->load->model("product_category_model");
        $this->load->model("product_sub_category_model");
        $this->load->model("product_childsub_category_model");

        if($this->input->post())
        { 

            $num = $this->input->post("id[]");
            $ids = $this->input->post("id[]");

            $min = min($ids);
            foreach($ids as $key => $id)
            {

                $menu = $this->product_childsub_category_model->find($key);
                $menu->modified_date = date("Y-m-d H:i:s");
                $menu->modified_by = $this->session->userdata("__administrator::administrator_id");
                $menu->order = $min;
                $this->product_childsub_category_model->save($menu);

                $min++;
            }

            $menu_validated = array("code" => "0x0000-00000", "message" => "Server response success. Request process complete with no error.");

            if($menu_validated["code"] == "0x0000-00000")
            {

                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }
            else
            {
                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }

        }
    }





    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "product_category";
            $default_filter = "product_category.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "product_category.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "product_category.order"       => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "order", "type" => "TEXT"),
                "product_category.name_th"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "name_th", "type" => "TEXT"),
                "product_category.name_en"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "name_en", "type" => "TEXT")
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
        else if($function == "index_subcategory")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "product_sub_category";
            $default_filter = "product_sub_category.status IN ('ACTIVATE','SUSPEND') AND product_sub_category.product_category_id = ".$this->input->post("product_category_id");
            $request_filter = array(
                "product_sub_category.id"           => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "product_sub_category.product_category_id" => array("method" => "LIKE", "value" => null, "alias" => "product_category_id" , "type" => "TEXT"),
                "product_sub_category.order"        => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "order", "type" => "TEXT"),
                "product_sub_category.name_th"      => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "name_th", "type" => "TEXT"),
                "product_sub_category.name_en"      => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "name_en", "type" => "TEXT")
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
        else if($function == "index_childsubcategory")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "product_childsub_category";
            $default_filter = "product_childsub_category.status IN ('ACTIVATE','SUSPEND') AND product_childsub_category.product_sub_category_id = ".$this->input->post("product_sub_category_id");
            $request_filter = array(
                "product_childsub_category.id"         => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "product_sub_category.product_category_id" => array("method" => "LIKE", "value" => null, "alias" => "product_category_id" , "type" => "TEXT"),
                "product_childsub_category.product_sub_category_id" => array("method" => "LIKE", "value" => null, "alias" => "product_sub_category_id" , "type" => "TEXT"),
                "product_childsub_category.order"      => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "order", "type" => "TEXT"),
                "product_childsub_category.name_th"    => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "name_th", "type" => "TEXT"),
                "product_childsub_category.name_en"    => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "name_en", "type" => "TEXT")
                
            );
            $join = array(
                "product_sub_category" => "product_childsub_category.product_sub_category_id = product_sub_category.id"
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
        else if($function == "index_technicalspec")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "technical_spec_category";
            $default_group = "";
            $default_filter = "technical_spec_category.status = 'ACTIVATE' AND technical_spec.status IN ('ACTIVATE','SUSPEND') AND technical_spec_category.product_category_id = ".$this->input->post("product_category_id");
            if (!empty($this->input->post('product_sub_category_id'))) {
                $default_filter.=" AND (technical_spec_category.product_sub_category_id is null OR technical_spec_category.product_sub_category_id = ".$this->input->post('product_sub_category_id').")";
                $request_filter = array(
                    "technical_spec_category.id"    => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                    "technical_spec_category.technical_spec_id"    => array("method" => "LIKE", "value" => null, "alias" => "technical_spec_id" , "type" => "TEXT"),
                    "technical_spec_category.product_category_id"    => array("method" => "LIKE", "value" => null, "alias" => "product_category_id" , "type" => "TEXT"),
                    "technical_spec_category.product_sub_category_id"    => array("method" => "LIKE", "value" => null, "alias" => "product_sub_category_id" , "type" => "TEXT"),
                    "technical_spec_category.product_childsub_category_id"    => array("method" => "LIKE", "value" => null, "alias" => "product_childsub_category_id" , "type" => "TEXT"),
                    "technical_spec.name_th"        => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "name_th", "type" => "TEXT"),
                    "technical_spec.name_en"        => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "name_en", "type" => "TEXT")
                );
            }
            else{
                $request_filter = array(
                    "technical_spec_category.technical_spec_id"    => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                    "technical_spec_category.technical_spec_id"    => array("method" => "LIKE", "value" => null, "alias" => "technical_spec_id" , "type" => "TEXT"),
                    "technical_spec_category.product_category_id"    => array("method" => "LIKE", "value" => null, "alias" => "product_category_id" , "type" => "TEXT"),
                    "technical_spec_category.product_sub_category_id"    => array("method" => "LIKE", "value" => null, "alias" => "product_sub_category_id" , "type" => "TEXT"),
                    "technical_spec_category.product_childsub_category_id"    => array("method" => "LIKE", "value" => null, "alias" => "product_childsub_category_id" , "type" => "TEXT"),
                    "technical_spec.name_th"        => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "name_th", "type" => "TEXT"),
                    "technical_spec.name_en"        => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "name_en", "type" => "TEXT")
                );
                $default_group = "technical_spec_category.technical_spec_id,technical_spec_category.product_category_id ";
            }


            
            $join = array(
                "technical_spec" => "technical_spec_category.technical_spec_id = technical_spec.id"
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join,$default_group);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join, $default_group);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join,$default_group);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        if($function == "edit_technical_spec")
        {
            $this->load->model("technical_spec_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_en"] = $this->input->get("search");
            }
            $technical_specs = $this->technical_spec_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $technical_specs);
        }
    }
}