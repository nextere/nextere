<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/member/Index");
    }

    public function detail($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("member_model");
        $this->load->model("member_address_model");
        $this->load->model("member_address_tax_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $member = $this->member_model->find($id);
        
        $model_filter = new stdClass();
        $model_filter->where["member_address.member_id"] = $id;
        $model_filter->where["member_address.status"] = "ACTIVATE";
        $model_filter->join = array(
            "address_province" => "member_address.address_province_id = address_province.id",
            "address_district" => "member_address.address_district_id = address_district.id",
            "address_sub_district" => "member_address.address_sub_district_id = address_sub_district.id"
        );
        $model_filter->order_by = "member_address.is_default desc";
        $member_addresses = $this->member_address_model->search($model_filter);

        $model_filter = new stdClass();
        $model_filter->where["member_address_tax.member_id"] = $id;
        $model_filter->where["member_address_tax.status"] = "ACTIVATE";
        $model_filter->join = array(
            "address_province" => "member_address_tax.address_province_id = address_province.id",
            "address_district" => "member_address_tax.address_district_id = address_district.id",
            "address_sub_district" => "member_address_tax.address_sub_district_id = address_sub_district.id"
        );
        $model_filter->order_by = "member_address_tax.is_default desc";
        $member_address_taxs = $this->member_address_tax_model->search($model_filter);

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "member" => $member,
            "member_addresses" => $member_addresses,
            "member_address_taxs" => $member_address_taxs
        );
        $this->load->view("administrator_area/member/Detail",$data);
    }


    public function export_member()
    {

        $this->load->model("member_model");

        $this->load->library('excel');

        $model_filter = new stdClass();
        $model_filter->where["status"] = "ACTIVATE";
        $members = $this->member_model->search($model_filter);

        $this->load->view("administrator_area/exportexcel/Member", compact("members"));
        
    }


    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "member";
            $default_filter = "member.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "member.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "member.email"       => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "email", "type" => "TEXT"),
                "member.fullname"    => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "fullname", "type" => "TEXT"),
                "member.gender"      => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "gender", "type" => "TEXT"),
                "member.telephone"   => array("method" => "LIKE", "value" => $datatables_request["columns"]["3"]["search"]["value"]  , "alias" => "telephone", "type" => "TEXT")
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        
    }
}