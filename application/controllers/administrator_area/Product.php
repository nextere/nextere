<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller
{
    private $product_path = FCPATH.'/assets/uploads/product/';
    private $product_feature_path = FCPATH.'/assets/uploads/product_feature/';
    private $product_datasheet_path = FCPATH.'/assets/uploads/product_datasheet/';
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/product/Index");
    }
    public function detail($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $model_filter = new stdClass();
        $model_filter->where["product.id"] = $id;
        $model_filter->join = array(
            "brand" => "product.brand_id = brand.id",
            "product_type" => "product.product_type_id = product_type.id"
        );
        $model_filter->get_first = true;
        $product = $this->product_model->search($model_filter);
        $product_images = array();
        if ($product != null) {
            $dir = $this->product_path.$product->{"product::code"};
            if (is_dir($dir)){
              $product_images = array_diff(scandir($dir), array('.', '..')); 
          }
      }

      $data = array(
        "product" => $product,
        "product_images" => $product_images
    );
      $this->load->view("administrator_area/product/Detail",$data);
  }
  public function review($id = null)
  {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_model");
        $this->load->model("review_model");
        $this->load->model("review_image_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/

        $model_filter = new stdClass();
        $model_filter->select = 'COUNT(id) as count,rating';
        $model_filter->where["product_id"] = $id;
        $model_filter->where["rating"] = 5;
        $model_filter->group_by = 'rating';
        $model_filter->get_first = true;
        $scores_5 = $this->review_model->search($model_filter);

        $model_filter = new stdClass();
        $model_filter->select = 'COUNT(id) as count,rating';
        $model_filter->where["product_id"] = $id;
        $model_filter->where["rating"] = 4;
        $model_filter->group_by = 'rating';
        $model_filter->get_first = true;
        $scores_4 = $this->review_model->search($model_filter);

        $model_filter = new stdClass();
        $model_filter->select = 'COUNT(id) as count,rating';
        $model_filter->where["product_id"] = $id;
        $model_filter->where["rating"] = 3;
        $model_filter->group_by = 'rating';
        $model_filter->get_first = true;
        $scores_3 = $this->review_model->search($model_filter);


        $model_filter = new stdClass();
        $model_filter->select = 'COUNT(id) as count,rating';
        $model_filter->where["product_id"] = $id;
        $model_filter->where["rating"] = 2;
        $model_filter->group_by = 'rating';
        $model_filter->get_first = true;
        $scores_2 = $this->review_model->search($model_filter);

        $model_filter = new stdClass();
        $model_filter->select = 'COUNT(id) as count,rating';
        $model_filter->where["product_id"] = $id;
        $model_filter->where["rating"] = 1;
        $model_filter->group_by = 'rating';
        $model_filter->get_first = true;
        $scores_1 = $this->review_model->search($model_filter);


        $model_filter = new stdClass();
        $model_filter->select = 'COUNT(id) as count,rating';
        $model_filter->where["product_id"] = $id;
        $model_filter->where["rating"] = 0;
        $model_filter->group_by = 'rating';
        $model_filter->get_first = true;
        $scores_0 = $this->review_model->search($model_filter);


        $model_filter = new stdClass();
        $model_filter->select = 'AVG(rating) as rating';
        $model_filter->where["product_id"] = $id;
        $model_filter->get_first = true;
        $avg = $this->review_model->search($model_filter);
        


        $model_filter = new stdClass();
        $model_filter->where["review.product_id"] = $id;
        $model_filter->join = array(
            "member" => "review.member_id = member.id",
        );
        $members = $this->review_model->search($model_filter);


        $model_filter = new stdClass();
        $model_filter->where["review.product_id"] = $id;
        $model_filter->join = array(
            "review" => "review_image.review_id = review.id",
        );
        $comments = $this->review_image_model->search($model_filter);

        

        $data = array(
            "scores_0" => $scores_0,
            "scores_1" => $scores_1,
            "scores_2" => $scores_2,
            "scores_3" => $scores_3,
            "scores_4" => $scores_4,
            "scores_5" => $scores_5,
            "avg" => $avg,
            "members" => $members,
            "comments" => $comments,
        );
        $this->load->view("administrator_area/product/Review",$data);
    }

    public function edit($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_model");
        $this->load->model("product_price_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $model_filter = new stdClass();
        $model_filter->where["product.id"] = $id;
        $model_filter->join = array(
            "brand" => "product.brand_id = brand.id",
            "product_type" => "product.product_type_id = product_type.id"
        );
        $model_filter->get_first = true;
        $product = $this->product_model->search($model_filter);
        $product_images = array();
        if ($product != null) {
            $dir = $this->product_path.$product->{"product::code"};
            if (is_dir($dir)){
              $product_images = array_diff(scandir($dir), array('.', '..')); 
          }
      }
      if($this->input->post())
      {
        $code = trim($this->input->post("code"));
        $name_th = trim($this->input->post("name_th"));
        $name_en = trim($this->input->post("name_en"));

        if (empty($code) || empty($name_th) || empty($name_en)) {
            $this->response_library->responseJSON("0x000I-00001", "Invalid data");
        }

        $image_names = $this->input->post("image_name");
        if ($product_images != null && count($product_images) > 0) {
            $dir = $this->product_path.$product->{"product::code"};
            foreach ($product_images as $product_image) {
                if ($image_names == null || count($image_names) ==0 || !in_array($product_image, $image_names)) {
                    unlink($dir.'/'.$product_image);
                }
            }
        }
        

            //Dup Data
        $dup_filter = new stdClass();
        $dup_filter->where["code"] = $code;
        if($id != null)
            $dup_filter->where["id != "] = $id;
        $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
        $dup_filter->get_first = true;
        $dup_data = $this->product_model->search($dup_filter);
        if ($dup_data != null) {
            $this->response_library->responseJSON("0x000C-U0002", "Duplicate data");
        }


        if($id != null)
        {
            $product = $this->product_model->find($id);
            $product->modified_date = date("Y-m-d h:i:s");
            $product->modified_by = $this->session->userdata("__administrator::id");
        }
        else
        {
            $product = new stdClass();
            $product->created_date = date("Y-m-d h:i:s");
            $product->created_by = $this->session->userdata("__administrator::id");
        } 

        $product->code = $code;
        $product->brand_id = empty($this->input->post("brand_id")) ? null : $this->input->post("brand_id");
        $product->name_th = $name_th;
        $product->name_en = $name_en;
        $product->model_name_th = trim($this->input->post("model_name_th"));
        $product->model_name_en = trim($this->input->post("model_name_en"));
        $product->product_type_id = empty($this->input->post("product_type_id")) ? null : $this->input->post("product_type_id");
        $product->detail_th = trim($this->input->post("detail_th"));
        $product->detail_en = trim($this->input->post("detail_en"));
        $product->description_th = trim($this->input->post("description_th"));
        $product->description_en = trim($this->input->post("description_en"));
        $product->basic_quality_th = trim($this->input->post("basic_quality_th"));
        $product->basic_quality_en = trim($this->input->post("basic_quality_en"));
        $product->retail_price = $this->input->post("retail_price") == '' ? null : $this->input->post("retail_price");
        $product->waiting_date = $this->input->post("waiting_date") == '' ? null : $this->input->post("waiting_date");
        $product->minimum_product = $this->input->post("minimum_product") == '' ? null : $this->input->post("minimum_product");
        $product->condition_product_th = trim($this->input->post("condition_product_th"));
        $product->condition_product_en = trim($this->input->post("condition_product_en"));
        $product->condition_warranty_th = trim($this->input->post("condition_warranty_th"));
        $product->condition_warranty_en = trim($this->input->post("condition_warranty_en"));
        if($id == null || !isset($product->in_stock_qty))
            $product->total_qty = $this->input->post("in_stock_qty") == '' ? null : $this->input->post("in_stock_qty");
        else if($this->input->post("in_stock_qty") != '' && $this->input->post("in_stock_qty") >= $product->in_stock_qty)
            $product->total_qty =  $product->total_qty +($this->input->post("in_stock_qty") - $product->in_stock_qty) ;

        $product->in_stock_qty = $this->input->post("in_stock_qty") == '' ? null : $this->input->post("in_stock_qty");
        $product->safety_stock = $this->input->post("safety_stock") == '' ? null : $this->input->post("safety_stock");
        $product->delivery_price = $this->input->post("delivery_price") == '' ? null : $this->input->post("delivery_price");
        $product->search_keyword = trim($this->input->post("search_keyword"));
        $product->is_recommend = $this->input->post("is_recommend") == '' ? 0 :1;
        $product->is_bestseller = $this->input->post("is_bestseller")== '' ? 0 :1;
        $product->is_new = $this->input->post("is_new")== '' ? 0 :1;
        $product->status = $this->input->post("status");
        
        $id = $this->product_model->save($product); 

        if (!file_exists($this->product_path.$code.'/')) {

            $oldmask = umask(0);
            mkdir($this->product_path.$code.'/', 0777, true);
            umask($oldmask);
        } 

        if(isset($_FILES["files"]))
        {
            $total = count($_FILES['files']['name']);
            for($i=0; $i<$total; $i++)
            {
                $tmpFilePath = $_FILES['files']['tmp_name'][$i];

                if($tmpFilePath != "")
                {
                    $info1 = pathinfo($_FILES['files']['name'][$i]);
                    $ext = $info1['extension'];
                    $fileName = date("YmdHis")."_".$this->guid_library->generate()."." . $ext; 
                    $newFilePath =$this->product_path.$code.'/'. $fileName;

                    if(move_uploaded_file($tmpFilePath, $newFilePath)){}
                }
        }
    }        

    $this->response_library->responseJSON("0x0000-00000", "Process request Complete.", $id);
}


        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "product" => $product,
            "product_images" => $product_images
        );
        $this->load->view("administrator_area/product/Edit",$data);
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $product = $this->product_model->find($id);
        $product->modified_date = date("Y-m-d h:i:s");
        $product->modified_by = $this->session->userdata("__administrator::id");
        $product->status = "REMOVED";

        $this->product_model->save($product);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    public function delete_all()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_model");

        $this->load->library("response_library");

         /** ------------------------------------------------------- **
         **           PROCESS PHASE
         /** ------------------------------------------------------- **/
         $id = $this->input->post("id");
         $count_id = count($id);

         for ($i=0; $i < $count_id; $i++) { 

            $product = $this->product_model->find($id[$i]);
            $product->modified_date = date("Y-m-d h:i:s");
            $product->modified_by = $this->session->userdata("__administrator::id");
            $product->status = "REMOVED";


            $this->product_model->save($product);
        }

        


        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    public function status_review()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("review_model");

        $id = $this->input->post("id");
        $checked = $this->input->post("checked");


        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $product = $this->review_model->find($id);
        $product->modified_date = date("Y-m-d h:i:s");
        $product->modified_by = $this->session->userdata("__administrator::id");
        $product->status = $checked;
        

        $this->review_model->save($product);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    //Feature
    public function edit_product_feature()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_feature_model");
        $this->load->model("product_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           POST PHASE
        /** ------------------------------------------------------- **/
        if($this->input->post())
        {
            $id = $this->input->post("id");
            $product_id = $this->input->post("product_id");
            $title_th = $this->input->post("title_th");
            $title_en = $this->input->post("title_en");
            $description_th = $this->input->post("description_th");
            $description_en = $this->input->post("description_en");
            $sort = $this->input->post("sort");
            if (empty($product_id) || $title_th =='' || $title_en=='' || empty($description_th) || empty($description_en)) {
                $this->response_library->responseJSON("0x0API-00001", "Please input data");
            }

            $dup_filter = new stdClass();
            $dup_filter->where["product_id"] = $product_id;
            $dup_filter->where["title_th"] = $title_th;
            $dup_filter->where["status"] = "ACTIVATE";
            if (!empty($id)) {
                $dup_filter->where["id != "] = $id;
            }
            $dup_filter->get_first = true;
            $dup_data = $this->product_feature_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x0API-00002", "Duplicate data");
            }

            $product = $this->product_model->find($product_id);

            $image_path = '';
            if (!file_exists($this->product_feature_path.$product->code.'/')) {

                $oldmask = umask(0);
                mkdir($this->product_feature_path.$product->code.'/', 0777, true);
                umask($oldmask);
            } 
            if (!empty($_FILES['feature_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = $this->product_feature_path.$product->code.'/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|mp4';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("feature_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $image_path = '/assets/uploads/product_feature/'.$product->code.'/'.$upload_data['file_name'];
                }
                else
                {
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            }


            if(empty($id))
            {
                $product_feature = new stdClass();
                $product_feature->created_date = date("Y-m-d h:i:s");
                $product_feature->created_by = $this->session->userdata("__administrator::id");
                $product_feature->status = "ACTIVATE";
            }
            else
            {
                $product_feature = $this->product_feature_model->find($id);
                $product_feature->modified_date = date("Y-m-d h:i:s");
                $product_feature->modified_by = $this->session->userdata("__administrator::id");
                
            }
            if (!empty($image_path)) {
             $product_feature->image_path = $image_path;
         }

         $product_feature->product_id = $product_id;
         $product_feature->title_th = $title_th;
         $product_feature->title_en = $title_en;
         $product_feature->description_th = $description_th;
         $product_feature->description_en = $description_en;
         $product_feature->sort = $sort;
         $product_feature_id = $this->product_feature_model->save($product_feature);

         $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
     }
 }

 public function delete_product_feature()
 {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_feature_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $id = $this->input->post("id");
        $product_feature = $this->product_feature_model->find($id);
        $product_feature->modified_date = date("Y-m-d h:i:s");
        $product_feature->modified_by = $this->session->userdata("__administrator::id");
        $product_feature->status = "REMOVED";

        $this->product_feature_model->save($product_feature);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    //Tutorial
    public function edit_product_tutorial()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_tutorial_model");
        $this->load->model("product_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           POST PHASE
        /** ------------------------------------------------------- **/
        if($this->input->post())
        {
            $id = $this->input->post("id");
            $product_id = $this->input->post("product_id");
            $title_th = $this->input->post("title_th");
            $title_en = $this->input->post("title_en");
            $link_url = $this->input->post("link_url");
            $sort = $this->input->post("sort");
            if (empty($product_id) || $title_th =='' || $title_en=='' || empty($link_url)) {
                $this->response_library->responseJSON("0x0API-00001", "Please input data");
            }

            $dup_filter = new stdClass();
            $dup_filter->where["product_id"] = $product_id;
            $dup_filter->where["title_th"] = $title_th;
            $dup_filter->where["status"] = "ACTIVATE";
            if (!empty($id)) {
                $dup_filter->where["id != "] = $id;
            }
            $dup_filter->get_first = true;
            $dup_data = $this->product_tutorial_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x0API-00002", "Duplicate data");
            }

            if(empty($id))
            {
                $product_tutorial = new stdClass();
                $product_tutorial->created_date = date("Y-m-d h:i:s");
                $product_tutorial->created_by = $this->session->userdata("__administrator::id");
                $product_tutorial->status = "ACTIVATE";
            }
            else
            {
                $product_tutorial = $this->product_tutorial_model->find($id);
                $product_tutorial->modified_date = date("Y-m-d h:i:s");
                $product_tutorial->modified_by = $this->session->userdata("__administrator::id");
                
            }

            $product_tutorial->product_id = $product_id;
            $product_tutorial->title_th = $title_th;
            $product_tutorial->title_en = $title_en;
            $product_tutorial->link_url = $link_url;
            $product_tutorial->sort = $sort;
            $product_tutorial_id = $this->product_tutorial_model->save($product_tutorial);

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }
    }

    public function delete_product_tutorial()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_tutorial_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $id = $this->input->post("id");
        $product_tutorial = $this->product_tutorial_model->find($id);
        $product_tutorial->modified_date = date("Y-m-d h:i:s");
        $product_tutorial->modified_by = $this->session->userdata("__administrator::id");
        $product_tutorial->status = "REMOVED";

        $this->product_tutorial_model->save($product_tutorial);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    //DataSheet
    public function edit_product_datasheet()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_datasheet_model");
        $this->load->model("product_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           POST PHASE
        /** ------------------------------------------------------- **/
        if($this->input->post())
        {
            $id = $this->input->post("id");
            $product_id = $this->input->post("product_id");
            $name = $this->input->post("name");
            $sort = $this->input->post("sort");
            if (empty($product_id) || empty($name)) {
                $this->response_library->responseJSON("0x0API-00001", "Please input data");
            }

            $dup_filter = new stdClass();
            $dup_filter->where["product_id"] = $product_id;
            $dup_filter->where["name"] = $name;
            $dup_filter->where["status"] = "ACTIVATE";
            if (!empty($id)) {
                $dup_filter->where["id != "] = $id;
            }
            $dup_filter->get_first = true;
            $dup_data = $this->product_datasheet_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x0API-00002", "Duplicate data");
            }

            $product = $this->product_model->find($product_id);

            $file_path = '';
            if (!file_exists($this->product_datasheet_path.$product->code.'/')) {

                $oldmask = umask(0);
                mkdir($this->product_datasheet_path.$product->code.'/', 0777, true);
                umask($oldmask);
            } 
            if (!empty($_FILES['datasheet_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = $this->product_datasheet_path.$product->code.'/';
                $config['allowed_types'] = '*';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("datasheet_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $file_path = '/assets/uploads/product_datasheet/'.$product->code.'/'.$upload_data['file_name'];
                }
                else
                {
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            }


            if(empty($id))
            {
                $product_datasheet = new stdClass();
                $product_datasheet->created_date = date("Y-m-d h:i:s");
                $product_datasheet->created_by = $this->session->userdata("__administrator::id");
                $product_datasheet->status = "ACTIVATE";
            }
            else
            {
                $product_datasheet = $this->product_datasheet_model->find($id);
                $product_datasheet->modified_date = date("Y-m-d h:i:s");
                $product_datasheet->modified_by = $this->session->userdata("__administrator::id");
                
            }
            if (!empty($file_path)) {
             $product_datasheet->file_path = $file_path;
         }

         $product_datasheet->product_id = $product_id;
         $product_datasheet->name = $name;
         $product_datasheet->sort = $sort;
         $product_datasheet_id = $this->product_datasheet_model->save($product_datasheet);

         $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
     }
 }

 public function delete_product_datasheet()
 {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_datasheet_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $id = $this->input->post("id");
        $product_datasheet = $this->product_datasheet_model->find($id);
        $product_datasheet->modified_date = date("Y-m-d h:i:s");
        $product_datasheet->modified_by = $this->session->userdata("__administrator::id");
        $product_datasheet->status = "REMOVED";

        $this->product_datasheet_model->save($product_datasheet);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    //Price
    public function edit_product_price()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_price_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           POST PHASE
        /** ------------------------------------------------------- **/
        if($this->input->post())
        {
            $id = $this->input->post("id");
            $product_id = $this->input->post("product_id");
            $minimum_qty = $this->input->post("minimum_qty");
            $price = $this->input->post("price");
            if (empty($product_id) || $minimum_qty =='' || $price=='') {
                $this->response_library->responseJSON("0x0API-00001", "Please input data");
            }

            $dup_filter = new stdClass();
            $dup_filter->where["product_id"] = $product_id;
            $dup_filter->where["minimum_qty"] = $minimum_qty;
            $dup_filter->where["status"] = "ACTIVATE";
            if (!empty($id)) {
                $dup_filter->where["id != "] = $id;
            }
            $dup_filter->get_first = true;
            $dup_data = $this->product_price_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x0API-00002", "Duplicate data");
            }


            if(empty($id))
            {
                $product_price = new stdClass();
                $product_price->created_date = date("Y-m-d h:i:s");
                $product_price->created_by = $this->session->userdata("__administrator::id");
                $product_price->status = "ACTIVATE";
            }
            else
            {
                $product_price = $this->product_price_model->find($id);
                $product_price->modified_date = date("Y-m-d h:i:s");
                $product_price->modified_by = $this->session->userdata("__administrator::id");
                
            }

            $product_price->product_id = $product_id;
            $product_price->minimum_qty = $minimum_qty;
            $product_price->price = $price;
            $product_price_id = $this->product_price_model->save($product_price);

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }
    }

    public function delete_product_price()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_price_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $id = $this->input->post("id");
        $product_price = $this->product_price_model->find($id);
        $product_price->modified_date = date("Y-m-d h:i:s");
        $product_price->modified_by = $this->session->userdata("__administrator::id");
        $product_price->status = "REMOVED";

        $this->product_price_model->save($product_price);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    public function edit_product_category()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_detail_category_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           POST PHASE
        /** ------------------------------------------------------- **/
        if($this->input->post())
        {
            $id = $this->input->post("id");
            $product_id = $this->input->post("product_id");
            $product_category_id = $this->input->post("product_category_id");
            $product_sub_category_id = $this->input->post("product_sub_category_id");
            if (empty($product_id) || empty($product_category_id)) {
                $this->response_library->responseJSON("0x0API-00001", "Please input data");
            }

            $dup_filter = new stdClass();
            $dup_filter->where["product_id"] = $product_id;
            $dup_filter->where["product_category_id"] = $product_category_id;

            $dup_filter->where["product_sub_category_id"] = (empty($product_sub_category_id) || $product_sub_category_id == 0) ? null : $product_sub_category_id;
            
            $dup_filter->where["status"] = "ACTIVATE";
            if (!empty($id)) {
                $dup_filter->where["id != "] = $id;
            }
            $dup_filter->get_first = true;
            $dup_data = $this->product_detail_category_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x0API-00002", "Duplicate data");
            }


            if(empty($id))
            {
                $product_detail_category = new stdClass();
                $product_detail_category->created_date = date("Y-m-d h:i:s");
                $product_detail_category->created_by = $this->session->userdata("__administrator::id");
                $product_detail_category->status = "ACTIVATE";
            }
            else
            {
                $product_detail_category = $this->product_detail_category_model->find($id);
                $product_detail_category->modified_date = date("Y-m-d h:i:s");
                $product_detail_category->modified_by = $this->session->userdata("__administrator::id");
                
            }

            $product_detail_category->product_id = $product_id;
            $product_detail_category->product_category_id = $product_category_id;
            if(!empty($product_sub_category_id) && $product_sub_category_id != 0)
                $product_detail_category->product_sub_category_id = $product_sub_category_id;
            else
                $product_detail_category->product_sub_category_id = null;
            
            $product_detail_category_id = $this->product_detail_category_model->save($product_detail_category);

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }
    }

    public function delete_product_category()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_detail_category_model");
        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $id = $this->input->post("id");
        $product_detail_category = $this->product_detail_category_model->find($id);
        $product_detail_category->modified_date = date("Y-m-d h:i:s");
        $product_detail_category->modified_by = $this->session->userdata("__administrator::id");
        $product_detail_category->status = "REMOVED";

        $this->product_detail_category_model->save($product_detail_category);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    public function edit_menu_category()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_detail_menu_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           POST PHASE
        /** ------------------------------------------------------- **/
        if($this->input->post())
        {
            $id = $this->input->post("id");
            $product_id = $this->input->post("product_id");
            $menu_category_id = $this->input->post("menu_category_id");
            $submenu_category_id = $this->input->post("submenu_category_id");
            $child_submenu_category_id = $this->input->post("child_submenu_category_id");
            if (empty($product_id) || empty($menu_category_id) || (!empty($child_submenu_category_id) && empty($submenu_category_id))) {
                $this->response_library->responseJSON("0x0API-00001", "Please input data");
            }

            $dup_filter = new stdClass();
            $dup_filter->where["product_id"] = $product_id;
            $dup_filter->where["menu_category_id"] = $menu_category_id;

            $dup_filter->where["submenu_category_id"] = (empty($submenu_category_id) || $submenu_category_id == 0) ? null : $submenu_category_id;
            $dup_filter->where["child_submenu_category_id"] = (empty($child_submenu_category_id) || $child_submenu_category_id == 0) ? null : $child_submenu_category_id;
            $dup_filter->where["status"] = "ACTIVATE";
            if (!empty($id)) {
                $dup_filter->where["id != "] = $id;
            }
            $dup_filter->get_first = true;
            $dup_data = $this->product_detail_menu_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x0API-00002", "Duplicate data");
            }


            if(empty($id))
            {
                $product_detail_menu = new stdClass();
                $product_detail_menu->created_date = date("Y-m-d h:i:s");
                $product_detail_menu->created_by = $this->session->userdata("__administrator::id");
                $product_detail_menu->status = "ACTIVATE";
            }
            else
            {
                $product_detail_menu = $this->product_detail_menu_model->find($id);
                $product_detail_menu->modified_date = date("Y-m-d h:i:s");
                $product_detail_menu->modified_by = $this->session->userdata("__administrator::id");
                
            }

            $product_detail_menu->product_id = $product_id;
            $product_detail_menu->menu_category_id = $menu_category_id;
            if(!empty($submenu_category_id) && $submenu_category_id != 0)
                $product_detail_menu->submenu_category_id = $submenu_category_id;
            else
                $product_detail_menu->submenu_category_id = null;

            if(!empty($child_submenu_category_id) && $child_submenu_category_id != 0)
                $product_detail_menu->child_submenu_category_id = $child_submenu_category_id;
            else
                $product_detail_menu->child_submenu_category_id = null;
            $product_detail_menu_id = $this->product_detail_menu_model->save($product_detail_menu);

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }
    }

    public function delete_menu_category()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_detail_menu_model");
        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $id = $this->input->post("id");
        $product_detail_menu = $this->product_detail_menu_model->find($id);
        $product_detail_menu->modified_date = date("Y-m-d h:i:s");
        $product_detail_menu->modified_by = $this->session->userdata("__administrator::id");
        $product_detail_menu->status = "REMOVED";

        $this->product_detail_menu_model->save($product_detail_menu);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }


    public function edit_product_relate()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_relate_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           POST PHASE
        /** ------------------------------------------------------- **/
        if($this->input->post())
        {
            $product_id = $this->input->post("product_id");
            $relate_product_id = $this->input->post("relate_product_id");
            if (empty($product_id) || empty($relate_product_id)) {
                $this->response_library->responseJSON("0x0API-00001", "Please input data");
            }

            $dup_filter = new stdClass();
            $dup_filter->where["main_product_id"] = $product_id;
            $dup_filter->where["relate_product_id"] = $relate_product_id;
            $dup_filter->where["status"] = "ACTIVATE";
            $dup_filter->get_first = true;
            $dup_data = $this->product_relate_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x0API-00002", "Duplicate data");
            }

            $product_relate = new stdClass();
            $product_relate->created_date = date("Y-m-d h:i:s");
            $product_relate->created_by = $this->session->userdata("__administrator::id");
            $product_relate->status = "ACTIVATE";
            $product_relate->main_product_id = $product_id;
            $product_relate->relate_product_id = $relate_product_id;
            $product_relate_id = $this->product_relate_model->save($product_relate);

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }
    }

    public function delete_product_relate()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_relate_model");
        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $id = $this->input->post("id");
        $product_relate = $this->product_relate_model->find($id);
        $product_relate->modified_date = date("Y-m-d h:i:s");
        $product_relate->modified_by = $this->session->userdata("__administrator::id");
        $product_relate->status = "REMOVED";

        $this->product_relate_model->save($product_relate);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    //Technical Spec
    public function detail_technical($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_model");
        $this->load->model("product_detail_category_model");
        $this->load->model("product_technical_spec_model");
        $this->load->model("technical_spec_model");
        $this->load->model("technical_spec_category_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $product = $this->product_model->find($id);
        $model_filter = new stdClass();
        $model_filter->where["product_detail_category.product_id"] = $id;
        $model_filter->where["product_detail_category.status"] = "ACTIVATE";
        $model_filter->custom_where =  "(product_detail_category.product_category_id is null OR product_category.status like 'ACTIVATE') AND (product_detail_category.product_sub_category_id is null OR product_sub_category.status like 'ACTIVATE')"; 
        $model_filter->join = array(
            "product_category" => "product_detail_category.product_category_id = product_category.id",
            "product_sub_category" => "product_detail_category.product_sub_category_id = product_sub_category.id"
        );
        $product_categorys = $this->product_detail_category_model->search($model_filter);
        $technical_spec_ids = array();
        $technical_specs = array();
        if ($product_categorys != null && count($product_categorys) > 0) {
            foreach ($product_categorys as $procat) {
                $model_filter = new stdClass();
                $model_filter->where["technical_spec_category.status"] = "ACTIVATE";
                $model_filter->where["technical_spec.status"] = "ACTIVATE";
                $model_filter->custom_where = "technical_spec_category.product_category_id = ".$procat->{"product_detail_category::product_category_id"};

                if (!empty($procat->{"product_detail_category.product_sub_category_id"})) {
                    $model_filter->custom_where.=" AND (technical_spec_category.product_sub_category_id is null OR technical_spec_category.product_sub_category_id = ".$procat->{"product_detail_category::product_sub_category_id"}.")";
                }
                $model_filter->join = array(
                    "technical_spec" => "technical_spec_category.technical_spec_id = technical_spec.id"
                );
                $technical_spec_categorys = $this->technical_spec_category_model->search($model_filter);
                if ($technical_spec_categorys != null && count($technical_spec_categorys) > 0) {
                    foreach ($technical_spec_categorys as $item) {
                        if(!in_array($item->{"technical_spec::id"}, $technical_spec_ids)){
                            $tech = $this->technical_spec_model->find($item->{"technical_spec::id"});
                            $technical_spec_ids[] = $item->{"technical_spec::id"};
                            $model_filter = new stdClass();
                            $model_filter->where["product_id"] = $id;
                            $model_filter->where["technical_spec_id"] = $item->{"technical_spec::id"};
                            $model_filter->where["status"] = "ACTIVATE";
                            $model_filter->get_first = true;
                            $value_spec = $this->product_technical_spec_model->search($model_filter);
                            $technical_specs[] = array(
                                "technical_spec" => $tech,
                                "value_spec" => $value_spec
                            );
                        }
                    }
                }
                

            }
        }          

        
        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "product" => $product,
            "technical_specs" => $technical_specs
        );
        $this->load->view("administrator_area/product/Detail_technical",$data);
    }

    public function edit_technical($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("product_model");
        $this->load->model("product_detail_category_model");
        $this->load->model("product_technical_spec_model");
        $this->load->model("technical_spec_model");
        $this->load->model("technical_spec_category_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $product = $this->product_model->find($id);
        $model_filter = new stdClass();
        $model_filter->where["product_detail_category.product_id"] = $id;
        $model_filter->where["product_detail_category.status"] = "ACTIVATE";
        $model_filter->custom_where =  "(product_detail_category.product_category_id is null OR product_category.status like 'ACTIVATE') AND (product_detail_category.product_sub_category_id is null OR product_sub_category.status like 'ACTIVATE')"; 
        $model_filter->join = array(
            "product_category" => "product_detail_category.product_category_id = product_category.id",
            "product_sub_category" => "product_detail_category.product_sub_category_id = product_sub_category.id"
        );
        $product_categorys = $this->product_detail_category_model->search($model_filter);
        $technical_spec_ids = array();
        $technical_specs = array();
        if ($product_categorys != null && count($product_categorys) > 0) {
            foreach ($product_categorys as $procat) {
                $model_filter = new stdClass();
                $model_filter->where["technical_spec_category.status"] = "ACTIVATE";
                $model_filter->where["technical_spec.status"] = "ACTIVATE";
                $model_filter->custom_where = "technical_spec_category.product_category_id = ".$procat->{"product_detail_category::product_category_id"};

                if (!empty($procat->{"product_detail_category.product_sub_category_id"})) {
                    $model_filter->custom_where.=" AND (technical_spec_category.product_sub_category_id is null OR technical_spec_category.product_sub_category_id = ".$procat->{"product_detail_category::product_sub_category_id"}.")";
                }
                $model_filter->join = array(
                    "technical_spec" => "technical_spec_category.technical_spec_id = technical_spec.id"
                );
                $technical_spec_categorys = $this->technical_spec_category_model->search($model_filter);
                if ($technical_spec_categorys != null && count($technical_spec_categorys) > 0) {
                    foreach ($technical_spec_categorys as $item) {
                        if(!in_array($item->{"technical_spec::id"}, $technical_spec_ids)){
                            $tech = $this->technical_spec_model->find($item->{"technical_spec::id"});
                            $technical_spec_ids[] = $item->{"technical_spec::id"};
                            $model_filter = new stdClass();
                            $model_filter->where["product_id"] = $id;
                            $model_filter->where["technical_spec_id"] = $item->{"technical_spec::id"};
                            $model_filter->where["status"] = "ACTIVATE";
                            $model_filter->get_first = true;
                            $value_spec = $this->product_technical_spec_model->search($model_filter);
                            $technical_specs[] = array(
                                "technical_spec" => $tech,
                                "value_spec" => $value_spec
                            );
                        }
                    }
                }
                

            }
        }          

        
        if($this->input->post())
        {
            $technical_spec_ids = $this->input->post("technical_spec_id");
            if ($technical_spec_ids != null && count($technical_spec_ids) > 0) {
                foreach ($technical_spec_ids as $tech_id) {
                    $value_th = $this->input->post("valueth_".$tech_id);
                    $value_en = $this->input->post("valueen_".$tech_id);

                    $model_filter = new stdClass();
                    $model_filter->where["product_id"] = $id;
                    $model_filter->where["technical_spec_id"] = $tech_id;
                    $model_filter->where["status"] = "ACTIVATE";
                    $model_filter->get_first = true;
                    $value_spec = $this->product_technical_spec_model->search($model_filter);

                    if($value_spec == null)
                    {
                        $value_spec = new stdClass();
                        $value_spec->created_date = date("Y-m-d h:i:s");
                        $value_spec->created_by = $this->session->userdata("__administrator::id");
                        $value_spec->status = "ACTIVATE";
                        $value_spec->product_id = $id;
                        $value_spec->technical_spec_id = $tech_id;
                    }
                    else
                    {
                        $value_spec->modified_date = date("Y-m-d h:i:s");
                        $value_spec->modified_by = $this->session->userdata("__administrator::id");
                        
                    }

                    $value_spec->value_th = $value_th;
                    $value_spec->value_en = $value_en;
                    $value_spec_id = $this->product_technical_spec_model->save($value_spec);
                }
            }
            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.", $id);
        }


        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "product" => $product,
            "technical_specs" => $technical_specs
        );
        $this->load->view("administrator_area/product/Edit_technical",$data);
    }

    public function export_form()
    {
        $this->load->library('excel');

        $filename = "ProductForm".date("Y_m_d");
        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);
        $row = 1;
        $column = 1;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Item");
        
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "1");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Product Solution (EN) \r (Format : Solution Name Level1 - Solution Name Level2 - Solution Name Level3)");
        $col_txt = PHPExcel_Cell::stringFromColumnIndex($column-1);
        $object->getActiveSheet()->getStyle($col_txt.$row)->getAlignment()->setWrapText(true);
        $row+=20;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "2");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Product Category (EN) \r (Format : Category Name Level1 - Category Name Level2)");
        $col_txt = PHPExcel_Cell::stringFromColumnIndex($column-1);
        $object->getActiveSheet()->getStyle($col_txt.$row)->getAlignment()->setWrapText(true);        
        $row+=20;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "3");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Product Type(ประเภทสินค้า) (EN)");
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "4");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Product Name (EN)*");  
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "5");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Product Name (TH)*"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "6");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Code*"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "7");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Brand (EN)"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "8");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Model (EN)"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "9");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Model (TH)"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "10");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Short Detail (EN)"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "11");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Short Detail (TH)"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "12");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Description (EN)"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "13");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Description (TH)"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "14");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Basic Quality (EN)"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "15");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Basic Quality (TH)"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "16");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "ระยะเวลาที่รอสินค้า (กรณีไม่มีสินค้าในสต็อก)"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "17");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "ราคาสินค้าปลีก"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "18");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "จำนวนสินค้าขั้นต่ำ"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "19");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "ค่าส่ง/ชิ้น"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "20");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "ราคาสินค้าตามจำนวน \r (Format : จำนวนสินค้า:ราคา, จำนวนสินค้า:ราคา,...)");
        $col_txt = PHPExcel_Cell::stringFromColumnIndex($column-1);
        $object->getActiveSheet()->getStyle($col_txt.$row)->getAlignment()->setWrapText(true); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "21");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "เงื่อนไขสินค้า (EN)"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "22");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "เงื่อนไขสินค้า (TH)"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "23");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "เงื่อนไขการรับประกัน (EN)"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "24");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "เงื่อนไขการรับประกัน (TH)"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "25");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "In Stock Qty"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "26");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Safety Stock"); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "27");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "คำค้นหา \r (Format : Keyword1, Keyword2, ...)"); 
        $col_txt = PHPExcel_Cell::stringFromColumnIndex($column-1);
        $object->getActiveSheet()->getStyle($col_txt.$row)->getAlignment()->setWrapText(true); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "28");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Feature Title (TH) \r (Format : Title1 | Title2 | ...)"); 
        $col_txt = PHPExcel_Cell::stringFromColumnIndex($column-1);
        $object->getActiveSheet()->getStyle($col_txt.$row)->getAlignment()->setWrapText(true); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "29");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Feature Title (EN) \r (Format : Title1 | Title2 | ...)"); 
        $col_txt = PHPExcel_Cell::stringFromColumnIndex($column-1);
        $object->getActiveSheet()->getStyle($col_txt.$row)->getAlignment()->setWrapText(true); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "30");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Feature Description (TH) \r (Format : Description1 | Description2 | ...)"); 
        $col_txt = PHPExcel_Cell::stringFromColumnIndex($column-1);
        $object->getActiveSheet()->getStyle($col_txt.$row)->getAlignment()->setWrapText(true); 
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "31");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Feature Description (EN) \r (Format : Description1 | Description2 | ...)"); 
        $col_txt = PHPExcel_Cell::stringFromColumnIndex($column-1);
        $object->getActiveSheet()->getStyle($col_txt.$row)->getAlignment()->setWrapText(true);
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "32");
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Tutorial \r (Format : Title(TH) | Title(EN) | Link)"); 
        $col_txt = PHPExcel_Cell::stringFromColumnIndex($column-1);
        $object->getActiveSheet()->getStyle($col_txt.$row)->getAlignment()->setWrapText(true); 
        
        ob_end_clean(); 
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header("Content-Disposition: attachment; filename=\"$filename.xls\"");
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        $objWriter->save('php://output');
    }

    public function importproduct()
    {
        $this->load->model("product_model");
        $this->load->model("product_price_model");
        $this->load->model("brand_model");
        $this->load->model("menu_category_model");
        $this->load->model("submenu_category_model");
        $this->load->model("child_submenu_category_model");
        $this->load->model("product_type_model");
        $this->load->model("product_category_model");
        $this->load->model("product_sub_category_model");
        $this->load->model("product_childsub_category_model");
        $this->load->model("product_detail_menu_model");
        $this->load->model("product_detail_category_model");
        $this->load->model("product_feature_model");
        $this->load->model("product_tutorial_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");
        $this->load->helper(array('form', 'url'));
        
        libxml_use_internal_errors(true);
        $file = $_FILES['fileproduct']['tmp_name'];
        //load the excel library
        $this->load->library('excel');
        $Reader = PHPExcel_IOFactory::createReaderForFile($file);
        $Reader->setReadDataOnly(true);
        $objPHPExcel = $Reader->load($file);
        //get only the Cell Collection
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        //extract to a PHP readable array format
        $totalRow =  $objPHPExcel->getActiveSheet()->getHighestRow();
        $totalColumn_str = $objPHPExcel->getActiveSheet()->getHighestColumn();
        $totalColumn = PHPExcel_Cell::columnIndexFromString($totalColumn_str);
        $i =0;
        $products = array();
        if ($totalColumn > 2) {
            $rcode = 2;
            $rcategory = 2;
            $rendcategory = 2;
            while ($objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0, $rcode) != "6" && $rcode<=$totalRow) {
                if($objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0, $rcode) == "2"){
                    $rcategory = $rcode;
                }
                if($objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0, $rcode) == "3"){
                    $rendcategory = $rcode;
                }
                $rcode++;
            }
            for ($c=2; $c < $totalColumn; $c++) { 
                $product = new stdClass();

                if (!empty(trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $rcode)))) {
                    $product->menu_categorys = array();
                    $r = 2;
                    while ($r< $rcategory) {
                        if (!empty(trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r)))) {
                            $product->menu_categorys[] = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r));
                        }
                        $r++;
                    }

                    
                    $product->product_categorys = array();
                    while ($r< $rendcategory) {
                        if (!empty(trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r)))) {
                            $product->product_categorys[] = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r));
                        }
                        $r++;
                    }

                    $product->product_type = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->name_en = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->name_th = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->code = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->brand_name_en = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->model_name_en = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->model_name_th = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->detail_en = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->detail_th = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->description_en = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->description_th = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->basic_quality_en = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->basic_quality_th = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->waiting_date = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->retail_price = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->minimum_product = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->delivery_price = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->product_prices = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->condition_product_en = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->condition_product_th = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->condition_warranty_en = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->condition_warranty_th = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->in_stock_qty = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->safety_stock = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->search_keyword = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->feature_title_th = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->feature_title_en = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->feature_description_th= trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->feature_description_en= trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r++));
                    $product->product_tutorials = array();
                    while ($r<= $totalRow) {
                        if (!empty(trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r)))) {
                            $product->product_tutorials[] = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r));
                        }
                        $r++;
                    }

                    $products[] = $product;
                }   
            }
        }

        $result = new stdClass();
        $result->correct = 0;
        $result->duplicate = 0;
        $result->not_name_product = 0;
        $result->not_found_brand = 0;
        $result->not_found_product_type = 0;
        $result->total = count($products);
        if ($products!= null && count($products) > 0) {
            foreach ($products as $item) {
                if (empty($item->name_th) || empty($item->name_en)) {
                    $result->not_name_product++;
                }
                else{
                    if (!file_exists($this->product_path.$item->code.'/')) {

                        $oldmask = umask(0);
                        mkdir($this->product_path.$item->code.'/', 0777, true);
                        umask($oldmask);
                    } 
                    if (!file_exists($this->product_feature_path.$item->code.'/')) {

                        $oldmask = umask(0);
                        mkdir($this->product_feature_path.$item->code.'/', 0777, true);
                        umask($oldmask);
                    } 
                    //Dup Data
                    $dup_filter = new stdClass();
                    $dup_filter->where["code"] = $item->code;
                    $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
                    $dup_filter->get_first = true;
                    $dup_data = $this->product_model->search($dup_filter);
                    
                    //Brand
                    $brand_id = null;
                    if (!empty($item->brand_name_en)) {
                        $model_filter = new stdClass();
                        $model_filter->where["LOWER(name_en)"] = strtolower($item->brand_name_en);
                        $model_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
                        $model_filter->get_first = true;
                        $brand = $this->brand_model->search($model_filter);
                        if ($brand != null) {
                            $brand_id = $brand->id;
                        }
                        else{
                            $brand = new stdClass();
                            $brand->name_en = $item->brand_name_en;
                            $brand->name_th = $item->brand_name_en;
                            $brand->status = "ACTIVATE";
                            $brand->created_date = date("Y-m-d h:i:s");
                            $brand->created_by = $this->session->userdata("__administrator::id");
                            $brand_id = $this->brand_model->save($brand);
                        }
                    }

                    //Product Type
                    $product_type_id = null;
                    if (!empty($item->product_type)) {
                        $model_filter = new stdClass();
                        $model_filter->where["LOWER(name_en)"] = strtolower($item->product_type);
                        $model_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
                        $model_filter->get_first = true;
                        $product_type = $this->product_type_model->search($model_filter);
                        if ($product_type != null) {
                            $product_type_id = $product_type->id;
                        }
                        else{
                            $product_type = new stdClass();
                            $product_type->name_en = $item->product_type;
                            $product_type->name_th = $item->product_type;
                            $product_type->status = "ACTIVATE";
                            $product_type->created_date = date("Y-m-d h:i:s");
                            $product_type->created_by = $this->session->userdata("__administrator::id");
                            $product_type_id = $this->product_type_model->save($brand);
                        }
                    }


                    if ($dup_data == null) {
                        $product = new stdClass();
                        $product->created_date = date("Y-m-d h:i:s");
                        $product->created_by = $this->session->userdata("__administrator::id");
                    }
                    else{
                        $product = $this->product_model->find($dup_data->id);
                        $product->modified_date = date("Y-m-d h:i:s");
                        $product->modified_by = $this->session->userdata("__administrator::id");
                    }
                    
                    $product->code = $item->code;
                    $product->brand_id = $brand_id;
                    $product->name_th = $item->name_th;
                    $product->name_en = $item->name_en;
                    $product->model_name_th = $item->model_name_th;
                    $product->model_name_en = $item->model_name_en;
                    $product->product_type_id = $product_type_id;
                    $product->detail_th = $item->detail_th;
                    $product->detail_en = $item->detail_en;
                    $product->description_th = $item->description_th;
                    $product->description_en = $item->description_en;
                    $product->basic_quality_th = $item->basic_quality_th;
                    $product->basic_quality_en = $item->basic_quality_en;
                    $product->retail_price = $item->retail_price == '' ? null : $item->retail_price;
                    $product->waiting_date = $item->waiting_date == '' ? null : $item->waiting_date;
                    $product->minimum_product = $item->minimum_product == '' ? null : $item->minimum_product;
                    $product->condition_product_th = $item->condition_product_th;
                    $product->condition_product_en = $item->condition_product_en;
                    $product->condition_warranty_th = $item->condition_warranty_th;
                    $product->condition_warranty_en = $item->condition_warranty_en;
                    $product->in_stock_qty = $item->in_stock_qty == '' ? null : $item->in_stock_qty;
                    $product->safety_stock = $item->safety_stock == '' ? null : $item->safety_stock;
                    $product->delivery_price = $item->delivery_price == '' ? null : $item->delivery_price;
                    $product->search_keyword = $item->search_keyword;
                    $product->status = 'ACTIVATE';
                    
                    $product_id = $this->product_model->save($product); 

                    $update_filter = new stdClass();
                    $update_filter->where["product_id"] = $product_id;
                    $update_filter->where["status"] = "ACTIVATE";

                    $update_data = array(
                        "status" => "REMOVED"
                    );
                    $this->product_detail_menu_model->update($update_filter, $update_data);
                    $this->product_detail_category_model->update($update_filter, $update_data);
                    $this->product_feature_model->update($update_filter, $update_data);
                    $this->product_price_model->update($update_filter, $update_data);
                    $this->product_tutorial_model->update($update_filter, $update_data);

                    if ($item->menu_categorys != null && count($item->menu_categorys) > 0) {
                        foreach ($item->menu_categorys as $category) {
                            $categorys = explode('-', $category);
                            $menu_category_id = null;
                            $submenu_category_id = null;
                            $child_submenu_category_id = null;
                            if (count($categorys) == 1) {
                                $model_filter = new stdClass();
                                $model_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
                                $model_filter->where["LOWER(name_en)"] = strtolower(trim($categorys[0]));
                                $model_filter->get_first = true;
                                $menu_category = $this->menu_category_model->search($model_filter);
                                if($menu_category != null)
                                    $menu_category_id = $menu_category->id;
                            }
                            else if(count($categorys) == 2){
                                $model_filter = new stdClass();
                                $model_filter->where_in["menu_category.status"] = array("ACTIVATE", "SUSPEND");
                                $model_filter->where_in["submenu_category.status"] = array("ACTIVATE", "SUSPEND");
                                $model_filter->where["LOWER(menu_category.name_en)"] = strtolower(trim($categorys[0]));
                                $model_filter->where["LOWER(submenu_category.name_en)"] = strtolower(trim($categorys[1]));
                                $model_filter->join = array(
                                    "menu_category" => "submenu_category.menu_category_id = menu_category.id"
                                );
                                $model_filter->get_first = true;
                                $submenu_category = $this->submenu_category_model->search($model_filter);
                                if($submenu_category != null){
                                    $menu_category_id = $submenu_category->{"submenu_category::menu_category_id"};
                                    $submenu_category_id = $submenu_category->{"submenu_category::id"};
                                }
                            }
                            else if(count($categorys) == 3){
                                $model_filter = new stdClass();
                                $model_filter->where_in["menu_category.status"] = array("ACTIVATE", "SUSPEND");
                                $model_filter->where_in["submenu_category.status"] = array("ACTIVATE", "SUSPEND");
                                $model_filter->where_in["child_submenu_category.status"] = array("ACTIVATE", "SUSPEND");
                                $model_filter->where["LOWER(menu_category.name_en)"] = strtolower(trim($categorys[0]));
                                $model_filter->where["LOWER(submenu_category.name_en)"] = strtolower(trim($categorys[1]));
                                $model_filter->where["LOWER(child_submenu_category.name_en)"] = strtolower(trim($categorys[2]));
                                $model_filter->join = array(
                                    "submenu_category" => "child_submenu_category.submenu_category_id = submenu_category.id",
                                    "menu_category" => "submenu_category.menu_category_id = menu_category.id"
                                );
                                $model_filter->get_first = true;
                                $child_submenu_category = $this->child_submenu_category_model->search($model_filter);
                                if($child_submenu_category != null && isset($child_submenu_category->{"child_submenu_category::id"})){
                                    $menu_category_id = $child_submenu_category->{"menu_category::id"};
                                    $submenu_category_id = $child_submenu_category->{"submenu_category::id"};
                                    $child_submenu_category_id = $child_submenu_category->{"child_submenu_category::id"};
                                }
                            }

                            if ($menu_category_id != null) {
                                $product_detail_menu = new stdClass();
                                $product_detail_menu->product_id = $product_id;
                                $product_detail_menu->menu_category_id = $menu_category_id;
                                $product_detail_menu->submenu_category_id= $submenu_category_id;
                                $product_detail_menu->child_submenu_category_id = $child_submenu_category_id;
                                $product_detail_menu->status = 'ACTIVATE';
                                $product_detail_menu->created_date = date("Y-m-d h:i:s");
                                $product_detail_menu->created_by = $this->session->userdata("__administrator::id");
                                $this->product_detail_menu_model->save($product_detail_menu);
                            }
                        }
                    }

                    if ($item->product_categorys != null && count($item->product_categorys) > 0) {
                        foreach ($item->product_categorys as $category) {
                            $categorys = explode('-', $category);
                            $product_category_id = null;
                            $product_sub_category_id = null;
                            $product_childsub_category_id = null;
                            if (count($categorys) == 1) {
                                $model_filter = new stdClass();
                                $model_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
                                $model_filter->where["LOWER(name_en)"] = strtolower(trim($categorys[0]));
                                $model_filter->get_first = true;
                                $product_category = $this->product_category_model->search($model_filter);
                                if($product_category != null)
                                    $product_category_id = $product_category->id;
                            }
                            else if(count($categorys) == 2){
                                $model_filter = new stdClass();
                                $model_filter->where_in["product_category.status"] = array("ACTIVATE", "SUSPEND");
                                $model_filter->where_in["product_sub_category.status"] = array("ACTIVATE", "SUSPEND");
                                $model_filter->where["LOWER(product_category.name_en)"] = strtolower(trim($categorys[0]));
                                $model_filter->where["LOWER(product_sub_category.name_en)"] = strtolower(trim($categorys[1]));
                                $model_filter->join = array(
                                    "product_category" => "product_sub_category.product_category_id = product_category.id"
                                );
                                $model_filter->get_first = true;
                                $product_sub_category = $this->product_sub_category_model->search($model_filter);
                                if($product_sub_category != null){
                                    $product_category_id = $product_sub_category->{"product_sub_category::product_category_id"};
                                    $product_sub_category_id = $product_sub_category->{"product_sub_category::id"};
                                }
                            }
                            

                            if ($product_category_id != null) {
                                $product_detail_category = new stdClass();
                                $product_detail_category->product_id = $product_id;
                                $product_detail_category->product_category_id = $product_category_id;
                                $product_detail_category->product_sub_category_id= $product_sub_category_id;
                                $product_detail_category->status = 'ACTIVATE';
                                $product_detail_category->created_date = date("Y-m-d h:i:s");
                                $product_detail_category->created_by = $this->session->userdata("__administrator::id");

                                $this->product_detail_category_model->save($product_detail_category);
                            }
                        }
                    }

                    if (!empty($item->product_prices)) {
                        $product_prices = explode(',', $item->product_prices);
                        foreach ($product_prices as $price) {
                            if (!empty(trim($price))) {
                                $categorys = explode(':', trim($price));
                                $product_price = new stdClass();
                                $product_price->product_id = $product_id;
                                $product_price->minimum_qty = trim($categorys[0]);
                                $product_price->price = trim($categorys[1]);
                                $product_price->status = 'ACTIVATE';
                                $product_price->created_date = date("Y-m-d h:i:s");
                                $product_price->created_by = $this->session->userdata("__administrator::id");
                                $this->product_price_model->save($product_price);
                            }
                        }
                    }

                    if (!empty($item->feature_title_th)) {
                        $feature_title_th = explode('|', $item->feature_title_th);
                        $feature_title_en = explode('|', $item->feature_title_en);
                        $feature_description_th = explode('|', $item->feature_description_th);
                        $feature_description_en = explode('|', $item->feature_description_en);
                        for ($j=0; $j < count($feature_title_th); $j++) { 
                            if (!empty(trim($feature_title_th[$j]))) {
                                $product_feature = new stdClass();
                                $product_feature->product_id = $product_id;
                                $product_feature->title_th = trim($feature_title_th[$j]);
                                $product_feature->title_en = isset($feature_title_en[$j]) ? trim($feature_title_en[$j]) : '';
                                $product_feature->description_th = isset($feature_description_th[$j]) ? trim($feature_description_th[$j]) : '';
                                $product_feature->description_en = isset($feature_description_en[$j]) ? trim($feature_description_en[$j]) : '';
                                $product_feature->sort = $j+1;
                                $product_feature->image_path = '/assets/uploads/product_feature/'.$code.'/'.($j+1).".png";
                                $product_feature->status = 'ACTIVATE';
                                $product_feature->created_date = date("Y-m-d h:i:s");
                                $product_feature->created_by = $this->session->userdata("__administrator::id");
                                $this->product_price_model->save($product_price);
                            }
                        }
                    }

                    if ($item->product_tutorials != null && count($item->product_tutorials) > 0) {
                        $sort=1;
                        foreach ($item->product_tutorials as $product_tutorial) {
                            $tutorials = explode('|', $product_tutorial);
                            if (count($tutorials) == 3 && !empty(trim($tutorials[0])) && !empty(trim($tutorials[1])) && !empty(trim($tutorials[2]))) {
                                $pt = new stdClass();
                                $pt->product_id = $product_id;
                                $pt->title_th = trim($tutorials[0]);
                                $pt->title_en = trim($tutorials[1]);
                                $pt->link_url = trim($tutorials[2]);
                                $pt->sort = $sort;
                                $pt->status = 'ACTIVATE';
                                $pt->created_date = date("Y-m-d h:i:s");
                                $pt->created_by = $this->session->userdata("__administrator::id");
                                $this->product_tutorial_model->save($pt);
                                $sort++;
                            }    
                        }
                    }

                    $result->correct++;
                }
            }
        }

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.", $result);
    }

    public function importtechnical()
    {
        $this->load->model("product_model");
        $this->load->model("product_technical_spec_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");
        $this->load->helper(array('form', 'url'));
        
        libxml_use_internal_errors(true);
        $file = $_FILES['filetechnical']['tmp_name'];
        //load the excel library
        $this->load->library('excel');
        $Reader = PHPExcel_IOFactory::createReaderForFile($file);
        $Reader->setReadDataOnly(true);
        $objPHPExcel = $Reader->load($file);
        //get only the Cell Collection
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        //extract to a PHP readable array format
        $totalRow =  $objPHPExcel->getActiveSheet()->getHighestRow();
        $totalColumn_str = $objPHPExcel->getActiveSheet()->getHighestColumn();
        $totalColumn = PHPExcel_Cell::columnIndexFromString($totalColumn_str);
        $result = new stdClass();
        $result->correct = 0;
        $result->fail = 0;
        $i =0;
        if ($totalColumn > 2 && $totalRow > 2) {
            for ($c=2; $c < $totalColumn; $c++) { 
                if (!empty(trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, 1)))) {
                    $product = $this->product_model->find(trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, 1)));
                    if ($product != null && $product->status != "REMOVED") {
                        for ($r=3; $r <$totalRow ; $r++) { 
                            $technical_spec_id = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0, $r));
                            if (!empty($technical_spec_id)) {
                                $valuestr = trim($objPHPExcel->getActiveSheet()->getCellByColumnAndRow($c, $r));
                                if (!empty($valuestr)) {
                                    $values = explode("|", $valuestr);
                                    $model_filter = new stdClass();
                                    $model_filter->where["product_id"] = $product->id;
                                    $model_filter->where["technical_spec_id"] = $technical_spec_id;
                                    $model_filter->where["status"] = "ACTIVATE";
                                    $model_filter->get_first = true;
                                    $product_technical_spec = $this->product_technical_spec_model->search($model_filter);
                                    if($product_technical_spec ==null){
                                        $product_technical_spec = new stdClass();
                                        $product_technical_spec->status = 'ACTIVATE';
                                        $product_technical_spec->created_date = date("Y-m-d h:i:s");
                                        $product_technical_spec->created_by = $this->session->userdata("__administrator::id");
                                    }
                                    else{
                                        $product_technical_spec->modified_date = date("Y-m-d h:i:s");
                                        $product_technical_spec->modified_by = $this->session->userdata("__administrator::id");
                                    }

                                    
                                    $product_technical_spec->product_id = $product->id;
                                    $product_technical_spec->technical_spec_id = $technical_spec_id;
                                    $product_technical_spec->value_th = empty(trim($values[0])) ? '' :trim($values[0]);
                                    $product_technical_spec->value_en = (count($values)<2 || empty(trim($values[1]))) ? trim($values[0]) :trim($values[1]);
                                    $this->product_technical_spec_model->save($product_technical_spec);
                                    $result->correct++;
                                }
                            }
                        }
                    }
                    else{
                     $result->fail++; 
                 }
             }
         }
     }


     $this->response_library->responseJSON("0x0000-00000", "Process request Complete.", $result);
 }

    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "product";
            $default_filter = "product.status != 'REMOVED'";
            $request_filter = array(
                "product.id"            => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "product.code"          => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "code", "type" => "TEXT"),
                "brand.name_en"         => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "brand_name_en", "type" => "TEXT"),
                "product.name_en"       => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "name_en", "type" => "TEXT"),
                "product.model_name_en" => array("method" => "LIKE", "value" => $datatables_request["columns"]["3"]["search"]["value"]  , "alias" => "model_name_en", "type" => "TEXT"),
                "product_type.name_en"  => array("method" => "LIKE", "value" => $datatables_request["columns"]["4"]["search"]["value"]  , "alias" => "product_type_name_en", "type" => "TEXT")
            );
            $join = array(
                "brand" => "product.brand_id = brand.id",
                "product_type" => "product.product_type_id = product_type.id"
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
        else if($function == "index_product_price")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "product_price";
            $default_filter = "product_price.status like 'ACTIVATE'";            
            $default_filter .= " AND product_price.product_id = ".$this->input->post("product_id");
            $request_filter = array(
                "product_price.id"              => array("method" => "LIKE", "value" => null, "alias" => "id"  , "type" => "TEXT"),
                "product_price.product_id"      => array("method" => "LIKE", "value" => null, "alias" => "product_id"  , "type" => "TEXT"),
                "product_price.minimum_qty" => array("method" => "LIKE", "value" => null  , "alias" => "minimum_qty", "type" => "TEXT"),
                "product_price.price"           => array("method" => "LIKE", "value" => null  , "alias" => "price", "type" => "TEXT")
            );
            $join = array(
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
        else if($function == "index_product_category")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "product_detail_category";
            $default_filter = "product_detail_category.status like 'ACTIVATE'";   
            $default_filter .= " AND (product_detail_category.product_category_id is null OR product_category.status like 'ACTIVATE') ";   
            $default_filter .= " AND (product_detail_category.product_sub_category_id is null OR product_sub_category.status like 'ACTIVATE')";             
            $default_filter .= " AND product_detail_category.product_id = ".$this->input->post("product_id");
            $request_filter = array(
                "product_detail_category.id"              => array("method" => "LIKE", "value" => null, "alias" => "id"  , "type" => "TEXT"),
                "product_detail_category.product_id"      => array("method" => "LIKE", "value" => null, "alias" => "product_id"  , "type" => "TEXT"),
                "product_detail_category.product_category_id" => array("method" => "LIKE", "value" => null  , "alias" => "product_category_id", "type" => "TEXT"),
                "product_category.name_en"           => array("method" => "LIKE", "value" => null  , "alias" => "product_category_name", "type" => "TEXT"),
                "product_detail_category.product_sub_category_id" => array("method" => "LIKE", "value" => null  , "alias" => "product_sub_category_id", "type" => "TEXT"),
                "product_sub_category.name_en"           => array("method" => "LIKE", "value" => null  , "alias" => "product_sub_category_name", "type" => "TEXT")
            );
            $join = array(
                "product_category" => "product_detail_category.product_category_id = product_category.id",
                "product_sub_category" => "product_detail_category.product_sub_category_id = product_sub_category.id"
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
        else if($function == "index_menu_category")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "product_detail_menu";
            $default_filter = "product_detail_menu.status like 'ACTIVATE'";   
            $default_filter .= " AND (product_detail_menu.menu_category_id is null OR menu_category.status like 'ACTIVATE') ";   
            $default_filter .= " AND (product_detail_menu.submenu_category_id is null OR submenu_category.status like 'ACTIVATE')";    
            $default_filter .= " AND (product_detail_menu.child_submenu_category_id is null OR child_submenu_category.status like 'ACTIVATE')";            
            $default_filter .= " AND product_detail_menu.product_id = ".$this->input->post("product_id");
            $request_filter = array(
                "product_detail_menu.id"              => array("method" => "LIKE", "value" => null, "alias" => "id"  , "type" => "TEXT"),
                "product_detail_menu.product_id"      => array("method" => "LIKE", "value" => null, "alias" => "product_id"  , "type" => "TEXT"),
                "product_detail_menu.menu_category_id" => array("method" => "LIKE", "value" => null  , "alias" => "menu_category_id", "type" => "TEXT"),
                "menu_category.name_en"           => array("method" => "LIKE", "value" => null  , "alias" => "menu_category_name", "type" => "TEXT"),
                "product_detail_menu.submenu_category_id" => array("method" => "LIKE", "value" => null  , "alias" => "submenu_category_id", "type" => "TEXT"),
                "submenu_category.name_en"           => array("method" => "LIKE", "value" => null  , "alias" => "submenu_category_name", "type" => "TEXT"),
                "product_detail_menu.child_submenu_category_id" => array("method" => "LIKE", "value" => null  , "alias" => "child_submenu_category_id", "type" => "TEXT"),
                "child_submenu_category.name_en"           => array("method" => "LIKE", "value" => null  , "alias" => "child_submenu_category_name", "type" => "TEXT")
            );
            $join = array(
                "menu_category" => "product_detail_menu.menu_category_id = menu_category.id",
                "submenu_category" => "product_detail_menu.submenu_category_id = submenu_category.id",
                "child_submenu_category" => "product_detail_menu.child_submenu_category_id = child_submenu_category.id"
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
        else if($function == "index_product_relate")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "product_relate";
            $default_filter = "product_relate.status like 'ACTIVATE'";   
            $default_filter .= " AND product.status like 'ACTIVATE'";             
            $default_filter .= " AND product_relate.main_product_id = ".$this->input->post("product_id");
            $request_filter = array(
                "product_relate.id"                 => array("method" => "LIKE", "value" => null, "alias" => "id"  , "type" => "TEXT"),
                "product_relate.main_product_id"    => array("method" => "LIKE", "value" => null, "alias" => "main_product_id"  , "type" => "TEXT"),
                "product_relate.relate_product_id"  => array("method" => "LIKE", "value" => null, "alias" => "relate_product_id"  , "type" => "TEXT"),
                "product.name_en"                   => array("method" => "LIKE", "value" => null  , "alias" => "product_name", "type" => "TEXT"),
                "product.model_name_en"             => array("method" => "LIKE", "value" => null  , "alias" => "product_model_name", "type" => "TEXT"),
                "product.code"                      => array("method" => "LIKE", "value" => null  , "alias" => "product_code", "type" => "TEXT")
            );
            $join = array(
                "product" => "product_relate.relate_product_id = product.id"
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
        else if($function == "index_product_feature")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "product_feature";
            $default_filter = "product_feature.status like 'ACTIVATE'";            
            $default_filter .= " AND product_feature.product_id = ".$this->input->post("product_id");
            $request_filter = array(
                "product_feature.id"              => array("method" => "LIKE", "value" => null, "alias" => "id"  , "type" => "TEXT"),
                "product_feature.product_id"      => array("method" => "LIKE", "value" => null, "alias" => "product_id"  , "type" => "TEXT"),
                "product_feature.sort"            => array("method" => "LIKE", "value" => null  , "alias" => "sort", "type" => "TEXT"),
                "product_feature.title_th"        => array("method" => "LIKE", "value" => null  , "alias" => "title_th", "type" => "TEXT"),
                "product_feature.title_en"        => array("method" => "LIKE", "value" => null  , "alias" => "title_en", "type" => "TEXT"),
                "product_feature.description_th"        => array("method" => "LIKE", "value" => null  , "alias" => "description_th", "type" => "TEXT"),
                "product_feature.description_en"        => array("method" => "LIKE", "value" => null  , "alias" => "description_en", "type" => "TEXT"),
                "product_feature.image_path"        => array("method" => "LIKE", "value" => null  , "alias" => "image_path", "type" => "TEXT")
            );
            $join = array(
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
        else if($function == "index_product_tutorial")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "product_tutorial";
            $default_filter = "product_tutorial.status like 'ACTIVATE'";            
            $default_filter .= " AND product_tutorial.product_id = ".$this->input->post("product_id");
            $request_filter = array(
                "product_tutorial.id"              => array("method" => "LIKE", "value" => null, "alias" => "id"  , "type" => "TEXT"),
                "product_tutorial.product_id"      => array("method" => "LIKE", "value" => null, "alias" => "product_id"  , "type" => "TEXT"),
                "product_tutorial.sort"            => array("method" => "LIKE", "value" => null  , "alias" => "sort", "type" => "TEXT"),
                "product_tutorial.title_th"        => array("method" => "LIKE", "value" => null  , "alias" => "title_th", "type" => "TEXT"),
                "product_tutorial.title_en"        => array("method" => "LIKE", "value" => null  , "alias" => "title_en", "type" => "TEXT"),
                "product_tutorial.link_url"        => array("method" => "LIKE", "value" => null  , "alias" => "link_url", "type" => "TEXT")
            );
            $join = array(
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
        else if($function == "index_product_datasheet")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "product_datasheet";
            $default_filter = "product_datasheet.status like 'ACTIVATE'";            
            $default_filter .= " AND product_datasheet.product_id = ".$this->input->post("product_id");
            $request_filter = array(
                "product_datasheet.id"              => array("method" => "LIKE", "value" => null, "alias" => "id"  , "type" => "TEXT"),
                "product_datasheet.product_id"      => array("method" => "LIKE", "value" => null, "alias" => "product_id"  , "type" => "TEXT"),
                "product_datasheet.sort"            => array("method" => "LIKE", "value" => null  , "alias" => "sort", "type" => "TEXT"),
                "product_datasheet.name"        => array("method" => "LIKE", "value" => null  , "alias" => "name", "type" => "TEXT"),
                "product_datasheet.file_path"        => array("method" => "LIKE", "value" => null  , "alias" => "file_path", "type" => "TEXT")
            );
            $join = array(
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }

    }

    public function select2($function)
    {
        $this->load->library("response_library");

        if($function == "edit_brand")
        {
            $this->load->model("brand_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_en"] = $this->input->get("search");
            }
            $brands = $this->brand_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $brands);
        }
        else if($function == "edit_product_type")
        {
            $this->load->model("product_type_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_en"] = $this->input->get("search");
            }
            $product_types = $this->product_type_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $product_types);
        }
        else if($function == "edit_product_category")
        {
            $this->load->model("product_category_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_en"] = $this->input->get("search");
            }
            $product_categorys = $this->product_category_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $product_categorys);
        }
        else if($function == "edit_product_sub_category")
        {
            $this->load->model("product_sub_category_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            $model_filter->where["product_category_id"] = $this->input->get("product_category_id");
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_en"] = $this->input->get("search");
            }
            $product_sub_categorys = $this->product_sub_category_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $product_sub_categorys);
        }
        else if($function == "edit_product_childsub_category")
        {
            $this->load->model("product_childsub_category_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            $model_filter->where["product_sub_category_id"] = $this->input->get("product_sub_category_id");
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_en"] = $this->input->get("search");
            }
            $product_childsub_categorys = $this->product_childsub_category_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $product_childsub_categorys);
        }
        else if($function == "edit_menu_category")
        {
            $this->load->model("menu_category_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_en"] = $this->input->get("search");
            }
            $menu_categorys = $this->menu_category_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $menu_categorys);
        }
        else if($function == "edit_submenu_category")
        {
            $this->load->model("submenu_category_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            $model_filter->where["menu_category_id"] = $this->input->get("menu_category_id");
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_en"] = $this->input->get("search");
            }
            $submenu_categorys = $this->submenu_category_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $submenu_categorys);
        }
        else if($function == "edit_child_submenu_category")
        {
            $this->load->model("child_submenu_category_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            $model_filter->where["submenu_category_id"] = $this->input->get("submenu_category_id");
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_en"] = $this->input->get("search");
            }
            $child_submenu_categorys = $this->child_submenu_category_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $child_submenu_categorys);
        }
        else if($function == "edit_product")
        {
            $this->load->model("product_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            $model_filter->where["id !="] = $this->input->get("product_id");
            if($this->input->get("search") != "")
            {

                $model_filter->like["CONCAT(name_en, code)"] = $this->input->get("search");
            }
            $products = $this->product_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $products);
        }
    }
}