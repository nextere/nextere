<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator_group extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
	public function index()
	{
        $this->load->view("administrator_area/administrator_group/Index");
	}

	public function edit($administrator_group_id = null)
	{
		/** ------------------------------------------------------- **
		 **           CONSTRUCT PHASE
		/** ------------------------------------------------------- **/
		$this->load->model("administrator_group_model");
		$this->load->model("administrator_group_permission_model");
		$this->load->model("administrator_group_permission_key_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

		/** ------------------------------------------------------- **
		 **           PROCESS PHASE
		/** ------------------------------------------------------- **/

        $administrator_group = $this->administrator_group_model->find($administrator_group_id);
        $administrator_group_permissions = $this->administrator_group_permission_model->getAdministratorGroupPermissionsByAdministratorGroupId($administrator_group_id);

        if($this->input->post())
        {
            if($administrator_group_id != null)
            {
                $administrator_group = $this->administrator_group_model->find($administrator_group_id);
                $administrator_group->modified_date = date("Y-m-d h:i:s");
                $administrator_group->modified_by = $this->session->userdata("__administrator::id");
            }
            else
            {
                $administrator_group = new stdClass();
                $administrator_group->created_date = date("Y-m-d h:i:s");
                $administrator_group->created_by = $this->session->userdata("__administrator::id");
            }

            $administrator_group->name = $this->input->post("name");
            $administrator_group->status = $this->input->post("status");

            $administrator_group_id = $this->administrator_group_model->save($administrator_group);

            // PERMISSION
            $model_filter = new stdClass();
            $model_filter->where["administrator_group_id"] = $administrator_group_id;
            $this->administrator_group_permission_model->delete($model_filter);
            $request_administrator_group_permissions = $this->input->post("administrator_group_permission");
            if($request_administrator_group_permissions != null && count($request_administrator_group_permissions) > 0)
            {
                foreach($request_administrator_group_permissions as $request_administrator_group_permission_key => $request_administrator_group_permission)
                {
                    $this->administrator_group_permission_model->saveAdministratorGroupPermission($administrator_group_id, $request_administrator_group_permission_key, $request_administrator_group_permission);
                }
            }

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

		/** ------------------------------------------------------- **
		 **           RENDER VIEW PHASE
		/** ------------------------------------------------------- **/
        $data = array(
            "administrator_group" => $administrator_group,
            "administrator_group_permissions" => $administrator_group_permissions
        );
        $this->load->view("administrator_area/administrator_group/Edit",$data);
	}

	public function delete($administrator_id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("administrator_group_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $administrator_group = $this->administrator_group_model->find($administrator_id);
        $administrator_group->modified_date = date("Y-m-d h:i:s");
        $administrator_group->modified_by = $this->session->userdata("__administrator::id");
        $administrator_group->status = "REMOVED";

        $this->administrator_group_model->save($administrator_group);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "administrator_group";
            $default_filter = "administrator_group.status IN ('ACTIVATE','SUSPEND') AND administrator_group.id != '1'";
            $request_filter = array(
                "administrator_group.id"  => array("method" => "LIKE", "value" => null, "alias" => "administrator_group_id"  , "type" => "TEXT"),
                "administrator_group.name"                    => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "name", "type" => "TEXT"),
                "administrator_group.status"                  => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "status", "type" => "TEXT"),
                "administrator_group.created_date"            => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "created_date", "type" => "FULL_DATE"),
                "administrator_group.modified_date"           => array("method" => "LIKE", "value" => $datatables_request["columns"]["3"]["search"]["value"]  , "alias" => "modified_date", "type" => "FULL_DATE")
            );
            $join = array( );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }
}