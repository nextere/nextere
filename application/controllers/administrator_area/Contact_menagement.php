<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_management extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/contact_management/Index");
    }

    public function detail($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("contact_form_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $model_filter = new stdClass();
        $model_filter->where["contact_form.id"] = $id;
        $model_filter->join = array(
            "contact_title" => "contact_form.contact_title_id = contact_title.id"
        );
        $contact_form = $this->contact_form_model->search($model_filter);
       
        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "contact_title" => $contact_title
        );
        $this->load->view("administrator_area/contact_management/Detail",$data);
    }

   



    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "contact_form";
            $default_filter = "contact_form.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "contact_form.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "contact_form.contact_title_id"          => array("method" => "LIKE", "value" => null, "alias" => "contact_title_id" , "type" => "TEXT"),
                "contact_title.name_en"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "contact_title_name", "type" => "TEXT"),
                "contact_form.fullname"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "fullname", "type" => "TEXT"),
                "contact_form.email"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "email", "type" => "TEXT"),
                "contact_form.telephone"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["3"]["search"]["value"]  , "alias" => "telephone", "type" => "TEXT"),
                "contact_form.message"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["4"]["search"]["value"]  , "alias" => "message", "type" => "TEXT"),
                "contact_title.created_date"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["5"]["search"]["value"]  , "alias" => "created_date", "type" => "SHORT_MONTH_DATETIME"),
            );
            $join = array(
                "contact_title" => "contact_form.contact_title_id = contact_title.id"
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        if($function == "edit_contact_title")
        {
            $this->load->model("contact_title_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_en"] = $this->input->get("search");
            }
            $contact_titles = $this->contact_title_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $contact_titles);
        }
    }
}