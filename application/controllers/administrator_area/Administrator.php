<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
	public function index()
	{
        $this->load->view("administrator_area/administrator/Index");
	}

	public function edit($administrator_id = null)
	{
		/** ------------------------------------------------------- **
		 **           CONSTRUCT PHASE
		/** ------------------------------------------------------- **/
		$this->load->model("administrator_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

		/** ------------------------------------------------------- **
		 **           PROCESS PHASE
		/** ------------------------------------------------------- **/
        $administrator = $this->administrator_model->getAdministrator($administrator_id);
        if($this->input->post())
        {
            $username = $this->input->post("username");
            $administrator_group_id = $this->input->post("administrator_group_id");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["username"] = $this->input->post("username");
            if($administrator_id != null)
                $dup_filter->where["id != "] = $administrator_id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->administrator_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "มีการใช้ชื่อ username นี้แล้ว");
            }


            if($administrator_id != null)
            {
                $administrator = $this->administrator_model->find($administrator_id);
                $administrator->modified_date = date("Y-m-d h:i:s");
                $administrator->modified_by = $this->session->userdata("__administrator::id");
            }
            else
            {
              $administrator = new stdClass();
                $administrator->authorization_key = 'Basic '.base64_encode($username."Vs".$administrator_group_id.":");
                $administrator->created_date = date("Y-m-d h:i:s");
                $administrator->created_by = $this->session->userdata("__administrator::id");
            }

            $administrator->username = $this->input->post("username");
            $administrator->email = $this->input->post("email");
            $administrator->first_name = $this->input->post("first_name");
            $administrator->last_name = $this->input->post("last_name");
            $administrator->telephone = $this->input->post("telephone");
            $administrator->administrator_group_id = $this->input->post("administrator_group_id");
            $administrator->status = $this->input->post("status");
            
            if($this->input->post("password") != "" && ($this->input->post("password") == $this->input->post("retype_password")))
            {
                $administrator->password = $this->encryption_library->encryptPassword($this->input->post("password"));
            }

            $administrator_id = $this->administrator_model->save($administrator);

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

		/** ------------------------------------------------------- **
		 **           RENDER VIEW PHASE
		/** ------------------------------------------------------- **/
        $data = array(
            "administrator" => $administrator
        );
        $this->load->view("administrator_area/administrator/Edit",$data);
	}

	public function delete($administrator_id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("administrator_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $administrator = $this->administrator_model->find($administrator_id);
        $administrator->modified_date = date("Y-m-d h:i:s");
        $administrator->modified_by = $this->session->userdata("__administrator::administrator_id");
        $administrator->status = "REMOVED";

        $this->administrator_model->save($administrator);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
	public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "administrator";
            $default_filter = "administrator.status IN ('ACTIVATE','SUSPEND') AND administrator.id != '1'";
            $default_filter.= " AND administrator.administrator_group_id >= ".$this->session->userdata("__administrator_group::id");

            $request_filter = array(
                "administrator.id" => array("method" => "LIKE", "value" => null, "alias" => "administrator_id" , "type" => "TEXT"),
                "administrator.username"         => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "username", "type" => "TEXT"),
                "administrator_group.name"       => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "group", "type" => "TEXT"),
                "administrator.first_name"=> array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "first_name", "type" => "TEXT"),
                "administrator.last_name"=> array("method" => "LIKE", "value" => $datatables_request["columns"]["3"]["search"]["value"]  , "alias" => "last_name", "type" => "TEXT"),
                "administrator.telephone"             => array("method" => "LIKE", "value" => $datatables_request["columns"]["4"]["search"]["value"]  , "alias" => "telephone", "type" => "TEXT"),
                "administrator.status"           => array("method" => "LIKE", "value" => $datatables_request["columns"]["5"]["search"]["value"]  , "alias" => "status"           , "type" => "TEXT")
            );
            $join = array(
                "administrator_group" => "administrator.administrator_group_id = administrator_group.id"
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        if($function == "edit_administrator_group")
        {
            $this->load->model("administrator_group_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            $model_filter->where["id >="] = $this->session->userdata("__administrator_group::id");
            if($this->input->get("search") != "")
            {
                $model_filter->like["name"] = $this->input->get("search");
            }
            $administrator_groups = $this->administrator_group_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $administrator_groups);
        }
    }
}