<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/brand/Index");
    }

    public function edit($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("brand_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $brand = $this->brand_model->find($id);
        if($this->input->post())
        {
            $name_th = $this->input->post("name_th");
            $name_en = $this->input->post("name_en");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["name_th"] = $name_th;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->brand_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "Duplicate data");
            }


            if($id != null)
            {
                $brand = $this->brand_model->find($id);
                $brand->modified_date = date("Y-m-d h:i:s");
                $brand->modified_by = $this->session->userdata("__administrator::id");
            }
            else
            {
                $brand = new stdClass();
                $brand->created_date = date("Y-m-d h:i:s");
                $brand->created_by = $this->session->userdata("__administrator::id");
            }

            $brand->name_th = $name_th;
            $brand->name_en = $name_en;
            $brand->status = $this->input->post("status");
            
            $id = $this->brand_model->save($brand);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "brand" => $brand
        );
        $this->load->view("administrator_area/brand/Edit",$data);
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("brand_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $brand = $this->brand_model->find($id);
        $brand->modified_date = date("Y-m-d h:i:s");
        $brand->modified_by = $this->session->userdata("__administrator::administrator_id");
        $brand->status = "REMOVED";

        $this->brand_model->save($brand);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }



    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "brand";
            $default_filter = "brand.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "brand.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "brand.name_th"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "name_th", "type" => "TEXT"),
                "brand.name_en"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "name_en", "type" => "TEXT")
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        
    }
}