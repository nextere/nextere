<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/news/Index");
    }

    public function edit($id = null)
    { 
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("news_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $news = $this->news_model->find($id);
        if($this->input->post())
        {
            $title = $this->input->post("title");
            $message = $this->input->post("message");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["title"] = $title;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->news_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "Duplicate data");
            }


            if($id != null)
            {
                $news = $this->news_model->find($id);
                $news->modified_date = date("Y-m-d h:i:s");
                $news->modified_by = $this->session->userdata("__administrator::id");
            }
            else
            {
                $news = new stdClass();
                $news->created_date = date("Y-m-d h:i:s");
                $news->created_by = $this->session->userdata("__administrator::id");
            }

            $news->title = $title;
            $news->message = $message;
            $news->status = $this->input->post("status");
            
            $id = $this->news_model->save($news);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "news" => $news
        );
        $this->load->view("administrator_area/news/Edit",$data);
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("news_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $news = $this->news_model->find($id);
        $news->modified_date = date("Y-m-d h:i:s");
        $news->modified_by = $this->session->userdata("__administrator::administrator_id");
        $news->status = "REMOVED";

        $this->news_model->save($news);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }
    public function sendmail(){

        $this->load->library("response_library");
        $this->load->library("encryption_library");
        $this->load->model("news_model");
        $this->load->model("news_log_model");
        $this->load->model("subscribe_model");
        $this->load->model("Contact_us_model");
        

        //***************** GET INPUT ***************************//

       
        $contact_id = $this->input->post("contact_id");
        $news_id = $this->input->post("news_id");
        $contact = $this->Contact_us_model->find(1);
        $news = $this->news_model->find($news_id);


        $model_filter = new stdClass();
        $model_filter->where["status"] = "ACTIVATE";
        $model_filter->where["contact_title_id"] = $contact_id;
        $subscribe = $this->subscribe_model->search($model_filter);

        $count_id = count($subscribe);



        for ($i=0; $i < $count_id; $i++) { 

            $subscribe_member = $this->subscribe_model->find($subscribe[$i]->id);

            $subject_email = "Nextere";
            $headers = "From: ".$contact->email."\r\n"."MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $email = $subscribe_member->email;

            $html = "
                <html>
                <head>
                <title></title>
                </head>
                <body>
                <p>".$news->title."</p>
                ".$news->message."
                <br>
                <br>
                </body>
                </html>
                ";

            mail($email, $subject_email, $html, $headers); 

            $subscribe_user = new stdClass();
            $subscribe_user->created_date = date("Y-m-d h:i:s");
            $subscribe_user->created_by = $this->session->userdata("__administrator::id");
            $subscribe_user->subscribe_id = $subscribe_member->id;
            $subscribe_user->news_id = $news->id;
            $subscribe_user->status = 'ACTIVATE';

            $this->news_log_model->save($subscribe_user);
            
        }


        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }




    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "news";
            $default_filter = "news.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "news.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "news.title"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "title", "type" => "TEXT"),
                "news.status"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "status", "type" => "TEXT")
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");


        if($function == "edit_contact") 
        {
            $this->load->model("contact_title_model");
            $model_filter = new stdClass();
            $model_filter->where_in["status"] = array("ACTIVATE","SUBSCRIBE");
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_en"] = $this->input->get("search");
            }
            $contact = $this->contact_title_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $contact);
        }

        
    }
}