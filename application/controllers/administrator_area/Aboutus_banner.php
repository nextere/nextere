<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aboutus_banner extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    } 
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/aboutus_banner/Index");
    }


    public function detail($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("aboutus_banner_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $aboutus_banner = $this->aboutus_banner_model->find($id);
        

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "aboutus_banner" => $aboutus_banner
        );
        $this->load->view("administrator_area/aboutus_banner/Detail",$data);
    }

    public function edit($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("aboutus_banner_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $aboutus_banner = $this->aboutus_banner_model->find($id);
        if($this->input->post())
        {
            $image_path = '';
            if (!file_exists(FCPATH.'/assets/uploads/aboutusbanner/')) {
                mkdir(FCPATH.'/assets/uploads/aboutusbanner/', 0777, true);
            }
            if (!empty($_FILES['image_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = FCPATH.'/assets/uploads/aboutusbanner/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("image_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $image_path = '/assets/uploads/aboutusbanner/'.$upload_data['file_name'];
                }
                else
                {
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            }


            if($id != null)
            {
                $aboutus_banner = $this->aboutus_banner_model->find($id);
                $aboutus_banner->modified_date = date("Y-m-d h:i:s");
                $aboutus_banner->modified_by = $this->session->userdata("__administrator::id");

                
            }
            else
            {
                $aboutus_banner = new stdClass();
                $aboutus_banner->created_date = date("Y-m-d h:i:s");
                $aboutus_banner->created_by = $this->session->userdata("__administrator::id");

                $sort_filter = new stdClass();
                $sort_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
                $counts = $this->aboutus_banner_model->search($sort_filter);
                $count = count($counts);
                $aboutus_banner->order = $count+1;
            } 
            $aboutus_banner->name_th =$this->input->post("name_th");
            $aboutus_banner->name_en =$this->input->post("name_en");
            $aboutus_banner->description_th =$this->input->post("description_th");
            $aboutus_banner->description_en =$this->input->post("description_en");
            if(!empty($image_path))
                $aboutus_banner->image_path = $image_path;
            $aboutus_banner->status = $this->input->post("status");
            
            $id = $this->aboutus_banner_model->save($aboutus_banner);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "aboutus_banner" => $aboutus_banner
        );
        $this->load->view("administrator_area/aboutus_banner/Edit",$data);
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("aboutus_banner_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $aboutus_banner = $this->aboutus_banner_model->find($id);
        $aboutus_banner->modified_date = date("Y-m-d h:i:s");
        $aboutus_banner->modified_by = $this->session->userdata("__administrator::administrator_id");
        $aboutus_banner->status = "REMOVED";

        $this->aboutus_banner_model->save($aboutus_banner);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    public function sort()
    {
        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $this->load->model("aboutus_banner_model");

        if($this->input->post())
        { 

            $num = $this->input->post("id[]");
            $ids = $this->input->post("id[]");

            $min = min($ids);
            foreach($ids as $key => $id)
            {

                $menu = $this->aboutus_banner_model->find($key);
                $menu->modified_date = date("Y-m-d H:i:s");
                $menu->modified_by = $this->session->userdata("__administrator::administrator_id");
                $menu->order = $min;
                $this->aboutus_banner_model->save($menu);

                $min++;
            }

            $menu_validated = array("code" => "0x0000-00000", "message" => "Server response success. Request process complete with no error.");

            if($menu_validated["code"] == "0x0000-00000")
            {

                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }
            else
            {
                $this->response_library->responseJSON($menu_validated["code"], $menu_validated["message"]);
            }

        }
    }



    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "aboutus_banner";
            $default_filter = "aboutus_banner.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "aboutus_banner.id"             => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "aboutus_banner.order"          => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "order", "type" => "TEXT"),
                "aboutus_banner.image_path"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "image_path", "type" => "TEXT"),
                "aboutus_banner.description_th" => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "description_th", "type" => "TEXT"),
                
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        
    }
}