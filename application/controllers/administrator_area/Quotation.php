<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotation extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/quotation/Index");
    }
    public function export_pdf($id = null)
    {
        $this->load->model("contact_us_model");
        $this->load->model("quotation_model");
        $this->load->model("quotation_detail_model");
        $this->load->model("administrator_model");

        $contact_us = $this->contact_us_model->find(1);
        $quotation = $this->quotation_model->find($id);
        $administrator = $this->administrator_model->find($quotation->created_by);

        $model_filter = new stdClass();
        $model_filter->where["quotation_detail.quotation_id"] = $id;
        $model_filter->where["quotation_detail.status"] = "ACTIVATE";
        $model_filter->join = array(
            "product" => "quotation_detail.product_id = product.id"
        );
        $quotation_details = $this->quotation_detail_model->search($model_filter);

        
        // $mpdf = new \Mpdf\Mpdf();
        // $mpdf->WriteHTML('<h1>Hello world!</h1>');
        // $mpdf->Output();

        $data = array(
            "contact_us" => $contact_us,
            "quotation" => $quotation,
            "administrator" => $administrator,
            "quotation_details" => $quotation_details,
        );

        $this->load->view("administrator_area/quotation",$data);

    }

    public function edit($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("quotation_model");
        $this->load->model("quotation_detail_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $model_filter = new stdClass();
        $model_filter->where["quotation.id"] = $id;
        $model_filter->join = array(
            "business_type" => "quotation.business_type_id = business_type.id",
            "member" => "quotation.member_id = member.id"
        );
        $model_filter->get_first = true;
        $quotation = $this->quotation_model->search($model_filter);
        $model_filter = new stdClass();
        $model_filter->where["quotation_detail.quotation_id"] = $id;
        $model_filter->where["quotation_detail.status"] = "ACTIVATE";
        $model_filter->join = array(
            "product" => "quotation_detail.product_id = product.id"
        );
        $quotation_details = $this->quotation_detail_model->search($model_filter);

        if($this->input->post())
        {
            $type = $this->input->post("type");
            $first_name = trim($this->input->post("first_name"));
            $last_name = trim($this->input->post("last_name"));
            $telephone = trim($this->input->post("telephone"));
            $email = trim($this->input->post("email"));

            if (empty($type) || empty($first_name) || empty($last_name) || empty($telephone) || empty($email)) {
                $this->response_library->responseJSON("0x000I-00001", "Invalid data");
            }


            if($id != null)
            {
                $quotation = $this->quotation_model->find($id);
                $quotation->modified_date = date("Y-m-d h:i:s");
                $quotation->modified_by = $this->session->userdata("__administrator::id");
            }
            else
            {
                $quotation = new stdClass();
                $quotation->created_date = date("Y-m-d h:i:s");
                $quotation->created_by = $this->session->userdata("__administrator::id");
                $quotation->quotation_no = "Q".time();
            }

            $quotation->member_id = empty($this->input->post("member_id")) ? null : $this->input->post("member_id");
            $quotation->type = $type;
            $quotation->first_name = $first_name;
            $quotation->last_name = $last_name;
            $quotation->telephone = $telephone;
            $quotation->email = $email;
            $quotation->company_name = trim($this->input->post("company_name"));
            $quotation->business_type_id = empty($this->input->post("business_type_id")) ? null : $this->input->post("business_type_id");
            $quotation->tax_id = trim($this->input->post("tax_id"));
            $quotation->message = trim($this->input->post("message"));
            $quotation->status = $this->input->post("status");
            
            $id = $this->quotation_model->save($quotation); 
 
            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.", $id);
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "quotation" => $quotation,
            "quotation_details" => $quotation_details
        );
        $this->load->view("administrator_area/quotation/Edit",$data);
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("quotation_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $quotation = $this->quotation_model->find($id);
        $quotation->modified_date = date("Y-m-d h:i:s");
        $quotation->modified_by = $this->session->userdata("__administrator::administrator_id");
        $quotation->status = "REMOVED";

        $this->quotation_model->save($quotation);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }


    public function edit_quotation_detail()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("quotation_detail_model");
        $this->load->model("product_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           POST PHASE
        /** ------------------------------------------------------- **/
        if($this->input->post())
        {
            $id = $this->input->post("id");
            $quotation_id = $this->input->post("quotation_id");
            $product_id = $this->input->post("product_id");
            $quantity = $this->input->post("quantity");
            $price = $this->input->post("price");

            if (empty($product_id) || $quantity =='' || $price=='') {
                $this->response_library->responseJSON("0x0API-00001", "Please input data");
            }

            $product = $this->product_model->find($product_id);

            if(empty($id))
            {
                $quotation_detail = new stdClass();
                $quotation_detail->created_date = date("Y-m-d h:i:s");
                $quotation_detail->created_by = $this->session->userdata("__administrator::id");
                $quotation_detail->status = "ACTIVATE";
            }
            else
            {
                $quotation_detail = $this->quotation_detail_model->find($id);
                $quotation_detail->modified_date = date("Y-m-d h:i:s");
                $quotation_detail->modified_by = $this->session->userdata("__administrator::id");
                
            }

            $quotation_detail->quotation_id = $quotation_id;
            $quotation_detail->product_id = $product_id;
            $quotation_detail->real_price = $product->retail_price;
            $quotation_detail->quantity = $quantity;
            $quotation_detail->price_per_unit = $price;
            $quotation_detail->total_price = $price*$quantity;
            $quotation_detail_id = $this->quotation_detail_model->save($quotation_detail);

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }
    }


    public function delete_quotation_detail()
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("quotation_detail_model");
        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $id = $this->input->post("id");
        $quotation_detail = $this->quotation_detail_model->find($id);
        $quotation_detail->modified_date = date("Y-m-d h:i:s");
        $quotation_detail->modified_by = $this->session->userdata("__administrator::id");
        $quotation_detail->status = "REMOVED";

        $this->quotation_detail_model->save($quotation_detail);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "quotation";
            $default_filter = "quotation.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "quotation.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "quotation.quotation_no"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "quotation_no", "type" => "TEXT"),
                "quotation.first_name"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "first_name", "type" => "TEXT"),
                "quotation.last_name"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "last_name", "type" => "TEXT"),
                "quotation.message"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["3"]["search"]["value"]  , "alias" => "message", "type" => "TEXT")
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
        else if($function == "index_quotation_detail")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "quotation_detail";
            $default_filter = "quotation_detail.status like 'ACTIVATE'";            
            $default_filter .= " AND quotation_detail.quotation_id = ".$this->input->post("quotation_id");
            $request_filter = array(
                "quotation_detail.id"       => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "quotation_detail.quotation_id"       => array("method" => "LIKE", "value" => null, "alias" => "quotation_id" , "type" => "TEXT"),
                "quotation_detail.product_id"       => array("method" => "LIKE", "value" => null, "alias" => "product_id" , "type" => "TEXT"),
                "product.name_en"   => array("method" => "LIKE", "value" =>null  , "alias" => "product_name_en", "type" => "TEXT"),
                "product.code"              => array("method" => "LIKE", "value" => null , "alias" => "product_code", "type" => "TEXT"),
                "quotation_detail.real_price"   => array("method" => "LIKE", "value" => null , "alias" => "real_price", "type" => "TEXT"),
                "quotation_detail.quantity" => array("method" => "LIKE", "value" => null , "alias" => "quantity", "type" => "TEXT"),
                "quotation_detail.price_per_unit" => array("method" => "LIKE", "value" => null , "alias" => "price_per_unit", "type" => "TEXT")
            );
            $join = array(
                "product" => "quotation_detail.product_id = product.id"
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");
        if($function == "edit_member")
        {
            $this->load->model("member_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            if($this->input->get("search") != "")
            {
                $model_filter->like["fullname"] = $this->input->get("search");
            }
            $members = $this->member_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $members);
        }
        else if($function == "edit_business_type")
        {
            $this->load->model("business_type_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_en"] = $this->input->get("search");
            }
            $business_types = $this->business_type_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $business_types);
        }
        else if($function == "edit_business_type")
        {
            $this->load->model("product_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            if($this->input->get("search") != "")
            {
                $model_filter->like["CONCAT(name_en,code)"] = $this->input->get("search");
            }
            $products = $this->product_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $products);
        }
        
    }
}