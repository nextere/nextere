<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function dash()
    {

        $this->load->model("purchase_transaction_detail_model");  
        $this->load->model("purchase_transaction_model");  
        $this->load->model("product_model");
        $this->load->model("brand_model");
        $this->load->model("member_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $model_filter = new stdClass();
        $model_filter->select = 'COUNT(id) as count';
        $model_filter->where["status"] = "ACTIVATE";
        $model_filter->get_first = true;
        $sum_member = $this->member_model->search($model_filter);


        $model_filter = new stdClass();
        $model_filter->select = 'COUNT(id) as count';
        $model_filter->where["status"] = "ACTIVATE";
        $model_filter->get_first = true;
        $sum_product = $this->product_model->search($model_filter);

        $model_filter = new stdClass();
        $model_filter->select = 'COUNT(id) as count';
        $model_filter->where["status"] = "ACTIVATE";
        $model_filter->get_first = true;
        $sum_brand = $this->brand_model->search($model_filter);


        $model_filter = new stdClass();
        $model_filter->select = 'COUNT(id) as count';
        $model_filter->get_first = true;
        $sum_purchase_transaction = $this->purchase_transaction_model->search($model_filter);

        $model_filter = new stdClass();
        $model_filter->select = 'SUM(purchase_transaction_detail.total_price) as price,MONTH(purchase_transaction_detail.created_date) AS month';
        $model_filter->join = array(
            "purchase_transaction" => "purchase_transaction_detail.purchase_transaction_id = purchase_transaction.id"
        );
        $model_filter->where["purchase_transaction_detail.status"] = "PAID";
        $model_filter->where["purchase_transaction.status"] = "SUCCESS";
        $model_filter->custom_where = "YEAR(purchase_transaction_detail.created_date) = '".date('Y')."'";
        $model_filter->group_by = ' MONTH(purchase_transaction_detail.created_date)';
        $chart = $this->purchase_transaction_detail_model->search($model_filter);

        $model_filter = new stdClass();
        $model_filter->join = array(
            "member" => "purchase_transaction.member_id = member.id",
        );
        $model_filter->where["purchase_transaction.status"] = 'SUCCESS';
        $model_filter->order_by = 'purchase_transaction.id desc';
        $model_filter->limit = 20;
        $purchases = $this->purchase_transaction_model->search($model_filter);
    
        $data = array(
            'sum_member' => $sum_member,
            'sum_product' => $sum_product,
            'sum_purchase_transaction' => $sum_purchase_transaction,
            'sum_brand' => $sum_brand,
            'chart' => $chart,
            'purchases' => $purchases
        );

        $this->load->view("administrator_area/Report/dashboard", $data);
        
        
    }
    public function product_sales()
    {
        $this->load->model("purchase_transaction_detail_model");  
        $this->load->model("purchase_transaction_model");  
        $this->load->model("product_model");
        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        if($this->input->post())
        {
            $model_filter = new stdClass();
            $model_filter->select = 'SUM(purchase_transaction_detail.qty) as qty,SUM(purchase_transaction_detail.total_price) as total_price,purchase_transaction_detail.created_date,product.name_en ,purchase_transaction.courier_name as courier_name';
            $model_filter->join = array(
                "product" => "purchase_transaction_detail.product_id = product.id",
                "purchase_transaction" => "purchase_transaction_detail.purchase_transaction_id = purchase_transaction.id"
            );
            if(!empty($this->input->post('product_id'))){
                $model_filter->where["purchase_transaction_detail.product_id"] = $this->input->post('product_id');
            }
                $model_filter->where["purchase_transaction_detail.status"] = "PAID";
                $model_filter->where["purchase_transaction.status"] = "SUCCESS";
            if(!empty($this->input->post('transaction_date')) && !empty($this->input->post('transaction_date_end'))){
                $model_filter->custom_where = "purchase_transaction_detail.created_date BETWEEN '".date_format(date_create($this->input->post('transaction_date')),"Y/m/d")."' AND '".date_format(date_create($this->input->post('transaction_date_end')),"Y/m/d")."'";
            }else if(!empty($this->input->post('transaction_date'))){
                $model_filter->custom_where = "purchase_transaction_detail.created_date >= '".date_format(date_create($this->input->post('transaction_date')),"Y/m/d")."'";
            }else if(!empty($this->input->post('transaction_date_end'))){
                $model_filter->custom_where = "purchase_transaction_detail.created_date <= '".date_format(date_create($this->input->post('transaction_date_end')),"Y/m/d")."'";
            }
            $model_filter->group_by = 'purchase_transaction_detail.product_id ';
            $results = $this->purchase_transaction_detail_model->search($model_filter);

            // print_r($results);exit();

            $model_filter = new stdClass();
            $model_filter->select = 'SUM(purchase_transaction_detail.qty) as qty,SUM(purchase_transaction_detail.total_price) as total_price,purchase_transaction_detail.created_date,product.name_en,purchase_transaction_detail.created_date,product.name_en ,purchase_transaction.courier_name as courier_name';
            $model_filter->join = array(
                "product" => "purchase_transaction_detail.product_id = product.id",
                "purchase_transaction" => "purchase_transaction_detail.purchase_transaction_id = purchase_transaction.id"
            );
            if(!empty($this->input->post('product_id'))){
                $model_filter->where["purchase_transaction_detail.product_id"] = $this->input->post('product_id');
            }
                $model_filter->where["purchase_transaction_detail.status"] = "PAID";
                $model_filter->where["purchase_transaction.status"] = "SUCCESS";
            if(!empty($this->input->post('transaction_date')) && !empty($this->input->post('transaction_date_end'))){
                $model_filter->custom_where = "purchase_transaction_detail.created_date BETWEEN '".date_format(date_create($this->input->post('transaction_date')),"Y/m/d")."' AND '".date_format(date_create($this->input->post('transaction_date_end')),"Y/m/d")."'";
            }else if(!empty($this->input->post('transaction_date'))){
                $model_filter->custom_where = "purchase_transaction_detail.created_date >= '".date_format(date_create($this->input->post('transaction_date')),"Y/m/d")."'";
            }else if(!empty($this->input->post('transaction_date_end'))){
                $model_filter->custom_where = "purchase_transaction_detail.created_date <= '".date_format(date_create($this->input->post('transaction_date_end')),"Y/m/d")."'";
            }
            $model_filter->get_first = true;
            $sum = $this->purchase_transaction_detail_model->search($model_filter);


            $search = $this->input->post();
            $product = $this->product_model->find($this->input->post('product_id'));


            $data  = array(
                'results' => $results,
                'search' => $search,
                'product' => $product,
                'sum' => $sum,
            );
           
            $this->load->view("administrator_area/Report/product_sales",$data); 
        }else{
            $this->load->view("administrator_area/Report/product_sales");
        }
        
    }
    public function product_order()
    {
        $this->load->model("purchase_transaction_detail_model");  
        $this->load->model("purchase_transaction_model");  
        $this->load->model("product_model");
        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        if($this->input->post())
        {
            $model_filter = new stdClass();
            // $model_filter->select = 'SUM(purchase_transaction_detail.qty) as qty,SUM(purchase_transaction_detail.total_price) as total_price,purchase_transaction_detail.created_date,product.name_en';
            $model_filter->join = array(
                "product" => "purchase_transaction_detail.product_id = product.id",
                "purchase_transaction" => "purchase_transaction_detail.purchase_transaction_id = purchase_transaction.id"
            );
            if(!empty($this->input->post('product_id'))){
                $model_filter->where["purchase_transaction_detail.product_id"] = $this->input->post('product_id');
            }
                $model_filter->where["purchase_transaction_detail.status"] = "PAID";
                $model_filter->where["purchase_transaction.status"] = "SUCCESS";
            if(!empty($this->input->post('transaction_date')) && !empty($this->input->post('transaction_date_end'))){
                $model_filter->custom_where = "purchase_transaction_detail.created_date BETWEEN '".date_format(date_create($this->input->post('transaction_date')),"Y/m/d")."' AND '".date_format(date_create($this->input->post('transaction_date_end')),"Y/m/d")."'";
            }else if(!empty($this->input->post('transaction_date'))){
                $model_filter->custom_where = "purchase_transaction_detail.created_date >= '".date_format(date_create($this->input->post('transaction_date')),"Y/m/d")."'";
            }else if(!empty($this->input->post('transaction_date_end'))){
                $model_filter->custom_where = "purchase_transaction_detail.created_date <= '".date_format(date_create($this->input->post('transaction_date_end')),"Y/m/d")."'";
            }
            $results = $this->purchase_transaction_detail_model->search($model_filter);

            $arrs = array();
                foreach ($results as $key => $item) {
                $arrs[$item->{'purchase_transaction_detail::product_id'}][$key] = $item;
                }


            $model_filter = new stdClass();
            $model_filter->select = 'SUM(purchase_transaction_detail.qty) as qty,SUM(purchase_transaction_detail.total_price) as total_price,purchase_transaction_detail.created_date,product.id';
            $model_filter->join = array(
                "product" => "purchase_transaction_detail.product_id = product.id",
                "purchase_transaction" => "purchase_transaction_detail.purchase_transaction_id = purchase_transaction.id"
            );
            if(!empty($this->input->post('product_id'))){
                $model_filter->where["purchase_transaction_detail.product_id"] = $this->input->post('product_id');
            }
                $model_filter->where["purchase_transaction_detail.status"] = "PAID";
                $model_filter->where["purchase_transaction.status"] = "SUCCESS";
            if(!empty($this->input->post('transaction_date')) && !empty($this->input->post('transaction_date_end'))){
                $model_filter->custom_where = "purchase_transaction_detail.created_date BETWEEN '".date_format(date_create($this->input->post('transaction_date')),"Y/m/d")."' AND '".date_format(date_create($this->input->post('transaction_date_end')),"Y/m/d")."'";
            }else if(!empty($this->input->post('transaction_date'))){
                $model_filter->custom_where = "purchase_transaction_detail.created_date >= '".date_format(date_create($this->input->post('transaction_date')),"Y/m/d")."'";
            }else if(!empty($this->input->post('transaction_date_end'))){
                $model_filter->custom_where = "purchase_transaction_detail.created_date <= '".date_format(date_create($this->input->post('transaction_date_end')),"Y/m/d")."'";
            }
            $model_filter->group_by = 'purchase_transaction_detail.product_id ';
            $sum = $this->purchase_transaction_detail_model->search($model_filter);
           
            $search = $this->input->post();
            $product = $this->product_model->find($this->input->post('product_id'));

            $data  = array(
                'results' => $results,
                'search' => $search,
                'product' => $product,
                'sum' => $sum,
                'arrs' => $arrs
            );
           
            $this->load->view("administrator_area/Report/product_order",$data); 
        }else{
            $this->load->view("administrator_area/Report/product_order");
        }
        
    }
    public function export_product_sales()
    {

        $this->load->view("administrator_area/exportexcel/Product_sales");
        
    }
    public function export_product_order()
    {

        $this->load->view("administrator_area/exportexcel/Product_order");
        
    }
    




    public function select2($function)
    {
        $this->load->library("response_library");
        if($function == "edit_product")
        {
            $this->load->model("product_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_en"] = $this->input->get("search");
            }
            $products = $this->product_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $products);
        }
        
    }
}