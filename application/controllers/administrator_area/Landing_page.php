<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing_page extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    } 
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->model("landing_page_model"); 

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $landing_page = $this->landing_page_model->find(1);
        if($this->input->post())
        {

            $id = $this->input->post("id");
            $message = $this->input->post("message");
            $status = $this->input->post("status");

            $banner_path = '';
            if (!file_exists(FCPATH.'/assets/uploads/landing_page/')) {
                mkdir(FCPATH.'/assets/uploads/landing_page/', 0777, true);
            }
            if (!empty($_FILES['banner_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = FCPATH.'/assets/uploads/landing_page/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("banner_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $banner_path = '/assets/uploads/landing_page/'.$upload_data['file_name'];
                }
                else
                {
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            }
            
            if($id != null)
            {
                $landing_page = $this->landing_page_model->find($id);
                $landing_page->modified_date = date("Y-m-d h:i:s");
                $landing_page->modified_by = $this->session->userdata("__administrator::id");
            }

            $landing_page->message = $message;
            $landing_page->status = $status;
            if(!empty($banner_path))
                $landing_page->banner_path = $banner_path;
            
            $id = $this->landing_page_model->save($landing_page);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        $data = array(
            "landing_page" => $landing_page
        );

        $this->load->view("administrator_area/landing_page/Edit",$data);
    }


}