<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Whyus extends CI_Controller
{
    private $upload_path = '/assets/uploads/whyus/';
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $this->load->view("administrator_area/whyus/Index");
    }
    
    public function detail($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("whyus_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $whyus = $this->whyus_model->find($id);
        

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "whyus" => $whyus
        );
        $this->load->view("administrator_area/whyus/Detail",$data);
    }

    public function edit($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("whyus_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $whyus = $this->whyus_model->find($id);
        if($this->input->post())
        {
            $title_th = $this->input->post("title_th");
            $title_en = $this->input->post("title_en");
            //Dup Data
            $dup_filter = new stdClass();
            $dup_filter->where["title_th"] = $title_th;
            if($id != null)
                $dup_filter->where["id != "] = $id;
            $dup_filter->where_in["status"] = array("ACTIVATE", "SUSPEND");
            $dup_filter->get_first = true;
            $dup_data = $this->whyus_model->search($dup_filter);
            if ($dup_data != null) {
                $this->response_library->responseJSON("0x000C-U0002", "มีการใช้หัวข้อนี้แล้ว");
            }

            if (!file_exists(FCPATH.$this->upload_path)) {
                mkdir(FCPATH.$this->upload_path, 0777, true);
            } 

            $file_name = "";
            if (!empty($_FILES['image_file']['name']) )
            {
                $fileName = date("YmdHis")."_".$this->guid_library->generate();
                $config['file_name'] = $fileName;
                $config['upload_path'] = FCPATH.$this->upload_path;
                $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $uploadCheck = $this->upload->do_upload("image_file");
                if ($uploadCheck == 1) {
                    $upload_data = $this->upload->data();
                    $file_name =   '/assets/uploads/whyus/'.$upload_data['file_name'];
                }
                else
                {
                    $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                }
            } 


            if($id != null)
            {
                $whyus = $this->whyus_model->find($id);
                $whyus->modified_date = date("Y-m-d h:i:s");
                $whyus->modified_by = $this->session->userdata("__administrator::id");
            }
            else
            {
                $whyus = new stdClass();
                $whyus->created_date = date("Y-m-d h:i:s");
                $whyus->created_by = $this->session->userdata("__administrator::id");
            }

            $whyus->title_th = $title_th;
            $whyus->title_en = $title_en;
            $whyus->description_th = $this->input->post("description_th");
            $whyus->description_en = $this->input->post("description_en");
            if (!empty($file_name)) {
                $whyus->thumbnail_path = $file_name;
            }
            $whyus->order = $this->input->post("order");
            $whyus->status = $this->input->post("status");
            
            $id = $this->whyus_model->save($whyus);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "whyus" => $whyus
        );
        $this->load->view("administrator_area/whyus/Edit",$data);
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("whyus_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $whyus = $this->whyus_model->find($id);
        $whyus->modified_date = date("Y-m-d h:i:s");
        $whyus->modified_by = $this->session->userdata("__administrator::administrator_id");
        $whyus->status = "REMOVED";

        $this->whyus_model->save($whyus);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }


    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "whyus";
            $default_filter = "whyus.status IN ('ACTIVATE','SUSPEND')";
            $request_filter = array(
                "whyus.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "whyus.title_th"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "title_th", "type" => "TEXT"),
                "whyus.title_en"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "title_en", "type" => "TEXT"),
                "whyus.thumbnail_path"     => array("method" => "LIKE", "value" => null  , "alias" => "thumbnail_path", "type" => "TEXT")
            );
            $join = array();

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }
}