<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscribe extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {

        $this->load->view("administrator_area/subscribe/Index");
    } 

    public function edit($id = null)
    { 
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("subscribe_model");
        $this->load->model("contact_title_model");

        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $subscribe = $this->subscribe_model->find($id);

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->where["id"] = $subscribe->contact_title_id;
        $contact_title = $this->contact_title_model->search($model_filter);

        if($this->input->post())
        {
           
            $subscribe = $this->subscribe_model->find($id);
            $subscribe->modified_date = date("Y-m-d h:i:s");
            $subscribe->modified_by = $this->session->userdata("__administrator::id");
            $subscribe->contact_title_id = $this->input->post('contact_title_id');
            
            $id = $this->subscribe_model->save($subscribe);            

            $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
        }

        /** ------------------------------------------------------- **
         **           RENDER VIEW PHASE
        /** ------------------------------------------------------- **/
        $data = array(
            "subscribe" => $subscribe,
            "contact_title" => $contact_title
        );
        $this->load->view("administrator_area/subscribe/Edit",$data);
    }

    public function delete($id = null)
    {
        /** ------------------------------------------------------- **
         **           CONSTRUCT PHASE
        /** ------------------------------------------------------- **/
        $this->load->model("subscribe_model");

        $this->load->library("response_library");

        /** ------------------------------------------------------- **
         **           PROCESS PHASE
        /** ------------------------------------------------------- **/
        $subscribe = $this->subscribe_model->find($id);
        $subscribe->modified_date = date("Y-m-d h:i:s");
        $subscribe->modified_by = $this->session->userdata("__administrator::administrator_id");
        $subscribe->status = "REMOVED";

        $this->subscribe_model->save($subscribe);

        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }

    public function sendmail(){

        $this->load->library("response_library");
        $this->load->library("encryption_library");
        $this->load->model("news_model");
        $this->load->model("news_log_model");
        $this->load->model("subscribe_model");
        $this->load->model("Contact_us_model");
        

        //***************** GET INPUT ***************************//

       
        $id = $this->input->post("id");
        $news_id = $this->input->post("news_id");
        $count_id = count($id);
        $news = $this->news_model->find($news_id);
        $contact = $this->Contact_us_model->find(1);

        for ($i=0; $i < $count_id; $i++) { 

            $subscribe = $this->subscribe_model->find($id[$i]);

            $subject_email = "Nextere";
            $headers = "From: ".$contact->email."\r\n"."MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $email = $subscribe->email;

            $html = "
                <html>
                <head>
                <title></title>
                </head>
                <body>
                <p>".$news->title."</p>
                ".$news->message."
                <br>
                <br>
                </body>
                </html>
                ";

            mail($email, $subject_email, $html, $headers); 

            $subscribe_user = new stdClass();
            $subscribe_user->created_date = date("Y-m-d h:i:s");
            $subscribe_user->created_by = $this->session->userdata("__administrator::id");
            $subscribe_user->subscribe_id = $subscribe->id;
            $subscribe_user->news_id = $news_id;
            $subscribe_user->status = 'ACTIVATE';

            $this->news_log_model->save($subscribe_user);
            
        }


        $this->response_library->responseJSON("0x0000-00000", "Process request Complete.");
    }



    /** ------------------------------------------------------- **
     **           ADDITIONAL SCRIPT
    /** ------------------------------------------------------- **/
    public function datatables($function)
    {
        if($function == "index")
        {
            $this->load->library("datatables_library");

            $datatables_request = $this->input->post();

            $table = "subscribe";
            $default_filter = "subscribe.status = 'ACTIVATE'";
            $request_filter = array(
                "subscribe.id"          => array("method" => "LIKE", "value" => null, "alias" => "id" , "type" => "TEXT"),
                "subscribe.email"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["0"]["search"]["value"]  , "alias" => "email", "type" => "TEXT"),
                "contact_title.name_en"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["1"]["search"]["value"]  , "alias" => "name_en", "type" => "TEXT"),
                "subscribe.created_date"     => array("method" => "LIKE", "value" => $datatables_request["columns"]["2"]["search"]["value"]  , "alias" => "created_date", "type" => "SHORT_MONTH_DATETIME")
            );
            $join = array(
                "contact_title" => "subscribe.contact_title_id = contact_title.id",
            );

            $search_query = $this->datatables_library->buildSearchQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $row_count_query = $this->datatables_library->buildRowCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);
            $total_count_query = $this->datatables_library->buildTotalCountQuery($datatables_request, $table, $default_filter, $request_filter, $join);

            echo $this->datatables_library->generate($datatables_request, $search_query, $row_count_query, $total_count_query);
        }
    }

    public function select2($function)
    {
        $this->load->library("response_library");

        if($function == "edit_contact_title") 
        {
            $this->load->model("contact_title_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            if($this->input->get("search") != "")
            {
                $model_filter->like["name_en"] = $this->input->get("search");
            }
            $brands = $this->contact_title_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $brands);
        }

        if($function == "edit_news") 
        {
            $this->load->model("news_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
            if($this->input->get("search") != "")
            {
                $model_filter->like["title"] = $this->input->get("search");
            }
            $news = $this->news_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $news);
        }

        
    }
}