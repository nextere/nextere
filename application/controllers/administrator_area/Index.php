<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller
{
	function __construct()
	{
		parent::__construct();              
    	$this->ci = & get_instance ();
	}

	public function index()
	{
        redirect(base_url("administrator_area/Authentication/Login"));
	}

}