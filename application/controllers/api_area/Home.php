 <?php
 defined('BASEPATH') OR exit('No direct script access allowed');

 class Home extends CI_Controller
 {
    function __construct()
    {
        parent::__construct();
    }

    private $product_path = FCPATH.'/assets/uploads/product/';

    
    public function langding_page()
    {
        $this->load->library("response_library");

        $this->load->model("landing_page_model");

        $model_filter = new stdClass();
        $model_filter->get_first = true;
        $model_filter->where["status"] = 'ACTIVATE';

        $langding_page = $this->landing_page_model->search($model_filter);

        $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("langding_page" => $langding_page));
    }
    
    public function hot_link()
    {
        $this->load->library("response_library");

        $this->load->model("link_model");

        $model_filter = new stdClass();
        // $model_filter->get_first = true;
        $model_filter->where["status"] = 'ACTIVATE';

        $hot_link = $this->link_model->search($model_filter);

        $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("hot_link" => $hot_link));
    }

    public function Menu()
    {
        $this->load->library("response_library");

        $this->load->model("menu_category_model");
        $this->load->model("submenu_category_model");
        $this->load->model("child_submenu_category_model");

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->order = "order ASC";

        $menu_categories = $this->menu_category_model->search($model_filter);

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->order = "order ASC";

        $submenu_categories = $this->submenu_category_model->search($model_filter);

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->order = "order ASC";

        $child_submenu_categories = $this->child_submenu_category_model->search($model_filter);


        $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("menu_categories" => $menu_categories, "submenu_categories" => $submenu_categories, "child_submenu_categories" => $child_submenu_categories));
    }

    public function Menu2()
    {
        $this->load->library("response_library");

        $this->load->model("menu_category_model");
        $this->load->model("submenu_category_model");
        $this->load->model("child_submenu_category_model");

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->order = "order ASC";

        $menu_categories = $this->menu_category_model->search($model_filter);

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->order = "order ASC";

        $submenu_categories = $this->submenu_category_model->search($model_filter);

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->order = "order ASC";

        $child_submenu_categories = $this->child_submenu_category_model->search($model_filter);

        foreach($submenu_categories as $submenu_key => $submenu_categorie)
        {
            foreach($child_submenu_categories as $child_submenu_key => $child_submenu_categorie)
            {
                if($submenu_categorie->id == $child_submenu_categorie->submenu_category_id)
                {
                    $submenu_categorie->child_submenu_categories[] = $child_submenu_categories[$child_submenu_key];
                }
            }

            // if($menu_categorie->id == $submenu_categorie->menu_category_id)
            // {
            //     $menu_categorie->submenu_categories[] = $submenu_categories[$submenu_key];
            // }
        }

        foreach($menu_categories as $menu_key => $menu_categorie)
        {
            foreach($submenu_categories as $submenu_key => $submenu_categorie)
            {
                if($menu_categorie->id == $submenu_categorie->menu_category_id)
                {
                    $menu_categorie->submenu_categories[] = $submenu_categories[$submenu_key];
                }

            }
        }

        // print_r($menu_categories);
        // exit();


        $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("menu_categories" => $menu_categories));
    }

    public function product_detail_menu()
    {
        $this->load->library("response_library");

        $this->load->model("promotion_discount_model");

        $this->load->model("menu_category_model");
        $this->load->model("submenu_category_model");
        $this->load->model("child_submenu_category_model");
        $this->load->model("product_detail_menu_model");
        $this->load->model("product_model");
        $this->load->model("product_type_model");
        $this->load->model("brand_model");        
        $this->load->model("product_technical_spec_model");
        $this->load->model("price_rate_model");

        if($this->input->post())
        {
            $menu_category_id = $this->input->post("menu_category_id");
            $submenu_category_id = $this->input->post("submenu_category_id");
            $child_submenu_category_id = $this->input->post("child_submenu_category_id");
            $show_item = $this->input->post("show_item");
            $page = $this->input->post("page");
            $price_rate_id = $this->input->post("price_rate_id");
            $technical_spec_id = $this->input->post("technical_spec_id");

            

            if(!isset($show_item))
            {
                $show_item = 12;
            }
            if(!isset($page))
            {
                $page = 1;
            }

            $price_rate = $this->price_rate_model->find($price_rate_id);

            $between = '';
            if(isset($price_rate))
            {
                $between = $price_rate->price_min . ' AND ' . $price_rate->price_max;
            }

            $submenu_categories = null;
            $child_submenu_categories =null;
            $menu_category = null;
            $submenu_category = null;
            $child_submenu_category = null;

            $model_filter = new stdClass();
            $model_filter->where["status"] = 'ACTIVATE';
            if(isset($menu_category_id))
            {
                $model_filter->where["menu_category_id"] = $menu_category_id;

                $menu_category = $this->menu_category_model->find($menu_category_id);

                $sub_filter = new stdClass();
                $sub_filter->where["status"] = 'ACTIVATE';
                $sub_filter->where["menu_category_id"] = $menu_category_id;
                $sub_filter->order_by = 'order ASC';

                $submenu_categories = $this->submenu_category_model->search($sub_filter);
            }
            if(isset($submenu_category_id))
            {
                $model_filter->where["submenu_category_id"] = $submenu_category_id;

                $submenu_category = $this->submenu_category_model->find($submenu_category_id);

                $sub_filter = new stdClass();
                $sub_filter->where["status"] = 'ACTIVATE';
                $sub_filter->where["submenu_category_id"] = $submenu_category_id;
                $sub_filter->order_by = 'order ASC';

                $child_submenu_categories = $this->child_submenu_category_model->search($sub_filter);
            }
            if(isset($child_submenu_category_id))
            {
             $model_filter->where["child_submenu_category_id"] = $child_submenu_category_id;

             $child_submenu_category = $this->child_submenu_category_model->find($child_submenu_category_id);

         }

         $products_all = $this->product_detail_menu_model->search($model_filter);

         $product_ids = array();
         if(isset($products_all) && count($products_all) > 0)
         {
            foreach($products_all as $item)
            {
                $products_ids[] = $item->product_id;

            }

            if(isset($technical_spec_id))
            {
                $model_filter = new stdClass();
                $model_filter->where["product_technical_spec.status"] = 'ACTIVATE';
                $model_filter->where_in["technical_spec_id"] = $technical_spec_id;
                $model_filter->where_in["product_id"] = $products_ids;
                $product_technical_specs = $this->product_technical_spec_model->search($model_filter);

                $product_technical_spec_array = array();
                foreach ($product_technical_specs as $key => $product_technical_spec) 
                {
                    $product_technical_spec_array[] = $product_technical_spec->product_id;
                }
            }    

            $page_start = ($page-1)*$show_item;

            $model_filter = new stdClass();
            $model_filter->where["product.status"] = 'ACTIVATE';
            if(!empty($product_technical_spec_array))
            {  
                $model_filter->where_in["product.id"] = $product_technical_spec_array;
            }else{
                $model_filter->where_in["product.id"] = $products_ids;
            }
            $model_filter->join = array("product_type" => "product.product_type_id = product_type.id","brand" => "product.brand_id = brand.id");

            if(isset($price_rate_id) && $price_rate_id != '')
            {
                $model_filter->custom_where = 'product.retail_price BETWEEN '.$between.' ';
            }

            $model_filter->limit = $show_item;
            $model_filter->limit_start = $page_start;

            $products = $this->product_model->search($model_filter);

            $model_filter = new stdClass();
            $model_filter->where["product.status"] = 'ACTIVATE';
            $model_filter->where_in["product.id"] = $products_ids;
            $model_filter->join = array("product_type" => "product.product_type_id = product_type.id","brand" => "product.brand_id = brand.id");

            if(isset($price_rate_id) && $price_rate_id != '')
            {
                $model_filter->custom_where = 'product.retail_price BETWEEN '.$between.' ';
            }

            $products_max = $this->product_model->search($model_filter);

            $item_max = count($products_max);

            $page_max = ceil(count($products_max)/$show_item);

            if(!empty($product_technical_spec_array))
            {
                $product_technical_spec = 'true';
            }
            else
            {
                $product_technical_spec = 'false';
            } 

            if(count($products) > 0)
            {
                foreach($products as $product)
                {
                    $image = array();
                    $dir = $this->product_path.$product->{"product::code"};
                    if (is_dir($dir))
                    {
                        $product_images = array_diff(scandir($dir), array('.', '..')); 
                    }
                    if(count($product_images) > 0)
                    {
                        foreach ($product_images as $product_image) 
                        {
                            $image[] = base_url("assets/uploads/product").'/'.$product->{"product::code"}.'/'.$product_image;
                        }
                        $product->{"product::images"} = $image;
                    }

                    $model_filter = new stdClass();
                    $model_filter->where["promotion_discount.status"] = 'ACTIVATE';
                    $model_filter->join = array("product_detail_category" => "promotion_discount.product_category_id = product_detail_category.product_category_id");
                    $promotion_discounts = $this->promotion_discount_model->search($model_filter);

                    $discount_array = array();
                    foreach($promotion_discounts as $promotion_discount)
                    {
                        if(isset($product->{"product::id"}) && ($promotion_discount->{"product_detail_category::product_id"} == $product->{"product::id"}))
                        {
                            $discount_array["discount_percent"] = $promotion_discount->{"promotion_discount::discount_percent"};
                            $discount_array["discount_baht"] = $promotion_discount->{"promotion_discount::discount_baht"};
                            $discount_array["product_id"] = $product->{"product::id"};
                        }
                    }

                    if(isset($discount_array) && !empty($discount_array))
                    {
                        $discount_model = new stdClass();
                        if($discount_array["discount_percent"] != '' && $discount_array["discount_percent"] != null)
                        {
                            $discount_model->discount_percent = $discount_array["discount_percent"];
                            $discount_model->retail_price = $product->{"product::retail_price"};
                            $discount_model->discount_price = number_format(($discount_model->retail_price*$discount_model->discount_percent), 2, '.', '');
                            $discount_model->total_price = ($discount_model->retail_price-$discount_model->discount_price);
                        }
                        if($discount_array["discount_baht"] != '' && $discount_array["discount_baht"] != null)
                        {
                            $discount_model->discount_percent = $discount_array["discount_baht"];
                            $discount_model->retail_price = $product->{"product::retail_price"};
                            $discount_model->discount_price = $discount_array["discount_baht"];
                            $discount_model->total_price = ($product->{"product::retail_price"}-$discount_array["discount_baht"]);
                        }

                        $product->{"product::discount"} = $discount_model;
                    }
                }

                
            }
            else
            {
                $products = null;
                $page_max = null;
                $item_max = null;
                $product_technical_spec =  null;
            }
        }
        else
        {
            $products = null;
            $page_max = null;
            $item_max = null;
            $product_technical_spec =  null;
        }

        $this->response_library->responseJSON("0x0000-00000","The sever respone contact form success.", array("page_max" => $page_max,"page" => $page,"show_item" => $show_item,"item_max" => $item_max,"menu_category_id" => $menu_category_id, "menu_category" => $menu_category , "submenu_categories" => $submenu_categories, "submenu_category_id" => $submenu_category_id, "submenu_category" => $submenu_category , "child_submenu_category_id" => $child_submenu_category_id , "child_submenu_category" => $child_submenu_category , "child_submenu_categories" => $child_submenu_categories , "products" => $products ,"product_technical_spec" => $product_technical_spec));

    }
    else
    {
        $this->response_library->responseJSON("1x0000-00000","The sever not respone.");
    }
}

public function Brand()
{
    $this->load->library("response_library");

    $this->load->model("brand_model");

    $model_filter = new stdClass();
    $model_filter->where["status"] = 'ACTIVATE';

    $brands = $this->brand_model->search($model_filter);


    $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("brands" => $brands));
}

public function Product_outstanding()
{
    $this->load->library("response_library");

    $this->load->model("promotion_discount_model");

    $this->load->model("product_model");

    $model_filter = new stdClass();
    $model_filter->where["status"] = 'ACTIVATE';
    $model_filter->where["is_recommend"] = '1';

    $recommendProducts = $this->product_model->search($model_filter);


    if(count($recommendProducts) > 0)
    {
        foreach($recommendProducts as $recommendProduct)
        {
            $image = array();
            $dir = $this->product_path.$recommendProduct->code;
            if (is_dir($dir))
            {
                $product_images = array_diff(scandir($dir), array('.', '..')); 
            }
            if(count($product_images) > 0)
            {
                foreach ($product_images as $product_image) 
                {
                    $image[] = base_url("assets/uploads/product").'/'.$recommendProduct->code.'/'.$product_image;
                }
                $recommendProduct->images = $image;
            }

            $model_filter = new stdClass();
            $model_filter->where["promotion_discount.status"] = 'ACTIVATE';
            $model_filter->join = array("product_detail_category" => "promotion_discount.product_category_id = product_detail_category.product_category_id");
            $promotion_discounts = $this->promotion_discount_model->search($model_filter);

            $discount_array = array();
            foreach($promotion_discounts as $promotion_discount)
            {
                if(isset($recommendProduct->id) && ($promotion_discount->{"product_detail_category::product_id"} == $recommendProduct->id))
                {
                    $discount_array["discount_percent"] = $promotion_discount->{"promotion_discount::discount_percent"};
                    $discount_array["discount_baht"] = $promotion_discount->{"promotion_discount::discount_baht"};
                    $discount_array["product_id"] = $recommendProduct->id;
                }
            }

            if(isset($discount_array) && !empty($discount_array))
            {
                $discount_model = new stdClass();
                if($discount_array["discount_percent"] != '' && $discount_array["discount_percent"] != null)
                {
                    $discount_model->discount_percent = $discount_array["discount_percent"];
                    $discount_model->retail_price = $recommendProduct->retail_price;
                    $discount_model->discount_price = number_format(($discount_model->retail_price*$discount_model->discount_percent), 2, '.', '');
                    $discount_model->total_price = ($discount_model->retail_price-$discount_model->discount_price);
                }
                if($discount_array["discount_baht"] != '' && $discount_array["discount_baht"] != null)
                {
                    $discount_model->discount_percent = $discount_array["discount_baht"];
                    $discount_model->retail_price = $recommendProduct->retail_price;
                    $discount_model->discount_price = $discount_array["discount_baht"];
                    $discount_model->total_price = ($recommendProduct->retail_price-$discount_array["discount_baht"]);
                }

                $recommendProduct->discount = $discount_model;
            }
        }
    }


    $model_filter = new stdClass();
    $model_filter->where["status"] = 'ACTIVATE';
    $model_filter->where["is_bestseller"] = '1';

    $bestsellerProducts = $this->product_model->search($model_filter);

    if(count($bestsellerProducts) > 0)
    {
        foreach($bestsellerProducts as $bestsellerProduct)
        {
            $image = array();
            $dir = $this->product_path.$bestsellerProduct->code;
            if (is_dir($dir))
            {
                $product_images = array_diff(scandir($dir), array('.', '..')); 
            }
            if(count($product_images) > 0)
            {
                foreach ($product_images as $product_image) 
                {
                    $image[] = base_url("assets/uploads/product").'/'.$bestsellerProduct->code.'/'.$product_image;
                }
                $bestsellerProduct->images = $image;
            }

            $model_filter = new stdClass();
            $model_filter->where["promotion_discount.status"] = 'ACTIVATE';
            $model_filter->join = array("product_detail_category" => "promotion_discount.product_category_id = product_detail_category.product_category_id");
            $promotion_discounts = $this->promotion_discount_model->search($model_filter);

            $discount_array = array();
            foreach($promotion_discounts as $promotion_discount)
            {
                if(isset($bestsellerProduct->id) && ($promotion_discount->{"product_detail_category::product_id"} == $bestsellerProduct->id))
                {
                    $discount_array["discount_percent"] = $promotion_discount->{"promotion_discount::discount_percent"};
                    $discount_array["discount_baht"] = $promotion_discount->{"promotion_discount::discount_baht"};
                    $discount_array["product_id"] = $bestsellerProduct->id;
                }
            }

            if(isset($discount_array) && !empty($discount_array))
            {
                $discount_model = new stdClass();
                if($discount_array["discount_percent"] != '' && $discount_array["discount_percent"] != null)
                {
                    $discount_model->discount_percent = $discount_array["discount_percent"];
                    $discount_model->retail_price = $bestsellerProduct->retail_price;
                    $discount_model->discount_price = number_format(($discount_model->retail_price*$discount_model->discount_percent), 2, '.', '');
                    $discount_model->total_price = ($discount_model->retail_price-$discount_model->discount_price);
                }
                if($discount_array["discount_baht"] != '' && $discount_array["discount_baht"] != null)
                {
                    $discount_model->discount_percent = $discount_array["discount_baht"];
                    $discount_model->retail_price = $bestsellerProduct->retail_price;
                    $discount_model->discount_price = $discount_array["discount_baht"];
                    $discount_model->total_price = ($bestsellerProduct->retail_price-$discount_array["discount_baht"]);
                }

                $bestsellerProduct->discount = $discount_model;
            }
        }
    }


    $model_filter = new stdClass();
    $model_filter->where["status"] = 'ACTIVATE';
    $model_filter->where["is_new"] = '1';

    $newProducts = $this->product_model->search($model_filter);

    if(count($newProducts) > 0)
    {
        foreach($newProducts as $newProduct)
        {
            $image = array();
            $dir = $this->product_path.$newProduct->code;
            if (is_dir($dir))
            {
                $product_images = array_diff(scandir($dir), array('.', '..')); 
            }
            if(count($product_images) > 0)
            {
                foreach ($product_images as $product_image) 
                {
                    $image[] = base_url("assets/uploads/product").'/'.$newProduct->code.'/'.$product_image;
                }
                $newProduct->images = $image;
            }

            $model_filter = new stdClass();
            $model_filter->where["promotion_discount.status"] = 'ACTIVATE';
            $model_filter->join = array("product_detail_category" => "promotion_discount.product_category_id = product_detail_category.product_category_id");
            $promotion_discounts = $this->promotion_discount_model->search($model_filter);

            $discount_array = array();
            foreach($promotion_discounts as $promotion_discount)
            {
                if(isset($newProduct->id) && ($promotion_discount->{"product_detail_category::product_id"} == $newProduct->id))
                {
                    $discount_array["discount_percent"] = $promotion_discount->{"promotion_discount::discount_percent"};
                    $discount_array["discount_baht"] = $promotion_discount->{"promotion_discount::discount_baht"};
                    $discount_array["product_id"] = $newProduct->id;
                }
            }

            if(isset($discount_array) && !empty($discount_array))
            {
                $discount_model = new stdClass();
                if($discount_array["discount_percent"] != '' && $discount_array["discount_percent"] != null)
                {
                    $discount_model->discount_percent = $discount_array["discount_percent"];
                    $discount_model->retail_price = $newProduct->retail_price;
                    $discount_model->discount_price = number_format(($discount_model->retail_price*$discount_model->discount_percent), 2, '.', '');
                    $discount_model->total_price = ($discount_model->retail_price-$discount_model->discount_price);
                }
                if($discount_array["discount_baht"] != '' && $discount_array["discount_baht"] != null)
                {
                    $discount_model->discount_percent = $discount_array["discount_baht"];
                    $discount_model->retail_price = $newProduct->retail_price;
                    $discount_model->discount_price = $discount_array["discount_baht"];
                    $discount_model->total_price = ($newProduct->retail_price-$discount_array["discount_baht"]);
                }

                $newProduct->discount = $discount_model;
            }
        }
    }


    $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("recommendProducts" => $recommendProducts, "bestsellerProducts" => $bestsellerProducts, "newProducts" => $newProducts));
}

public function article_list($page = '1')
{
    $this->load->library("response_library");

    $this->load->model("article_model");


    $model_filter = new stdClass();
    $model_filter->get_count = true;
    $model_filter->where["status"] = "ACTIVATE";
    $max_article = $this->article_model->search($model_filter);


    $page_max = ceil($max_article/12);

    $page_start = ($page-1)*12;


    $model_filter = new stdClass();
    $model_filter->where["status"] = 'ACTIVATE';
    $model_filter->orderby = 'sort ASC';
    $model_filter->limit = '$page_start , 12';

    $articles = $this->article_model->search($model_filter);



    $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("max_article" => $max_article, "page" => $page, "page_max" => $page_max, "articles" => $articles));
}

public function article_banner()
{
    $this->load->library("response_library");

    $this->load->model("article_banner_model");


    $model_filter = new stdClass();
    $model_filter->where["status"] = 'ACTIVATE';
    $model_filter->orderby = 'sort ASC';
        // $model_filter->limit = '$page_start , 12';

    $article_banners = $this->article_banner_model->search($model_filter);



    $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("article_banners" => $article_banners));
}

public function banner()
{
    $this->load->library("response_library");

    $this->load->model("banner_model");


    $model_filter = new stdClass();
    $model_filter->where["status"] = 'ACTIVATE';
    $model_filter->orderby = 'sort ASC';
        // $model_filter->limit = '$page_start , 12';

    $banners = $this->banner_model->search($model_filter);



    $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("banners" => $banners));
}

public function article($id)
{
    $this->load->library("response_library");

    $this->load->model("article_model");
    $this->load->model("article_product_model");
    $this->load->model("product_model");

    if(isset($id) && $id != '' && $id != null)
    {

        $article = $this->article_model->find($id);

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->where["article_id"] = $id;
        $article_relates = $this->article_product_model->search($model_filter);

        $article_array = array();
        $article_relate_alls = null;

        if(count($article_relates) > 0)
        {
            foreach($article_relates as $article_relate)
            {
                $article_array[] = $article_relate->product_id;
            }

            $model_filter = new stdClass();
            $model_filter->where["product.status"] = 'ACTIVATE';
            $model_filter->where_in["product.id"] = $article_array;
            $model_filter->join = array("product_type" => "product.product_type_id = product_type.id","brand" => "product.brand_id = brand.id");

            $article_relate_alls = $this->product_model->search($model_filter);
            
            if(count($article_relate_alls) > 0)
            {
                foreach($article_relate_alls as $product_relate_all)
                {
                    $image = array();
                    $dir = $this->product_path.$product_relate_all->{"product::code"};
                    if (is_dir($dir))
                    {
                        $product_images = array_diff(scandir($dir), array('.', '..')); 
                    }
                    if(count($product_images) > 0)
                    {
                        foreach ($product_images as $product_image) 
                        {
                            $image[] = base_url("assets/uploads/product").'/'.$product_relate_all->{"product::code"}.'/'.$product_image;
                        }
                        $product_relate_all->{"product::images"} = $image;
                    }
                }
            }

        }

        $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("article" => $article, "article_relate_alls" => $article_relate_alls));
    }
    else
    {
        $this->response_library->responseJSON("1x0000-00000","The sever respone Fails.");   
    }
}

public function business_type()
{
    $this->load->library("response_library");

    $this->load->model("business_type_model");

    $model_filter = new stdClass();
    $model_filter->where["status"] = 'ACTIVATE';

    $business_types = $this->business_type_model->search($model_filter);



    $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("business_types" => $business_types));
}

public function subscribe()
{
    $this->load->library("encryption_library");
    $this->load->library("response_library");

    $this->load->model("subscribe_model");


    if($this->input->post())
    {

        $email = $this->input->post("email");

        $model_filter = new stdClass();
        $model_filter->where["email"] = $email;
        $model_filter->where["status"] = 'ACTIVATE';

        $ck_emails = $this->subscribe_model->search($model_filter);

        if(count($ck_emails) == 0)
        {
            $subscribe = new stdClass();

            $subscribe->email = $email;
            $subscribe->status = 'ACTIVATE';
            $subscribe->created_date = date("Y-m-d h:i:s");
            $subscribe->contact_title_id = $this->input->post("contact_title_id");

            $subscribe_id = $this->subscribe_model->save($subscribe);

            $token = $this->encryption_library->encryptString($email,"unsub");
            $subscribe->token = $token;

                $to = $email; // note the comma

                $subject = 'Thank you for subscribing.';            // Subject

                $message = $this->load->view("EmailTemplate/Subscribe",compact('subscribe'),true); // Message


                $headers[] = 'MIME-Version: 1.0';
                $headers[] = 'Content-type: text/html; charset=utf-8';

                $headers[] = 'From: Nextere <info@nextere.com>';
                mail($to, $subject, $message, implode("\r\n", $headers));

                $this->response_library->responseJSON("0x0000-00000","The sever respone subscribe success.");
            }
            else
            {
                $this->response_library->responseJSON("1x0000-00000","The sever respone email dulpicate.");
            }

        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The sever not respone.");
        }
    }

    public function unSubscribe()
    {
        $this->load->library("response_library");
        $this->load->library("encryption_library");

        $this->load->model("subscribe_model");



        $token = $this->input->get("code");

        $email = $this->encryption_library->decryptString($token,"unsub");


        $model_filter = new stdClass();
        $model_filter->where["email"] = $email;
    // $model_filter->where["status"] = 'CONFRIM';
        $model_filter->get_first = true;

        $subscribe = $this->subscribe_model->search($model_filter);

        if($subscribe != null)
        {
        // $subscribe->password = $this->encryption_library->encryptPassword($password);
            $subscribe->status = 'REMOVED';
            $subscribe->modified_date = date("Y-m-d h:i:s");

            $this->subscribe_model->save($subscribe);

            $this->response_library->responseJSON("0x0000-00000", "The server update Success.");
        // redirect(base_url("login"));
        }
        else
        {
            $this->response_library->responseJSON("0x000A-AC001", "Code incorrect.");
        }
    }

    public function contact_form()
    {
        $this->load->library("response_library");

        $this->load->model("contact_form_model");


        if($this->input->post())
        {
            $insert = new stdClass();

            $insert->email = $this->input->post("email");
            $insert->fullname = $this->input->post("fullname");
            $insert->telephone = $this->input->post("telephone");
            $insert->company = $this->input->post("company");
            $insert->message = $this->input->post("message");
            $insert->is_reply = $this->input->post("is_reply");
            $insert->status = 'ACTIVATE';
            $insert->created_date = date("Y-m-d h:i:s");
            $insert->contact_title_id = $this->input->post("contact_title_id");

            $contact_form = $this->contact_form_model->save($insert);


            $this->response_library->responseJSON("0x0000-00000","The sever respone contact form success.");

        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The sever not respone.");
        }
    }

    public function contact_title()
    {
        $this->load->library("response_library");

        $this->load->model("contact_title_model");

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';

        $contact_titles = $this->contact_title_model->search($model_filter);



        $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("contact_titles" => $contact_titles));
    }

    public function decode($token)
    {
        $this->load->library("response_library");
        $this->load->library("jwt_library");


        $decode = $this->jwt_library->decode($token,"smartclickss_encryp_drm");

        if(isset($decode))
        {
            if($decode->date >= date("Ymdhis"))
            {
                echo "exp";
            }
            else
            {
                print_r($decode);

            }
        }
        else
        {
            echo "error";
        }
        exit();
    }

    public function contactus()
    {
        $this->load->library("response_library");

        $this->load->model("contactus_model");

        $model_filter = new stdClass();
    // $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->get_first = true;
    // $model_filter->order_by = 'order ASC';

        $contactus = $this->contactus_model->search($model_filter);

        $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("contactus" => $contactus));
    }

    public function aboutus_banner()
    {
        $this->load->library("response_library");

        $this->load->model("aboutus_banner_model");

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
    // $model_filter->get_first = true;
        $model_filter->order_by = 'order ASC';

        $aboutus_banner = $this->aboutus_banner_model->search($model_filter);

        $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("aboutus_banner" => $aboutus_banner));
    }

    public function whyus_list()
    {
        $this->load->library("response_library");

        $this->load->model("whyus_model");

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
    // $model_filter->get_first = true;
        $model_filter->order_by = 'order ASC';

        $whyus_list = $this->whyus_model->search($model_filter);

        $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("whyus_list" => $whyus_list));
    }

    public function whyus($id)
    {
        $this->load->library("response_library");

        $this->load->model("whyus_model");

        if(isset($id) && $id != '' && $id != null)
        {
            $whyus = $this->whyus_model->find($id);

            $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("whyus" => $whyus));
        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The sever respone Fails.");   
        }
    }

    public function news_list()
    {
        $this->load->library("response_library");

        $this->load->model("news_model");

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
    // $model_filter->get_first = true;
        $model_filter->order_by = 'id DESC';

        $news_list = $this->news_model->search($model_filter);

        $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("news_list" => $news_list));
    }

    public function news($id)
    {
        $this->load->library("response_library");

        $this->load->model("news_model");

        if(isset($id) && $id != '' && $id != null)
        {
            $news = $this->news_model->find($id);

            $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("news" => $news));
        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The sever respone Fails.");   
        }
    }

    public function conditions()
    {
        $this->load->library("response_library");

        $this->load->model("conditions_model");

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->order_by = 'order ASC';

        $conditions = $this->conditions_model->search($model_filter);

        $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("conditions" => $conditions));
    }

    public function partner()
    {
        $this->load->library("response_library");

        $this->load->model("partner_model");

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->order_by = 'order ASC';

        $partners = $this->partner_model->search($model_filter);

        $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("partners" => $partners));

    }

    public function select2($function,$id=null)
    {
        $this->load->library("response_library");

        if($function == "search_product")
        {
            $this->load->model("product_model");
            $model_filter = new stdClass();
            $model_filter->where["status"] = "ACTIVATE";
        // $model_filter->where["id >="] = $this->session->userdata("__administrator_group::id");
            if($this->input->get("search") != "")
            {
                // $model_filter->like["name_en"] = $this->input->get("search");
                $model_filter->custom_where = "search_keyword like '%".$this->input->get("search")."%' OR name_en like '%".$this->input->get("search")."%'";
                // $model_filter->get_query = true;
            }
            $product = $this->product_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $product);
        }


        if($function == "address_province")
        {
            $this->load->model("address_province_model");
            $model_filter = new stdClass();
        // $model_filter->where["status"] = "ACTIVATE";
        // $model_filter->where["id >="] = $this->session->userdata("__administrator_group::id");
            if($this->input->get("search") != "")
            {
                $model_filter->like["name"] = $this->input->get("search");
            }
            $address_province = $this->address_province_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $address_province);
        }


        if($function == "address_district")
        {
            $this->load->model("address_district_model");
            $model_filter = new stdClass();
        // $model_filter->where["status"] = "ACTIVATE";
            $model_filter->where["address_province_id"] = $id;
            if($this->input->get("search") != "")
            {
                $model_filter->like["name"] = $this->input->get("search");
            }
            $address_district = $this->address_district_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $address_district);
        }


        if($function == "address_sub_district")
        {
            $this->load->model("address_sub_district_model");
            $model_filter = new stdClass();
        // $model_filter->where["status"] = "ACTIVATE";
            $model_filter->where["address_district_id"] = $id;
            if($this->input->get("search") != "")
            {
                $model_filter->like["name"] = $this->input->get("search");
            }
            $address_sub_district = $this->address_sub_district_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $address_sub_district);
        }

        if($function == "article")
        {
            $this->load->model("article_model");
            $model_filter = new stdClass();
            if($this->input->get("search") != "")
            {
                $model_filter->like["title_en"] = $this->input->get("search");
            }
            $article = $this->article_model->search($model_filter);
            $this->response_library->responseJSON("0x0000-00000", "Select2 data generated complete.", $article);
        }
    }
    public function filter_category($id = null)
    {
        $this->load->library("response_library");


        $this->load->model("filter_category_model");

        $model_filter = new stdClass();
        $model_filter->select = 'technical_spec.id,technical_spec.name_th,technical_spec.name_en';
        $model_filter->where["filter_category.status"] = 'ACTIVATE';
        $model_filter->where["filter_category.product_sub_category_id"] = $id;
        $model_filter->join  = array(
            "product_sub_category" => "filter_category.product_sub_category_id = product_sub_category.id",
            "technical_spec" => "filter_category.technical_spec_id = technical_spec.id"
        );

        $filter_category = $this->filter_category_model->search($model_filter);

        $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("filter_category" => $filter_category));

    }
    public function filter_solution($id = null)
    {
        $this->load->library("response_library");


        $this->load->model("filter_solution_model");

        $model_filter = new stdClass();
        $model_filter->select = 'technical_spec.id,technical_spec.name_th,technical_spec.name_en';
        $model_filter->where["filter_solution.status"] = 'ACTIVATE';
        $model_filter->where["filter_solution.child_submenu_category_id"] = $id;
        $model_filter->join  = array(
            "child_submenu_category" => "filter_solution.child_submenu_category_id = child_submenu_category.id",
            "technical_spec" => "filter_solution.technical_spec_id = technical_spec.id"
        );

        $filter_solution = $this->filter_solution_model->search($model_filter);

        $this->response_library->responseJSON("0x0000-00000","The sever respone success.", array("filter_solution" => $filter_solution));

    }
}