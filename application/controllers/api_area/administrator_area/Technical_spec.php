<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Technical_spec extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

    public function export_form()
    {
        $this->load->library('excel');
        $this->load->model("technical_spec_model");
        $this->load->model("product_model");

        $model_filter = new stdClass();
        $model_filter->where["status"] = "ACTIVATE";
        $technical_specs = $this->technical_spec_model->search($model_filter);

        $model_filter = new stdClass();
        $model_filter->where["status"] = "ACTIVATE";
        $model_filter->order_by = "product_type_id asc, name_en asc, model_name_en asc";
        $products = $this->product_model->search($model_filter);


        $filename = "TechnicalSpecForm".date("Y_m_d");
        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);
        $row = 1;
        $column = 1;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Product Id");
        if($products != null && count($products)> 0){
            foreach ($products as $item) { 
                $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, $item->id);
            }
        }
        $row++;
        $column = 0;
        $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, "Technical spec Id");
        $column++;
        if($products != null && count($products)> 0){
            foreach ($products as $item) { 
                $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, ($item->name_en.(empty($item->model_name_en)? '': '/'.$item->model_name_en)));
            }
        }


        if($technical_specs != null && count($technical_specs) > 0) {
            foreach ($technical_specs as $tech) {
                $row++;
                $column = 0;
                $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, $tech->id);
                $object->getActiveSheet()->setCellValueByColumnAndRow($column++,$row, $tech->name_en);
       
            }
        } 



        ob_end_clean(); 
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header("Content-Disposition: attachment; filename=\"$filename.xls\"");
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel5');
        $objWriter->save('php://output');
    }

	public function export_forms()
    {
        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->model("technical_spec_model");
        $this->load->model("product_model");
        $this->load->library('excel');

        $model_filter = new stdClass();
        $model_filter->where["status"] = "ACTIVATE";
        $technical_specs = $this->technical_spec_model->search($model_filter);

        $model_filter = new stdClass();
        $model_filter->where["status"] = "ACTIVATE";
        $model_filter->order_by = "product_type_id asc, name_en asc, model_name_en asc";
        $products = $this->product_model->search($model_filter);

        $filename = "TechnicalSpecForm".date("Y_m_d").".csv";
 		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Type: text/csv; charset=utf-8");
		header('Content-Disposition: attachment; filename='.$filename);
		$content = ",Product Id";
        if($products != null && count($products)> 0){
            foreach ($products as $item) { 
               $content .= ",".$item->id;
            }
        }
        $content .= "\n";

        $content .= "Technical spec Id,";
        if($products != null && count($products)> 0){
            foreach ($products as $item) { 
            $content .= ",".($item->name_en.(empty($item->model_name_en)? '': '/'.$item->model_name_en));
          
            }
        }
        $content .= "\n";

        if($technical_specs != null && count($technical_specs) > 0) {
          	foreach ($technical_specs as $tech) {
	            $content .= $tech->id.",".$tech->name_en;
	              	for ($i=0; $i < count($products); $i++) { 
		                $content .= ",";
		              }
	            $content .= "\n";
       
          	}
        } 


		echo $content;
  
    }

    
}
?>