 <?php
 defined('BASEPATH') OR exit('No direct script access allowed');

 class Payment extends CI_Controller
 {
    function __construct()
    {
        parent::__construct();
    }

    public function CreditCardPayment()
    {
        $this->load->library("response_library");
        $this->load->library("encryption_library");
        $this->load->library("mpay_library");

        $this->load->model("purchase_transaction_model");
        $this->load->model("purchase_transaction_detail_model");

        if($this->input->post())
        {
            // $model_filter = new stdClass();
            // $model_filter->
            // $this->purchase_transaction_model->search($model_filter);

            $member_id = $this->input->post("member_id");
            $order_id = date("YmdHis");
            $cust_id = "C".time();
            $net_price = $this->input->post("net_price");
            $cust_id = $cust_id;

            // print_r($order_id);
            // exit;


            $purchase_transaction = new stdClass();
            $purchase_transaction->member_id = $member_id;
            $purchase_transaction->member_address_id = $this->input->post("member_address_id");
            $purchase_transaction->member_address_tax_id = $this->input->post("member_address_tax_id");
            $purchase_transaction->courier_name = $this->input->post("courier_name");
            $purchase_transaction->courier_code = $this->input->post("courier_code");

            $purchase_transaction->order_id = $order_id;
            $purchase_transaction->payment_id = $this->input->post("payment_id");
            $purchase_transaction->promotion_discount_id = $this->input->post("promotion_discount_id");
            $purchase_transaction->coupon_id = $this->input->post("coupon_id");

            // $purchase_transaction->purchase_transaction_no = $purchase_transaction_no;
            $purchase_transaction->channel_type = "WEBSITE";
            $purchase_transaction->status = "ACTIVATE";

            $purchase_transaction->product_price = $this->input->post("product_price");
            $purchase_transaction->delivery_price = $this->input->post("delivery_price");
            $purchase_transaction->discount = $this->input->post("discount");
            $purchase_transaction->net_price = $this->input->post("net_price");

            $purchase_transaction->created_date = date("Y-m-d h:i:s");
            $purchase_transaction->created_by = $this->input->post("member_id");

            $purchase_transaction_id = $this->purchase_transaction_model->save($purchase_transaction);

            $product_ids = $this->input->post("product_id");
            $product_retail_prices = $this->input->post("product_retail_price");
            $product_qtys = $this->input->post("product_qty");
            $product_total_prices = $this->input->post("product_total_price");

            $array_product_meta = array();
            if(isset($purchase_transaction_id))
            {

                for ($i=0; $i < count($product_ids); $i++) 
                { 
                    $purchase_transaction_detail = new stdClass();
                    $purchase_transaction_detail->purchase_transaction_id = $purchase_transaction_id;
                    $purchase_transaction_detail->product_id = $product_ids[$i];
                    $purchase_transaction_detail->price = $product_retail_prices[$i];
                    $purchase_transaction_detail->qty = $product_qtys[$i];
                    $purchase_transaction_detail->total_price = $product_total_prices[$i];

                    $purchase_transaction_detail->status = 'ACTIVATE';
                    $purchase_transaction_detail->created_date = date("Y-m-d h:i:s");
                    $purchase_transaction_detail->created_by = $this->input->post("member_id");

                    $purchase_transaction_detail_id = $this->purchase_transaction_detail_model->save($purchase_transaction_detail);

                    // $array_product_meta[] = array(
                    //     "purchase_transaction_id" => $purchase_transaction_id,
                    //     "product_id" => $product_ids[$i],
                    //     "price" => $product_retail_prices[$i],
                    //     "qty" => $product_qtys[$i],
                    //     "total_price" => $product_total_prices[$i]
                    // );
                }
            }

            // print_r($purchase_transaction);
            // exit();


            $requestBody = array(
                "order_id" => $order_id,
                "product_name" => "Nextere Payment",
                "service_id" => $this->config->item('MPAY_SERVICE_ID'),
                "channel_type" => "WEBSITE",
                "cust_id" => $cust_id,
                "amount" => $net_price,
                "currency" => "THB",
                "ref_1" => "Reference1",
                "form_type" => "FORM",
                // "meta" => array("123" => "123","234" => "234"),
                "is_remember" => false,
                "3ds" => array(
                    "3ds_required" => true,
                    "3ds_url_success" => base_url("Product/Bill"),
                    "3ds_url_fail" => base_url("Product/Bill_fail")
                )
            );
            
            // if(!empty($response->error_code))
            // {

                $requestBodyJson = json_encode($requestBody);
                $response = $this->mpay_library->PaymentCreditCard($requestBodyJson);
                // print_r($response);
                // exit();

                $purchase_transaction = $this->purchase_transaction_model->find($purchase_transaction_id);

                $purchase_transaction->txn_id = $response->txn_id;
                $purchase_transaction->txn_token = $response->txn_token;
                $purchase_transaction->form_url = $response->form_url;

                $this->purchase_transaction_model->save($purchase_transaction);

                $this->response_library->responseJSON("0x0000-00000", "Server response success.", array("response" => $response));
            // }
            // else
            // {
            //     $this->response_library->responseJSON("1x0000-00000", "Server response fail.", array("response" => $response == null ? null : $response));
            // }
        }
    }


    public function InternetbankingPayment()
    {
        $this->load->library("response_library");
        $this->load->library("encryption_library");
        $this->load->library("mpay_library");

        $this->load->model("purchase_transaction_model");
        $this->load->model("purchase_transaction_detail_model");

        if($this->input->post())
        {
            $member_id = $this->input->post("member_id");
            $purchase_transaction_no = time();
            $cust_id = "C".time();
            $net_price = $this->input->post("net_price");
            $bank_code = $this->input->post("bank_code");
            $cust_id = $cust_id;


            $purchase_transaction = new stdClass();
            $purchase_transaction->member_id = $member_id;
            $purchase_transaction->member_address_id = $this->input->post("member_address_id");
            $purchase_transaction->member_address_tax_id = $this->input->post("member_address_tax_id");
            $purchase_transaction->courier_name = $this->input->post("courier_name");
            $purchase_transaction->courier_code = $this->input->post("courier_code");

            $purchase_transaction->payment_id = $this->input->post("payment_id");
            $purchase_transaction->promotion_discount_id = $this->input->post("promotion_discount_id");
            $purchase_transaction->coupon_id = $this->input->post("coupon_id");

            $purchase_transaction->purchase_transaction_no = $purchase_transaction_no;
            $purchase_transaction->channel_type = "WEBSITE";
            $purchase_transaction->status = "ACTIVATE";

            $purchase_transaction->product_price = $this->input->post("product_price");
            $purchase_transaction->delivery_price = $this->input->post("delivery_price");
            $purchase_transaction->discount = $this->input->post("discount");
            $purchase_transaction->net_price = $this->input->post("net_price");

            $purchase_transaction->created_date = date("Y-m-d h:i:s");
            $purchase_transaction->created_by = $this->input->post("member_id");

            $purchase_transaction_id = $this->purchase_transaction_model->save($purchase_transaction);

            $product_ids = $this->input->post("product_id");
            $product_retail_prices = $this->input->post("product_retail_price");
            $product_qtys = $this->input->post("product_qty");
            $product_total_prices = $this->input->post("product_total_price");

            $array_product_meta = array();
            if(isset($purchase_transaction_id))
            {

                for ($i=0; $i < count($product_ids); $i++) 
                { 
                    $purchase_transaction_detail = new stdClass();
                    $purchase_transaction_detail->purchase_transaction_id = $purchase_transaction_id;
                    $purchase_transaction_detail->product_id = $product_ids[$i];
                    $purchase_transaction_detail->price = $product_retail_prices[$i];
                    $purchase_transaction_detail->qty = $product_qtys[$i];
                    $purchase_transaction_detail->total_price = $product_total_prices[$i];

                    $purchase_transaction_detail->status = 'ACTIVATE';
                    $purchase_transaction_detail->created_date = date("Y-m-d h:i:s");
                    $purchase_transaction_detail->created_by = $this->input->post("member_id");

                    $purchase_transaction_detail_id = $this->purchase_transaction_detail_model->save($purchase_transaction_detail);

                    // $array_product_meta[] = array(
                    //     "purchase_transaction_id" => $purchase_transaction_id,
                    //     "product_id" => $product_ids[$i],
                    //     "price" => $product_retail_prices[$i],
                    //     "qty" => $product_qtys[$i],
                    //     "total_price" => $product_total_prices[$i]
                    // );
                }
            }

            $requestBody = array(
                "order_id" => $purchase_transaction_no,
                "product_name" => "Nextere Payment",
                "bank_code" => $bank_code, //002 = BBL ,014 = SCB 
                "service_id" => $this->config->item('MPAY_SERVICE_ID'),
                "channel_type" => "WEBSITE",
                "cust_id" => $cust_id,
                "amount" => $net_price,
                "currency" => "THB",
                "ref_1" => "Reference1",
                "redirect_urls" => array(
                    "url_success" => base_url("Product/Bill"),
                    "url_fail" => base_url("Product/Bill_fail")
                )
            );

            $requestBodyJson = json_encode($requestBody);
            $response = $this->mpay_library->PaymentInternetBanking($requestBodyJson);

            $this->response_library->responseJSON("0x0000-00000", "Server response success.", array("response" => $response));
        }
    }

    public function image_qr64()
    {
        echo '<img alt="Embedded Image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADIAQAAAACFI5MzAAADhElEQVR42u2YPY6sOhCFq+WADDaA5G2QeUuwAWg2AFty5m0geQOQObCod6pN99wbvIDqJ71kWqMWwzfCpn5OHQ/xv33ol/znJBNZ9mbldgpmcbl3ZieatGTidqxy38Vnxwf+xMdDbipJX9mD7Zzy5Gnsck/m8O0XBPvlI2VyvAbDKX5H7EJmqdpevrHlrwhi0Hc8+21gKzHA9090bhOidgh//fSfzN0m8jnJ7JWdmWqHYGDLn6q6SzJVRMg2xSfZNfCzi0sXtYRXL+ugEmtn1sRI+1iVXStIbhLquh3YLF0efH6gjtiyloy0kTMzMhy2Wrafm6AnvaPJt31lns6idmaO5xUdDXlwXDlKl7BZwzbIRYmBhkAAjgQSOUkHL46oM6wkSC/vkhnsHbVTYlAypyB5YOrdq3Y6ewSD7dddqUQF4b2Ka5LaGdHKbuurbXRlBxqyehoCNCAPEgO7otKvdRREJAqdceCXKwbb5I2WyCOxzg5VdtAYI7XjrJqcbnuk3CAG6Dxs/1M7KnJ45Af1ghtF+czVcxqSh4BiabHUFMCx5W161859wnjw6VqpHbw6tMqhiDYtgfLF5UerkOqPVikIPwkfESrMn52KrJboaAgHCJ6Rb9F4DBDEwGgJ9ogR0dYUDwhegEq1g49awmeVRyc+AsK8ykvADlgtwTapCXh788SQ9GZHHVVRTQYZGrjc+m6TGFSQ/LdW3SY8F/dEkbFZvL0Yn6IUGnLCgoUoWuWsaLz/aJWGcLInkpPi4i6HcrrLH9wnyPbWJGpSfsUAYw2NUtyThsjtYLEOB4nBnJAlZiV51TVHcXaYtCii9BOD+wS7truzcHY1LtB8DiHZtIQXxABKXNGQEAPMInoUT6EisGCcRKvQc/BiC5JzxUBDkNv9pVVjB2ssel+/q+o+gdTBEWcJpxwCippeHawgeHW07LNrCbUj+oeeUxM5PewkXkwe//IUb61SEPEjSyc3UDKNLIs7JQYaMiR5+x1ceg6eAum6nJCCEJqMsI5oFbk/o6Mg8kiSQMZdDp3okpaIvyAyx1A7A7co8L3L77mtIHJmarwY4SEUrcKx6T0x7hMxJjL8P7MRhwA9wcm1xsSWMNg5iMYTxW9IA8vD9PCwFSLP7O03pBYvtqGDpZVDW3d6AguAFR4JNorGCtogWqgmCOEoB3QclXCbD/HIUUt+/8P0/5N/AJwIX+brYaEYAAAAAElFTkSuQmCC" />';
    }

    public function qrPayment()
    {
        $this->load->library("response_library");
        $this->load->library("encryption_library");
        $this->load->library("mpay_library");

        $this->load->model("purchase_transaction_model");
        $this->load->model("purchase_transaction_detail_model");

        if($this->input->post())
        {
            $member_id = $this->input->post("member_id");
            $purchase_transaction_no = time();
            $cust_id = "C".time();
            $net_price = $this->input->post("net_price");
            $bank_code = $this->input->post("bank_code");
            $cust_id = $cust_id;


            $purchase_transaction = new stdClass();
            $purchase_transaction->member_id = $member_id;
            $purchase_transaction->member_address_id = $this->input->post("member_address_id");
            $purchase_transaction->member_address_tax_id = $this->input->post("member_address_tax_id");
            $purchase_transaction->courier_name = $this->input->post("courier_name");
            $purchase_transaction->courier_code = $this->input->post("courier_code");


            $purchase_transaction->payment_id = $this->input->post("payment_id");
            $purchase_transaction->promotion_discount_id = $this->input->post("promotion_discount_id");
            $purchase_transaction->coupon_id = $this->input->post("coupon_id");

            $purchase_transaction->purchase_transaction_no = $purchase_transaction_no;
            $purchase_transaction->channel_type = "WEBSITE";
            $purchase_transaction->status = "ACTIVATE";

            $purchase_transaction->product_price = $this->input->post("product_price");
            $purchase_transaction->delivery_price = $this->input->post("delivery_price");
            $purchase_transaction->discount = $this->input->post("discount");
            $purchase_transaction->net_price = $this->input->post("net_price");

            $purchase_transaction->created_date = date("Y-m-d h:i:s");
            $purchase_transaction->created_by = $this->input->post("member_id");

            $purchase_transaction_id = $this->purchase_transaction_model->save($purchase_transaction);

            $product_ids = $this->input->post("product_id");
            $product_retail_prices = $this->input->post("product_retail_price");
            $product_qtys = $this->input->post("product_qty");
            $product_total_prices = $this->input->post("product_total_price");

            $array_product_meta = array();
            if(isset($purchase_transaction_id))
            {

                for ($i=0; $i < count($product_ids); $i++) 
                { 
                    $purchase_transaction_detail = new stdClass();
                    $purchase_transaction_detail->purchase_transaction_id = $purchase_transaction_id;
                    $purchase_transaction_detail->product_id = $product_ids[$i];
                    $purchase_transaction_detail->price = $product_retail_prices[$i];
                    $purchase_transaction_detail->qty = $product_qtys[$i];
                    $purchase_transaction_detail->total_price = $product_total_prices[$i];

                    $purchase_transaction_detail->status = 'ACTIVATE';
                    $purchase_transaction_detail->created_date = date("Y-m-d h:i:s");
                    $purchase_transaction_detail->created_by = $this->input->post("member_id");

                    $purchase_transaction_detail_id = $this->purchase_transaction_detail_model->save($purchase_transaction_detail);

                    // $array_product_meta[] = array(
                    //     "purchase_transaction_id" => $purchase_transaction_id,
                    //     "product_id" => $product_ids[$i],
                    //     "price" => $product_retail_prices[$i],
                    //     "qty" => $product_qtys[$i],
                    //     "total_price" => $product_total_prices[$i]
                    // );
                }
            }

            $requestBody = array(
                "order_id" => $purchase_transaction_no,
                "product_name" => "Nextere Payment",
                "sof" => "PROMPTPAY",
                "service_id" => $this->config->item('MPAY_SERVICE_ID'),
                "location_name" => "Nextere Payment",
                "amount" => $net_price,
                "currency" => "THB",
                "expire_time_seconds" => 600, 
                "ref_1" => "Reference1"
                
            );

            $requestBodyJson = json_encode($requestBody);
            $response = $this->mpay_library->PaymentQR($requestBodyJson);
            
            $this->response_library->responseJSON("0x0000-00000", "Server response success.", array("response" => $response));
        }
        // }
        // $this->load->library("mpay_library");

        // $requestBody = array(
        //     "order_id" => time(),
        //     "product_name" => "Test Payment",
        //     "sof" => "PROMPTPAY",
        //     "service_id" => $this->config->item('MPAY_SERVICE_ID'),
        //     // "terminal_id" => "19100061935189003", 
        //     "location_name" => "Shop-01", 
        //     "amount" => 1010.00, 
        //     "currency" => "THB", 
        //     "expire_time_seconds" => 600, 
        //     "ref_1" => "reference1"
        // );
        

        // $requestBodyJson = json_encode($requestBody);
        // $response = $this->mpay_library->PaymentQR($requestBodyJson);
        // print_r($response);
    }

    public function test_rabbit()
    {
        $this->load->library("mpay_library");

        $requestBody = array(
            "amount" => 100.00,
            "currency" => "THB", 
            "product_name" => "Test Payment",
            "order_id" => time(),
            // "cust_id" => "C091818111",
            "is_pre_approve" => false, 
            "service_id" => $this->config->item('MPAY_SERVICE_ID'),
            "channel_type" => "WEBSITE ", 
            "ref_1" => "some text", 
            "ref_2" => "some text", 
            "ref_3" => "some text", 
            "ref_4" => "some text", 
            "ref_5" => "some text", 
            "metadata" => array(
                "key1" => "value1", 
                "key2" => "value2" 
            ),
            "redirect_urls" => array( 
                "confirm_url" => "https://pay-store.line.com/order/payment/authorize", 
                "cancel_url" => "https://pay-store.line.com/order/payment/cancel" 
            )
        );
        

        $requestBodyJson = json_encode($requestBody);
        $response = $this->mpay_library->PaymentRabbitLinePay($requestBodyJson);
        print_r($response);
    }

    public function test_alipay()
    {
        //ไม่ได้ respone กลับ
        $this->load->library("mpay_library");

        $requestBody = array(
            "amount" => 1,
            "currency" => "THB", 
            "product_name" => "Test Payment",
            "order_id" => time(),
            "cust_id" => "C091818111",
            "service_id" => $this->config->item('MPAY_SERVICE_ID'),
            "channel_type" => "WEBSITE", 
            "ref_1" => "some text", 
            "ref_2" => "some text", 
            "ref_3" => "some text",
            "metadata" => array(
                "key1" => "value1", 
                "key2" => "value2" 
            ), 
          "redirect_url" => array( 
            "mch_redirect_url" => "https://success.com", 
            "mch_redirect_url_fail" => "http://fail.com" 
          )
        );

        $requestBodyJson = json_encode($requestBody);
        $response = $this->mpay_library->PaymentAlipay($requestBodyJson);
        print_r($response);
    }

    public function paidresult()
    {
        $this->load->library("response_library");
        $inputJSON = file_get_contents('php://input');
        // $input = json_decode($inputJSON, TRUE); //convert JSON into array
        // $json = json_encode($inputJSON, TRUE); //convert JSON into array
        var_dump($this->input->get());
        print_r(file_get_contents('php://input'));
        exit();
        echo "string";
        print_r($inputJSON);

        $input = $this->input->post();
        print_r($input);
        $inputObj = json_decode($inputJSON, TRUE);

        writelog_paymentCredit($inputObj);
        writelog_paymentCredit($inputJSON);
        // echo "<pre>";
        // print_r($inputObj);
        // echo "<hr>";
        print_r($inputObj);
        exit();
        // exit;

        // $order_id = $inputObj->order_id;
        // $merchant_id = $this->input->post("merchant_id");
        // $transaction_id = $this->input->post("txn_id");
        // $status = $this->input->post("status");
        // $status_code = $this->input->post("status_code");
        // $status_message = $this->input->post("status_message");
        // $amount = $this->input->post("amount");
        // $amount_net = $this->input->post('amount_net');
        // $amount_cust_fee = $this->input->post('amount_cust_fee');
        // $currency = $this->input->post('currency');
        // $service_id = $this->input->post('service_id');
        // $channel_type = $this->input->post('channel_type');
        // $ref_1 = $this->input->post('ref_1');
        // $ref_2 = $this->input->post('ref_2');
    }

    public function Confirm()
    {
        $this->load->library("response_library");

        $this->load->model("cart_model");
        $this->load->model("member_model");
        $this->load->model("purchase_transaction_model");
        $this->load->model("purchase_transaction_detail_model");

        $inputJSON = file_get_contents('php://input');
        $inputObj = json_decode($inputJSON, TRUE);

        writelog_paymentCredit(date("Y-m-d H:i:s")."||hook||respone=".$inputJSON);
        if(isset($inputJSON))
        {
            $status = $inputObj["status"];
            if($status == "FAIL")
            {
                $txn_id = $inputObj["txn_id"];
                $txn_type = $inputObj["txn_type"];
                $status = $inputObj["status"];
                $status_code = $inputObj["status_code"];
                $status_message = $inputObj["status_message"];
                $order_id = $inputObj["order_id"];
                $cust_id = $inputObj["cust_id"];
                $service_id = $inputObj["service_id"];
                $channel_type = $inputObj["channel_type"];
                $amount = $inputObj["amount"];
                $currency = $inputObj["currency"];
                $amount_net = $inputObj["amount_net"];
                $ref_1 = $inputObj["ref_1"];
                $created_at = $inputObj["created_at"];
                $fail_at = $inputObj["fail_at"];
                $sof_type = $inputObj["sof_type"];

                $model_filter = new stdClass();
                $model_filter->where["txn_id"] = $txn_id;
                $model_filter->where["status"] = "ACTIVATE";
                $model_filter->get_first = true;
                $purchase_transaction = $this->purchase_transaction_model->search($model_filter);

                if(isset($purchase_transaction))
                {
                    $purchase_transaction->status = $status;
                    // $purchase_transaction->modified_by = $member_id;
                    $purchase_transaction->modified_date = date("Y-m-d h:i:s");

                    $purchase_transaction_id = $this->purchase_transaction_model->save($purchase_transaction);

                    if(isset($purchase_transaction_id))
                    {
                        $update_filter = new stdClass();
                        $update_filter->where["purchase_transaction_id"] = $purchase_transaction_id;

                        $update_data = array(
                            "status" => "FAIL",
                            "modified_date" => date("Y-m-d h:i:s")
                        );
                        $this->purchase_transaction_detail_model->update($update_filter, $update_data);
                    }
                }

                // print_r($purchase_transaction);

            }
            if($status == "SUCCESS")
            {
                $txn_id = $inputObj["txn_id"];
                $txn_type = $inputObj["txn_type"];
                $status = $inputObj["status"];
                $status_code = $inputObj["status_code"];
                $status_message = $inputObj["status_message"];
                $order_id = $inputObj["order_id"];
                $cust_id = $inputObj["cust_id"];
                $service_id = $inputObj["service_id"];
                $channel_type = $inputObj["channel_type"];
                $amount = $inputObj["amount"];
                $currency = $inputObj["currency"];
                $amount_net = $inputObj["amount_net"];
                $ref_1 = $inputObj["ref_1"];
                $sof_txn_id = $inputObj["sof_txn_id"];
                $created_at = $inputObj["created_at"];
                $success_at = $inputObj["success_at"];
                $sof_type = $inputObj["sof_type"];

                $model_filter = new stdClass();
                $model_filter->where["txn_id"] = $txn_id;
                $model_filter->where["status"] = "ACTIVATE";
                $model_filter->get_first = true;
                $purchase_transaction = $this->purchase_transaction_model->search($model_filter);

                if(isset($purchase_transaction))
                {
                    $ptn = new stdClass();
                    $ptn->order_by = 'purchase_transaction_no desc';
                    $ptn->get_first = true;
                    $ptn_last = $this->purchase_transaction_model->search($ptn);

                    $current_m = date("Ym");

                    $ptn_current_m = substr($ptn_last->purchase_transaction_no,3,6);
                    $ptn_current_no = substr($ptn_last->purchase_transaction_no,9);


                    if($current_m === $ptn_current_m && $purchase_transaction->id != $ptn_last->id)
                    {
                        $variable=$ptn_current_no+1;
                        $no = 'NQT'.$current_m.sprintf("%'04d", $variable);
                        // echo $no;

                        $purchase_transaction->purchase_transaction_no = $no;
                    }
                    else
                    {
                        $variable=1;
                        $no = 'NQT'.$current_m.sprintf("%'04d", $variable);
                        // echo $no;

                        $purchase_transaction->purchase_transaction_no = $no;
                    }
                    // exit;


                    $purchase_transaction->status = $status;
                    // $purchase_transaction->modified_by = $member_id;
                    $purchase_transaction->modified_date = date("Y-m-d h:i:s");

                    $purchase_transaction_id = $this->purchase_transaction_model->save($purchase_transaction);

                    if(isset($purchase_transaction_id))
                    {
                        $update_filter = new stdClass();
                        $update_filter->where["purchase_transaction_id"] = $purchase_transaction_id;

                        $update_data = array(
                            "status" => "PAID",
                            "modified_date" => date("Y-m-d h:i:s")
                        );
                        $this->purchase_transaction_detail_model->update($update_filter, $update_data);

                        $model_filter = new stdClass();
                        $model_filter->where["purchase_transaction_id"] = $purchase_transaction_id;

                        $purchase_transaction_details = $this->purchase_transaction_detail_model->search($model_filter);

                        $product_array = array();

                        foreach($purchase_transaction_details as $purchase_transaction_detail)
                        {
                            $product_array[] = $purchase_transaction_detail->product_id;
                        }

                        if(count($product_array) > 0)
                        {
                            
                            $delete_filter = new stdClass();
                            $delete_filter->where_in["product_id"] = $product_array;
                            $delete_filter->where["member_id"] = $purchase_transaction->created_by;

                            $this->cart_model->delete($delete_filter);
                        }

                        $member = $this->member_model->find($purchase_transaction->created_by);

                //         $to = $member->email; // note the comma

                //         // print_r($member->email);exit;


                // // Subject
                //         $subject = 'Thank you, Purchase Transaction.';

                //         // $token = $this->encryption_library->encryptString($email,"token");
                //         // $member->token = $token;


                // // Message
                //         $message = $this->load->view("EmailTemplate/SuccesPayment",true);

                //         // print_r($message);
                //         // exit();

                // // To send HTML mail, the Content-type header must be set
                //         $headers[] = 'MIME-Version: 1.0';
                //         $headers[] = 'Content-type: text/html; charset=utf-8';

                // // Additional headers
                //         // $headers[] = 'To: Mary <mary@example.com>, Kelly <kelly@example.com>';
                //         $headers[] = 'From: Nextere <sale@nextere.co.th>';
                //         // $headers[] = 'Cc: birthdayarchive@example.com';
                //         // $headers[] = 'Bcc: birthdaycheck@example.com';

                // // Mail it
                //         mail($to, $subject, $message, implode("\r\n", $headers));
                    }
                }

                // print_r($purchase_transaction);
            }
        }
    }
}