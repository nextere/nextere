 <?php
 defined('BASEPATH') OR exit('No direct script access allowed');

 class Product extends CI_Controller
 {
    function __construct()
    {
        parent::__construct();
    }

    private $product_path = FCPATH.'/assets/uploads/product/';

    public function product_category_all()
    {
        $this->load->library("response_library");

        $this->load->model("product_category_model");
        $this->load->model("product_sub_category_model");
        // $this->load->model("child_submenu_category_model");

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->order = "order ASC";

        $product_categories = $this->product_category_model->search($model_filter);

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->order = "order ASC";

        $product_sub_categories = $this->product_sub_category_model->search($model_filter);

        foreach ($product_categories as $key => $product_category) {
            foreach ($product_sub_categories as $key => $product_sub_category) {
                if($product_sub_category->product_category_id == $product_category->id)
                {
                    $product_category->product_sub_categories[] = $product_sub_category;
                }
            }
        }


        $this->response_library->responseJSON("0x0000-00000","The server respone success.", array("product_categories" => $product_categories));
    }

    public function product_compare()
    {
        $this->load->library("response_library");

        $this->load->model("promotion_discount_model");

        $this->load->model("product_model");
        $this->load->model("product_detail_category_model");
        $this->load->model("technical_spec_category_model");
        $this->load->model("technical_spec_model");
        $this->load->model("product_technical_spec_model");

        if($this->input->post())
        {
            $array_id = array();
            $product1 = $this->input->post("product1");
            $product2 = $this->input->post("product2");
            $product3 = $this->input->post("product3");

            if(isset($product1))
            {
                $array_id[] = $product1;
            }
            if(isset($product2))
            {
                $array_id[] = $product2;
            }
            if(isset($product3))
            {
                $array_id[] = $product3;
            }



            // $model_filter->where_in["id"] = $array_id;
            // $products = $this->product_model->search($model_filter);


            if(count($array_id) > 0)
            {
                $model_filter = new stdClass();
                $model_filter->where["product.status"] = 'ACTIVATE';
                $model_filter->where_in["product.id"] = $array_id;
                $model_filter->join = array("product_type" => "product.product_type_id = product_type.id","brand" => "product.brand_id = brand.id");

                $products = $this->product_model->search($model_filter);

                if(count($products) > 0)
                {
                    foreach($products as $product)
                    {
                        $dir = $this->product_path.$product->{"product::code"};
                        if (is_dir($dir))
                        {
                            $product_images = array_diff(scandir($dir), array('.', '..')); 
                        }
                        if(count($product_images) > 0)
                        {
                            $image = array();
                            foreach ($product_images as $product_image) 
                            {
                                $image[] = base_url("assets/uploads/product").'/'.$product->{"product::code"}.'/'.$product_image;
                            }
                            $product->images = $image;
                        }

                        $model_filter = new stdClass();
                        $model_filter->where["promotion_discount.status"] = 'ACTIVATE';
                        $model_filter->where["product_detail_category.status"] = 'ACTIVATE';
                        $model_filter->join = array("product_detail_category" => "promotion_discount.product_category_id = product_detail_category.product_category_id");
                        $promotion_discounts = $this->promotion_discount_model->search($model_filter);

                        $discount_array = array();
                        foreach($promotion_discounts as $promotion_discount)
                        {
                            if((isset($product->{"product::id"}) && ($promotion_discount->{"product_detail_category::product_id"} == $product->{"product::id"})))
                            {
                                $discount_array["discount_percent"] = $promotion_discount->{"promotion_discount::discount_percent"};
                                $discount_array["discount_baht"] = $promotion_discount->{"promotion_discount::discount_baht"};
                                $discount_array["product_id"] = $product->{"product::id"};
                            }
                        }

                        if(isset($discount_array) && !empty($discount_array))
                        {
                            $discount_model = new stdClass();
                            if($discount_array["discount_percent"] != '' && $discount_array["discount_percent"] != null)
                            {
                                $discount_model->discount_percent = $discount_array["discount_percent"];
                                $discount_model->retail_price = $product->{"product::retail_price"};
                                $discount_model->discount_price = number_format(($discount_model->retail_price*$discount_model->discount_percent), 2, '.', '');
                                $discount_model->total_price = ($discount_model->retail_price-$discount_model->discount_price);
                            }
                            if($discount_array["discount_baht"] != '' && $discount_array["discount_baht"] != null)
                            {
                                $discount_model->discount_percent = $discount_array["discount_baht"];
                                $discount_model->retail_price = $product->{"product::retail_price"};
                                $discount_model->discount_price = $discount_array["discount_baht"];
                                $discount_model->total_price = ($product->{"product::retail_price"}-$discount_array["discount_baht"]);
                            }

                            $product->{"product::discount"} = $discount_model;
                        }
                    }
                }
            }

            $model_filter = new stdClass();
            $model_filter->where_in["product_detail_category.product_id"] = $array_id;
            $model_filter->where["product_detail_category.status"] = "ACTIVATE";
            $model_filter->custom_where =  "(product_detail_category.product_category_id is null OR product_category.status like 'ACTIVATE') AND (product_detail_category.product_sub_category_id is null OR product_sub_category.status like 'ACTIVATE')"; 
            $model_filter->join = array(
                "product_category" => "product_detail_category.product_category_id = product_category.id",
                "product_sub_category" => "product_detail_category.product_sub_category_id = product_sub_category.id"
            );
            $model_filter->group_by = 'product_detail_category.product_category_id , product_detail_category.product_sub_category_id';
            $product_categorys = $this->product_detail_category_model->search($model_filter);
            $technical_spec_ids = array();
            $technical_specs = array();
            if ($product_categorys != null && count($product_categorys) > 0) {
                foreach ($product_categorys as $procat) {
                    $model_filter = new stdClass();
                    $model_filter->where["technical_spec_category.status"] = "ACTIVATE";
                    $model_filter->where["technical_spec.status"] = "ACTIVATE";
                    $model_filter->custom_where = "technical_spec_category.product_category_id = ".$procat->{"product_detail_category::product_category_id"};

                    if (!empty($procat->{"product_detail_category.product_sub_category_id"})) {
                        $model_filter->custom_where.=" AND (technical_spec_category.product_sub_category_id is null OR technical_spec_category.product_sub_category_id = ".$procat->{"product_detail_category::product_sub_category_id"}.")";
                    }
                    $model_filter->join = array(
                        "technical_spec" => "technical_spec_category.technical_spec_id = technical_spec.id"
                    );
                    $technical_spec_categorys = $this->technical_spec_category_model->search($model_filter);
                    if ($technical_spec_categorys != null && count($technical_spec_categorys) > 0) {
                        foreach ($technical_spec_categorys as $item) {
                            if(!in_array($item->{"technical_spec::id"}, $technical_spec_ids)){
                                $tech = $this->technical_spec_model->find($item->{"technical_spec::id"});
                                $technical_spec_ids[] = $item->{"technical_spec::id"};
                                $model_filter = new stdClass();
                                $model_filter->where_in["product_id"] = $array_id;
                                $model_filter->where["technical_spec_id"] = $item->{"technical_spec::id"};
                                $model_filter->where["status"] = "ACTIVATE";
                                $model_filter->get_first = true;
                                $value_spec = $this->product_technical_spec_model->search($model_filter);
                                $technical_specs[] = array(
                                    "technical_spec" => $tech,
                                    "value_spec" => $value_spec
                                );
                            }
                        }
                    }
                    $procat->{"product::technical_spec"} = $technical_specs;
                }
            }
            $this->response_library->responseJSON("0x0000-00000","The server respone success.", array("products" => $products, "product_spec" => $product_categorys));
        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The server respone Fails."); 
        }
    }

    public function product_list($page = null)
    {
        $this->load->library("response_library");

        $this->load->model("promotion_discount_model");

        $this->load->model("product_relate_model");
        $this->load->model("product_model");
        $this->load->model("product_detail_category_model");
        $this->load->model("product_detail_menu_model");
        $this->load->model("product_technical_spec_model");
        $this->load->model("price_rate_model");

        if($this->input->post())
        {
            $product_category_id = $this->input->post("product_category_id");
            $product_sub_category_id = $this->input->post("product_sub_category_id");
            $price_rate_id = $this->input->post("price_rate_id");
            $show_item = $this->input->post("show_item");
            $page = $this->input->post("page");
            $technical_spec_id = $this->input->post("technical_spec_id");
            $sale_type = $this->input->post("sale_type");


            if(isset($technical_spec_id))
            {
                $model_filter = new stdClass();
                $model_filter->where["product_technical_spec.status"] = 'ACTIVATE';
                $model_filter->where_in["technical_spec_id"] = $technical_spec_id;
                $product_technical_specs = $this->product_technical_spec_model->search($model_filter);

                $product_technical_spec_array = array();
                foreach ($product_technical_specs as $key => $product_technical_spec) {
                    $product_technical_spec_array[] = $product_technical_spec->product_id;
                }
            }

            if(!isset($show_item))
            {
                $show_item = 12;
            }
            if(!isset($page))
            {
                $page = 1;
            }


            $price_rate = $this->price_rate_model->find($price_rate_id);

            $between = '';
            if(isset($price_rate))
            {
                $between = $price_rate->price_min . ' AND ' . $price_rate->price_max;
            }

            $model_filter = new stdClass();
            $model_filter->where["status"] = 'ACTIVATE';
            if(isset($product_category_id) && $product_category_id != '')
            {
                $model_filter->where["product_category_id"] = $product_category_id;
            }
            if(isset($product_sub_category_id) && $product_sub_category_id != '')
            {
                $model_filter->where["product_sub_category_id"] = $product_sub_category_id;
            }
            if(isset($product_technical_spec_array))
            {
                $model_filter->where_in["product_id"] = $product_technical_spec_array;
            }
            $model_filter->group_by = 'product_id';
            // $model_filter->get_query = true;
            $product_detail_categories = $this->product_detail_category_model->search($model_filter);

            if(count($product_detail_categories) > 0)
            {

                $product_array = array();
                foreach ($product_detail_categories as $key => $product_detail_category) {
                    $product_array[] = $product_detail_category->product_id;
                }

                // print_r($product_array);
                // exit();

                $page_start = ($page-1)*$show_item;

                $model_filter = new stdClass();
                $model_filter->where["product.status"] = 'ACTIVATE';
                $model_filter->where_in["product.id"] = $product_array;
                $model_filter->join = array("product_type" => "product.product_type_id = product_type.id","brand" => "product.brand_id = brand.id");
                if(isset($price_rate_id) && $price_rate_id != '')
                {
                    $model_filter->custom_where = 'product.retail_price BETWEEN '.$between.' ';
                }
                // $model_filter->limit = $show_item;
                // $model_filter->limit_start = $page_start;
                // $model_filter->get_query = true;
                $product_mains_all = $this->product_model->search($model_filter);

                $model_filter = new stdClass();
                $model_filter->where["product.status"] = 'ACTIVATE';
                $model_filter->where_in["product.id"] = $product_array;
                if(isset($price_rate_id) && $price_rate_id != '')
                {
                    $model_filter->custom_where = 'product.retail_price BETWEEN '.$between.' ';
                }
                $model_filter->join = array("product_type" => "product.product_type_id = product_type.id","brand" => "product.brand_id = brand.id");
                $model_filter->limit = $show_item;
                $model_filter->limit_start = $page_start;
                // $model_filter->get_query = true;

                if(isset($sale_type) && !empty($sale_type))
                {
                    if($sale_type == "sale")
                    {
                        $model_filter->order_by = "is_recommend DESC";
                    }
                    if($sale_type == "best_seller")
                    {
                        $model_filter->order_by = "is_bestseller DESC";
                    }
                }
                $product_mains = $this->product_model->search($model_filter);

                if(count($product_mains) > 0)
                {
                    foreach($product_mains as $product_relate_all)
                    {
                        $image = array();
                        $dir = $this->product_path.$product_relate_all->{"product::code"};
                        if (is_dir($dir))
                        {
                            $product_images = array_diff(scandir($dir), array('.', '..')); 
                        }
                        if(count($product_images) > 0)
                        {
                            foreach ($product_images as $product_image) 
                            {
                                $image[] = base_url("assets/uploads/product").'/'.$product_relate_all->{"product::code"}.'/'.$product_image;
                            }
                            $product_relate_all->{"product::images"} = $image;
                        }

                        $model_filter = new stdClass();
                        if(isset($product_category_id) && $product_category_id != '')
                        {
                            $model_filter->where["promotion_discount.product_category_id"] = $product_category_id;
                        }
                        $model_filter->where["promotion_discount.status"] = 'ACTIVATE';
                        $model_filter->where["product_detail_category.status"] = 'ACTIVATE';
                        $model_filter->join = array("product_detail_category" => "promotion_discount.product_category_id = product_detail_category.product_category_id");
                        $promotion_discounts = $this->promotion_discount_model->search($model_filter);

                        $discount_array = array();
                        foreach($promotion_discounts as $promotion_discount)
                        {
                            if(isset($product_relate_all->{"product::id"}) && ($promotion_discount->{"product_detail_category::product_id"} == $product_relate_all->{"product::id"}))
                            {
                                $discount_array["discount_percent"] = $promotion_discount->{"promotion_discount::discount_percent"};
                                $discount_array["discount_baht"] = $promotion_discount->{"promotion_discount::discount_baht"};
                                $discount_array["product_id"] = $product_relate_all->{"product::id"};
                            }
                        }

                        if(isset($discount_array) && !empty($discount_array))
                        {
                            $discount_model = new stdClass();
                            if($discount_array["discount_percent"] != '' && $discount_array["discount_percent"] != null)
                            {
                                $discount_model->discount_percent = $discount_array["discount_percent"];
                                $discount_model->retail_price = $product_relate_all->{"product::retail_price"};
                                $discount_model->discount_price = number_format(($discount_model->retail_price*$discount_model->discount_percent), 2, '.', '');
                                $discount_model->total_price = ($discount_model->retail_price-$discount_model->discount_price);
                            }
                            if($discount_array["discount_baht"] != '' && $discount_array["discount_baht"] != null)
                            {
                                $discount_model->discount_percent = $discount_array["discount_baht"];
                                $discount_model->retail_price = $product_relate_all->{"product::retail_price"};
                                $discount_model->discount_price = $discount_array["discount_baht"];
                                $discount_model->total_price = ($product_relate_all->{"product::retail_price"}-$discount_array["discount_baht"]);
                            }

                            $product_relate_all->{"product::discount"} = $discount_model;
                        }
                    }
                }

                $item_max = count($product_mains_all);

                $page_max = ceil(count($product_mains_all)/$show_item);

                $this->response_library->responseJSON("0x0000-00000","The server respone success.", array("page_max" => $page_max,"page" => $page,"show_item" => $show_item,"item_max" => $item_max,"product_category_id" => $product_category_id,"product_sub_category_id" => $product_sub_category_id,"price_rate_id" => $price_rate_id,"product_mains" => $product_mains));
            }
            else
            {
                $this->response_library->responseJSON("1x0000-00000","The server respone Fails. No item.", );
            }
        }
        // else
        // {
        //     $show_item = 12;

        //     $model_filter = new stdClass();
        //     $model_filter->where["status"] = 'ACTIVATE';
        //     $model_filter->group_by = 'product_id';
        //     $product_detail_menus = $this->product_detail_menu_model->search($model_filter);

        //     $product_array = array();
        //     foreach ($product_detail_menus as $key => $product_detail_menu) {
        //         $product_array[] = $product_detail_menu->product_id;
        //     }

        //     // print_r($product_detail_menus);
        //     // exit();

        //     $page_start = ($page-1)*$show_item;

        //     $model_filter = new stdClass();
        //     $model_filter->where["product.status"] = 'ACTIVATE';
        //     $model_filter->where_in["product.id"] = $product_array;
        //     $model_filter->join = array("product_type" => "product.product_type_id = product_type.id","brand" => "product.brand_id = brand.id");
        //     // $model_filter->limit = $show_item;
        //     // $model_filter->limit_start = $page_start;
        //     // $model_filter->get_query = true;
        //     $product_mains_all = $this->product_model->search($model_filter);

        //     $model_filter = new stdClass();
        //     $model_filter->where["product.status"] = 'ACTIVATE';
        //     $model_filter->where_in["product.id"] = $product_array;
        //     $model_filter->join = array("product_type" => "product.product_type_id = product_type.id","brand" => "product.brand_id = brand.id");
        //     $model_filter->limit = $show_item;
        //     $model_filter->limit_start = $page_start;
        //     // $model_filter->get_query = true;
        //     $product_mains = $this->product_model->search($model_filter);

        //     // print_r($product_mains);
        //     // exit();           

        //     $item_max = count($product_mains_all);

        //     $page_max = ceil(count($product_mains_all)/$show_item);

        //     // print_r($page_max);
        //     // exit();


        //     if(count($product_mains) > 0)
        //     {
        //         foreach($product_mains as $product_relate_all)
        //         {
        //             $image = array();
        //             $dir = $this->product_path.$product_relate_all->{"product::code"};
        //             if (is_dir($dir))
        //             {
        //                 $product_images = array_diff(scandir($dir), array('.', '..')); 
        //             }
        //             if(count($product_images) > 0)
        //             {
        //                 foreach ($product_images as $product_image) 
        //                 {
        //                     $image[] = base_url("assets/uploads/product").'/'.$product_relate_all->{"product::code"}.'/'.$product_image;
        //                 }
        //                 $product_relate_all->{"product::images"} = $image;
        //             }
        //         }
        //     }

        //     $this->response_library->responseJSON("0x0000-00000","The server respone success.", array("page_max" => $page_max,"page" => $page,"show_item" => $show_item,"item_max" => $item_max,"product_mains" => $product_mains));
        // }

    }

    public function product_detail($main_product_id = null)
    {

        $this->load->library("response_library");

        $this->load->model("promotion_discount_model");

        $this->load->model("product_relate_model");
        $this->load->model("product_model");
        $this->load->model("product_category_model");
        $this->load->model("product_sub_category_model");
        $this->load->model("product_detail_category_model");
        $this->load->model("technical_spec_category_model");
        $this->load->model("product_technical_spec_model");
        $this->load->model("technical_spec_model");
        $this->load->model("product_tutorial_model");
        $this->load->model("product_feature_model");
        $this->load->model("product_datasheet_model");
        $this->load->model("product_price_model");

        $this->load->model("promotion_discount_model");

        $this->load->model("review_model");
        $this->load->model("review_image_model");
        $this->load->model("member_model");

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->where["main_product_id"] = $main_product_id;
        $product_relates = $this->product_relate_model->search($model_filter);

        $model_filter = new stdClass();
        $model_filter->where["product.status"] = 'ACTIVATE';
        $model_filter->where["product.id"] = $main_product_id;
        $model_filter->get_first = true;
        $model_filter->join = array("product_type" => "product.product_type_id = product_type.id","brand" => "product.brand_id = brand.id");
        $product_main = $this->product_model->search($model_filter);

        if(isset($product_main))
        {
            $dir = $this->product_path.$product_main->{"product::code"};
            if (is_dir($dir))
            {
                $product_images = array_diff(scandir($dir), array('.', '..')); 
            }
            if(count($product_images) > 0)
            {
                foreach ($product_images as $product_image) 
                {
                    $image[] = base_url("assets/uploads/product").'/'.$product_main->{"product::code"}.'/'.$product_image;
                }
                $product_main->{"product::images"} = $image;
            }
            else
            {
                $product_main->{"product::images"} = array();
            }

            $model_filter = new stdClass();
            $model_filter->where["status"] = 'ACTIVATE';
            $model_filter->where["product_id"] = $product_main->{"product::id"};

            $product_reviews = $this->review_model->search($model_filter);

            if(isset($product_reviews) && count($product_reviews) > 0)
            {
                foreach($product_reviews as $product_review)
                {
                    $review = new stdClass();
                    $review->where["review_id"] = $product_review->id;
                    $review_images = $this->review_image_model->search($review);
                    $product_review->images = $review_images;
                }
                
                $product_main->{"product::product_reviews"} = $product_reviews;
            }


            $product_tutorials = $this->product_tutorial_model->search($model_filter);

            if(isset($product_tutorials))
            {
                $product_main->{"product::product_tutorials"} = $product_tutorials;
            }

            $product_features = $this->product_feature_model->search($model_filter);

            if(isset($product_features))
            {
                $product_main->{"product::product_features"} = $product_features;
            }

            $product_datasheets = $this->product_datasheet_model->search($model_filter);
            
            if(isset($product_datasheets))
            {
                $product_main->{"product::product_datasheets"} = $product_datasheets;
            }

            $product_price = $this->product_price_model->search($model_filter);
            
            if(isset($product_price))
            {
                $model_filter->order = "minimum_qty DESC";
                $product_main->{"product::product_price"} = $product_price;
            }


            $model_filter = new stdClass();
            $model_filter->where_in["product_detail_category.product_id"] = $product_main->{"product::id"};
            $model_filter->where["product_detail_category.status"] = "ACTIVATE";
            $model_filter->custom_where =  "(product_detail_category.product_category_id is null OR product_category.status like 'ACTIVATE') AND (product_detail_category.product_sub_category_id is null OR product_sub_category.status like 'ACTIVATE')"; 
            $model_filter->join = array(
                "product_category" => "product_detail_category.product_category_id = product_category.id",
                "product_sub_category" => "product_detail_category.product_sub_category_id = product_sub_category.id"
            );
            $model_filter->group_by = 'product_detail_category.product_category_id , product_detail_category.product_sub_category_id';
            $product_categorys = $this->product_detail_category_model->search($model_filter);
            $technical_spec_ids = array();
            $technical_specs = array();
            if ($product_categorys != null && count($product_categorys) > 0) {
                foreach ($product_categorys as $procat) {
                    $model_filter = new stdClass();
                    $model_filter->where["technical_spec_category.status"] = "ACTIVATE";
                    $model_filter->where["technical_spec.status"] = "ACTIVATE";
                    $model_filter->custom_where = "technical_spec_category.product_category_id = ".$procat->{"product_detail_category::product_category_id"};

                    if (!empty($procat->{"product_detail_category.product_sub_category_id"})) {
                        $model_filter->custom_where.=" AND (technical_spec_category.product_sub_category_id is null OR technical_spec_category.product_sub_category_id = ".$procat->{"product_detail_category::product_sub_category_id"}.")";
                    }
                    $model_filter->join = array(
                        "technical_spec" => "technical_spec_category.technical_spec_id = technical_spec.id"
                    );
                    $technical_spec_categorys = $this->technical_spec_category_model->search($model_filter);
                    if ($technical_spec_categorys != null && count($technical_spec_categorys) > 0) {
                        foreach ($technical_spec_categorys as $item) {
                            if(!in_array($item->{"technical_spec::id"}, $technical_spec_ids)){
                                $tech = $this->technical_spec_model->find($item->{"technical_spec::id"});
                                $technical_spec_ids[] = $item->{"technical_spec::id"};
                                $model_filter = new stdClass();
                                $model_filter->where_in["product_id"] = $product_main->{"product::id"};
                                $model_filter->where["technical_spec_id"] = $item->{"technical_spec::id"};
                                $model_filter->where["status"] = "ACTIVATE";
                                $model_filter->get_first = true;
                                $value_spec = $this->product_technical_spec_model->search($model_filter);
                                $technical_specs[] = array(
                                    "technical_spec" => $tech,
                                    "value_spec" => $value_spec
                                );
                            }
                        }
                    }
                    $procat->{"product::technical_spec"} = $technical_specs;
                }
            }
            $product_main->{"product_detail_categorys"} = $product_categorys;

            $model_filter = new stdClass();
            $model_filter->where["promotion_discount.status"] = 'ACTIVATE';
            $model_filter->where["product_detail_category.status"] = 'ACTIVATE';
            $model_filter->join = array("product_detail_category" => "promotion_discount.product_category_id = product_detail_category.product_category_id");
            $promotion_discounts = $this->promotion_discount_model->search($model_filter);

            // $discount = false;
            $discount_array = array();
            foreach($promotion_discounts as $promotion_discount)
            {
                if(isset($main_product_id) && ($promotion_discount->{"product_detail_category::product_id"} == $main_product_id))
                {
                    // $discount = true;
                    $discount_array["discount_percent"] = $promotion_discount->{"promotion_discount::discount_percent"};
                    $discount_array["discount_baht"] = $promotion_discount->{"promotion_discount::discount_baht"};
                    $discount_array["product_id"] = $main_product_id;
                }
            }

            if(isset($discount_array) && !empty($discount_array))
            {
                $discount_model = new stdClass();
                if($discount_array["discount_percent"] != '' && $discount_array["discount_percent"] != null)
                {
                    $discount_model->discount_percent = $discount_array["discount_percent"];
                    $discount_model->retail_price = $product_main->{"product::retail_price"};
                    $discount_model->discount_price = number_format(($discount_model->retail_price*$discount_model->discount_percent), 2, '.', '');
                    $discount_model->total_price = ($discount_model->retail_price-$discount_model->discount_price);
                }
                if($discount_array["discount_baht"] != '' && $discount_array["discount_baht"] != null)
                {
                    $discount_model->discount_percent = $discount_array["discount_baht"];
                    $discount_model->retail_price = $product_main->{"product::retail_price"};
                    $discount_model->discount_price = $discount_array["discount_baht"];
                    $discount_model->total_price = ($product_main->{"product::retail_price"}-$discount_array["discount_baht"]);
                }

                $product_main->{"product::discount"} = $discount_model;
                // echo "<pre>";
                // print_r($product_main);
                // exit();
            }
        }

        $product_array = array();
        $product_relate_alls = null;
        $image = array();

        if(count($product_relates) > 0)
        {
            foreach($product_relates as $product_relate)
            {
                $product_array[] = $product_relate->relate_product_id;
            }

            $model_filter = new stdClass();
            $model_filter->where["product.status"] = 'ACTIVATE';
            $model_filter->where_in["product.id"] = $product_array;
            $model_filter->join = array("product_type" => "product.product_type_id = product_type.id","brand" => "product.brand_id = brand.id");

            $product_relate_alls = $this->product_model->search($model_filter);

            if(count($product_relate_alls) > 0)
            {
                foreach($product_relate_alls as $product_relate_all)
                {
                    $image = array();
                    $dir = $this->product_path.$product_relate_all->{"product::code"};
                    if (is_dir($dir))
                    {
                        $product_images = array_diff(scandir($dir), array('.', '..')); 
                    }
                    if(count($product_images) > 0)
                    {
                        foreach ($product_images as $product_image) 
                        {
                            $image[] = base_url("assets/uploads/product").'/'.$product_relate_all->{"product::code"}.'/'.$product_image;
                        }
                        $product_relate_all->{"product::images"} = $image;
                    }


                    $model_filter = new stdClass();
                    $model_filter->where["promotion_discount.status"] = 'ACTIVATE';
                    $model_filter->where["product_detail_category.status"] = 'ACTIVATE';
                    $model_filter->join = array("product_detail_category" => "promotion_discount.product_category_id = product_detail_category.product_category_id");
                    $promotion_discounts = $this->promotion_discount_model->search($model_filter);

                    $discount_array = array();
                    foreach($promotion_discounts as $promotion_discount)
                    {
                        if(isset($product_relate_all->{"product::id"}) && ($promotion_discount->{"product_detail_category::product_id"} == $product_relate_all->{"product::id"}))
                        {
                            $discount_array["discount_percent"] = $promotion_discount->{"promotion_discount::discount_percent"};
                            $discount_array["discount_baht"] = $promotion_discount->{"promotion_discount::discount_baht"};
                            $discount_array["product_id"] = $product_relate_all->{"product::id"};
                        }
                    }

                    if(isset($discount_array) && !empty($discount_array))
                    {
                        $discount_model = new stdClass();
                        if($discount_array["discount_percent"] != '' && $discount_array["discount_percent"] != null)
                        {
                            $discount_model->discount_percent = $discount_array["discount_percent"];
                            $discount_model->retail_price = $product_relate_all->{"product::retail_price"};
                            $discount_model->discount_price = number_format(($discount_model->retail_price*$discount_model->discount_percent), 2, '.', '');
                            $discount_model->total_price = ($discount_model->retail_price-$discount_model->discount_price);
                        }
                        if($discount_array["discount_baht"] != '' && $discount_array["discount_baht"] != null)
                        {
                            $discount_model->discount_percent = $discount_array["discount_baht"];
                            $discount_model->retail_price = $product_relate_all->{"product::retail_price"};
                            $discount_model->discount_price = $discount_array["discount_baht"];
                            $discount_model->total_price = ($product_relate_all->{"product::retail_price"}-$discount_array["discount_baht"]);
                        }

                        $product_relate_all->{"product::discount"} = $discount_model;
                    }
                }
            }
        }

        $this->response_library->responseJSON("0x0000-00000","The server respone success.", array("product_main" => $product_main, "product_relate_alls" => $product_relate_alls));

    }

    public function price_rate()
    {
        $this->load->library("response_library");

        $this->load->model("price_rate_model");

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';

        $price_rate = $this->price_rate_model->search($model_filter);

        $this->response_library->responseJSON("0x0000-00000","The server respone success.", array("price_rate" => $price_rate));

    }

    public function get_cart($member_id)
    {
        $this->load->library("response_library");

        $this->load->model("promotion_discount_model");

        $this->load->model("cart_model");
        $this->load->model("product_price_model");
        $this->load->model("product_model");

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->where["member_id"] = $member_id;

        $carts = $this->cart_model->search($model_filter);

        if(count($carts) > 0)
        {
            $product_array = array();
            foreach($carts as $cart)
            {
                $product_array[] = $cart->product_id;
            }

            // print_r($product_array);
            // exit();

            $model_filter = new stdClass();
            $model_filter->where["product.status"] = 'ACTIVATE';
            $model_filter->where_in["product.id"] = $product_array;
            $model_filter->where["cart.member_id"] = $member_id;
            $model_filter->join = array("product_type" => "product.product_type_id = product_type.id","brand" => "product.brand_id = brand.id","cart" => "product.id = cart.product_id");
            // $model_filter->limit = $show_item;
            // $model_filter->limit_start = $page_start;
            // $model_filter->get_query = true;
            $product_mains = $this->product_model->search($model_filter);

            // print_r($product_mains);
            // exit();

            $model_filter = new stdClass();
            $model_filter->where["promotion_discount.status"] = 'ACTIVATE';
            $model_filter->where["product_detail_category.status"] = 'ACTIVATE';
            $model_filter->join = array("product_detail_category" => "promotion_discount.product_category_id = product_detail_category.product_category_id");
            $promotion_discounts = $this->promotion_discount_model->search($model_filter);

            foreach($product_mains as $product_main)
            {

            $discount_array = array();
            foreach($promotion_discounts as $promotion_discount)
            {
                if(($promotion_discount->{"product_detail_category::product_id"} == $product_main->{"product::id"}))
                {
                    $discount_array["discount_percent"] = $promotion_discount->{"promotion_discount::discount_percent"};
                    $discount_array["discount_baht"] = $promotion_discount->{"promotion_discount::discount_baht"};
                    $discount_array["product_id"] = $product_main->{"product::id"};
                }
            }


            if(isset($discount_array) && !empty($discount_array))
            {
                $discount_model = new stdClass();
                if($discount_array["discount_percent"] != '' && $discount_array["discount_percent"] != null)
                {
                    $discount_model->discount_percent = $discount_array["discount_percent"];
                    $discount_model->retail_price = $product_main->{"product::retail_price"};
                    $discount_model->discount_price = number_format(($discount_model->retail_price*$discount_model->discount_percent), 2, '.', '');
                    $discount_model->total_price = ($discount_model->retail_price-$discount_model->discount_price);
                }
                if($discount_array["discount_baht"] != '' && $discount_array["discount_baht"] != null)
                {
                    $discount_model->discount_percent = $discount_array["discount_baht"];
                    $discount_model->retail_price = $product_main->{"product::retail_price"};
                    $discount_model->discount_price = $discount_array["discount_baht"];
                    $discount_model->total_price = ($product_main->{"product::retail_price"}-$discount_array["discount_baht"]);
                }

                $product_main->{"product::discount"} = $discount_model;
            }
        }


            if(count($product_mains) > 0)
            {
                foreach($product_mains as $product_relate_all)
                {
                    $image = array();
                    $dir = $this->product_path.$product_relate_all->{"product::code"};
                    $product_id = $product_relate_all->{"product::id"};

                    $model_filter = new stdClass();
                    $model_filter->where["status"] = 'ACTIVATE';
                    $model_filter->where_in["product_id"] = $product_id;
                    $prdocut_price = $this->product_price_model->search($model_filter);

                    $product_relate_all->prdocut_price = $prdocut_price;

                    if (is_dir($dir))
                    {
                        $product_images = array_diff(scandir($dir), array('.', '..')); 
                    }
                    if(count($product_images) > 0)
                    {
                        foreach ($product_images as $product_image) 
                        {
                            $image[] = base_url("assets/uploads/product").'/'.$product_relate_all->{"product::code"}.'/'.$product_image;
                        }
                        $product_relate_all->{"product::images"} = $image;
                    }
                }
            }

            $this->response_library->responseJSON("0x0000-00000","The server respone success.",array("product_carts" => $product_mains)); 

        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The server respone cart out."); 
        }

    }

    public function add_cart()
    {
        $this->load->library("response_library");

        $this->load->model("cart_model");

        if($this->input->post())
        {
            $member_id = $this->input->post("member_id");
            $product_id = $this->input->post("product_id");

            $cK_model = new stdClass();
            $cK_model->where["member_id"] = $member_id;
            $cK_model->where["product_id"] = $product_id;
            $cK_model->where["status"] = 'ACTIVATE';
            $cK_model->get_first = true;

            $ck = $this->cart_model->search($cK_model);
            if(isset($ck) && !empty($ck))
            {
                $ck->qty = $ck->qty + 1;
                $ck->modified_date = date("Y-m-d h:i:s");
                $ck->modified_by = $this->input->post("member_id");

                $cart_id = $this->cart_model->save($ck);

            }
            else
            {

                $model_filter = new stdClass();
                $model_filter->status = 'ACTIVATE';
                $model_filter->member_id = $this->input->post("member_id");
                $model_filter->product_id = $this->input->post("product_id");
                $model_filter->qty = 1;
                $model_filter->created_date = date("Y-m-d h:i:s");
                $model_filter->created_by = $this->input->post("member_id");

                $cart_id = $this->cart_model->save($model_filter);
            }



            $this->response_library->responseJSON("0x0000-00000","The server respone create success.",array("cart_id" => $cart_id));
        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The server respone Fails."); 
        }
    }

    public function update_cart()
    {
        $this->load->library("response_library");

        $this->load->model("cart_model");

        if($this->input->post())
        {
            $action = $this->input->post("action");
            $cart_id = $this->input->post("cart_id");

            if($action == 'plus')
            {
                $cart = $this->cart_model->find($cart_id);

                $cart->qty = $cart->qty+1;
                $cart->modified_date = date("Y-m-d h:i:s");
                $cart->modified_by = $this->input->post("member_id");

                $this->cart_model->save($cart);

            }
            else if($action == 'minus')
            {
                $cart = $this->cart_model->find($cart_id);

                $cart->qty = $cart->qty-1;
                $cart->modified_date = date("Y-m-d h:i:s");
                $cart->modified_by = $this->input->post("member_id");

                $this->cart_model->save($cart);
            }

            $this->response_library->responseJSON("0x0000-00000","The server respone update success.");
        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The server respone Fails."); 
        }
    }

    public function delete_cart($cart_id)
    {
        $this->load->library("response_library");

        $this->load->model("cart_model");

        $model_filter = new stdClass();
        $model_filter->where["id"] = $cart_id;

        $this->cart_model->delete($model_filter);

        $this->response_library->responseJSON("0x0000-00000","The server respone delete success.");
    }

    public function get_quotation($member_id)
    {
        $this->load->library("response_library");

        $this->load->model("quotation_model");
        $this->load->model("quotation_detail_model");
        $this->load->model("product_model");

        $model_filter = new stdClass();
        $model_filter->where["status"] = 'ACTIVATE';
        $model_filter->where["member_id"] = $member_id;

        $quotations = $this->quotation_model->search($model_filter);

        if(count($quotations) > 0)
        {
            $quotation_id_array = array();
            foreach($quotations as $quotation)
            {

                $model_filter = new stdClass();
                $model_filter->where["quotation_detail.status"] = 'ACTIVATE';
                $model_filter->where_in["quotation_detail.quotation_id"] = $quotation->id;
                $model_filter->join = array("product" => "quotation_detail.product_id = product.id","brand" => "product.brand_id = brand.id","product_type" => "product.product_type_id = product_type.id");

                $quotation_details = $this->quotation_detail_model->search($model_filter);

                if(count($quotation_details) > 0)
                {
                    foreach($quotation_details as $product_relate_all)
                    {
                        $image = array();
                        $dir = $this->product_path.$product_relate_all->{"product::code"};
                        if (is_dir($dir))
                        {
                            $product_images = array_diff(scandir($dir), array('.', '..')); 
                        }
                        if(count($product_images) > 0)
                        {
                            foreach ($product_images as $product_image) 
                            {
                                $image[] = base_url("assets/uploads/product").'/'.$product_relate_all->{"product::code"}.'/'.$product_image;
                            }
                            $product_relate_all->{"product::images"} = $image;
                        }
                    }
                }
                $quotation->quotation_details = $quotation_details;
            }

            $this->response_library->responseJSON("0x0000-00000","The server respone success.",array("quotations" => $quotations)); 

        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The server respone cart out."); 
        }
    }

    public function quotation()
    {
        $this->load->library("response_library");

        $this->load->model("quotation_model");
        $this->load->model("quotation_detail_model");

        if($this->input->post())
        {
            $email = $this->input->post("email");

            $quotation_id = $this->input->post("quotation_id");
            $model_filter = new stdClass();
            $model_filter->status = 'ACTIVATE';
            $model_filter->member_id = $this->input->post("member_id");
            $model_filter->type = $this->input->post("type");
            $model_filter->quotation_no = "Q".time();
            $model_filter->first_name = $this->input->post("first_name");
            $model_filter->last_name = $this->input->post("last_name");
            $model_filter->telephone = $this->input->post("telephone");
            $model_filter->email = $this->input->post("email");
            $model_filter->company_name = $this->input->post("company_name");
            $model_filter->tax_id = $this->input->post("tax_id");
            $model_filter->message = $this->input->post("message");
            $model_filter->created_date = date("Y-m-d h:i:s");
            $model_filter->created_by = $this->input->post("member_id");

            $quotation_id = $this->quotation_model->save($model_filter);

            if(isset($quotation_id))
            {

                $product_ids = $this->input->post("product_id");
                $quantitys = $this->input->post("quantity");
                $real_prices = $this->input->post("real_price");
                $price_per_units = $this->input->post("price_per_unit");
                $total_prices = $this->input->post("total_price");

                for ($i=0; $i < count($product_ids); $i++) { 

                    $model_filter = new stdClass();
                    $model_filter->quotation_id = $quotation_id;
                    $model_filter->product_id = $product_ids[$i];
                    $model_filter->quantity = $quantitys[$i];
                    $model_filter->real_price = $real_prices[$i];
                    $model_filter->price_per_unit = $price_per_units[$i];
                    $model_filter->total_price = $total_prices[$i];
                    $model_filter->status = 'ACTIVATE';
                    $model_filter->created_date = date("Y-m-d h:i:s");
                    $model_filter->created_by = $this->input->post("member_id");

                    $quotation_detail_id = $this->quotation_detail_model->save($model_filter);
                }

                $to = $email; // note the comma

            // Subject
                $subject = 'Thank you, Quotation.';

                    // $token = $this->encryption_library->encryptString($email,"token");
                    // $member->token = $token;


            // Message
                $message = $this->load->view("EmailTemplate/Quotation",true);

                    // print_r($message);
                    // exit();

            // To send HTML mail, the Content-type header must be set
                $headers[] = 'MIME-Version: 1.0';
                $headers[] = 'Content-type: text/html; charset=utf-8';

            // Additional headers
                    // $headers[] = 'To: Mary <mary@example.com>, Kelly <kelly@example.com>';
                $headers[] = 'From: Nextere <sale@nextere.co.th>';
                    // $headers[] = 'Cc: birthdayarchive@example.com';
                    // $headers[] = 'Bcc: birthdaycheck@example.com';

            // Mail it
                // mail($to, $subject, $message, implode("\r\n", $headers));
            }

            $this->response_library->responseJSON("0x0000-00000","The server respone create success.",array("quotation_id" => $quotation_id));
        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The server respone Fails."); 
        }
    }

    public function review()
    {
        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $this->load->model("review_model");
        $this->load->model("review_image_model");

        if($this->input->post())
        {

            $product_id = $this->input->post("product_id");
            $member_id = $this->input->post("member_id");
            $purchase_transaction_detail_id = $this->input->post("purchase_transaction_detail_id");
            $rating = $this->input->post("rating");
            $message = $this->input->post("message");
            $status = 'ACTIVATE';
            $created_date = date("Y-m-d h:i:s");
            $created_by = $this->input->post("member_id");

            $review = new stdClass();
            $review->product_id = $product_id;
            $review->member_id = $member_id;
            $review->purchase_transaction_detail_id = $purchase_transaction_detail_id;
            $review->rating = $rating;
            $review->message = $message;
            $review->status = $status;
            $review->created_date = $created_date;
            $review->created_by = $created_by;

            $review_id = $this->review_model->save($review);

            if(isset($review_id))
            {
                if(isset($_FILES['image_paths'])){

                // $image_paths = $_FILES['image_paths'];


                    for ($i=0; $i < count($_FILES['image_paths']['name']); $i++) 
                    { 


                        $model_filter = new stdClass();
                        $model_filter->status = $status;
                        $model_filter->review_id = $review_id;

                        $review_image = '';
                        if (!file_exists(FCPATH.'/assets/uploads/review_image/')) {
                            mkdir(FCPATH.'/assets/uploads/review_image/', 0777, true);
                        }
                        if (!empty($_FILES['image_paths']['name'][$i]) )
                        {
                            $_FILES['image_path']['name']= $_FILES['image_paths']['name'][$i];
                            $_FILES['image_path']['type']= $_FILES['image_paths']['type'][$i];
                            $_FILES['image_path']['tmp_name']= $_FILES['image_paths']['tmp_name'][$i];
                            $_FILES['image_path']['error']= $_FILES['image_paths']['error'][$i];
                            $_FILES['image_path']['size']= $_FILES['image_paths']['size'][$i];  

                            $fileName = date("YmdHis")."_".$this->guid_library->generate();
                            $config['file_name'] = $fileName;
                            $config['upload_path'] = FCPATH.'/assets/uploads/review_image/';
                            $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                            $config['max_size'] = '5000000';
                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            $uploadCheck = $this->upload->do_upload("image_path");
                            if ($uploadCheck == 1) {
                                $upload_data = $this->upload->data();
                                $review_image = '/assets/uploads/review_image/'.$upload_data['file_name'];
                            }
                            else
                            {
                                $this->response_library->responseJSON("0x000C-U0001", $this->upload->display_errors());
                            }
                        }

                        if(!empty($review_image))
                        {
                            $model_filter->image_path = $review_image;
                        }
                        $model_filter->created_date = date("Y-m-d h:i:s");
                        $model_filter->created_by = $this->input->post("member_id");

                        $review_image_id = $this->review_image_model->save($model_filter);
                    }

                }
            }

            $this->response_library->responseJSON("0x0000-00000","The server respone create success.",array("review" => $review));
        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The server respone Fails."); 
        }
    }

    public function check_coupon()
    {
        $this->load->library("encryption_library");
        $this->load->library("response_library");
        $this->load->library("guid_library");

        $this->load->model("coupon_model");
        $this->load->model("coupon_except_category_model");
        $this->load->model("coupon_except_product_model");

        if($this->input->post())
        {
            $coupon_code = $this->input->post("code");
            $products = $this->input->post("products");

            $product_array = array();
            if(isset($products))
            {
                $product_array = explode(",",$products);

                // print_r($product_array);
                // exit();
            }
            // $coupon_code = $this->input->post("code");

            $model_filter = new stdClass();
            $model_filter->get_first = true;
            $model_filter->where["code"] = $coupon_code;
            $model_filter->where["status"] = 'ACTIVATE';

            $coupon = $this->coupon_model->search($model_filter);

            if(isset($coupon))
            {
                $end_time = $coupon->end_time;
                $start_time = $coupon->start_time;
                $remaining_qty = $coupon->remaining_qty;
                $date = date("Y-m-d");
                if($remaining_qty == 0)
                {
                    $this->response_library->responseJSON("1x0000-00000","Coupon sold out."); 
                    exit();
                }
                else if($date > $end_time)
                {
                    $this->response_library->responseJSON("1x0000-00000","Coupon expired."); 
                    exit();
                }
                else if($date < $start_time)
                {
                    $this->response_library->responseJSON("1x0000-00000","Coupon cannot be used."); 
                    exit();
                }
                else
                {

                    $model_filter = new stdClass();
                    $model_filter->where["coupon_id"] = $coupon->id;
                    $model_filter->where_in["product_id"] = $product_array;
                    $model_filter->where["status"] = "ACTIVATE";
                    $coupon_except_product = $this->coupon_except_product_model->search($model_filter);

                    if(count($coupon_except_product) > 0)
                    {
                        $this->response_library->responseJSON("1x0000-00000","Coupon not found (except product)."); 
                        exit();
                    }


                    $model_filter = new stdClass();
                    $model_filter->where["coupon_except_category.coupon_id"] = $coupon->id;
                    $model_filter->where_in["product_detail_category.product_id"] = $product_array;
                    $model_filter->where["coupon_except_category.status"] = "ACTIVATE";
                    $model_filter->join = array("product_detail_category" => "coupon_except_category.product_category_id = product_detail_category.product_category_id");
                    $coupon_except_category = $this->coupon_except_category_model->search($model_filter);

                    // print_r($coupon_except_category);exit();

                    if(count($coupon_except_category) > 0)
                    {
                        $this->response_library->responseJSON("1x0000-00000","Coupon not found (except product category)."); 
                        exit();
                    }

                    

                    $this->response_library->responseJSON("0x0000-00000","The server respone success.",array("coupon" => $coupon));

                }
            }
            else
            {
                $this->response_library->responseJSON("1x0000-00000","Coupon not found."); 
                exit();
            }

            // print_r($coupon);
            // exit();

        }
        else
        {
            $this->response_library->responseJSON("1x0000-00000","The server respone Fails."); 
        }
    }
}