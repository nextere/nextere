<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Article extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $block_list = file_get_contents(base_url('api_area/home/article_list'));

        $block_list = json_decode($block_list);
        // echo '<pre>';
        // print_r($block_list);
        // exit();
        $block_list = $block_list->data->articles;

        $banner_list = file_get_contents(base_url('api_area/home/article_banner'));
        $banner_list = json_decode($banner_list);
        // echo '<pre>';
        // print_r($banner_list);
        // exit();
        $banner_list = $banner_list->data->article_banners;
        $this->load->view("Content", compact('block_list','banner_list'));

    }

    public function detail($id)
    {
        $block_list = file_get_contents(base_url('api_area/home/article_list'));

        $block_list = json_decode($block_list);
        // echo '<pre>';
        // print_r($block_list->data->articles);
        // exit();
        $block_list = $block_list->data->articles;
        $block_detail = array();
        foreach ($block_list as $key => $block_search){
            if($id == $block_search->id){
                 array_push($block_detail, $block_search); 
            }
        }
        $block_detail = $block_detail[0];

        // echo '<pre>';
        // print_r($block_detail);
        // exit();
        
        $block_show = file_get_contents(base_url('api_area/home/article_list'));
        $block_show = json_decode($block_show);
        $block_show = $block_show->data->articles;

        $block_limit = array();
        foreach ($block_show as $key => $block_show_list ){
            if(count($block_limit) <= 4 && $block_show_list->id != $id){
                array_push($block_limit,$block_show_list);
            }

        }
        // echo '<pre>';
        // print_r($block_limit);
        // exit();
        
        $article_relate = file_get_contents(base_url('api_area/home/article/'.$id));
        $article_relate = json_decode($article_relate);
        $article_relate = $article_relate->data->article_relate_alls;
        // echo '<pre>';
        // print_r($article_relate);
        // exit();


        $this->load->view("Content-Details", compact('block_limit','block_detail','article_relate'));

    } 
}
