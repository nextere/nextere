<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Account extends CI_Controller
{
    var $api_url;
    function __construct()
    {
        parent::__construct();
        $this->api_url = $this->config->item('WEBSITE_API_URL');
    }

    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function login()
    {
        $this->load->library('session');
        //  $url =  $this->config->item('WEBSITE_API_URL');
        // if (!empty($this->session->userdata('member::token'))) {
        // print_r($this->session->userdata('member::token'));
        // }

        if ($this->input->post()) {

            // echo '<pre>';
            // print_r($this->input->post());
            // exit();

            // $code = $this->input->get("code");
            // print_r($code);
            // exit();

            $postdata = http_build_query(
                array(
                    'email' => $this->input->post('email'),
                    'password' => $this->input->post('password'),
                    'token_facebook' => $this->input->post('token_facebook'),
                    'token_google' => $this->input->post('token_google'),
                    'token_line' => $this->input->post('token_line'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $login = file_get_contents($this->api_url . "api_area/member/login", false, $context);

            $login = json_decode($login);

            // $this->session->set_userdata('user_token', $login->data->token);

            // echo '<pre>';
            // print_r($login);
            // exit();

            if ($login->code == "0x0000-00000") {
                // $this->session->userdata('member::token') = $login->data->token;
                $this->session->set_userdata(array("member::token" => $login->data->token));
                $this->session->set_userdata(array("member::id" => $login->data->member_information->id));
                $this->session->set_userdata(array("member::email" => $login->data->member_information->email));
                $this->session->set_userdata(array("member::fullname" => $login->data->member_information->fullname));
                $this->session->set_userdata(array("member::gender" => $login->data->member_information->gender));
                $this->session->set_userdata(array("member::birth_date" => $login->data->member_information->birth_date));
                $this->session->set_userdata(array("member::tax_id" => $login->data->member_information->tax_id));
                $this->session->set_userdata(array("member::telephone" => $login->data->member_information->telephone));
                $this->session->set_userdata(array("member::status" => $login->data->member_information->status));

                $split_name =  explode(" ", $login->data->member_information->fullname);

                $first_name = $split_name[0];

                $this->session->set_userdata(array("member::first_name" => $first_name));
            }

            $this->load->library("response_library");
            $this->response_library->responseJSON($login->code, $login->message, $login->data);
        }

        $this->load->view("Login");
    }

    public function logout()
    {
        session_destroy();
        redirect(base_url());
    }

    public function line()
    {
        $this->load->library("response_library");

        $data =  $this->input->post('data');

        // $this->session->set_userdata(array("__website::facebook::email" => $data["email"]));
        // $this->session->set_userdata(array("__website::facebook::token" => $data["token"]));

        $postdata = http_build_query(
            array(
                'email' => $this->input->post('line_token'),
                'token_line' => $this->input->post('line_token'),
            )
        );
        // echo '<pre>';
        // print_r($this->input->post());
        // exit();
        $opts = array(
            'http' =>
            array(
                'method'  => 'POST',
                'header'  => array(
                    'Content-Type: application/x-www-form-urlencoded',
                    'Authorization: Basic ' . base64_encode('HouseVsSmartClick:')
                ),
                'content' => $postdata
            )
        );

        $context = stream_context_create($opts);

        $login = file_get_contents($this->api_url . "api_area/member/login", false, $context);
        // echo '<pre>';
        // print_r($login);
        // exit();

        $login = json_decode($login);
        if ($login->code == "0x0000-00000") {
            // $this->session->userdata('member::token') = $login->data->token;
            $this->session->set_userdata(array("member::token" => $login->data->token));
            $this->session->set_userdata(array("member::id" => $login->data->member_information->id));
            $this->session->set_userdata(array("member::email" => $login->data->member_information->email));
            $this->session->set_userdata(array("member::fullname" => $login->data->member_information->fullname));
            $this->session->set_userdata(array("member::gender" => $login->data->member_information->gender));
            $this->session->set_userdata(array("member::birth_date" => $login->data->member_information->birth_date));
            $this->session->set_userdata(array("member::tax_id" => $login->data->member_information->tax_id));
            $this->session->set_userdata(array("member::telephone" => $login->data->member_information->telephone));
            $this->session->set_userdata(array("member::status" => $login->data->member_information->status));

            $split_name =  explode(" ", $login->data->member_information->fullname);

            $first_name = $split_name[0];

            $this->session->set_userdata(array("member::first_name" => $first_name));
        }

        $this->load->library("response_library");
        $this->response_library->responseJSON($login->code, $login->message, $login->data);
    }


    public function Facebook_login()
    {
        $this->load->library("response_library");

        $data =  $this->input->post('data');

        // $this->session->set_userdata(array("__website::facebook::email" => $data["email"]));
        // $this->session->set_userdata(array("__website::facebook::token" => $data["token"]));

        $postdata = http_build_query(
            array(
                'email' => $data["email"],
                'token_facebook' => $data["id"],
            )
        );
        // echo '<pre>';
        // print_r($data);
        // exit();
        $opts = array(
            'http' =>
            array(
                'method'  => 'POST',
                'header'  => array(
                    'Content-Type: application/x-www-form-urlencoded',
                    'Authorization: Basic ' . base64_encode('HouseVsSmartClick:')
                ),
                'content' => $postdata
            )
        );

        $context = stream_context_create($opts);

        $login = file_get_contents($this->api_url . "api_area/member/login", false, $context);

        $login = json_decode($login);
        if ($login->code == "0x0000-00000") {
            // $this->session->userdata('member::token') = $login->data->token;
            $this->session->set_userdata(array("member::token" => $login->data->token));
            $this->session->set_userdata(array("member::id" => $login->data->member_information->id));
            $this->session->set_userdata(array("member::email" => $login->data->member_information->email));
            $this->session->set_userdata(array("member::fullname" => $login->data->member_information->fullname));
            $this->session->set_userdata(array("member::gender" => $login->data->member_information->gender));
            $this->session->set_userdata(array("member::birth_date" => $login->data->member_information->birth_date));
            $this->session->set_userdata(array("member::tax_id" => $login->data->member_information->tax_id));
            $this->session->set_userdata(array("member::telephone" => $login->data->member_information->telephone));
            $this->session->set_userdata(array("member::status" => $login->data->member_information->status));

            $split_name =  explode(" ", $login->data->member_information->fullname);

            $first_name = $split_name[0];

            $this->session->set_userdata(array("member::first_name" => $first_name));
        }

        $this->load->library("response_library");
        $this->response_library->responseJSON($login->code, $login->message, $login->data);
    }

    public function Google_login()
    {

        $this->load->library("response_library");
        $data = json_decode(file_get_contents('php://input'), true);

        //$data =  $this->input->post('data');

        // $this->session->set_userdata(array("__website::google::email" => $data["email"]));
        // $this->session->set_userdata(array("__website::google::token" => $data["token"]));

        $postdata = http_build_query(
            array(
                'email' => $data["email"],
                'token_google' => $data["token"],
            )
        );
        $opts = array(
            'http' =>
            array(
                'method'  => 'POST',
                'header'  => array(
                    'Content-Type: application/x-www-form-urlencoded',
                    'Authorization: Basic ' . base64_encode('HouseVsSmartClick:')
                ),
                'content' => $postdata
            )
        );

        $context = stream_context_create($opts);

        $login = file_get_contents($this->api_url . "api_area/member/login", false, $context);
        $login = json_decode($login);

        if ($login->code == "0x0000-00000") {
            // $this->session->userdata('member::token') = $login->data->token;
            $this->session->set_userdata(array("member::token" => $login->data->token));
            $this->session->set_userdata(array("member::id" => $login->data->member_information->id));
            $this->session->set_userdata(array("member::email" => $login->data->member_information->email));
            $this->session->set_userdata(array("member::fullname" => $login->data->member_information->fullname));
            $this->session->set_userdata(array("member::gender" => $login->data->member_information->gender));
            $this->session->set_userdata(array("member::birth_date" => $login->data->member_information->birth_date));
            $this->session->set_userdata(array("member::tax_id" => $login->data->member_information->tax_id));
            $this->session->set_userdata(array("member::telephone" => $login->data->member_information->telephone));
            $this->session->set_userdata(array("member::status" => $login->data->member_information->status));

            $split_name =  explode(" ", $login->data->member_information->fullname);

            $first_name = $split_name[0];

            $this->session->set_userdata(array("member::first_name" => $first_name));
        }

        $this->load->library("response_library");
        $this->response_library->responseJSON($login->code, $login->message, $login->data);
    }



    public function forget_password()
    {
        if ($this->input->post()) {

            // echo 'pre';
            // print_r($this->input->post());
            // exit();

            $postdata = http_build_query(
                array(
                    'email' => $this->input->post('email'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $forget_password = file_get_contents($this->api_url . "api_area/member/send_rememberPassword/", false, $context);

            $forget_password = json_decode($forget_password);

            $this->load->library("response_library");

            $this->response_library->responseJSON($forget_password->code, $forget_password->message, $forget_password->data);
        }

        $this->load->view("ForgetPass");
    }

    public function register()
    {
        if ($this->input->post()) {

            // echo "<pre>";
            // print_r($this->input->post());
            // exit();

            $postdata = http_build_query(
                array(
                    'fullname' => $this->input->post('name'),
                    // 'password' => $this->input->post('password'),
                    // 'gender' => $this->input->post('gender'),
                    // 'birdth_date' => $this->input->post('birdth_date'),
                    'email' => $this->input->post('email'),
                    // 'tax_id' => $this->input->post('tax_id'),
                    // 'telephone' => $this->input->post('teltphone'),
                    'token_facebook' => $this->input->post('facebook_token'),
                    'token_google' => $this->input->post('google_token'),
                    'token_line' => $this->input->post('line_token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $register = file_get_contents($this->api_url . "api_area/member/register", false, $context);

            $register = json_decode($register);

            $this->load->library("response_library");
            $this->response_library->responseJSON($register->code, $register->message, $register->data);
        }


        $this->load->view("Register");
    }

    public function profile()
    {
        if (!empty($this->session->userdata('member::token'))) {
            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $user_info = json_decode($user_info);

            $user_info =  $user_info->data->member;

            // echo "<pre>";
            // print_r($user_info);
            // exit();

            $this->load->view("Profile", compact('user_info'));
        } else {
            redirect(base_url('account/login'));
        }
    }

    public function profile_edit()
    {
        if (!empty($this->session->userdata('member::token'))) {
            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $id = json_decode($id);

            $id =  $id->data->member->id;

            // echo '';
            // print_r($id);
            // exit();

            if ($this->input->post()) {

                // echo 'pre';
                // print_r($this->input->post());
                // exit();

                $postdata = http_build_query(
                    array(
                        // 'fullname' => $this->input->post('name'),
                        'fullname' => $this->input->post('fullname'),
                        'email' => $this->input->post('email'),
                        'telephone' => $this->input->post('telephone'),
                        'gender' => $this->input->post('gender'),
                        'birth_date' => $this->input->post('birthday'),
                        'tax_id' => $this->input->post('tax'),
                        'token_facebook' => $this->input->post('token_facebook'),
                        'token_google' => $this->input->post('token_google'),
                        'token_line' => $this->input->post('token_line'),
                        'follow' => $this->input->post('follow'),
                    )
                );

                $opts = array(
                    'http' =>
                    array(
                        'method' => 'POST',
                        'header' => array(
                            'Content-type: application/x-www-form-urlencoded'
                        ),
                        'content' => $postdata
                    )
                );

                $context = stream_context_create($opts);

                $profile = file_get_contents($this->api_url . "api_area/member/edit_profile/" . $id, false, $context);

                $profile = json_decode($profile);

                $this->load->library("response_library");

                $this->response_library->responseJSON($profile->code, $profile->message, $profile->data);
            } else {
                $postdata = http_build_query(
                    array(
                        'token' => $this->session->userdata('member::token')
                    )
                );

                $opts = array(
                    'http' =>
                    array(
                        'method' => 'POST',
                        'header' => array(
                            'Content-type: application/x-www-form-urlencoded'
                        ),
                        'content' => $postdata
                    )
                );

                $context = stream_context_create($opts);

                $user_edit = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

                $user_edit = json_decode($user_edit);

                $user_edit =  $user_edit->data->member;
            }

            $this->load->view("Profile-edit", compact('user_edit'));
        } else {
            redirect(base_url('account/login'));
        }
    }

    public function profile_google()
    {
        if ($this->session->userdata('member::token') != "") {
            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $id = json_decode($id);

            $id =  $id->data->member->id;

            // echo '';
            // print_r($id);
            // exit();

            $this->load->library("response_library");
            $data = json_decode(file_get_contents('php://input'), true);


            // echo 'pre';
            // print_r($this->input->post());
            // exit();


            $postdata = http_build_query(
                array(
                    // 'fullname' => $this->input->post('name'),
                    'fullname' => $data["fullname"],
                    'email' => $data["email"],
                    'telephone' => $data["telephone"],
                    'gender' => $data["gender"],
                    'birth_date' => $data["birth_date"],
                    'tax_id' => $data["tax_id"],
                    'token_google' => $data["token"],
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $profile = file_get_contents($this->api_url . "api_area/member/edit_profile/" . $id, false, $context);

            $profile = json_decode($profile);

            $this->load->library("response_library");

            $this->response_library->responseJSON($profile->code, $profile->message, $profile->data);
        } else {
            redirect(base_url('account/login'));
        }
    }


    public function profile_line()
    {
        if ($this->session->userdata('member::token') != "") {
            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $id = json_decode($id);

            $id =  $id->data->member->id;

            // echo '';
            // print_r($id);
            // exit();

            if ($this->input->post()) {

                // echo 'pre';
                // print_r($this->input->post());
                // exit();

                $this->load->library("response_library");

                $data =  $this->input->post('data');

                // $this->session->set_userdata(array("__website::facebook::email" => $data["email"]));
                // $this->session->set_userdata(array("__website::facebook::token" => $data["token"]));

                $postdata = http_build_query(
                    array(
                        // 'email' => $data["email"],
                        'fullname' => $this->input->post('fullname'),
                        'email' => $this->input->post('email'),
                        'telephone' => $this->input->post('telephone'),
                        'gender' => $this->input->post('gender'),
                        'birth_date' => $this->input->post('birthday'),
                        'tax_id' => $this->input->post('tax'),
                        'token_line' => $this->input->post('line_token'),
                    )
                );
                // echo '<pre>';
                // print_r($postdata);
                // exit();
                $opts = array(
                    'http' =>
                    array(
                        'method'  => 'POST',
                        'header'  => array(
                            'Content-Type: application/x-www-form-urlencoded',
                            'Authorization: Basic ' . base64_encode('HouseVsSmartClick:')
                        ),
                        'content' => $postdata
                    )
                );
                $context = stream_context_create($opts);

                $profile = file_get_contents($this->api_url . "api_area/member/edit_profile/" . $id, false, $context);

                $profile = json_decode($profile);

                $this->load->library("response_library");

                $this->response_library->responseJSON($profile->code, $profile->message, $profile->data);
            } else {
                $postdata = http_build_query(
                    array(
                        'token' => $_SESSION['user_token']
                    )
                );

                $opts = array(
                    'http' =>
                    array(
                        'method' => 'POST',
                        'header' => array(
                            'Content-type: application/x-www-form-urlencoded'
                        ),
                        'content' => $postdata
                    )
                );

                $context = stream_context_create($opts);

                $user_edit = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

                $user_edit = json_decode($user_edit);

                $user_edit =  $user_edit->data->member;
            }

            $this->load->view("Profile-edit", compact('user_edit'));
        } else {
            redirect(base_url('account/login'));
        }
    }

    public function profile_facebook()
    {
        if ($this->session->userdata('member::token') != "") {
            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $id = json_decode($id);

            $id =  $id->data->member->id;

            // echo '';
            // print_r($id);
            // exit();

            if ($this->input->post()) {

                // echo 'pre';
                // print_r($this->input->post());
                // exit();

                $this->load->library("response_library");

                $data =  $this->input->post('data');

                // $this->session->set_userdata(array("__website::facebook::email" => $data["email"]));
                // $this->session->set_userdata(array("__website::facebook::token" => $data["token"]));

                $postdata = http_build_query(
                    array(
                        // 'email' => $data["email"],
                        'fullname' => $this->input->post('fullname'),
                        'email' => $this->input->post('email'),
                        'telephone' => $this->input->post('telephone'),
                        'gender' => $this->input->post('gender'),
                        'birth_date' => $this->input->post('birthday'),
                        'tax_id' => $this->input->post('tax'),
                        'token_facebook' => $data,
                    )
                );
                // echo '<pre>';
                // print_r($postdata);
                // exit();
                $opts = array(
                    'http' =>
                    array(
                        'method'  => 'POST',
                        'header'  => array(
                            'Content-Type: application/x-www-form-urlencoded',
                            'Authorization: Basic ' . base64_encode('HouseVsSmartClick:')
                        ),
                        'content' => $postdata
                    )
                );
                $context = stream_context_create($opts);

                $profile = file_get_contents($this->api_url . "api_area/member/edit_profile/" . $id, false, $context);

                $profile = json_decode($profile);

                $this->load->library("response_library");

                $this->response_library->responseJSON($profile->code, $profile->message, $profile->data);
            } else {
                $postdata = http_build_query(
                    array(
                        'token' => $_SESSION['user_token']
                    )
                );

                $opts = array(
                    'http' =>
                    array(
                        'method' => 'POST',
                        'header' => array(
                            'Content-type: application/x-www-form-urlencoded'
                        ),
                        'content' => $postdata
                    )
                );

                $context = stream_context_create($opts);

                $user_edit = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

                $user_edit = json_decode($user_edit);

                $user_edit =  $user_edit->data->member;
            }

            $this->load->view("Profile-edit", compact('user_edit'));
        } else {
            redirect(base_url('account/login'));
        }


        // $context = stream_context_create($opts);

        // $login = file_get_contents($this->api_url . "/api_area/member/edit_profile/", false, $context);





        // $this->load->library("response_library");
        // $this->response_library->responseJSON();
    }

    public function change_password()
    {
        if (!empty($this->session->userdata('member::token'))) {
            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $id = json_decode($id);

            $id =  $id->data->member->id;

            // echo 'pre';
            // print_r($id);
            // exit();

            if ($this->input->post()) {

                // echo 'pre';
                // print_r($this->input->post());
                // exit();

                $postdata = http_build_query(
                    array(
                        'old_password' => $this->input->post('old_password'),
                        'new_password' => $this->input->post('confirm_new_password'),
                    )
                );

                $opts = array(
                    'http' =>
                    array(
                        'method' => 'POST',
                        'header' => array(
                            'Content-type: application/x-www-form-urlencoded'
                        ),
                        'content' => $postdata
                    )
                );

                $context = stream_context_create($opts);

                $change_password = file_get_contents($this->api_url . "api_area/member/change_password/" . $id, false, $context);

                $change_password = json_decode($change_password);

                $this->load->library("response_library");

                $this->response_library->responseJSON($change_password->code, $change_password->message, $change_password->data);
            }
        } else {
            redirect(base_url('account/login'));
        }
    }

    // public function new_password()
    // {
    //     $this->load->view("change_password");
    // }
    public function invoice($id = null)
    {
        if (!empty($this->session->userdata('member::token'))) {

            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);
            $user_info = json_decode($user_info);
            $user_info =  $user_info->data->member;

            $member_id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);
            $member_id = json_decode($member_id);
            $member_id =  $member_id->data->member->id;

            $order_detail = file_get_contents($this->api_url . "api_area/member/product_history/" . $member_id);
            $order_detail = json_decode($order_detail);
            $order_detail = $order_detail->data->purchase_transactions;

            // echo "<pre>";
            // print_r($order_detail);
            // exit();

            $order_limit = array();

            foreach ($order_detail as $key => $order_detail_list) {
                if ($id == $order_detail_list->order_id) {
                    array_push($order_limit, $order_detail_list);

                    $order_product_history = array();

                    foreach ($order_detail_list->purchase_transaction_detail as $key => $order_product_search) {
                        $product_list = file_get_contents($this->api_url . 'api_area/product/product_detail/' . $order_product_search->product_id);
                        $product_list = json_decode($product_list);
                        // echo "<pre>";
                        // print_r($order_product_search->product_id);
                        // exit();
                        $product_list = $product_list->data->product_main;
                        array_push($order_product_history, $product_list);
                    }
                }
            }
            // echo "<pre>";
            // print_r($order_detail_list);
            // exit();
            // $order_limit = $order_limit->purchase_transaction_detail;



            // echo "<pre>";
            // print_r( $id_address);
            // exit();

            $contact = file_get_contents($this->api_url . 'api_area/home/contactus');
            $contact = json_decode($contact);
            $contact = $contact->data->contactus;


            $id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);
            $id = json_decode($id);
            $id =  $id->data->member->id;

            // echo "<pre>";
            // print_r($id);
            // exit();

            $address = file_get_contents($this->api_url . "api_area/member/address_all/" . $id);
            $address = json_decode($address);
            $address =  $address->data->member_address;

            foreach ($address as $key => $address_detail) {
                if ($id == $address_detail->member_id) {

                    $address_province = $this->get_province($address_detail->address_province_id);
                    $address_detail->province = $address_province;
                    // array_push($address_limit, $address);
                }
            }

            foreach ($address as $key => $address_detail) {
                if ($id == $address_detail->member_id) {

                    $address_district = $this->get_district($address_detail->address_district_id, $address_detail->address_province_id);
                    $address_detail->district = $address_district;
                    // array_push($address_limit, $address);


                }
            }

            foreach ($address as $key => $address_detail) {
                if ($id == $address_detail->member_id) {

                    $address_sub_district = $this->get_sub_district($address_detail->address_sub_district_id, $address_detail->address_district_id);
                    $address_detail->sub_district = $address_sub_district;
                    // array_push($address_limit, $address);
                }
            }


            $id_address = $order_limit[0]->member_address_id;
            $address_send = array();

            foreach ($address as $key => $address_search) {
                if ($id_address == $address_search->id) {
                    array_push($address_send, $address_search);
                }
            }

            $tax_address = file_get_contents($this->api_url . "api_area/member/address_tax_all/" . $id);
            $tax_address = json_decode($tax_address);
            $tax_address =  $tax_address->data->member_address;

            foreach ($tax_address as $key => $tax_address_detail) {
                if ($id == $tax_address_detail->member_id) {

                    $address_province = $this->get_province($tax_address_detail->address_province_id);
                    $tax_address_detail->province = $address_province;
                    // array_push($address_limit, $address);
                }
            }

            foreach ($tax_address as $key => $tax_address_detail) {
                if ($id == $tax_address_detail->member_id) {

                    $address_district = $this->get_district($tax_address_detail->address_district_id, $tax_address_detail->address_province_id);
                    $tax_address_detail->district = $address_district;
                    // array_push($address_limit, $address);


                }
            }

            foreach ($tax_address as $key => $tax_address_detail) {
                if ($id == $tax_address_detail->member_id) {

                    $address_sub_district = $this->get_sub_district($tax_address_detail->address_sub_district_id, $tax_address_detail->address_district_id);
                    $tax_address_detail->sub_district = $address_sub_district;
                    // array_push($address_limit, $address);
                }
            }

            $id_tax = $order_limit[0]->member_address_tax_id;
            $address_tax = array();

            foreach ($tax_address as $key => $tax_address_search) {
                if ($id_tax == $tax_address_search->id) {
                    array_push($address_tax, $tax_address_search);
                }
            }
            // echo "<pre>";
            // print_r($address_send);
            // exit();

            $this->load->view("invoice", compact('user_info', 'order_limit', 'order_product_history', 'contact', 'address_send', 'address_tax'));
        } else {
            redirect(base_url('account/login'));
        }
    }

    public function order_history()
    {
        if (!empty($this->session->userdata('member::token'))) {

            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $user_info = json_decode($user_info);

            $user_info = $user_info->data->member;

            $id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $id = json_decode($id);

            $id =  $id->data->member->id;

            // echo "<pre>";
            // print_r($user_info);
            // exit();

            $order = file_get_contents($this->api_url . "api_area/member/product_history/" . $id);
            $order = json_decode($order);
            $order = $order->data->purchase_transactions;
            // echo "<pre>";
            // print_r($order);
            // exit();
            $order_success = array();
            foreach ($order as $key => $order_limit) {
                if ($order_limit->status == 'SUCCESS') {
                    array_push($order_success, $order_limit);
                }
            }
            // echo "<pre>";
            // print_r($order_success);
            // exit();

            $this->load->view("HistorySale", compact('user_info', 'order', 'order_success'));
        } else {
            redirect(base_url('account/login'));
        }
    }
    public function order_details($id = null)
    {
        if (!empty($this->session->userdata('member::token'))) {

            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);
            $user_info = json_decode($user_info);
            $user_info =  $user_info->data->member;

            $member_id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);
            $member_id = json_decode($member_id);
            $member_id =  $member_id->data->member->id;

            $order_detail = file_get_contents($this->api_url . "api_area/member/product_history/" . $member_id);
            $order_detail = json_decode($order_detail);
            $order_detail = $order_detail->data->purchase_transactions;

            // echo "<pre>";
            // print_r($order_detail);
            // exit();

            $order_limit = array();

            foreach ($order_detail as $key => $order_detail_list) {
                if ($id == $order_detail_list->order_id) {
                    array_push($order_limit, $order_detail_list);

                    $order_product_history = array();

                    foreach ($order_detail_list->purchase_transaction_detail as $key => $order_product_search) {
                        $product_list = file_get_contents($this->api_url . 'api_area/product/product_detail/' . $order_product_search->product_id);
                        $product_list = json_decode($product_list);
                        // echo "<pre>";
                        // print_r($order_product_search->product_id);
                        // exit();
                        $product_list = $product_list->data->product_main;
                        array_push($order_product_history, $product_list);
                    }
                }
            }
            // echo "<pre>";
            // print_r($order_detail_list);
            // exit();
            // $order_limit = $order_limit->purchase_transaction_detail;



            // echo "<pre>";
            // print_r( $id_address);
            // exit();

            $contact = file_get_contents($this->api_url . 'api_area/home/contactus');
            $contact = json_decode($contact);
            $contact = $contact->data->contactus;


            $id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);
            $id = json_decode($id);
            $id =  $id->data->member->id;

            // echo "<pre>";
            // print_r($id);
            // exit();

            $address = file_get_contents($this->api_url . "api_area/member/address_all/" . $id);
            $address = json_decode($address);
            $address =  $address->data->member_address;

            foreach ($address as $key => $address_detail) {
                if ($id == $address_detail->member_id) {

                    $address_province = $this->get_province($address_detail->address_province_id);
                    $address_detail->province = $address_province;
                    // array_push($address_limit, $address);
                }
            }

            foreach ($address as $key => $address_detail) {
                if ($id == $address_detail->member_id) {

                    $address_district = $this->get_district($address_detail->address_district_id, $address_detail->address_province_id);
                    $address_detail->district = $address_district;
                    // array_push($address_limit, $address);


                }
            }

            foreach ($address as $key => $address_detail) {
                if ($id == $address_detail->member_id) {

                    $address_sub_district = $this->get_sub_district($address_detail->address_sub_district_id, $address_detail->address_district_id);
                    $address_detail->sub_district = $address_sub_district;
                    // array_push($address_limit, $address);
                }
            }


            $id_address = $order_limit[0]->member_address_id;
            $address_send = array();

            foreach ($address as $key => $address_search) {
                if ($id_address == $address_search->id) {
                    array_push($address_send, $address_search);
                }
            }

            $tax_address = file_get_contents($this->api_url . "api_area/member/address_tax_all/" . $id);
            $tax_address = json_decode($tax_address);
            $tax_address =  $tax_address->data->member_address;

            foreach ($tax_address as $key => $tax_address_detail) {
                if ($id == $tax_address_detail->member_id) {

                    $address_province = $this->get_province($tax_address_detail->address_province_id);
                    $tax_address_detail->province = $address_province;
                    // array_push($address_limit, $address);
                }
            }

            foreach ($tax_address as $key => $tax_address_detail) {
                if ($id == $tax_address_detail->member_id) {

                    $address_district = $this->get_district($tax_address_detail->address_district_id, $tax_address_detail->address_province_id);
                    $tax_address_detail->district = $address_district;
                    // array_push($address_limit, $address);


                }
            }

            foreach ($tax_address as $key => $tax_address_detail) {
                if ($id == $tax_address_detail->member_id) {

                    $address_sub_district = $this->get_sub_district($tax_address_detail->address_sub_district_id, $tax_address_detail->address_district_id);
                    $tax_address_detail->sub_district = $address_sub_district;
                    // array_push($address_limit, $address);
                }
            }

            $id_tax = $order_limit[0]->member_address_tax_id;
            $address_tax = array();

            foreach ($tax_address as $key => $tax_address_search) {
                if ($id_tax == $tax_address_search->id) {
                    array_push($address_tax, $tax_address_search);
                }
            }
            // echo "<pre>";
            // print_r($address_send);
            // exit();

            $this->load->view("HistorySale-Details", compact('user_info', 'order_limit', 'order_product_history', 'contact', 'address_send', 'address_tax'));
        } else {
            redirect(base_url('account/login'));
        }
    }

    public function quotation_request_history()
    {
        if (!empty($this->session->userdata('member::token'))) {
            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $user_info = json_decode($user_info);

            $user_info =  $user_info->data->member;

            $id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $id = json_decode($id);

            $id =  $id->data->member->id;

            // echo '<pre>';
            // print_r($id);
            // exit();

            $bill_quotation = file_get_contents($this->api_url . "api_area/product/get_quotation/" . $id);
            $bill_quotation = json_decode($bill_quotation);
            if ($bill_quotation->data == null) {
                $bill_quotation = $bill_quotation->data;
            } else {
                $bill_quotation = $bill_quotation->data->quotations;
            }

            // echo '<pre>';
            // print_r($bill_quotation);
            // exit();

            // $quotation_detail = array();

            // foreach ($bill_quotation as $key => $quotation) {
            //     if ($quotation_id == $quotation->id) {
            //         array_push($quotation_detail, $quotation);
            //     }
            // }

            $this->load->view("RequestHistory", compact('user_info', 'bill_quotation'));
        } else {
            redirect(base_url('account/login'));
        }
    }
    public function quotation_request_detail($id = null)
    {
        if (!empty($this->session->userdata('member::token'))) {
            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $user_info = json_decode($user_info);

            $user_info =  $user_info->data->member;

            $member_id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $member_id = json_decode($member_id);

            $member_id =  $member_id->data->member->id;

            $bill_quotation = file_get_contents($this->api_url . "api_area/product/get_quotation/" . $member_id);

            $bill_quotation = json_decode($bill_quotation);

            $bill_quotation = $bill_quotation->data->quotations;

            $quotation_detail = array();

            foreach ($bill_quotation as $key => $quotation) {
                if ($id == $quotation->quotation_no) {
                    array_push($quotation_detail, $quotation);
                }
            }

            // echo '<pre>';
            // print_r($quotation_detail[0]);
            // exit();

            $business_type = file_get_contents($this->api_url . 'api_area/home/business_type');
            $business_type = json_decode($business_type);
            $business_type = $business_type->data->business_types;


            $contact = file_get_contents($this->api_url . 'api_area/home/contactus');
            $contact = json_decode($contact);
            $contact = $contact->data->contactus;

            // echo '<pre>';
            // print_r($contact);
            // exit();

            $this->load->view("RequestHistory-Details", compact('user_info', 'quotation_detail', 'business_type', 'contact'));
        } else {
            redirect(base_url('account/login'));
        }
    }

    public function sending_address()
    {
        if (!empty($this->session->userdata('member::token'))) {
            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $id = json_decode($id);

            // echo "<pre>";
            // print_r($id);
            // exit();

            $id =  $id->data->member->id;

            $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $user_info = json_decode($user_info);

            $user_info =  $user_info->data->member;

            // echo "<pre>";
            // print_r($user_info);
            // exit();

            $address = file_get_contents($this->api_url . "api_area/member/address_all/" . $id);
            $address = json_decode($address);
            $address =  $address->data->member_address;

            foreach ($address as $key => $address_detail) {
                if ($id == $address_detail->member_id) {

                    $address_province = $this->get_province($address_detail->address_province_id);
                    $address_detail->province = $address_province;
                    // array_push($address_limit, $address);
                }
            }

            foreach ($address as $key => $address_detail) {
                if ($id == $address_detail->member_id) {

                    $address_district = $this->get_district($address_detail->address_district_id, $address_detail->address_province_id);
                    $address_detail->district = $address_district;
                    // array_push($address_limit, $address);


                }
            }

            foreach ($address as $key => $address_detail) {
                if ($id == $address_detail->member_id) {

                    $address_sub_district = $this->get_sub_district($address_detail->address_sub_district_id, $address_detail->address_district_id);
                    $address_detail->sub_district = $address_sub_district;
                    // array_push($address_limit, $address);
                }
            }

            // echo "<pre>";
            // print_r($address);
            // exit();

            $this->load->view("Delivery-address", compact('address', 'user_info'));
        } else {
            redirect(base_url('account/login'));
        }
    }

    public function remove_address()
    {
        $remove_address = file_get_contents($this->api_url . 'api_area/member/remove_address/' . $this->input->post('id'));

        $remove_address = json_decode($remove_address);

        $this->load->library("response_library");

        $this->response_library->responseJSON($remove_address->code, $remove_address->message, $remove_address->data);
    }

    public function sending_address_add()
    {
        if (!empty($this->session->userdata('member::token'))) {
            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $id = json_decode($id);

            $id =  $id->data->member->id;

            $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $user_info = json_decode($user_info);

            $user_info =  $user_info->data->member;

            if ($this->input->post()) {

                $postdata = http_build_query(
                    array(
                        'member_id' => $id,
                        'first_name' => $this->input->post('name'),
                        'last_name' => $this->input->post('lastname'),
                        'telephone' => $this->input->post('telephone'),
                        'email' => $this->input->post('email'),
                        'company_name' => $this->input->post('company'),
                        'company_branch' => $this->input->post('branch'),
                        'address' => $this->input->post('address'),
                        'address_province_id' => $this->input->post('province'),
                        'address_district_id' => $this->input->post('district'),
                        'address_sub_district_id' => $this->input->post('sub_district'),
                        'postcode' => $this->input->post('postcode'),
                        'is_default' => $this->input->post('is_default'),
                    )
                );

                $opts = array(
                    'http' =>
                    array(
                        'method' => 'POST',
                        'header' => array(
                            'Content-type: application/x-www-form-urlencoded'
                        ),
                        'content' => $postdata
                    )
                );


                $context = stream_context_create($opts);

                $address_add = file_get_contents($this->api_url . "api_area/member/add_address/", false, $context);

                $address_add = json_decode($address_add);

                // echo "<pre>";
                // print_r($address_add);
                // exit();

                $this->load->library("response_library");

                $this->response_library->responseJSON($address_add->code, $address_add->message, $address_add->data);
            } else {

                $api_url =  $this->api_url;
            }

            $this->load->view("Delivery-address-add", compact('api_url', 'user_info'));
        } else {
            redirect(base_url('account/login'));
        }
    }

    public function get_postcode()
    {
        if ($this->input->post()) {

            $postcode = file_get_contents($this->api_url . "api_area/home/select2/address_district/" . $this->input->post('province_id') . '?search=' . $this->input->post('district_name'));

            $postcode = json_decode($postcode);

            // $postcode = $postcode->data[0];

            // echo "<pre>";
            // print_r($postcode);
            // exit();

            $this->load->library("response_library");

            $this->response_library->responseJSON($postcode->code, $postcode->message, $postcode->data[0]);
        }
    }

    public function sending_address_edit($id = null)
    {
        if (!empty($this->session->userdata('member::token'))) {

            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $member_id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $member_id = json_decode($member_id);

            $member_id =  $member_id->data->member->id;

            $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $user_info = json_decode($user_info);

            $user_info =  $user_info->data->member;

            $address = file_get_contents($this->api_url . "api_area/member/address_all/" . $member_id);
            $address = json_decode($address);
            $address =  $address->data->member_address;

            $address_limit = array();

            foreach ($address as $key => $address) {
                if ($id == $address->id) {
                    array_push($address_limit, $address);
                }
            }

            // echo "<pre>";
            // print_r($address_limit);
            // exit();

            if ($this->input->post()) {

                $postdata = http_build_query(
                    array(
                        'member_id' => $member_id,
                        'first_name' => $this->input->post('name'),
                        'last_name' => $this->input->post('lastname'),
                        'telephone' => $this->input->post('telephone'),
                        'email' => $this->input->post('email'),
                        'company_name' => $this->input->post('company'),
                        'company_branch' => $this->input->post('branch'),
                        'address' => $this->input->post('address'),
                        'address_province_id' => $this->input->post('province'),
                        'address_district_id' => $this->input->post('district'),
                        'address_sub_district_id' => $this->input->post('sub_district'),
                        'postcode' => $this->input->post('postcode'),
                        'is_default' => $this->input->post('is_default'),
                    )
                );

                $opts = array(
                    'http' =>
                    array(
                        'method' => 'POST',
                        'header' => array(
                            'Content-type: application/x-www-form-urlencoded'
                        ),
                        'content' => $postdata
                    )
                );

                $context = stream_context_create($opts);

                $address_edit = file_get_contents($this->api_url . "api_area/member/add_address/" . $this->input->post('address_id'), false, $context);

                $address_edit = json_decode($address_edit);

                // echo "<pre>";
                // print_r($address_edit);
                // exit();

                $this->load->library("response_library");

                $this->response_library->responseJSON($address_edit->code, $address_edit->message, $address_edit->data);
            } else {
                $api_url =  $this->api_url;
            }

            $this->load->view("Delivery-address-edit", compact('address_limit', 'api_url', 'user_info'));
        } else {
            redirect(base_url('account/login'));
        }
    }

    public function tax_invoice_address()
    {
        if (!empty($this->session->userdata('member::token'))) {
            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $id = json_decode($id);

            $id =  $id->data->member->id;

            $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $user_info = json_decode($user_info);

            $user_info =  $user_info->data->member;

            $tax_address = file_get_contents($this->api_url . "api_area/member/address_tax_all/" . $id);

            $tax_address = json_decode($tax_address);

            $tax_address =  $tax_address->data->member_address;

            // echo "<pre>";
            // print_r($tax_address);
            // exit();

            foreach ($tax_address as $key => $tax_address_detail) {
                if ($id == $tax_address_detail->member_id) {

                    $address_province = $this->get_province($tax_address_detail->address_province_id);
                    $tax_address_detail->province = $address_province;
                    // array_push($address_limit, $address);
                }
            }

            foreach ($tax_address as $key => $tax_address_detail) {
                if ($id == $tax_address_detail->member_id) {

                    $address_district = $this->get_district($tax_address_detail->address_district_id, $tax_address_detail->address_province_id);
                    $tax_address_detail->district = $address_district;
                    // array_push($address_limit, $address);


                }
            }

            foreach ($tax_address as $key => $tax_address_detail) {
                if ($id == $tax_address_detail->member_id) {

                    $address_sub_district = $this->get_sub_district($tax_address_detail->address_sub_district_id, $tax_address_detail->address_district_id);
                    $tax_address_detail->sub_district = $address_sub_district;
                    // array_push($address_limit, $address);
                }
            }

            // echo "<pre>";
            // print_r($tax_address);
            // exit();

            $this->load->view("Vat-address", compact('tax_address', 'user_info'));
        } else {
            redirect(base_url('account/login'));
        }
    }

    public function remove_address_tax()
    {
        $remove_address_tax = file_get_contents($this->api_url . 'api_area/member/remove_address_tax/' . $this->input->post('id'));

        $remove_address_tax = json_decode($remove_address_tax);

        $this->load->library("response_library");

        $this->response_library->responseJSON($remove_address_tax->code, $remove_address_tax->message, $remove_address_tax->data);
    }

    public function tax_invoice_address_edit($id = null)
    {
        if (!empty($this->session->userdata('member::token'))) {
            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $member_id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $member_id = json_decode($member_id);

            $member_id =  $member_id->data->member->id;

            $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $user_info = json_decode($user_info);

            $user_info =  $user_info->data->member;

            $tax_address = file_get_contents($this->api_url . "api_area/member/address_tax_all/" . $member_id);

            $tax_address = json_decode($tax_address);

            $tax_address =  $tax_address->data->member_address;

            $tax_address_limit = array();

            foreach ($tax_address as $key => $tax_address) {
                if ($id == $tax_address->id) {
                    array_push($tax_address_limit, $tax_address);
                }
            }

            // echo "<pre>";
            // print_r($tax_address_limit);
            // exit();

            if ($this->input->post()) {

                $postdata = http_build_query(
                    array(
                        'member_id' => $member_id,
                        'first_name' => $this->input->post('name'),
                        'last_name' => $this->input->post('lastname'),
                        'telephone' => $this->input->post('telephone'),
                        'email' => $this->input->post('email'),
                        'company_name' => $this->input->post('company'),
                        'company_branch' => $this->input->post('branch'),
                        'address' => $this->input->post('address'),
                        'address_province_id' => $this->input->post('province'),
                        'address_district_id' => $this->input->post('district'),
                        'address_sub_district_id' => $this->input->post('sub_district'),
                        'postcode' => $this->input->post('postcode'),
                        'tax_id' => $this->input->post('tax'),
                        'is_default' => $this->input->post('is_default'),
                    )
                );

                $opts = array(
                    'http' =>
                    array(
                        'method' => 'POST',
                        'header' => array(
                            'Content-type: application/x-www-form-urlencoded'
                        ),
                        'content' => $postdata
                    )
                );

                $context = stream_context_create($opts);

                $tax_address_edit = file_get_contents($this->api_url . "api_area/member/add_address_tax/" . $this->input->post('address_id'), false, $context);

                $tax_address_edit = json_decode($tax_address_edit);

                // echo "<pre>";
                // print_r($tax_address_limit);
                // exit();

                $this->load->library("response_library");

                $this->response_library->responseJSON($tax_address_edit->code, $tax_address_edit->message, $tax_address_edit->data);
            } else {
                $api_url =  $this->api_url;
            }

            $this->load->view("Vat-address-edit", compact('tax_address_limit', 'api_url', 'user_info'));
        } else {
            redirect(base_url('account/login'));
        }
    }

    public function tax_invoice_address_add()
    {
        if (!empty($this->session->userdata('member::token'))) {
            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $id = json_decode($id);

            $id =  $id->data->member->id;

            $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $user_info = json_decode($user_info);

            $user_info =  $user_info->data->member;

            if ($this->input->post()) {

                $postdata = http_build_query(
                    array(
                        'member_id' => $id,
                        'first_name' => $this->input->post('name'),
                        'last_name' => $this->input->post('lastname'),
                        'telephone' => $this->input->post('telephone'),
                        'email' => $this->input->post('email'),
                        'company_name' => $this->input->post('company'),
                        'company_branch' => $this->input->post('branch'),
                        'address' => $this->input->post('address'),
                        'address_province_id' => $this->input->post('province'),
                        'address_district_id' => $this->input->post('district'),
                        'address_sub_district_id' => $this->input->post('sub_district'),
                        'postcode' => $this->input->post('postcode'),
                        'tax_id' => $this->input->post('tax'),
                        'is_default' => $this->input->post('is_default'),
                    )
                );

                $opts = array(
                    'http' =>
                    array(
                        'method' => 'POST',
                        'header' => array(
                            'Content-type: application/x-www-form-urlencoded'
                        ),
                        'content' => $postdata
                    )
                );

                $context = stream_context_create($opts);

                $address_tax_add = file_get_contents($this->api_url . "api_area/member/add_address_tax/", false, $context);

                $address_tax_add = json_decode($address_tax_add);

                // echo "<pre>";
                // print_r($address_add);
                // exit();

                $this->load->library("response_library");

                $this->response_library->responseJSON($address_tax_add->code, $address_tax_add->message, $address_tax_add->data);
            } else {

                $api_url =  $this->api_url;
            }

            // $this->load->view("Delivery-address-add", compact('api_url', 'address_province', 'address_district', 'address_sub_district'));

            $this->load->view("Vat-address-add", compact('api_url', 'user_info'));
        } else {
            redirect(base_url('account/login'));
        }
    }

    public function payment()
    {
        if (!empty($this->session->userdata('member::token'))) {
            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $user_info = json_decode($user_info);

            $user_info =  $user_info->data->member;

            $this->load->view("Card", compact('user_info'));
        } else {
            redirect(base_url('account/login'));
        }
    }

    public function payment_add()
    {
        if (!empty($this->session->userdata('member::token'))) {
            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $user_info = json_decode($user_info);

            $user_info =  $user_info->data->member;

            $this->load->view("Card-add", compact('user_info'));
        } else {
            redirect(base_url('account/login'));
        }
    }

    public function datasheet()
    {
        if (!empty($this->session->userdata('member::token'))) {
            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $user_info = json_decode($user_info);

            $user_info =  $user_info->data->member;

            $this->load->view("Datasheet", compact('user_info'));
        } else {
            redirect(base_url('account/login'));
        }
    }
    public function review($id = null)
    {
        if (!empty($this->session->userdata('member::token'))) {

            $postdata = http_build_query(
                array(
                    'token' => $this->session->userdata('member::token'),
                )
            );

            $opts = array(
                'http' =>
                array(
                    'method' => 'POST',
                    'header' => array(
                        'Content-type: application/x-www-form-urlencoded'
                    ),
                    'content' => $postdata
                )
            );

            $context = stream_context_create($opts);

            $user_info = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $user_info = json_decode($user_info);

            $user_info =  $user_info->data->member;

            $member_id = file_get_contents($this->api_url . "api_area/member/information/", false, $context);

            $member_id = json_decode($member_id);

            $member_id =  $member_id->data->member->id;

            $order_detail = file_get_contents($this->api_url . "api_area/member/product_history/" . $member_id);
            $order_detail = json_decode($order_detail);
            $order_detail = $order_detail->data->purchase_transactions;

            $order_limit = array();

            foreach ($order_detail as $key => $order_detail) {
                if ($id == $order_detail->id) {
                    array_push($order_limit, $order_detail);
                }
            }

            // echo "<pre>";
            // print_r($order_limit);
            // exit();

            $order_product_history = array();

            foreach ($order_detail->purchase_transaction_detail as $key => $order_product_search) {
                $product_list = file_get_contents($this->api_url . 'api_area/product/product_detail/' . $order_product_search->product_id);
                $product_list = json_decode($product_list);
                // echo "<pre>";
                // print_r($order_product_search->product_id);
                // exit();
                $product_list = $product_list->data->product_main;
                array_push($order_product_history, $product_list);
            }

            // $order_limit = $order_limit->purchase_transaction_detail;

            // echo "<pre>";
            // print_r($order_limit);
            // exit();
            $this->load->view("create_review", compact('user_info', 'order_limit', 'order_product_history'));
        } else {
            redirect(base_url('account/login'));
        }
    }

    function image_decode($base64_string = null, $output_file = null)
    {
        // open the output file for writing
        $ifp = fopen($output_file, 'wb');

        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode(',', $base64_string);

        // we could add validation here with ensuring count( $data ) > 1
        fwrite($ifp, base64_decode($data[1]));

        // clean up the file resource
        fclose($ifp);

        return $output_file;
    }

    public function create_review()
    {
        $data = $this->input->post();
        // echo "<pre>";
        // print_r($data);
        // exit();
        if ($this->input->post()) {
            // echo "<pre>";
            // print_r($this->input->post());
            // exit();

            // $data = json_decode($this->input->post('data'));

            foreach ($this->input->post() as $key => $review) {
                // $image = array();
                // foreach ($review->image as $img_key => $img_review) {
                //     array_push($image, $this->image_decode($img_review->dataURL, $img_review->upload->filename));
                // }

                // echo "<pre>";
                // print_r($review);
                // exit();

                $postdata = http_build_query(
                    array(
                        'product_id' => $review->product_id,
                        'member_id' => $review->member_id,
                        'purchase_transaction_detail_id' => $review->purchase_transaction_detail_id,
                        'rating' => $review->rating,
                        'message' => $review->message,
                        'image_paths' => $_FILES,
                    )
                );

                $opts = array(
                    'http' =>
                    array(
                        'method' => 'POST',
                        'header' => array(
                            'Content-type: application/x-www-form-urlencoded'
                        ),
                        'content' => $postdata
                    )
                );

                $context = stream_context_create($opts);

                $review = file_get_contents($this->api_url . "api_area/Product/review/", false, $context);

                $review = json_decode($review);

                // echo "<pre>";
                // print_r($postdata);
                // exit();

                if ($review->code != "0x0000-00000") {
                    $this->load->library("response_library");
                    $this->response_library->responseJSON($review->code, $review->message, $review->data);
                }
            }

            $this->load->library("response_library");
            $this->response_library->responseJSON($review->code, $review->message, $review->data);
        }

        // $this->load->view("create_review");
    }

    function get_province($id = null)
    {
        if (!empty($id)) {
            $address_province = file_get_contents($this->api_url . "api_area/home/select2/address_province");

            $address_province = json_decode($address_province);

            $address_province =  $address_province->data;

            foreach ($address_province as $key => $value) {
                if ($value->id == $id) {
                    return $value;
                }
            }
        } else {
            return new stdClass();
        }
    }

    function get_district($id = null, $province_id = null)
    {
        if (!empty($id)) {
            $address_district = file_get_contents($this->api_url . "api_area/home/select2/address_district/" . $province_id);

            $address_district = json_decode($address_district);

            $address_district =  $address_district->data;

            foreach ($address_district as $key => $value) {
                if ($value->id == $id) {
                    return $value;
                }
            }
        } else {
            return new stdClass();
        }
    }

    function get_sub_district($id = null, $district_id = null)
    {
        if (!empty($id)) {
            $address_sub_district = file_get_contents($this->api_url . "api_area/home/select2/address_sub_district/" . $district_id);

            $address_sub_district = json_decode($address_sub_district);

            $address_sub_district =  $address_sub_district->data;

            foreach ($address_sub_district as $key => $value) {
                if ($value->id == $id) {
                    return $value;
                }
            }
        } else {
            return new stdClass();
        }
    }
}
