<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payment extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function Delivery()
    {
        $this->load->view("Payment");
    }

    public function Not_delivery()
    {
        $this->load->view("Payment-Not-Delivery");
    }

    public function AddCreditcard()
    {
        $this->load->view("Add-creditcard");
    }

    public function AddCreditcard2()
    {
        $this->load->view("Add-creditcard2");
    }

    public function Bill()
    {
        $this->load->view("Bill");
    }
    public function Bill_fail()
    {
        $this->load->view("Bill_fail");
    }

}