<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Header extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /** ------------------------------------------------------- **
     **           MANDATORY SCRIPT
    /** ------------------------------------------------------- **/
    public function index()
    {
        $submenu = file_get_contents(base_url('/api_area/home/menu2'));
        $index_article = file_get_contents(base_url('/api_area/home/article'));

        $submenu = json_decode($submenu);
        $submenu = $submenu->data;

       
        $$this->load->view("header", compact('submenu'));
    }
}
