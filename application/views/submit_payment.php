<?php include('header.php') ?>
<!-- <link href="../assets/css/style-content.css" rel="stylesheet"> -->
<style>
    .bill .dashed {
        border: 1px dashed #BCBCBC;
        padding: 30px;
    }

    .bill .btn {
        border-radius: 20px;
        font-size: 12px;
    }
</style>

<body>
    <section class="bill">
        <div class="col-12  " style="background-color: #EFEFEF; text-align: center; padding:70px;">
            <h4><b>การชําระเงินเสร็จสิ้น</b></h4>
            <h4><b>ขอบคุณทีไว้วางใจ Nextere</b></h4>
        </div>
        <div class="container">
            <div class="row justify-content-center mt-3">
                <div class="col-3" style="text-align: left; font-weight: bold;">
                    <p>เลขทีใบสังซือ</p>
                    <p>วันทีสังสินค้า</p>
                    <p>ประเภทการชําระเงิน</p>
                </div>
                <div class="col-3" style="text-align-last: right; font-weight: bold;">
                    <p>#0000012</p>
                    <p>8 พฤศจิกายน พ.ศ. 2563 เวลา 00.24 น.</p>
                    <p>Mobile Banking (AIS mPay)</p>
                </div>
                <hr style="width:50%; margin-left:25% !important; margin-right:25% !important; margin-top:15px !important;" />
                <div class="col-12" style="text-align:center;">
                    <b> *ใบเสนอราคาของคุณจะถูกส่งเข้าไปยังอีเมล Loremipsum@gmail.com ของคุณ</b>
                </div>
                <div class="col-12 mt-3" style="text-align:center;">
                    <b>รายละเอียดคําสังซือ</b>
                </div>
                <div class="row justify-content-center mt-3">
                    <div class="col-7 dashed">
                        <div class="row">
                            <div class="col-8 line">
                                <img class="w-20" src="<?php echo base_url('/assets/img/squre.jpg') ?>" alt="">
                                <div class="mt-3" style="text-align:left ;padding-left: 20px;">
                                    <B>Model no. S888 -ET-2255U</b>
                                </div>
                            </div>
                            <div class="col-4" style="text-align: right;"><b>จำนวน : 100 </b></div>
                            <hr class="mt-3" style="color: #BCBCBC ;">
                            <div class="col-8 line">
                                <img class="w-20" src="<?php echo base_url('/assets/img/squre.jpg') ?>" alt="">
                                <div class="mt-3" style="text-align:left ;padding-left: 20px;">
                                    <B>Model no. S888 -ET-2255U</b>
                                </div>
                            </div>
                            <div class="col-4" style="text-align: right;"><b>จำนวน : 100 </b></div>
                            <hr class="mt-3" style="color: #BCBCBC ;">
                        </div>

                    </div>
                    <div class="col-6 mt-3" style="text-align:right;">
                        <a href="<?php echo base_url('') ?>"> <button type="button" class="btn btn-outline-dark mb-5" style="width: 250px; height: 40px; padding-top: 5px;"><b>กลับสู่หน้าแรก</b></button></a>
                    </div>
                    <div class="col-6 mt-3">
                        <a href="<?php echo base_url('/Account/order_history') ?>"> <button type="button" class="btn btn-secondary nb-5" style="width: 250px; height: 40px; padding-top: 5px;"><b>ดูประวัติการสังซื้อ</b></button></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-5"> <?php include('footer.php') ?></div>
    </section>
</body>

</html>