<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <title>Quotation</title> -->
    <link href="<?php echo assetsDirectory("global/plugins/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/simple-line-icons/simple-line-icons.min.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"); ?>" rel="stylesheet" type="text/css" />

    <!-- PLUGINS AREA -->


    <!-- THEME STYLE AREA -->
    <link href="<?php echo assetsDirectory("global/css/components-md.min.css"); ?>" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo assetsDirectory("global/css/plugins-md.min.css"); ?>" rel="stylesheet" type="text/css" />

    <!-- LAYOUT AREA -->
    <link href="<?php echo assetsDirectory("layouts/layout/css/layout.min.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("layouts/layout/css/custom.min.css"); ?>" rel="stylesheet" type="text/css" />
</head>
<style>
    @media print {
        @page {
            margin-top: 0;
            margin-bottom: 0;
        }

        body {
            padding-top: 72px;
            padding-bottom: 72px;
        }

        .color_grey {
            color: #aaaaaa !important;
        }

        .mt-3 {
            margin-top: 1.5rem !important;
        }

    }

    .footer {
        position: fixed;
        left: 0;
        bottom: 0;
    }

    .color_grey {
        color: #aaaaaa !important;
    }

    .mt-3 {
        margin-top: 1.5rem !important;
    }
</style>
<?php
// echo "<pre>";
// print_r($address_send[0]->sub_district->name);
// exit();

?>

<body>

    <table width="100%" border="0">
        <br><br>
        <tr>
            <td width="50%">
                <img src="<?php echo base_url($contact->logo_path) ?>" alt="" style="width: 164px;">
                <p style="margin: 5px;"><?php echo $contact->company_th ?></p>
                <p style="margin: 5px;"><?php echo $contact->address_th ?></p>
                <p style="margin: 5px;"><?php echo 'เลขประจำตัวผูเสียภาษี ' . $contact->tax_identification_number ?></p>
                <p style="margin: 5px;"><?php echo 'โทร ' . $contact->telephone ?></p>
                <p style="margin: 5px;"><?php echo 'เบอร์มือถือ ' . $contact->phone ?></p>
                <p style="margin: 5px;"><?php echo base_url() ?></p>
            </td>
            <td>
                <h4 class="color_grey" style="white-space: nowrap; text-align:center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ใบส่งสินค้า/ใบแจ้งหนี้/ใบกำกับภาษี</h4>
                <h5 class="color_grey" style="white-space: nowrap; text-align:center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ต้นฉบับ (เอกสารออกเป็นชุด)</h5>
                <hr style="margin: 5px;">
                <p style="margin: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="color_grey">เลขที่</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $order_limit[0]->order_id ?></p>
                <p style="margin: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="color_grey">วันที่</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo date("d/m/Y"); ?></p>
                <p style="margin: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="color_grey">ผู้ขาย</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nextere admin</p>
                <p style="margin: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="color_grey">อ้างอิง</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>

                <hr style="margin: 5px;">

                <p style="margin: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="color_grey">ชื่องาน</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nextere website</p>
                <p class="mt-3" style="margin: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="color_grey">ผู้ติดต่อ</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $user_info->fullname ?></p>
                <p style="margin: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="color_grey">เบอร์โทร</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $user_info->telephone ?></p>
                <p style="margin: 5px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="color_grey">อีเมล</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $user_info->email ?></p>
            </td>
        </tr>
        <tr>
            <td>
                <h5 class="color_grey" style="margin: 0px;">ลูกค้า</h5>
                <p style="margin: 5px;">
                    <?php echo $user_info->fullname ?><br>
                    <?php echo 'เลขประจำตัวผูเสียภาษี ' . $user_info->tax_id ?>

                </p>
                
                <?php  if ($address_send != "") { ?>
                    <span><b>ที่อยู่ :</b></span>
                    <span>
                        <?php echo $address_send[0]->address ?>
                        <?php echo $address_send[0]->sub_district->name ?>
                        <?php echo $address_send[0]->district->name ?>
                        <?php echo $address_send[0]->province->name ?>
                        <?php echo $address_send[0]->sub_district->code ?>
                    </span>
                <?php } else { ?>
                    <span>ที่อยู่ : - </span>
                <?php } ?>
            </td>
        </tr>
    </table>
    <br>
    <table width="100%" class="table table-bordered">
        <thead style=" background-color: lightgray;border: none; color: white;">
            <tr>
                <th style="text-align: center;" width="5%">#</th>
                <th style="text-align: center;" width="50%">รายละเอียด</th>
                <th style="text-align: center;" width="10%">จำนาน</th>
                <th style="text-align: center;" width="20%">ราคาต่อหน่วย</th>
                <th style="text-align: center;" width="20%">ยอดรวม</th>
            </tr>
        </thead>
        <tbody>
            <?php $total = 0; ?>
            <?php foreach ($order_limit[0]->purchase_transaction_detail as $key => $order_detail) {
                $key++; ?>
                <tr>
                    <?php foreach ($order_product_history as $key => $order_product) { ?>
                        <?php if ($order_product->{'product::id'} == $order_detail->product_id) { ?>
                            <td style="text-align: center;"><?php echo $key; ?></td>
                            <td style="text-align: center;"><?php echo getVariable($order_product, 'product::name')  ?></td>
                            <td style="text-align: center;"><?php echo  $order_detail->qty ?></td>
                            <td style="text-align: center;">
                                <?php echo number_format($order_detail->price, 2); ?></td>
                            <td style="text-align: center;">
                                <?php echo number_format($order_detail->total_price, 2); ?></td>
                        <?php } ?>
                    <?php } ?>
                </tr>
                <?php $total  += $order_detail->total_price; ?>

            <?php } ?>

        </tbody>
    </table>

    <p style="text-align: end;margin: 5px;">
        <span class="color_grey">รวมเป็นเงิน</span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <?php echo number_format($total, 2); ?>
        บาท
    </p>
    <p style="text-align: end;margin: 5px;">
        <span class="color_grey">ค่าจัดส่ง</span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <?php echo number_format($order_limit[0]->delivery_price, 2); ?>
        บาท
    </p>
    <p style="text-align: end;margin: 5px;">
        <span class="color_grey">ส่วนลด</span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <?php echo number_format($order_limit[0]->discount, 2); ?>
        บาท
    </p>
    <p style="text-align: end;margin: 5px;">
        <span class="color_grey">จำนวนเงินหลักหักส่วนลด</span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <?php echo number_format($order_limit[0]->net_price, 2); ?>
        บาท
    </p>
    <p style="text-align: end;margin: 5px;">
        <span class="color_grey">ภาษีมูลค่าเพิ่ม 7%</span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <?php echo number_format(($order_limit[0]->net_price * (7 / 100)), 2); ?>
        บาท
    </p>
    <p style="text-align: end;margin: 5px;">
        <span class="color_grey">จำนานเงินรวมทั้งสิ้น</span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <?php echo number_format($order_limit[0]->net_price + ($order_limit[0]->net_price * (7 / 100)), 2); ?>
        บาท
    </p>
    <p id="AMOUNT" style="text-align: center;font-size: 17px;"></p>
    <br><br><br><br><br><br><br>
    <!-- <table width="100%" border="0">
    <tr>
        <td width="65%">
        หมายเหตุ
        </td>
    </tr>
</table> -->
    <div class="footer">
        <table width="100%" border="0">
            <tr>
                <td style="text-align: center;" width="15%">
                    <p style="padding-bottom: 40px;">ในนามบริษัท</p>
                    <hr>
                    <p>ผู้รับสินค้า/บริการ</p>
                </td>
                <td style="text-align: center;" width="1%">

                </td>
                <td style="text-align: center;" width="15%">
                    <p style="padding-bottom: 40px;">&nbsp;</p>
                    <hr>
                    <p>วันที่</p>
                </td>
                <td style="text-align: center;" width="10%">

                </td>
                <td style="text-align: center;" width="15%">
                    <p style="padding-bottom: 40px;">&nbsp;</p>
                    <hr>
                    <p>ผู้อนุมัติ</p>
                </td>
                <td style="text-align: center;" width="1%">

                </td>
                <td style="text-align: center;" width="15%">
                    <p style="padding-bottom: 40px;">ในนามบริษัท ทูเนียร์ จํากัด</p>
                    <hr>
                    <p>วันที่</p>
                </td>
            </tr>
        </table>
    </div>



</body>
<script src="<?php echo assetsDirectory("global/plugins/jquery.min.js"); ?>" type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/bootstrap/js/bootstrap.min.js"); ?>" type="text/javascript">
</script>
<script src="<?php echo assetsDirectory("global/plugins/js.cookie.min.js"); ?>" type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"); ?>" type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"); ?>" type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/jquery.blockui.min.js"); ?>" type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"); ?>" type="text/javascript"></script>

<!-- THEME SCRIPT AREA -->
<script src="<?php echo assetsDirectory("global/scripts/app.min.js"); ?>" type="text/javascript"></script>

<!-- LAYOUT AREA -->
<script src="<?php echo assetsDirectory("layouts/layout/scripts/layout.min.js"); ?>" type="text/javascript"></script>
<!-- CUSTOM AREA -->
<script src="<?php echo assetsDirectory("js/thaibath.js"); ?>"></script>
<script>
    var thaibath = ArabicNumberToText("<?php echo number_format($order_limit[0]->net_price + ($order_limit[0]->net_price * (7 / 100)), 2); ?>");
    $("#AMOUNT").html('(' + thaibath + ')');
</script>
<script>
    $(document).ready(function() {
        window.print();
    });
</script>

</html>