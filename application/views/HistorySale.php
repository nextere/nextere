<?php
include('header.php');
// echo '<pre>';
// print_r($order_success);
// exit();

?>
<style>
    .Datasheet ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 100%;
        background-color: white;
        border: 1px solid #BCBCBC;
        opacity: 1;
        font-size: 16px;
    }

    .Datasheet li a.active {
        background-color: #BCBCBC;
        color: white;
    }

    .Datasheet li a {
        display: block;
        color: #000;
        padding: 8px 12px;
        text-decoration: none;
        font-size: 14px;
    }

    select {

        /* styling */
        background-color: white;
        border: thin solid lightgray;
        border-radius: 5px;
        display: inline-block;
        line-height: 1.5em;
        padding: 0.5em 3.5em 0.5em 1em;


        -webkit-appearance: none;
        -moz-appearance: none;
    }

    select.minimal {
        background-image:
            linear-gradient(45deg, transparent 50%, gray 50%),
            linear-gradient(135deg, gray 50%, transparent 50%),
            linear-gradient(to right, #ccc, #ccc);
        background-position:
            calc(100% - 21px) calc(1em + 2px),
            calc(100% - 16px) calc(1em + 2px),
            calc(100% - 2.5em) 0.5em;
        background-size:
            5px 5px,
            5px 5px,
            1px 1.5em;
        background-repeat: no-repeat;
    }

    #sampleTableA input {
        display: none;
    }

    #sampleTableA thead tr {
        background: #e5e5e5;
    }

    #sampleTableA tbody tr {
        background-color: #fff !important;
    }

    #sampleTableA tbody tr {
        border: none;
        background-color: white;
    }

    #sampleTableA .fancySearchRow th {
        padding: 0 !important;
        border: none !important;
    }

    #sampleTableA tfoot {
        text-align: center;
    }

    .Datasheet .btn-light.active {
        color: #000 !important;
        background-color: lightgray !important;
        border-color: gray;
        width: 30px;
        height: 30px;
    }

    .Datasheet .btn-light {
        color: black !important;
        background-color: white !important;
        border-color: gray;
        width: 30px;
        height: 30px;
    }
</style>

<body>
    <section class="Datasheet">
        <div class="container mt-5">
            <div class="row">
                <div class="col-sm-12 col-xl-12 pt-5 pb-5 pl-5 mt-5" style="background-color: lightgray; text-align:left">
                    <h4><b><?php echo getWording('profile', 'my_account') ?></b></h4>
                    <p><?php echo getWording('profile', 'hi') ?> <?php echo $user_info->fullname ?>, <?php echo getWording('profile', 'detail') ?></p>
                </div>
                <div class="col-sm-4 col-xl-3 mt-3 pl-0">
                    <ul>
                        <li><a href="<?php echo base_url('Account/profile') ?>"> <img class="mr-3" style="width: 16px; height: 16px;" src="<?php echo base_url('/assets/img/icon/user.png') ?>"><?php echo getWording('profile', 'profile') ?></a></li>
                        <li><a class="active" href="<?php echo base_url('Account/order_history') ?>"><img class="mr-3" style="width: 20px; height: 18px;" src="<?php echo base_url('/assets/img/icon/shopping-cart-2.png') ?>"><?php echo getWording('profile', 'my_orders') ?></a></li>
                        <li><a href="<?php echo base_url('Account/quotation_request_history') ?>"><img class="mr-3" style="width: 18px; height: 18px;" src="<?php echo base_url('/assets/img/icon/Invoice.png') ?>"><?php echo getWording('profile', 'quotation') ?></a></li>
                        <li><a href="<?php echo base_url('Account/sending_address') ?>"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php echo base_url('/assets/img/icon/delivery-truck.png') ?>"><?php echo getWording('profile', 'shipping_address') ?></a></li>
                        <li><a href="<?php echo base_url('Account/tax_invoice_address') ?>"><img class="mr-3" style="width: 16px; height: 16px;" src="<?php echo base_url('/assets/img/icon/location-2.png') ?>"><?php echo getWording('profile', 'tax_invoice') ?></a></li>
                        <!-- <li><a href="<?php echo base_url('Account/payment') ?>"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php echo base_url('/assets/img/icon/credit-card.png') ?>"><?php echo getWording('profile', 'credit') ?></a></li> -->
                        <li><a href="<?php echo base_url('Account/datasheet') ?>"><img class="mr-3" style="width: 16px; height: 20px;" src="<?php echo base_url('/assets/img/icon/Datasheet.png') ?>"><?php echo getWording('profile', 'datasheet') ?></a></li>
                    </ul>
                </div>
                <div class="col-sm-8 col-xl-9">
                    <div class="row">
                        <div class="col-sm-12 col-xl-12 mt-3">
                            <h3 style="font-weight: bold"><?php echo getWording('profile', 'my_orders') ?></h3>
                        </div>
                        <div class="container">
                            <div class="my-5">
                                <label for="year">Year</label>
                                <select class="minimal" id="year" name="year">
                                    <option value="2021">2021</option>
                                    <option value="2020">2020</option>
                                    <option value="2019">2019</option>
                                    <option value="2018">2018</option>
                                </select>
                                <label for="year">Month</label>
                                <select class="minimal" id="months" name="months">
                                    <option value=""><?php echo getWording('login', 'all') ?></option>
                                    <option value="01"><?php echo getWording('login', 'jan') ?></option>
                                    <option value="02"><?php echo getWording('login', 'feb') ?></option>
                                    <option value="03"><?php echo getWording('login', 'mar') ?></option>
                                    <option value="04"><?php echo getWording('login', 'apr') ?></option>
                                    <option value="05"><?php echo getWording('login', 'may') ?></option>
                                    <option value="06"><?php echo getWording('login', 'jun') ?></option>
                                    <option value="07"><?php echo getWording('login', 'jul') ?></option>
                                    <option value="08"><?php echo getWording('login', 'aug') ?></option>
                                    <option value="09"><?php echo getWording('login', 'sep') ?></option>
                                    <option value="10"><?php echo getWording('login', 'oct') ?></option>
                                    <option value="11"><?php echo getWording('login', 'nov') ?></option>
                                    <option value="12"><?php echo getWording('login', 'dec') ?></option>
                                </select>
                                <button onclick="search(document.getElementById('year').value,document.getElementById('months').value)" class="btn btn-secondary"><?php echo getWording('profile', 'search') ?></button>
                            </div>
                            <hr>
                            <table id="sampleTableA" class="table sampleTable">
                                <thead style="border: 1px solid; border-color: lightgray;">
                                    <tr>
                                        <th class=""><?php echo getWording('profile', 'order_number') ?></th>
                                        <th class=""><?php echo getWording('profile', 'transaction_date') ?></th>
                                        <th class=""><?php echo getWording('profile', 'total') ?></th>
                                        <th class=""><?php echo getWording('profile', 'status') ?></th>
                                        <th class=""><?php echo getWording('profile', 'download') ?></th>
                                        <th class=""></th>
                                    </tr>
                                </thead>
                                <tbody style="border: 1px solid; border-color: lightgray;">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="mt-5"> <?php include('footer.php') ?></div>
    <script type="text/javascript">
        function search(year, months) {
            var search_word = year + "-" + months;
            console.log(year + ' ' + months);
            $('#sampleTableA input').val(search_word);
            $('#sampleTableA input').change();
        }
        // Word genarator
        var sources = function() {
            var result = [
                <?php foreach ($order_success as $key => $order_item) {
                    echo "['" . $order_item->order_id . "','" . $order_item->created_date . "','" . number_format($order_item->net_price) . "','" . $order_item->status . "','" . $order_item->order_id . "','" . $order_item->order_id . "'],";
                } ?>

            ];
            return result;
        }();

        console.log(sources[0][0]);
        console.log(sources[0].length);

        $(document).ready(function() {
            // Generate a big table
            for (var x = 0; x < sources.length; x++) {
                var row = $("<tr>");
                $("#sampleTableA").find("thead th").each(function(index, item) {
                    // console.log(sources[3][index]);
                    if (index == 0) {
                        $("<td>", {
                            html: "<a href='order_details/" + sources[x][index] + "'>" + sources[x][index] + "</a>",
                        }).appendTo($(row));
                    } else if (index == 4) {

                        $("<td >", {
                            html: "<a target='_blank' href='<?php echo base_url('account/invoice') ?>" + "/" + +sources[x][index] + "'><img class='w-auto me-2' src='<?php echo base_url("assets/img/icon/Download.png") ?>'><?php echo getWording('profile', 'invoice') ?></a>",
                        }).appendTo($(row));

                    } else if (index == 5) {
                        $("<td>", {
                            html: "<a target='_blank' href='https://www.nextere.space/Account/invoice/" + sources[x][index] + "'><img class='w-auto me-2' src='<?php echo base_url("assets/img/icon/Download.png") ?>'><?php echo getWording('profile', 'tax_invoice') ?></a>",
                        }).appendTo($(row));
                    } else if (index == 3){
                        $("<td text-align: right; padding-right: 10px;>", {
                            html: sources[x][index],
                        }).appendTo($(row));
                    } else {
                        $("<td>", {
                            html: sources[x][index],
                        }).appendTo($(row));
                    }
                });
                row.appendTo($("#sampleTableA").find("tbody"));
            }
            // Load data simulation
            $("#loadDataSimulation").click(function() {
                $(fancyTableA).find("tbody").empty();
                for (var n = 0; n < 1000; n++) {
                    var row = $("<tr>");
                    $(fancyTableA).find("thead tr:nth-child(1) th").each(function() {
                        $("<td>", {
                            html: rWord(8),
                            style: "padding:2px;"
                        }).appendTo($(row));
                    });
                    row.appendTo($(fancyTableA).find("tbody"));
                }
                fancyTableA.reinit();
            });


            // And make them fancy
            var fancyTableA = $("#sampleTableA").fancyTable({
                sortColumn: 0,
                pagination: true,
                perPage: 10,
                globalSearch: true
            });

        });
    </script>

</body>

</html>