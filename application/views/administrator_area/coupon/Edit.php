<!-- ------------------------------------------------------------------ -->
<!-- PERMISSION CHECK IS BEGIN                                          -->
<!-- ------------------------------------------------------------------ -->
<?php
$codeigniter_instance =& get_instance();

$configurations = array(
  "PAGE_TITLE" => "Administrator System",
  "PAGE_HEADER" => array(
   "MAIN_TITLE" => "Coupon",
   "SUB_TITLE" => ""
  ), 
  "PORTLET_HEADER" => array(
    "ICON" => "fa fa-bars",
    "TITLE" => "Coupon Detail"
  )
);
?>
<!-- ------------------------------------------------------------------ -->
<!-- HEADER IS BEGIN                                                    -->
<!-- ------------------------------------------------------------------ -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
  <head>
    <title><?php echo $this->config->item('WEBSITE_NAME');?> | Login System</title>
    <link href="<?php echo faviconDirectory($this->config->item('WEBSITE_FAVICON'));?>" rel="shortcut icon" />

    <!-- META TAG AREA -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- MANDATORY STYLE AREA -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/simple-line-icons/simple-line-icons.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-switch/css/bootstrap-switch.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- PLUGINS AREA -->
    <link href="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/clockface/css/clockface.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2-bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/jstree/dist/themes/default/style.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/dropzone/basic.min.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- THEME STYLE AREA -->
    <link href="<?php echo assetsDirectory("global/css/components-md.min.css");?>" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo assetsDirectory("global/css/plugins-md.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- LAYOUT AREA -->
    <link href="<?php echo assetsDirectory("layouts/layout/css/layout.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("layouts/layout/css/themes/light.min.css");?>" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?php echo assetsDirectory("layouts/layout/css/custom.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- CUSTOM AREA -->
    <style>
      .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice {
        color: #fff;
        background: #53c6d2;
        border: 1px solid #53c6d2;
        margin: 5px 0 0 6px;
        padding: 0 35px 0 6px;
      }
      .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice__remove {
        margin-right: 30px;
        color: #e7505a;
      }
      .product-image {
        height: 150px;
        margin-top: 10px;
      }
    </style>
  </head>

  <!-- ------------------------------------------------------------------ -->
  <!-- BODY IS BEGIN                                                      -->
  <!-- ------------------------------------------------------------------ -->
  <body class="page-header-fixed page-sidebar-closed-hide-logo page-md">
    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueHeader",array());?>
    <div class="page-container">
      <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueSideBar",array());?>
      <div class="page-content-wrapper">
        <div class="page-content">
          <h3 class="page-title">
              <?php echo $configurations["PAGE_HEADER"]["MAIN_TITLE"]?>
              <small>
                  <?php echo $configurations["PAGE_HEADER"]["SUB_TITLE"]?>
              </small>
          </h3>
          <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueBreadCrumb",array());?>

          <!-- ------------------------------------------------------------------ -->
          <!-- CONTENT IS BEGIN                                                   -->
          <!-- ------------------------------------------------------------------ -->
          
          <div class="row">
            <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light bordered">
                <div class="portlet-title">
                  <div class="caption font-green">
                    <i class="<?php echo $configurations["PORTLET_HEADER"]["ICON"]?> font-green"></i>
                    <span class="caption-subject bold uppercase">
                      <?php echo $configurations["PORTLET_HEADER"]["TITLE"]?>
                    </span>
                  </div>
                </div>
                <div class="portlet-body">
                  <form class="form-horizontal" role="form" method="post" id="main-form" enctype="multipart/form-data"> 
                    <input type="hidden" name="session_time" value="<?php echo date("YmdHis")?>">
                    <div class="form-group">
                      <label class="col-md-2 control-label">Code<span class="required"> *</span></label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="code" autocomplete="off" placeholder="Code" value="<?php if(isset($coupon->code))  echo $coupon->code; ?>">
                      </div>
                    </div>
                    <div class="form-group"> 
                      <label class="col-md-2 control-label">Total Qty<span class="required"> *</span></label>
                      <div class="col-md-3">
                        <input type="text" class="form-control number" name="total_qty" autocomplete="off" placeholder="0" value="<?php if(isset($coupon->total_qty))  echo $coupon->total_qty; ?>">
                      </div>
                      <label class="col-md-3 control-label">Remaining Qty</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control number" name="remaining_qty" autocomplete="off" placeholder="0" value="<?php if(isset($coupon->remaining_qty))  echo $coupon->remaining_qty; ?>" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">วัน/เวลาที่เริ่ม</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control datetime-picker" name="start_time" autocomplete="off" value="<?php if(isset($coupon->start_time)) echo date("d F Y H:i", strtotime($coupon->start_time))?>" data-default="<?php if(isset($coupon->start_time)) echo date("d F Y H:i", strtotime($coupon->start_time))?>">
                      </div>
                      <label class="col-md-3 control-label">ถึง</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control datetime-picker" name="end_time" autocomplete="off" value="<?php if(isset($coupon->end_time)) echo date("d F Y H:i", strtotime($coupon->end_time))?>" data-default="<?php if(isset($coupon->end_time)) echo date("d F Y H:i", strtotime($coupon->end_time))?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Discount(%)</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control number" name="discount_percent" placeholder="0" autocomplete="off" value="<?php if(isset($coupon->discount_percent))  echo $coupon->discount_percent*100 ?>">
                        <small style="font-style: normal;color: lightslategray;">(ระบุเมื่อมีการลดแบบเปอร์เซนต์(%))</small>
                      </div>
                      <label class="col-md-3 control-label">Discount(Bant)</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control number" name="discount_baht" placeholder="0" autocomplete="off" value="<?php if(isset($coupon->discount_baht))  echo $coupon->discount_baht ?>">
                        <small style="font-style: normal;color: lightslategray;">(ระบุเมื่อมีการลดบาท)</small>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">เฉพาะราคาปกติ</label>
                      <div class="col-md-3">
                        <div class="mt-checkbox-inline">
                          <label class="mt-checkbox mt-checkbox-outline mt-checkbox-disabled">
                             <input type="checkbox" name="is_onlynormalprice" value="1" <?php if(isset($coupon->is_onlynormalprice) && $coupon->is_onlynormalprice == 1) echo "checked"?> >
                             <span></span>
                          </label>                                  
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">ประเภทส่วนลด<span class="required"> *</span></label>
                      <div class="col-md-4">
                        <div class="mt-radio-inline">
                          <label class="mt-radio">
                            <input type="radio" name="is_delivery" value="1" <?php if(isset($coupon->is_delivery) && $coupon->is_delivery == 1)echo "checked"?>> ค่าจัดส่ง
                            <span></span>
                          </label>
                          <label class="mt-radio">
                            <input type="radio" name="is_delivery" value="0" <?php if(!isset($coupon->is_delivery) || $coupon->is_delivery != 1)echo "checked"?>> ท้ายบิล
                            <span></span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Status<span class="required"> *</span></label>
                      <div class="col-md-4">
                        <div class="mt-radio-inline">
                          <label class="mt-radio">
                            <input type="radio" name="status" value="ACTIVATE" <?php if(isset($coupon->status) && $coupon->status == "ACTIVATE")echo "checked"?>> Activate
                            <span></span>
                          </label>
                          <label class="mt-radio">
                            <input type="radio" name="status" value="SUSPEND" <?php if(!isset($coupon->status) || $coupon->status != "ACTIVATE")echo "checked"?>> Suspend
                            <span></span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                          <button type="button" class="btn green-jungle" data-toggle="modal" data-target="#confirmation-save" data-backdrop="static" data-keyboard="false">Save Changes</button>
                          <button type="button" class="btn red btn-outline" data-toggle="modal" data-target="#confirmation-cancel" data-backdrop="static" data-keyboard="false">Cancel</button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
            </div>
          </div>
          <?php if(isset($coupon->id)) {?>
            <div class="row">
              <div class="col-md-12">
                <div class="portlet light bordered" >
                  <div class="portlet-title">
                    <div class="caption font-green" >
                      <span class="caption-subject bold uppercase">
                        หมวดหมู่สินค้าที่ไม่ร่วมรายการ
                      </span>
                    </div>
                  </div>
                  <div class="portlet-body">
                    <form class="form-horizontal" role="form" method="post" id="category-form" enctype="multipart/form-data">
                      <input type="hidden" name="session_time" value="<?php echo date("YmdHis")?>">
                      <div class="form-group">
                        <label class="col-md-2 control-label">Category<span class="required"> *</span></label>
                        <div class="col-md-3">
                          <select class="form-control select2" name="product_category_id">
                          </select>
                        </div>
                        <div class="col-md-3">
                          <button type="button" class="btn green-jungle create-category-button" data-coupon_id="<?php echo $coupon->id?>">Add</button>
                        </div>
                      </div>
                    </form>
                    <hr>
                    <table class="table table-striped table-bordered table-hover order-column coupon-except-category-list" data-coupon_id="<?php echo $coupon->id?>">
                      <thead>
                        <tr>
                          <th > Category</th>
                          <th style="width:20%;"> Action </th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="portlet light bordered" >
                  <div class="portlet-title">
                    <div class="caption font-green" >
                      <span class="caption-subject bold uppercase">
                        สินค้าที่ไม่ร่วมรายการ
                      </span>
                    </div>
                  </div>
                  <div class="portlet-body">
                    <form class="form-horizontal" role="form" method="post" id="product-form" enctype="multipart/form-data">
                      <input type="hidden" name="session_time" value="<?php echo date("YmdHis")?>">
                      <div class="form-group">
                        <label class="col-md-2 control-label">Product Name<span class="required"> *</span></label>
                        <div class="col-md-5">
                          <select class="form-control select2" name="product_id">
                          </select>
                        </div>
                        <div class="col-md-2">
                          <button type="button" class="btn green-jungle create-product-button" data-coupon_id="<?php echo $coupon->id?>">Add</button>
                        </div>
                      </div>
                    </form>
                    <hr>
                    <table class="table table-striped table-bordered table-hover order-column coupon-except-product-list" data-coupon_id="<?php echo $coupon->id?>">
                      <thead>
                        <tr>
                          <th > Code</th>
                          <th > Name (TH)</th>
                          <th > Model (TH)</th>
                          <th style="width:20%;"> Action </th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>

    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueFooter",array());?>
  </body>

  <!-- ------------------------------------------------------------------ -->
  <!-- JAVASCRIPT IS BEGIN                                                -->
  <!-- ------------------------------------------------------------------ -->
  <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/respond.min.js');?>"></script> <![endif]-->
  <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/excanvas.min.js');?>"></script> <![endif]-->

  <!-- MANDATORY SCRIPT AREA -->
  <script src="<?php echo assetsDirectory("global/plugins/jquery.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap/js/bootstrap.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/js.cookie.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-slimscroll/jquery.slimscroll.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery.blockui.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-switch/js/bootstrap-switch.min.js");?>" type="text/javascript"></script>

  <!-- PLUGINS AREA -->
  <script src="<?php echo assetsDirectory("global/plugins/jquery-ui/jquery-ui.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/moment.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/clockface/js/clockface.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/select2/js/select2.full.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/jquery.validate.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/additional-methods.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.js");?>"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote-cleaner.js");?>"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jstree/dist/jstree.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/scripts/datatable.js");?>" type="text/javascript"></script>

  <script src="<?php echo assetsDirectory("global/plugins/backstretch/jquery.backstretch.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.js");?>" type="text/javascript"></script>
    

  <!-- THEME SCRIPT AREA -->
  <script src="<?php echo assetsDirectory("global/scripts/app.min.js");?>" type="text/javascript"></script>

  <!-- LAYOUT AREA -->
  <script src="<?php echo assetsDirectory("layouts/layout/scripts/layout.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("layouts/layout/scripts/demo.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("layouts/global/scripts/quick-sidebar.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery.numeric.js");?>" type="text/javascript"></script>

  <!-- CUSTOM AREA -->
  <script>
    jQuery(document).ready(function() {
      $('.number').numeric({ decimal: false, negative: false });
      $('.pos-decimal').numeric({ negative: false });
      $(".datetime-picker").datetimepicker({
        todayHighlight: true,
        autoclose: true,
        format: "dd MM yyyy hh:ii",
        altField  : ".datetime-picker"
      });
      
      $("select[name='product_category_id']").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/coupon/select2/edit_product_category")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term,
              coupon_id: '<?php echo isset($coupon->id) ? $coupon->id : "";?>'
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_th };
              })
            };
          }
        }
      });

      $("select[name='product_id']").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/coupon/select2/edit_product")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term,
              coupon_id: '<?php echo isset($coupon->id) ? $coupon->id : "";?>'
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_th +" ("+obj.code+")" };
              })
            };
          }
        }
      });
      /** =================================================================== **/
      /** FORM VALIDATE                                                       **/
      /** =================================================================== **/


      $("#main-form").validate({
        errorElement: 'span', errorClass: 'help-block', focusInvalid: false,
        rules: {
          code: {required: true},
          total_qty: {required:true},
          status: {required: true}
        },
        highlight: function (element) { $(element).closest('.form-group').addClass('has-error'); },
        success: function (label) { label.closest('.form-group').removeClass('has-error'); label.remove(); },
        invalidHandler: function(form, validator) { $('.confirmation-save').modal('toggle'); },
        errorPlacement: function (error, element) { error.appendTo(element.closest('div')); },
        submitHandler: function() {
          App.blockUI({ target: 'body', animate: true });
          $.ajax({
            type: "POST",
            <?php
            $post_url = "";
            if(isset($coupon->id)){
              $post_url = base_url("administrator_area/coupon/edit/".$coupon->id);
            }else{
              $post_url = base_url("administrator_area/coupon/edit");
            }
            ?>
            url: "<?php echo $post_url?>",
            data: new FormData($('#main-form')[0]),
            cache: false,
            contentType: false,
            processData: false,
            success: function(response){
              try {
                if(response.code == "0x0000-00000") {
                  toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "positionClass": "toast-bottom-right",
                  "onclick": null,
                  "showDuration": "1000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                toastr.success('Update Completed', 'Success.');
                window.location.assign("<?php echo base_url("administrator_area/coupon/edit")?>/"+response.data);
                }else{
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch(e) {
                $("#system-return-failed").modal("toggle");
              }
              App.unblockUI('body');
            },
            error: function(){
              $("#system-disconnected").modal("toggle");
              App.unblockUI('body');
            }
          });
        }
      });

      //Category
      $(".coupon-except-category-list").dataTable({
          processing: true,
          serverSide: true,
          pageLength: 25,
          searching: false,
          ajax:{
            url: "<?php echo base_url("administrator_area/coupon/datatables/index_except_product_category")?>",
            type: "POST",
            data:{
              coupon_id: $(".coupon-except-category-list").data("coupon_id")
            }
          },
          columns: [
            {
              data: 'product_category_name'
            },
            {
              render: function(data, type, row, meta){
                var action_header = '<button type="button" class="btn red-thunderbird open-remove-category" data-id="'+row["id"]+'"> Delete <i class="fa fa-trash"></i></button>';
                return action_header;
              },
              searchable: false,
              orderable: false
            }
          ]
      });

      $(document).on("click", ".create-category-button",function(){
        var coupon_id = $(this).data("coupon_id");
        var product_category_id = $("#category-form select[name='product_category_id']").val();
        if ((product_category_id == null )) {
          $("#system-return-error .message").html("Invalid Data");
          $("#system-return-error").modal("toggle");
        }
        else{
          var formData = new FormData();
          formData.append('coupon_id', coupon_id);
          formData.append('product_category_id', product_category_id);

          $.ajax({
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            url: "<?php echo base_url("administrator_area/coupon/edit_expept_category")?>",
            success: function (response) {
              try {
                if (response.code == "0x0000-00000") {
                  var table = $(".coupon-except-category-list").DataTable();
                  table.columns().search("").draw();

                  toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                  toastr.success('Update Completed', 'Success.');
                  $("#category-form select[name='product_category_id']").select2("val", "");
                } else {
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch (e) {
                $("#system-return-failed").modal("toggle");
              }
            },
            error: function () {
              $("#system-disconnected").modal("toggle");
            }
          });
        }
      });

      $(document).on("click", ".open-remove-category", function(){
        $("#remove-category .category-id").val('');
        
        $("#remove-category .category-id").val($(this).data('id'));
        $('#remove-category').modal('show');
      });

      $(document).on("click", ".category-remove-button", function(){
        var id = $("#remove-category .category-id").val();
        $("#remove-category").modal('hide');

        var formData = new FormData();
        formData.append('id', id);

        $.ajax({
          data: formData,
          type: "POST",
          contentType: false,
          processData: false,
          url: "<?php echo base_url("administrator_area/coupon/delete_expept_category")?>",
          success: function (response) {
            try {
              if (response.code == "0x0000-00000") {
                var table = $(".coupon-except-category-list").DataTable();
                table.columns().search("").draw();

                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "positionClass": "toast-bottom-right",
                  "onclick": null,
                  "showDuration": "1000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                toastr.success('Update Completed', 'Success');
              } else {
                $("#system-return-error .message").html(response.message);
                $("#system-return-error").modal("toggle");
              }
            } catch (e) {
              $("#system-return-failed").modal("toggle");
            }
          },
          error: function () {
            $("#system-disconnected").modal("toggle");
          }
        });
      });

      //Product
       $(".coupon-except-product-list").dataTable({
          processing: true,
          serverSide: true,
          pageLength: 25,
          searching: false,
          ajax:{
            url: "<?php echo base_url("administrator_area/coupon/datatables/index_except_product")?>",
            type: "POST",
            data:{
              coupon_id: $(".coupon-except-product-list").data("coupon_id")
            }
          },
          columns: [
            {
              data: 'code'
            },
            {
              data: 'product_name'
            },
            {
              data: 'model_name'
            },
            {
              render: function(data, type, row, meta){
                var action_header = '<button type="button" class="btn red-thunderbird open-remove-product" data-id="'+row["id"]+'"> Delete <i class="fa fa-trash"></i></button>';
                return action_header;
              },
              searchable: false,
              orderable: false
            }
          ]
      });

      $(document).on("click", ".create-product-button",function(){
        var coupon_id = $(this).data("coupon_id");
        var product_id = $("#product-form select[name='product_id']").val();
        if ((product_id == null )) {
          $("#system-return-error .message").html("Invalid Data");
          $("#system-return-error").modal("toggle");
        }
        else{
          var formData = new FormData();
          formData.append('coupon_id', coupon_id);
          formData.append('product_id', product_id);

          $.ajax({
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            url: "<?php echo base_url("administrator_area/coupon/edit_expept_product")?>",
            success: function (response) {
              try {
                if (response.code == "0x0000-00000") {
                  var table = $(".coupon-except-product-list").DataTable();
                  table.columns().search("").draw();

                  toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                  toastr.success('Update Completed', 'Success.');
                  $("#product-form select[name='product_id']").select2("val", "");
                } else {
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch (e) {
                $("#system-return-failed").modal("toggle");
              }
            },
            error: function () {
              $("#system-disconnected").modal("toggle");
            }
          });
        }
      });

      $(document).on("click", ".open-remove-product", function(){
        $("#remove-product .product-id").val('');
        
        $("#remove-product .product-id").val($(this).data('id'));
        $('#remove-product').modal('show');
      });

      $(document).on("click", ".product-remove-button", function(){
        var id = $("#remove-product .product-id").val();
        $("#remove-product").modal('hide');

        var formData = new FormData();
        formData.append('id', id);

        $.ajax({
          data: formData,
          type: "POST",
          contentType: false,
          processData: false,
          url: "<?php echo base_url("administrator_area/coupon/delete_expept_product")?>",
          success: function (response) {
            try {
              if (response.code == "0x0000-00000") {
                var table = $(".coupon-except-product-list").DataTable();
                table.columns().search("").draw();

                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "positionClass": "toast-bottom-right",
                  "onclick": null,
                  "showDuration": "1000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                toastr.success('Update Completed', 'Success');
              } else {
                $("#system-return-error .message").html(response.message);
                $("#system-return-error").modal("toggle");
              }
            } catch (e) {
              $("#system-return-failed").modal("toggle");
            }
          },
          error: function () {
            $("#system-disconnected").modal("toggle");
          }
        });
      });

    });
  </script>

  <!-- INCLUDE RAW SCRIPT AREA -->
  <?php $this->load->view("administrator_area/__scripts/Javascript_Modal",array());?>
  <?php $this->load->view("administrator_area/__scripts/Javascript_Select2",array());?>
</html>

<!-- ------------------------------------------------------------------ -->
<!-- MODAL IS BEGIN                                                     -->
<!-- ------------------------------------------------------------------ -->
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationCancel");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationDelete");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationRefresh");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationSave");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationSaveMultiform");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_PermissionDenied");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemDisconnected");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnError");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnFailed");?>


<div class="modal fade" id="remove-category" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-red-thunderbird">
            <i class="fa fa-close font-red-thunderbird" data-dismiss="modal"></i>
            <span class="caption-category bold uppercase"> Delete Product Category</span>
          </div>
        </div>
        <div class="portlet-body form">
          <div class="form-horizontal" role="form">
            <input type="hidden" class="category-id">
            <div class="form-group">
              <div class="col-md-12">Are you sure to delete?
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3 control-label"></div>
              <div class="col-md-9">
                  <button class="btn green-jungle category-remove-button">Save</button>
                  <button class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="remove-product" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-red-thunderbird">
            <i class="fa fa-close font-red-thunderbird" data-dismiss="modal"></i>
            <span class="caption-product bold uppercase"> Delete Product</span>
          </div>
        </div>
        <div class="portlet-body form">
          <div class="form-horizontal" role="form">
            <input type="hidden" class="product-id">
            <div class="form-group">
              <div class="col-md-12">Are you sure to delete?
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3 control-label"></div>
              <div class="col-md-9">
                  <button class="btn green-jungle product-remove-button">Save</button>
                  <button class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>