<?php
$codeigniter_instance =& get_instance();

$html = $_POST["htmlExcel"];

$date = (date("Ymd_His"));
// @$act=$_GET['act'];
// if($act=='excel'){
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=TechnicalSpecForm.xls");
header("Pragma: no-cache");
header("Cache-Control: max-age=0")
// header("Expires: 0");
// }
?>

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
  <div class="container">
    <div class="row">
      <table x:str border=1 cellpadding=0 cellspacing=1 style="border-collapse:collapse">
        <tr>
          <th></th>
          <th>Product Id</th>
          <?php if($products != null && count($products)> 0){
            foreach ($products as $item) { 
            ?>
              <th><?php echo $item->id;?></th>
          <?php 
            }
          }
          ?>
        </tr>
        <tr>
          <th>Technical spec Id</th>
          <th></th>
          <?php if($products != null && count($products)> 0){
            foreach ($products as $item) { 
            ?>
              <th><?php echo $item->name_en.(empty($item->model_name_en)? '': '/'.$item->model_name_en);?></th>
          <?php 
            }
          }
          ?>
        </tr>
        <?php if($technical_specs != null && count($technical_specs) > 0) {
          foreach ($technical_specs as $tech) {
        ?>
            <tr>
              <th><?php echo $tech->id;?></th>
              <th><?php echo $tech->name_en;?></th>
              <?php for ($i=0; $i < count($products); $i++) { 
                echo "<td></td>";
              } ?>
            </tr>
        <?php 
          }
        } ?>
        <tr>
          
        </tr>
      </table>
    </div>
  </div>
</body>
</html>