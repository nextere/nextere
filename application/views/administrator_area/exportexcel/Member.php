<?php
$codeigniter_instance =& get_instance();

$html = $_POST["htmlExcel"];

$date = (date("Ymd_His"));
// @$act=$_GET['act'];
// if($act=='excel'){
header("Content-Type: application/x-msexcel;");
header("Content-Disposition: attachment; filename=Member-".date("Ymd").".xls");
header("Pragma: no-cache");
// header("Expires: 0");
// }
?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http://www.w3.org/TR/REC-html40">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
  <div class="container">
    <div class="row">
      <table x:str border=1 cellpadding=0 cellspacing=1 style="border-collapse:collapse">
        <tr>
          <th>Email</th>
          <th>Full name</th>
          <th>Gender</th>
          <th>Birth Date</th>
          <th>Tax ID</th>
          <th>Telephone</th>
        </tr>
        <?php if($members != null && count($members) > 0) {
          foreach ($members as $item) {
        ?>
            <tr>
              <td><?php echo $item->email;?></td>
              <td><?php echo $item->fullname;?></td>
              <td><?php echo $item->gender;?></td>
              <td><?php if(isset($item->birth_date) && $item->birth_date != null) { echo date("d/M/Y", strtotime($item->birth_date)); } ?></td>
              <td><?php echo $item->tax_id;?></td>
              <td><?php echo $item->telephone;?></td>
            </tr>
        <?php 
          }
        } ?>
      </table>
    </div>
  </div>
</body>
</html>