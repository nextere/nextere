
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
  <head>
    <title>AME-SM API</title>
    <link href="<?php echo faviconDirectory($this->config->item('WEBSITE_FAVICON'));?>" rel="shortcut icon" />

    <!-- META TAG AREA -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- MANDATORY STYLE AREA -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/simple-line-icons/simple-line-icons.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-switch/css/bootstrap-switch.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- PLUGINS AREA -->
    

    <!-- THEME STYLE AREA -->
    <link href="<?php echo assetsDirectory("global/css/components-md.min.css");?>" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo assetsDirectory("global/css/plugins-md.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- LAYOUT AREA -->
    <link href="<?php echo assetsDirectory("layouts/layout/css/layout.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("layouts/layout/css/custom.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- CUSTOM AREA -->
    <style>
           
    </style>
  </head>

  <!-- ------------------------------------------------------------------ -->
  <!-- BODY IS BEGIN                                                      -->
  <!-- ------------------------------------------------------------------ -->
  <body class="">
    
    <div class="page-container">
      <div class="page-content-wrapper">
        <div class="row">
          <div class="col-md-12" style="text-align: center;margin-top: 20px;">
            <h3 class="page-title">
              AME-SM Api
            </h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="col-md-12" style="margin-top: 20px;margin-bottom: 20px;">
              <a href="<?php echo base_url("assets/uploads/manualAPI.pdf");?>" target="_blank">
              <i class="fa fa-file-pdf-o"></i> PDF
            </a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="col-md-12"><h4> Car Models</h4></div>
            <div class="row">
              <div class="col-md-12">
                <ul class="nav">
                  <li>
                    <a href="<?php echo base_url("api/carmodels/m_brand")?>">- Get car brand</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url("api/carmodels/m_models")?>">- Get all car models</a>
                  </li>
                </ul>
                
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="col-md-12"><h4> Config Address</h4></div>
            <div class="row">
              <div class="col-md-12">
                <ul class="nav">
                  <li>
                    <a href="<?php echo base_url("api/config_address/geo")?>">- Get all Geography</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url("api/config_address/province")?>">- Get all province</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url("api/config_address/district")?>">- Get all district</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url("api/config_address/subdistrict")?>">- Get all sub district</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url("api/config_address/zip")?>">- Get all zipcode</a>
                  </li>
                </ul>
                
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="col-md-12"><h4> Insurance</h4></div>
            <div class="row">
              <div class="col-md-12">
                <ul class="nav">
                  <li>
                    <a href="<?php echo base_url("api/insurance/company")?>">- Get all insurance company</a>
                  </li>
                 <!--  <li>
                    <a href="<?php echo base_url("api/insurance/car")?>">- Get all insurance car</a>
                  </li> -->
                  <li>
                    <a href="<?php echo base_url("api/insurance/cartype")?>">- Get all insurance car type</a>
                  </li>
                </ul>
                
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="col-md-12"><h4> User</h4></div>
            <div class="row">
              <div class="col-md-12">
                <ul class="nav">
                  <!-- <li>
                    <a href="<?php echo base_url("api/user/reg")?>">- Register user</a>
                  </li> -->
                  <!-- <li>
                    <a href="<?php echo base_url("api/user/li")?>">- Login</a>
                  </li>-->
                  <li>
                    <a href="<?php echo base_url("api/user/m_brokerlogin")?>">- Broker Login</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url("api/user/m_logout")?>">- Logout</a>
                  </li> 
                </ul>
                
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="col-md-12"><h4> Customer</h4></div>
            <div class="row">
              <div class="col-md-12">
                <ul class="nav">
                  <li>
                    <a href="<?php echo base_url("api/customer/m_search")?>">- Search</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url("api/customer/m_detail")?>">- Detail</a>
                  </li>
                 <!--  <li>
                    <a href="<?php echo base_url("api/customer/m_insert")?>">- Insert</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url("api/customer/m_update")?>">- Update</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url("api/customer/m_remove")?>">- Remove</a>
                  </li> -->
                </ul>
                
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="col-md-12"><h4> Customer car insurance</h4></div>
            <div class="row">
              <div class="col-md-12">
                <ul class="nav">
                  <li>
                    <a href="<?php echo base_url("api/customercar_insurance/m_insert_broker")?>">- Insert form of Broker</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url("api/customercar_insurance/m_search")?>">- Search</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url("api/customercar_insurance/m_updatinsurancenumber")?>">- Update Insurance number</a>
                  </li>
                  <!-- <li>
                  <li>
                    <a href="<?php echo base_url("api/customercar_insurance/m_update_broker")?>">- Update form (for Broker กรณีที่มีการอนุมัติข้อมูลแล้ว)</a>
                  </li>
                    <a href="<?php echo base_url("api/customercar_insurance/m_insert")?>">- Insert form</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url("api/customercar_insurance/m_update")?>">- Update form</a>
                  </li> -->
                </ul>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>

  <!-- ------------------------------------------------------------------ -->
  <!-- JAVASCRIPT IS BEGIN                                                -->
  <!-- ------------------------------------------------------------------ -->
  <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/respond.min.js');?>"></script> <![endif]-->
  <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/excanvas.min.js');?>"></script> <![endif]-->
    
  <!-- MANDATORY SCRIPT AREA -->
  <script src="<?php echo assetsDirectory("global/plugins/jquery.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap/js/bootstrap.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/js.cookie.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-slimscroll/jquery.slimscroll.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery.blockui.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-switch/js/bootstrap-switch.min.js");?>" type="text/javascript"></script>
    
  <!-- PLUGINS AREA -->
  
    
  <!-- THEME SCRIPT AREA -->
  <script src="<?php echo assetsDirectory("global/scripts/app.min.js");?>" type="text/javascript"></script>

  <!-- LAYOUT AREA -->
  <script src="<?php echo assetsDirectory("layouts/layout/scripts/layout.min.js");?>" type="text/javascript"></script>
  <!-- CUSTOM AREA -->
  

  <!-- INCLUDE RAW SCRIPT AREA -->
</html>

<!-- ------------------------------------------------------------------ -->
<!-- MODAL IS BEGIN                                                     -->
<!-- ------------------------------------------------------------------ -->

