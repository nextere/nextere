<!-- ------------------------------------------------------------------ -->
<!-- PERMISSION CHECK IS BEGIN                                          -->
<!-- ------------------------------------------------------------------ -->
<?php
$codeigniter_instance =& get_instance();

$configurations = array(
  "PAGE_TITLE" => "Administrator System",
  "PAGE_HEADER" => array(
   "MAIN_TITLE" => "Quotation",
   "SUB_TITLE" => ""
  ),
  "PORTLET_HEADER" => array(
    "ICON" => "fa fa-bars",
    "TITLE" => "Quotation Detail"
  )
);
?>
<!-- ------------------------------------------------------------------ -->
<!-- HEADER IS BEGIN                                                    -->
<!-- ------------------------------------------------------------------ -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
  <head>
    <title><?php echo $this->config->item('WEBSITE_NAME');?> | Login System</title>
    <link href="<?php echo faviconDirectory($this->config->item('WEBSITE_FAVICON'));?>" rel="shortcut icon" />

    <!-- META TAG AREA -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- MANDATORY STYLE AREA -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/simple-line-icons/simple-line-icons.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-switch/css/bootstrap-switch.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- PLUGINS AREA -->
    <link href="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/clockface/css/clockface.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2-bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/jstree/dist/themes/default/style.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/dropzone/basic.min.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- THEME STYLE AREA -->
    <link href="<?php echo assetsDirectory("global/css/components-md.min.css");?>" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo assetsDirectory("global/css/plugins-md.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- LAYOUT AREA -->
    <link href="<?php echo assetsDirectory("layouts/layout/css/layout.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("layouts/layout/css/themes/light.min.css");?>" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?php echo assetsDirectory("layouts/layout/css/custom.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- CUSTOM AREA -->
    <style>
      .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice {
        color: #fff;
        background: #53c6d2;
        border: 1px solid #53c6d2;
        margin: 5px 0 0 6px;
        padding: 0 35px 0 6px;
      }
      .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice__remove {
        margin-right: 30px;
        color: #e7505a;
      }
      .product-image {
        height: 150px;
        margin-top: 10px;
      }
    </style>
  </head>

  <!-- ------------------------------------------------------------------ -->
  <!-- BODY IS BEGIN                                                      -->
  <!-- ------------------------------------------------------------------ -->
  <body class="page-header-fixed page-sidebar-closed-hide-logo page-md">
    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueHeader",array());?>
    <div class="page-container">
      <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueSideBar",array());?>
      <div class="page-content-wrapper">
        <div class="page-content">
          <h3 class="page-title">
              <?php echo $configurations["PAGE_HEADER"]["MAIN_TITLE"]?>
              <small>
                  <?php echo $configurations["PAGE_HEADER"]["SUB_TITLE"]?>
              </small>
          </h3>
          <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueBreadCrumb",array());?>

          <!-- ------------------------------------------------------------------ -->
          <!-- CONTENT IS BEGIN                                                   -->
          <!-- ------------------------------------------------------------------ -->
          
          <div class="row">
            <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light bordered">
                <div class="portlet-title">
                  <div class="caption font-green">
                    <i class="<?php echo $configurations["PORTLET_HEADER"]["ICON"]?> font-green"></i>
                    <span class="caption-subject bold uppercase">
                      <?php echo $configurations["PORTLET_HEADER"]["TITLE"]?>
                    </span>
                  </div>
                  <?php if(isset($quotation->{"quotation::id"})){ ?>
                    <div class="actions">
                      <!-- <a href="< ?php echo base_url("administrator_area/quotation/edit_techical/".$product->{"quotation::id"})?>" class="btn btn-sm btn-warning">Technical Spec</a> -->
                    </div>
                  <?php } ?>

                </div>
                <div class="portlet-body">
                  <form class="form-horizontal" role="form" method="post" id="main-form" enctype="multipart/form-data"> 
                    <input type="hidden" name="session_time" value="<?php echo date("YmdHis")?>">
                    <div class="form-group">
                      <label class="col-md-2 control-label">Quotation</label>
                      <div class="col-md-3">
                        <p class="form-control-static">
                          <?php if(isset($quotation->{"quotation::quotation_no"}))  echo $quotation->{"quotation::quotation_no"} ?>
                        </p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Member</label>
                      <div class="col-md-3">
                        <select class="form-control select2" name="member_id">
                          <?php
                          if(isset($quotation->{"member::id"})){
                          ?>
                            <option value="<?php echo $quotation->{"member::id"}?>" selected><?php echo $quotation->{"member::fullname"}?></option>
                          <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Type<span class="required"> *</span></label>
                      <div class="col-md-9">
                        <div class="mt-radio-inline">
                          <label class="mt-radio">
                            <input type="radio" name="type" value="1" <?php if(isset($quotation->{"quotation::type"}) && $quotation->{"quotation::type"} == 1) echo "checked"?>> ประเภทบุคคลธรรมดา
                            <span></span>
                          </label>
                          <label class="mt-radio">
                            <input type="radio" name="type" value="2" <?php if(isset($quotation->{"quotation::type"}) && $quotation->{"quotation::type"} == 2) echo "checked"?>> ประเภทนิติบุคคล
                            <span></span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">First Name<span class="required"> *</span></label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" name="first_name" autocomplete="off" placeholder="First Name" value="<?php if(isset($quotation->{"quotation::first_name"}))  echo $quotation->{"quotation::first_name"}  ?>">
                      </div>
                      <label class="col-md-3 control-label">Last Name<span class="required"> *</span></label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" name="last_name" autocomplete="off" placeholder="Last Name" value="<?php if(isset($quotation->{"quotation::last_name"}))  echo $quotation->{"quotation::last_name"}  ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Telephone<span class="required"> *</span></label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" name="telephone" autocomplete="off" placeholder="Telephone" value="<?php if(isset($quotation->{"quotation::telephone"}))  echo $quotation->{"quotation::telephone"}  ?>">
                      </div>
                      <label class="col-md-3 control-label">Email<span class="required"> *</span></label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" name="email" autocomplete="off" placeholder="Email" value="<?php if(isset($quotation->{"quotation::email"}))  echo $quotation->{"quotation::email"}  ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Company Name</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" name="company_name" autocomplete="off" placeholder="Company Name" value="<?php if(isset($quotation->{"quotation::company_name"}))  echo $quotation->{"quotation::company_name"}  ?>">
                      </div>
                      <label class="col-md-3 control-label">Business Type</label>
                      <div class="col-md-3">
                        <select class="form-control select2" name="business_type_id">
                          <?php
                          if(isset($quotation->{"business_type::id"})){
                          ?>
                            <option value="<?php echo $quotation->{"business_type::id"}?>" selected><?php echo $quotation->{"business_type::name_en"}?></option>
                          <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                     <div class="form-group">
                      <label class="col-md-2 control-label">Tax ID</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" name="tax_id" autocomplete="off" placeholder="Tax ID" value="<?php if(isset($quotation->{"quotation::tax_id"}))  echo $quotation->{"quotation::tax_id"}  ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Message</label>
                      <div class="col-md-9">
                        <textarea class="form-control" rows="3"  name="message"><?php if(isset($quotation->{"quotation::message"})) echo $quotation->{"quotation::message"}  ?></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Status<span class="required"> *</span></label>
                      <div class="col-md-3">
                        <div class="mt-radio-inline">
                          <label class="mt-radio">
                            <input type="radio" name="status" value="ACTIVATE" <?php if(isset($quotation->{"quotation::status"}) && $quotation->{"quotation::status"} == "ACTIVATE")echo "checked"?>> Activate
                            <span></span>
                          </label>
                          <label class="mt-radio">
                            <input type="radio" name="status" value="SUSPEND" <?php if(!isset($quotation->{"quotation::status"}) || $quotation->{"quotation::status"} != "ACTIVATE")echo "checked"?>> Suspend
                            <span></span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                          <button type="button" class="btn green-jungle" data-toggle="modal" data-target="#confirmation-save" data-backdrop="static" data-keyboard="false">Save Changes</button>
                          <button type="button" class="btn red btn-outline" data-toggle="modal" data-target="#confirmation-cancel" data-backdrop="static" data-keyboard="false">Cancel</button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
            </div>
          </div>
          <?php if(isset($quotation->{"quotation::id"})) {?>
            <div class="row">
              <div class="col-md-12">
                <div class="portlet light bordered" >
                  <div class="portlet-title">
                    <div class="caption font-green" >
                      <span class="caption-subject bold uppercase">
                        Product Detail
                      </span>
                    </div>
                  </div>
                  <div class="portlet-body">
                    <form class="form-horizontal" role="form" method="post" id="quotation-detail-form" enctype="multipart/form-data">
                      <input type="hidden" name="session_time" value="<?php echo date("YmdHis")?>">
                      <div class="form-group">
                        <label class="col-md-2 control-label">Product<span class="required"> *</span></label>
                        <div class="col-md-6">
                          <select class="form-control select2" name="product_id">
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Price/Qty<span class="required"> *</span></label>
                        <div class="col-md-2">
                          <input type="text" class="form-control pos-decimal" name="price" autocomplete="off" value="">
                        </div>
                        <label class="col-md-2 control-label">Qty<span class="required"> *</span></label>
                        <div class="col-md-2">
                          <input type="text" class="form-control number" name="quantity" autocomplete="off" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-3">
                          <button type="button" class="btn green-jungle quotation-detail-button" data-quotation_id="<?php echo $quotation->{"quotation::id"}?>">Add</button>
                        </div>
                      </div>
                    </form>
                    <hr>
                    <table class="table table-striped table-bordered table-hover order-column quotation-detail-list" data-quotation_id="<?php echo $quotation->{"quotation::id"}?>">
                      <thead>
                        <tr>
                          <th style="width:40%;"> Product</th>
                          <th style="width:15%;"> Real Price </th>
                          <th style="width:10%;"> Quantity </th>
                          <th style="width:15%;"> Price/Qty </th>
                          <th style="width:20%;"> Action </th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>

    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueFooter",array());?>
  </body>

  <!-- ------------------------------------------------------------------ -->
  <!-- JAVASCRIPT IS BEGIN                                                -->
  <!-- ------------------------------------------------------------------ -->
  <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/respond.min.js');?>"></script> <![endif]-->
  <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/excanvas.min.js');?>"></script> <![endif]-->

  <!-- MANDATORY SCRIPT AREA -->
  <script src="<?php echo assetsDirectory("global/plugins/jquery.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap/js/bootstrap.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/js.cookie.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-slimscroll/jquery.slimscroll.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery.blockui.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-switch/js/bootstrap-switch.min.js");?>" type="text/javascript"></script>

  <!-- PLUGINS AREA -->
  <script src="<?php echo assetsDirectory("global/plugins/jquery-ui/jquery-ui.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/moment.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/clockface/js/clockface.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/select2/js/select2.full.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/jquery.validate.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/additional-methods.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.js");?>"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote-cleaner.js");?>"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jstree/dist/jstree.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/scripts/datatable.js");?>" type="text/javascript"></script>

  <script src="<?php echo assetsDirectory("global/plugins/backstretch/jquery.backstretch.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.js");?>" type="text/javascript"></script>
    

  <!-- THEME SCRIPT AREA -->
  <script src="<?php echo assetsDirectory("global/scripts/app.min.js");?>" type="text/javascript"></script>

  <!-- LAYOUT AREA -->
  <script src="<?php echo assetsDirectory("layouts/layout/scripts/layout.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("layouts/layout/scripts/demo.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("layouts/global/scripts/quick-sidebar.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery.numeric.js");?>" type="text/javascript"></script>

  <!-- CUSTOM AREA -->
  <script>
    jQuery(document).ready(function() {
      $('.number').numeric({ decimal: false, negative: false });
      $('.pos-decimal').numeric({ negative: false });
      /** =================================================================== **/
      /** SELECT 2                                                            **/
      /** =================================================================== **/
      $("#main-form select[name='member_id']").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/quotation/select2/edit_member")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.fullname };
              })
            };
          }
        }
      });

      $("#main-form select[name='business_type_id']").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/quotation/select2/edit_business_type")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_en };
              })
            };
          }
        }
      });

      $("select[name='product_id']").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/product/select2/edit_product")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_en+'('+obj.code+')' };
              })
            };
          }
        }
      });

      /** =================================================================== **/
      /** FORM VALIDATE                                                       **/
      /** =================================================================== **/

      $("#main-form").validate({
        errorElement: 'span', errorClass: 'help-block', focusInvalid: false,
        rules: {
          type: {required: true},
          first_name: {required: true},
          last_name: {required:true},
          telephone: {required:true},
          email: {required:true},
          status: {required: true}
        },
        highlight: function (element) { $(element).closest('.form-group').addClass('has-error'); },
        success: function (label) { label.closest('.form-group').removeClass('has-error'); label.remove(); },
        invalidHandler: function(form, validator) { $('.confirmation-save').modal('toggle'); },
        errorPlacement: function (error, element) { error.appendTo(element.closest('div')); },
        submitHandler: function() {
          App.blockUI({ target: 'body', animate: true });
          $.ajax({
            type: "POST",
            <?php
            $post_url = "";
            if(isset($quotation->{"quotation::id"})){
              $post_url = base_url("administrator_area/quotation/edit/".$quotation->{"quotation::id"});
            }else{
              $post_url = base_url("administrator_area/quotation/edit");
            }
            ?>
            url: "<?php echo $post_url?>",
            data: new FormData($('#main-form')[0]),
            cache: false,
            contentType: false,
            processData: false,
            success: function(response){
              try {
                if(response.code == "0x0000-00000") {
                  toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "positionClass": "toast-bottom-right",
                  "onclick": null,
                  "showDuration": "1000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                toastr.success('Update Completed', 'Success.');

                window.location.assign("<?php echo base_url("administrator_area/quotation/edit")?>/"+response.data);
                }else{
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch(e) {
                $("#system-return-failed").modal("toggle");
              }
              App.unblockUI('body');
            },
            error: function(){
              $("#system-disconnected").modal("toggle");
              App.unblockUI('body');
            }
          });
        }
      });

      //Price by Quantity
      $(".quotation-detail-list").dataTable({
          processing: true,
          serverSide: true,
          pageLength: 25,
          searching: false,
          ajax:{
            url: "<?php echo base_url("administrator_area/quotation/datatables/index_quotation_detail")?>",
            type: "POST",
            data:{
              quotation_id: $(".quotation-detail-list").data("quotation_id")
            }
          },
          columns: [
            {
              render: function(data, type, row, meta){
                return row["product_name_en"]+' ('+row["product_code"]+")";
              },
              data: 'product_name_en'
            },
            {
              data: 'real_price'
            },
            {
              data: 'quantity'
            },
            {
              data: 'price_per_unit'
            },
            {
              render: function(data, type, row, meta){
                var action_header = '<button type="button" class="btn yellow-crusta open-update-quotation-detail-modal" ' +
                  'data-id="'+row["id"]+'" ' +
                  'data-product_id="'+row["product_id"]+'" ' +
                  'data-quotation_id="'+row["quotation_id"]+'" ' +
                  'data-product_name_en="'+row["product_name_en"]+'" ' +
                  'data-product_code="'+row["product_code"]+'" ' +
                  'data-quantity="'+row["quantity"]+'" ' +
                  'data-price_per_unit="'+row["price_per_unit"]+'" ' +
                  '> Edit</button>' +
                '<button type="button" class="btn red-thunderbird open-remove-quotation-detail" data-id="'+row["id"]+'"> Delete</button>';
                return action_header;
              },
              searchable: false,
              orderable: false
            }
          ]
      });

      $(document).on("click", ".quotation-detail-button",function(){
        var quotation_id = $(this).data("quotation_id");
        var product_id = $("#quotation-detail-form select[name='product_id']").val();
        var price = $("#quotation-detail-form input[name='price']").val();
        var quantity = $("#quotation-detail-form input[name='quantity']").val();

        if (product_id==null || price == '' || quantity == '') {
          $("#system-return-error .message").html("กรุณาระบุข้อมูลให้ครบ");
          $("#system-return-error").modal("toggle");
        }
        else{
          var formData = new FormData();
          formData.append('quotation_id', quotation_id);
          formData.append('product_id', product_id);
          formData.append('quantity', quantity);
          formData.append('price', price);

          $.ajax({
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            url: "<?php echo base_url("administrator_area/quotation/edit_quotation_detail")?>",
            success: function (response) {
              try {
                if (response.code == "0x0000-00000") {
                  var table = $(".quotation-detail-list").DataTable();
                  table.columns().search("").draw();

                  toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                  toastr.success('Update Completed', 'Success.');
                  $("#quotation-detail-form select[name='product_id']").html('').trigger('change');
                  $("#quotation-detail-form input[name='quantity']").val("");
                  $("#quotation-detail-form input[name='price']").val("");
                } else {
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch (e) {
                $("#system-return-failed").modal("toggle");
              }
            },
            error: function () {
              $("#system-disconnected").modal("toggle");
            }
          });
        }
      });

      $(document).on("click", ".open-update-quotation-detail-modal", function(){
        $("#update-quotation-detail .quotation-detail-id").val('');
        $("#update-quotation-detail .quotation-detail-product-id").val('');
        $("#update-quotation-detail .quotation-detail-quotation-id").val('');
        $("#update-quotation-detail .quotation-detail-product-name-en").val('');
        $("#update-quotation-detail .quotation-detail-product-code").val('');
        $("#update-quotation-detail .quotation-detail-quantity").val('');
        $("#update-quotation-detail .quotation-detail-price").val('');
        
        $("#update-quotation-detail .quotation-detail-id").val($(this).data('id'));
        $("#update-quotation-detail .quotation-detail-product-id").val($(this).data('product_id'));
        $("#update-quotation-detail .quotation-detail-quotation-id").val($(this).data('quotation_id'));
        $("#update-quotation-detail .quotation-detail-product-name-en").val($(this).data('product_name_en'));
        $("#update-quotation-detail .quotation-detail-product-code").val($(this).data('product_code'));
        $("#update-quotation-detail .quotation-detail-quantity").val($(this).data('quantity'));
        $("#update-quotation-detail .quotation-detail-price").val($(this).data('price_per_unit'));
        $('#update-quotation-detail').modal('show');
      });

      $(document).on("click", ".update-quotation-button", function(){
        var id = $("#update-quotation-detail .quotation-detail-id").val();
        var product_id = $("#update-quotation-detail .quotation-detail-product-id").val();
        var quotation_id = $("#update-quotation-detail .quotation-detail-quotation-id").val();
        var quantity = $("#update-quotation-detail .quotation-detail-quantity").val();
        var price = $("#update-quotation-detail .quotation-detail-price").val();
        $("#update-quotation-detail").modal('hide');

        
        if (product_id==null || price == '' || quantity == '') {
          $("#system-return-error .message").html("กรุณาระบุข้อมูลให้ครบ");
          $("#system-return-error").modal("toggle");
        }
        else{
          var formData = new FormData();
          formData.append('id', id);
          formData.append('quotation_id', quotation_id);
          formData.append('product_id', product_id);
          formData.append('quantity', quantity);
          formData.append('price', price);

          $.ajax({
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            url: "<?php echo base_url("administrator_area/quotation/edit_quotation_detail")?>",
            success: function (response) {
              try {
                if (response.code == "0x0000-00000") {
                  var table = $(".quotation-detail-list").DataTable();
                  table.columns().search("").draw();

                  toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                  toastr.success('Update Completed', 'Success.');
                  $("#update-quotation-detail .quotation-detail-id").val('');
                  $("#update-quotation-detail .quotation-detail-product-id").val('');
                  $("#update-quotation-detail .quotation-detail-quotation-id").val('');
                  $("#update-quotation-detail .quotation-detail-product-name-en").val('');
                  $("#update-quotation-detail .quotation-detail-product-code").val('');
                  $("#update-quotation-detail .quotation-detail-quantity").val('');
                  $("#update-quotation-detail .quotation-detail-price").val('');
                } else {
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch (e) {
                $("#system-return-failed").modal("toggle");
              }
            },
            error: function () {
              $("#system-disconnected").modal("toggle");
            }
          });
        }
      });

      $(document).on("click", ".open-remove-quotation-detail", function(){
        $("#remove-quotation-detail .quotation-detail-id").val('');
        
        $("#remove-quotation-detail .quotation-detail-id").val($(this).data('id'));
        $('#remove-quotation-detail').modal('show');
      });

      $(document).on("click", ".quotation-detail-remove-button", function(){
        var id = $("#remove-quotation-detail .quotation-detail-id").val();
        $("#remove-quotation-detail").modal('hide');

        var formData = new FormData();
        formData.append('id', id);

        $.ajax({
          data: formData,
          type: "POST",
          contentType: false,
          processData: false,
          url: "<?php echo base_url("administrator_area/quotation/delete_quotation_detail")?>",
          success: function (response) {
            try {
              if (response.code == "0x0000-00000") {
                var table = $(".quotation-detail-list").DataTable();
                table.columns().search("").draw();

                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "positionClass": "toast-bottom-right",
                  "onclick": null,
                  "showDuration": "1000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                toastr.success('Update Completed', 'Success');
              } else {
                $("#system-return-error .message").html(response.message);
                $("#system-return-error").modal("toggle");
              }
            } catch (e) {
              $("#system-return-failed").modal("toggle");
            }
          },
          error: function () {
            $("#system-disconnected").modal("toggle");
          }
        });
      });

    });
  </script>

  <!-- INCLUDE RAW SCRIPT AREA -->
  <?php $this->load->view("administrator_area/__scripts/Javascript_Modal",array());?>
  <?php $this->load->view("administrator_area/__scripts/Javascript_Select2",array());?>
</html>

<!-- ------------------------------------------------------------------ -->
<!-- MODAL IS BEGIN                                                     -->
<!-- ------------------------------------------------------------------ -->
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationCancel");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationDelete");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationRefresh");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationSave");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationSaveMultiform");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_PermissionDenied");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemDisconnected");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnError");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnFailed");?>

<div class="modal fade" id="update-quotation-detail" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-yellow-crusta">
            <i class="fa fa-pencil font-yellow-crusta" data-dismiss="modal"></i>
            <span class="caption-subject bold uppercase"> Edit Detail</span>
          </div>
        </div>
        <div class="portlet-body form">
          <div class="form-horizontal" role="form">
            <input type="hidden" class="quotation-detail-id">
            <input type="hidden" class="quotation-detail-product-id">
            <input type="hidden" class="quotation-detail-quotation-id">
            <div class="form-group">
              <div class="col-md-3 control-label">Product</div>
              <div class="col-md-8">
                <input type="text" class="form-control quotation-detail-product-name-en" readonly>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3 control-label">Code</div>
              <div class="col-md-8">
                <input type="text" class="form-control quotation-detail-product-code" readonly>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3 control-label">Quantity<span class="required"> *</span></div>
              <div class="col-md-5">
                <input type="text" class="form-control number quotation-detail-quantity">
              </div>
            </div>
            <div class="form-group">
                <div class="col-md-3 control-label">Price<span class="required"> *</span></div>
                <div class="col-md-5">
                    <input type="text" class="form-control pos-decimal quotation-detail-price">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3 control-label"></div>
                <div class="col-md-5">
                    <button class="btn green-jungle update-quotation-button">Save</button>
                    <button class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="remove-quotation-detail" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-red-thunderbird">
            <i class="fa fa-close font-red-thunderbird" data-dismiss="modal"></i>
            <span class="caption-quotation-detail bold uppercase"> Delete Quotation Detail</span>
          </div>
        </div>
        <div class="portlet-body form">
          <div class="form-horizontal" role="form">
            <input type="hidden" class="quotation-detail-id">
            <div class="form-group">
              <div class="col-md-12">Are you sure to delete?
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3 control-label"></div>
              <div class="col-md-9">
                  <button class="btn green-jungle quotation-detail-remove-button">Save</button>
                  <button class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

