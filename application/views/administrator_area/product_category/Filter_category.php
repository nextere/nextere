<!-- ------------------------------------------------------------------ -->
<!-- PERMISSION CHECK IS BEGIN                                          -->
<!-- ------------------------------------------------------------------ -->
<?php
$codeigniter_instance =& get_instance();

$configurations = array(
	"PAGE_TITLE" => "Administrator System",
	"PAGE_HEADER" => array(
		"MAIN_TITLE" => "Product Category",
		"SUB_TITLE" => "Filter"
	),
	"PORTLET_HEADER" => array(
		"ICON" => "fa fa-bars",
		"TITLE" => "Filter List"
	)
);
?>
<!-- ------------------------------------------------------------------ -->
<!-- HEADER IS BEGIN                                                    -->
<!-- ------------------------------------------------------------------ -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
	<head>
		<title><?php echo $this->config->item('WEBSITE_NAME');?> | Login System</title>
		<link href="<?php echo faviconDirectory($this->config->item('WEBSITE_FAVICON'));?>" rel="shortcut icon" />

		<!-- META TAG AREA -->
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="" name="description" />
		<meta content="" name="author" />

		<!-- MANDATORY STYLE AREA -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
		<link href="<?php echo assetsDirectory("global/plugins/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assetsDirectory("global/plugins/simple-line-icons/simple-line-icons.min.css");?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assetsDirectory("global/plugins/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assetsDirectory("global/plugins/bootstrap-switch/css/bootstrap-switch.min.css");?>" rel="stylesheet" type="text/css" />

		<!-- PLUGINS AREA -->
		<link href="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.css");?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css");?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.css");?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css");?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css");?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assetsDirectory("global/plugins/clockface/css/clockface.css");?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assetsDirectory("global/plugins/select2/css/select2.min.css");?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assetsDirectory("global/plugins/select2/css/select2-bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.css");?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.css");?>" rel="stylesheet">
		<link href="<?php echo assetsDirectory("global/plugins/jstree/dist/themes/default/style.css");?>" rel="stylesheet">
		<link href="<?php echo assetsDirectory("global/plugins/dropzone/basic.min.css");?>" rel="stylesheet">
		<link href="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.css");?>" rel="stylesheet">
		<link href="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.css");?>" rel="stylesheet" type="text/css" />
		<!-- THEME STYLE AREA -->
		<link href="<?php echo assetsDirectory("global/css/components-md.min.css");?>" rel="stylesheet" id="style_components" type="text/css" />
		<link href="<?php echo assetsDirectory("global/css/plugins-md.min.css");?>" rel="stylesheet" type="text/css" />

		<!-- LAYOUT AREA -->
		<link href="<?php echo assetsDirectory("layouts/layout/css/layout.min.css");?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assetsDirectory("layouts/layout/css/themes/light.min.css");?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assetsDirectory("layouts/layout/css/custom.min.css");?>" rel="stylesheet" type="text/css" />

		<!-- CUSTOM AREA -->
	</head>

	<!-- ------------------------------------------------------------------ -->
	<!-- BODY IS BEGIN                                                      -->
	<!-- ------------------------------------------------------------------ -->
	<body class="page-header-fixed page-sidebar-closed-hide-logo page-md">
		<?php $this->load->view("administrator_area/__shards/HTML_DarkBlueHeader",array());?>
		<div class="page-container">
			<?php $this->load->view("administrator_area/__shards/HTML_DarkBlueSideBar",array());?>
			<div class="page-content-wrapper">
				<div class="page-content">
					<h3 class="page-title">
						<?php echo $configurations["PAGE_HEADER"]["MAIN_TITLE"]?>
						<small>
							<?php echo $configurations["PAGE_HEADER"]["SUB_TITLE"]?>
						</small>
					</h3>
					<?php $this->load->view("administrator_area/__shards/HTML_DarkBlueBreadCrumb",array());?>

					<!-- ------------------------------------------------------------------ -->
					<!-- CONTENT IS BEGIN                                                   -->
					<!-- ------------------------------------------------------------------ -->
					<?php
					if(checkAdministratorPermission("PRODUCT_CATEGORY", "index")){
					?>
						<div class="row">
							<div class="col-md-12">
								<!-- BEGIN EXAMPLE TABLE PORTLET-->
								<div class="portlet light bordered">
									<div class="portlet-title">
										<div class="caption font-green">
											<i class="<?php echo $configurations["PORTLET_HEADER"]["ICON"]?> font-green"></i>
											<span class="caption-subject bold uppercase"><?php echo $configurations["PORTLET_HEADER"]["TITLE"]?></span> : <?php echo $product_category->name_en.(isset($product_sub_category->name_en) ? ' > '.$product_sub_category->name_en  : '')?>
										</div>
										<div class="actions">
											<a href="<?php echo base_url("administrator_area/product_category/subcategory/".$product_sub_category->product_category_id)?>" class= "btn btn-default">
                          <i class="fa fa-chevron-left"></i> Back
                        </a>
                    </div>
									</div>
									<div class="portlet-body">
										<form class="form-horizontal" role="form"  method="post" id="main-form">
											<table class="table table-striped table-bordered table-hover " id="main-table">
												<thead>
													<tr>
														<th style="text-align: center;">Filter By</th>
														<th style="width:45%;"> Technical Name (TH) </th>
														<th style="width:45%;"> Technical Name (EN) </th>
													</tr>
												</thead>
												<tbody>
													<?php if($technical_specs != null && count($technical_specs) > 0){
														foreach ($technical_specs as $technical_spec) {
													?>
														<tr>
															<td style="text-align: center;">
																<input type="checkbox" name="technical_specid[]" id="<?php echo "check-".$technical_spec->{"technical_spec::id"} ?>" value="<?php echo $technical_spec->{"technical_spec::id"} ?>"   <?php if(isset($technical_spec->{"filter_category::id"})){ echo "checked";} ?> >
															</td>
															<td><?php echo $technical_spec->{"technical_spec::name_th"}; ?></td>
															<td><?php echo $technical_spec->{"technical_spec::name_en"}; ?></td>
														</tr>
													<?php 
														}
													} ?>
												</tbody>
											</table>
											<button type="button" class="btn green-jungle" data-toggle="modal" data-target="#confirmation-save" data-backdrop="static" data-keyboard="false">Save Changes</button>
										</form>
									</div>
								</div>
								<!-- END EXAMPLE TABLE PORTLET-->
							</div>
						</div>
					<?php
					}else{
					?>
						<div class="row">
							<div class="col-md-12">
								<div style="text-align: center;">
									<div class="row" style="padding-top: 50px;">
										<div class="page-spinner-bar-custom">
											<div class="bounce1"></div>
											<div class="bounce2"></div>
											<div class="bounce3"></div>
											<div class="bounce4"></div>
											<div class="bounce5"></div>
										</div>
									</div>
									<div class="caption font-red-thunderbird row" style="padding-top: 30px;padding-bottom: 50px;">
										<i class="fa fa-unlock-alt font-red-thunderbird"></i><br>
										<span class="caption-subject bold uppercase"> Permission Denied<br><small>You are not allow to use this function.</small></span><br><br>
									</div>
								</div>
							</div>
						</div>
					<?php
					}
					?>
				</div>
			</div>
		</div>

		<?php $this->load->view("administrator_area/__shards/HTML_DarkBlueFooter",array());?>
	</body>

	<!-- ------------------------------------------------------------------ -->
	<!-- JAVASCRIPT IS BEGIN                                                -->
	<!-- ------------------------------------------------------------------ -->
	<!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/respond.min.js');?>"></script> <![endif]-->
	<!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/excanvas.min.js');?>"></script> <![endif]-->

	<!-- MANDATORY SCRIPT AREA -->
	<script src="<?php echo assetsDirectory("global/plugins/jquery.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/bootstrap/js/bootstrap.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/js.cookie.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/jquery-slimscroll/jquery.slimscroll.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/jquery.blockui.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/bootstrap-switch/js/bootstrap-switch.min.js");?>" type="text/javascript"></script>

	<!-- PLUGINS AREA -->
	<script src="<?php echo assetsDirectory("global/plugins/jquery-ui/jquery-ui.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/moment.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/clockface/js/clockface.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/select2/js/select2.full.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/jquery.validate.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/additional-methods.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.js");?>"></script>
	<script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote-cleaner.js");?>"></script>
	<script src="<?php echo assetsDirectory("global/plugins/jstree/dist/jstree.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("global/scripts/datatable.js");?>" type="text/javascript"></script>

	<script src="<?php echo assetsDirectory("global/plugins/backstretch/jquery.backstretch.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.js");?>" type="text/javascript"></script>

	<!-- THEME SCRIPT AREA -->
	<script src="<?php echo assetsDirectory("global/scripts/app.min.js");?>" type="text/javascript"></script>

	<!-- LAYOUT AREA -->
	<script src="<?php echo assetsDirectory("layouts/layout/scripts/layout.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("layouts/layout/scripts/demo.min.js");?>" type="text/javascript"></script>
	<script src="<?php echo assetsDirectory("layouts/global/scripts/quick-sidebar.js");?>" type="text/javascript"></script>

	<!-- CUSTOM AREA -->
	<script>
		jQuery(document).ready(function() {
			$("#main-form").validate({
        errorElement: 'span', errorClass: 'help-block', focusInvalid: false,
        rules: {
        },
        highlight: function (element) { $(element).closest('.form-group').addClass('has-error'); },
        success: function (label) { label.closest('.form-group').removeClass('has-error'); label.remove(); },
        invalidHandler: function(form, validator) { $('.confirmation-save').modal('toggle'); },
        errorPlacement: function (error, element) { error.appendTo(element.closest('div')); },
        submitHandler: function() {
          App.blockUI({ target: 'body', animate: true });
          $.ajax({
            type: "POST",
            <?php
            $post_url = base_url("administrator_area/product_category/edit_filter_category/".$product_sub_category->id);
            ?>
            url: "<?php echo $post_url?>",
            data: new FormData($('#main-form')[0]),
            cache: false,
            contentType: false,
            processData: false,
            success: function(response){
              try {
                if(response.code == "0x0000-00000") {
                  toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                  toastr.success('Update Completed', 'Success.');
                }else{
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch(e) {
                $("#system-return-failed").modal("toggle");
              }
              App.unblockUI('body');
            },
            error: function(){
              $("#system-disconnected").modal("toggle");
              App.unblockUI('body');
            }
          });
        }
      });
		});
	</script>

	<!-- INCLUDE RAW SCRIPT AREA -->
	<?php $this->load->view("administrator_area/__scripts/Javascript_Datatables",array());?>
	<?php $this->load->view("administrator_area/__scripts/Javascript_Modal",array());?>
	<?php $this->load->view("administrator_area/__scripts/Javascript_Select2",array());?>
</html>

<!-- ------------------------------------------------------------------ -->
<!-- MODAL IS BEGIN                                                     -->
<!-- ------------------------------------------------------------------ -->
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationCancel");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationDelete");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationRefresh");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationSave");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationSaveMultiform");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_PermissionDenied");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SummernoteGallery");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemDisconnected");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnError");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnFailed");?>