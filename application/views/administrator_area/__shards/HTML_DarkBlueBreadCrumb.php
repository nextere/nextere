<?php
$codeigniter_instance =& get_instance();
?>

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <span><?php echo str_replace("_"," ",preg_replace('/[A-Z]/', ' $0', ucfirst("Home")))?></span>
            <i class="fa fa-angle-right"></i>
        </li>
        <?php
        $current_url_array = explode("/",uri_string());
        foreach($current_url_array as $current_url_key => $current_url_value){
            ?>
            <li>
                <span><?php echo str_replace("_"," ",preg_replace('/[A-Z]/', ' $0', ucfirst($current_url_value)))?></span>
                <?php
                if(isset($current_url_array[$current_url_key+1])){
                    ?>
                    <i class="fa fa-angle-right"></i>
                    <?php
                }
                ?>
            </li>
            <?php
        }
        ?>
    </ul>
</div>