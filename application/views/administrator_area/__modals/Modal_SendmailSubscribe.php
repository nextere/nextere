<div class="modal fade" id="sendmail_subscribe" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-envelope"></i> Sending Email</h5>
        
      </div>
      <div class="modal-body">
        <form  method="post" id="main-form">
          <div class="form-group" >
              <label class="col-md-2 control-label">News :</label>
              <div class="col-md-9">
                <select class="form-control select2" name="news_id" id="news_id"></select>
              </div>
          </div>
        </form>
      </div>
      <br>
      <hr>
      <div class="modal-footer">
      <div style="text-align: center;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="send-button">Send</button>
      </div>
      </div>
    </div>
  </div>
</div>
<script>
    jQuery(document).ready(function() {
        $("#send-button").attr("onclick","$('#main-form').submit();");
    });
</script>