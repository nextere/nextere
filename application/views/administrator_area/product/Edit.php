<!-- ------------------------------------------------------------------ -->
<!-- PERMISSION CHECK IS BEGIN                                          -->
<!-- ------------------------------------------------------------------ -->
<?php
$codeigniter_instance =& get_instance();

$configurations = array(
  "PAGE_TITLE" => "Administrator System",
  "PAGE_HEADER" => array(
   "MAIN_TITLE" => "Product Management",
   "SUB_TITLE" => ""
  ),
  "PORTLET_HEADER" => array(
    "ICON" => "fa fa-bars",
    "TITLE" => "Product Detail"
  )
);
?>
<!-- ------------------------------------------------------------------ -->
<!-- HEADER IS BEGIN                                                    -->
<!-- ------------------------------------------------------------------ -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
  <head>
    <title><?php echo $this->config->item('WEBSITE_NAME');?> | Login System</title>
    <link href="<?php echo faviconDirectory($this->config->item('WEBSITE_FAVICON'));?>" rel="shortcut icon" />

    <!-- META TAG AREA -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- MANDATORY STYLE AREA -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/simple-line-icons/simple-line-icons.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-switch/css/bootstrap-switch.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- PLUGINS AREA -->
    <link href="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/clockface/css/clockface.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2-bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/jstree/dist/themes/default/style.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/dropzone/basic.min.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.css");?>" rel="stylesheet">
    <link href="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- THEME STYLE AREA -->
    <link href="<?php echo assetsDirectory("global/css/components-md.min.css");?>" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo assetsDirectory("global/css/plugins-md.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- LAYOUT AREA -->
    <link href="<?php echo assetsDirectory("layouts/layout/css/layout.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo assetsDirectory("layouts/layout/css/themes/light.min.css");?>" rel="stylesheet" type="text/css" id="style_color" />
    <link href="<?php echo assetsDirectory("layouts/layout/css/custom.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- CUSTOM AREA -->
    <style>
      .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice {
        color: #fff;
        background: #53c6d2;
        border: 1px solid #53c6d2;
        margin: 5px 0 0 6px;
        padding: 0 35px 0 6px;
      }
      .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice__remove {
        margin-right: 30px;
        color: #e7505a;
      }
      .product-image {
        height: 150px;
        margin-top: 10px;
      }
    </style>
  </head>

  <!-- ------------------------------------------------------------------ -->
  <!-- BODY IS BEGIN                                                      -->
  <!-- ------------------------------------------------------------------ -->
  <body class="page-header-fixed page-sidebar-closed-hide-logo page-md">
    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueHeader",array());?>
    <div class="page-container">
      <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueSideBar",array());?>
      <div class="page-content-wrapper">
        <div class="page-content">
          <h3 class="page-title">
              <?php echo $configurations["PAGE_HEADER"]["MAIN_TITLE"]?>
              <small>
                  <?php echo $configurations["PAGE_HEADER"]["SUB_TITLE"]?>
              </small>
          </h3>
          <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueBreadCrumb",array());?>

          <!-- ------------------------------------------------------------------ -->
          <!-- CONTENT IS BEGIN                                                   -->
          <!-- ------------------------------------------------------------------ -->
          
          <div class="row">
            <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light bordered">
                <div class="portlet-title">
                  <div class="caption font-green">
                    <i class="<?php echo $configurations["PORTLET_HEADER"]["ICON"]?> font-green"></i>
                    <span class="caption-subject bold uppercase">
                      <?php echo $configurations["PORTLET_HEADER"]["TITLE"]?>
                    </span>
                  </div>
                  <?php if(isset($product->{"product::id"})){ ?>
                    <div class="actions">
                      <a href="<?php echo base_url("administrator_area/product/edit_technical/".$product->{"product::id"})?>" class="btn btn-sm btn-warning">Technical Spec</a>
                    </div>
                  <?php } ?>

                </div>
                <div class="portlet-body">
                  <form class="form-horizontal" role="form" method="post" id="main-form" enctype="multipart/form-data"> 
                    <input type="hidden" name="session_time" value="<?php echo date("YmdHis")?>">
                    <div class="form-group">
                      <label class="col-md-2 control-label">Code<span class="required"> *</span></label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" name="code" autocomplete="off" placeholder="Code" value="<?php if(isset($product->{"product::code"}))  echo $product->{"product::code"} ?>">
                      </div>
                      <label class="col-md-3 control-label">Brand</label>
                      <div class="col-md-3">
                        <select class="form-control select2" name="brand_id">
                          <?php
                          if(isset($product->{"brand::id"})){
                          ?>
                            <option value="<?php echo $product->{"brand::id"}?>" selected><?php echo $product->{"brand::name_en"}?></option>
                          <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Name (TH)<span class="required"> *</span></label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" name="name_th" autocomplete="off" placeholder="Name (TH)" value="<?php if(isset($product->{"product::name_th"}))  echo $product->{"product::name_th"}  ?>">
                      </div>
                      <label class="col-md-3 control-label">Name (EN)<span class="required"> *</span></label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" name="name_en" autocomplete="off" placeholder="Name (EN)" value="<?php if(isset($product->{"product::name_en"}))  echo $product->{"product::name_en"}  ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Model (TH)</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" name="model_name_th" autocomplete="off" placeholder="Model (TH)" value="<?php if(isset($product->{"product::model_name_th"}))  echo $product->{"product::model_name_th"}  ?>">
                      </div>
                      <label class="col-md-3 control-label">Model (EN)</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" name="model_name_en" placeholder="Model (EN)" autocomplete="off" value="<?php if(isset($product->{"product::model_name_en"}))  echo $product->{"product::model_name_en"}  ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Type</label>
                      <div class="col-md-3">
                        <select class="form-control select2" name="product_type_id">
                          <?php
                          if(isset($product->{"product_type::id"})){
                          ?>
                            <option value="<?php echo $product->{"product_type::id"}?>" selected><?php echo $product->{"product_type::name_en"}?></option>
                          <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Detail (TH)</label>
                      <div class="col-md-9">
                        <textarea class="form-control" rows="2"  name="detail_th"><?php if(isset($product->{"product::detail_th"})) echo $product->{"product::detail_th"}  ?></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Detail (EN)</label>
                      <div class="col-md-9">
                        <textarea class="form-control" rows="2"  name="detail_en"><?php if(isset($product->{"product::detail_en"})) echo $product->{"product::detail_en"}  ?></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Description (TH)</label>
                      <div class="col-md-9">
                        <textarea class="form-control" rows="3"  id="description_th" name="description_th"><?php if(isset($product->{"product::description_th"})) echo $product->{"product::description_th"}  ?></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Description (EN)</label>
                      <div class="col-md-9">
                        <textarea class="form-control" rows="3"  id="description_en" name="description_en"><?php if(isset($product->{"product::description_en"})) echo $product->{"product::description_en"}  ?></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Basic Quality (TH)</label>
                      <div class="col-md-9">
                        <textarea class="form-control" rows="3"  name="basic_quality_th"><?php if(isset($product->{"product::basic_quality_th"})) echo $product->{"product::basic_quality_th"}  ?></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Basic Quality (EN)</label>
                      <div class="col-md-9">
                        <textarea class="form-control" rows="3"  name="basic_quality_en"><?php if(isset($product->{"product::basic_quality_en"})) echo $product->{"product::basic_quality_en"}  ?></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Retail Price</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control pos-decimal" name="retail_price" placeholder="0" autocomplete="off" value="<?php if(isset($product->{"product::retail_price"}))  echo $product->{"product::retail_price"}  ?>">
                      </div>
                      <label class="col-md-3 control-label">Waiting Day <br>
                        <small style="font-style: normal;color: lightslategray;">(if product out of stock)</small>
                      </label>
                      <div class="col-md-3">
                        <input type="text" class="form-control number" name="waiting_date" placeholder="0" autocomplete="off" value="<?php if(isset($product->{"product::waiting_date"}))  echo $product->{"product::waiting_date"}  ?>" >
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Minimum Quantity/Order</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control number" name="minimum_product" placeholder="0" autocomplete="off" value="<?php if(isset($product->{"product::minimum_product"}))  echo $product->{"product::minimum_product"}  ?>">
                      </div>
                      <label class="col-md-3 control-label">Delivery Price/Qty</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control pos-decimal" name="delivery_price" placeholder="0" autocomplete="off" value="<?php if(isset($product->{"product::delivery_price"}))  echo $product->{"product::delivery_price"}  ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Condition of Product (TH)</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="condition_product_th" autocomplete="off" placeholder="Condition of Product (TH)" value="<?php if(isset($product->{"product::condition_product_th"}))  echo $product->{"product::condition_product_th"}  ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Condition of Product (EN)</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="condition_product_en" autocomplete="off" placeholder="Condition of Product (EN)" value="<?php if(isset($product->{"product::condition_product_en"}))  echo $product->{"product::condition_product_en"}  ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Condition of Warranty (TH)</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="condition_warranty_th" placeholder="Condition of Warranty (TH)" autocomplete="off" value="<?php if(isset($product->{"product::condition_warranty_th"}))  echo $product->{"product::condition_warranty_th"}  ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Condition of Warranty (EN)</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="condition_warranty_en" placeholder="Condition of Warranty (EN)" autocomplete="off" value="<?php if(isset($product->{"product::condition_warranty_en"}))  echo $product->{"product::condition_warranty_en"}  ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">In Stock Qty</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control number" name="in_stock_qty" placeholder="0" autocomplete="off" value="<?php if(isset($product->{"product::in_stock_qty"}))  echo $product->{"product::in_stock_qty"}  ?>">
                      </div>
                      <label class="col-md-3 control-label">Safety Stock</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control number" name="safety_stock" placeholder="0" autocomplete="off" value="<?php if(isset($product->{"product::safety_stock"}))  echo $product->{"product::safety_stock"}  ?>" >
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Search Keyword <br>
                        <small style="font-style: normal;color: lightslategray;">Seperate by comma</small>
                      </label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="search_keyword" value="<?php if(isset($product->{"product::search_keyword"})) echo $product->{"product::search_keyword"};?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Add Image <br><small>Image size 375*500</small></label>
                      <div class="col-md-6">
                        <input type="file" name="files[]" class="form-control" multiple="" accept="image/*"> 
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label"></label>
                      
                        <?php if($product_images != null && count($product_images) > 0) {
                          foreach ($product_images as $image) {
                        ?>
                            <div class="col-md-3 product-image" style="text-align: center;">
                              <img src="<?php echo uploadsDirectory("product/".$product->{"product::code"}."/".$image)?>"  style="max-width: 100%; max-height: 100%;" >
                              <button class="btn btn-circle btn-icon-only btn-default remove-image" type="button" title="Delete"><i class="icon-trash"></i></button> 
                              <input type="hidden" name="image_name[]" value="<?php echo $image?>">
                            </div>
                        <?php 
                          }
                         } ?>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Recommend</label>
                      <div class="col-md-3">
                        <div class="mt-checkbox-inline">
                          <label class="mt-checkbox mt-checkbox-outline ">
                            <input type="checkbox" name="is_recommend"  <?php if(isset($product->{"product::is_recommend"}) && $product->{"product::is_recommend"} == 1) echo "checked";?> >
                            <span></span>
                          </label>
                        </div>
                      </div>
                      <label class="col-md-3 control-label">Best Seller</label>
                      <div class="col-md-3">
                        <div class="mt-checkbox-inline">
                          <label class="mt-checkbox mt-checkbox-outline ">
                            <input type="checkbox" name="is_bestseller"  <?php if(isset($product->{"product::is_bestseller"}) && $product->{"product::is_bestseller"} == 1) echo "checked";?> >
                            <span></span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">New Product</label>
                      <div class="col-md-3">
                        <div class="mt-checkbox-inline">
                          <label class="mt-checkbox mt-checkbox-outline ">
                            <input type="checkbox" name="is_new"  <?php if(isset($product->{"product::is_new"}) && $product->{"product::is_new"} == 1) echo "checked";?> >
                            <span></span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Status<span class="required"> *</span></label>
                      <div class="col-md-3">
                        <div class="mt-radio-inline">
                          <label class="mt-radio">
                            <input type="radio" name="status" value="ACTIVATE" <?php if(isset($product->{"product::status"}) && $product->{"product::status"} == "ACTIVATE")echo "checked"?>> Activate
                            <span></span>
                          </label>
                          <label class="mt-radio">
                            <input type="radio" name="status" value="SUSPEND" <?php if(!isset($product->{"product::status"}) || $product->{"product::status"} != "ACTIVATE")echo "checked"?>> Suspend
                            <span></span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <div class="form-actions">
                      <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                          <button type="button" class="btn green-jungle" data-toggle="modal" data-target="#confirmation-save" data-backdrop="static" data-keyboard="false">Save Changes</button>
                          <button type="button" class="btn red btn-outline" data-toggle="modal" data-target="#confirmation-cancel" data-backdrop="static" data-keyboard="false">Cancel</button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
            </div>
          </div>
          <?php if(isset($product->{"product::id"})) {?>
            <div class="row">
              <div class="col-md-12">
                <div class="portlet light bordered" >
                  <div class="portlet-title">
                    <div class="caption font-green" >
                      <span class="caption-subject bold uppercase">
                        Product Feature
                      </span>
                    </div>
                    <div class="caption font-green  pull-right">
                      <button type="button" class="btn green-jungle btn-sx open-create-feature-modal" data-product_id="<?php echo $product->{"product::id"}?>"> ADD</button>
                    </div>
                  </div>
                  <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover order-column product-feature-list" data-product_id="<?php echo $product->{"product::id"}?>">
                      <thead>
                        <tr>
                          <th style="width:10%;"> Sort</th>
                          <th style="width:20%;"> Title(TH)</th>
                          <th style="width:25%;"> Description(TH)</th>
                          <th style="width:15%;"> Image </th>
                          <th style="width:20%;">Action</th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="portlet light bordered" >
                  <div class="portlet-title">
                    <div class="caption font-green" >
                      <span class="caption-subject bold uppercase">
                        Product Tutorial
                      </span>
                    </div>
                    <div class="caption font-green  pull-right">
                      <button type="button" class="btn green-jungle btn-sx open-create-tutorial-modal" data-product_id="<?php echo $product->{"product::id"}?>"> ADD</button>
                    </div>
                  </div>
                  <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover order-column product-tutorial-list" data-product_id="<?php echo $product->{"product::id"}?>">
                      <thead>
                        <tr>
                          <th style="width:10%;"> Sort</th>
                          <th style="width:20%;"> Title(TH)</th>
                          <th style="width:20%;"> Title(EN)</th>
                          <th style="width:30%;"> Link Url</th>
                          <th style="width:20%;">Action</th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="portlet light bordered" >
                  <div class="portlet-title">
                    <div class="caption font-green" >
                      <span class="caption-subject bold uppercase">
                        Product DataSheet
                      </span>
                    </div>
                    <div class="caption font-green  pull-right">
                      <button type="button" class="btn green-jungle btn-sx open-create-datasheet-modal" data-product_id="<?php echo $product->{"product::id"}?>"> ADD</button>
                    </div>
                  </div>
                  <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover order-column product-datasheet-list" data-product_id="<?php echo $product->{"product::id"}?>">
                      <thead>
                        <tr>
                          <th style="width:10%;"> Sort</th>
                          <th style="width:60%;"> Name</th>
                          <th style="width:10%;"> File</th>
                          <th style="width:20%;">Action</th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="portlet light bordered" >
                  <div class="portlet-title">
                    <div class="caption font-green" >
                      <span class="caption-subject bold uppercase">
                        Price by Quantity
                      </span>
                    </div>
                  </div>
                  <div class="portlet-body">
                    <form class="form-horizontal" role="form" method="post" id="price-quantity-form" enctype="multipart/form-data">
                      <input type="hidden" name="session_time" value="<?php echo date("YmdHis")?>">
                      <div class="form-group">
                        <label class="col-md-2 control-label">Minimum Quantity<span class="required"> *</span></label>
                        <div class="col-md-2">
                          <input type="text" class="form-control number" name="minimum_qty" autocomplete="off" value="">
                        </div>
                        <label class="col-md-2 control-label">Price<span class="required"> *</span></label>
                        <div class="col-md-2">
                          <input type="text" class="form-control pos-decimal" name="price" autocomplete="off" value="">
                        </div>
                        <div class="col-md-3">
                          <button type="button" class="btn green-jungle create-price-qty-button" data-product_id="<?php echo $product->{"product::id"}?>">Add</button>
                        </div>
                      </div>
                    </form>
                    <hr>
                    <table class="table table-striped table-bordered table-hover order-column product-price-list" data-product_id="<?php echo $product->{"product::id"}?>">
                      <thead>
                        <tr>
                          <th style="width:40%;"> Minimun Quantity</th>
                          <th style="width:40%;"> Price </th>
                          <th style="width:20%;"> Action </th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="portlet light bordered" >
                  <div class="portlet-title">
                    <div class="caption font-green" >
                      <span class="caption-subject bold uppercase">
                        Product Category
                      </span>
                    </div>
                  </div>
                  <div class="portlet-body">
                    <form class="form-horizontal" role="form" method="post" id="category-form" enctype="multipart/form-data">
                      <input type="hidden" name="session_time" value="<?php echo date("YmdHis")?>">
                      <div class="form-group">
                        <label class="col-md-2 control-label">Category<span class="required"> *</span></label>
                        <div class="col-md-3">
                          <select class="form-control select2" name="product_category_id">
                          </select>
                        </div>
                        <label class="col-md-2 control-label">Sub Category</label>
                        <div class="col-md-3">
                          <select class="form-control select2" name="product_sub_category_id">
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-3">
                          <button type="button" class="btn green-jungle create-category-button" data-product_id="<?php echo $product->{"product::id"}?>">Add</button>
                        </div>
                      </div>
                    </form>
                    <hr>
                    <table class="table table-striped table-bordered table-hover order-column product-category-list" data-product_id="<?php echo $product->{"product::id"}?>">
                      <thead>
                        <tr>
                          <th style="width:40%;"> Category</th>
                          <th style="width:40%;"> Sub Category </th>
                          <th style="width:20%;"> Action </th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="portlet light bordered" >
                  <div class="portlet-title">
                    <div class="caption font-green" >
                      <span class="caption-subject bold uppercase">
                        Solution Category
                      </span>
                    </div>
                  </div>
                  <div class="portlet-body">
                    <form class="form-horizontal" role="form" method="post" id="menu-form" enctype="multipart/form-data">
                      <input type="hidden" name="session_time" value="<?php echo date("YmdHis")?>">
                      <div class="form-group">
                        <label class="col-md-2 control-label">Menu<span class="required"> *</span></label>
                        <div class="col-md-3">
                          <select class="form-control select2" name="menu_category_id">
                          </select>
                        </div>
                        <label class="col-md-2 control-label">Sub Category</label>
                        <div class="col-md-3">
                          <select class="form-control select2" name="submenu_category_id">
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Child Category</label>
                        <div class="col-md-3">
                          <select class="form-control select2" name="child_submenu_category_id">
                          </select>
                        </div>
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-3">
                          <button type="button" class="btn green-jungle create-menu-button" data-product_id="<?php echo $product->{"product::id"}?>">Add</button>
                        </div>
                      </div>
                    </form>
                    <hr>
                    <table class="table table-striped table-bordered table-hover order-column menu-category-list" data-product_id="<?php echo $product->{"product::id"}?>">
                      <thead>
                        <tr>
                          <th style="width:25%;"> Menu</th>
                          <th style="width:25%;"> Sub Category </th>
                          <th> Child Category </th>
                          <th style="width:20%;"> Action </th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="portlet light bordered" >
                  <div class="portlet-title">
                    <div class="caption font-green" >
                      <span class="caption-subject bold uppercase">
                        Relate Product
                      </span>
                    </div>
                  </div>
                  <div class="portlet-body">
                    <form class="form-horizontal" role="form" method="post" id="product-relate-form" enctype="multipart/form-data">
                      <input type="hidden" name="session_time" value="<?php echo date("YmdHis")?>">
                      <div class="form-group">
                        <label class="col-md-2 control-label">Product<span class="required"> *</span></label>
                        <div class="col-md-3">
                          <select class="form-control select2" name="relate_product_id">
                          </select>
                        </div>
                        <div class="col-md-3">
                          <button type="button" class="btn green-jungle create-product-relate-button" data-product_id="<?php echo $product->{"product::id"}?>">Add</button>
                        </div>
                      </div>
                    </form>
                    <hr>
                    <table class="table table-striped table-bordered table-hover order-column product-relate-list" data-product_id="<?php echo $product->{"product::id"}?>">
                      <thead>
                        <tr>
                          <th style="width:25%;"> Name</th>
                          <th style="width:25%;"> Model </th>
                          <th> Code</th>
                          <th style="width:20%;"> Action </th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>

    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueFooter",array());?>
  </body>

  <!-- ------------------------------------------------------------------ -->
  <!-- JAVASCRIPT IS BEGIN                                                -->
  <!-- ------------------------------------------------------------------ -->
  <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/respond.min.js');?>"></script> <![endif]-->
  <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/excanvas.min.js');?>"></script> <![endif]-->

  <!-- MANDATORY SCRIPT AREA -->
  <script src="<?php echo assetsDirectory("global/plugins/jquery.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap/js/bootstrap.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/js.cookie.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-slimscroll/jquery.slimscroll.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery.blockui.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-switch/js/bootstrap-switch.min.js");?>" type="text/javascript"></script>

  <!-- PLUGINS AREA -->
  <script src="<?php echo assetsDirectory("global/plugins/jquery-ui/jquery-ui.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/moment.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/clockface/js/clockface.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/select2/js/select2.full.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/jquery.validate.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/additional-methods.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.js");?>"></script>
  <!-- <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote-cleaner.js");?>"></script> -->
  <script src="<?php echo assetsDirectory("global/plugins/jstree/dist/jstree.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/scripts/datatable.js");?>" type="text/javascript"></script>

  <script src="<?php echo assetsDirectory("global/plugins/backstretch/jquery.backstretch.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.js");?>" type="text/javascript"></script>
    

  <!-- THEME SCRIPT AREA -->
  <script src="<?php echo assetsDirectory("global/scripts/app.min.js");?>" type="text/javascript"></script>

  <!-- LAYOUT AREA -->
  <script src="<?php echo assetsDirectory("layouts/layout/scripts/layout.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("layouts/layout/scripts/demo.min.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("layouts/global/scripts/quick-sidebar.js");?>" type="text/javascript"></script>
  <script src="<?php echo assetsDirectory("global/plugins/jquery.numeric.js");?>" type="text/javascript"></script>

  <!-- CUSTOM AREA -->
  <script>
    jQuery(document).ready(function() {
      var summernote_selector = "#description_th";

      // INITIAL SUMMERNOTE
      var summernote_instance = $(summernote_selector).summernote({
          height: 500,
          focus: false,
          toolbar: [
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ]
      });
      var summernote_selector = "#description_en";

      // INITIAL SUMMERNOTE
      var summernote_instance = $(summernote_selector).summernote({
          height: 500,
          focus: false,
          toolbar: [
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ]
      });


      $('.number').numeric({ decimal: false, negative: false });
      $('.pos-decimal').numeric({ negative: false });
      /** =================================================================== **/
      /** SELECT 2                                                            **/
      /** =================================================================== **/
      $("#main-form select[name='brand_id']").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/product/select2/edit_brand")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_en };
              })
            };
          }
        }
      });

      $("#main-form select[name='product_type_id']").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/product/select2/edit_product_type")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_en };
              })
            };
          }
        }
      });


      $("#category-form select[name='product_category_id']").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/product/select2/edit_product_category")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_en };
              })
            };
          }
        }
      });

      $("#category-form select[name='product_sub_category_id']").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/product/select2/edit_product_sub_category")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term,
              product_category_id : $("#category-form select[name='product_category_id']").val()
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_en };
              })
            };
          }
        }
      });

      
      $("#category-form select[name='product_category_id']").on("change",function(){
        $("#category-form select[name='product_sub_category_id']").select2("val", "");
      });


      $("#update-category .category-product-category-id").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/product/select2/edit_product_category")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_en };
              })
            };
          }
        }
      });

      $("#update-category .category-product-sub-category-id").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/product/select2/edit_product_sub_category")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term,
              product_category_id : $("#update-category .category-product-category-id").val()
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_en };
              })
            };
          }
        }
      });


      $("#update-category .category-product-category-id").on("change",function(){
        $("#update-category .category-product-sub-category-id").html('').trigger('change');
        $("#update-category .category-product-childsub-category-id").html('').trigger('change');
      });

      $("#update-category .category-product-sub-category-id").on("change",function(){
        $("#update-category .category-product-childsub-category-id").html('').trigger('change');
      });

      $("select[name='relate_product_id']").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/product/select2/edit_product")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term,
              product_id: "<?php echo isset($product->{"product::id"}) ? $product->{"product::id"} : ''?>"
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_en+' ('+obj.code+')' };
              })
            };
          }
        }
      });

      $("#menu-form select[name='menu_category_id']").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/product/select2/edit_menu_category")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_en };
              })
            };
          }
        }
      });

      $("#menu-form select[name='submenu_category_id']").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/product/select2/edit_submenu_category")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term,
              menu_category_id : $("#menu-form select[name='menu_category_id']").val()
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_en };
              })
            };
          }
        }
      });

      $("#menu-form select[name='child_submenu_category_id']").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/product/select2/edit_child_submenu_category")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term,
              submenu_category_id : $("#menu-form select[name='submenu_category_id']").val()
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_en };
              })
            };
          }
        }
      });

      $("#menu-form select[name='menu_category_id']").on("change",function(){
        $("#menu-form select[name='submenu_category_id']").select2("val", "");
        $("#menu-form select[name='child_submenu_category_id']").select2("val", "");
      });

      $("#menu-form select[name='submenu_category_id']").on("change",function(){
        $("#menu-form select[name='child_submenu_category_id']").select2("val", "");
      });

      $("#update-menu .menu-menu-category-id").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/product/select2/edit_menu_category")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_en };
              })
            };
          }
        }
      });

      $("#update-menu .menu-submenu-category-id").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/product/select2/edit_submenu_category")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term,
              product_category_id : $("#update-menu .menu-menu-category-id").val()
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_en };
              })
            };
          }
        }
      });

      $("#update-menu .menu-child-submenu-category-id").select2({
        placeholder: "Select...", width: null, allowClear: true,
        ajax: {
          url: "<?php echo base_url("administrator_area/product/select2/edit_child_submenu_category")?>",
          dataType: 'json', delay: 250, cache: true,
          data: function(params) {
            return {
              search: params.term,
              product_sub_category_id : $("#update-menu .menu-submenu-category-id").val()
            };
          },
          processResults: function(data) {
            return {
              results: $.map(data.data, function(obj) {
                return { id: obj.id, text: obj.name_en };
              })
            };
          }
        }
      });

      $("#update-menu .menu-menu-category-id").on("change",function(){
        $("#update-menu .menu-submenu-category-id").html('').trigger('change');
        $("#update-menu .menu-child-submenu-category-id").html('').trigger('change');
      });

      $("#update-menu .menu-submenu-category-id").on("change",function(){
        $("#update-menu .menu-child-submenu-category-id").html('').trigger('change');
      });

      /** =================================================================== **/
      /** FORM VALIDATE                                                       **/
      /** =================================================================== **/

      $(".remove-image").on("click", function () {
        if (confirm("Are you want to delete selected file?")) {
          $(this).closest('div').remove();
        }
      });

      $("#main-form").validate({
        errorElement: 'span', errorClass: 'help-block', focusInvalid: false,
        rules: {
          code: {required: true},
          name_en: {required: true},
          name_en: {required:true},
          status: {required: true}
        },
        highlight: function (element) { $(element).closest('.form-group').addClass('has-error'); },
        success: function (label) { label.closest('.form-group').removeClass('has-error'); label.remove(); },
        invalidHandler: function(form, validator) { $('.confirmation-save').modal('toggle'); },
        errorPlacement: function (error, element) { error.appendTo(element.closest('div')); },
        submitHandler: function() {
          App.blockUI({ target: 'body', animate: true });
          $.ajax({
            type: "POST",
            <?php
            $post_url = "";
            if(isset($product->{"product::id"})){
              $post_url = base_url("administrator_area/product/edit/".$product->{"product::id"});
            }else{
              $post_url = base_url("administrator_area/product/edit");
            }
            ?>
            url: "<?php echo $post_url?>",
            data: new FormData($('#main-form')[0]),
            cache: false,
            contentType: false,
            processData: false,
            success: function(response){
              try {
                if(response.code == "0x0000-00000") {
                  toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "positionClass": "toast-bottom-right",
                  "onclick": null,
                  "showDuration": "1000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                toastr.success('Update Completed', 'Success.');

                window.location.assign("<?php echo base_url("administrator_area/product/edit")?>/"+response.data);
                }else{
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch(e) {
                $("#system-return-failed").modal("toggle");
              }
              App.unblockUI('body');
            },
            error: function(){
              $("#system-disconnected").modal("toggle");
              App.unblockUI('body');
            }
          });
        }
      });

      //Feature 
      $(".product-feature-list").dataTable({
        processing: true,
        serverSide: true,
        pageLength: 25,
        searching: false,
        ajax:{
          url: "<?php echo base_url("administrator_area/product/datatables/index_product_feature")?>",
          type: "POST",
          data:{
            product_id: $(".product-feature-list").data("product_id")
          }
        },
        columns: [
          {
            data: 'sort'
          },
          {
            data: 'title_th'
          },
          {
            data: 'description_th'
          },
          {
            render: function(data, type, row, meta){
              var action_header = "";
              if(row['image_path'] != ''){
                action_header = "<img src='<?php echo uploadsDirectory('')?>"+row['image_path']+"' style='max-width: 100%; max-height: 100%;' >";
              }
              return action_header;
            },
            searchable: false,
            orderable: false
          },
          {
            render: function(data, type, row, meta){
              var action_header = '<button type="button" class="btn yellow-crusta open-update-feature-modal" ' +
                'data-id="'+row["id"]+'" ' +
                'data-product_id="'+row["product_id"]+'" ' +
                'data-title_th="'+row["title_th"]+'" ' +
                'data-title_en="'+row["title_en"]+'" ' +
                'data-description_th="'+row["description_th"]+'" ' +
                'data-description_en="'+row["description_en"]+'" ' +
                'data-image_path="<?php echo uploadsDirectory('')?>'+row["image_path"]+'" ' +
                'data-sort="'+row["sort"]+'" ' +
                '> Edit</button>' +
              '<button type="button" class="btn red-thunderbird open-remove-feature" data-id="'+row["id"]+'"> Delete <i class="fa fa-trash"></i></button>';
              return action_header;
            },
            searchable: false,
            orderable: false
          }
        ]
      });

      $(document).on("click", ".open-create-feature-modal", function(){
        $("#update-feature .feature-id").val('');
        $("#update-feature .feature-product-id").val($(this).data('product_id'));
        $("#update-feature .feature-title-th").val('');
        $("#update-feature .feature-title-en").val('');
        $("#update-feature .feature-description-th").val('');
        $("#update-feature .feature-description-en").val('');
        $("#update-feature .feature-sort").val('');
        $("#update-feature .feature-file").val('');
        $("#feature-img").attr("src", '');
        $('#update-feature').modal('show');
      });

      $(document).on("click", ".open-update-feature-modal", function(){
        $("#update-feature .feature-id").val($(this).data('id'));
        $("#update-feature .feature-product-id").val($(this).data('product_id'));
        $("#update-feature .feature-title-th").val($(this).data('title_th'));
        $("#update-feature .feature-title-en").val($(this).data('title_en'));
        $("#update-feature .feature-description-th").val($(this).data('description_th'));
        $("#update-feature .feature-description-en").val($(this).data('description_en'));
        $("#update-feature .feature-sort").val($(this).data('sort'));
        $("#update-feature .feature-file").val('');
        $("#feature-img").attr("src", $(this).data('image_path'));
        $('#update-feature').modal('show');
      });

      $(document).on("click", ".feature-update-button", function(){
        var title_th = $("#update-feature .feature-title-th").val();
        var title_en = $("#update-feature .feature-title-en").val();
        var description_th = $("#update-feature .feature-description-th").val();
        var description_en = $("#update-feature .feature-description-en").val();
        var sort = $("#update-feature .feature-sort").val();
        $("#update-feature").modal('hide');

        if (title_th == '' || title_en == '' || description_th == '' || description_en == '' || sort == '') {
          $("#system-return-error .message").html("กรุณาระบุข้อมูลให้ครบ");
          $("#system-return-error").modal("toggle");
        }
        else{
          var formData = new FormData($('#feature-form')[0]);
          $.ajax({
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            url: "<?php echo base_url("administrator_area/product/edit_product_feature")?>",
            success: function (response) {
              try {
                if (response.code == "0x0000-00000") {
                  var table = $(".product-feature-list").DataTable();
                  table.columns().search("").draw();

                  toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                  toastr.success('Update Completed', 'Success');
                  $("#update-feature .feature-id").val('');
                  $("#update-feature .feature-product-id").val('');
                  $("#update-feature .feature-title-th").val('');
                  $("#update-feature .feature-title-en").val('');
                  $("#update-feature .feature-description-th").val('');
                  $("#update-feature .feature-description-en").val('');
                  $("#update-feature .feature-sort").val('');
                  $("#update-feature .feature-file").val('');
                  $("#feature-img").attr("src", '');
                } else {
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch (e) {
                $("#system-return-failed").modal("toggle");
              }
            },
            error: function () {
              $("#system-disconnected").modal("toggle");
            }
          });
        }
      });

      $(document).on("click", ".open-remove-feature", function(){
        $("#remove-feature .feature-id").val('');
        
        $("#remove-feature .feature-id").val($(this).data('id'));
        $('#remove-feature').modal('show');
      });

      $(document).on("click", ".feature-remove-button", function(){
        var id = $("#remove-feature .feature-id").val();
        $("#remove-feature").modal('hide');

        var formData = new FormData();
        formData.append('id', id);

        $.ajax({
          data: formData,
          type: "POST",
          contentType: false,
          processData: false,
          url: "<?php echo base_url("administrator_area/product/delete_product_feature")?>",
          success: function (response) {
            try {
              if (response.code == "0x0000-00000") {
                var table = $(".product-feature-list").DataTable();
                table.columns().search("").draw();

                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "positionClass": "toast-bottom-right",
                  "onclick": null,
                  "showDuration": "1000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                toastr.success('Update Completed', 'Success');
              } else {
                $("#system-return-error .message").html(response.message);
                $("#system-return-error").modal("toggle");
              }
            } catch (e) {
              $("#system-return-failed").modal("toggle");
            }
          },
          error: function () {
            $("#system-disconnected").modal("toggle");
          }
        });
      });

      //Tutorial 
      $(".product-tutorial-list").dataTable({
        processing: true,
        serverSide: true,
        pageLength: 25,
        searching: false,
        ajax:{
          url: "<?php echo base_url("administrator_area/product/datatables/index_product_tutorial")?>",
          type: "POST",
          data:{
            product_id: $(".product-feature-list").data("product_id")
          }
        },
        columns: [
          {
            data: 'sort'
          },
          {
            data: 'title_th'
          },
          {
            data: 'title_en'
          },
          {
            render: function(data, type, row, meta){
              var action_header = "";
              if(row['link_url'] != ''){
                action_header = "<a href='"+row['link_url']+"' target='_blank' >"+row['link_url']+"</a>";
              }
              return action_header;
            }
          },
          {
            render: function(data, type, row, meta){
              var action_header = '<button type="button" class="btn yellow-crusta open-update-tutorial-modal" ' +
                'data-id="'+row["id"]+'" ' +
                'data-product_id="'+row["product_id"]+'" ' +
                'data-title_th="'+row["title_th"]+'" ' +
                'data-title_en="'+row["title_en"]+'" ' +
                'data-link_url="'+row["link_url"]+'" ' +
                'data-sort="'+row["sort"]+'" ' +
                '> Edit</button>' +
              '<button type="button" class="btn red-thunderbird open-remove-tutorial" data-id="'+row["id"]+'"> Delete <i class="fa fa-trash"></i></button>';
              return action_header;
            },
            searchable: false,
            orderable: false
          }
        ]
      });

      $(document).on("click", ".open-create-tutorial-modal", function(){
        $("#update-tutorial .tutorial-id").val('');
        $("#update-tutorial .tutorial-product-id").val($(this).data('product_id'));
        $("#update-tutorial .tutorial-title-th").val('');
        $("#update-tutorial .tutorial-title-en").val('');
        $("#update-tutorial .tutorial-link-url").val('');
        $("#update-tutorial .tutorial-sort").val('');
        $('#update-tutorial').modal('show');
      });

      $(document).on("click", ".open-update-tutorial-modal", function(){
        $("#update-tutorial .tutorial-id").val($(this).data('id'));
        $("#update-tutorial .tutorial-product-id").val($(this).data('product_id'));
        $("#update-tutorial .tutorial-title-th").val($(this).data('title_th'));
        $("#update-tutorial .tutorial-title-en").val($(this).data('title_en'));
        $("#update-tutorial .tutorial-link-url").val($(this).data('link_url'));
        $("#update-tutorial .tutorial-sort").val($(this).data('sort'));
        $('#update-tutorial').modal('show');
      });

      $(document).on("click", ".tutorial-update-button", function(){
        var title_th = $("#update-tutorial .tutorial-title-th").val();
        var title_en = $("#update-tutorial .tutorial-title-en").val();
        var link_url = $("#update-tutorial .tutorial-link-url").val();
        var sort = $("#update-tutorial .tutorial-sort").val();
        $("#update-tutorial").modal('hide');

        if (title_th == '' || title_en == '' || link_url == '' || sort == '') {
          $("#system-return-error .message").html("กรุณาระบุข้อมูลให้ครบ");
          $("#system-return-error").modal("toggle");
        }
        else{
          var formData = new FormData($('#tutorial-form')[0]);
          $.ajax({
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            url: "<?php echo base_url("administrator_area/product/edit_product_tutorial")?>",
            success: function (response) {
              try {
                if (response.code == "0x0000-00000") {
                  var table = $(".product-tutorial-list").DataTable();
                  table.columns().search("").draw();

                  toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                  toastr.success('Update Completed', 'Success');
                  $("#update-tutorial .tutorial-id").val('');
                  $("#update-tutorial .tutorial-product-id").val('');
                  $("#update-tutorial .tutorial-title-th").val('');
                  $("#update-tutorial .tutorial-title-en").val('');
                  $("#update-tutorial .tutorial-link-url").val('');
                  $("#update-tutorial .tutorial-sort").val('');
                } else {
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch (e) {
                $("#system-return-failed").modal("toggle");
              }
            },
            error: function () {
              $("#system-disconnected").modal("toggle");
            }
          });
        }
      });

      $(document).on("click", ".open-remove-tutorial", function(){
        $("#remove-tutorial .tutorial-id").val('');
        
        $("#remove-tutorial .tutorial-id").val($(this).data('id'));
        $('#remove-tutorial').modal('show');
      });

      $(document).on("click", ".tutorial-remove-button", function(){
        var id = $("#remove-tutorial .tutorial-id").val();
        $("#remove-tutorial").modal('hide');

        var formData = new FormData();
        formData.append('id', id);

        $.ajax({
          data: formData,
          type: "POST",
          contentType: false,
          processData: false,
          url: "<?php echo base_url("administrator_area/product/delete_product_tutorial")?>",
          success: function (response) {
            try {
              if (response.code == "0x0000-00000") {
                var table = $(".product-tutorial-list").DataTable();
                table.columns().search("").draw();

                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "positionClass": "toast-bottom-right",
                  "onclick": null,
                  "showDuration": "1000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                toastr.success('Update Completed', 'Success');
              } else {
                $("#system-return-error .message").html(response.message);
                $("#system-return-error").modal("toggle");
              }
            } catch (e) {
              $("#system-return-failed").modal("toggle");
            }
          },
          error: function () {
            $("#system-disconnected").modal("toggle");
          }
        });
      });

      //DataSheet 
      $(".product-datasheet-list").dataTable({
        processing: true,
        serverSide: true,
        pageLength: 25,
        searching: false,
        ajax:{
          url: "<?php echo base_url("administrator_area/product/datatables/index_product_datasheet")?>",
          type: "POST",
          data:{
            product_id: $(".product-datasheet-list").data("product_id")
          }
        },
        columns: [
          {
            data: 'sort'
          },
          {
            data: 'name'
          },
          {
            render: function(data, type, row, meta){
              var action_header = "";
              if(row['file_path'] != ''){
                action_header = "<a href='<?php echo base_url()?>"+row['file_path']+"' target='_blank' ><i class='fa fa-file'></i> file</a>";
              }
              return action_header;
            },
            searchable: false,
            orderable: false
          },
          {
            render: function(data, type, row, meta){
              var action_header = '<button type="button" class="btn yellow-crusta open-update-datasheet-modal" ' +
                'data-id="'+row["id"]+'" ' +
                'data-product_id="'+row["product_id"]+'" ' +
                'data-name="'+row["name"]+'" ' +
                'data-file_path="<?php echo base_url()?>'+row['file_path']+'" ' +
                'data-sort="'+row["sort"]+'" ' +
                '> Edit</button>' +
              '<button type="button" class="btn red-thunderbird open-remove-datasheet" data-id="'+row["id"]+'"> Delete <i class="fa fa-trash"></i></button>';
              return action_header;
            },
            searchable: false,
            orderable: false
          }
        ]
      });

      $(document).on("click", ".open-create-datasheet-modal", function(){
        $("#update-datasheet .datasheet-id").val('');
        $("#update-datasheet .datasheet-product-id").val($(this).data('product_id'));
        $("#update-datasheet .datasheet-name").val('');
        $("#update-datasheet .datasheet-sort").val('');
        $("#update-datasheet .datasheet-file").val('');
        $('#update-datasheet').modal('show');
      });

      $(document).on("click", ".open-update-datasheet-modal", function(){
        $("#update-datasheet .datasheet-id").val($(this).data('id'));
        $("#update-datasheet .datasheet-product-id").val($(this).data('product_id'));
        $("#update-datasheet .datasheet-name").val($(this).data('name'));
        $("#update-datasheet .datasheet-sort").val($(this).data('sort'));
        $("#update-datasheet .datasheet-file").val('');
        $('#update-datasheet').modal('show');
      });

      $(document).on("click", ".datasheet-update-button", function(){
        var name = $("#update-datasheet .datasheet-name").val();
        var sort = $("#update-datasheet .datasheet-sort").val();
        $("#update-datasheet").modal('hide');

        if (name == '' || sort == '') {
          $("#system-return-error .message").html("กรุณาระบุข้อมูลให้ครบ");
          $("#system-return-error").modal("toggle");
        }
        else{
          var formData = new FormData($('#datasheet-form')[0]);
          $.ajax({
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            url: "<?php echo base_url("administrator_area/product/edit_product_datasheet")?>",
            success: function (response) {
              try {
                if (response.code == "0x0000-00000") {
                  var table = $(".product-datasheet-list").DataTable();
                  table.columns().search("").draw();

                  toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                  toastr.success('Update Completed', 'Success');
                  $("#update-feature .feature-id").val('');
                  $("#update-feature .feature-product-id").val('');
                  $("#update-feature .feature-title-th").val('');
                  $("#update-feature .feature-title-en").val('');
                  $("#update-feature .feature-description-th").val('');
                  $("#update-feature .feature-description-en").val('');
                  $("#update-feature .feature-sort").val('');
                  $("#update-feature .feature-file").val('');
                  $("#feature-img").attr("src", '');
                } else {
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch (e) {
                $("#system-return-failed").modal("toggle");
              }
            },
            error: function () {
              $("#system-disconnected").modal("toggle");
            }
          });
        }
      });

      $(document).on("click", ".open-remove-datasheet", function(){
        $("#remove-datasheet .datasheet-id").val('');
        
        $("#remove-datasheet .datasheet-id").val($(this).data('id'));
        $('#remove-datasheet').modal('show');
      });

      $(document).on("click", ".datasheet-remove-button", function(){
        var id = $("#remove-datasheet .datasheet-id").val();
        $("#remove-datasheet").modal('hide');

        var formData = new FormData();
        formData.append('id', id);

        $.ajax({
          data: formData,
          type: "POST",
          contentType: false,
          processData: false,
          url: "<?php echo base_url("administrator_area/product/delete_product_datasheet")?>",
          success: function (response) {
            try {
              if (response.code == "0x0000-00000") {
                var table = $(".product-datasheet-list").DataTable();
                table.columns().search("").draw();

                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "positionClass": "toast-bottom-right",
                  "onclick": null,
                  "showDuration": "1000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                toastr.success('Update Completed', 'Success');
              } else {
                $("#system-return-error .message").html(response.message);
                $("#system-return-error").modal("toggle");
              }
            } catch (e) {
              $("#system-return-failed").modal("toggle");
            }
          },
          error: function () {
            $("#system-disconnected").modal("toggle");
          }
        });
      });

      //Price by Quantity
      $(".product-price-list").dataTable({
          processing: true,
          serverSide: true,
          pageLength: 25,
          searching: false,
          ajax:{
            url: "<?php echo base_url("administrator_area/product/datatables/index_product_price")?>",
            type: "POST",
            data:{
              product_id: $(".product-price-list").data("product_id")
            }
          },
          columns: [
            {
              data: 'minimum_qty'
            },
            {
              data: 'price'
            },
            {
              render: function(data, type, row, meta){
                var action_header = '<button type="button" class="btn yellow-crusta open-update-price-modal" ' +
                  'data-id="'+row["id"]+'" ' +
                  'data-product_id="'+row["product_id"]+'" ' +
                  'data-minimum_qty="'+row["minimum_qty"]+'" ' +
                  'data-price="'+row["price"]+'" ' +
                  '> Edit</button>' +
                '<button type="button" class="btn red-thunderbird open-remove-price" data-id="'+row["id"]+'"> Delete <i class="fa fa-trash"></i></button>';
                return action_header;
              },
              searchable: false,
              orderable: false
            }
          ]
      });

      $(document).on("click", ".create-price-qty-button",function(){
        var product_id = $(this).data("product_id");
        var minimum_qty = $("#price-quantity-form input[name='minimum_qty']").val();
        var price = $("#price-quantity-form input[name='price']").val();
              

        if (minimum_qty == '' || price == '') {
          $("#system-return-error .message").html("กรุณาระบุข้อมูลให้ครบ");
          $("#system-return-error").modal("toggle");
        }
        else{
          var formData = new FormData();
          formData.append('product_id', product_id);
          formData.append('minimum_qty', minimum_qty);
          formData.append('price', price);

          $.ajax({
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            url: "<?php echo base_url("administrator_area/product/edit_product_price")?>",
            success: function (response) {
              try {
                if (response.code == "0x0000-00000") {
                  var table = $(".product-price-list").DataTable();
                  table.columns().search("").draw();

                  toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                  toastr.success('Update Completed', 'Success.');
                  $("#price-quantity-form input[name='minimum_qty']").val("");
                  $("#price-quantity-form input[name='price']").val("");
                } else {
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch (e) {
                $("#system-return-failed").modal("toggle");
              }
            },
            error: function () {
              $("#system-disconnected").modal("toggle");
            }
          });
        }
      });

      $(document).on("click", ".open-update-price-modal", function(){
        $("#update-price .price-id").val('');
        $("#update-price .price-product-id").val('');
        $("#update-price .price-minimun-qty").val('');
        $("#update-price .price-price").val('');
        
        $("#update-price .price-id").val($(this).data('id'));
        $("#update-price .price-product-id").val($(this).data('product_id'));
        $("#update-price .price-minimun-qty").val($(this).data('minimum_qty'));
        $("#update-price .price-price").val($(this).data('price'));
        $('#update-price').modal('show');
      });

      $(document).on("click", ".price-update-button", function(){
        var id = $("#update-price .price-id").val();
        var product_id = $("#update-price .price-product-id").val();
        var minimum_qty = $("#update-price .price-minimun-qty").val();
        var price = $("#update-price .price-price").val();
        $("#update-price").modal('hide');

        
        if (minimum_qty == '' || price == '' || id == null|| product_id == null) {
          $("#system-return-error .message").html("กรุณาระบุข้อมูลให้ครบ");
          $("#system-return-error").modal("toggle");
        }
        else{
          var formData = new FormData();
          formData.append('id', id);
          formData.append('product_id', product_id);
          formData.append('minimum_qty', minimum_qty);
          formData.append('price', price);

          $.ajax({
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            url: "<?php echo base_url("administrator_area/product/edit_product_price")?>",
            success: function (response) {
              try {
                if (response.code == "0x0000-00000") {
                  var table = $(".product-price-list").DataTable();
                  table.columns().search("").draw();

                  toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                  toastr.success('Update Completed', 'Success');
                  $("#update-price .price-id").val('');
                  $("#update-price .price-product-id").val('');
                  $("#update-price .price-minimun-qty").val('');
                  $("#update-price .price-price").val('');
                } else {
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch (e) {
                $("#system-return-failed").modal("toggle");
              }
            },
            error: function () {
              $("#system-disconnected").modal("toggle");
            }
          });
        }
      });

      $(document).on("click", ".open-remove-price", function(){
        $("#remove-price .price-id").val('');
        
        $("#remove-price .price-id").val($(this).data('id'));
        $('#remove-price').modal('show');
      });

      $(document).on("click", ".price-remove-button", function(){
        var id = $("#remove-price .price-id").val();
        $("#remove-price").modal('hide');

        var formData = new FormData();
        formData.append('id', id);

        $.ajax({
          data: formData,
          type: "POST",
          contentType: false,
          processData: false,
          url: "<?php echo base_url("administrator_area/product/delete_product_price")?>",
          success: function (response) {
            try {
              if (response.code == "0x0000-00000") {
                var table = $(".product-price-list").DataTable();
                table.columns().search("").draw();

                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "positionClass": "toast-bottom-right",
                  "onclick": null,
                  "showDuration": "1000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                toastr.success('Update Completed', 'Success');
              } else {
                $("#system-return-error .message").html(response.message);
                $("#system-return-error").modal("toggle");
              }
            } catch (e) {
              $("#system-return-failed").modal("toggle");
            }
          },
          error: function () {
            $("#system-disconnected").modal("toggle");
          }
        });
      });

      //Category
      $(".product-category-list").dataTable({
          processing: true,
          serverSide: true,
          pageLength: 25,
          searching: false,
          ajax:{
            url: "<?php echo base_url("administrator_area/product/datatables/index_product_category")?>",
            type: "POST",
            data:{
              product_id: $(".product-category-list").data("product_id")
            }
          },
          columns: [
            {
              data: 'product_category_name'
            },
            {
              data: 'product_sub_category_name'
            },
            {
              render: function(data, type, row, meta){
                var action_header = '<button type="button" class="btn yellow-crusta open-update-category-modal" '+
                  'data-id="'+row["id"]+'" ' +
                  'data-product_id="'+row["product_id"]+'" ' +
                  'data-product_category_id="'+row["product_category_id"]+'" ' +
                  'data-product_category_name="'+row["product_category_name"]+'" ' +
                  'data-product_sub_category_id="'+row["product_sub_category_id"]+'" ' +
                  'data-product_sub_category_name="'+row["product_sub_category_name"]+'" ' +
                '> Edit</button>' +
                '<button type="button" class="btn red-thunderbird open-remove-category" data-id="'+row["id"]+'"> Delete <i class="fa fa-trash"></i></button>';
                return action_header;
              },
              searchable: false,
              orderable: false
            }
          ]
      });

      $(document).on("click", ".create-category-button",function(){
        var product_id = $(this).data("product_id");
        var product_category_id = $("#category-form select[name='product_category_id']").val();
        var product_sub_category_id = $("#category-form select[name='product_sub_category_id']").val();
              
        if ((product_category_id == null )) {
          $("#system-return-error .message").html("Invalid Data");
          $("#system-return-error").modal("toggle");
        }
        else{
          var formData = new FormData();
          formData.append('product_id', product_id);
          formData.append('product_category_id', product_category_id);
          formData.append('product_sub_category_id', product_sub_category_id);

          $.ajax({
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            url: "<?php echo base_url("administrator_area/product/edit_product_category")?>",
            success: function (response) {
              try {
                if (response.code == "0x0000-00000") {
                  var table = $(".product-category-list").DataTable();
                  table.columns().search("").draw();

                  toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                  toastr.success('Update Completed', 'Success.');
                  $("#category-form select[name='product_category_id']").select2("val", "");
                  $("#category-form select[name='product_sub_category_id']").select2("val", "");
                } else {
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch (e) {
                $("#system-return-failed").modal("toggle");
              }
            },
            error: function () {
              $("#system-disconnected").modal("toggle");
            }
          });
        }
      });

      $(document).on("click", ".open-update-category-modal", function(){
        $("#update-category .category-id").val('');
        $("#update-category .category-product-id").val('');
        $("#update-category .category-product-category-id").html('').trigger('change');
        $("#update-category .category-product-sub-category-id").html('').trigger('change');
        
        $("#update-category .category-id").val($(this).data('id'));
        $("#update-category .category-product-id").val($(this).data('product_id'));
        var product_category_id = $(this).data("product_category_id");
        var product_category_name = $(this).data("product_category_name");
        var option = new Option(product_category_name, product_category_id, false, false);
        $("#update-category .category-product-category-id").append(option).trigger('change');
        var product_sub_category_id = $(this).data("product_sub_category_id");
        var product_sub_category_name = $(this).data("product_sub_category_name");
        if(product_sub_category_id != null){
          var option = new Option(product_sub_category_name, product_sub_category_id, false, false);
          $("#update-category .category-product-sub-category-id").append(option).trigger('change');
        }     
        
        $('#update-category').modal('show');
      });

      $(document).on("click", ".category-update-button", function(){
        var id = $("#update-category .category-id").val();
        var product_id = $("#update-category .category-product-id").val();
        var product_category_id = $("#update-category .category-product-category-id").val();
        var product_sub_category_id = $("#update-category .category-product-sub-category-id").val();

        $("#update-category").modal('hide');

        if ((product_category_id == null )) {
          $("#system-return-error .message").html("Invalid Data");
          $("#system-return-error").modal("toggle");
        }
        else{
          var formData = new FormData();
          formData.append('id', id);
          formData.append('product_id', product_id);
          formData.append('product_category_id', product_category_id);
          formData.append('product_sub_category_id', product_sub_category_id);

          $.ajax({
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            url: "<?php echo base_url("administrator_area/product/edit_product_category")?>",
            success: function (response) {
              try {
                if (response.code == "0x0000-00000") {
                  var table = $(".product-category-list").DataTable();
                  table.columns().search("").draw();

                  toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                  toastr.success('Update Completed', 'Success');
                  $("#update-category .category-id").val('');
                  $("#update-category .category-product-id").val('');
                  $("#update-category .category-product-category-id").html('').trigger('change');
                  $("#update-category .category-product-sub-category-id").html('').trigger('change');
                } else {
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch (e) {
                $("#system-return-failed").modal("toggle");
              }
            },
            error: function () {
              $("#system-disconnected").modal("toggle");
            }
          });
        }
      });

      $(document).on("click", ".open-remove-category", function(){
        $("#remove-category .category-id").val('');
        
        $("#remove-category .category-id").val($(this).data('id'));
        $('#remove-category').modal('show');
      });

      $(document).on("click", ".category-remove-button", function(){
        var id = $("#remove-category .category-id").val();
        $("#remove-category").modal('hide');

        var formData = new FormData();
        formData.append('id', id);

        $.ajax({
          data: formData,
          type: "POST",
          contentType: false,
          processData: false,
          url: "<?php echo base_url("administrator_area/product/delete_product_category")?>",
          success: function (response) {
            try {
              if (response.code == "0x0000-00000") {
                var table = $(".product-category-list").DataTable();
                table.columns().search("").draw();

                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "positionClass": "toast-bottom-right",
                  "onclick": null,
                  "showDuration": "1000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                toastr.success('Update Completed', 'Success');
              } else {
                $("#system-return-error .message").html(response.message);
                $("#system-return-error").modal("toggle");
              }
            } catch (e) {
              $("#system-return-failed").modal("toggle");
            }
          },
          error: function () {
            $("#system-disconnected").modal("toggle");
          }
        });
      });

      //Menu
      $(".menu-category-list").dataTable({
          processing: true,
          serverSide: true,
          pageLength: 25,
          searching: false,
          ajax:{
            url: "<?php echo base_url("administrator_area/product/datatables/index_menu_category")?>",
            type: "POST",
            data:{
              product_id: $(".menu-category-list").data("product_id")
            }
          },
          columns: [
            {
              data: 'menu_category_name'
            },
            {
              data: 'submenu_category_name'
            },
            {
              data: 'child_submenu_category_name'
            },
            {
              render: function(data, type, row, meta){
                var action_header = '<button type="button" class="btn yellow-crusta open-update-menu-modal" ' +
                  'data-id="'+row["id"]+'" ' +
                  'data-product_id="'+row["product_id"]+'" ' +
                  'data-menu_category_id="'+row["menu_category_id"]+'" ' +
                  'data-menu_category_name="'+row["menu_category_name"]+'" ' +
                  'data-submenu_category_id="'+row["submenu_category_id"]+'" ' +
                  'data-submenu_category_name="'+row["submenu_category_name"]+'" ' +
                  'data-child_submenu_category_id="'+row["child_submenu_category_id"]+'" ' +
                  'data-child_submenu_category_name="'+row["child_submenu_category_name"]+'" ' +
                  '> Edit</button>' +
                '<button type="button" class="btn red-thunderbird open-remove-menu" data-id="'+row["id"]+'"> Delete <i class="fa fa-trash"></i></button>';
                return action_header;
              },
              searchable: false,
              orderable: false
            }
          ]
      });

      $(document).on("click", ".create-menu-button",function(){
        var product_id = $(this).data("product_id");
        var menu_category_id = $("#menu-form select[name='menu_category_id']").val();
        var submenu_category_id = $("#menu-form select[name='submenu_category_id']").val();
        var child_submenu_category_id = $("#menu-form select[name='child_submenu_category_id']").val();
              
        if ((menu_category_id == null )|| (child_submenu_category_id != null && submenu_category_id ==null)) {
          $("#system-return-error .message").html("Invalid Data");
          $("#system-return-error").modal("toggle");
        }
        else{
          var formData = new FormData();
          formData.append('product_id', product_id);
          formData.append('menu_category_id', menu_category_id);
          formData.append('submenu_category_id', submenu_category_id);
          formData.append('child_submenu_category_id', child_submenu_category_id);

          $.ajax({
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            url: "<?php echo base_url("administrator_area/product/edit_menu_category")?>",
            success: function (response) {
              try {
                if (response.code == "0x0000-00000") {
                  var table = $(".menu-category-list").DataTable();
                  table.columns().search("").draw();

                  toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                  toastr.success('Update Completed', 'Success.');
                  $("#menu-form select[name='menu_category_id']").select2("val", "");
                  $("#menu-form select[name='submenu_category_id']").select2("val", "");
                  $("#menu-form select[name='child_submenu_category_id']").select2("val", "");
                } else {
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch (e) {
                $("#system-return-failed").modal("toggle");
              }
            },
            error: function () {
              $("#system-disconnected").modal("toggle");
            }
          });
        }
      });

      $(document).on("click", ".open-update-menu-modal", function(){
        $("#update-menu .menu-id").val('');
        $("#update-menu .menu-product-id").val('');
        $("#update-menu .menu-menu-category-id").html('').trigger('change');
        $("#update-menu .menu-submenu-category-id").html('').trigger('change');
        $("#update-menu .menu-child-submenu-category-id").html('').trigger('change');
        
        $("#update-menu .menu-id").val($(this).data('id'));
        $("#update-menu .menu-product-id").val($(this).data('product_id'));
        var menu_category_id = $(this).data("menu_category_id");
        var menu_category_name = $(this).data("menu_category_name");
        var option = new Option(menu_category_name, menu_category_id, false, false);
        $("#update-menu .menu-menu-category-id").append(option).trigger('change');
        var submenu_category_id = $(this).data("submenu_category_id");
        var submenu_category_name = $(this).data("submenu_category_name");
        if(submenu_category_id != null){
          var option = new Option(submenu_category_name, submenu_category_id, false, false);
          $("#update-menu .menu-submenu-category-id").append(option).trigger('change');
        }        
        var child_submenu_category_id = $(this).data("child_submenu_category_id");
        var child_submenu_category_name = $(this).data("child_submenu_category_name");
        if (child_submenu_category_id) {
          var option = new Option(child_submenu_category_name, child_submenu_category_id, false, false);
          $("#update-menu .menu-child-submenu-category-id").append(option).trigger('change');
        }
        
        $('#update-menu').modal('show');
      });

      $(document).on("click", ".menu-update-button", function(){
        var id = $("#update-menu .menu-id").val();
        var product_id = $("#update-menu .menu-product-id").val();
        var menu_category_id = $("#update-menu .menu-menu-category-id").val();
        var submenu_category_id = $("#update-menu .menu-submenu-category-id").val();
        var child_submenu_category_id = $("#update-menu .menu-child-submenu-category-id").val();

        $("#update-menu").modal('hide');

        if ((menu_category_id == null )|| (child_submenu_category_id != null && submenu_category_id ==null)) {
          $("#system-return-error .message").html("Invalid Data");
          $("#system-return-error").modal("toggle");
        }
        else{
          var formData = new FormData();
          formData.append('id', id);
          formData.append('product_id', product_id);
          formData.append('menu_category_id', menu_category_id);
          formData.append('submenu_category_id', submenu_category_id);
          formData.append('child_submenu_category_id', child_submenu_category_id);

          $.ajax({
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            url: "<?php echo base_url("administrator_area/product/edit_menu_category")?>",
            success: function (response) {
              try {
                if (response.code == "0x0000-00000") {
                  var table = $(".menu-category-list").DataTable();
                  table.columns().search("").draw();

                  toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                  toastr.success('Update Completed', 'Success');
                  $("#update-menu .menu-id").val('');
                  $("#update-menu .menu-product-id").val('');
                  $("#update-menu .menu-menu-category-id").html('').trigger('change');
                  $("#update-menu .menu-submenu-category-id").html('').trigger('change');
                  $("#update-menu .menu-child-submenu-category-id").html('').trigger('change');
                } else {
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch (e) {
                $("#system-return-failed").modal("toggle");
              }
            },
            error: function () {
              $("#system-disconnected").modal("toggle");
            }
          });
        }
      });

      $(document).on("click", ".open-remove-menu", function(){
        $("#remove-menu .menu-id").val('');
        
        $("#remove-menu .menu-id").val($(this).data('id'));
        $('#remove-menu').modal('show');
      });

      $(document).on("click", ".menu-remove-button", function(){
        var id = $("#remove-menu .menu-id").val();
        $("#remove-menu").modal('hide');

        var formData = new FormData();
        formData.append('id', id);

        $.ajax({
          data: formData,
          type: "POST",
          contentType: false,
          processData: false,
          url: "<?php echo base_url("administrator_area/product/delete_menu_category")?>",
          success: function (response) {
            try {
              if (response.code == "0x0000-00000") {
                var table = $(".menu-category-list").DataTable();
                table.columns().search("").draw();

                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "positionClass": "toast-bottom-right",
                  "onclick": null,
                  "showDuration": "1000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                toastr.success('Update Completed', 'Success');
              } else {
                $("#system-return-error .message").html(response.message);
                $("#system-return-error").modal("toggle");
              }
            } catch (e) {
              $("#system-return-failed").modal("toggle");
            }
          },
          error: function () {
            $("#system-disconnected").modal("toggle");
          }
        });
      });


      //Product Relate
      $(".product-relate-list").dataTable({
          processing: true,
          serverSide: true,
          pageLength: 25,
          searching: false,
          ajax:{
            url: "<?php echo base_url("administrator_area/product/datatables/index_product_relate")?>",
            type: "POST",
            data:{
              product_id: $(".product-relate-list").data("product_id")
            }
          },
          columns: [
            {
              data: 'product_name'
            },
            {
              data: 'product_model_name'
            },
            {
              data: 'product_code'
            },
            {
              render: function(data, type, row, meta){
                var action_header = '<button type="button" class="btn red-thunderbird open-remove-product-relate" data-id="'+row["id"]+'"> Delete <i class="fa fa-trash"></i></button>';
                return action_header;
              },
              searchable: false,
              orderable: false
            }
          ]
      });

      $(document).on("click", ".create-product-relate-button",function(){
        var product_id = $(this).data("product_id");
        var relate_product_id = $("select[name='relate_product_id']").val();
              
        if ((relate_product_id == null )) {
          $("#system-return-error .message").html("Invalid Data");
          $("#system-return-error").modal("toggle");
        }
        else{
          var formData = new FormData();
          formData.append('product_id', product_id);
          formData.append('relate_product_id', relate_product_id);

          $.ajax({
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            url: "<?php echo base_url("administrator_area/product/edit_product_relate")?>",
            success: function (response) {
              try {
                if (response.code == "0x0000-00000") {
                  var table = $(".product-relate-list").DataTable();
                  table.columns().search("").draw();

                  toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                  toastr.success('Update Completed', 'Success.');
                  $("select[name='relate_product_id']").select2("val", "");
                } else {
                  $("#system-return-error .message").html(response.message);
                  $("#system-return-error").modal("toggle");
                }
              } catch (e) {
                $("#system-return-failed").modal("toggle");
              }
            },
            error: function () {
              $("#system-disconnected").modal("toggle");
            }
          });
        }
      });

      $(document).on("click", ".open-remove-product-relate", function(){
        $("#remove-product-relate .product-relate-id").val('');
        
        $("#remove-product-relate .product-relate-id").val($(this).data('id'));
        $('#remove-product-relate').modal('show');
      });

      $(document).on("click", ".product-relate-remove-button", function(){
        var id = $("#remove-product-relate .product-relate-id").val();
        $("#remove-product-relate").modal('hide');

        var formData = new FormData();
        formData.append('id', id);

        $.ajax({
          data: formData,
          type: "POST",
          contentType: false,
          processData: false,
          url: "<?php echo base_url("administrator_area/product/delete_product_relate")?>",
          success: function (response) {
            try {
              if (response.code == "0x0000-00000") {
                var table = $(".product-relate-list").DataTable();
                table.columns().search("").draw();

                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "positionClass": "toast-bottom-right",
                  "onclick": null,
                  "showDuration": "1000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                toastr.success('Update Completed', 'Success');
              } else {
                $("#system-return-error .message").html(response.message);
                $("#system-return-error").modal("toggle");
              }
            } catch (e) {
              $("#system-return-failed").modal("toggle");
            }
          },
          error: function () {
            $("#system-disconnected").modal("toggle");
          }
        });
      });


    });
  </script>

  <!-- INCLUDE RAW SCRIPT AREA -->
  <?php $this->load->view("administrator_area/__scripts/Javascript_Modal",array());?>
  <?php $this->load->view("administrator_area/__scripts/Javascript_Select2",array());?>
</html>

<!-- ------------------------------------------------------------------ -->
<!-- MODAL IS BEGIN                                                     -->
<!-- ------------------------------------------------------------------ -->
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationCancel");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationDelete");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationRefresh");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationSave");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationSaveMultiform");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_PermissionDenied");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemDisconnected");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnError");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnFailed");?>

<div class="modal fade" id="update-feature" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-yellow-crusta">
            <i class="fa fa-pencil font-yellow-crusta" data-dismiss="modal"></i>
            <span class="caption-subject bold uppercase"> Add/Edit Product Feature</span>
          </div>
        </div>
        <div class="portlet-body form">
          <form class="form-horizontal" role="form" id="feature-form" method="post" enctype="multipart/form-data">
            <input type="hidden" class="feature-id" name="id" value="">
            <input type="hidden" class="feature-product-id" name="product_id" value="">
            <div class="form-group">
              <div class="col-md-4 control-label">
                Title (TH)<span class="required"> *</span>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control feature-title-th" name="title_th" value="" required>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4 control-label">
                Title (EN)<span class="required"> *</span>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control feature-title-en" name="title_en" value="" required>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4 control-label">
                Description (TH)<span class="required"> *</span>
              </div>
              <div class="col-md-8">
                <textarea class="form-control feature-description-th" name="description_th"></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4 control-label">
                Description (EN)<span class="required"> *</span>
              </div>
              <div class="col-md-8">
                <textarea class="form-control feature-description-en" name="description_en" ></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4 control-label">
                Sort<span class="required"> *</span>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control number feature-sort" name="sort" required>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4 control-label">
                Image
              </div>
              <div class="col-md-8">
                <input type="file" class="form-control feature-file" name="feature_file" accept="image/*">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4 control-label">
              </div>
              <div class="col-md-4">
                <img src="" style="max-width: 100%" id="feature-img">
              </div>
            </div>
            <div class="form-group">
                <div class="col-md-4 control-label"></div>
                <div class="col-md-8">
                    <button type="button" class="btn green-jungle feature-update-button">Save</button>
                    <button type="button" class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="remove-feature" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-red-thunderbird">
            <i class="fa fa-close font-red-thunderbird" data-dismiss="modal"></i>
            <span class="caption-feature bold uppercase"> Delete Feature</span>
          </div>
        </div>
        <div class="portlet-body form">
          <div class="form-horizontal" role="form">
            <input type="hidden" class="feature-id">
            <div class="form-group">
              <div class="col-md-12">Are you sure to delete?
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3 control-label"></div>
              <div class="col-md-9">
                  <button class="btn green-jungle feature-remove-button">Save</button>
                  <button class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="update-tutorial" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-yellow-crusta">
            <i class="fa fa-pencil font-yellow-crusta" data-dismiss="modal"></i>
            <span class="caption-subject bold uppercase"> Add/Edit Product Tutorial</span>
          </div>
        </div>
        <div class="portlet-body form">
          <form class="form-horizontal" role="form" id="tutorial-form" method="post" enctype="multipart/form-data">
            <input type="hidden" class="tutorial-id" name="id" value="">
            <input type="hidden" class="tutorial-product-id" name="product_id" value="">
            <div class="form-group">
              <div class="col-md-4 control-label">
                Title (TH)<span class="required"> *</span>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control tutorial-title-th" name="title_th" value="" required>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4 control-label">
                Title (EN)<span class="required"> *</span>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control tutorial-title-en" name="title_en" value="" required>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4 control-label">
                Link Url<span class="required"> *</span>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control tutorial-link-url" name="link_url" value="" required>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4 control-label">
                Sort<span class="required"> *</span>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control number tutorial-sort" name="sort" required>
              </div>
            </div>
            <div class="form-group">
                <div class="col-md-4 control-label"></div>
                <div class="col-md-8">
                    <button type="button" class="btn green-jungle tutorial-update-button">Save</button>
                    <button type="button" class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="remove-tutorial" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-red-thunderbird">
            <i class="fa fa-close font-red-thunderbird" data-dismiss="modal"></i>
            <span class="caption-tutorial bold uppercase"> Delete tutorial</span>
          </div>
        </div>
        <div class="portlet-body form">
          <div class="form-horizontal" role="form">
            <input type="hidden" class="tutorial-id">
            <div class="form-group">
              <div class="col-md-12">Are you sure to delete?
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3 control-label"></div>
              <div class="col-md-9">
                  <button class="btn green-jungle tutorial-remove-button">Save</button>
                  <button class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="update-datasheet" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-yellow-crusta">
            <i class="fa fa-pencil font-yellow-crusta" data-dismiss="modal"></i>
            <span class="caption-subject bold uppercase"> Add/Edit Product DataSheet</span>
          </div>
        </div>
        <div class="portlet-body form">
          <form class="form-horizontal" role="form" id="datasheet-form" method="post" enctype="multipart/form-data">
            <input type="hidden" class="datasheet-id" name="id" value="">
            <input type="hidden" class="datasheet-product-id" name="product_id" value="">
            <div class="form-group">
              <div class="col-md-3 control-label">
                Title (TH)<span class="required"> *</span>
              </div>
              <div class="col-md-9">
                <input type="text" class="form-control datasheet-name" name="name" value="" required>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3 control-label">
                Sort<span class="required"> *</span>
              </div>
              <div class="col-md-9">
                <input type="text" class="form-control number datasheet-sort" name="sort" required>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3 control-label">
                File<span class="required"> *</span>
              </div>
              <div class="col-md-9">
                <input type="file" class="form-control datasheet-file" name="datasheet_file" >
              </div>
            </div>
            <div class="form-group">
                <div class="col-md-3 control-label"></div>
                <div class="col-md-9">
                    <button type="button" class="btn green-jungle datasheet-update-button">Save</button>
                    <button type="button" class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="remove-datasheet" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-red-thunderbird">
            <i class="fa fa-close font-red-thunderbird" data-dismiss="modal"></i>
            <span class="caption-datasheet bold uppercase"> Delete DataSheet</span>
          </div>
        </div>
        <div class="portlet-body form">
          <div class="form-horizontal" role="form">
            <input type="hidden" class="datasheet-id">
            <div class="form-group">
              <div class="col-md-12">Are you sure to delete?
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3 control-label"></div>
              <div class="col-md-9">
                  <button class="btn green-jungle datasheet-remove-button">Save</button>
                  <button class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="update-price" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-yellow-crusta">
            <i class="fa fa-pencil font-yellow-crusta" data-dismiss="modal"></i>
            <span class="caption-subject bold uppercase"> Edit Price by Quantity</span>
          </div>
        </div>
        <div class="portlet-body form">
          <div class="form-horizontal" role="form">
            <input type="hidden" class="price-id">
            <input type="hidden" class="price-product-id">
            <div class="form-group">
                <div class="col-md-4 control-label">Minimun Quantity<span class="required"> *</span></div>
                <div class="col-md-5">
                    <input type="text" class="form-control number price-minimun-qty">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-4 control-label">Price<span class="required"> *</span></div>
                <div class="col-md-5">
                    <input type="text" class="form-control pos-decimal price-price">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-4 control-label"></div>
                <div class="col-md-5">
                    <button class="btn green-jungle price-update-button">Save</button>
                    <button class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="remove-price" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-red-thunderbird">
            <i class="fa fa-close font-red-thunderbird" data-dismiss="modal"></i>
            <span class="caption-price bold uppercase"> Delete Price</span>
          </div>
        </div>
        <div class="portlet-body form">
          <div class="form-horizontal" role="form">
            <input type="hidden" class="price-id">
            <div class="form-group">
              <div class="col-md-12">Are you sure to delete?
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3 control-label"></div>
              <div class="col-md-9">
                  <button class="btn green-jungle price-remove-button">Save</button>
                  <button class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="update-category" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-yellow-crusta">
            <i class="fa fa-pencil font-yellow-crusta" data-dismiss="modal"></i>
            <span class="caption-subject bold uppercase"> Edit Category</span>
          </div>
        </div>
        <div class="portlet-body form">
          <div class="form-horizontal" role="form">
            <input type="hidden" class="category-id">
            <input type="hidden" class="category-product-id">
            <div class="form-group">
              <div class="col-md-4 control-label">Category<span class="required"> *</span></div>
              <div class="col-md-6">
                <select class="form-control select2 category-product-category-id" name="product_category_id">
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4 control-label">Sub Category</div>
              <div class="col-md-6">
                <select class="form-control select2 category-product-sub-category-id" name="product_sub_category_id">
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4 control-label"></div>
              <div class="col-md-6">
                <button class="btn green-jungle category-update-button">Save</button>
                <button class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="remove-category" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-red-thunderbird">
            <i class="fa fa-close font-red-thunderbird" data-dismiss="modal"></i>
            <span class="caption-category bold uppercase"> Delete Category</span>
          </div>
        </div>
        <div class="portlet-body form">
          <div class="form-horizontal" role="form">
            <input type="hidden" class="category-id">
            <div class="form-group">
              <div class="col-md-12">Are you sure to delete?
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3 control-label"></div>
              <div class="col-md-9">
                  <button class="btn green-jungle category-remove-button">Save</button>
                  <button class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="update-menu" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-yellow-crusta">
            <i class="fa fa-pencil font-yellow-crusta" data-dismiss="modal"></i>
            <span class="caption-subject bold uppercase"> Edit Menu</span>
          </div>
        </div>
        <div class="portlet-body form">
          <div class="form-horizontal" role="form">
            <input type="hidden" class="menu-id">
            <input type="hidden" class="menu-product-id">
            <div class="form-group">
              <div class="col-md-4 control-label">Menu Category<span class="required"> *</span></div>
              <div class="col-md-6">
                <select class="form-control select2 menu-menu-category-id" name="menu_category_id">
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4 control-label">Sub Category</div>
              <div class="col-md-6">
                <select class="form-control select2 menu-submenu-category-id" name="submenu_category_id">
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4 control-label">Child Category</div>
              <div class="col-md-6">
                <select class="form-control select2 menu-child-submenu-category-id" name="child_submenu_category_id">
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-4 control-label"></div>
              <div class="col-md-6">
                <button class="btn green-jungle menu-update-button">Save</button>
                <button class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="remove-menu" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-red-thunderbird">
            <i class="fa fa-close font-red-thunderbird" data-dismiss="modal"></i>
            <span class="caption-menu bold uppercase"> Delete Menu</span>
          </div>
        </div>
        <div class="portlet-body form">
          <div class="form-horizontal" role="form">
            <input type="hidden" class="menu-id">
            <div class="form-group">
              <div class="col-md-12">Are you sure to delete?
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3 control-label"></div>
              <div class="col-md-9">
                  <button class="btn green-jungle menu-remove-button">Save</button>
                  <button class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="remove-product-relate" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-red-thunderbird">
            <i class="fa fa-close font-red-thunderbird" data-dismiss="modal"></i>
            <span class="caption-product-relate bold uppercase"> Delete Product Relate</span>
          </div>
        </div>
        <div class="portlet-body form">
          <div class="form-horizontal" role="form">
            <input type="hidden" class="product-relate-id">
            <div class="form-group">
              <div class="col-md-12">Are you sure to delete?
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3 control-label"></div>
              <div class="col-md-9">
                  <button class="btn green-jungle product-relate-remove-button">Save</button>
                  <button class="btn red-thunderbird" data-dismiss="modal">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
