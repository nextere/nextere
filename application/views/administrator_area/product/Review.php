<!-- ------------------------------------------------------------------ -->
<!-- PERMISSION CHECK IS BEGIN                                          -->
<!-- ------------------------------------------------------------------ -->
<?php
$codeigniter_instance =& get_instance();

$configurations = array(
  "PAGE_TITLE" => "Administrator System",
  "PAGE_HEADER" => array(
   "MAIN_TITLE" => "Review Management",
   "SUB_TITLE" => ""
  ),
  "PORTLET_HEADER" => array(
    "ICON" => "fa fa-bars",
    "TITLE" => "Review Detail"
  )
);
?>
<!-- ------------------------------------------------------------------ -->
<!-- HEADER IS BEGIN                                                    -->
<!-- ------------------------------------------------------------------ -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
  <title><?php echo $this->config->item('WEBSITE_NAME');?> | Login System</title>
  <link href="<?php echo faviconDirectory($this->config->item('WEBSITE_FAVICON'));?>" rel="shortcut icon" />

  <!-- META TAG AREA -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />

  <!-- MANDATORY STYLE AREA -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
    type="text/css" />
  <link href="<?php echo assetsDirectory("global/plugins/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet"
    type="text/css" />
  <link href="<?php echo assetsDirectory("global/plugins/simple-line-icons/simple-line-icons.min.css");?>"
    rel="stylesheet" type="text/css" />
  <link href="<?php echo assetsDirectory("global/plugins/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet"
    type="text/css" />
  <link href="<?php echo assetsDirectory("global/plugins/bootstrap-switch/css/bootstrap-switch.min.css");?>"
    rel="stylesheet" type="text/css" />

  <!-- PLUGINS AREA -->
  <link href="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.css");?>" rel="stylesheet"
    type="text/css" />
  <link href="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css");?>"
    rel="stylesheet" type="text/css" />
  <link href="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.css");?>"
    rel="stylesheet" type="text/css" />
  <link href="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css");?>"
    rel="stylesheet" type="text/css" />
  <link href="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css");?>"
    rel="stylesheet" type="text/css" />
  <link
    href="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");?>"
    rel="stylesheet" type="text/css" />
  <link href="<?php echo assetsDirectory("global/plugins/clockface/css/clockface.css");?>" rel="stylesheet"
    type="text/css" />
  <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2.min.css");?>" rel="stylesheet"
    type="text/css" />
  <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2-bootstrap.min.css");?>" rel="stylesheet"
    type="text/css" />
  <link href="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.css");?>"
    rel="stylesheet" type="text/css" />
  <link href="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.css");?>" rel="stylesheet">
  <link href="<?php echo assetsDirectory("global/plugins/jstree/dist/themes/default/style.css");?>" rel="stylesheet">
  <link href="<?php echo assetsDirectory("global/plugins/dropzone/basic.min.css");?>" rel="stylesheet">
  <link href="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.css");?>" rel="stylesheet">
  <link href="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.css");?>" rel="stylesheet"
    type="text/css" />

  <!-- THEME STYLE AREA -->
  <link href="<?php echo assetsDirectory("global/css/components-md.min.css");?>" rel="stylesheet" id="style_components"
    type="text/css" />
  <link href="<?php echo assetsDirectory("global/css/plugins-md.min.css");?>" rel="stylesheet" type="text/css" />

  <!-- LAYOUT AREA -->
  <link href="<?php echo assetsDirectory("layouts/layout/css/layout.min.css");?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo assetsDirectory("layouts/layout/css/themes/light.min.css");?>" rel="stylesheet" type="text/css"
    id="style_color" />
  <link href="<?php echo assetsDirectory("layouts/layout/css/custom.min.css");?>" rel="stylesheet" type="text/css" />

  <!-- CUSTOM AREA -->
  <style>
    .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice {
      color: #fff;
      background: #53c6d2;
      border: 1px solid #53c6d2;
      margin: 5px 0 0 6px;
      padding: 0 35px 0 6px;
    }

    .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice__remove {
      margin-right: 30px;
      color: #e7505a;
    }

    .product-image {
      height: 150px;
      margin-top: 10px;
    }

    body {
      padding-top: 70px;
    }

    .btn-grey {
      background-color: #D8D8D8;
      color: #FFF;
    }

    .rating-block {
      background-color: #FAFAFA;
      border: 1px solid #EFEFEF;
      padding: 15px 15px 20px 15px;
      border-radius: 3px;
    }

    .bold {
      font-weight: 700;
    }

    .padding-bottom-7 {
      padding-bottom: 7px;
    }

    .review-block {
      background-color: #FAFAFA;
      border: 1px solid #EFEFEF;
      padding: 15px;
      border-radius: 3px;
      margin-bottom: 15px;
    }

    .review-block-name {
      font-size: 12px;
      margin: 10px 0;
    }

    .review-block-date {
      font-size: 12px;
    }

    .review-block-rate {
      font-size: 13px;
      margin-bottom: 15px;
    }

    .review-block-title {
      font-size: 15px;
      font-weight: 700;
      margin-bottom: 10px;
    }

    .review-block-description {
      font-size: 13px;
    }
    .switch {
					position: relative;
					display: block;
					vertical-align: top;
					width: 100px;
					height: 30px;
					padding: 3px;
					margin: 0 10px 10px 0;
					background: linear-gradient(to bottom, #eeeeee, #FFFFFF 25px);
					background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF 25px);
					border-radius: 18px;
					box-shadow: inset 0 -1px white, inset 0 1px 1px rgba(0, 0, 0, 0.05);
					cursor: pointer;
					box-sizing:content-box;
				}
				.switch-input {
					position: absolute;
					top: 0;
					left: 0;
					opacity: 0;
					box-sizing:content-box;
				}
				.switch-label {
					position: relative;
					display: block;
					height: inherit;
					font-size: 10px;
					text-transform: uppercase;
					background: #eceeef;
					border-radius: inherit;
					box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.12), inset 0 0 2px rgba(0, 0, 0, 0.15);
					box-sizing:content-box;
				}
				.switch-label:before, .switch-label:after {
					position: absolute;
					top: 50%;
					margin-top: -.5em;
					line-height: 1;
					-webkit-transition: inherit;
					-moz-transition: inherit;
					-o-transition: inherit;
					transition: inherit;
					box-sizing:content-box;
				}
				.switch-label:before {
					content: attr(data-off);
					right: 11px;
					color: #aaaaaa;
					text-shadow: 0 1px rgba(255, 255, 255, 0.5);
				}
				.switch-label:after {
					content: attr(data-on);
					left: 11px;
					color: #FFFFFF;
					text-shadow: 0 1px rgba(0, 0, 0, 0.2);
					opacity: 0;
				}
				.switch-input:checked ~ .switch-label {
					background: #E1B42B;
					box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.15), inset 0 0 3px rgba(0, 0, 0, 0.2);
				}
				.switch-input:checked ~ .switch-label:before {
					opacity: 0;
				}
				.switch-input:checked ~ .switch-label:after {
					opacity: 1;
				}
				.switch-handle {
					position: absolute;
					top: 4px;
					left: 4px;
					width: 28px;
					height: 28px;
					background: linear-gradient(to bottom, #FFFFFF 40%, #f0f0f0);
					background-image: -webkit-linear-gradient(top, #FFFFFF 40%, #f0f0f0);
					border-radius: 100%;
					box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);
				}
				.switch-handle:before {
					content: "";
					position: absolute;
					top: 50%;
					left: 50%;
					margin: -6px 0 0 -6px;
					width: 12px;
					height: 12px;
					background: linear-gradient(to bottom, #eeeeee, #FFFFFF);
					background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF);
					border-radius: 6px;
					box-shadow: inset 0 1px rgba(0, 0, 0, 0.02);
				}
				.switch-input:checked ~ .switch-handle {
					left: 74px;
					box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);
				}
  </style>
</head>

<!-- ------------------------------------------------------------------ -->
<!-- BODY IS BEGIN                                                      -->
<!-- ------------------------------------------------------------------ -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-md">
  <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueHeader",array());?>
  <div class="page-container">
    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueSideBar",array());?>
    <div class="page-content-wrapper">
      <div class="page-content">
        <h3 class="page-title">
          <?php echo $configurations["PAGE_HEADER"]["MAIN_TITLE"]?>
          <small>
            <?php echo $configurations["PAGE_HEADER"]["SUB_TITLE"]?>
          </small>
        </h3>
        <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueBreadCrumb",array());?>

        <!-- ------------------------------------------------------------------ -->
        <!-- CONTENT IS BEGIN                                                   -->
        <!-- ------------------------------------------------------------------ -->

        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
              <div class="portlet-title">
                <div class="caption font-green">
                  <i class="<?php echo $configurations["PORTLET_HEADER"]["ICON"]?> font-green"></i>
                  <span class="caption-subject bold uppercase">
                    <?php echo $configurations["PORTLET_HEADER"]["TITLE"]?>
                  </span>
                </div>

              </div>
              <div class="portlet-body">
                <div class="row">
                  <div class="col-sm-5">
                    <div class="rating-block">
                      <h4>Average user rating</h4>
                      <h2 class="bold padding-bottom-7"><?php echo number_format($avg->rating, 1) ?> <small>/ 5</small></h2>
                      <br>
                      <button type="button" class="btn btn-sm <?php echo ($avg->rating >= 1 ? 'btn-warning' : 'btn-default btn-grey') ?>" aria-label="Left Align">
                        <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                      </button>
                      <button type="button" class="btn btn-sm <?php echo ($avg->rating >= 2 ? 'btn-warning' : 'btn-default btn-grey') ?>" aria-label="Left Align">
                        <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                      </button>
                      <button type="button" class="btn btn-sm <?php echo ($avg->rating >= 3 ? 'btn-warning' : 'btn-default btn-grey') ?>" aria-label="Left Align">
                        <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                      </button>
                      <button type="button" class="btn btn-sm <?php echo ($avg->rating >= 4 ? 'btn-warning' : 'btn-default btn-grey') ?>" aria-label="Left Align">
                        <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                      </button>
                      <button type="button" class="btn btn-sm <?php echo ($avg->rating >= 5 ? 'btn-warning' : 'btn-default btn-grey') ?>" aria-label="Left Align">
                        <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                      </button>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <h4>Rating breakdown</h4>
                      <?php if(!empty($scores_5)) { ?>
                        <div class="pull-left">
                          <div class="pull-left" style="width:35px; line-height:1;">
                            <div style="height:9px; margin:5px 0;">5 <span class="glyphicon glyphicon-star"></span></div>
                          </div>
                          <div class="pull-left" style="width:180px;">
                            <div class="progress" style="height:9px; margin:8px 0;">
                              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5"
                                aria-valuemin="0" aria-valuemax="5" style="width: 1000%">
                                <span class="sr-only">80% Complete (danger)</span>
                              </div>
                            </div>
                          </div>
                          <div class="pull-right" style="margin-left:10px;"><?php echo $scores_5->count ?></div>
                        </div>
                      <?php }else{ ?>
                        <div class="pull-left">
                          <div class="pull-left" style="width:35px; line-height:1;">
                            <div style="height:9px; margin:5px 0;">5 <span class="glyphicon glyphicon-star"></span></div>
                          </div>
                          <div class="pull-left" style="width:180px;">
                            <div class="progress" style="height:9px; margin:8px 0;">
                              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5"
                                aria-valuemin="0" aria-valuemax="5" style="width: 1000%">
                                <span class="sr-only">80% Complete (danger)</span>
                              </div>
                            </div>
                          </div>
                          <div class="pull-right" style="margin-left:10px;">0</div>
                        </div>
                      <?php } if(!empty($scores_4)) { ?>
                        <div class="pull-left">
                          <div class="pull-left" style="width:35px; line-height:1;">
                            <div style="height:9px; margin:5px 0;">4 <span class="glyphicon glyphicon-star"></span></div>
                          </div>
                          <div class="pull-left" style="width:180px;">
                            <div class="progress" style="height:9px; margin:8px 0;">
                              <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="4"
                                aria-valuemin="0" aria-valuemax="5" style="width: 80%">
                                <span class="sr-only">80% Complete (danger)</span>
                              </div>
                            </div>
                          </div>
                          <div class="pull-right" style="margin-left:10px;"><?php echo $scores_4->count ?></div>
                        </div>
                      <?php }else{ ?>
                        <div class="pull-left">
                          <div class="pull-left" style="width:35px; line-height:1;">
                            <div style="height:9px; margin:5px 0;">4 <span class="glyphicon glyphicon-star"></span></div>
                          </div>
                          <div class="pull-left" style="width:180px;">
                            <div class="progress" style="height:9px; margin:8px 0;">
                              <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="4"
                                aria-valuemin="0" aria-valuemax="5" style="width: 80%">
                                <span class="sr-only">80% Complete (danger)</span>
                              </div>
                            </div>
                          </div>
                          <div class="pull-right" style="margin-left:10px;">0</div>
                        </div>
                      <?php }if(!empty($scores_3)) { ?>
                        <div class="pull-left">
                          <div class="pull-left" style="width:35px; line-height:1;">
                            <div style="height:9px; margin:5px 0;">3 <span class="glyphicon glyphicon-star"></span></div>
                          </div>
                          <div class="pull-left" style="width:180px;">
                            <div class="progress" style="height:9px; margin:8px 0;">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="3"
                                aria-valuemin="0" aria-valuemax="5" style="width: 60%">
                                <span class="sr-only">80% Complete (danger)</span>
                              </div>
                            </div>
                          </div>
                          <div class="pull-right" style="margin-left:10px;"><?php echo $scores_3->count ?></div>
                        </div>
                      <?php }else { ?>
                        <div class="pull-left">
                          <div class="pull-left" style="width:35px; line-height:1;">
                            <div style="height:9px; margin:5px 0;">3 <span class="glyphicon glyphicon-star"></span></div>
                          </div>
                          <div class="pull-left" style="width:180px;">
                            <div class="progress" style="height:9px; margin:8px 0;">
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="3"
                                aria-valuemin="0" aria-valuemax="5" style="width: 60%">
                                <span class="sr-only">80% Complete (danger)</span>
                              </div>
                            </div>
                          </div>
                          <div class="pull-right" style="margin-left:10px;">0</div>
                        </div>
                      <?php } if(!empty($scores_2)) { ?>
                        <div class="pull-left">
                          <div class="pull-left" style="width:35px; line-height:1;">
                            <div style="height:9px; margin:5px 0;">2 <span class="glyphicon glyphicon-star"></span></div>
                          </div>
                          <div class="pull-left" style="width:180px;">
                            <div class="progress" style="height:9px; margin:8px 0;">
                              <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="2"
                                aria-valuemin="0" aria-valuemax="5" style="width: 40%">
                                <span class="sr-only">80% Complete (danger)</span>
                              </div>
                            </div>
                          </div>
                          <div class="pull-right" style="margin-left:10px;"><?php echo $scores_2->count ?></div>
                        </div>
                      <?php }else { ?>
                        <div class="pull-left">
                          <div class="pull-left" style="width:35px; line-height:1;">
                            <div style="height:9px; margin:5px 0;">2 <span class="glyphicon glyphicon-star"></span></div>
                          </div>
                          <div class="pull-left" style="width:180px;">
                            <div class="progress" style="height:9px; margin:8px 0;">
                              <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="2"
                                aria-valuemin="0" aria-valuemax="5" style="width: 40%">
                                <span class="sr-only">80% Complete (danger)</span>
                              </div>
                            </div>
                          </div>
                          <div class="pull-right" style="margin-left:10px;">0</div>
                        </div>
                      <?php }if(!empty($scores_1)) { ?>
                        <div class="pull-left">
                          <div class="pull-left" style="width:35px; line-height:1;">
                            <div style="height:9px; margin:5px 0;">1 <span class="glyphicon glyphicon-star"></span></div>
                          </div>
                          <div class="pull-left" style="width:180px;">
                            <div class="progress" style="height:9px; margin:8px 0;">
                              <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1"
                                aria-valuemin="0" aria-valuemax="5" style="width: 20%">
                                <span class="sr-only">80% Complete (danger)</span>
                              </div>
                            </div>
                          </div>
                          <div class="pull-right" style="margin-left:10px;"><?php echo $scores_1->count ?></div>
                        </div>
                        <?php }else{ ?>
                          <div class="pull-left">
                          <div class="pull-left" style="width:35px; line-height:1;">
                            <div style="height:9px; margin:5px 0;">1 <span class="glyphicon glyphicon-star"></span></div>
                          </div>
                          <div class="pull-left" style="width:180px;">
                            <div class="progress" style="height:9px; margin:8px 0;">
                              <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1"
                                aria-valuemin="0" aria-valuemax="5" style="width: 20%">
                                <span class="sr-only">80% Complete (danger)</span>
                              </div>
                            </div>
                          </div>
                          <div class="pull-right" style="margin-left:10px;">0</div>
                        </div>
                        <?php }if(!empty($scores_0)) { ?>
                        <div class="pull-left">
                          <div class="pull-left" style="width:35px; line-height:1;">
                            <div style="height:9px; margin:5px 0;">0 <span class="glyphicon glyphicon-star"></span></div>
                          </div>
                          <div class="pull-left" style="width:180px;">
                            <div class="progress" style="height:9px; margin:8px 0;">
                              <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1"
                                aria-valuemin="0" aria-valuemax="5" style="width: 0%">
                                <span class="sr-only">80% Complete (danger)</span>
                              </div>
                            </div>
                          </div>
                          <div class="pull-right" style="margin-left:10px;"><?php echo $scores_0->count ?></div>
                        </div>
                        <?php }else{ ?>
                          <div class="pull-left">
                          <div class="pull-left" style="width:35px; line-height:1;">
                            <div style="height:9px; margin:5px 0;">0 <span class="glyphicon glyphicon-star"></span></div>
                          </div>
                          <div class="pull-left" style="width:180px;">
                            <div class="progress" style="height:9px; margin:8px 0;">
                              <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1"
                                aria-valuemin="0" aria-valuemax="5" style="width: 0%">
                                <span class="sr-only">80% Complete (danger)</span>
                              </div>
                            </div>
                          </div>
                          <div class="pull-right" style="margin-left:10px;">0</div>
                        </div>
                        <?php } ?>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <hr />
                    <div class="review-block">
                      <?php foreach($members as $member) { ?>
                        <div class="row">
                        
                        <div class="col-sm-2">
                          <img src="<?php echo base_url('assets/uploads/noimage.png'); ?>" class="img-rounded">
                          <div class="review-block-name"><a href="#"><?php echo $member->{'member::fullname'}; ?></a></div>
                          <div class="review-block-date"><?php $date=date_create($member->{'review::created_date'}); echo date_format($date,"M d, Y");  ?><br /><?php $date1=date_create($member->{'review::created_date'});$date2= date_create(date("Y-m-d"));$diff=date_diff($date1,$date2);echo $diff->format("%a").' Day ago';  ?></div>

                        </div>
                        <div class="col-sm-8">
                          <div class="review-block-rate">
                          <button type="button" class="btn btn-sm <?php echo ($member->{'review::rating'} >= 1 ? 'btn-warning' : 'btn-default btn-grey') ?>" aria-label="Left Align">
                            <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                          </button>
                          <button type="button" class="btn btn-sm <?php echo ($member->{'review::rating'} >= 2 ? 'btn-warning' : 'btn-default btn-grey') ?>" aria-label="Left Align">
                            <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                          </button>
                          <button type="button" class="btn btn-sm <?php echo ($member->{'review::rating'} >= 3 ? 'btn-warning' : 'btn-default btn-grey') ?>" aria-label="Left Align">
                            <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                          </button>
                          <button type="button" class="btn btn-sm <?php echo ($member->{'review::rating'} >= 4 ? 'btn-warning' : 'btn-default btn-grey') ?>" aria-label="Left Align">
                            <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                          </button>
                          <button type="button" class="btn btn-sm <?php echo ($member->{'review::rating'} >= 5 ? 'btn-warning' : 'btn-default btn-grey') ?>" aria-label="Left Align">
                            <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                          </button>
                          </div>
                          <!-- <div class="review-block-title">this was nice in buy</div> -->
                          <div>
                            <?php foreach($comments as $comment) { ?>
                              <?php if($comment->{'review_image::review_id'} == $member->{'review::id'}) { ?>
                                <img  src="<?php echo base_url().$comment->{'review_image::image_path'}; ?>" class="img-rounded" style="padding: 10px;width:230px;">
                              <?php } ?>
                              
                            <?php } ?>
                          </div>
                          <div class="review-block-description"><?php echo $member->{'review::message'} ?></div>
                        </div>
                        <div class="col-sm-2">
                        <label class="switch">
                          <input class="switch-input myCheckbox" id="" type="checkbox"  <?php if($member->{'review::status'} =="ACTIVATE") echo 'checked' ; ?> value="<?php echo $member->{'review::id'} ?>" />
                          <span class="switch-label" data-on="On" data-off="Off"></span> 
                          <span class="switch-handle"></span> 
                        </label>
                          
                        </div>
                        
                      </div>
                      <hr />
                      <?php  } ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueFooter",array());?>
</body>

<!-- ------------------------------------------------------------------ -->
<!-- JAVASCRIPT IS BEGIN                                                -->
<!-- ------------------------------------------------------------------ -->
<!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/respond.min.js');?>"></script> <![endif]-->
<!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/excanvas.min.js');?>"></script> <![endif]-->

<!-- MANDATORY SCRIPT AREA -->
<script src="<?php echo assetsDirectory("global/plugins/jquery.min.js");?>" type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/bootstrap/js/bootstrap.min.js");?>" type="text/javascript">
</script>
<script src="<?php echo assetsDirectory("global/plugins/js.cookie.min.js");?>" type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js");?>"
  type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/jquery-slimscroll/jquery.slimscroll.min.js");?>"
  type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/jquery.blockui.min.js");?>" type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/bootstrap-switch/js/bootstrap-switch.min.js");?>"
  type="text/javascript"></script>

<!-- PLUGINS AREA -->
<script src="<?php echo assetsDirectory("global/plugins/jquery-ui/jquery-ui.min.js");?>" type="text/javascript">
</script>
<script src="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.js");?>" type="text/javascript">
</script>
<script src="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js");?>"
  type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/moment.min.js");?>" type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.js");?>"
  type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js");?>"
  type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js");?>"
  type="text/javascript"></script>
<script
  src="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");?>"
  type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/clockface/js/clockface.js");?>" type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/select2/js/select2.full.min.js");?>" type="text/javascript">
</script>
<script src="<?php echo assetsDirectory("global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js");?>"
  type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/jquery.validate.min.js");?>"
  type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/additional-methods.min.js");?>"
  type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.js");?>"
  type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.js");?>"></script>
<script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote-cleaner.js");?>"></script>
<script src="<?php echo assetsDirectory("global/plugins/jstree/dist/jstree.min.js");?>" type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.js");?>" type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/scripts/datatable.js");?>" type="text/javascript"></script>

<script src="<?php echo assetsDirectory("global/plugins/backstretch/jquery.backstretch.min.js");?>"
  type="text/javascript"></script>
<script src="<?php echo assetsDirectory("global/plugins/bootstrap-toastr/toastr.min.js");?>" type="text/javascript">
</script>


<!-- THEME SCRIPT AREA -->
<script src="<?php echo assetsDirectory("global/scripts/app.min.js");?>" type="text/javascript"></script>

<!-- LAYOUT AREA -->
<script src="<?php echo assetsDirectory("layouts/layout/scripts/layout.min.js");?>" type="text/javascript"></script>
<script src="<?php echo assetsDirectory("layouts/layout/scripts/demo.min.js");?>" type="text/javascript"></script>
<script src="<?php echo assetsDirectory("layouts/global/scripts/quick-sidebar.js");?>" type="text/javascript"></script>

<!-- CUSTOM AREA -->
<script>
  jQuery(document).ready(function () {

    $('.myCheckbox').click(function() {
    var checked = $(this).is(':checked');
    var id = $(this).val();
    var data = new FormData();
    data.append ('id', id);
    if(checked){
      data.append ('checked', 'ACTIVATE');
    }else{
      data.append ('checked', 'SUSPEND');
    }
    
    
    $.ajax({
          data: data,
          type: "POST",
          contentType: false,
          processData: false,
          url: "<?php echo base_url("administrator_area/product/status_review")?>",
          success: function (response) {
            console.log(response);
            try {
              if (response.code == "0x0000-00000") {

                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "positionClass": "toast-bottom-right",
                  "onclick": null,
                  "showDuration": "1000",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
                toastr.success('Update Completed', 'Success');
              } else {
                $("#system-return-error .message").html(response.message);
                $("#system-return-error").modal("toggle");
              }
            } catch (e) {
              $("#system-return-failed").modal("toggle");
            }
          },
          error: function () {
            $("#system-disconnected").modal("toggle");
          }
        });
});


  });
</script>

<!-- INCLUDE RAW SCRIPT AREA -->
<?php $this->load->view("administrator_area/__scripts/Javascript_Modal",array());?>
<?php $this->load->view("administrator_area/__scripts/Javascript_Select2",array());?>

</html>

<!-- ------------------------------------------------------------------ -->
<!-- MODAL IS BEGIN                                                     -->
<!-- ------------------------------------------------------------------ -->
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationCancel");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationDelete");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationRefresh");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationSave");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_ConfirmationSaveMultiform");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_PermissionDenied");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemDisconnected");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnError");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnFailed");?>