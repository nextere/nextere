<?php
$codeigniter_instance =& get_instance();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <title><?php echo $this->config->item('WEBSITE_NAME');?> | Login System</title>
        <link href="<?php echo faviconDirectory($this->config->item('WEBSITE_FAVICON'));?>" rel="shortcut icon" />

        <!-- META TAG AREA -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

        <!-- MANDATORY STYLE AREA -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/simple-line-icons/simple-line-icons.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap/css/bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/bootstrap-switch/css/bootstrap-switch.min.css");?>" rel="stylesheet" type="text/css" />

        <!-- PLUGINS AREA -->
        <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("global/plugins/select2/css/select2-bootstrap.min.css");?>" rel="stylesheet" type="text/css" />

        <!-- THEME STYLE AREA -->
        <link href="<?php echo assetsDirectory("global/css/components-md.min.css");?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo assetsDirectory("global/css/plugins-md.min.css");?>" rel="stylesheet" type="text/css" />

        <!-- LAYOUT AREA -->
        <link href="<?php echo assetsDirectory("layouts/layout/css/layout.min.css");?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assetsDirectory("layouts/layout/css/themes/light.min.css");?>" rel="stylesheet" type="text/css"  />
        <link href="<?php echo assetsDirectory("layouts/layout/css/custom.min.css");?>" rel="stylesheet" type="text/css" />

        <!-- CUSTOM AREA -->
    </head>

    <!-- ------------------------------------------------------------------ -->
    <!-- BODY IS BEGIN                                                      -->
    <!-- ------------------------------------------------------------------ -->
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-md">
        <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueHeader",array());?>
        <div class="page-container">
            <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueSideBar",array());?>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <h3 class="page-title"> หน้าหลัก
                        <small>จัดการข้อมูลเว็บไซต์</small>
                    </h3>
                    <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueBreadCrumb",array());?>

                    <pre>
                    <?php
//                    print_r($codeigniter_instance->session->userdata());
                    ?>
                    </pre>
                </div>
            </div>
        </div>

        <?php $this->load->view("administrator_area/__shards/HTML_DarkBlueFooter",array());?>
    </body>

    <!-- ------------------------------------------------------------------ -->
    <!-- JAVASCRIPT IS BEGIN                                                -->
    <!-- ------------------------------------------------------------------ -->
    <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/respond.min.js');?>"></script> <![endif]-->
    <!--[if lt IE 9]> <script src="<?php echo assetsDirectory('global/plugins/excanvas.min.js');?>"></script> <![endif]-->
    
    <!-- MANDATORY SCRIPT AREA -->
    <script src="<?php echo assetsDirectory("global/plugins/jquery.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap/js/bootstrap.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/js.cookie.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery-slimscroll/jquery.slimscroll.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery.blockui.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-switch/js/bootstrap-switch.min.js");?>" type="text/javascript"></script>
    
    <!-- PLUGINS AREA -->
    <script src="<?php echo assetsDirectory("global/plugins/jquery-ui/jquery-ui.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/datatables/datatables.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/moment.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-daterangepicker/daterangepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/clockface/js/clockface.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/select2/js/select2.full.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/jquery.validate.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jquery-validation/js/additional-methods.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-fileinput/bootstrap-fileinput.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote.js");?>"></script>
    <script src="<?php echo assetsDirectory("global/plugins/bootstrap-summernote/summernote-cleaner.js");?>"></script>
    <script src="<?php echo assetsDirectory("global/plugins/jstree/dist/jstree.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/plugins/dropzone/dropzone.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("global/scripts/datatable.js");?>" type="text/javascript"></script>
    
    <script src="<?php echo assetsDirectory("global/plugins/backstretch/jquery.backstretch.min.js");?>" type="text/javascript"></script>
    
    <!-- THEME SCRIPT AREA -->
    <script src="<?php echo assetsDirectory("global/scripts/app.min.js");?>" type="text/javascript"></script>

    <!-- LAYOUT AREA -->
    <script src="<?php echo assetsDirectory("layouts/layout/scripts/layout.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("layouts/layout/scripts/demo.min.js");?>" type="text/javascript"></script>
    <script src="<?php echo assetsDirectory("layouts/global/scripts/quick-sidebar.js");?>" type="text/javascript"></script>
    
    <!-- CUSTOM AREA -->
    <script>
        jQuery(document).ready(function() {
            /** =================================================================== **/
            /** ELEMENT SETTING                                                     **/
            /** =================================================================== **/
            //$("body").addClass("page-sidebar-closed");
            //$(".page-sidebar-menu").addClass("page-sidebar-menu-closed");

        });
    </script>

    <!-- INCLUDE RAW SCRIPT AREA -->
</html>

<!-- ------------------------------------------------------------------ -->
<!-- MODAL IS BEGIN                                                     -->
<!-- ------------------------------------------------------------------ -->
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemDisconnected");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnError");?>
<?php $codeigniter_instance->load->view("administrator_area/__modals/Modal_SystemReturnFailed");?>