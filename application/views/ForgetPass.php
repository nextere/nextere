<?php include('header.php') ?>

<body style="background-color:#F8F8F8">

    <div class="container w-100" style="text-align: -webkit-center;">

        <div class="col-sm-12 col-xl-8" style="background-color:#FFFFFF; ">

            <form id="datafrom">
                <div class="pt-4 w-75">
                    <div style="border-bottom: 2px solid #D6D6D6; padding-bottom:20px; margin-bottom:35px;width: auto; padding-top:91px">
                        <h4>
                            <b>
                            <?php echo getWording('login', 'change_password') ?>
                            </b>
                        </h4>
                    </div>

                    <div style="width: auto; height:auto;">

                        <div class="col-sm-12 col-xl-12 pt-1" style="text-align:left;"><b><?php echo getWording('login', 'email') ?>*</b>
                            <div class="email pt-2">
                                <input type="email" id="email" name="email" placeholder="<?php echo getWording('login', 'email') ?>" required style="width: 100%; height: 45px;
                        border: 1px solid #707070; border-radius: 5px;padding-left:15px ">
                            </div>
                        </div>

                        <div class="submit pt-4">
                            <!-- <a href="page57.php"> -->
                            <input type="submit" id="submit-datafrom" value="<?php echo getWording('login', 'submit') ?>" style="width: 250px; margin-bottom:10%;
                    height: 45px; background: #D3D3D3; border: 0px; border-radius: 23px; font-weight: bold;">
                            <!-- </a> -->
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-content-style">
                <div class="modal-header modal-header-style">

                </div>
                <div class="modal-body">
                    <div class="btn-close-modal">
                        <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
                    </div>

                    <h3 class="forget_response" style="text-align: center;"></h3>
                </div>
                <div class="modal-footer modal-footer-style forget_response_btn">
                    <!-- <button type="button" class="btn btn-success register_success d-none" onclick="location.href='<?php // echo base_url() 
                                                                                                                        ?>'">Understood</button> -->
                    <button type="button" class="btn register_fail d-none" style="width: 142px;height: 40px; background-color: #AAAAAA; border-radius: 23px; padding: 10px; margin:10px;color:white;" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $("#datafrom").submit(function(e) {
            e.preventDefault();
            return false;
        });

        $("#submit-datafrom").click(function() {

            $("#datafrom").validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                },
                messages: {
                    email: "Please enter a valid email address",
                },

                errorPlacement: function(error, element) {
                    error.insertBefore(element);
                },
                submitHandler: function() {

                    var data = {
                        "email": $("#email").val(),
                    }

                    var send_data = {
                        "url": "<?php echo base_url('Account/forget_password') ?>",
                        "method": "POST",
                        "data": data
                    }

                    $.ajax(send_data).done(function(response) {
                        if (response.code == "0x0000-00000") {
                            window.location.href = "<?php echo base_url() ?>";
                            $('.forget_success').addClass('d-none');
                            $('.forget_success').removeClass('d-block');
                            $('.forget_fail').addClass('d-block');
                            $('.forget_fail').removeClass('d-none');
                            $(".forget_response").text('Your reset e-mail has been sent. Please check your registered e-mail.');
                        } else {
                            $('.forget_success').addClass('d-none');
                            $('.forget_success').removeClass('d-block');
                            $('.forget_fail').addClass('d-block');
                            $('.forget_fail').removeClass('d-none');
                            $(".forget_response").text(response.message);
                        }
                        $('#staticBackdrop').modal('show');
                    });
                }
            });
        });
    </script>
</body>

<?php include('footer.php') ?>