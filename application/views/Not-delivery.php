<?php include('header.php') ?>




<body>
    <div class="container">
        <div class="head27 pt-5">
            <h2>ชำระสินค้า</h2>
        </div>
        <div class="row">
            <div class="col-sm-6 col-xl-9" style="margin-bottom: auto; padding-bottom: auto;">
                <div class="payment" id="accordion">
                    <div class="card1 mt-3" style="border: solid#7C7C7C;">
                        <div class="card-header" id="headingOne" style="background-color: #7C7C7C">
                            <div class="row">
                                <h5 class="col-8" style="padding-top: 8px;">1. วิธีการจัดส่ง</h5>
                                <h5 class="col-4" style="text-align: right;">
                                    <button class="btn" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <u>แก้ไข</u>
                                    </button>
                                </h5>
                            </div>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">

                                <div class="radio-page27" style="text-align: left;">
                                    <label class="mt-2 mr-5">
                                        <a href="http://localhost/nextere2/payment">
                                            <input type="radio" class="mr-1" name="shipping">จัดส่งสินค้าตามที่อยู่
                                        </a>
                                    </label>
                                </div>
                                <div class="radio-page27" style="text-align: left;">
                                    <label class="mt-3 mr-5">
                                        <a href="Notdelivery">
                                            <input type="radio" class="mr-1" name="shipping" checked="true">รับสินค้าเองที่ตู้
                                        </a>
                                    </label>
                                </div>

                                <div class="pt-3 mt-3" style="border-top: solid 2px; border-color:#D3D3D3">
                                    <h5>
                                        เลือกสาขาที่ต้องการรับ
                                    </h5>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-xl-6 subsidiary pt-1">
                                        <select id="subsidiary" name="subsidiary" style="width: 100%;">
                                            <option value="city">สาขารัชดา</option>
                                            <option value="city">สาขาลาดพร้าว</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-xl-4">
                                        *สามารถรับช่วง 08.00 - 20.00 น.
                                        และภายใน 7 วันหลังจากยืนยันการสั่ง
                                    </div>
                                </div>

                                <div class="mt-4">
                                    ที่อยู่
                                </div>
                                <div>
                                    88/8 Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor incididu 10090
                                </div>

                                <div class="card5 pt-4">
                                    <div class="card-header" id="headingThree1">
                                        <h5 class="mb-0">
                                            <button class="btn" data-toggle="collapse" data-target="#collapseThree1" aria-expanded="true" aria-controls="collapseThree1">
                                                <div class="want pt-1">
                                                    <label class="form-check-label"><input type="checkbox" required="required"> ต้องการใบกำกับภาษี</label>
                                                </div>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThree1" class="collapse" aria-labelledby="headingThree1">
                                        <div class="card-body">

                                            <div class="information" style="width: auto; height: auto; border: solid #DBDBDB;">
                                                <div class="row p-3">
                                                    <div class="col-sm-12 col-xl-10">
                                                        Jidapa Thaiudomsub<br>
                                                        26/46 อาคารอรกานต์ ชั้น 12A ถ.ชิดลม แขวงลุมพินีเขตปทุมวัน กรุงเทพมหานคร 10330<br>
                                                        เลขประจําตัวผู้เสียภาษี 1-10000-00-00-0<br>
                                                        085-022-8426

                                                    </div><div class="col-1"></div>
                                                    <div class="col-sm-12 col-xl-1">
                                                        แก้ไข
                                                    </div>
                                                </div>

                                            </div>
                                            <?php include('Add-address.php') ?>
                                        </div>
                                    </div>
                                </div>



                                <div style="text-align: center;">
                                    <button class="btn1 mt-3" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true">
                                        ถัดไป
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card4 mt-3" style="border: solid#7C7C7C;">
                        <div class="card-header" id="headingFour" style="background-color: #7C7C7C">
                            <div class="row">
                                <h5 class="col-8" style="padding-top: 8px;">2. ช่องทางการชำระเงิน</h5>
                                <h5 class="col-4" style="text-align: right;">
                                    <button class="btn" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                        <u>แก้ไข</u>
                                    </button>
                                </h5>
                            </div>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                <div class="radio-page27" style="text-align: left;">
                                    <label class="mt-2 mr-5">
                                        <input type="radio" class="mr-1" name="payment" checked="true">บัตรเครดิต/เดบิต
                                    </label>


                                    <div class="visa pl-3" style="text-align: left;">
                                        <div class="information" style="width: 100%; height: 145px; border: solid #DBDBDB;">
                                            <div class="row p-3">
                                                <div class="col-10">
                                                    Visa เลขที่บัตร xxxxxxxxxx<br>
                                                    ชื่อบนบัตร --------<br>
                                                    วันหมดอายุ xx/xx
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 pt-3" style="text-align: left;">
                                            <?php include('Add-creditcard2.php') ?>
                                        </div>
                                    </div>

                                    <div class="mt-5">
                                        <h5>ผ่าน AIS mPlay</h5>
                                    </div>
                                    <div>
                                        <label class="mt-2 mr-5">
                                            <input type="radio" class="mr-1" name="payment">Mobile Banking
                                        </label>
                                    </div>
                                    <div>
                                        <label class="mt-2 mr-5">
                                            <input type="radio" class="mr-1" name="payment">Internet Banking

                                        </label>
                                    </div>
                                    <div>
                                        <label class="mt-2 mr-5">
                                            <input type="radio" class="mr-1" name="payment">PromtPay
                                        </label>
                                    </div>
                                    <div>
                                        <label class="mt-2 mr-5">
                                            <input type="radio" class="mr-1" name="payment">Rabbit Line Pay (Online/Qr Code)
                                        </label>
                                    </div>
                                    <div>
                                        <label class="mt-2 mr-5">
                                            <input type="radio" class="mr-1" name="payment">Alipay
                                        </label>
                                    </div>

                                    <div>
                                        <label class="mt-2 mr-5">
                                            <input type="radio" class="mr-1" name="payment">WeChat
                                        </label>
                                    </div>

                                    <div>
                                        <label class="mt-2 mr-5">
                                            <input type="radio" class="mr-1" name="payment">VIA
                                        </label>
                                    </div>

                                    <div>
                                        <label class="mt-2 mr-5">
                                            <input type="radio" class="mr-1" name="payment">mPay Station
                                        </label>
                                    </div>

                                </div>


                            </div>
                            <div class="ml-3">
                                <h5>ข้อตกลงและเงื่อนไขการรับประกันสินค้า*</h5>
                            </div>
                            <div style="background-color: #F2F2F2; margin: 10px; padding: 15px;">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                                dolore magna aliqua. Cursus in hac habitasse platea dictumst quisque. Nisl rhoncus mattis rhoncus urna
                                neque viverra justo. Vel pharetra vel turpis nunc eget. Dolor magna eget est lorem. Cras adipiscing enim
                                eu turpis egestas pretium. Nisl purus in mollis nunc sed id. Massa vitae tortor condimentum lacinia quis
                                vel eros donec. Cursus sit amet dictum sit amet justo donec. Justo eget magna fermentum iaculis eu.
                            </div>
                            <div>
                                <h5>
                                    <button class="btn" data-toggle="collapse" data-target="#collapseThree1" aria-expanded="true" aria-controls="collapseThree1">
                                        <div class="want pt-1">
                                            <label class="form-check-label"><input type="checkbox" required="required"> เข้าใจและยอมรับในข้อตกลงและเงื่อนไขการรับประกันสินค้า</label>
                                        </div>
                                    </button>
                                </h5>
                            </div>

                            <div style="padding-bottom: 40px;">
                                <?php include('Confirm-order.php') ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-xl-3">
                <div class="pay mt-4" style="border: solid#7C7C7C; width: 347px; height: 321px;">

                    <div class="shipment" style="width: auto; height: 50px; background-color: #7C7C7C;  padding-top: 12px;">
                        <div class="row">
                            <div class="col-5" style="color: #FFFFFF; margin-left: 15px;">สรุปรายการสั่งซื้อ</div>
                            <div class="col-6" style="color: #FFFFFF; text-align: right;">... รายการ</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-5" style="margin-left: 15px; padding-top: 12px;">
                            ราคาสินค้า
                        </div>
                        <div class="col-2">
                        </div>
                        <div class="col-2 price" style="text-align: right; padding-top: 12px;">
                            1150
                        </div>
                        <div class="col-1" style="padding-top: 12px;">
                            บาท
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-5 shipping" style="margin-left: 15px; padding-top: 12px;">
                            ค่าจัดส่ง
                        </div>
                        <div class="col-2">
                        </div>
                        <div class="col-2 shipping" style="text-align: right; padding-top: 12px;">
                            50
                        </div>
                        <div class="col-1" style="padding-top: 12px;">
                            บาท
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-5 discount" style="margin-left: 15px; padding-top: 12px;">
                            ส่วนลด
                        </div>
                        <div class="col-2">
                        </div>
                        <div class="col-2 discount" style="text-align: right; padding-top: 12px;">
                            100
                        </div>
                        <div class="col-1" style="padding-top: 12px;">
                            บาท
                        </div>
                    </div>
                    <hr style="border-width: 2px; width:100%">
                    <div class="row">
                        <div class="col-5 total" style="margin-left: 15px; padding-top: 12px;">
                            ราคารวมทั้งหมด
                        </div>
                        <div class="col-2 price">
                        </div>
                        <div class="col-2 price" style="text-align: right; padding-top: 12px;">
                            1100
                        </div>
                        <div class="col-1" style="padding-top: 12px;">
                            บาท
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-5">
        <?php include('footer.php') ?>
    </div>
</body>