<?php include('header.php') ?>
<style>
    .Datasheet ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 100%;
        background-color: white;
        border: 1px solid #BCBCBC;
        opacity: 1;
        font-size: 16px;
    }

    .Datasheet li a.active {
        background-color: #BCBCBC;
        color: white;
    }

    .Datasheet li a {
        display: block;
        color: #000;
        padding: 8px 12px;
        text-decoration: none;
        font-size: 14px;
    }

    .contact {
        list-style-type: none;

        padding: 20px;
        background-color: white;
        border: 1px solid #BCBCBC;
        opacity: 1;
        font-size: 16px;
    }
</style>

<title>RequestHistory</title>
</head>

<body>
    <section class="Datasheet">
        <div class="container mt-5">
            <div class="row">
                <div class="col-sm-12 col-xl-12 pt-5 pb-5 pl-5 mt-5" style="background-color: lightgray; text-align:left">
                    <h4><b><?php echo getWording('profile', 'my_account') ?></b></h4>
                    <p><?php echo getWording('profile', 'hi') ?> <?php echo $user_info->fullname ?>, <?php echo getWording('profile', 'detail') ?></p>
                </div>
                <div class="col-sm-4 col-xl-3 mt-3 pl-0">
                    <ul>
                        <li><a href="<?php echo base_url('Account/profile') ?>"> <img class="mr-3" style="width: 16px; height: 16px;" src="<?php echo base_url('/assets/img/icon/user.png') ?>"><?php echo getWording('profile', 'profile') ?></a></li>
                        <li><a href="<?php echo base_url('Account/order_history') ?>"><img class="mr-3" style="width: 20px; height: 18px;" src="<?php echo base_url('/assets/img/icon/shopping-cart.png') ?>"><?php echo getWording('profile', 'my_orders') ?></a></li>
                        <li><a class="active" href="<?php echo base_url('Account/quotation_request_history') ?>"><img class="mr-3" style="width: 18px; height: 18px;" src="<?php echo base_url('/assets/img/icon/Invoice_White.png') ?>"><?php echo getWording('profile', 'quotation') ?></a></li>
                        <li><a href="<?php echo base_url('Account/sending_address') ?>"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php echo base_url('/assets/img/icon/delivery-truck.png') ?>"><?php echo getWording('profile', 'shipping_address') ?></a></li>
                        <li><a href="<?php echo base_url('Account/tax_invoice_address') ?>"><img class="mr-3" style="width: 16px; height: 16px;" src="<?php echo base_url('/assets/img/icon/location-2.png') ?>"><?php echo getWording('profile', 'tax_invoice') ?></a></li>
                        <!-- <li><a href="<?php echo base_url('Account/payment') ?>"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php echo base_url('/assets/img/icon/credit-card.png') ?>"><?php echo getWording('profile', 'credit') ?></a></li> -->
                        <li><a href="<?php echo base_url('Account/datasheet') ?>"><img class="mr-3" style="width: 16px; height: 20px;" src="<?php echo base_url('/assets/img/icon/Datasheet.png') ?>"><?php echo getWording('profile', 'datasheet') ?></a></li>
                    </ul>
                </div>
                <div class="col-sm-8 col-xl-9">
                    <div class="row">
                        <div class="col-sm-12 col-xl-12 mt-3">
                            <h3 style="font-weight: bold"><?php echo getWording('profile', 'quotation') ?></h3>
                        </div>

                        <div class="col-sm-12 col-xl-12 mt-3">
                            <h5 style="font-weight: bold"><?php echo getWording('profile', 'quotation_number') ?></h5>
                            <h5 style="font-weight: bold"><?php echo $quotation_detail[0]->quotation_no ?></h5>
                        </div>

                        <hr>
                        <div class="col-12 pb-5">
                            <table class="table  ">
                                <thead class="thead-light">
                                    <tr class="table-active" style="text-align: center;">
                                        <th class="col-3"><?php echo getWording('profile', 'product_list') ?></th>
                                        <th class="col-2"><?php echo getWording('profile', 'price') ?></th>
                                        <th class="col-2"><?php echo getWording('profile', 'quantity') ?></th>
                                        <th class="col-2"><?php echo getWording('profile', 'total') ?></th>
                                        <th class="col-3"></th>
                                    </tr>
                                </thead>
                                <tbody style="vertical-align: middle;">
                                    <?php foreach ($quotation_detail[0]->quotation_details as $key => $quotation_item) { ?>
                                        <?php
                                        // echo '<pre>';
                                        // print_r($order_item);
                                        // exit();
                                        ?>
                                        <?php if (empty($quotation_item)) { ?>
                                            <?php echo null ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-8  d-block pl-3" style="align-self: center;">
                                                            <img class="w-100" src="<?php echo $quotation_item->{'product::images'}[0] ?>" alt="">
                                                        </div>

                                                        <div class="col-4 d-block pl-3" style="align-self: center;">
                                                            <p><?php echo getVariable($quotation_item, 'product::name')  ?></p>
                                                            <p><?php echo getVariable($quotation_item, 'product::model_name')  ?></p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="text-align: center;">
                                                    <?php echo $quotation_item->{'quotation_detail::price_per_unit'} ?>
                                                </td>
                                                <td style="text-align: center;">
                                                    <?php echo $quotation_item->{'quotation_detail::quantity'} ?>
                                                </td>
                                                <td style="text-align: center;">
                                                    <?php echo $quotation_item->{'quotation_detail::total_price'} ?>
                                                </td>
                                                <td>
                                                    <a href="<?php echo base_url('/Product/shopping_cart') ?>"><button type="button" class="btn btn-secondary align-self-center button1" style="width: 120px; height: 40px; padding-top: 5px; border-radius: 25px;"><?php echo getWording('index', 'buy') ?> </button>

                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6 col-xl-5">
                                <div class="col-12">
                                    <b>
                                        <p><?php echo getWording('quotation', 'quotation') ?></p>
                                    </b>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-6">
                                                <div><?php echo getWording('profile', 'transaction_date') ?></div>
                                                <div class="pt-2"><?php echo getWording('profile', 'company') ?></div>
                                                <div class="pt-2"><?php echo getWording('quotation', 'business') ?></div>
                                            </div>
                                            <div class="col-6">
                                                <div><?php echo $quotation_detail[0]->created_date ?></div>

                                                <div class="pt-2">
                                                    <!-- บริษัท -->
                                                    <?php if ($quotation_detail[0]->company_name == null) { ?>
                                                        <?php echo '-' ?>
                                                    <?php } else { ?>
                                                        <?php echo $quotation_detail[0]->company_name ?>
                                                    <?php } ?>
                                                </div>
                                                <div class="pt-2">
                                                    <?php if ($quotation_detail[0]->business_type_id == null) { ?>
                                                        <?php echo '-' ?>
                                                    <?php } else { ?>
                                                        <?php foreach ($business_type as $key => $business_type_search) { ?>
                                                            <?php if ($business_type_search->id == $quotation_title->business_type_id) { ?>
                                                                <div><?php echo getVariable($business_type_search, 'name') ?></div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 mt-3">
                                    <b>
                                        <p><?php echo getWording('contact', 'message') ?></p>
                                    </b>
                                    <p>
                                        <?php echo $quotation_detail[0]->message ?>
                                    </p>

                                </div>
                                <div class="col-12 mt-3">
                                    <b>
                                        <p><?php echo getWording('quotation', 'ps') ?></p>
                                    </b>
                                    <p>
                                        - <?php echo getWording('quotation', 'costs') ?> <br>
                                        - Price Validity 7 days
                                    </p>

                                </div>
                            
                        </div>
                        <div class="col-sm-0 col-lg-2 ">
                        </div>
                        <div class="contact col-sm-5 col-xl-5 mt-3 pl-5 ">
                            <b class="pb-2"><?php echo getWording('profile', 'problem_quotation') ?></b>
                            <p><?php echo ($contact->telephone) ?></p>
                            <b><?php echo getWording('login', 'or') ?></b>
                            <div class="mt-2"><a href="<?php echo base_url('Contact') ?>"> <button type="button" class="btn btn-secondary align-self-center"><?php echo getWording('index', 'contact') ?> </button></a></div>
                        </div>
                        <div class="p-10 " style="text-align:center; font-weight: bold">
                            <a href="<?php echo base_url('Account/quotation_request_history') ?>" style="text-decoration: underline;"><?php echo getWording('profile', 'back') ?></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <div class="mt-5"> <?php include('footer.php') ?></div>


</body>

</html>