<?php include('header.php') ?>
<!-- <link href="../assets/css/style-content.css" rel="stylesheet"> -->
<style>
    .bill .dashed {
        border: 1px dashed #BCBCBC;
        padding: 30px;
    }

    .bill .btn {
        border-radius: 20px;
        font-size: 12px;
    }
</style>

<body>
    <section class="bill">
        <div class="col-12  " style="background-color: #EFEFEF; text-align: center; padding:70px; height:250px; vertical-align: middle;">
            <h4><b><?php echo getWording('payment', 'fail') ?></b></h4>
            <h4><b><?php echo getWording('quotation', 'thank') ?> Nextere</b></h4>
        </div>
        <div class="row justify-content-center mt-3">
            <div class="col-6 mt-3" style="text-align:right;">
                <a href="<?php echo base_url('') ?>"> <button type="button" class="btn btn-outline-dark mb-5" style="width: 250px; height: 40px; padding-top: 5px;"><b><?php echo getWording('quotation', 'home') ?></b></button></a>
            </div>
            <div class="col-6 mt-3">
                <a href="<?php echo base_url('/Product/shopping-cart') ?>"> <button type="button" class="btn btn-secondary nb-5" style="width: 250px; height: 40px; padding-top: 5px;"><b><?php echo getWording('profile', 'order_again') ?></b></button></a>
            </div>
        </div>
    </section>
</body>
<div class="mt-5"> <?php include('footer.php') ?></div>