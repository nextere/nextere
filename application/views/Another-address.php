
<body>

    <div class="container">

        <!-- Button to Open the Modal -->
        <button type="button" class="btn1 btn-primary" data-toggle="modal" data-target="#myModal1" style="width: 200px;
        height: 45px;
        background-color: #ffffff;
        color: #000000;
        border: 1px solid #555555;
        border-radius: 23px;
        ">
            เลือกที่อยู่อื่น
        </button>

        <!-- The Modal -->
        <div class="modal" id="myModal1">
            <div class="modal-dialog modal-dialog-scrollable" style="justify-content: center;">
                <div class="modal-content" style="min-inline-size: fit-content;">
                    <!-- Modal Header -->
                    <div class="modal-header" style="width:auto ">
                        <h5 class="modal-title" style="padding-left:20px">เลือกที่อยู่อื่น</h5>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body" style="width:auto">
                        <div class="row">
                            <div class="col-sm-12 col-xl-12" style="padding-bottom: 10px; text-align: center;">
                                <div class="mt-3" style="width: auto; height: auto; background-color: #ffffff; border:solid;">
                                    <div class="col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        กรกนก นามสกุล
                                    </div>
                                    <div class="col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        88/8 Lorem ipsum dolor sit amet, consectetur elit, sed
                                        do eiusmod tempor incididu 10090
                                    </div>
                                    <div class="col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        email@gmail.com <br>
                                        088-888-8888
                                    </div>

                                    <div class="col-sm-12 col-xl-7" style="width: 161px;
                                                                            height: 45px;
                                                                            background-color: #E6E5E5;
                                                                            color: #000000;
                                                                            border-radius: 3px;
                                                                            margin: 10px;
                                                                            padding: 10px;">
                                        ที่อยู่หลัก
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xl-12" style="padding-bottom: 10px; text-align: center;">
                                <div class="mt-3" style="width:auto; height: auto; background-color: #ffffff; border:solid 1px;">
                                    <div class="col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        กรกนก นามสกุล
                                    </div>
                                    <div class="col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        88/8 Lorem ipsum dolor sit amet, consectetur elit, sed
                                        do eiusmod tempor incididu 10090
                                    </div>
                                    <div class="col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        email@gmail.com <br>
                                        088-888-8888
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-xl-12" style="padding-bottom: 10px; text-align: center;">
                                <div class="mt-3" style="width:auto; height: auto; background-color: #ffffff; border:solid 1px;">
                                    <div class="col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        กรกนก นามสกุล
                                    </div>
                                    <div class="col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        88/8 Lorem ipsum dolor sit amet, consectetur elit, sed
                                        do eiusmod tempor incididu 10090
                                    </div>
                                    <div class="col-sm-12 col-xl-7" style="text-align: left; padding:10px">
                                        email@gmail.com <br>
                                        088-888-8888
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="text-align: center; padding-bottom: 40px;">
                            <button class="btn3 mt-3" style="width: 252px; height: 45px; background-color: #D3D3D3; border: 0px; border-radius: 23px;">
                                บันทึก
                            </button>
                        </div>


                    </div>




                </div>
            </div>
        </div>

    </div>

</body>
