<?php include('header.php') ?>

<style>
    .styleform {
        width: auto;
        margin: auto;
        padding: 30px 0;
        border-radius: 5px;
        border: 1px solid #BABABA;
        padding: 20px;
    }

    .headform {
        text-align: center;
    }

    input[type=text],
    select,
    textarea {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        /* box-sizing: border-box; */
        margin-top: 6px;
        margin-bottom: 16px;
        resize: vertical;
    }

    input[type=tel],
    select,
    textarea {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        /* box-sizing: border-box; */
        margin-top: 6px;
        margin-bottom: 16px;
        resize: vertical;
    }
</style>

<body>


    <div class="contact">
        <div class="container">
            <div class="row pt-5">
                <div class="col-sm-6 col-xl-6" style="padding-left: 7rem;">
                    <div class="contact-us mt-5">
                        <h2 style="padding-top: 40px;"><b><?php echo getWording('contact', 'contact') ?></b></h2>
                    </div>
                    <div class="col-xl-6 address pt-6">
                        <p><?php echo getVariable($contactus_list, 'address') ?></p>
                    </div>
                    <div class="time pt-6">

                        <p><?php echo getWording('contact', 'time') ?> : <?php echo getVariable($contactus_list, 'work_time') ?></p>

                        <p><?php echo getWording('contact', 'tel') ?> : <?php echo $contactus_list->telephone ?></p>

                        <p><?php echo getWording('contact', 'email') ?> : <?php echo $contactus_list->email ?></p>

                        <p><?php echo getWording('contact', 'line') ?> : <?php echo $contactus_list->line ?></p>

                        <p>Twitter : <?php echo $contactus_list->twitter ?></p>

                        <!-- <p>Facebook : <?php // echo $contactus_list->facebook 
                                            ?></p>
                        <p>Instagram : <?php // echo $contactus_list->instagram 
                                        ?></p> -->
                    </div>
                    <div class="col-12" style="padding-top:20px;">
                        <?php echo $contactus_list->map ?>
                        <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.5268396558804!2d100.56043161477913!3d13.747072190350508!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29e4db039425d%3A0xc9442fce68cb1a5d!2z4Lia4Lij4Li04Lip4Lix4LiXIOC4quC4oeC4suC4o-C5jOC4l-C4hOC4peC4tOC5iuC4geC5guC4i-C4peC4ueC4iuC4seC5iOC4mSDguIjguLPguIHguLHguJQ!5e0!3m2!1sth!2sth!4v1622523410426!5m2!1sth!2sth" width="100%" height="311" style="border:0;" allowfullscreen="" loading="lazy"></iframe> -->
                    </div>
                </div>
                <div class="col-sm-6 col-xl-6 pt-4">
                    <div class="styleform pt-4">
                        <div class="headform p-5">
                            <h2><b><?php echo getWording('contact', 'contact_form') ?></b></h2>
                        </div>
                        <form id="dataForm">
                            <?php
                            // echo '<pre>';
                            // print_r($contact_title);
                            // exit();
                            ?>
                            <div class="topic pt-2">
                                <b><?php echo getWording('contact', 'topic') ?>*</b>
                                <select id="topic" style="width: 100%; height: 45px;">
                                    <?php foreach ($contact_title as $key => $contact_list) { ?>
                                        <option value="<?php echo $contact_list->id ?>"><?php echo getVariable($contact_list, 'name') ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="name pt-2">
                                <b><?php echo getWording('contact', 'name') ?> *</b>
                                <input type="text" id="name" name="name" placeholder="<?php echo getWording('contact', 'name') ?>" required>
                            </div>

                            <div class="email pt-2">
                                <b><?php echo getWording('contact', 'email') ?>*</b>
                                <input type="text" id="email" name="email" placeholder="<?php echo getWording('contact', 'email') ?>" required>
                            </div>

                            <div class="telephone pt-2">
                                <b><?php echo getWording('contact', 'phone') ?>*</b>
                                <input type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" id="telephone" name="telephone" placeholder="<?php echo getWording('contact', 'phone') ?>" required>
                            </div>

                            <div class="company pt-2">
                                <b><?php echo getWording('contact', 'company') ?></b>
                                <input type="text" id="company" name="company" placeholder="<?php echo getWording('contact', 'company') ?>">
                            </div>

                            <div class="message pt-2">
                                <b><?php echo getWording('contact', 'message') ?></b>
                                <textarea id="message" name="message" placeholder="<?php echo getWording('contact', 'message') ?>" style="height:100px"></textarea>
                            </div>
                            <div class="submit pt-4" style=" text-align: center">
                                <input id="submit-contactus" type="submit" value="<?php echo getWording('contact', 'submit') ?>" style="width: 151px; height: 45px; background: #7c7c7c; color:#FFFFFF; border: 0px; border-radius: 23px; font-weight: bold;">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-content-style">
                <div class="modal-header modal-header-style">
                </div>
                <div class="modal-body" style="text-align: -webkit-center;">
                    <div class="btn-close-modal">
                        <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
                    </div>
                    <h3 style="text-align: center;" class="contact_response"></h3>
                    <button class="btn" type="button" data-bs-dismiss="modal" aria-label="Close" style="width: 142px;height: 40px; background-color: #AAAAAA; border-radius: 23px; padding: 10px; margin:10px;color:white;">Close</button>
                </div>
                <div class="modal-footer modal-footer-style register_response_btn">

                </div>
            </div>
        </div>
    </div>

    <div class="mt-5"> <?php include('footer.php') ?></div>

    <script src="<?php echo base_url('assets/js/validator.js') ?>"></script>

    <script>
        $(document).ready(function() {
            $('#telephone').mask('000-000-0000');
        });

        $("#dataForm").submit(function(e) {
            e.preventDefault();

            return false;
        });


        $("#submit-contactus").click(function() {

            $("#dataForm").validate({
                rules: {
                    name: "required",

                    email: {
                        required: true,
                        email: true
                    },
                    telephone: {
                        required: true,
                        pattern: '\\d{3}-\\d{3}-\\d{4}',
                        minlength: 12,
                        maxlength: 12
                    },
                    // company: "required",
                    // message: "required"
                },
                messages: {
                    name: "Please enter your name",
                    email: "Please enter a valid email address",
                    telephone: "Please correct telephone format",
                    // company: "Please enter your company",
                    // message: "Please enter your message",
                },

                errorPlacement: function(error, element) {
                    error.insertBefore(element);
                },
                submitHandler: function() {

                    var data = {
                        "name": $("#name").val(),
                        "email": $("#email").val(),
                        "telephone": $("#telephone").val(),
                        "company": $("#company").val(),
                        "message": $("#message").val(),
                        "topic": $("#topic").val()
                    }

                    var send_data = {
                        "url": "<?php echo base_url('Contact') ?>",
                        "method": "POST",
                        "data": data
                    }

                    $.ajax(send_data).done(function(response) {
                        if (response.code == "0x0000-00000") {
                            $('.contact_success').addClass('d-none');
                            $('.contact_success').removeClass('d-block');
                            $('.contact_fail').addClass('d-block');
                            $('.contact_fail').removeClass('d-none');
                            $(".contact_response").text('บริษัทได้รับข้อความของท่านแล้ว จะติดต่อกลับโดยเร็วที่สุด ขอบคุณที่เลือก Nextere ค่ะ');
                            $('#staticBackdrop').modal('show');
                        } else {
                            $('.contact_success').addClass('d-none');
                            // $('#staticBackdrop').modal('show');
                            $('.contact_success').removeClass('d-block');
                            $('.contact_fail').addClass('d-block');
                            $('.contact_fail').removeClass('d-none');
                            $(".contact_response").text(response.message);
                            $('#staticBackdrop').modal('show');
                        }
                    });
                }
            });


        });
    </script>
</body>