<?php include('header.php') ?>
<link href="<?php echo base_url('/assets/css/style-content.css') ?>" rel="stylesheet">
<style>
    .quantity-field {
        width: 200px;
        height: 40px;

    }

    .quantity-field .value-button {
        border: 1px solid #ddd;
        margin: 0px;
        width: 40px;
        height: 100%;
        padding: 0;
        outline: none;
        cursor: pointer;
    }

    .quantity-field .value-button:hover {
        background: rgb(230, 230, 230);
    }

    .quantity-field .value-button:active {
        background: rgb(230, 230, 230);
    }

    .quantity-field .decrease-button {
        margin-right: -4px;
    }

    .quantity-field .increase-button {
        margin-left: -4px;
    }

    .quantity-field .number {
        display: inline-block;
        text-align: center;
        border: none;
        border-top: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
        margin: 0px;
        width: 80px;
        height: 100%;
        line-height: 40px;
        font-size: 11pt;
        box-sizing: border-box;
        background: white;
        font-family: calibri;
    }

    .quantity-field .number::selection {
        background: none;
    }

    .nav-tabs .nav-link {
        color: black;
        margin-bottom: -1px;
        background: 0 0;
        border-top: none;
        border-left: none;
        border-right: none;
        border-bottom: 1px solid transparent;
        ;
        /* border: 1px solid transparent; */
        /* border-bottom-left-radius: .25rem;
        border-top-right-radius: .25rem; */
    }

    .nav-tabs .nav-item.show .nav-link,
    .nav-tabs .nav-link.active {
        border-bottom: 1px solid transparent;
        color: #495057;
        background-color: #fff;
        border-color: #dee2e6 #dee2e6 #dee2e6;
    }

    .review {
        background-color: lightgray;
    }

    .review .active {
        background-color: lightgray !important;
        border-bottom: revert !important;
    }


    /*
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
*/
</style>
<?php
// echo '<pre>';
// print_r($product_list->{'product::product_reviews'});
// exit();
?>

<body>
    <?php if (!empty($product_list)) { ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12 pt-5 pb-2" style="text-align:left;">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <?php if ($breadcrumbs_search == "") { ?>
                                <li class="breadcrumb-item"><a href="<?php echo base_url('') ?>"><?php echo getWording('index', 'home') ?> </a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url('Product?type=all_product') ?>"><?php echo getWording('index', 'our_products') ?></a></li>
                                <?php if ($product_list->{'product_detail_categorys'} != null) { ?>
                                    <li class="breadcrumb-item"><a href="<?php echo base_url('Product/index/' . $product_list->{'product_detail_categorys'}[0]->{'product_detail_category::product_category_id'} . '?type=category') ?>"><?php echo getVariable($product_list->{'product_detail_categorys'}[0], 'product_category::name')  ?></a></li>
                                <?php } else { ?>
                                <?php } ?>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo getVariable($product_list, 'product::name')  ?></li>
                            <?php } else { ?>
                                <li class="breadcrumb-item"><a href="<?php echo base_url('') ?>"><?php echo getWording('index', 'home') ?> </a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url('Solution/detail/' . $breadcrumbs_search->{'menu_category_id'} . '?type=solution') ?>"><?php echo getVariable($breadcrumbs_search->{'menu_category'}, 'name') ?></a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url('Solution/detail/' . $breadcrumbs_search->{'submenu_category_id'} . '?type=sub_solution' . '?type=child_category&submenu_id=' . $solution_sub->id . '&child_id=' . $child_submenu->id) ?>"><?php echo getVariable($breadcrumbs_search->{'submenu_category'}, 'name') ?></a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url('Solution/soluton_product_list/' . $breadcrumbs_search->{'child_submenu_category_id'} . '?type=child_category&submenu_id=' . $solution_sub->id . '&child_id=' . $child_submenu->id) ?>"><?php echo getVariable($breadcrumbs_search->{'child_submenu_category'}, 'name') ?></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo getVariable($product_list, 'product::name')  ?></li>
                            <?php } ?>
                        </ol>


                    </nav>
                </div>
                <div class="col-sm-12 col-lg-6 ">
                    <div class="row">
                        <div class="column small-11 small-centered">
                            <div class="slider slider-single " id="container">
                                <div style="border: 1px solid #E5E5E5;text-align:-webkit-center; background-color: #fff;">
                                    <?php if (!empty($product_list->{'product::images'}[0])) { ?>
                                        <img class="zoom w-75" src="<?php echo $product_list->{'product::images'}[0] ?>" width="90%">
                                    <?php } else { ?>
                                        <img class="zoom w-75" src="<?php echo base_url('/assets/img/no-images.png') ?>" alt="">
                                        </a>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="slider slider-nav mt-3">
                                <div style="border: 1px solid #E5E5E5;text-align:-webkit-center; background-color: #fff;">
                                    <?php if (!empty($product_list->{'product::images'}[0])) { ?>
                                        <img src="<?php echo $product_list->{'product::images'}[0] ?>" height="100px" width="100px">
                                    <?php } else { ?>
                                        <img src="<?php echo base_url('/assets/img/no-images.png') ?>" alt="" height="100px">
                                        </a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6 " style="text-align:left;">
                    <span><b style="color:#8B8B8B">Brand <?php echo getVariable($product_list, 'brand::name') ?></b></span>
                    <h3><b><?php echo getVariable($product_list, 'product::name')  ?></b></h3>
                    <p><?php echo getVariable($product_list, 'product::detail') ?>
                    </p>
                    <p><?php echo getWording('product', 'model') ?> : <?php echo getVariable($product_list, 'product::model_name') ?></p>
                    <p><?php echo getWording('product', 'category') ?> :
                        <?php foreach ($product_list->{'product_detail_categorys'} as $key => $product_categorys) { ?>
                            <?php echo getVariable($product_categorys, 'product_category::name') ?> /
                        <?php } ?>
                    </p>
                    <?php if ($product_list->{'product::retail_price'} != null) { ?>
                        <?php if (empty($product_list->{'product::discount'})) { ?>
                            <h4>
                                <b><?php echo number_format($product_list->{'product::retail_price'}) ?> <?php echo getWording('index', 'baht') ?></b>
                            </h4>
                        <?php } else { ?>
                            <h4>
                                <div class="row">
                                    <span class="col-3">
                                        <b class="strikethrough"><?php echo number_format($product_list->{'product::retail_price'}) ?> <?php echo getWording('index', 'baht') ?></b>
                                    </span>
                                    <span class="col-3">
                                        <?php if ($product_list->{'product::discount'}->total_price > "0") { ?>
                                            <b style="color: red;"><?php echo number_format($product_list->{'product::discount'}->total_price) ?> <?php echo getWording('index', 'baht') ?></b>
                                        <?php } else { ?>
                                            <b style="color: red;"> 0 <?php echo getWording('index', 'baht') ?></b>
                                        <?php } ?>
                                    </span>
                                </div>
                            </h4>
                        <?php } ?>

                        <hr style="color:#8B8B8B">
                        <div class="quantity-field d-flex align-content-start">
                            <sapn style="align-self: center; padding-right: 10px;"><?php echo getWording('product', 'piece') ?></sapn>
                            <button class="value-button decrease-button" onclick="decreaseValue(this)"><i class="fas fa-minus"></i></button>
                            <div class="number">1</div>
                            <button class="value-button increase-button" onclick="increaseValue(this, 10)"><i class="fas fa-plus"></i></button>
                        </div>
                        <p class="pt-5"><?php echo getWording('product', 'conditions') ?> : <?php echo getVariable($product_list, 'product::condition_product') ?></p>
                        <br>
                        <button type="button" class="btn disabled" style="opacity: 1; width: 150px; height:60px; background-color: #F0F0F0;border-radius: 0px;margin-right: 20px;">
                            <span><?php echo getWording('product', 'min') ?></span>
                            <h4><?php echo $product_list->{'product::minimum_product'} ?> <?php echo getWording('product', 'piece') ?></h4>
                        </button>
                        <button type="button" class="btn disabled" style="opacity: 1; width: 150px; height:60px; background-color: #F0F0F0;border-radius: 0px;margin-right: 20px;">
                            <span><?php echo getWording('product', 'stock') ?></span>
                            <h4><?php echo $product_list->{'product::in_stock_qty'} ?> <?php echo getWording('product', 'piece') ?></h4>
                        </button>
                        <button type="button" class="btn disabled" style="opacity: 1; width: 150px; height:60px; background-color: #F0F0F0;border-radius: 0px;margin-right: 20px;">
                            <span><?php echo getWording('product', 'warranty') ?></span>
                            <h4><?php echo getVariable($product_list, 'product::condition_warranty') ?> <?php echo getWording('profile', 'year') ?></h4>
                        </button>
                        <br>
                        <?php if ($token != '') { ?>
                            <a class="btn order_btn  mt-3" data-quantity="" data-product-qty="<?php echo $product_list->{'product::in_stock_qty'} ?>" data-product-id="<?php echo $product_list->{'product::id'} ?>" style="width: 220px; height: 40px; padding-top:11px;background-color: #7C7C7C;color: white;"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php echo base_url('/assets/img/icon/shopping-cart-2.png') ?>"><?php echo getWording('product', 'place_order') ?> </a>
                        <?php } else { ?>
                            <a href="<?php echo base_url('Account/login') ?>" class="btn  mt-3" style="width: 220px; height: 40px; padding-top:11px;background-color: #7C7C7C;color: white;"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php echo base_url('/assets/img/icon/shopping-cart-2.png') ?>"><?php echo getWording('product', 'place_order') ?> </a>
                        <?php } ?>
                    <?php } else { ?>
                        <h5>
                            <b><?php echo getWording('product', 'interested') ?></b>
                        </h5>
                        <a href="<?php echo base_url('Contact') ?>" class="btn  mt-3" style="width: 220px; height: 40px; padding-top:11px;background-color: #7C7C7C;color: white;"><?php echo getWording('index', 'contact') ?></a>
                    <?php } ?>
                </div>
            </div>
            <div class="col-sm-12 col-lg-12 pt-5 pb-5" style="text-align:left;color:black;">
                <ul class="nav nav-tabs " id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="Description-tab" data-bs-toggle="tab" data-bs-target="#Description" type="button" role="tab" aria-controls="Description" aria-selected="true"><?php echo getWording('product', 'description') ?></button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="Product-Feature-tab" data-bs-toggle="tab" data-bs-target="#Product-Feature" type="button" role="tab" aria-controls="Product Feature" aria-selected="false"><?php echo getWording('product', 'feature') ?></button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="Technical-Specification-tab" data-bs-toggle="tab" data-bs-target="#Technical-Specification" type="button" role="tab" aria-controls="Technical-Specification" aria-selected="false"><?php echo getWording('product', 'technical') ?></button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="Tutorial-tab" data-bs-toggle="tab" data-bs-target="#Tutorial" type="button" role="tab" aria-controls="Tutorial" aria-selected="false"><?php echo getWording('product', 'tutorial') ?></button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="Review-tab" data-bs-toggle="tab" data-bs-target="#Review" type="button" role="tab" aria-controls="Review" aria-selected="false"><?php echo getWording('product', 'review') ?></button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="Datasheet-tab" data-bs-toggle="tab" data-bs-target="#Datasheet" type="button" role="tab" aria-controls="Datasheet" aria-selected="false"><?php echo getWording('product', 'datasheet') ?></button>
                    </li>
                </ul>
            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active p-3" id="Description" role="tabpanel" aria-labelledby="Description-tab">
                    <?php echo getVariable($product_list, 'product::description') ?>
                </div>
                <div class="tab-pane fade p-3" id="Product-Feature" role="tabpanel" aria-labelledby="Product-Feature-tab">
                    <div class="row justify-content-center">
                        <?php
                        function comparator_9($a, $b)
                        {
                            return $a->sort < $b->sort ? -1 : 1;
                        };
                        usort($product_list->{'product::product_features'}, "comparator_9");
                        ?>
                        <?php foreach ($product_list->{'product::product_features'} as $key => $product_features) { ?>
                            <div class="col-3" style="text-align: -webkit-center;">
                                <div class="border-images w-75">
                                    <?php if (!empty($product_features->image_path)) { ?>
                                        <img class="w-100" src="<?php echo base_url($product_features->image_path) ?>" width="90%">
                                    <?php } else { ?>
                                        <img class="w-100" src="<?php echo base_url('/assets/img/no-images.png') ?>" alt="">
                                        </a>
                                    <?php } ?>
                                </div>
                                <h4 class="mt-3 mb-2"><?php echo getVariable($product_features, 'title') ?></h4>
                                <p><?php echo getVariable($product_features, 'description') ?></p>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="tab-pane fade p-3" id="Technical-Specification" role="tabpanel" aria-labelledby="Technical-Specification-tab">
                    <table class="table table-striped table-bordered table-hover order-column">
                        <thead>
                            <tr>
                                <th style="width: 30%">Technical Spec</th>
                                <th style="width: 35%">Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($product_list->{'product_detail_categorys'}[0]->{'product::technical_spec'} as $key => $categorys_details) { ?>
                                <tr>
                                    <td>
                                        <?php if (!empty($categorys_details->technical_spec->name_th)) { ?>
                                            <?php echo getVariable($categorys_details->technical_spec, 'name') ?>
                                        <?php } else { ?>
                                            <h3 style="padding-left: 10px;"><?php echo ('-') ?></h3>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if (!empty($categorys_details->value_spec->value_th)) { ?>
                                            <?php echo getVariable($categorys_details->value_spec, 'value') ?>
                                        <?php } else { ?>
                                            <h3 style="padding-left: 10px;"><?php echo ('-') ?></h3>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade p-3" id="Tutorial" role="tabpanel" aria-labelledby="Tutorial-tab">
                    <div class="row">
                        <?php
                        function comparator_7($a, $b)
                        {
                            return $a->sort < $b->sort ? -1 : 1;
                        };
                        usort($product_list->{'product::product_tutorials'}, "comparator_7");
                        ?>
                        <?php foreach ($product_list->{'product::product_tutorials'} as $key => $product_tutorials) { ?>
                            <div class="col-8">
                                <h4><?php echo getVariable($product_tutorials, 'title') ?></h4>
                                <hr>
                                <div>
                                    <?php echo ($product_tutorials->{'link_url'}) ?>
                                </div>

                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="tab-pane fade p-3" id="Review" role="tabpanel" aria-labelledby="Review-tab">
                    <?php if (!empty($product_list->{'product::product_reviews'})) { ?>
                        <div class="col-12">
                            <div class="2">

                                <h3>
                                    <?php
                                    $average_reviews_sumaray = 0;
                                    foreach ($product_list->{'product::product_reviews'} as $key => $average_reviews) {
                                        $average_reviews_sumaray +=  $average_reviews->rating;
                                    }
                                    $average_reviews_total = $average_reviews_sumaray / count($product_list->{'product::product_reviews'});
                                    ?>
                                    <?php echo number_format($average_reviews_total, 1)   ?> / 5

                                </h3>
                                <span>( <?php echo count($product_list->{'product::product_reviews'}) ?> Review )</span>
                            </div>

                        </div>
                        <div class="col-12 mt-3">
                            <ul class="nav nav-tabs review" id="myTab" role="tablist">
                                <li class="nav-item " role="presentation">
                                    <button class="nav-link review active " id="review-tab" data-bs-toggle="tab" data-bs-target="#review" type="button" role="tab" aria-controls="review" aria-selected="true"><?php echo getWording('product', 'review_all') ?> ( <?php echo count($product_list->{'product::product_reviews'}) ?> )</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link review " id="review_2-tab" data-bs-toggle="tab" data-bs-target="#review_2" type="button" role="tab" aria-controls="review_2" aria-selected="false">
                                        <?php echo getWording('product', 'review_image') ?>
                                        (
                                        <?php
                                        $reviews_images = 0;
                                        foreach ($product_list->{'product::product_reviews'} as $key => $product_reviews) {
                                            $reviews_images += count($product_reviews->images);
                                        } ?>
                                        <?php echo  $reviews_images ?> ) </button>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active p-3" id="review" role="tabpanel" aria-labelledby="review-tab">
                                    <?php if (!empty($product_list->{'product::product_reviews'})) { ?>
                                        <?php foreach ($product_list->{'product::product_reviews'} as $key => $product_reviews) { ?>
                                            <div class="col-6  ">
                                                <!-- <h5 class="mt-3"><b><?php echo $product_reviews->message ?></b></h5> -->
                                                <p><?php echo $product_reviews->message ?></p>
                                                <?php if (!empty($product_reviews->images)) { ?>
                                                    <?php foreach ($product_reviews->images as $key => $product_reviews_image) { ?>
                                                        <img class="w-25 mb-3" src="<?php echo base_url($product_reviews_image->image_path) ?>" alt="">
                                                    <?php } ?>
                                                <?php } else { ?>
                                                    <img class="w-25 mb-3" src="<?php echo base_url('/assets/img/no-images.png') ?>" alt="">
                                                <?php } ?>
                                                <hr>
                                            </div>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <h4 style="text-align: center;"><?php echo getWording('product', 'no_review') ?></h4>
                                    <?php } ?>
                                </div>
                                <div class="tab-pane fade show p-3" id="review_2" role="tabpanel" aria-labelledby="review_2-tab">
                                    <?php if (!empty($product_list->{'product::product_reviews'}[0]->images)) { ?>
                                        <?php foreach ($product_list->{'product::product_reviews'} as $key => $product_reviews) { ?>
                                            <div class="col-6  ">
                                                <p><?php echo $product_reviews->message ?></p>
                                                <?php if (!empty($product_reviews->images)) { ?>
                                                    <?php foreach ($product_reviews->images as $key => $product_reviews_image) { ?>
                                                        <img class="w-25 mb-3" src="<?php echo base_url($product_reviews_image->image_path) ?>" alt="">
                                                    <?php } ?>
                                                <?php } else { ?>
                                                    <img class="w-25 mb-3" src="<?php echo base_url('/assets/img/no-images.png') ?>" alt="">
                                                <?php } ?>
                                                <hr>
                                            </div>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <h4 style="text-align: center;"><?php echo getWording('product', 'no_review') ?></h4>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <h4 style="text-align: center;"><?php echo getWording('product', 'no_review') ?></h4>
                    <?php } ?>

                </div>
                <div class="tab-pane fade p-3" id="Datasheet" role="tabpanel" aria-labelledby="Datasheet-tab">
                    <table class="table table-striped">
                        <tbody>
                            <?php
                            function comparator_6($a, $b)
                            {
                                return $a->sort < $b->sort ? -1 : 1;
                            };
                            usort($product_list->{'product::product_datasheets'}, "comparator_6");
                            ?>
                            <?php foreach ($product_list->{'product::product_datasheets'} as $key => $datatsheets) { ?>
                                <tr>
                                    <td><?php echo ($datatsheets->{'name'}) ?></td>
                                    <td style="text-align:center"><a href="<?php echo base_url($datatsheets->{'file_path'}) ?>" target="_blank"><img class="mr-3" style="width: 16px; height: 14px;" src="<?php echo base_url('/assets/img/icon/Download.png') ?>"></i> <?php echo getWording('profile', 'download') ?></a></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>



        <?php if (empty($product_limit)) { ?>
            <?php echo null ?>
        <?php } else { ?>
            <div style="background-color: #F0F0F0">
                <div class="container">
                    <div class="row pt-5 pb-5">
                        <div class="col-sm-12 col-lg-12" style="text-align: center;">
                            <h4><b><?php echo getWording('product', 'related') ?></b> </h4>
                        </div>
                        <div class="cardplay">
                            <?php foreach ($product_limit as $key => $product_item) { ?>
                                <div class="col-sm-6 col-xl-4 mt-4 item" style="text-align:left">
                                    <div class="product-item">
                                        <div class="border-images mb-3">
                                            <a href="<?php echo base_url('/Product/Details/' . $product_item->{'product::id'}) ?>">
                                                <?php if (property_exists($product_item, 'product::images')) { ?>
                                                    <img class="w-100" src="<?php echo $product_item->{'product::images'}[0] ?>" alt="">
                                                <?php } else { ?>
                                                    <img class="w-100" src="<?php echo base_url('/assets/img/no-images.png') ?>" alt="">
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <span><?php echo getVariable($product_item, 'brand::name') ?></span>
                                        <h4><?php echo getVariable($product_item, 'product::name') ?></h4>
                                        <?php if ($product_item->{'product::retail_price'} != null) { ?>
                                            <?php if (empty($product_item->{'product::discount'})) { ?>
                                                <h4 class="mt-6">
                                                    <?php echo number_format($product_item->{'product::retail_price'}); ?>
                                                    <?php echo getWording('index', 'baht') ?>
                                                </h4>
                                            <?php } else { ?>
                                                <h4 class="mt-1">
                                                    <div class="row">
                                                        <span class="col-6">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <span class="discount-product">- <?php echo ($product_item->{'product::discount'}->discount_percent * 100); ?>%</span>
                                                                </div>
                                                                <span class="strikethrough">
                                                                    <?php echo number_format($product_item->{'product::retail_price'}); ?>
                                                                    <?php echo getWording('index', 'baht') ?>
                                                                </span>
                                                            </div>
                                                        </span>
                                                        <span class="col-6 mt-5" style="color: red;">
                                                            <?php echo number_format($product_item->{'product::discount'}->total_price); ?>
                                                            <?php echo getWording('index', 'baht') ?>
                                                        </span>
                                                    </div>
                                                </h4>
                                            <?php } ?>
                                            <div class="row mt-5">
                                                <?php if ($token != '') { ?>
                                                    <div class="col-8 pr-1">
                                                        <button class="order_btn w-100" data-quantity="1" data-product-id="<?php echo $product_item->{'product::id'} ?>"><?php echo getWording('index', 'buy') ?></button>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="col-8 pr-1">
                                                        <a href="<?php echo base_url('Account/login') ?>"><button class="w-100" ? style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"><?php echo getWording('index', 'buy') ?></button></a>
                                                    </div>
                                                <?php } ?>

                                                <div class="col-4 pl-1">
                                                    <button id="test-btn" type="button" class="filter_btn w-100 p-0" data-compare="<?php echo $product_item->{'product::id'} ?>">
                                                        <svg class="w-100">
                                                            <use xlink:href="#icon-filter"></use>
                                                        </svg>
                                                    </button>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <h5 class="mt-6">
                                                <b><?php echo getWording('index', 'interested_product') ?></b>
                                            </h5>
                                            <div class="row mt-5">
                                                <div class="col-8 pr-1">
                                                    <a href="<?php echo base_url('Contact') ?>"><button class="w-100" ? style="border: none; background-color: #9F9F9F;color: #fff;border-radius: 30px;height: 100%;font-size: 1rem;padding: 0.8rem 0;"><?php echo getWording('index', 'contact') ?></button></a>
                                                </div>
                                                <div class="col-4 pl-1">
                                                    <button id="test-btn" type="button" class="filter_btn w-100 p-0" data-compare="<?php echo $product_item->{'product::id'} ?>">
                                                        <svg class="w-100">
                                                            <use xlink:href="#icon-filter"></use>
                                                        </svg>
                                                    </button>
                                                </div>

                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php } else { ?>
        <?php header("Location: https://www.nextere.space/Product?type=all_product",);
        exit(); ?>
    <?php } ?>
    <?php include('footer.php') ?>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->

    <!-- <script src="../assets/vender/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> -->
    <script type="text/javascript">
        $(document).ready(function() {
            console.log(document.referrer);
            $('.cardplay').slick({
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                arrows: true,
                autoplay: true,
                autoplaySpeed: 2000,
                prevArrow: "<i class='slick-prev fas fa-chevron-left'></i>",
                nextArrow: "<i class='slick-next fas fa-chevron-right'></i>",
                responsive: [{
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            arrows: true,
                        }
                    },
                    {
                        breakpoint: 1000,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            arrows: true,
                        }
                    },
                    {
                        breakpoint: 700,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: true,
                        }
                    },
                ]
            });
        });

        $('.slider-single').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: false,
            adaptiveHeight: true,
            infinite: false,
            useTransform: true,


        });
        $('.slider-nav')
            .on('init', function(event, slick) {
                $('.slider-nav .slick-slide.slick-current').addClass('is-active');
            })
            .slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                dots: false,
                focusOnSelect: false,
                infinite: false,
            });

        $('.slider-single').on('afterChange', function(event, slick, currentSlide) {
            $('.slider-nav').slick('slickGoTo', currentSlide);
            var currrentNavSlideElem = '.slider-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
            $('.slider-nav .slick-slide.is-active').removeClass('is-active');
            $(currrentNavSlideElem).addClass('is-active');
        });
        $('.slider-nav').on('click', '.slick-slide', function(event) {
            event.preventDefault();
            var goToSingleSlide = $(this).data('slick-index');

            $('.slider-single').slick('slickGoTo', goToSingleSlide);
        });
    </script>
    <script>
        const img = document.querySelector('.zoom')

        container.addEventListener('mousemove', (e) => {
            const x = e.clientX - e.target.offsetLeft
            const y = e.clientY - e.target.offsetTop


            img.style.transformOrigin = `${x}px ${y}px`
            img.style.transform = 'scale(2)'

        })

        container.addEventListener('mouseleave', (e) => {
            img.style.transform = " scale(1)"
        })
    </script>
    <script>
        function increaseValue(button, limit) {
            const numberInput = button.parentElement.querySelector('.number');
            var value = parseInt(numberInput.innerHTML, 10);
            if (isNaN(value)) value = 1;
            if (limit && value >= limit) {
                $('.order_btn').data('quantity', limit);
                return;

            } else {
                $('.order_btn').data('quantity', value + 1);
                numberInput.innerHTML = value + 1;
            }
        }


        function decreaseValue(button) {
            const numberInput = button.parentElement.querySelector('.number');
            var value = parseInt(numberInput.innerHTML, 10);
            if (isNaN(value)) value = 1;
            if (value < 2) {
                $('.order_btn').data('quantity', value);
                return;

            } else {
                $('.order_btn').data('quantity', value - 1);
                numberInput.innerHTML = value - 1;
            }
        }
    </script>
</body>

</html>