<?php include('header.php') ?>

<style>
    input[type=text],
    select,
    textarea {
        padding: 12px;
        margin-top: 6px;
        margin-bottom: 16px;
        width: 100%;
        height: 45px;
        background: #FFFFFF;
        border: 1px solid #707070;
        border-radius: 5px;
    }

    input[type=tel],
    select,
    textarea {
        padding: 12px;
        margin-top: 6px;
        margin-bottom: 16px;
        width: 100%;
        height: 45px;
        background: #FFFFFF;
        border: 1px solid #707070;
        border-radius: 5px;
    }

    input[type=submit] {
        width: 179px;
        height: 45px;
        background: #7C7C7C;
        color: white;
        border-radius: 23px;
        border: 0px;
    }
</style>
<?php
$product_total_retail_price = 0;
$product_total_delivery_price = 0;
$product_total_price_all = 0;
if ($product_list['product_id'] == null) {
    redirect(base_url('Product/shopping-cart'));
} 

?>

<div class="container" style="text-align: -webkit-center;">
    <div class="col-sm-6 col-xl-8">
        <div class="head24" style="text-align: center; padding-top: 69px;">
            <h2><?php echo getWording('quotation', 'quotation') ?></h2>
        </div>
        <!-- <form> -->
        <hr style="border-width: 3px;">
        <div class="radio-page24" style="text-align: left;">
            <div style="display: flex;">
                <!-- <a href="http://localhost/nextere2/QuotationInformation"> -->
                <label class="col-sm-12 col-xl-2 m-4">
                    <input type="radio" class="quotation_type personal" name="type" checked="true"> <?php echo getWording('quotation', 'personal') ?>
                </label>
                <!-- </a> -->
                <!-- <a href="QuotationInformation/entity"> -->
                <label class="col-sm-12 col-xl-2 mt-4">
                    <input type="radio" class="quotation_type corporation" name="type"> <?php echo getWording('quotation', 'corporation') ?>
                </label>
                <!-- </a> -->
            </div>
        </div>
        <form id="dataForm">
            <div class="row" id="personal_form">
                <div class="col-xl-6 mt-3" style="text-align:left;">
                    <b><?php echo getWording('profile', 'name') ?>*</b>
                    <input type="text" id="name" name="name" placeholder="<?php echo getWording('profile', 'name') ?>" required>
                </div>

                <div class="col-xl-6 mt-3" style="text-align:left;">
                    <b><?php echo getWording('profile', 'lastname') ?>*</b>
                    <input type="text" id="lastname" name="lastname" placeholder="<?php echo getWording('profile', 'lastname') ?>" required>
                </div>

                <div class="col-xl-6 mt-2" style="text-align:left;">
                    <b><?php echo getWording('profile', 'phone') ?>*</b>
                    <input type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" id="telephone" name="telephone" placeholder="<?php echo getWording('profile', 'phone') ?>" required>
                </div>

                <div class="col-xl-6 mt-2" style="text-align:left;">
                    <b><?php echo getWording('profile', 'email') ?>*</b>
                    <input type="text" id="email" name="email" placeholder="<?php echo getWording('profile', 'email') ?>" required>
                </div>

                <div class="col-xl-12 mt-2" style="text-align:left;">
                    <b><?php echo getWording('contact', 'message') ?>*</b>
                    <textarea id="message" name="message" placeholder="<?php echo getWording('contact', 'message') ?>.." style="width: 100%; height: 177px;" required></textarea>
                </div>
            </div>
            <?php // foreach ($product_cart as $key => $product_cart_list) { 
            ?>
            <?php
            // $retail_price = $product_cart_list->{'product::retail_price'};
            // $product_check = json_decode(file_get_contents(base_url() . 'Product/details/' . $product_cart_list->{'product::id'} . '/check'));
            // if (count($product_check->{'product::product_price'}) > 0) {
            //     foreach ($product_check->{'product::product_price'} as $key => $value) {
            //         if ($product_cart_list->{'cart::qty'} > $value->minimum_qty) {
            //             $retail_price = $value->price;
            //         }
            //     }
            // }
            ?>

            <div>
                <?php foreach ($product_list['product_id'] as $key => $product_id) { ?>
                    <input class="product_id" id="product_id" name="product_id" value="<?php echo $product_id ?>" type="hidden">
                <?php } ?>
            </div>
            <div>
                <?php foreach ($product_list['product_qty'] as $key => $product_qty) { ?>
                    <input class="quantity" id="quantity" name="quantity" value="<?php echo $product_qty ?>" type="hidden">
                <?php } ?>
            </div>
            <div>
                <?php foreach ($product_list['real_price'] as $key => $real_price) { ?>
                    <input class="real_price" id="real_price" name="real_price" value="<?php echo $real_price ?>" type="hidden">
                <?php } ?>
            </div>
            <div>
                <?php foreach ($product_list['product_retail_price'] as $key => $product_retail_price) { ?>
                    <input class="price_per_unit" id="price_per_unit" name="price_per_unit" value="<?php echo $product_retail_price ?>" type="hidden">
                <?php } ?>
            </div>
            <div>
                <?php foreach ($product_list['product_total_price'] as $key => $product_total_price) { ?>
                    <input class="total_price" id="total_price" name="total_price" value="<?php echo $product_total_price ?>" type="hidden">
                <?php } ?>
            </div>

            <div class="row" id="corporation_form" style="display: none;">
                <div class="col-xl-6 mt-3" style="text-align:left;">
                    <b><?php echo getWording('profile', 'name') ?>*</b>
                    <input type="text" id="company_name" name="company_name" placeholder="<?php echo getWording('profile', 'name') ?>" required>

                </div>

                <div class="col-xl-6 mt-3" style="text-align:left;">
                    <b><?php echo getWording('profile', 'lastname') ?>*</b>
                    <input type="text" id="company_lastname" name="company_lastname" placeholder="<?php echo getWording('profile', 'lastname') ?>" required>

                </div>

                <div class="col-xl-6 mt-2" style="text-align:left;">
                    <b><?php echo getWording('profile', 'phone') ?>*</b>
                    <input type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" id="company_telephone" name="company_telephone" placeholder="<?php echo getWording('profile', 'phone') ?>" required>
                </div>

                <div class="col-xl-6 mt-2" style="text-align:left;">
                    <b><?php echo getWording('profile', 'email') ?>*</b>
                    <input type="text" id="company_email" name="company_email" placeholder="<?php echo getWording('profile', 'email') ?>" required>

                </div>

                <div class="col-xl-6 mt-2" style="text-align:left;">
                    <b><?php echo getWording('profile', 'company') ?>*</b>
                    <input type="text" id="company" name="company" placeholder="<?php echo getWording('profile', 'company') ?>" required>

                </div>

                <div class="col-xl-6 mt-2" style="text-align:left;">

                    <b><?php echo getWording('quotation', 'business') ?>*</b>
                    <select id="business_type" name="business_type" style=" height: 48px;">
                        <?php foreach ($business_type as $key => $business_list) { ?>
                            <option value="<?php echo $business_list->id ?>"><?php echo getVariable($business_list, 'name') ?></option>
                        <?php } ?>
                    </select>

                </div>

                <div class="col-xl-6 mt-2" style="text-align:left;">
                    <b><?php echo getWording('quotation', 'corporate') ?>*</b>
                    <input type="text" id="tax_id" name="tax_id" onKeyPress="return isNumber(event)" min="0" maxlength="13" placeholder="<?php echo getWording('quotation', 'corporate') ?>" required>

                </div>

                <div class="col-xl-12 mt-2" style="text-align:left;">
                    <b><?php echo getWording('contact', 'message') ?>*</b>
                    <textarea id="company_message" name="company_message" placeholder="<?php echo getWording('contact', 'message') ?>.." style="width:  100%; height: 177px;"></textarea>

                </div>
            </div>

            <div class="pt-4">
                <!-- <button class="btn3 mt-3" style="width: 179px;
                    height: 45px; background: #7C7C7C; border: 0px; border-radius: 23px; font-weight: bold; color:white;">
                        <b>
                            ขอใบเสนอราคา
                        </b>
                    </button> -->
                <div class="submit" style=" text-align: center">
                    <input id="submit-quotation" type="submit" value="<?php echo getWording('quotation', 'request') ?>" style="width: 179px;
                    height: 45px; background: #7C7C7C; border: 0px; border-radius: 23px; font-weight: bold; color:white;">
                </div>
            </div>

            <div class="pt-1" style="text-align: left; padding-top: 80px;">
                <p><?php echo getWording('quotation', 'ps') ?></p>
                <p>- <?php echo getWording('quotation', 'costs') ?></p>
                <p>- กำหนดยืนราคา 7 วัน นับจากวันที่ในใบเสนอราคา</p>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true" style="z-index: 1061;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h3 class="request_response"></h3>
            </div>
            <div class="modal-footer request_response_btn">
                <!-- <button type="button" class="btn btn-success register_success d-none" onclick="location.href='<?php // echo base_url() 
                                                                                                                    ?>'">Understood</button> -->
                <button type="button" class="btn btn-secondary request_fail d-none" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="mt-5">
    <?php include('footer.php') ?>
</div>

<script>
    // $("#dataForm").submit(function(e) {
    //     var product_id = $('input.product_id').map(function() {
    //         return this.value;
    //     }).get();
    //     console.log(product_id);

    //     var quantity = $('input.quantity').map(function() {
    //         return this.value;
    //     }).get();
    //     console.log(quantity);

    //     var price_per_unit = $('input.price_per_unit').map(function() {
    //         return this.value;
    //     }).get();
    //     console.log(price_per_unit);

    //     var total_price = $('input.total_price').map(function() {
    //         return this.value;
    //     }).get();
    //     console.log(total_price);
    // });

    function phoneMask() {
        var num = $(this).val().replace(/\D/g, '');
        $(this).val(num.substring(0, 3) + '-' + num.substring(3, 6) + '-' + num.substring(6, 10));
    }

    $(document).ready(function() {
        $('#telephone').mask('000-000-0000');
        $('#company_telephone').mask('000-000-0000');
    });

    $('.quotation_type').click(function() {
        if ($(this).is(':checked') && $(this).hasClass('personal')) {
            // console.log('personal');
            $('#corporation_form').hide();
            $('#personal_form').fadeIn(800);
        } else {
            // console.log('corporation');
            $('#personal_form').hide();
            $('#corporation_form').fadeIn(800);

        }

    });

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    $("#submit-quotation").click(function() {
        // console.log('test submit');
        // return false;

        var product_id = $('input.product_id').map(function() {
            return this.value;
        }).get();
        // console.log('id',product_id);

        var quantity = $('input.quantity').map(function() {
            return this.value;
        }).get();
        // console.log('qty',quantity);

        var real_price = $('input.real_price').map(function() {
            return this.value;
        }).get();
        // console.log('real',real_price);

        var price_per_unit = $('input.price_per_unit').map(function() {
            return this.value;
        }).get();
        // console.log('per unit',price_per_unit);

        var total_price = $('input.total_price').map(function() {
            return this.value;
        }).get();
        // console.log('total',total_price);

        if ($('.quotation_type.personal').is(':checked')) {
            // console.log('check')
            $("#dataForm").validate({
                rules: {
                    name: "required",
                    lastname: "required",

                    email: {
                        required: true,
                        email: true
                    },
                    telephone: {
                        required: true,
                        pattern: '\\d{3}-\\d{3}-\\d{4}',
                        minlength: 12,
                        maxlength: 12
                    },
                    message: "required"
                },
                messages: {
                    name: "Please enter your name",
                    lastname: "Please enter your lastname",
                    email: "Please enter a valid email address",
                    telephone: "Please correct telephone format",
                    message: "Please enter your message",
                },

                errorPlacement: function(error, element) {
                    error.insertBefore(element);
                },
                submitHandler: function() {
                    // console.log('validated');
                    var data = {
                        "name": $("#name").val(),
                        "lastname": $("#lastname").val(),
                        "email": $("#email").val(),
                        "telephone": $("#telephone").val(),
                        "message": $("#message").val(),
                        "product_id": product_id,
                        "quantity": quantity,
                        "price_per_unit": price_per_unit,
                        "real_price": real_price,
                        "total_price": total_price,
                        "type": '1',
                    }
                    // console.log(data);
                    var send_data = {
                        "url": "<?php echo base_url('Product/request_quotation') ?>",
                        "method": "POST",
                        "data": data
                    }
                    console.log(data);

                    $.ajax(send_data).done(function(response) {
                        console.log('submit quotation');
                        console.log(response);
                        if (response.code == "0x0000-00000") {
                            window.location.href = "<?php echo base_url('Product/bill_quotation') ?>/" + response.data.quotation_id;
                            $('.request_success').addClass('d-none');
                            $('.request_success').removeClass('d-block');
                            $('.request_fail').addClass('d-block');
                            $('.request_fail').removeClass('d-none');
                            $(".request_response").text(response.message);
                        } else {
                            $('.request_success').addClass('d-none');
                            // $('#staticBackdrop').modal('show');
                            $('.request_success').removeClass('d-block');
                            $('.request_fail').addClass('d-block');
                            $('.request_fail').removeClass('d-none');
                        }
                        $('#staticBackdrop').modal('show');
                    });
                }
            });
        } else {
            console.log('uncheck')
            $("#dataForm").validate({
                rules: {
                    company_name: "required",
                    company_lastname: "required",

                    company_email: {
                        required: true,
                        email: true
                    },
                    company_telephone: {
                        required: true,
                        pattern: '\\d{3}-\\d{3}-\\d{4}',
                        minlength: 12,
                        maxlength: 12
                    },
                    company: "required",
                    company_message: "required",
                    tax_id: "required"
                },
                messages: {
                    company_name: "Please enter your name",
                    company_lastname: "Please enter your lastname",
                    company_email: "Please enter a valid email address",
                    company_telephone: "Please correct telephone format",
                    company: "Please enter your company",
                    company_message: "Please enter your message",
                    tax_id: "Please enter your tax number"
                },
                errorPlacement: function(error, element) {
                    error.insertBefore(element);
                },
                submitHandler: function() {
                    // console.log('validated');
                    var data = {
                        "name": $("#company_name").val(),
                        "lastname": $("#company_lastname").val(),
                        "email": $("#company_email").val(),
                        "telephone": $("#company_telephone").val(),
                        "message": $("#company_message").val(),
                        "company": $("#company").val(),
                        "tax_id": $("#tax_id").val(),
                        "business_type": $("#business_type").val(),
                        "product_id": product_id,
                        "quantity": quantity,
                        "price_per_unit": price_per_unit,
                        "real_price": real_price,
                        "total_price": total_price,
                        "type": '2',
                    }
                    console.log(data);
                    var send_data = {
                        "url": "<?php echo base_url('Product/request_quotation') ?>",
                        "method": "POST",
                        "data": data
                    }

                    $.ajax(send_data).done(function(response) {
                        if (response.code == "0x0000-00000") {
                            console.log(response);
                            window.location.href = "<?php echo base_url('Product/bill_quotation') ?>/" + response.data.quotation_id;
                            $('.request_success').addClass('d-none');
                            $('.request_success').removeClass('d-block');
                            $('.request_fail').addClass('d-block');
                            $('.request_fail').removeClass('d-none');
                            $(".request_response").text(response.message);
                        } else {
                            $('.request_success').addClass('d-none');
                            // $('#staticBackdrop').modal('show');
                            $('.request_success').removeClass('d-block');
                            $('.request_fail').addClass('d-block');
                            $('.request_fail').removeClass('d-none');
                        }
                        $('#staticBackdrop').modal('show');
                    });
                }
            });

        }
    });
</script>