<?php include('header.php') ?>
<?php
// echo '<pre>';
// print_r($block_list);
// exit();
?>
<style>
    .article {
        position: absolute;
        color: white;
        text-align: left;
        bottom: 5%;
        left: 0;
        margin-left: 140px;

    }

    @media screen and (max-width: 400px) {
        .article {
            position: absolute;
            color: white;
            text-align: left;
            bottom: 5%;
            left: 0;
            width: 70%;
            margin-left: 60px;
        }

        .slick-slide img {
            width: 100% !important;
        }

        .slick-prev:before,
        .slick-next:before {
            color: white !important;
            font-size: 50% !important;

        }

        .slick-dots {
            padding-right: 40% !important;
            color: black !important;
            bottom: -25% !important;
        }

        .head_article {
            padding-top: 1.5rem !important;
        }


    }

    @media screen and (max-width: 768px) {
        .slick-slide img {
            width: 100% !important;
        }

        .slick-prev:before,
        .slick-next:before {
            color: white !important;
        }
    }

    @media screen and (max-width: 1024px) {
        .slick-slide img {
            width: 100% !important;
        }

        .slick-prev:before,
        .slick-next:before {
            color: white !important;
        }
    }




    .head_article {
        padding-top: 0.5rem !important;
    }

    .article_banner {
        position: relative;
        text-align: -webkit-center;
    }

    a {
        color: black;
        text-decoration: none;
    }

    a:hover {
        color: black;
        text-decoration: none;
    }

    .blog span {
        border: solid 1px;
        border-color: lightgray;
        padding: 2px 7px;
        background: lightgrey;
        margin-right: 10px;
        border-radius: 5px;
        font-size: .5rem;
    }

    .blog {
        margin-top: 30px;
    }

    .box {
        display: block;
        width: 100%;
        padding: .375rem .75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #212529;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        border-radius: 25px;
        transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    }


    .select2 {
        width: 100%;
    }

    .select2-selection {
        border: 0 !important;
        border-radius: 23px !important;
    }

    .select2-dropdown {
        border-radius: 23px !important;
        border-top: 1px solid #aaa !important;
        margin: 6px;
    }

    .select2-results ul {
        border-radius: 23px !important;
    }

    /* .select2 .selection span {
            border-radius: 23px;
        } */
    .slick-prev:before,
    .slick-next:before {
        color: black;

    }

    .slick-slide img {
        width: 87%;
    }

    .slick-dots {
        display: block !important;
        padding-right: 50%;
        color: black !important;
        bottom: -10%;
    }

    .slick-dots li.slick-active button:before {
        color: black !important;
        font-size: 14px !important;
    }

    .slick-dots li button::before {
        color: black !important;
        font-size: 14px !important;

    }
</style>

<body>
    <div class="container">
        <div class="row">
            <div class="col-4 col-xl-6 mb-5">
                <h1 class="head_article mt-3"><?php echo getWording('index', 'article') ?></h1>
            </div>
            <div class="col-8 col-xl-6 mb-5">
                <form class="box d-flex ms-auto w-100 w-xl-50 mt-3 ">
                    <select class="js-example-basic-multiple" name="article" multiple="multiple">
                        <!-- <option value="AL">Alabama</option>
                        ... 
                        <option value="WY">Wyoming</option> -->
                    </select>
                    <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="banner_slider">
            <?php foreach ($banner_list as $key => $banner_item) { ?>
                <div class="article_banner">
                    <?php if ($banner_item->banner_path != '') { ?>
                        <img src="<?php echo base_url($banner_item->banner_path) ?>" alt="">
                    <?php } else { ?>
                        <img class="w-100" src="<?php echo base_url('/assets/img/image-none.png') ?>" alt="">
                    <?php } ?>
                    <div class="article col-12">
                        <h3><b><?php echo getVariable($banner_item, 'title') ?></b></h3>
                        <p><?php echo getVariable($banner_item, 'detail') ?></p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <section class="blog mt-10">
        <div class="container">
            <div class="row">
                <?php foreach ($block_list as $key => $block_item) { ?>
                    <div class="col-6 col-sm-6 col-xl-6">
                        <a href="<?php echo base_url('Article/detail/' . $block_item->id) ?>">
                            <div class="row my-3">
                                <div class="col-12 col-xl-4">
                                    <img class="w-100" src="<?php echo base_url($block_item->thumbnail_path) ?>" alt="">
                                </div>
                                <div class="col-12 col-xl-8 align-self-center d-block pl-3 mt-5">
                                    <?php
                                    $tag = array();
                                    $tag = explode(",", $block_item->tags);
                                    // print_r($tag)
                                    ?>
                                    <?php foreach ($tag as $key => $block_tag) { ?>
                                        <?php if ($block_tag != null) { ?>
                                            <span>
                                                <?php echo ($block_tag) ?>
                                            </span>
                                        <?php } else { ?>
                                        <?php } ?>
                                    <?php } ?>
                                    <h4 class="mt-2"><?php echo getVariable($block_item, 'title') ?></h4>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <div class="mt-5">
        <?php include('footer.php') ?>
    </div>

    <svg aria-hidden="true" style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="icon-filter" width="100%" viewBox="0 0 25 25">
                <g>
                    <path d="M1.75,7.75h6.6803589c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891   S13.9000854,9.0452271,14.2356567,7.75H24.25C24.6640625,7.75,25,7.4140625,25,7s-0.3359375-0.75-0.75-0.75H14.2356567   c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891S8.7659302,4.9547729,8.4303589,6.25H1.75   C1.3359375,6.25,1,6.5859375,1,7S1.3359375,7.75,1.75,7.75z M11.3330078,5.4912109   c0.8320313,0,1.5087891,0.6767578,1.5087891,1.5087891s-0.6767578,1.5087891-1.5087891,1.5087891S9.8242188,7.8320313,9.8242188,7   S10.5009766,5.4912109,11.3330078,5.4912109z" fill="#1D1D1B" />
                    <path d="M24.25,12.25h-1.6061401c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891   S17.1741333,10.9547729,16.838562,12.25H1.75C1.3359375,12.25,1,12.5859375,1,13s0.3359375,0.75,0.75,0.75h15.088562   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891s2.5670776-0.963562,2.9026489-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,12.25,24.25,12.25z M19.7412109,14.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891S21.25,12.1679688,21.25,13   S20.5732422,14.5087891,19.7412109,14.5087891z" fill="#1D1D1B" />
                    <path d="M24.25,18.25H9.7181396c-0.3355103-1.2952271-1.5037842-2.2587891-2.9017334-2.2587891   c-1.3987427,0-2.5670776,0.963562-2.9026489,2.2587891H1.75C1.3359375,18.25,1,18.5859375,1,19s0.3359375,0.75,0.75,0.75h2.1637573   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891c1.3979492,0,2.5662231-0.963562,2.9017334-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,18.25,24.25,18.25z M6.8164063,20.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891   c0.8310547,0,1.5078125,0.6767578,1.5078125,1.5087891S7.6474609,20.5087891,6.8164063,20.5087891z" fill="#1D1D1B" />
                </g>
            </symbol>
        </defs>
    </svg>

</body>

</html>

<script>
    $(document).ready(function() {
        var search_article = '<?php echo getWording('index', 'search_article') ?>';
        $('.banner_slider').slick({
            dots: true,
            arrows: true,
            prevArrow: "<i class='slick-prev fas fa-chevron-left'></i>",
            nextArrow: "<i class='slick-next fas fa-chevron-right'></i>"
        });

        $('.product_heightlight_slider').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: "<i class='slick-prev fas fa-chevron-left'></i>",
            nextArrow: "<i class='slick-next fas fa-chevron-right'></i>"
        });

        $('.our_customer_slider').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            arrows: false
        });
        var base_url = '<?php echo base_url(); ?>';
        // $(document).ready(function() {
        //     $('.js-example-basic-multiple').select2();
        // });
        $("select[name='article']").select2({
            placeholder: search_article,
            width: null,
            allowClear: true,
            ajax: {
                url: "https://nextere.space/api_area/home/select2/article",
                dataType: 'json',
                delay: 250,
                cache: true,
                data: function(params) {
                    return {
                        search: params.term
                    };
                },
                processResults: function(data) {

                    return {
                        results: $.map(data.data, function(obj) {
                            return {
                                id: obj.id,
                                text: obj.title_th
                            };
                        })
                    };
                }
            }
        });

        $('select[name="article"]').on('select2:select', function(e) {
            var data = e.params.data;
            window.location.replace(base_url + "Article/detail/" + data.id);
        });
    });


    $('#play-button').click(function() {
        var mediaVideo = $("#my-video").get(0);
        if (mediaVideo.paused) {
            mediaVideo.play();
            $("#play-button").hide();
            $("#pause-button").fadeIn();
        }
    });

    $('#pause-button').click(function() {
        var mediaVideo = $("#my-video").get(0);
        if (mediaVideo.play) {
            mediaVideo.pause();
            $("#play-button").fadeIn();
            $("#pause-button").hide();
        }
    });

    $('#image_banner').click(function() {
        $('#exampleModal').modal('show');
    });

    $('#spinner-form2').click(function() {
        if ($("#spinner-form2").prop('checked') == true) {

            $(".secd-menu").addClass("active-secd-menu");
        } else {

            $(".secd-menu").removeClass("active-secd-menu");
        }
    });
</script>