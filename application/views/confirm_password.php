<?php include('header.php') ?>

<body style="background-color:#F8F8F8">
    <div class="container w-100" style="text-align: -webkit-center;">

        <div class="col-sm-6 col-xl-8" style="background-color:#FFFFFF; ">

            <?php if (empty($code)) { ?>
                <?php redirect(base_url('account/login')); ?>
            <?php } else { ?>

                <div class="confirm_password pt-5 w-75">

                    <div class="p-5 m-5" style="border-bottom: 2px solid #D6D6D6;">
                        <h4>
                            <b>
                                <?php echo getWording('profile', 'new_password') ?>
                            </b>
                        </h4>
                    </div>

                    <div style="width: auto; height:auto;">
                        <form id="datafrom">
                            <div class="col-sm-12 col-xl-12 pt-4" style="text-align:left;"><b><?php echo getWording('login', 'new_pass') ?>*</b>
                                <div class="password pt-2">
                                    <input type="password" id="new_password" name="new_password" placeholder="<?php echo getWording('login', 'new_pass') ?>" required style="width: 100%; height: 45px;
                        border: 1px solid #707070; border-radius: 5px;padding-left:15px">
                                </div>
                            </div>

                            <div class="col-sm-12 col-xl-12 pt-4" style="text-align:left;"><b><?php echo getWording('login', 'confirm_password') ?>*</b>
                                <div class="password pt-2">
                                    <input type="password" id="confirm_password" name="confirm_password" placeholder="<?php echo getWording('login', 'confirm_password') ?>" required style="width: 100%; height: 45px;
                        border: 1px solid #707070; border-radius: 5px;padding-left:15px">
                                </div>
                            </div>


                            <div class="password pt-2">
                                <input type="text" id="token" name="token" placeholder="token" value="<?php echo $code ?>" required style="width: 100%; height: 45px;
                        border: 1px solid #707070; border-radius: 5px;padding-left:15px" hidden>
                            </div>


                            <div class="col-sm-12 col-xl-5 p-5">
                                <input type="submit" id="submit-datafrom" value="<?php echo getWording('login', 'submit_pass') ?>" style="width: 100%;
                    height: 45px; background: #D3D3D3; border: 0px; border-radius: 23px; font-weight: bold;">
                            </div>
                        </form>
                    </div>
                </div>
            <?php } ?>

        </div>

    </div>

    <!-- Modal -->
    <!-- <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <h3 class="confirm_response"></h3>
                </div>
                <div class="modal-footer confirm_response_btn">
                    <button type="button" class="btn btn-secondary confirm_fail d-none" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div> -->
    <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-content-style">
                <div class="modal-header modal-header-style">
                </div>
                <div class="modal-body" style="text-align: center;">
                    <div class="btn-close-modal">
                        <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
                    </div>
                    <h3 class="confirm_response"></h3>
                </div>
                <div class="modal-footer modal-footer-style register_response_btn">

                </div>
            </div>
        </div>
    </div>


</body>
<?php include('footer.php') ?>

<script>
    $("#datafrom").submit(function(e) {
        e.preventDefault();
        return false;
    });

    $("#submit-datafrom").click(function() {

        $("#datafrom").validate({
            rules: {
                new_password: {
                    required: true,
                    minlength: 5
                },
                confirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#new_password"
                },
            },
            messages: {
                new_password: {
                    required: "Please enter your new password.",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirm_password: {
                    required: "Please confirm your new password.",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
            },

            errorPlacement: function(error, element) {
                error.insertBefore(element);
            },
            submitHandler: function() {

                var data = {
                    "token": $("#token").val(),
                    "confirm_new_password": $("#confirm_password").val(),
                }

                var send_data = {
                    "url": "<?php echo base_url('member/confirm_password') ?>",
                    "method": "POST",
                    "data": data
                }

                $.ajax(send_data).done(function(response) {
                    if (response.code == "0x0000-00000") {
                        window.location.href = "<?php echo base_url('Account/login')
                                                ?>";
                        $('.confirm_success').addClass('d-none');
                        $('.confirm_success').removeClass('d-block');
                        $('.confirm_fail').addClass('d-block');
                        $('.confirm_fail').removeClass('d-none');
                        $(".confirm_response").text('The password has been reset.');
                    } else {
                        $('.confirm_success').addClass('d-none');
                        $('.confirm_success').removeClass('d-block');
                        $('.confirm_fail').addClass('d-block');
                        $('.confirm_fail').removeClass('d-none');
                        $(".confirm_response").text(response.message);
                    }
                    $('#staticBackdrop').modal('show');
                });
            }
        });
    });
</script>