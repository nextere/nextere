<?php include('header.php') ?>

<body style="background-color:#F8F8F8">
    <div class="container w-100" style="text-align: -webkit-center;">

        <div class="col-sm-12 col-xl-8" style="background-color:#FFFFFF; ">

            <div style="text-align: right; padding-bottom: 10px; padding-top: 20px;">

                <button class="btn3 mt-3" onclick="location.href='<?php echo base_url('Account/login') ?>'" style="width: 142px; height: 45px; background-color: #FFFFFF; border: 1px solid #707070; border-radius: 23px; margin-right: 20px;">
                    <b>
                        <?php echo getWording('login', 'login') ?>
                    </b>
                </button>

            </div>
            <form class="member pt-4 w-75" id="dataForm">
                <div style="border-bottom: 2px solid #D6D6D6; padding-bottom:20px; margin-bottom:35px;width: auto;">
                    <h4>
                        <b>
                            <?php echo getWording('login', 'register') ?>
                        </b>
                    </h4>
                </div>

                <div style="width: auto; height:auto;">

                    <div class="col-sm-12 col-xl-12" style="text-align:left;"><b><?php echo getWording('login', 'name') ?>*</b>
                        <div class="name pt-2">
                            <input type="text" id="name" name="name" placeholder="<?php echo getWording('login', 'name') ?>" required style="width: 100%; height: 45px;
                        border: 1px solid #707070; border-radius: 5px;padding-left:15px ">
                        </div>
                    </div>

                    <!-- <div class="col-sm-12 col-xl-12 pt-4" style="text-align:left;" id="password_remove"><b><?php echo getWording('login', 'password') ?>*</b>
                        <div class="password pt-2">
                            <input type="password" id="password" name="password" placeholder="Password" required style="width: 100%; height: 45px;
                        border: 1px solid #707070; border-radius: 5px;padding-left:15px ">
                        </div>
                    </div>

                    <div class="col-sm-12 col-xl-12 pt-4" style="text-align:left;" id="confirm_password_remove"><b><?php echo getWording('login', 'confirm_password') ?>*</b>
                        <div class="confirm_password pt-2">
                            <input type="password" id="confirm_password" name="confirm_password" placeholder="<?php echo getWording('login', 'confirm_password') ?>" required style="width: 100%; height: 45px;
                        border: 1px solid #707070; border-radius: 5px;padding-left:15px ">
                        </div>
                    </div> -->

                    <!-- <div class="col-sm-12 col-xl-12 pt-4" style="text-align:left;"><b><?php echo getWording('login', 'gender') ?>*</b>
                        <div class="gender pt-2">
                            <input class="mr-2" type="radio" name="gender" value="male" id="male"><label for="male"><?php echo getWording('login', 'male') ?></label>
                              <input class="mr-2" type="radio" name="gender" value="female" id="female"><label for="female"><?php echo getWording('login', 'female') ?></label>
                              <input class="mr-2" type="radio" name="gender" value="lgbtq" id="lgbtq"><label for="lgbtq">LGBTQ</label>
                        </div>
                    </div> -->

                    <!-- <div class="col-sm-12 col-xl-12 pt-4" style="text-align:left;"><b><?php echo getWording('login', 'birth') ?>*</b>
                        <div class="birdth_date pt-2" id="birthdate_input">
                            <input type="text" id="birdth_date" name="birdth_date" placeholder="วัน/เดือน/ปีเกิด" required style="width: 100%; height: 45px;
                        border: 1px solid #707070; border-radius: 5px;padding-left:15px ">
                        </div>
                    </div> -->
                    <div class="col-sm-12 col-xl-12 pt-4" style="text-align:left;"><b><?php echo getWording('login', 'email') ?>*</b>
                        <div class="email pt-2">
                            <input type="text" id="email" name="email" placeholder="<?php echo getWording('login', 'email') ?>" required style="width: 100%; height: 45px;
                        border: 1px solid #707070; border-radius: 5px;padding-left:15px ">
                        </div>
                    </div>
                    <!-- <div class="col-sm-12 col-xl-12 pt-4" style="text-align:left;"><b><?php echo getWording('login', 'tax') ?>*</b>
                        <div class="tax_id pt-2">
                            <input type="text" id="tax_id" name="tax_id" placeholder="<?php echo getWording('login', 'tax') ?>" required style="width: 100%; height: 45px;
                        border: 1px solid #707070; border-radius: 5px;padding-left:15px ">
                        </div>
                    </div>
                    <div class="col-sm-12 col-xl-12 pt-4" style="text-align:left;"><b><?php echo getWording('contact', 'phone') ?>*</b>
                        <div class="telephone pt-2">
                            <input type="tel" name="telephone" placeholder="xxx-xxx-xxxx" id="telephone" required style="width: 100%; height: 45px;
                        border: 1px solid #707070; border-radius: 5px;padding-left:15px ">
                        </div>
                    </div> -->

                    <div class="row ml-1" style="font-size: smaller;">
                        <div class="col-sm-12 col-xl-12 pt-4" style="text-align:left;">
                            <div class="pt-1">
                                 <?php echo getWording('login', 'detail1') ?>
                            </div>
                        </div>
                        <!-- <div class="col-sm-12 col-xl-12" style="text-align:left;">
                            <div>
                                - <?php echo getWording('login', 'detail2') ?>
                            </div>
                        </div> -->

                    </div>
                    <div class="mt-2 pl-2" style="text-align: left;">
                        <input type="checkbox" id="checkbox" name="checkbox" value="checkbox">
                        <label for="checkbox">
                            <sapan><?php echo getWording('login', 'you_accept') ?></sapan>
                            <span class="pointer" data-bs-toggle="modal" data-bs-target="#example_1"><?php echo getWording('login', 'terms') ?></span>
                            <?php echo getWording('login', 'and') ?>
                            <span class="pointer" data-bs-toggle="modal" data-bs-target="#example"><?php echo getWording('login', 'privacy_policy') ?></span>
                            <?php echo getWording('login', 'of') ?>
                        </label><br>
                    </div>
                    <div class="submit pt-4 mb-3">
                        <input id="register" class="submit_btn" disabled type="submit" onclick="logOut()" value="<?php echo getWording('login', 'register') ?>" style="width: 50%;
                    height: 45px; background: #D3D3D3; border: 0px; border-radius: 23px; font-weight: bold;">
                    </div>
                    <!-- <button class="btn" id="logout" onclick="logOut()" style="width: 142px;height: 45px; background-color: #AAAAAA; border-radius: 23px; padding: 14px; margin:10px; display:none;"><b>Line logOut</b></button> -->

                </div>
            </form>
            <section id="social">
                <div style=" color:#9F9F9F">
                    <hr>
                    <?php echo getWording('login', 'or') ?>
                    <hr>
                </div>
                <div class="row" style="justify-content: center; font-weight: bold; padding-bottom: 20px;">
                    <button class="btn social facebook-icon-login " onClick="logInWithFacebook()" type="button" style="margin-right: 0px;"> <i class="fab fa-facebook-f"></i></button>
                    <button class="btn social instagram-icon-login" id="customBtn" style="margin-right: 10px; margin-left:10px"><i class="fab fa-google"></i></button>
                    <button class="btn social line-icon-login" onclick="logIn()" style="margin-right: 0px;"><i class="fab fa-line"></i></button>
                </div>
            </section>

        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog" style="text-align: -webkit-center;">
            <div class="modal-content modal-content-style">
                <div class="modal-header modal-header-style">

                </div>
                <div class="modal-body">
                    <div class="btn-close-modal">
                        <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
                    </div>

                    <h3 class="register_response" style="text-align: center;"></h3>
                    <button class="btn" type="button" data-bs-dismiss="modal" aria-label="Close" style="width: 142px;height: 40px; background-color: #AAAAAA; border-radius: 23px; padding: 10px; margin:10px;color:white;">รับทราบ</button>
                </div>
                <div class="modal-footer modal-footer-style register_response_btn">
                    <!-- <button type="button" class="btn  register_success d-none" style="width: 142px;height: 40px; background-color: #AAAAAA; border-radius: 23px; padding: 10px; margin:10px;color:white;" onclick="location.href='<?php echo base_url() ?>'">Close</button>
                    <button type="button" class="btn register_fail d-none" style="width: 142px;height: 40px; background-color: #AAAAAA; border-radius: 23px; padding: 10px; margin:10px;color:white;" data-bs-dismiss="modal">Close</button> -->
                </div>
            </div>
        </div>
    </div>



</body>

<?php include('footer.php') ?>

<script src="https://static.line-scdn.net/liff/edge/2/sdk.js"></script>

<script>
    $('#checkbox').change(function() {
        if (this.checked) {
            $('#register').prop("disabled", false);
        } else {
            $('#register').prop("disabled", true);
        }
    });

    function logOut() {
        liff.logout()
        // window.location.reload()
    }

    function logIn() {
        liff.login({
            redirectUri: window.location.href
        })
    }
    async function getUserProfile() {
        var information_completely = '<?php echo getWording('login', 'information_completely') ?>';
        const profile = await liff.getProfile()
        console.log('profile', profile)
        line_token = profile.userId;
        $("#name").val(profile.displayName);
        $(".register_response").text(information_completely);
        $('#staticBackdrop').modal('show');
        $('#password_remove').hide();
        $('#confirm_password_remove').hide();
        // $('#social').hide();
        if (liff.isLoggedIn() == true) {
            $('#social').hide(function() {
                logOut();
            });
        }
        // logOut();
        // console.log(liff.isLoggedIn());
    }


    async function main() {
        await liff.init({
            liffId: "1655647105-yOBORX2j"
        });
        if (liff.isInClient()) {
            getUserProfile();
        } else {
            if (liff.isLoggedIn() == false) {
                // getUserProfile();
                console.log('not logged in');
                console.log('liff_id', liff.init);
            } else {
                getUserProfile();
            }
        }
    }
    main()
    // console.log(login());
</script>

<meta name="google-signin-client_id" content="441404752301-1a5fd972o9ab8ps1960ksk63od66p2n8.apps.googleusercontent.com">
<meta name="google-signin-scope" content="profile email">
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://apis.google.com/js/api:client.js"></script>
<!-- ------------------------------------------------------------------------------------ -->
<script src="https://apis.google.com/js/api:client.js"></script>


<script>
    var googleUser = {};
    var startApp = function() {
        gapi.load('auth2', function() {
            auth2 = gapi.auth2.init({
                client_id: '441404752301-1a5fd972o9ab8ps1960ksk63od66p2n8.apps.googleusercontent.com',
                cookiepolicy: 'single_host_origin'
            });
            attachSignin(document.getElementById('customBtn'));
        });
    };

    function attachSignin(element) {
        // console.log(element.id);
        auth2.attachClickHandler(element, {},
            function(googleUser) {
                document.getElementById('name').innerText = "Signed in: "
                var information_completely = '<?php echo getWording('login', 'information_completely') ?>';
                var profile = googleUser.getBasicProfile();
                console.log(profile);
                $("#name").val(profile.Qe);
                var email = profile.getEmail();
                var token = googleUser.getAuthResponse().id_token;
                $("#email").val(email);
                google_token = profile.US
                // google_token = token
                $(".register_response").text(information_completely);
                $('#staticBackdrop').modal('show');
                $('#password_remove').hide();
                $('#confirm_password_remove').hide();
                $('#social').hide();
            }, );
    }
</script>
<div id="name"></div>
<script>
    startApp();
</script>

<!-- --------------------------------------------------------------------------------- -->


<!-- --------------------------------------------------------------------------------- -->
<script>
    var facebook_name = "";
    var facebook_token = null;
    var google_token = null;
    var line_token = null;
    jQuery(document).ready(function() {


        window.fbAsyncInit = function() {
            FB.init({
                appId: '295555285694750',
                cookie: true, // This is important, it's not enabled by default
                version: 'v2.5'
            });
        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    });
</script>

<script type="text/javascript">
    logInWithFacebook = function() {
        FB.login(function(response) {
            FB.api(response.authResponse.userID, function(response) {

                facebook_name = response.name;
                $("#name").val(facebook_name);
            });
            var authResponse = '';
            if (response.authResponse) {
                FB.getLoginStatus(function(response) {
                    authResponse = response.authResponse.accessToken;
                    fetch('https://graph.facebook.com/v2.5/me?fields=email&access_token=' + authResponse).then((resp) => console.log(resp.json().then(data => {
                        // facebook_token = authResponse;
                        facebook_token = data.id;
                        // console.log(data);
                        var information_completely = '<?php echo getWording('login', 'information_completely') ?>';
                        $("#email").val(data.email);
                        $(".register_response").text(information_completely);
                        $('#staticBackdrop').modal('show');
                        $('#password_remove').hide();
                        $('#confirm_password_remove').hide();
                        $('#social').hide();
                    })))
                });
            } else {
                alert('User cancelled login or did not fully authorize.');
            }
        }, {
            scope: 'email'
        });
        return false;
    };
</script>

<script>
    $('#birthdate_input input').datepicker({
        format: "dd/mm/yyyy"
    });


    $('#dataForm input').keypress(function() {
        console.log('birdth_date', $('#birdth_date').val());
        console.log('birdth_date', $('#telephone').val());
        // console.log('All input validate!!!', $("#dataForm").valid());
    });

    $(document).ready(function() {

        $('#telephone').mask('000-000-0000');

        $("#dataForm").validate({
            rules: {
                name: "required",
                gender: "required",
                password: {
                    required: true,
                    minlength: 5
                },
                confirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#password"
                },
                email: {
                    required: true,
                    email: true
                },
                telephone: {
                    required: true,
                    pattern: '\\d{3}-\\d{3}-\\d{4}',
                    minlength: 12,
                    maxlength: 12
                },
                tax_id: "required",
                birdth_date: {
                    required: true,
                    pattern: '\\d{2}/\\d{2}/\\d{4}',
                }
            },
            messages: {
                name: "Please enter your name",
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirm_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
                email: "Please enter a valid email address",
                telephone: "Please correct telephone format",
                tax_id: "Please enter your tax ID",
                birdth_date: "Please enter your birdth date",
            },

            errorPlacement: function(error, element) {
                error.insertBefore(element);
            }

        });


        jQuery.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                if (regexp.constructor != RegExp)
                    regexp = new RegExp(regexp);
                else if (regexp.global)
                    regexp.lastIndex = 0;
                return this.optional(element) || regexp.test(value);
            }, "erreur expression reguliere"
        );

    });

    $("#dataForm").submit(function(e) {
        // console.log('facebooktoken :', facebook_token),
        //     console.log('googletoken :', google_token),
        //     console.log('linetoken :', line_token),
        e.preventDefault();
        // console.log($("#subscribe").val());.
        if ($("#dataForm").valid() == true) {
            var data = {
                "name": $("#name").val(),
                // "password": $("#password").val(),
                // "gender": $('input[name=gender]:checked', '#dataForm').val(),
                // "birdth_date": $("#birdth_date").val(),
                "email": $("#email").val(),
                // "tax_id": $("#tax_id").val(),
                // "telephone": $("#telephone").val(),
                "facebook_token": facebook_token,
                "google_token": google_token,
                "line_token": line_token,
            }
            console.log(data);
            console.log($("#dataForm").valid());


            var send_data = {
                "url": "<?php echo base_url('/Account/register') ?>",
                "method": "POST",
                "data": data
            }

            var base_url = 'location.href="' + '<?php echo base_url() ?>' + '"';

            $.ajax(send_data).done(function(response) {

                if (response.code == "0x0000-00000") {
                    $('.register_success').removeClass('d-none');
                    $('.register_success').addClass('d-block');
                    $('.register_fail').removeClass('d-block');
                    $('.register_fail').addClass('d-none');
                    $(".register_response").text('Please verify your email.');
                } else if (response.code == "1x0000-00000") {
                    $('.register_success').addClass('d-none');
                    $('.register_success').removeClass('d-block');
                    $('.register_fail').addClass('d-block');
                    $('.register_fail').removeClass('d-none');
                    $(".register_response").text('Email already exists.');
                } else {
                    $('.register_success').addClass('d-none');
                    $('.register_success').removeClass('d-block');
                    $('.register_fail').addClass('d-block');
                    $('.register_fail').removeClass('d-none');
                    $(".register_response").text('Email already exists.');
                }
                $('#staticBackdrop').modal('show');
                // console.log(response);
            });
            return false;
        } else {
            console.log('not submit!!!');
        }


    });
    // }
</script>