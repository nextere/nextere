<?php include('header.php') ?>
<style>
    /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */

    .glyphicon {
        margin-right: 5px;
    }

    .thumbnail {
        margin-bottom: 20px;
        padding: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        border-radius: 0px;
    }

    .item.list-group-item {
        float: none;
        width: 100%;

        background-color: #fff;
    }

    .item.list-group-item .list-group-image {
        margin-right: 10px;
    }

    .item.list-group-item .thumbnail {
        margin-bottom: 0px;
    }

    .item.list-group-item .caption {
        padding: 9px 9px 0px 9px;
    }

    .item.list-group-item img {
        float: left;
    }

    .item.list-group-item:after {
        clear: both;
    }

    .list-group-item-text {
        margin: 0 0 11px;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>

<body>
    <div class="row ">
        <div class="col-12  pt-5 pb-5" style="background-color: lightgray; text-align: center;">
            <h4><b>Door lock</b></h4>
            <P>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</P>
        </div>

        <div class="col-12  pt-2 pb-2 mt-3" style="background-color: lightgray; text-align:left;">
            <div class="container">
                <a href="#" class="btn btn-secondary buttomsale">ราคา</a> <a href="#" class="btn btn-secondary buttomsale">รูปแบบในการใช้งาน</a>
            </div>
        </div>
    </div>
    <div class="container ">
        <div class="row">
            <div class="col-sm-6 col-xl-8 mt-3 ">
                <a href="#" class="btn btn-outline-dark buttomsale" id="grid"><i class="fas fa-th-large "></i></a>
                <a href="#" class="btn btn-outline-dark buttomsale" id="list"><i class="fas fa-list"></i></a>
            </div>
            <div class="col-sm-6 col-xl-4 mt-3 " style="text-align: right;">
                เรียงตาม:
                <select id="fillter" name="fillter">
                    <option value="popula">สินค้าขายดี</option>
                    <option value="sale">ลดราคา</option>
                </select>

                แสดงผล:
                <select id="fillter" name="fillter">
                    <option value="8">8</option>
                    <option value="12">12</option>
                    <option value="24">24</option>
                </select>
            </div>
        </div>
        <div class="row " id="products">
            <div class="col-sm-6 col-xl-4 mt-4 item" style="text-align:left">
                <div class="product-item">
                    <img class="w-100" src="<?php echo base_url('/assets/img/squre.jpg') ?>" alt="">
                    <span>Security Sensor</span>
                    <h4>Google Nest Mini Chalk T…</h4>
                    <h4 class="price">53,000 บาท</h4>
                    <div class="row mt-5">
                        <div class="col-8 pr-1">
                            <button class="order_btn w-100">สั่งซื้อสินค้า</button>
                        </div>

                        <div class="col-4 pl-1">
                            <button type="button" class="filter_btn w-100 p-0" data-bs-toggle="modal" data-bs-target="#compare_product_modal">
                                <svg class="w-100">
                                    <use xlink:href="#icon-filter"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-4 mt-4 item" style="text-align:left">
                <div class="product-item">
                    <img class="w-100" src="<?php echo base_url('/assets/img/squre.jpg') ?>" alt="">
                    <span>Security Sensor</span>
                    <h4>Google Nest Mini Chalk T…</h4>
                    <h4 class="price">53,000 บาท</h4>
                    <div class="row mt-5">
                        <div class="col-8 pr-1">
                            <button class="order_btn w-100">สั่งซื้อสินค้า</button>
                        </div>

                        <div class="col-4 pl-1">
                            <button type="button" class="filter_btn w-100 p-0" data-bs-toggle="modal" data-bs-target="#compare_product_modal">
                                <svg class="w-100">
                                    <use xlink:href="#icon-filter"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-4 mt-4 item" style="text-align:left">
                <div class="product-item">
                    <img class="w-100" src="<?php echo base_url('/assets/img/squre.jpg') ?>" alt="">
                    <span>Security Sensor</span>
                    <h4>Google Nest Mini Chalk T…</h4>
                    <h4 class="price">53,000 บาท</h4>
                    <div class="row mt-5">
                        <div class="col-8 pr-1">
                            <button class="order_btn w-100">สั่งซื้อสินค้า</button>
                        </div>

                        <div class="col-4 pl-1">
                            <button type="button" class="filter_btn w-100 p-0" data-bs-toggle="modal" data-bs-target="#compare_product_modal">
                                <svg class="w-100">
                                    <use xlink:href="#icon-filter"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-4 mt-4 item" style="text-align:left">
                <div class="product-item">
                    <img class="w-100" src="<?php echo base_url('/assets/img/squre.jpg') ?>" alt="">
                    <span>Security Sensor</span>
                    <h4>Google Nest Mini Chalk T…</h4>
                    <h4 class="price">53,000 บาท</h4>
                    <div class="row mt-5">
                        <div class="col-8 pr-1">
                            <button class="order_btn w-100">สั่งซื้อสินค้า</button>
                        </div>

                        <div class="col-4 pl-1">
                            <button type="button" class="filter_btn w-100 p-0" data-bs-toggle="modal" data-bs-target="#compare_product_modal">
                                <svg class="w-100">
                                    <use xlink:href="#icon-filter"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-4 mt-4 item" style="text-align:left">
                <div class="product-item">
                    <img class="w-100" src="<?php echo base_url('/assets/img/squre.jpg') ?>" alt="">
                    <span>Security Sensor</span>
                    <h4>Google Nest Mini Chalk T…</h4>
                    <h4 class="price">53,000 บาท</h4>
                    <div class="row mt-5">
                        <div class="col-8 pr-1">
                            <button class="order_btn w-100">สั่งซื้อสินค้า</button>
                        </div>

                        <div class="col-4 pl-1">
                            <button type="button" class="filter_btn w-100 p-0" data-bs-toggle="modal" data-bs-target="#compare_product_modal">
                                <svg class="w-100">
                                    <use xlink:href="#icon-filter"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-4 mt-4 item" style="text-align:left">
                <div class="product-item">
                    <img class="w-100" src="<?php echo base_url('/assets/img/squre.jpg') ?>" alt="">
                    <span>Security Sensor</span>
                    <h4>Google Nest Mini Chalk T…</h4>
                    <h4 class="price">53,000 บาท</h4>
                    <div class="row mt-5">
                        <div class="col-8 pr-1">
                            <button class="order_btn w-100">สั่งซื้อสินค้า</button>
                        </div>

                        <div class="col-4 pl-1">
                            <button type="button" class="filter_btn w-100 p-0" data-bs-toggle="modal" data-bs-target="#compare_product_modal">
                                <svg class="w-100">
                                    <use xlink:href="#icon-filter"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-4 mt-4 item" style="text-align:left">
                <div class="product-item">
                    <img class="w-100" src="<?php echo base_url('/assets/img/squre.jpg') ?>" alt="">
                    <span>Security Sensor</span>
                    <h4>Google Nest Mini Chalk T…</h4>
                    <h4 class="price">53,000 บาท</h4>
                    <div class="row mt-5">
                        <div class="col-8 pr-1">
                            <button class="order_btn w-100">สั่งซื้อสินค้า</button>
                        </div>

                        <div class="col-4 pl-1">
                            <button type="button" class="filter_btn w-100 p-0" data-bs-toggle="modal" data-bs-target="#compare_product_modal">
                                <svg class="w-100">
                                    <use xlink:href="#icon-filter"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-4 mt-4 item" style="text-align:left">
                <div class="product-item">
                    <img class="w-100" src="<?php echo base_url('/assets/img/squre.jpg') ?>" alt="">
                    <span>Security Sensor</span>
                    <h4>Google Nest Mini Chalk T…</h4>
                    <h4 class="price">53,000 บาท</h4>
                    <div class="row mt-5">
                        <div class="col-8 pr-1">
                            <button class="order_btn w-100">สั่งซื้อสินค้า</button>
                        </div>

                        <div class="col-4 pl-1">
                            <button type="button" class="filter_btn w-100 p-0" data-bs-toggle="modal" data-bs-target="#compare_product_modal">
                                <svg class="w-100">
                                    <use xlink:href="#icon-filter"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-4 mt-4 item" style="text-align:left">
                <div class="product-item">
                    <img class="w-100" src="<?php echo base_url('/assets/img/squre.jpg') ?>" alt="">
                    <span>Security Sensor</span>
                    <h4>Google Nest Mini Chalk T…</h4>
                    <h4 class="price">53,000 บาท</h4>
                    <div class="row mt-5">
                        <div class="col-8 pr-1">
                            <button class="order_btn w-100">สั่งซื้อสินค้า</button>
                        </div>

                        <div class="col-4 pl-1">
                            <button type="button" class="filter_btn w-100 p-0" data-bs-toggle="modal" data-bs-target="#compare_product_modal">
                                <svg class="w-100">
                                    <use xlink:href="#icon-filter"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-4 mt-4 item" style="text-align:left">
                <div class="product-item">
                    <img class="w-100" src="<?php echo base_url('/assets/img/squre.jpg') ?>" alt="">
                    <span>Security Sensor</span>
                    <h4>Google Nest Mini Chalk T…</h4>
                    <h4 class="price">53,000 บาท</h4>
                    <div class="row mt-5">
                        <div class="col-8 pr-1">
                            <button class="order_btn w-100">สั่งซื้อสินค้า</button>
                        </div>

                        <div class="col-4 pl-1">
                            <button type="button" class="filter_btn w-100 p-0" data-bs-toggle="modal" data-bs-target="#compare_product_modal">
                                <svg class="w-100">
                                    <use xlink:href="#icon-filter"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-4 mt-4 item" style="text-align:left">
                <div class="product-item">
                    <img class="w-100" src="<?php echo base_url('/assets/img/squre.jpg') ?>" alt="">
                    <span>Security Sensor</span>
                    <h4>Google Nest Mini Chalk T…</h4>
                    <h4 class="price">53,000 บาท</h4>
                    <div class="row mt-5">
                        <div class="col-8 pr-1">
                            <button class="order_btn w-100">สั่งซื้อสินค้า</button>
                        </div>

                        <div class="col-4 pl-1">
                            <button type="button" class="filter_btn w-100 p-0" data-bs-toggle="modal" data-bs-target="#compare_product_modal">
                                <svg class="w-100">
                                    <use xlink:href="#icon-filter"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-4 mt-4 item" style="text-align:left">
                <div class="product-item">
                    <img class="w-100" src="<?php echo base_url('/assets/img/squre.jpg') ?>" alt="">
                    <span>Security Sensor</span>
                    <h4>Google Nest Mini Chalk T…</h4>
                    <h4 class="price">53,000 บาท</h4>
                    <div class="row mt-5">
                        <div class="col-8 pr-1">
                            <button class="order_btn w-100">สั่งซื้อสินค้า</button>
                        </div>

                        <div class="col-4 pl-1">
                            <button type="button" class="filter_btn w-100 p-0" data-bs-toggle="modal" data-bs-target="#compare_product_modal">
                                <svg class="w-100">
                                    <use xlink:href="#icon-filter"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-5"> <?php include('footer.php') ?></div>



    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <!-- <script src="../assets/vender/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script> -->
    <svg aria-hidden="true" style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="icon-filter" width="100%" viewBox="0 0 25 25">
                <g>
                    <path d="M1.75,7.75h6.6803589c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891   S13.9000854,9.0452271,14.2356567,7.75H24.25C24.6640625,7.75,25,7.4140625,25,7s-0.3359375-0.75-0.75-0.75H14.2356567   c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891S8.7659302,4.9547729,8.4303589,6.25H1.75   C1.3359375,6.25,1,6.5859375,1,7S1.3359375,7.75,1.75,7.75z M11.3330078,5.4912109   c0.8320313,0,1.5087891,0.6767578,1.5087891,1.5087891s-0.6767578,1.5087891-1.5087891,1.5087891S9.8242188,7.8320313,9.8242188,7   S10.5009766,5.4912109,11.3330078,5.4912109z" fill="#1D1D1B" />
                    <path d="M24.25,12.25h-1.6061401c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891   S17.1741333,10.9547729,16.838562,12.25H1.75C1.3359375,12.25,1,12.5859375,1,13s0.3359375,0.75,0.75,0.75h15.088562   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891s2.5670776-0.963562,2.9026489-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,12.25,24.25,12.25z M19.7412109,14.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891S21.25,12.1679688,21.25,13   S20.5732422,14.5087891,19.7412109,14.5087891z" fill="#1D1D1B" />
                    <path d="M24.25,18.25H9.7181396c-0.3355103-1.2952271-1.5037842-2.2587891-2.9017334-2.2587891   c-1.3987427,0-2.5670776,0.963562-2.9026489,2.2587891H1.75C1.3359375,18.25,1,18.5859375,1,19s0.3359375,0.75,0.75,0.75h2.1637573   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891c1.3979492,0,2.5662231-0.963562,2.9017334-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,18.25,24.25,18.25z M6.8164063,20.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891   c0.8310547,0,1.5078125,0.6767578,1.5078125,1.5087891S7.6474609,20.5087891,6.8164063,20.5087891z" fill="#1D1D1B" />
                </g>
            </symbol>
        </defs>
    </svg>
    <!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'></script> -->
    <script>
        $(document).ready(function() {
            $('#list').click(function(event) {
                event.preventDefault();
                $('#products .item').addClass('list-group-item');
                $('.product-item .row .col-4').addClass('col-1')
                $('.product-item .row .col-4').removeClass('col-4');
                $('.product-item .row .col-8').addClass('col-3')
                $('.product-item .row .col-8').removeClass('col-8');
                $('.product-item img').removeClass('w-100');
                $('.product-item img').addClass('w-20');
                $('.product-item h4').addClass('pl-5 ');
                $('.product-item span').addClass('pl-5');


            });
            $('#grid').click(function(event) {
                event.preventDefault();
                $('#products .item').removeClass('list-group-item');
                $('#products .item').addClass('grid-group-item');
                $('.product-item .row .col-1').addClass('col-4')
                $('.product-item .row .col-1').removeClass('col-1');
                $('.product-item .row .col-3').addClass('col-8')
                $('.product-item .row .col-3').removeClass('col-3');
                $('.product-item img').removeClass('w-20');
                $('.product-item img').addClass('w-100');
            });
        });
    </script>
    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
</body>

</html>