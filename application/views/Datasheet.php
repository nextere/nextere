<?php include('header.php') ?>
<style>
    .Datasheet ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 100%;
        background-color: white;
        border: 1px solid #BCBCBC;
        opacity: 1;
        font-size: 16px;
    }

    .Datasheet li a.active {
        background-color: #BCBCBC;
        color: white;
    }

    .Datasheet li a {
        display: block;
        color: #000;
        padding: 8px 12px;
        text-decoration: none;
        font-size: 14px;
    }
</style>

<body>
    <section class="Datasheet">
        <div class="container mt-5">
            <div class="row">
                <div class="col-sm-12 col-xl-12 pt-5 pb-5 pl-5 mt-5" style="background-color: lightgray; text-align:left">
                    <h4><b><?php echo getWording('profile', 'my_account') ?></b></h4>
                    <p><?php echo getWording('profile', 'hi') ?> <?php echo $user_info->fullname ?>, <?php echo getWording('profile', 'detail') ?></p>
                </div>
                <div class="col-sm-4 col-xl-3 mt-3 pl-0">
                    <ul>
                        <li><a href="<?php echo base_url('Account/profile') ?>"> <img class="mr-3" style="width: 16px; height: 16px;" src="<?php echo base_url('/assets/img/icon/user.png') ?>"><?php echo getWording('profile', 'profile') ?></a></li>
                        <li><a href="<?php echo base_url('Account/order_history') ?>"><img class="mr-3" style="width: 20px; height: 18px;" src="<?php echo base_url('/assets/img/icon/shopping-cart.png') ?>"><?php echo getWording('profile', 'my_orders') ?></a></li>
                        <li><a href="<?php echo base_url('Account/quotation_request_history') ?>"><img class="mr-3" style="width: 18px; height: 18px;" src="<?php echo base_url('/assets/img/icon/Invoice.png') ?>"><?php echo getWording('profile', 'quotation') ?></a></li>
                        <li><a href="<?php echo base_url('Account/sending_address') ?>"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php echo base_url('/assets/img/icon/delivery-truck.png') ?>"><?php echo getWording('profile', 'shipping_address') ?></a></li>
                        <li><a href="<?php echo base_url('Account/tax_invoice_address') ?>"><img class="mr-3" style="width: 16px; height: 16px;" src="<?php echo base_url('/assets/img/icon/location-2.png') ?>"><?php echo getWording('profile', 'tax_invoice') ?></a></li>
                        <!-- <li><a href="<?php echo base_url('Account/payment') ?>"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php echo base_url('/assets/img/icon/credit-card.png') ?>"><?php echo getWording('profile', 'credit') ?></a></li> -->
                        <li><a class="active" href="<?php echo base_url('Account/datasheet') ?>"><img class="mr-3" style="width: 16px; height: 20px;" src="<?php echo base_url('/assets/img/icon/Datasheet-White.png') ?>"><?php echo getWording('profile', 'datasheet') ?></a></li>
                    </ul>
                </div>
                <div class="col-sm-8 col-xl-9">
                    <div class="row">
                        <div class="col-sm-12 col-xl-12 mt-3">
                            <h3 style="font-weight: bold"><?php echo getWording('profile', 'datasheet') ?></h3>
                        </div>
                        <hr>

                        <table class="table">
                            <thead class="thead-light">
                                <tr class="table-active">
                                    <th class="col-sm-8 col-xl-9"><?php echo getWording('profile', 'datasheet') ?></th>
                                    <th class="col-sm-4 col-xl-3"><?php echo getWording('profile', 'download') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Datasheet</td>
                                    <td><i class="fas fa-download"></i> Datasheet </td>
                                </tr>
                                <tr>
                                    <td>Datasheet</td>
                                    <td><i class="fas fa-download"></i> Datasheet </td>
                                </tr>
                                <tr>
                                    <td>Datasheet</td>
                                    <td><i class="fas fa-download"></i> Datasheet </td>
                                </tr>
                                <tr>
                                    <td>Datasheet</td>
                                    <td><i class="fas fa-download"></i> Datasheet </td>
                                </tr>
                                <tr>
                                    <td>Datasheet</td>
                                    <td><i class="fas fa-download"></i> Datasheet </td>
                                </tr>
                                <tr>
                                    <td>Datasheet</td>
                                    <td><i class="fas fa-download"></i> Datasheet </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="mt-5"> <?php include('footer.php') ?></div>
</body>

</html>