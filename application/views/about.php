<?php include('header.php') ?>
<style>
    .photo {
        border-radius: 50%;
        border: 1px solid;

    }

    .centered {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
</style>

<body>
    <div class="w-100 banner_slider">
        <?php foreach ($about_list as $key => $about_item) { ?>
            <div class="article_banner">
                <img class="w-100" src="<?php echo base_url($about_item->image_path) ?>">
                <div class="centered">
                    <h1 style="text-align: center;"><?php echo getWording('index', 'about_us') ?></h1>
                    <p style="text-align: center;">
                        <?php echo getVariable($about_item, 'description') ?>
                    </p>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="container ">

        <div class="row justify-content-center mt-10">
            <div class="col-4">
                <h3 style="text-align: center;"><b><?php echo getWording('index', 'vision') ?></b></h3>
                <p> <?php echo $contactus_list->vision ?></p>
            </div>
            <div class="col-1" style="border-right: ridge;"></div>
            <div class="col-1"></div>
            <div class="col-4">
                <h3 style="text-align: center;"><b><?php echo getWording('index', 'mission') ?></b></h3>
                <p><?php echo $contactus_list->vision ?></p>
            </div>
        </div>
        <div class="row mt-10">
            <div class="col-sm-12 col-lg-12 mt-5 mb-10">
                <h3 style="text-align: center;"><b><?php echo getWording('index', 'why') ?></b></h3>
            </div>
            <?php foreach ($whyus_list as $key => $whyus_item) { ?>
                <div class="col-sm-12 col-xl-6" style="text-align:center;">
                    <div class="line">
                        <div class="col-4 mb-3">
                            <?php if ($whyus_item->thumbnail_path != '') { ?>
                                <img class="w-50 photo" src="<?php echo base_url($whyus_item->thumbnail_path) ?>" alt="">
                            <?php } else { ?>
                                <img class="w-50 photo" src="<?php echo base_url('/assets/img/no-images.png') ?>" alt="">
                            <?php } ?>
                        </div>
                        <div class="col-6" style="align-self: center;">
                            <div class="mt-3" style="text-align:left ;">
                                <B><?php echo getVariable($whyus_item, 'title') ?></b>
                                <p><?php echo getVariable($whyus_item, 'description') ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="mt-5">
        <?php include('footer.php') ?>
    </div>


    <svg aria-hidden="true" style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="icon-filter" width="100%" viewBox="0 0 25 25">
                <g>
                    <path d="M1.75,7.75h6.6803589c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891   S13.9000854,9.0452271,14.2356567,7.75H24.25C24.6640625,7.75,25,7.4140625,25,7s-0.3359375-0.75-0.75-0.75H14.2356567   c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891S8.7659302,4.9547729,8.4303589,6.25H1.75   C1.3359375,6.25,1,6.5859375,1,7S1.3359375,7.75,1.75,7.75z M11.3330078,5.4912109   c0.8320313,0,1.5087891,0.6767578,1.5087891,1.5087891s-0.6767578,1.5087891-1.5087891,1.5087891S9.8242188,7.8320313,9.8242188,7   S10.5009766,5.4912109,11.3330078,5.4912109z" fill="#1D1D1B" />
                    <path d="M24.25,12.25h-1.6061401c-0.3355713-1.2952271-1.5039063-2.2587891-2.9026489-2.2587891   S17.1741333,10.9547729,16.838562,12.25H1.75C1.3359375,12.25,1,12.5859375,1,13s0.3359375,0.75,0.75,0.75h15.088562   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891s2.5670776-0.963562,2.9026489-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,12.25,24.25,12.25z M19.7412109,14.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891S21.25,12.1679688,21.25,13   S20.5732422,14.5087891,19.7412109,14.5087891z" fill="#1D1D1B" />
                    <path d="M24.25,18.25H9.7181396c-0.3355103-1.2952271-1.5037842-2.2587891-2.9017334-2.2587891   c-1.3987427,0-2.5670776,0.963562-2.9026489,2.2587891H1.75C1.3359375,18.25,1,18.5859375,1,19s0.3359375,0.75,0.75,0.75h2.1637573   c0.3355713,1.2952271,1.5039063,2.2587891,2.9026489,2.2587891c1.3979492,0,2.5662231-0.963562,2.9017334-2.2587891H24.25   c0.4140625,0,0.75-0.3359375,0.75-0.75S24.6640625,18.25,24.25,18.25z M6.8164063,20.5087891   c-0.8320313,0-1.5087891-0.6767578-1.5087891-1.5087891s0.6767578-1.5087891,1.5087891-1.5087891   c0.8310547,0,1.5078125,0.6767578,1.5078125,1.5087891S7.6474609,20.5087891,6.8164063,20.5087891z" fill="#1D1D1B" />
                </g>
            </symbol>
        </defs>
    </svg>


</body>

</html>

<script>
    $(document).ready(function() {
        $("#play-button").hide();
        $('.banner_slider').slick({
            dots: true,
            arrows: true,
            prevArrow: "<i class='slick-prev fas fa-chevron-left'></i>",
            nextArrow: "<i class='slick-next fas fa-chevron-right'></i>"
        });

        $('.product_heightlight_slider').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: "<i class='slick-prev fas fa-chevron-left'></i>",
            nextArrow: "<i class='slick-next fas fa-chevron-right'></i>"
        });

        $('.our_customer_slider').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            arrows: false
        });
    });

    $('#play-button').click(function() {
        var mediaVideo = $("#my-video").get(0);
        if (mediaVideo.paused) {
            mediaVideo.play();
            $("#play-button").hide();
            $("#pause-button").fadeIn();
        }
    });

    $('#pause-button').click(function() {
        var mediaVideo = $("#my-video").get(0);
        if (mediaVideo.play) {
            mediaVideo.pause();
            $("#play-button").fadeIn();
            $("#pause-button").hide();
        }
    });

    $('#image_banner').click(function() {
        $('#exampleModal').modal('show');
    });

    $('#spinner-form2').click(function() {
        if ($("#spinner-form2").prop('checked') == true) {
            console.log('burger on!!!');
            $(".secd-menu").addClass("active-secd-menu");
        } else {
            console.log('burger off!!!');
            $(".secd-menu").removeClass("active-secd-menu");
        }
    });
</script>