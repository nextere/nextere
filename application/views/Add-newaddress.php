<head>
    <style>
        input[type=text],
        select,
        textarea {
            padding: 12px;
            margin-top: 6px;
            margin-bottom: 16px;
            width: 100%;
            height: 45px;
            background: #FFFFFF;
            border: 1px solid #707070;
            border-radius: 5px;
            opacity: 1;
        }

        input[type=email],
        select,
        textarea {
            padding: 12px;
            margin-top: 6px;
            margin-bottom: 16px;
            width: 100%;
            height: 45px;
            background: #FFFFFF;
            border: 1px solid #707070;
            border-radius: 5px;
            opacity: 1;
        }

       
    </style>
</head>

<body>

    <div class="container">

        <!-- Button to Open the Modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2" style="width: 200px;
        height: 45px;
        background-color: #ffffff;
        color: #000000;
        border: 1px solid #555555;
        border-radius: 23px;">
            + เพิ่มที่อยู่ใหม่
        </button>

        <!-- The Modal -->
        <div class="modal" id="myModal2">
            <div class="modal-dialog modal-dialog-scrollable" style="justify-content: center;">
                <div class="modal-content" style="min-inline-size: fit-content;">

                    <!-- Modal Header -->
                    <div class="modal-header" style=" width: 100%">
                        <h5 class="modal-title" style="padding-left:20px">เพิ่มที่อยู่ใหม่</h5>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body" style="width:100%">
                        <div class="row">
                            <div class="col-sm-6 col-xl-12 mt-2" style="text-align:left;">ชื่อ*
                                <div class="name pt-2">
                                    <input type="text" id="name" name="name" placeholder="ชื่อ" required>
                                </div>
                            </div>

                            <div class="col-sm-6 col-xl-12 mt-2" style="text-align:left;">นามสกุล*
                                <div class="last pt-2">
                                    <input type="text" id="lastname" name="lastname" placeholder="นามสกุล" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xl-12 mt-2" style="text-align:left;">เบอร์โทรติดต่อ*
                                <div class="pass pt-2">
                                    <input type="text" id="phone" onKeyPress="return isNumber(event)" min="0" maxlength="10" placeholder="เบอร์โทร" required>
                                </div>
                            </div>

                            <div class="col-sm-6 col-xl-12 mt-2" style="text-align:left;">อีเมล*
                                <div class="email pt-2">
                                    <input type="email" id="email" name="email" placeholder="อีเมล" required>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xl-12 mt-2" style="text-align:left;">ชื่อบริษัท
                                <div class="company pt-2">
                                    <input type="text" id="company" name="company" placeholder="ชื่อบริษัท">
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-12 mt-2" style="text-align:left;">สาขา
                                <div class="branch pt-2">
                                    <input type="text" id="branch" name="branch" placeholder="สาขา">
                                </div>
                            </div>


                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xl-12 mt-2" style="text-align:left;">ที่อยู่*
                                <div class="address pt-2">
                                    <input type="text" id="address" name="address" placeholder="เลขที่อยู่/อาคาร/ถนน">
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-12 mt-2" style="text-align:left;">
                                <div class="province pt-1">
                                    <label for="province">จังหวัด*</label><br>
                                    <select id="province" name="province">
                                        <option value="bangkok">กรุงเทพมหานคร</option>
                                        <option value="khonkaen">ขอนแก่น</option>
                                        <option value="chaiyaphum">ชัยภูมิ</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xl-12 mt-2" style="text-align:left;">
                                <div class="district pt-1">
                                    <label for="district">อำเภอ/เขต*</label><br>
                                    <select id="district" name="district">
                                        <option value="city">เมือง</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-12 mt-2" style="text-align:left;">
                                <div class="sub-district pt-1">
                                    <label for="sub-district">ตำบล/แขวง*</label><br>
                                    <select id="sub-district" name="sub-district">
                                        <option value="city">ในเมือง</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-xl-12 mt-2" style="text-align:left;">
                                <div class="district pt-3">
                                    <label for="district">รหัสไปรษณีย์*</label><br>
                                    <select id="district" name="district">
                                        <option value="city">10000</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h5 class="pt-3">
                                <button class="btn" data-toggle="collapse" data-target="#collapseThree2" aria-expanded="true" aria-controls="collapseThree2">
                                    <div class="want pt-1">
                                        <label class="form-check-label"><input type="checkbox" required="required"> ตั้งเป็นที่อยู่หลัก</label>
                                    </div>
                                </button>
                            </h5>
                        </div>


                        <div style="text-align: center; padding-bottom: 40px;">
                            <button class="btn3 mt-3" style="width: 252px; height: 45px; background-color: #D3D3D3; border: 0px; border-radius: 23px;">
                                บันทึก
                            </button>
                        </div>


                    </div>




                </div>
            </div>
        </div>

    </div>

</body>

<script>
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }

        return true;
    }
</script>