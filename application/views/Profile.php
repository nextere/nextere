<?php include('header.php') ?>
<?php
// echo '<pre>' ;
// print_r($user_info);
// exit();
?>
<style>
    .profile ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 100%;
        background-color: white;
        border: 1px solid #BCBCBC;
        opacity: 1;
        font-size: 16px;
    }

    .profile li a.active {
        background-color: #BCBCBC;
        color: white;
    }

    .profile li a {
        display: block;
        color: #000;
        padding: 8px 12px;
        text-decoration: none;
        font-size: 14px;
    }

    .password a {
        color: #7C7C7C;
    }
</style>


<body>
    <section class="profile">
        <div class="container mt-5">
            <div class="row">
                <div class="col-sm-12 col-xl-12 pt-5 pb-5 pl-5 mt-5" style="background-color: lightgray; text-align:left">
                    <h4><b><?php echo getWording('profile', 'my_account') ?></b></h4>
                    <p><?php echo getWording('profile', 'hi') ?> <?php echo $user_info->fullname ?>, <?php echo getWording('profile', 'detail') ?></p>
                </div>
                <div class="col-sm-4 col-xl-3 mt-3 pl-0">
                    <ul>
                        <li><a class="active" href="<?php echo base_url('Account/profile') ?>"> <img class="mr-3" style="width: 16px; height: 16px;" src="<?php echo base_url('/assets/img/icon/user-White.png') ?>"><?php echo getWording('profile', 'profile') ?></a></li>
                        <li><a href="<?php echo base_url('Account/order_history') ?>"><img class="mr-3" style="width: 20px; height: 18px;" src="<?php echo base_url('/assets/img/icon/shopping-cart.png') ?>"><?php echo getWording('profile', 'my_orders') ?></a></li>
                        <li><a href="<?php echo base_url('Account/quotation_request_history') ?>"><img class="mr-3" style="width: 18px; height: 18px;" src="<?php echo base_url('/assets/img/icon/Invoice.png') ?>"><?php echo getWording('profile', 'quotation') ?></a></li>
                        <li><a href="<?php echo base_url('Account/sending_address') ?>"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php echo base_url('/assets/img/icon/delivery-truck.png') ?>"><?php echo getWording('profile', 'shipping_address') ?></a></li>
                        <li><a href="<?php echo base_url('Account/tax_invoice_address') ?>"><img class="mr-3" style="width: 16px; height: 16px;" src="<?php echo base_url('/assets/img/icon/location-2.png') ?>"><?php echo getWording('profile', 'tax_invoice') ?></a></li>
                        <!-- <li><a href="<?php // echo base_url('Account/payment') 
                                            ?>"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php // echo base_url('/assets/img/icon/credit-card.png') 
                                                                                                                                                    ?>"><?php // echo getWording('profile', 'credit') 
                                                                                                                                                                                                                    ?></a></li> -->
                        <li><a href="<?php echo base_url('Account/datasheet') ?>"><img class="mr-3" style="width: 16px; height: 20px;" src="<?php echo base_url('/assets/img/icon/Datasheet.png') ?>"><?php echo getWording('profile', 'datasheet') ?></a></li>
                    </ul>

                </div>
                <div class="col-sm-8 col-xl-9 mt-3">
                    <div class="head">
                        <h3 style="font-weight: bold"><?php echo getWording('profile', 'profile') ?></h3>
                    </div>
                    <hr style="border-width: 3px;">
                    <?php if (!empty($user_info->token_facebook)) { ?>
                        <?php echo null ?>
                    <?php } else if (!empty($user_info->token_google)) { ?>
                        <?php echo null ?>
                    <?php } else if (!empty($user_info->token_line)) { ?>
                        <?php echo null ?>
                    <?php } else { ?>

                        <div class="row" style="padding-bottom: 20px;">
                            <div class="button1 col-sm-2 col-xl-3">
                                <button class="facebook" onClick="logInWithFacebook()" style="width: 245px; height: 45px; border: 0px; background-color: #DEDEDE; border-radius: 23px; padding: 3px; margin:10px; justify-content: center;">
                                    <img class="mr-3" style="width: 20px; height: 20px;" src="<?php echo base_url('/assets/img/icon/facebook_Black.png') ?>"> <b><?php echo getWording('profile', 'connect') ?> Facebook</b>
                                </button>
                            </div>
                            <div class="button2 col-sm-2 col-xl-3">
                                <button class="google" id="customBtn" style="width: 245px; height: 45px; border: 0px; background-color: #DEDEDE; border-radius: 23px; padding: 9px; margin:10px; justify-content: center;">
                                    <img class="mr-3" style="width: 20px; height: 20px;" src="<?php echo base_url('/assets/img/icon/Goolg_Black.png') ?>"> <b style="padding-top: 7px;"><?php echo getWording('profile', 'connect') ?> Google</b>
                                </button>
                            </div>
                            <div class="button3 col-sm-2 col-xl-3">
                                <button class="line" onclick="logIn()" style="width: 245px; height: 45px; border: 0px; background-color: #DEDEDE; border-radius: 23px; padding: 9px; margin:10px; justify-content: center;">
                                    <img class="mr-3" style="width: 23px; height: 22px; " src="<?php echo base_url('/assets/img/icon/line-black.png') ?>"> <b style="padding-top: 3px;"><?php echo getWording('profile', 'connect') ?> Line</b>
                                </button>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="col-sm-12 col-xl-12 p-3" style="border:solid #DBDBDB; width: auto; height: auto;">
                        <div class="row" style="padding:10px">

                            <div class="col-sm-6 col-xl-6">
                                <b><?php echo getWording('profile', 'name') ?></b>
                                <p class="mt-3"><?php
                                                $name = array();
                                                $name = explode(" ", $user_info->fullname);
                                                echo $name[0];
                                                ?></p>
                            </div>
                            <div class="col-sm-6 col-xl-6">
                                <b><?php echo getWording('profile', 'lastname') ?></b>
                                <p class="mt-3"><?php
                                                // $name = array();
                                                // $name = explode(" ", $user_info->fullname);
                                                if (count($name) >= 2) {
                                                    echo $name[1];
                                                } else {
                                                    echo '-';
                                                }
                                                ?></p>
                            </div>
                        </div>

                        <div class="row" style="padding:10px">
                            <div class="col-sm-6 col-xl-6">
                                <b><?php echo getWording('profile', 'gender') ?></b>
                                <p class="mt-3"><?php echo $user_info->gender ?></p>
                            </div>
                            <div class="col-sm-6 col-xl-6">
                                <b><?php echo getWording('profile', 'birthdate') ?></b>
                                <p class="mt-3"><?php echo date("d/m/Y", strtotime($user_info->birth_date)) ?></p>
                            </div>
                        </div>

                        <div class="row" style="padding:10px">

                            <div class="col-sm-6 col-xl-6">
                                <b><?php echo getWording('profile', 'tax') ?></b>
                                <p class="mt-3"><?php echo $user_info->tax_id ?></p>
                            </div>
                            <div class="col-sm-6 col-xl-6">
                                <b><?php echo getWording('profile', 'phone') ?></b>
                                <p class="mt-3"><?php echo $user_info->telephone ?></p>
                            </div>
                        </div>

                        <div class="row" style="padding:10px">

                            <div class="col-sm-6 col-xl-6">
                                <b><?php echo getWording('profile', 'email') ?></b>
                                <p class="mt-3"><?php echo $user_info->email ?></p>
                            </div>
                            <div class="col-sm-6 col-xl-2">
                                <b><?php echo getWording('profile', 'password') ?></b>
                                <p class="mt-3">*********
                                    <?php
                                    // echo $user_info->password 
                                    ?></p>
                            </div>
                            <!-- <div class="password col-sm-6 col-xl-3">
                                <a href="#" disabled>ส่งคำขอเปลี่ยนรหัสผ่าน</a>
                            </div> -->

                            <div class="want p-4">
                                <?php if (( $user_info->follow) == 'ACTIVATE') { ?>
                                    <input type="checkbox" name="is_default" id="ACTIVATE" value="1" checked="true" style="pointer-events: none;"> <?php echo getWording('profile', 'follow') ?>
                                <?php } else { ?>
                                    <input type="checkbox" name="is_default" id="NULL" value="0" style="pointer-events: none;" > <?php echo getWording('profile', 'follow') ?>
                                <?php } ?>
                            </div>
                        </div>
                        <input style="display: none;" type="text" id="token_facebook" name="token_facebook" value="<?php echo $user_info->token_facebook ?>">
                        <input style="display: none;" type="text" id="token_line" name="token_line" value="<?php echo $user_info->token_line ?>">
                        <input style="display: none;" type="text" id="fullname" name="fullname" value="<?php echo $user_info->fullname ?>">
                        <input style="display: none;" type="text" id="gender" name="gender" value="<?php echo $user_info->gender ?>">
                        <input style="display: none;" type="text" id="telephone" name="telephone" value="<?php echo $user_info->telephone ?>">
                        <input style="display: none;" type="text" id="email" name="email" value="<?php echo $user_info->email ?>">
                        <input style="display: none;" type="text" id="birthday" name="birthday" value="<?php echo $user_info->birth_date ?>">
                        <input style="display: none;" type="text" id="tax" name="tax" value="<?php echo $user_info->tax_id ?>">
                        <input style="display: none;" type="text" id="password" name="password" value="<?php echo $user_info->password ?>">
                    </div>

                    <div class="button4 pt-4" style="text-align: center;">
                        <a href="<?php echo base_url('Account/profile_edit') ?>">
                            <button class="edit" style="width: 205px; height:45px; border:solid 1px; border-radius: 23px; padding: 9px; margin:10px; background-color:#FFFFFF">
                                <?php echo getWording('profile', 'edit') ?>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </section>


    <div class="mt-5">
        <?php include('footer.php') ?>
    </div>
    <script src="https://static.line-scdn.net/liff/edge/2/sdk.js"></script>

    <script>
        function logOut() {
            liff.logout()
            window.location.reload()
        }

        function logIn() {
            liff.login({
                redirectUri: window.location.href
            })
        }
        async function getUserProfile() {
            const profile = await liff.getProfile()
            console.log('profile', profile)
            line_token = profile.userId;
            $("#name").val(profile.displayName);
            $(".register_response").text('กรุณากรอกข้อมูลให้ครบถ้วน');
            $('#staticBackdrop').modal('show');
            $('#password_remove').hide();
            $('#confirm_password_remove').hide();
            // $('#social').hide();
            if (liff.isLoggedIn() == true) {
                logOut();
            }

            var data = {
                "line_token": line_token,
                "fullname": $("#fullname").val(),
                "email": $("#email").val(),
                "telephone": $("#telephone").val(),
                "gender": $("#gender").val(),
                "birthday": $("#birthday").val(),
                "tax": $("#tax").val(),
            }
            // console.log(data);

            var send_data = {
                "url": "<?php echo base_url('/Account/profile_line') ?>",
                "method": "POST",
                "data": data
            }

            var base_url = 'location.href="' + '<?php echo base_url('/Account/profile') ?>' + '"';

            $.ajax(send_data).done(function(response) {
                var base_url = 'location.href="' + '<?php echo base_url('/Account/profile') ?>' + '"';
                if (response.code == "0x0000-00000") {
                    $(".register_response").text(response.message);
                    window.location.href = "<?php echo base_url('/Account/profile') ?>";
                } else if (response.code == "0x000A-AU001") {
                    $('.register_success').addClass('d-none');
                    $('.register_success').removeClass('d-block');
                    $('.register_fail').addClass('d-block');
                    $('.register_fail').removeClass('d-none');
                    $(".register_response").text(response.message);
                }
                $('#staticBackdrop').modal('show');
                console.log(response);
            });
            // console.log('not submit!!');
            // console.log('birdth_date', $('#telephone').val());
            return false;

        }

        async function main() {
            await liff.init({
                liffId: "1656346484-ajAlodXR"
            });
            if (liff.isInClient()) {
                getUserProfile();
            } else {
                if (liff.isLoggedIn() == false) {
                    // getUserProfile();
                    console.log('not logged in');
                    console.log('liff_id', liff.init);
                } else {
                    getUserProfile();
                }
            }
        }
        main()
        // console.log(login());
    </script>


    <meta name="google-signin-client_id" content="441404752301-1a5fd972o9ab8ps1960ksk63od66p2n8.apps.googleusercontent.com">
    <meta name="google-signin-scope" content="profile email">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="https://apis.google.com/js/api:client.js"></script>
    <!-- ------------------------------------------------------------------------------------ -->

    <script src="https://apis.google.com/js/api:client.js"></script>
    <script>
        var googleUser = {};
        var startApp = function() {
            gapi.load('auth2', function() {
                auth2 = gapi.auth2.init({
                    client_id: '441404752301-1a5fd972o9ab8ps1960ksk63od66p2n8.apps.googleusercontent.com',
                    cookiepolicy: 'single_host_origin'
                });
                attachSignin(document.getElementById('customBtn'));
            });
        };

        function attachSignin(element) {
            // console.log(element.id);
            auth2.attachClickHandler(element, {},
                function(googleUser) {
                    document.getElementById('name').innerText = "Signed in: "
                    var profile = googleUser.getBasicProfile();
                    console.log(profile);
                    var email = profile.getEmail();
                    var token = googleUser.getAuthResponse().id_token;
                    // google_token = token
                    var data2 = {
                        // 'email': email,
                        'token': profile.US,
                        "fullname": $("#fullname").val(),
                        "email": $("#email").val(),
                        "telephone": $("#telephone").val(),
                        "gender": $("#gender").val(),
                        "birthday": $("#birthday").val(),
                        "tax": $("#tax").val(),
                    };
                    console.log(data2);
                    var formData = new FormData();
                    for (var key in data2) {
                        formData.append(key, data2[key]);
                    }

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url('/Account/profile_google') ?>",
                        data: JSON.stringify(data2),
                        processData: false,
                        contentType: "application/json; charset=utf-8",
                    }).done(function(response) {
                        console.log(response);
                        if (response.code == "0x0000-00000") {
                            window.location.href = "<?php echo base_url('/Account/profile') ?>";
                        }
                    });
                }, );
        }
    </script>

    <div id="name"></div>
    <script>
        startApp();
    </script>

    <!-- --------------------------------------------------------------------------------- -->
    <script>
        jQuery(document).ready(function() {
            var facebook_name = "";
            var facebook_token;
            var google_token;
            var line_token;

            window.fbAsyncInit = function() {
                FB.init({
                    appId: '295555285694750',
                    cookie: true, // This is important, it's not enabled by default
                    version: 'v2.5'
                });
            };

            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "https://connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        });
    </script>

    <script type="text/javascript">
        logInWithFacebook = function() {
            FB.login(function(response) {
                var authResponse = '';
                if (response.authResponse) {
                    FB.getLoginStatus(function(response) {
                        authResponse = response.authResponse.accessToken;
                        fetch('https://graph.facebook.com/v2.5/me?fields=email&access_token=' + authResponse).then((resp) => console.log(resp.json().then(data => {
                            data.token = authResponse;
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url('/Account/profile_facebook') ?> ",
                                data: {
                                    "data": data["id"],
                                    "fullname": $("#fullname").val(),
                                    "email": $("#email").val(),
                                    "telephone": $("#telephone").val(),
                                    "gender": $("#gender").val(),
                                    "birthday": $("#birthday").val(),
                                    "tax": $("#tax").val(),
                                },

                            }).done(function(data) {
                                var base_url = 'location.href="' + '<?php echo base_url('/Account/profile') ?>' + '"';
                                console.log(data);
                                if (data.code == "0x0000-00000") {
                                    window.location = '<?php echo base_url('/Account/profile') ?>';
                                }
                            });
                        })))
                    });
                } else {
                    alert('User cancelled login or did not fully authorize.');
                }
            }, {
                scope: 'email'
            });
            return false;
        };
    </script>



</body>