<?php include('header.php') ?>

<style>
    .card ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 100%;
        background-color: white;
        border: 1px solid #BCBCBC;
        opacity: 1;
        font-size: 16px;
    }

    .card li a.active {
        background-color: #BCBCBC;
        color: white;
    }

    .card li a {
        display: block;
        color: #000;
        padding: 8px 12px;
        text-decoration: none;
        font-size: 14px;
    }

    input[type=text],
    select,
    textarea {
        padding: 12px;
        margin-top: 6px;
        margin-bottom: 16px;
        width: 100%;
        height: 45px;
        border: 1px solid #707070;
        border-radius: 5px;
        opacity: 1;
    }

    input[type=email],
    select,
    textarea {
        padding: 12px;
        margin-top: 6px;
        margin-bottom: 16px;
        width: 100%;
        height: 45px;
        border: 1px solid #707070;
        border-radius: 5px;
        opacity: 1;
    }
</style>


<body>
    <section class="card">
        <div class="container mt-5">
            <div class="row">
                <div class="col-sm-12 col-xl-12 pt-5 pb-5 pl-5" style="background-color: lightgray; text-align:left">
                    <h4><b><?php echo getWording('profile', 'my_account') ?></b></h4>
                    <p><?php echo getWording('profile', 'hi') ?> <?php echo $user_info->fullname ?>, <?php echo getWording('profile', 'detail') ?></p>
                </div>
                <div class="col-sm-2 col-xl-2 mt-3 pl-0">
                    <ul>
                        <li><a href="<?php echo base_url('Account/profile') ?>"> <img class="mr-3" style="width: 16px; height: 16px;" src="<?php echo base_url('/assets/img/icon/user.png') ?>"><?php echo getWording('profile', 'profile') ?></a></li>
                        <li><a href="<?php echo base_url('Account/order_history') ?>"><img class="mr-3" style="width: 20px; height: 18px;" src="<?php echo base_url('/assets/img/icon/shopping-cart.png') ?>"><?php echo getWording('profile', 'my_orders') ?></a></li>
                        <li><a href="<?php echo base_url('Account/quotation_request_history') ?>"><img class="mr-3" style="width: 18px; height: 18px;" src="<?php echo base_url('/assets/img/icon/Invoice.png') ?>"><?php echo getWording('profile', 'quotation') ?></a></li>
                        <li><a href="<?php echo base_url('Account/sending_address') ?>"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php echo base_url('/assets/img/icon/delivery-truck.png') ?>"><?php echo getWording('profile', 'shipping_address') ?></a></li>
                        <li><a href="<?php echo base_url('Account/tax_invoice_address') ?>"><img class="mr-3" style="width: 16px; height: 16px;" src="<?php echo base_url('/assets/img/icon/location-2.png') ?>"><?php echo getWording('profile', 'tax_invoice') ?></a></li>
                        <li><a class="active" href="<?php echo base_url('Account/payment') ?>"><img class="mr-3" style="width: 18px; height: 16px;" src="<?php echo base_url('/assets/img/icon/credit-card-White.png') ?>"><?php echo getWording('profile', 'credit') ?></a></li>
                        <li><a href="<?php echo base_url('Account/datasheet') ?>"><img class="mr-3" style="width: 16px; height: 20px;" src="<?php echo base_url('/assets/img/icon/Datasheet.png') ?>"><?php echo getWording('profile', 'datasheet') ?></a></li>
                    </ul>
                </div>
                <div class="col-sm-2 col-xl-10 mt-3">

                    <div class="head">
                        <h3 style="font-weight: bold"></i><?php echo getWording('profile', 'credit') ?></h3>
                    </div>
                    <hr style="border-width: 3px;">

                    <!-- <div class="row" style="padding-bottom: 20px;">
                   .
                </div> -->

                    <div class="row" style="padding-bottom: 20px;">
                        <div class="1" style="width: 100%; height: auto; border: solid #DBDBDB;">

                            <div class="row p-3">
                                <div class="col-10">
                                    <div class="CardNo">Visa เลขที่บัตร xxxxxxxxxx</div>
                                    <div class="CardName">ชื่อบนบัตร --------</div>
                                    <div class="CardExp">วันหมดอายุ xx/xx</div>
                                </div>
                                <div class="col-2" style="text-align: right;">
                                    <i class="fas fa-trash-alt ml-3"></i>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="button4 ">
                        <a href="<?php echo base_url('Account/payment_add') ?>">
                            <button class="edit" style="width: 199px; height:45px; border:solid 1px; border-radius: 23px; padding: 9px; margin:10px; background-color:#FFFFFF;">
                                <?php echo getWording('profile', 'add_credit') ?>
                            </button>
                        </a>
                    </div>

                </div>



            </div>
        </div>





    </section>
    <div class="mt-5">
        <?php include('footer.php') ?>
    </div>
</body>