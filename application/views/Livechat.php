<style>
    #sy-whatshelp {
        right: 25px;
        bottom: 15px;
        position: fixed;
        z-index: 1000;
    }

    #sy-whatshelp a {
        position: relative;
    }

    #sy-whatshelp a.sywh-open-services {
        background-color: gray;
        color: #fff;
        line-height: 55px;
        margin-top: 10px;
        border: none;
        cursor: pointer;
        width: 200px;
        height: 55px;
        text-align: center;
        box-shadow: 2px 2px 8px -3px #000;
        border-radius: 50px 50px;
        -webkit-border-radius: 50px 50px;
        -moz-border-radius: 50px 50px;
        -ms-border-radius: 50px 50px;
        display: inline-block;
        transition: .3s;
    }

    #sy-whatshelp a.sywh-open-services i {
        line-height: 55px;
    }

    #sy-whatshelp a.sywh-open-services i.fa-times {
        display: none;
    }

    #sy-whatshelp .sywh-services {
        width: 55px;
        height: auto;
    }

    #sy-whatshelp .sywh-services a {
        display: none;
    }

    #sy-whatshelp .sywh-services a i {
        /* background-color: #129bf4; */
        background-color: gray;
        color: #fff;
        line-height: 55px;
        border: none;
        cursor: pointer;
        width: 55px;
        height: 55px;
        text-align: center;
        box-shadow: 2px 2px 8px -3px #000;
        border-radius: 100%;
        -webkit-border-radius: 100%;
        -moz-border-radius: 100%;
        -ms-border-radius: 100%;
        font-size: 23px;
        transition: .3s;


    }

    #sy-whatshelp a.sywh-open-services span {
        font-size: 16px;
        margin-left: 10px;
    }

    #sy-whatshelp .sywh-services a.email i {
        /* background-color: #b92b27; */
        background-color: gray;
    }

    #sy-whatshelp .sywh-services a.instagram i {
        /* background-color: #e4405f; */
        background-color: gray;
    }

    #sy-whatshelp .sywh-services a.messenger i {
        /* background-color: #0084ff; */
        background-color: gray;
    }

    #sy-whatshelp .sywh-services a.whatsapp i {
        /* background-color: #25d366; */
        background-color: gray;
    }

    #sy-whatshelp .sywh-services a.call i {
        /* background-color: #ff6600; */
        background-color: gray;
    }


    div.sticky {
        position: sticky;
        line-height: 30px;
        bottom: 0;
        background-color: lightgray;
        padding: 15px;
        color: #000;
        width: 100%;

    }

    div.compare {
        position: sticky;
        bottom: 0;
        background-color: lightgray;
        padding: 15px;
        color: #000;
        width: 100%;

    }

    .foot_sticky {
        position: sticky;
        line-height: 30px;
        bottom: 0;
        color: #000;
        width: 100%;
        padding: 0;

    }

    .pointer {
        cursor: pointer;
        text-decoration: underline;
    }

    /* .close-social {
        right: 35px;
        bottom: 150px;
        position: fixed;
        z-index: 1000;
    } */
    @media screen and (max-width: 500px) {
        #sy-whatshelp a.sywh-open-services.show{
            width: 40px !important;
            height: 40px !important;
        }
        #sy-whatshelp a.sywh-open-services {
            background-color: gray;
            color: #fff;
            line-height: 40px;
            margin-top: 0px;
            border: none;
            cursor: pointer;
            width: 150px !important;
            height: 40px !important;
            text-align: center;
            box-shadow: 2px 2px 8px -3px #000;
            border-radius: 50px 50px;
            -webkit-border-radius: 50px 50px;
            -moz-border-radius: 50px 50px;
            -ms-border-radius: 50px 50px;
            display: inline-block;
            transition: .3s;
        }

        #sy-whatshelp a.sywh-open-services span {
            font-size: 12px !important;
            margin-left: 10px;
        }

        #sy-whatshelp a.sywh-open-services i {
            line-height: 40px;
        }

        .fa-comments {
            font-size: 12px !important;
        }

        #sy-whatshelp .sywh-services a i {
            /* background-color: #129bf4; */
            background-color: gray;
            color: #fff;
            line-height: 42px;
            border: none;
            cursor: pointer;
            width: 40px;
            height: 40px;
            text-align: center;
            box-shadow: 2px 2px 8px -3px #000;
            border-radius: 100%;
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            -ms-border-radius: 100%;
            font-size: 23px;
            transition: .3s;
        }

        .fa-phone {
            font-size: 18px !important;
        }

        .fa-facebook-messenger {
            font-size: 18px !important;
        }

        .fa-line {
            font-size: 18px !important;
        }
    }
</style>
<div class="mb-5" id="sy-whatshelp" style="text-align: -webkit-right;">
    <div class="sywh-services ">
        <a href="tel:+66<?php echo ($contactus_list->telephone) ?>" class="call" data-placement="left"><i class="fa fa-phone mb-2"></i></a>
        <a href="<?php echo ($contactus_list->fackbook_messenger) ?>" class="messenger" data-placement="left" target="_blank">
            <i class="fab fa-facebook-messenger mb-2"></i></a>
        <a href="<?php echo ($contactus_list->line) ?>" class="whatsapp" data-placement="left" target="_blank">
            <i class="fab fa-line"></i>
        </a>
        <div class="close-social">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
    </div>

    <a class="sywh-open-services mb-8 mt-2" data-placement="left">
        <i class="fa fa-comments fa-2x"><Span style=" font-family: 'Prompt';"><?php echo getWording('index', 'contact') ?></Span></i><i class="fa fa-times "></i>
    </a>
</div>
<div class="foot_sticky ">
    <div class="compare" id="compare" style="display: none;">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-sm-12 col-lg-7" style="place-self: center;">
                            <h5 class="compare_amount"><?php echo getWording('index', 'compare_product') ?> (0/3)</h5>
                        </div>
                        <div class="col-sm-12 col-lg-5" style="text-align-last: center;">
                            <button type="button" class="btn btn-secondary mr-5 " id="submit_compare" style="width: 120px;height: 40px; border-radius: 25px;"><?php echo getWording('index', 'compare') ?></button>
                            <button type="button" id="reset" class="btn " style="width: 120px;height: 40px; border-radius: 25px;border: 1px solid;"><?php echo getWording('index', 'clear') ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sticky" id="cookie" style="z-index: 9999;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-xl-11" style="place-self: center;">
                    <span><?php echo getWording('index', 'cookies') ?></span>
                    <span class="pointer" data-bs-toggle="modal" data-bs-target="#example"> <?php echo getWording('index', 'cookie_policy') ?></span>
                </div>
                <div class="col-12 col-xl-1" style="text-align: -webkit-right;">
                    <button id="btn-cookie" type="button" class="btn btn-light w-100 " onclick="acceptCookieConsent();" style="width: 120px;height: 40px; border-radius: 25px;"><?php echo getWording('index', 'got') ?></button>
                </div>
            </div>
        </div>
    </div>

</div>




<!-- Modal -->
<div class="modal fade " id="example" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="color: black;">
                <h5 class="modal-title" id="exampleModalLabel" style="padding-left: 28%;">นโยบายความเป็นส่วนตัวของบริษัท เนกซ์เทียร์ จำกัด</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" style="color: black;">
                <div class="container">
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px; line-height: 107%; font-family: Prompt;">นโยบายความเป็นส่วนตัวของบริษัท เนกซ์เทียร์ จำกัด</span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><strong><span style="line-height: 107%; font-family: Prompt;">1. บทนำ</span></strong></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">1.1&nbsp;เมื่อท่านใช้บริการของเราท่านได้ไว้วางใจให้เรารักษาข้อมูลของท่าน เราจึงถือว่านี่คือความรับผิดชอบที่ยิ่งใหญ่และพยายามอย่างยิ่งที่จะปกป้องข้อมูลของท่าน รวมถึงมอบอำนาจในการควบคุมข้อมูลให้แก่ท่าน บริษัท เนกซ์เทียร์ จำกัด เราดูแลความเป็นส่วนตัวของท่านอย่างจริงจัง เราให้คำมั่นว่าจะปฏิบัติตามพระราชบัญญัติคุ้มครองข้อมูลส่วนบุคคล พ.ศ. 2562 (PDPA)ที่มีผลบังคับใช้กับทางบริษัททั้งหมด</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">1.2&nbsp;ในระหว่างการให้บริการแก่ท่าน เราจะเก็บรวบรวม ใช้ เปิดเผย เก็บรักษา และ/หรือประมวลผลข้อมูล โดยรวมถึงข้อมูลส่วนบุคคลของท่าน</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">1.3&nbsp;นโยบายความเป็นส่วนตัวฉบับนี้มีขึ้นเพื่อให้ท่านทราบเกี่ยวกับวิธีการที่เราเก็บรวบรวม ใช้ เปิดเผย เก็บรักษาและ/หรือประมวลผลข้อมูลที่เราเก็บรวบรวมและได้รับระหว่างการให้บริการหรือการเข้าถึงเว็บไซต์ของท่าน ผู้ซึ่งเป็นลูกค้าของเรา เราจะเก็บรวบรวม ใช้ เปิดเผย เก็บรักษาและ/หรือประมวลผลข้อมูลส่วนบุคคลของท่านตามนโยบายความเป็นส่วนตัวนี้เท่านั้น</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">1.4&nbsp;ท่านจำเป็นจะต้องอ่านนโยบายความเป็นส่วนตัวนี้รวมถึงงประกาศแจ้งอื่นใดที่เราอาจมีขึ้นเฉพาะกาล ขณะที่เราทำการเก็บรวบรวม ใช้ เปิดเผยและประมวลผลข้อมูลส่วนบุคคลเกี่ยวกับท่าน เพื่อให้ท่านได้รับรู้ว่าเราใช้ข้อมูลส่วนบุคคลของท่านอย่างไรและเพื่ออะไร</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">1.5&nbsp;เมื่อคลิกหรือเลือกปุ่ม &ldquo;สมัคร&rdquo; &ldquo;ข้าพเจ้าตกลงยอมรับนโยบายความเป็นส่วนตัวของ&nbsp; &nbsp;บริษัท เนกซ์เทียร์ จำกัด&rdquo; &ldquo;ข้าพเจ้าตกลงและยินยอมให้เก็บรวบรวม ใช้ เปิดเผย เก็บรักษาและ/หรือประมวลผลซึ่งข้อมูลส่วนบุคคลของข้าพเจ้าเพื่อวัตถุประสงค์ที่ระบุไว้ในนี้และภายใต้ข้อกำหนดของนโยบายความเป็นส่วนตัวของ บริษัท เนกซ์เทียร์ จำกัด&rdquo; หรือข้อความคล้ายคลึงกันที่มีอยู่ในหน้าลงทะเบียนของ บริษัท เนกซ์เทียร์ จำกัด ท่านรับทราบว่าท่านได้รับแจ้งและเข้าใจนโยบายความเป็นส่วนตัวฉบับนี้และท่านยังได้ตกลงและยินยอมให้เก็บรวบรวม ใช้ เปิดเผย และประมวลผลข้อมูลส่วนบุคคลของท่านตามที่ได้อธิบายและอยู่ภายใต้ข้อกำหนดนี้</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">1.6&nbsp;เราอาจแก้ไขนโยบายความเป็นส่วนตัวนี้ให้ทันสมัยเป็นครั้งคราว โดยการเปลี่ยนแปลงใด ๆ ในนโยบายความเป็นส่วนตัวฉบับนี้ในอนาคตจะถูกประกาศให้ทราบไว้ในหน้าเว็บนี้และเราจะแจ้งให้ท่านทราบหากเป็นการเปลี่ยนแปลงอย่างมีนัยยะสำคัญ (ตามความเหมาะสม และ/หรือได้รับอนุญาตภายใต้กฎหมายท้องถิ่น) และเมื่อท่านยังคงใช้บริการ เข้าถึงเว็บไซต์หรือใช้บริการใด ๆ รวมถึงทำคำสั่งซื้อบนเว็บไซต์ต่อไปหลังจากนั้น ให้ถือว่าท่านได้รับทราบและยอมรับการเปลี่ยนแปลงนโยบายความเป็นส่วนตัวฉบับนี้โดยเราแล้ว กรุณาตรวจสอบนโยบายความเป็นส่วนตัวเป็นประจำเพื่อให้ทราบถึงการปรับเปลี่ยนให้ทันสมัยหรือการแก้ไขใด ๆ กับนโยบายความเป็นส่วนตัวนี้</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">1.7&nbsp;นโยบายความเป็นส่วนตัวฉบับนี้มีผลบังคับใช้ร่วมกับประกาศ ข้อความสัญญาและข้อความความยินยอมอื่น ๆ ที่เกี่ยวข้องกับการการเก็บรวบรวม เก็บรักษา ใช้และ/หรือการประมวลผลข้อมูลส่วนบุคคลโดยเราและไม่มีจุดประสงค์ในการแทนที่ประกาศ ข้อความสัญญาและข้อความยินยอมดังกล่าวเว้นแต่ว่าเราระบุไว้เป็นอย่างอื่นโดยชัดแจ้ง</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">1.8 ท่านสามารถเข้าใช้เว็บไซต์และสำรวจเว็บไซต์ได้โดยไม่ต้องให้ข้อมูลส่วนบุคคล อย่างไรก็ตามท่านจำเป็นต้องลงทะเบียนบัญชีผู้ใช้หากท่านต้องการใช้บริการ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><strong><span style="line-height: 107%; font-family: Prompt;">2. ข้อมูลส่วนบุคคลที่เราเก็บรวบรวมจากท่าน</span></strong></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">2.1&nbsp;ข้อมูลส่วนบุคคลหมายถึง ข้อมูลใด ๆ ไม่ว่าจะถูกบันทึกในรูปแบบเอกสารหรือไม่ ซึ่งมีการแสดงข้อมูลระบุตัวตน หรือทำให้ผู้ที่ถือข้อมูลนั้นสามารถยืนยันตัวบุคคลได้โดยตรง หรืออย่างสมเหตุสมผล หรือเมื่อนำไปรวมกับข้อมูลอื่น ๆ จะทำให้สามารถระบุตัวตนของบุคคลได้อย่างแน่นอนไม่ว่าทางตรงหรือทางอ้อม</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">2.2&nbsp;ระหว่างระยะเวลาที่ท่านใช้เว็บไซต์และใช้บริการจากเรา เราอาจเก็บรวบรวมข้อมูลส่วนบุคคลจากท่านซึ่งรวมถึงแต่ไม่จำกัดเพียง</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ก) ข้อมูลระบุตัวตน เช่น ชื่อ เพศ รูปโปรไฟล์ และวันเกิดของท่าน</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ข) ข้อมูลติดต่อ เช่น ที่อยู่สำหรับจัดส่งใบเรียกชำระเงิน ที่อยู่สำหรับส่งพัสดุ ที่อยู่อีเมลและหมายเลขโทรศัพท์</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ค) ข้อมูลบัญชี เช่น หมายเลขบัญชีธนาคารและรายละเอียดการชำระเงิน</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ง) ข้อมูลการทำธุรกรรม เช่น รายละเอียดเกี่ยวกับคำสั่งซื้อและการชำระเงินถึงท่านและจากท่าน และรายละเอียดอื่นของสินค้าและบริการที่ท่านได้ซื้อหรือได้รับผ่านเว็บไซต์</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(จ) ข้อมูลทางเทคนิค เช่น ที่อยู่ไอพี (IP address) ข้อมูลการเข้าใช้ ชนิดและรุ่นของบราวเซอร์ การตั้งเขตเวลาและสถานที่ ชนิดและรุ่นของโปรแกรมเสริมของบราวเซอร์ ระบบปฏิบัติการและเว็บไซต์ รหัสสากลประจำอุปกรณ์เคลื่อนที่ (IMEI) หมายเลขประจำเครื่อง และข้อมูลและเทคโนโลยีอื่นของอุปกรณ์ที่ท่านใช้ในการเข้าถึงเว็บไซต์</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ฉ) ข้อมูลโปรไฟล์ เช่น ชื่อผู้ใช้และรหัสผ่านของท่าน การซื้อหรือคำสั่งซื้อของท่าน ความสนใจของท่าน สิ่งที่ท่านสนใจ ผลตอบรับและคำตอบของแบบสอบถาม</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ช) ข้อมูลการใช้ เช่น ข้อมูลว่าท่านใช้เว็บไซต์ สินค้าและการบริการอย่างไร หรือดูเนื้อหาใดบนเว็บไซต์</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ซ) ข้อมูลสถานที่ เช่น เมื่อท่านบันทึกและแบ่งปันซึ่งสถานที่ของท่านกับเราในรูปแบบของรูปถ่ายหรือวีดีโอและอัพโหลดเนื้อหาดังกล่าวขึ้นบนเว็บไซต์</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ฌ) ข้อมูลทางชีวภาพ เช่น การทำงานที่เกี่ยวกับใบหน้าหรือส่วนอื่นของร่างกายและเสียงของท่านเองและ/หรือผู้อื่นในวีดีโอของท่านเมื่อท่านอัพโหลดวีดีโอขึ้นบนเว็บไซต์</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ญ) ข้อมูลทางการตลาดและการสื่อสาร เช่น ช่องทางที่ท่านพึงพอใจในการได้รับการทำการตลาดจากเราและบุคคลภายนอก และวิธีการสื่อสารที่ท่านพึงพอใจ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">2.3&nbsp;ระหว่างระยะเวลาที่ท่านใช้เว็บไซต์และใช้บริการจากเรา เราอาจได้รับข้อมูลส่วนบุคคลจากท่านในสถานการณ์ดังต่อไปนี้</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ก) เมื่อท่านได้สร้างบัญชีผู้ใช้กับเรา</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ข) เมื่อท่านได้ซื้อสินค้าใดที่มีอยู่บนเว็บไซต์</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ค) เมื่อท่านได้ใช้ฟีเจอร์หรือฟังก์ชั่นการทำงานใดที่มีอยู่บนเว็บไซต์หรือการบริการ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ง) เมื่อท่านได้บันทึกเนื้อหาที่สร้างขึ้นโดยผู้ใช้ใด ๆ ที่อัพโหลดอยู่บนเว็บไซต์</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(จ) เมื่อท่านใช้งานฟังก์ชั่นการพูดคุย (แชท) บนเว็บไซต์</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ฉ) เมื่อท่านได้ตกลงรับข่าวสารหรือสื่อการตลาดของเรา</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ช) เมื่อท่านได้เข้าร่วมการแข่งขัน โปรโมชั่นหรือการสำรวจข้อมูล</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ซ) เมื่อท่านได้เข้าร่วมในกิจกรรมหรือโครงการใดบนเว็บไซต์</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ฌ) เมื่อท่านได้เข้าสู่ระบบจากบัญชีผู้ใช้ของท่านบนเว็บไซต์หรือสื่อสารกับเราผ่านบริการหรือแอปพลิเคชันภายนอก เช่น เฟซบุ๊ก กูเกิ้ล ไลน์ ฯลฯ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ญ) เมื่อผู้ใช้อื่นในเว็บไซต์ได้แสดงความคิดเห็นใด ๆ บนเนื้อหาของท่านที่อัพโหลดอยู่บนเว็บไซต์หรือเมื่อท่านได้แสดงความคิดเห็นใดๆ บนเนื้อหาของผู้ใช้อื่นที่อัพโหลดอยู่บนเว็บไซต์&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ฎ) เมื่อบุคคลภายนอกได้ยื่นคำร้องเรียนเกี่ยวกับท่านหรือเนื้อหาที่ท่านได้แสดงไว้บนเว็บไซต์&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ฏ) เมื่อท่านได้ติดต่อกับเราทางออฟไลน์ รวมไปถึงเมื่อท่านติดต่อกับตัวแทนฝ่ายบริการลูกค้าของเราที่เป็นบุคคลภายนอก</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">2.4&nbsp;ระหว่างระยะเวลาที่ท่านใช้เว็บไซต์และและใช้บริการ ท่านได้ให้ความยินยอมอย่างชัดแจ้งในการโอนข้อมูลส่วนบุคคลของท่านจากบุคคลภายนอกมายัง บริษัท เนกซ์เทียร์ จำกัด เพื่อวัตถุประสงค์ที่ระบุไว้ในนโยบายความเป็นส่วนตัวฉบับนี้หรือข้อกำหนดอื่นใด</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">2.5&nbsp;ท่านต้องส่งข้อมูลส่วนบุคคลซึ่งถูกต้องและไม่ก่อให้เกิดความเข้าใจผิดเท่านั้น และท่านต้องหมั่นทำให้ข้อมูลดังกล่าวเป็นปัจจุบันและแจ้งให้เราทราบหากมีการเปลี่ยนแปลงข้อมูลส่วนบุคคลใด ๆ ที่ท่านได้ให้ไว้กับเรา เรามีสิทธิที่จะร้องขอเอกสารยืนยันความถูกต้องของข้อมูลส่วนบุคคลที่ท่านได้เคยให้ไว้ เพื่อเป็นส่วนหนึ่งในขั้นตอนการตรวจสอบลูกค้าของเรา</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">2.6&nbsp;เราสามารถเก็บรวบรวมข้อมูลส่วนบุคคลของท่านได้ต่อเมี่อท่านสมัครใจที่จะส่งข้อมูลส่วนบุคคลให้กับเราหรือตามที่ระบุไว้เป็นอย่างอื่นภายใต้นโยบายความเป็นส่วนตัวนี้เท่านั้น อย่างไรก็ตามหากท่านเลือกที่จะไม่ให้ข้อมูลส่วนบุคคลของท่านแก่เราหรือท่านได้ถอนความยินยอมของท่านในการให้เราใช้ข้อมูลส่วนบุคคลของท่านในภายหลัง เราอาจจะไม่สามารถให้บริการแก่ท่านหรืออนุญาตให้ท่านเข้าถึงเว็บไซต์ต่อไปได้</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">2.7&nbsp;ท่านสามารถเข้าถึงและแก้ไขข้อมูลที่ท่านเคยส่งให้เราให้เป็นปัจจุบันได้ไม่ว่าในเวลาใดตามที่ระบุไว้ด้านล่าง</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">2.8&nbsp;หากท่านให้ข้อมูลส่วนบุคคลของบุคคลภายนอกใดแก่เรา ท่านรับรองและรับประกันว่าท่านได้รับความยินยอม หนังสืออนุญาต และการอนุญาตตามที่จำเป็นจากบุคคลภายนอกดังกล่าวให้นำมาเปิดเผย และโอนข้อมูลส่วนบุคคลของเขาหรือเธอให้แก่เรา และให้เราเก็บรวบรวม เก็บรักษา ใช้และเปิดเผยข้อมูลดังกล่าวตามนโยบายความเป็นส่วนตัวฉบับนี้</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">2.9&nbsp;หากท่านสมัครเป็นผู้ใช้เว็บไซต์ของเราโดยใช้บัญชีเครือข่ายสังคมใดของท่าน หรือเชื่อมต่อบัญชีเนกซ์เทียร์ของท่านกับบัญชีเครือข่ายสังคมของท่านหรือใช้ฟีเจอร์อื่นของเครือข่ายสังคมเนกซ์เทียร์ เราจะสามารถเข้าถึงข้อมูลส่วนบุคคลเกี่ยวกับท่านซึ่งท่านได้สมัครใจให้ไว้แก่ผู้ให้บริการเครือข่ายสังคมตามนโยบายของผู้ให้บริการดังกล่าวและเราจะจัดการข้อมูลส่วนบุคคลของท่านตามนโยบายความเป็นส่วนตัวฉบับนี้</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><strong><span style="line-height: 107%; font-family: Prompt;">3. การใช้และเปิดเผยข้อมูลส่วนบุคคล</span></strong></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">3.1 ข้อมูลส่วนบุคคลที่เราเก็บรวบรวมจากท่านหรือผ่านบุคคลภายนอกอาจถูกใช้หรือเผยแพร่หรือโอนให้แก่บุคคลภายนอก (รวมไปถึงบริษัทที่เกี่ยวข้อง ผู้ให้บริการที่เป็นบุคคลภายนอกและผู้ให้บริการของหน่วยงานดังกล่าวและบริษัทที่เกี่ยวข้อง บริษัทที่ตั้งอยู่ทั้งในและนอกประเทศของท่าน ผู้ขายและผู้ใช้อื่น) สำหรับวัตถุประสงค์เฉพาะเรื่องซึ่งรวมถึงแต่ไม่จำกัดเพียงวัตถุประสงค์ดังต่อไปนี้&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ก) เพื่ออำนวยความสะดวกในการใข้บริการหรือการเข้าถึงเว็บไซต์ของท่าน</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ข) เพื่อตอบซึ่งข้อซักถาม ผลตอบรับ ข้อเรียกร้องหรือข้อโต้แย้งของท่าน ไม่ว่าโดยตรงหรือผ่านตัวแทนฝ่ายบริการลูกค้า</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ค) เพื่อประมวลผลคำสั่งที่ท่านส่งเข้ามาผ่านเว็บไซต์ (การชำระเงินที่ท่านได้ทำผ่านเว็บไซต์สำหรับสินค้าทั้งที่ขายโดยเราและผู้ขายบุคคลภายนอกจะถูกประมวลผลโดยตัวแทนของเรา)&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ง) เพื่อส่งสินค้าที่ท่านได้ซื้อผ่านเว็บไซต์ทั้งที่ขายโดยเราหรือผู้ขายบุคคลภายนอก เราอาจส่งข้อมูลส่วนบุคคลต่อไปยังบุคคลภายนอกเพื่อที่จะดำเนินการส่งสินค้าให้แก่ท่าน (เช่น ส่งให้ผู้ขนส่งสินค้าของเราหรือผู้ขาย) ไม่ว่าสินค้าดังกล่าวจะขายผ่านเว็บไซต์โดยเราหรือผู้ขายบุคคลภายนอก</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(จ) เพื่อแจ้งให้ท่านทราบสถานะการขนส่งสินค้าทั้งที่ขายผ่านเว็บไซต์โดยเราและผู้ขายบุคคลภายนอก และเพื่อวัตถุประสงค์ในการช่วยเหลือลูกค้า</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ฉ) เพื่อเปรียบเทียบข้อมูลและตรวจสอบกับบุคคลภายนอกเพื่อที่จะยืนยันว่าข้อมูลดังกล่าวนั้นถูกต้อง</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ช) เพื่อยืนยันตัวตนของท่านสำหรับวัตถุประสงค์ในการตรวจจับการทุจริต</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ซ) เพื่ออำนวยความสะดวกในการนำสินค้าต้องห้ามและสินค้าควบคุมออกจากเว็บไซต์&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ฌ) เพื่อจัดการบัญชีของท่านที่มีกับเรา (หากมี)&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ญ) เพื่อตรวจสอบและดำเนินธุรกรรมทางการเงินที่เกี่ยวข้องกับการชำระเงินออนไลน์ของท่าน&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ฎ) เพื่อตรวจสอบการดาวน์โหลดข้อมูลจากเว็บไซต์&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ฏ) เพื่อปรับปรุงรูปแบบหรือเนื้อหาของเพจของเว็บไซต์และปรับแต่งเพจนั้นให้เหมาะกับผู้ใช้&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ฐ) เพื่อระบุตัวตนผู้เยี่ยมชมบนเว็บไซต์&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ฑ) เพื่อดำเนินการค้นคว้าวิจัยเกี่ยวกับสถิติและพฤติกรรมของผู้ใช้&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ฒ) เพื่อให้ข้อมูลที่เราเห็นว่าอาจมีประโยชน์สำหรับท่านหรือที่ท่านร้องขอจากเรา รวมถึงข้อมูลที่เกี่ยวข้องกับสินค้าและบริการ ของเราหรือผู้ขายบุคคลภายนอก ซึ่งท่านได้แจ้งแล้วว่าท่านไม่คัดค้านที่จะได้รับการติดต่อเพื่อวัตถุประสงค์ดังกล่าว</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ณ) เพื่อแสดงชื่อ ชื่อผู้ใช้ หรือโปรไฟล์ของท่านบนเว็บไซต์&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ด) เพื่อส่งเสริมการบริการและการใช้ข้อมูลที่ท่านให้กับเรา เช่น เนื้อหาที่สร้างขึ้นโดยผู้ใช้ (รวมไปถึงเนื้อหาในรูปแบบวีดีโอ) ที่ท่านสามารถเลือกอัพโหลดหรือเผยแพร่บนเว็บไซต์ของเราซึ่งสามารถเข้าถึงได้ทางอินเตอร์เน็ตและอาจถูกเผยแพร่โดยสาธารณะชน (ซึ่งกรณีดังกล่าวจะไม่อยู่ภายใต้ความควบคุมของเรา) โดยถือเป็นส่วนหนึ่งของการโฆษณาและการตลาดเพื่อส่งเสริมเว็บไซต์</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ต) เพื่อดำเนินการเกี่ยวกับข้อร้องเรียน ผลตอบรับ การบังคับใช้และคำร้องขอให้ลบ อันเกี่ยวกับเนื้อหาใด ๆ ที่ท่านได้อัพโหลดขึ้นเว็บไซต์</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ถ) เพื่อได้รับข้อมูลเพิ่มเติมที่เกี่ยวข้องกับท่าน โดยมีพื้นฐานมาจากข้อมูลส่วนบุคคลที่ท่านได้ให้มา (ไม่ว่าให้กับเราหรือบุคคลภายนอก) เพื่อที่จะให้ข้อมูลกับท่านได้ตรงเป้าและ/หรือเกี่ยวข้องมากขึ้น&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ท) เพื่อส่งสื่อทางการตลาดหรือสื่อส่งเสริมการขายที่เกี่ยวข้องกับการขายสินค้าและบริการของเราหรือผู้ขายบุคคลภายนอกให้กับท่านเป็นครั้งคราวไป (เว้นแต่ในกรณีที่ท่านเลือกที่จะไม่รับสิ่งดังกล่าว) และ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ธ) เราอาจปฎิบัติการประมวลผลการตัดสินใจแบบอัตโนมัติตามวัตถุประสงค์ที่ได้กล่าวไว้ข้างต้น</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">3.2 เพื่อความชัดเจน ท่านรับทราบและยินยอมให้เนกซ์เทียร์แบ่งปันข้อมูลนิรนาม รวมถึงแต่ไม่จำกัดเพียงในสถานการณ์ดังต่อไปนี้&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ก) ข้อมูลผลรวม โดยเราอาจทำโครงการวิเคราะห์ข้อมูลรวมกับผู้ให้บริการบุคคลภายนอกที่ได้คัดเลือกมาโดยใช้ข้อมูลที่ไม่สามารถระบุตัวบุคคลที่เป็นเจ้าของข้อมูลได้ เพื่อประเมินความสนใจของผู้ใช้และเพื่อให้ผู้ใช้ได้รับซึ่งข้อมูลที่ตรงเป้าและ/หรือเกี่ยวข้อง โดยข้อมูลดังกล่าวจะมาจากข้อมูลผลรวมเกี่ยวกับกิจกรรมของผู้ใช้นอกเว็บไซต์&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ข) การโฆษณาโดยอิงตามพฤติกรรม เราอาจร่วมมือกับบุคคลภายนอกที่ได้คัดเลือกมาโดยใช้ข้อมูลที่ไม่สามารถระบุตัวบุคคลที่เป็นเจ้าของข้อมูลได้เพื่อพัฒนารูปแบบที่จะแสดงโฆษณาที่แม่นยำมากขึ้นแก่ผู้ใช้แต่ละราย</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">3.3 ท่านสามารถยกเลิกการรับข่าวสารข้อมูลการตลาดไม่ว่าเวลาใดโดยการใช้วิธีการยกเลิกการรับข่าวสารภายในโฆษณาทางอิเล็กทรอนิกส์ เราอาจใช้ข้อมูลการติดต่อของท่านเพื่อส่งจดหมายข่าวสารจากเราและจากบริษัทที่เกี่ยวข้อง</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">3.4 ในพฤติการณ์พิเศษเราอาจต้องเปิดเผยข้อมูลส่วนบุคคล เช่น ในสถานการณ์ที่มีมูลเหตุให้เชื่อว่าการเปิดเผยข้อมูลนั้นมีความจำเป็นเพื่อป้องกันอันตรายต่อชีวิตหรือสุขภาพ หรือเพื่อวัตถุประสงค์การบังคับใช้กฎหมาย หรือเพื่อให้สามารถดำเนินการตามข้อบังคับหรือคำขอตามกฎหมายและกฎเกณฑ์</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">3.5 เราอาจเปิดเผยและอนุญาตให้มีการเปิดเผยข้อมูลส่วนบุคคลของท่านกับบุคคลภายนอกและบริษัทในเครือของเราสำหรับวัตถุประสงค์ใด ๆ ที่กล่าวมาข้างต้น รวมไปถึงแต่ไม่จำกัดเพียงเพื่อให้เราให้ความสะดวกสบายกับท่านในการใช้บริการ เพื่อดำเนินการธุรกรรมของท่านให้สำเร็จ จัดการบัญชีผู้ใช้ของท่านและความสัมพันธ์ของเรากับท่าน ทำการตลาดและดำเนินการตามข้อบังคับหรือคำขอตามกฎหมายและกฎเกณฑ์ตามที่เห็นว่าจำเป็น โดยการเปิดเผยซึ่งข้อมูลส่วนบุคคลของท่านกับบุคคลดังกล่าว เราจะทำให้แน่ใจว่าบุคคลภายนอกและบริษัทในเครือของเราจะเก็บรักษาข้อมูลส่วนบุคคลของท่านให้ปลอดภัยจากการเข้าถึง การรวบรวม ใช้ เปิดเผย ประมวลผลหรือความเสี่ยงอื่นใดที่ใกล้เคียงกันโดยไม่ได้รับอนุญาตและจะเก็บรักษาข้อมูลส่วนบุคคลของท่านไว้เพียงเท่าที่จำเป็นจะต้องใช้เพื่อให้บรรลุซึ่งวัตถุประสงค์ที่กล่าวมาข้างต้น&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">3.6 หากท่านอยู่ในประเทศไทย เราอาจโอนหรืออนุญาตให้มีการโอนข้อมูลส่วนบุคคลของท่านไปนอกประเทศไทยเพื่อวัตถุประสงค์ใดตามที่ได้ระบุไว้ในนโยบายความเป็นส่วนตัวฉบับนี้&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><strong><span style="line-height: 107%; font-family: Prompt;">4. การถอนความยินยอมที่จะให้ใช้ เปิดเผย เก็บรักษา และ/หรือประมวลผลข้อมูลส่วนบุคคล</span></strong></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">4.1 ท่านอาจถอนความยินยอมของท่านที่จะให้ใช้ เปิดเผย เก็บรักษา และ/หรือประมวลผลซึ่งข้อมูลส่วนบุคคลของท่านต่อไปเพื่อวัตถุประสงค์ใด ๆ และในรูปแบบที่ระบุไว้ข้างต้น ไม่ว่าในเวลาใดก็ตาม</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">4.2 โปรดทราบว่าหากท่านถอนความยินยอมของท่านที่จะให้เราใช้ เปิดเผย เก็บรักษา หรือประมวลผลซึ่งข้อมูลของท่านเพื่อวัตถุประสงค์และในรูปแบบที่ระบุไว้ข้างต้น เราอาจจะไม่อยู่ในฐานะที่จะดำเนินการให้บริการแก่ท่านต่อไปหรือไม่สามารถปฏิบัติตามสัญญาใดที่เรามีกับท่านได้ และเราจะไม่ต้องรับผิดในกรณีที่เราไม่ได้ดำเนินการให้บริการท่านหรือไม่ปฏิบัติตามสัญญากับท่าน โดยเราขอสงวนสิทธิ์ทางกฎหมายและการเยียวยาของเราโดยชัดแจ้งสำหรับกรณีดังกล่าว&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><strong><span style="line-height: 107%; font-family: Prompt;">5. การปรับข้อมูลส่วนบุคคลของท่านให้เป็นปัจจุบัน</span></strong></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">5.1 ความถูกต้องและสมบูรณ์ของข้อมูลส่วนบุคคลที่ท่านให้แก่เรานั้นเป็นสิ่งสำคัญเพื่อให้ท่านสามารถใช้เว็บไซต์ต่อไปได้และให้เราสามารถให้บริการได้ ท่านเป็นผู้รับผิดชอบในการแจ้งเราหากมีการเปลี่ยนแปลงข้อมูลส่วนบุคคลของท่าน หรือในกรณีที่ท่านเชื่อว่าข้อมูลส่วนบุคคลเกี่ยวกับท่านที่อยู่กับเราไม่แม่นยำ ไม่สมบูรณ์ ไม่ถูกต้องหรือล้าสมัย&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">5.2 ท่านสามารถแก้ไขข้อมูลส่วนบุคคลของท่านให้เป็นปัจจุบันได้ไม่ว่าในเวลาใดโดยการเข้าถึงซึ่งบัญชีผู้ใช้ของท่านบนเว็บไซต์</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">5.3 เรามีการเปิดเผยข้อมูลส่วนบุคคลที่ได้แก้ไขเป็นปัจจุบันของท่านแก่บุคคลภายนอกและบริษัทในเครือของเราผู้ซึ่งเราได้เคยเปิดเผยข้อมูลส่วนบุคคลของท่านแล้ว หากข้อมูลส่วนบุคคลของท่านนั้นยังมีความจำเป็นในการดำเนินการตามวัตถุประสงค์ที่ระบุไว้ข้างต้น</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><strong><span style="line-height: 107%; font-family: Prompt;">6. การเข้าถึงข้อมูลส่วนบุคคลของท่าน</span></strong></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">6.1 ท่านสามารถขอข้อมูลเกี่ยวกับข้อมูลส่วนบุคคลของท่านที่เราเก็บรวบรวม หรือสอบถามเกี่ยวกับวิธีที่ข้อมูลส่วนบุคคลของท่านถูกใช้ เปิดเผย เก็บรักษาหรือประมวลผลโดยเราภายในปีที่ผ่านมา และเพื่อที่จะอำนวยความสะดวกตามคำขอของท่าน เราอาจจำเป็นต้องร้องขอข้อมูลเพิ่มเติมเกี่ยวกับคำขอของท่าน</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">6.2 เราขอสงวนสิทธิ์ในการเรียกเก็บค่าธรรมเนียมการบริหารจัดการตามสมควร สำหรับการกู้บันทึกข้อมูลส่วนบุคคลของท่าน กรณีดังกล่าวเราจะแจ้งให้ท่านทราบถึงค่าธรรมเนียมก่อนจะดำเนินการตามคำขอของท่าน</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><strong><span style="line-height: 107%; font-family: Prompt;">7. ความปลอดภัยของข้อมูลส่วนบุคคลของท่าน</span></strong></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">7.1 เพื่อรักษาความปลอดภัยซึ่งข้อมูลส่วนบุคคลของท่านจากการเข้าถึง การเก็บรวบรวม การใช้ การเปิดเผย การประมวลผล การทำสำเนา การเปลี่ยนแปลง การจำหน่าย การทำให้สูญหาย การใช้ในทางที่ผิด การดัดแปลงหรือความเสี่ยงอื่นที่คล้ายคลึงกัน โดยไม่ได้รับอนุญาต เราได้ใช้มาตรการในการจัดการที่เหมาะสม ทั้งที่เป็นรูปธรรมและในทางเทคนิค เช่น&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ก) จำกัดสิทธิการเข้าถึงข้อมูลส่วนบุคคลให้เข้าถึงได้เฉพาะของบุคคลที่จำเป็นต้องเข้าถึง</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ข) รักษาระดับของผลิตภัณฑ์ที่เกี่ยวข้องกับเทคโนโลยีเพื่อป้องกันการเข้าถึงทางคอมพิวเตอร์ที่ไม่ได้รับอนุญาต และ&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">7.2 หากท่านเชื่อว่าความเป็นส่วนตัวของท่านถูกละเมิด กรุณาติดต่อเราโดยทันที&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">7.3 ท่านควรทราบว่าไม่มีวิธีการถ่ายโอนใดบนอินเตอร์เน็ตหรือขั้นตอนการเก็บข้อมูลทางอิเล็กทรอนิกส์ใดที่ปลอดภัยโดยสมบูรณ์ แม้ไม่สามารถรับประกันความปลอดภัยนั้นได้ แต่เราได้พยายามอย่างมากที่จะปกป้องความปลอดภัยในข้อมูลของท่านและจะทบทวนและเพิ่มเติมมาตรการการรักษาข้อมูลอย่างสม่ำเสมอ&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">7.4 รหัสผ่านของท่านคือกุญแจในการเข้าถึงบัญชีของท่าน กรุณาใช้ตัวเลขเฉพาะ ตัวอักษรและอักษรพิเศษ และจะต้องไม่แบ่งปันรหัสผ่านเนกซ์เทียร์ของท่านกับบุคคลใด หากท่านแบ่งปันรหัสของท่านกับผู้อื่น ท่านจะต้องรับผิดชอบการกระทำทั้งหมดที่ได้ทำไปในนามของบัญชีของท่านและผลที่ตามมา หากท่านเสียการควบคุมรหัสผ่านของท่าน ท่านอาจเสียการควบคุมข้อมูลส่วนบุคคลของท่านและข้อมูลอื่นที่ท่านได้ส่งให้แก่เนกซ์เทียร์อย่างมีนัยสำคัญ รวมทั้งท่านอาจตกอยู่ภายใต้ความผูกพันตามกฎหมายในการกระทำใด ๆ ที่ได้กระทำไปในนามของท่าน ดังนั้นหากมีการเข้าถึงรหัสของท่านโดยไม่ได้รับอนุญาตไม่ว่าด้วยเหตุใดหรือมีเหตุอันควรเชื่อว่ารหัสของท่านถูกเข้าถึงโดยไม่ได้รับอนุญาต ท่านควรติดต่อเราและเปลี่ยนรหัสของท่านโดยทันที ท่านจะต้องไม่ลืมออกจากระบบบัญชีของท่านและปิดบราวเซอร์หากท่านใช้งานในคอมพิวเตอร์ซึ่งใช้ร่วมกับผู้อื่น&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><strong><span style="line-height: 107%; font-family: Prompt;">8. การเก็บรักษาข้อมูลส่วนบุคคล</span></strong></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">8.1 เราจะเก็บรักษาข้อมูลส่วนบุคคลของท่านไว้เพียงเท่าที่กฎหมายกำหนดไว้หรือเท่าที่จำเป็นสำหรับวัตถุประสงค์ของการเก็บข้อมูลดังกล่าว</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">8.2 เราจะยุติการเก็บรักษาข้อมูลส่วนบุคคลของท่านหรือนำวิธีการที่ข้อมูลที่อาจเชื่อมโยงกับท่านออก โดยทันทีที่เห็นสมควรแล้วว่าการเก็บรักษานั้น ไม่สามารถบรรลุวัตถุประสงค์ที่ข้อมูลส่วนบุคคลดังกล่าวได้ถูกเก็บรวบรวมมาแล้วอีกต่อไป และไม่มีความจำเป็นสำหรับวัตถุประสงค์ทางกฎหมายหรือทางธุรกิจใด</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><strong><span style="line-height: 107%; font-family: Prompt;">9. ผู้เยาว์</span></strong></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">9.1 เนกซ์เทียร์ไม่ขายสินค้าใดๆ เพื่อให้ผู้เยาว์ซื้อ (ตามกฎหมายที่เกี่ยวข้องกำหนด) และเนกซ์เทียร์ไม่มีเจตนาที่จะให้บริการใด ๆ แก่ผู้เยาว์ หรือให้ผู้เยาว์ใช้เว็บไซต์ และเราไม่มีเจตนาเก็บรวบรวมข้อมูลส่วนบุคคลซึ่งเกี่ยวข้องกับผู้เยาว์&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">9.2 ด้วยเอกสารฉบับนี้ ท่านยืนยันและรับประกันว่าท่านมีอายุเกินเกณฑ์ผู้เยาว์และท่านมีความสามารถที่จะเข้าใจและยอมรับข้อกำหนดนโยบายความปลอดภัยฉบับนี้ หากท่านเป็นผู้เยาว์ ท่านจะสามารถใช้เว็บไซต์ภายใต้ความยินยอมของบุพการีหรือผู้ปกครองเท่านั้น</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">9.3 ในฐานะบุพการีหรือผู้ปกครองตามกฎหมาย โปรดห้ามผู้เยาว์ภายใต้การดูแลของท่านส่งข้อมูลให้แก่เนกซ์เทียร์ ในกรณีที่ข้อมูลส่วนบุคคลของผู้เยาว์ดังกล่าวนั้นถูกเปิดเผยแก่เนกซ์เทียร์ ท่านยินยอมให้มีการประมวลผลข้อมูลส่วนบุคคลของผู้เยาว์และยอมรับและตกลงที่จะผูกพันตามนโยบายความเป็นส่วนตัวฉบับนี้และรับผิดชอบในการกระทำของผู้เยาว์ดังกล่าว&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">9.4 เราจะไม่รับผิดชอบต่อการใช้บริการบนเว็บไซต์โดยไม่ได้รับอนุญาตโดยตัวท่าน ผู้ใช้ผู้ดำเนินการในนามของท่านหรือผู้ใช้ที่ไม่ได้รับอนุญาต ท่านต้องรับผิดชอบในการตัดสินใจเกี่ยวกับการใช้บริการบนเว็บไซต์และดำเนินการใดๆเพื่อป้องกันการใช้บริการที่ไม่ถูกต้องบนเว็บไซต์&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><strong><span style="line-height: 107%; font-family: Prompt;">10. การเก็บรวบรวมข้อมูลคอมพิวเตอร์</span></strong></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">10.1 เราหรือผู้ให้บริการที่ได้รับอนุญาตจากเราอาจใช้ คุกกี้ เว็บ บีคอนและเทคโนโลยีอื่นใดที่คล้ายคลึงกันเพื่อเก็บรักษาข้อมูลที่ช่วยให้เราสร้างประสบการณ์เฉพาะตัวที่ดีขึ้น เร็วขึ้น ปลอดภัยขึ้นเมื่อท่านใช้บริการหรือเข้าถึงเว็บไซต์&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">10.2 เมื่อท่านเข้าชมเว็บไซต์ผ่านคอมพิวเตอร์ อุปกรณ์เคลื่อนที่ หรืออุปกรณ์อื่นใดที่มีการเชื่อมต่ออินเตอร์เน็ตของท่าน เซิร์ฟเวอร์ของบริษัทเราจะบันทึกข้อมูลที่บราวเซอร์ของท่านได้ส่งมาโดยอัตโนมัติเมื่อได้เข้าสู่เว็ปไซต์ โดยข้อมูลอาจรวมถึง&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ก) หมายเลขไอพีของคอมพิวเตอร์หรืออุปกรณ์ของท่าน&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ข) ชนิดของบราวเซอร์</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ค) เว็บเพจที่ท่านเข้าชมก่อนที่ท่านจะเข้าถึงเว็บไซต์&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(ง) หน้าในเว็บไซต์ที่ท่านเข้าชม และ&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">(จ) ระยะเวลาที่ท่านใช้บนหน้าดังกล่าว สิ่งของและข้อมูลที่ท่านค้นหาบนเว็บไซต์ ระยะเวลาเข้าถึงและวันที่และสถิติอื่น ๆ&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">10.3 ข้อมูลเหล่านี้จะถูกเก็บรวบรวมเพื่อวิเคราะห์และประเมินเพื่อที่จะช่วยเราปรับปรุงเว็ปไซต์และการบริการและสินค้าที่เรามี&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">10.4 คุกกี้คือไฟล์ตัวอักษรขนาดเล็ก (ปกติแล้วจะประกอบด้วยตัวอักษรและตัวเลข) ที่ตั้งอยู่ในความจำของบราวเซอร์หรืออุปกรณ์ของท่านเมื่อท่านเข้าชมเวปไซต์หรือดูข้อความ คุกกี้ทำให้เราสามารถจำอุปกรณ์หรือบราวเซอร์ดังกล่าวและช่วยให้เราสามารถทำเนื้อหาให้เป็นการเฉพาะโดยให้เหมาะสมกับสิ่งที่ท่านสนใจได้รวดเร็วขึ้นและจะทำให้การบริการและเว็บไซต์มีความสะดวกสบายและมีประโยชน์กับท่าน&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">10.5 ท่านสามารถจัดการและลบคุกกี้ผ่านการตั้งค่าในบราวเซอร์หรืออุปกรณ์ของท่าน สำหรับข้อมูลเพิ่มเติมเกี่ยวกับวิธีการนั้น กรุณาเข้าชมส่วนช่วยเหลือของบราวเซอร์หรืออุปกรณ์ของท่าน</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">10.6 เว็บ บีคอนคือรูปกราฟฟิคขนาดเล็กที่อาจถูกรวมอยู่บนการบริการและเว็บไซต์ของเรา เว็บ บีคอนนั้นช่วยให้เราสามารถนับจำนวนผู้ใช้ผู้เข้าชมเพจดังกล่าวเพื่อให้เราสามารถเข้าใจความต้องการและความสนใจของท่าน</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><strong><span style="line-height: 107%; font-family: Prompt;">11. สิทธิของเนกซ์เทียร์ในการเปิดเผยข้อมูลส่วนบุคคล</span></strong><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">11.1 ท่านรับทราบและตกลงว่าเนกซ์เทียร์มีสิทธิที่จะเปิดเผยข้อมูลส่วนบุคคลของท่านต่อหน่วยงานทางกฎหมาย หน่วยงานควบคุมกฎเกณฑ์ หน่วยงานรัฐบาล หน่วยงานภาษี หน่วยงานบังคับใช้กฎหมาย หรือหน่วยงานใดหรือผู้มีสิทธิที่เกี่ยวข้อง หากเนกซ์เทียร์มีเหตุอันควรเชื่อได้ว่าการเปิดเผยข้อมูลส่วนบุคคลของท่านนั้นจำเป็นสำหรับวัตถุประสงค์ในการปฏิบัติตามหน้าที่ สิ่งที่ต้องทำหรือการจัดการ ไม่ว่าสมัครใจหรือจำเป็น ซึ่งเป็นผลมาจากการร่วมมือกับคำสั่งหรือการสอบสวน และ/หรือการร้องขอโดยหน่วยงานดังกล่าว โดยสิทธิดังกล่าวนั้นจะต้องอยู่ในขอบเขตของกฎหมายที่เกี่ยวข้อง และท่านตกลงที่จะไม่ดำเนินการใดๆ และ/หรือสละสิทธิของท่านในการดำเนินการใดๆ ต่อเนกซ์เทียร์สำหรับการเปิดเผยข้อมูลส่วนบุคคลของท่านในสถานการณ์ดังกล่าว</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><strong><span style="line-height: 107%; font-family: Prompt;">12. เว็บไซต์ของบุคคลภายนอก</span></strong></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">12.1 เว็บไซต์อาจมีลิงก์ที่นำไปสู่เว็บไซต์อื่นที่ดำเนินการโดยฝ่ายอื่น เช่น ผู้เกี่ยวข้องกับเราทางธุรกิจ พ่อค้า หรือช่องทางการชำระเงิน เราจะไม่รับผิดชอบในวิธีปฎิบัติเกี่ยวกับความเป็นส่วนตัวของเว็บไซต์ที่ดำเนินการโดยฝ่ายอื่นดังกล่าว ขอแนะนำท่านให้ตรวจสอบนโยบายความเป็นส่วนตัวที่เกี่ยวข้องในเว็บไซต์ดังกล่าวเพื่อที่จะทราบว่าเว็บไซต์ดังกล่าวจะจัดการกับข้อมูลที่ได้เก็บรวบรวมไปจากท่านอย่างไร</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><strong><span style="line-height: 107%; font-family: Prompt;">13. คำถาม ผลตอบรับ ข้อกังวล คำแนะนำหรือข้อเรียกร้อง</span></strong></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">13.1 หากท่านมีคำถามใดเกี่ยวข้องกับการคุ้มครองข้อมูลส่วนบุคคลหรือความเป็นส่วนตัวของข้อมูล โปรดอ้างถึงรายการคำถามที่พบบ่อยในการคุ้มครองข้อมูล/ความเป็นส่วนตัวของข้อมูล</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">13.2 หากคำถามของท่านไม่อยู่ในรายการคำถามที่พบบ่อยของเรา หรือหากท่านมีผลตอบรับ ข้อกังวล คำแนะนำหรือข้อเรียกร้องเกี่ยวข้องกับข้อมูลส่วนบุคคล โปรดติดต่อเรามาทางระบบสนทนาออนไลน์ของเรา หรือช่องทางดังนี้<span style="color:red;"><br>&nbsp;</span>หมายเลขโทรศัพท์:&nbsp;02-121-4645</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">ที่อยู่:&nbsp;88/188 ถนนเทศบาลสงเคราะห์ แขวงลาดยาว เขตจตุจักร กรุงเทพมหานคร&nbsp;10900</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px; line-height: 107%; font-family: Prompt;">&nbsp;อีเมล: support@nextere.co.th</span></p>
                </div>
            </div>
            <div class="modal-footer " style="justify-content: center;">
                <button id="btn-cookie-modal" type="button" class="btn btn-secondary col-11" data-bs-dismiss="modal" style="width: 120px;height: 40px; border-radius: 25px;">ยอมรับ</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade " id="example_1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="color: black;">
                <h5 class="modal-title" id="exampleModalLabel" style="padding-left: 43%;">ข้อกำหนดและเงื่อนไข</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" style="color: black;">
                <div class="container">
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px; line-height: 107%; font-family: Prompt;">ข้อกำหนดและเงื่อนไข</span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">ขอต้อนรับสู่&nbsp;www.nextere.store&nbsp;ขอบคุณที่เข้ามาเยี่ยมชมเว็บไซต์ของเรา ในหน้านี้มีข้อกำหนดและเงื่อนไขเกี่ยวกับการจัดจำหน่ายและจัดส่งสินค้า (ซึ่งต่อไปนี้เรียกว่า &ldquo;สินค้า&rdquo;) ที่แสดงอยู่บนเว็บไซต์&nbsp;www.nextere.store (&ldquo;เว็บไซต์&rdquo;) ที่คุณควรทราบ (&ldquo;เงื่อนไข&rdquo;) โปรดอ่านข้อกำหนดและเงื่อนไขดังกล่าวก่อนสั่งซื้อสินค้าจากเว็บไซต์ของเรา เมื่อสั่งซื้อสินค้าของเราหมายความว่าคุณตกลงที่จะปฏิบัติตามข้อกำหนดและเงื่อนไขนั้น โปรดอ่านข้อกำหนดและเงื่อนไขอย่างสม่ำเสมอเพื่อทราบข้อกำหนดและเงื่อนไขดังกล่าวที่อาจมีการเปลี่ยนแปลง</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">1.เราเป็นใคร</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">1.1&nbsp;หน้านี้แสดงเงื่อนไขและข้อกำหนดที่นำมาใช้กับการซื้อขายสินค้าจากเว็บไซต์ของเรา การซื้อสินค้าจากเว็บไซต์ดำเนินการโดยบริษัท เนกซ์เทียร์ จำกัด</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">1.2บุคคลอื่นไม่ได้รับอนุญาตให้ใช้เนื้อหาในข้อกำหนดและเงื่อนไขเพื่อการพาณิชย์แม้ว่าจะเป็นเนื้อหาที่ถูกลบ ไปแล้วก็ตามเพื่อเสนอขายสินค้า และ/หรือบริการ ทั้งนี้หากมีการกระทำดังกล่าว อาจมีการฟ้องร้องคดีตามกฎหมาย</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">2.&nbsp;คำนิยาม</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">ในข้อกำหนดและเงื่อนไข</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&bull; &quot;วันทำการ&quot; หมายถึงวันทำการ (จันทร์-ศุกร์) ยกเว้นวันหยุดตามประเพณีที่บริษัทกำหนด</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&bull; &quot;การยืนยันคำสั่งซื้อ&quot;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">หมายถึงอีเมลของเราที่ส่งให้คุณเพื่อแสดงว่าเราได้รับคำสังซื้อของคุณตามข้อกำหนดและเงื่อนไขนี้แล้วในข้ อ&nbsp;8.2&nbsp;ข.</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&bull; &quot;ข้อตกลง&quot; หมายถึงคำสั่งซื้อของคุณที่เป็นการสั่งซื้อสินค้าตามข้อกำหนดและเงื่อนไข</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&bull; &quot;ลูกค้า&quot; หมายถึงบุคคลที่ส่งคำสั่งซื้อในเว็บไซต์นี้</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&bull; &quot;คำสั่งซื้อ&quot; หมายถึง คำสั่งซื้อที่ลูกค้าส่งมาที่เว็บไซต์ของเราเพื่อซื้อสินค้า</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&bull; &ldquo;เนกซ์เทียร์&rdquo; หมายถึงบริษัท เนกซ์เทียร์ จำกัด</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&bull; &ldquo;คุณ&rdquo; หมายถึงลูกค้าที่ส่งคำสั่งซื้อบนเว็บไซต์&nbsp;www.nextere.store</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&bull;&nbsp;การอ้างอิงเกี่ยวกับ &quot;ข้อ&quot; หมายถึงข้อกำหนดและเงื่อนไขตามเงื่อนไขในข้อตกลงและเงื่อนไขเหล่านี้</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&bull;&nbsp;หัวข้อหรือหัวเรื่องมีไว้เพื่ออำนวยความสะดวกในการอ้างอิงเท่านั้น และไม่มีผลต่อการตีความเนื้อหาในข้อกำหนดและเงื่อนไขนี้</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&bull;&nbsp;คำที่เป็นเอกพจน์จะหมายรวมถึงคำที่เป็นพหูพจน์ด้วย</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">และคำที่เป็นพหูจน์จะหมายรวมถึงคำที่เป็นเอกพจน์ด้วย คำที่แสดงเพศจะหมายรวมถึงเพศทั้งหมด และคำว่า &ldquo;บุคคล&rdquo; จะหมายรวมถึง บุคคล บริษัทหรือห้างหุ้นส่วน</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&bull;&nbsp;คำว่า &ldquo;รวมถึง&rdquo; &ldquo;เช่น&rdquo; หรือคำใด ๆ ที่มีลักษณะใกล้เคียงกันจะหมายถึง &ldquo;ไม่จำกัดกับ&rdquo;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">3.ข้อจำกัด</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">คุณไม่ได้รับอนุญาตให้ใช้เว็บไซต์นี้ในทางที่ผิดกฎหมาย และห้ามใช้เพื่อส่งเสริมการก่ออาชญากรรม การโอนหรือแพร่ไวรัส ซึ่งรวมถึงแต่ไม่จำกัดกับ โทรจัน (Trojan horse),&nbsp;เวิร์ม (worm),&nbsp;ลอกจิก (logic),&nbsp;บอมบ์ (bomb)&nbsp;หรือโพสต์เนื้อหาใด ๆ บนเว็บไซต์อันแสดงเจตนาที่ไม่ดีหรือที่อาจเป็นอันตรายทางเทคโนโลยี หรือโพสต์ข้อมูลไม่เหมาะสม เจาะเข้าระบบบริการนี้ ใช้ข้อมูลนี้ไปในทางมิชอบ สร้างความรำคาญกับบุคคลอื่น ละเมิดสิทธิ์ของบุคคลอื่น โฆษณาหรือส่งเสริมการขายที่ไม่จำเป็นหรือ พยายามสร้างผลกระทบต่อการดำเนินงาน หรือใช้งานสิ่งอำนวยความสะดวกของคอมพิวเตอร์บนว็บไซต์นี้ หรือที่เข้าถึงเว็บไซต์นี้ได้ คุณจะต้องไม่ใช้เว็บไซต์นี้ในทางที่ผิดกฎหมายโดยสร้างบัญชีผู้ใช้งานหลายบัญชี การละเมิดบทบัญญัติจะถือว่าเป็นความผิดทางอาญาภายใต้กฎหมายการก่ออาชญากรรมทางไซเบอร์ปี&nbsp;2007&nbsp;และพระราชบัญญัติอาชญากรรมคอมพิวเตอร์ ปี&nbsp;2007&nbsp;ในกรณีที่มีการละเมิด เราจะรายงานการละเมิดนั้นไปยังหน่วยงานบังคับกฎหมายที่เกี่ยวข้องเพื่อดำเนินคดีทางกฎหมายต่อไป</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">4.การมีผลบังคับใช้</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">4.1&nbsp;ข้อกำหนดและเงื่อนไขนี้บังคับใช้กับคำสั่งซื้อและข้อตกลงทั้งหมดที่จัดทำขึ้นเพื่อการซื้อและจัดส่งสินค้า ข้อกำหนดและเงื่อนไขเหล่านี้เป็นข้อตกลงโดยสมบูรณ์ระหว่างคุณกับบริษัท และจะนำมาบังคับใช้แทนข้อตกลงที่ทำไว้ก่อนหน้า และที่จะทำในเวลาเดียวกันระหว่างคุณกับบริษัทที่เกี่ยวข้องกับการซื้อขายสินค้า การเปลี่ยนแปลงบทบัญญัติในข้อกำหนดและเงื่อนไขจะมีผลโดยสมบูรณ์เมื่อจัดทำเป็นลายลักษณ์อักษรและ ลงนามโดยผู้มีอำนาจของบริษัท</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">4.2&nbsp;ข้อกำหนดใด ๆที่คุณร้องขอและเราไม่ยอมรับเป็นอย่างลายลักษณ์อักษร ซึ่งรวมถึงเงื่อนไขอื่น ๆ ที่คุณร่างและนำเสนอไว้ ไม่ว่าในรูปแบบใด หรือเวลาไหนก็ตาม และไม่ว่าจะเป็นลายลักษณ์อักษร อีเมล หรือทางวาจาจะถูกปฏิเสธและไม่ยอมรับนอกจากว่าจะมีการระบุไว้อย่างเป็นลายลักษณ์อักษร</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">5.&nbsp;สินค้าของเรา</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">5.1&nbsp;ภาพสินค้าบนเว็บไซต์ของเราใช้เป็นภาพประกอบเท่านั้น แม้ว่าเราจะพยายามแสดงสีทั้งหมดอย่างถูกต้อง เราไม่อาจรับประกันว่าสีที่แสดงบนคอมพิวเตอร์ของคุณจะตรงกับสีจริงของสินค้าหรือไม่ สินค้าของคุณอาจดูต่างออกไปเล็กน้อยจากภาพที่แสดงไว้บนเว็บไซต์</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">5.2&nbsp;แม้ว่าเราจะพยายามแสดงข้อมูลอย่างถูกต้องเกี่ยวกับ ขนาด น้ำหนัก ความจุ มิติ และตัวเลขการวัดของสินค้าที่แสดงไว้บนเว็บไซต์ของเรา แต่ก็อาจเกิดข้อผิดพลาดได้ไม่เกิน&nbsp;2%</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">5.3&nbsp;บรรจุภัณฑ์ของสินค้าอาจแตกต่างจากภาพที่แสดงบนว็บไซต์ของเรา</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">5.4&nbsp;สินค้าทั้งหมดที่แสดงบนเว็บไซต์ของเรามีพร้อมจัดส่ง คุณจะได้รับแจ้งทางอีเมลโดยเร็วที่สุด หากสินค้าที่คุณสั่งไม่มีสินค้าคงเหลือ ทางบริษัทจะไม่ดำเนินการกับคำสั่งซื้อของคุณ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">5.5&nbsp;เราปฏิบัติตามข้อกำหนดและข้อบังคับที่สำนักงานคณะกรรมการคุ้มครองผู้บริโภคได้กำหนดไว้</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">6.&nbsp;การใช้งานเว็บไซต์ของเรา</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">6.1&nbsp;การใช้งานบนเว็บไซต์ของเราเป็นไปตามเงื่อนไขการใช้งานเว็บไซต์และนโยบายการใช้งานเว็บไซต์ที่ยอมรับได้ โปรดอ่านข้อมูลในส่วนนี้ เพราะว่ามีข้อมูลสำคัญที่คุณควรทราบ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">7.&nbsp;รูปแบบการใช้ข้อมูลส่วนตัวของคุณ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">7.1&nbsp;เราใช้ข้อมูลส่วนตัวของคุณตามนโยบายความเป็นส่วนตัวของเรา โปรดอ่านข้อมูลในส่วนนี้เพราะว่ามีข้อมูลสำคัญที่คุณควรทราบ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">8.&nbsp;เงื่อนไขการขาย</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">8.1&nbsp;อายุ หากคุณเป็นลูกค้า คุณจะซื้อสินค้าผ่านเว็บไซต์ได้เมื่อคุณมีอายุเกินเกณฑ์ผู้เยาว์ขึ้นไป</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">8.2&nbsp;การลงทะเบียนและการจัดทำข้อตกลง&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">ก) เงื่อนไขและข้อกำหนดบนเว็บไซต์นี้ไม่ถือว่าเป็นข้อเสนอขาย และไม่มีข้อตกลงเกี่ยวกับการซื้อขายสินค้าระหว่างคุณกับบริษัทจนกว่าเราจะส่งสินค้าไปยังที่อยู่ของคุณ&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">ข) สำหรับการส่งคำสั่งซื้อ คุณจำเป็นต้องปฏิบัติตามกระบวนการซื้อขายออนไลน์บนเว็บไซต์นี้หลังจากที่คุณได้รับอีเมลยืนยันคำสั่งซื้อ แล้วว่าเราได้รับคำสั่งซื้อของคุณเป็นที่เรียบร้อย&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">ค) คำสั่งซื้อถือว่าเป็นที่ยอมรับแล้ว เมื่อเราจัดส่งสินค้าที่คุณสั่งซื้อไปตามที่อยู่ที่คุณแจ้งไว้ หากคำสั่งซื้อของคุณประกอบด้วยสินค้าหลายชิ้น อาจมีการจัดส่งหลายครั้ง ซึ่งการจัดส่งที่สมบูรณ์แต่ละครั้งจะหมายถึงว่า คำสั่งซื้อนั้นได้รับการยอมรับแล้ว</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">8.3&nbsp;ราคาและการชำระเงิน แม้ว่าเราจะพยายามจัดทำรายละเอียด คำอธิบาย และราคาที่ถูกต้องบนเว็บไซต์แล้ว แต่ก็อาจเกิดความผิดพลาดได้ หากเราพบข้อผิดพลาดในส่วนของราคาสินค้าที่คุณสั่งซื้อ เราจะแจ้งให้คุณทราบโดยเร็วที่สุด และคุณอาจตัดสินใจว่าจะยืนยันคำสั่งซื้อของคุณด้วยราคาที่ถูกต้อง หรือยกเลิกคำสั่งซื้อดังกล่าว หากเราไม่สามารถติดต่อคุณได้ เราจะถือว่าคำสั่งซื้อดังกล่าวถูกยกเลิก หากคุณยกเลิกคำสั่งซื้อก่อนจัดส่งสินค้า และคุณชำระเงินสำหรับคำสั่งซื้อนั้นเป็นที่เรียบร้อยแล้ว คุณจะได้รับเงินคืนทั้งหมด&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">ก) ราคาดังกล่าวรวมภาษีมูลค่าเพิ่ม (VAT)&nbsp;แล้วสำหรับสกุลเงินไทยบาท สำหรับค่าจัดส่ง (ถ้ามี) จะคำนวณเพิ่มจากราคาสินค้า ซึ่งค่าจัดส่งดังกล่าวจะแสดงและรวมอยู่ใน &ldquo;ราคาสุดท้าย&rdquo;&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">ข) ไม่มีข้อภาระผูกพันในการปฏิบัติตามคำสั่งซื้อของคุณหากราคาที่แสดงไว้บนเว็บไซต์ไม่ถูกต้อง (แม้ว่าเราจะได้รับคำสั่งซื้อของคุณแล้วก็ตาม)&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">ค) คุณสามารถชำระเงินผ่านช่องทางการชำระเงินของเราที่แสดงไว้บนหน้าเว็บไซต์&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">ง) เพื่อป้องกันความเสี่ยงจากการเข้าใช้งานโดยไม่ได้รับอนุญาต ข้อมูลบนบัตรของคุณจะใส่รหัสไว้เมื่อได้รับคำสั่งซื้อของคุณจะหักเงินจากบัตรของคุณ &nbsp;หากสำเร็จจะจัดส่งอีเมลยืนยันคำสั่งซื้อกับคุณ คุณสามารถให้ผู้ออกบัตรตรวจสอบบัตรก่อนได้ หากไม่ได้รับการยืนยันจากผู้ออกบัตร บริษัทจะไม่รับผิดชอบต่อการจัดส่งสินค้าล่าช้าหรือการไม่จัดส่ง&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">จ) เมื่อคุณดำเนินการไปจนถึงหน้าถัดไปหลังจากคลิก &ldquo;ส่งคำสั่งซื้อ&rdquo; แล้ว หมายความว่าเราได้รับการยืนยันจากผู้ออกบัตรเรียบร้อยแล้ว</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">8.4&nbsp;การสั่งซื้อล่วงหน้า หากคุณสั่งซื้อสินค้าล่วงหน้า สั่งซื้อสินค้าที่ยังไม่มีของจัดส่ง หรือจองรอบการจัดส่งล่วงหน้า บริษัทและหุ้นส่วนการชำระเงินจะอนุญาตหรือสงวนสิทธิ์ในการเรียกเก็บเงินจากบัตรของคุณเต็มจำนวนได้ ทุกเมื่อในช่วงที่กำลังส่งคำสั่งซื้อ และจัดส่งสินค้า หากการอนุญาตครบกำหนดหรือไม่ว่าด้วยเหตุผลใดที่ทำให้ดำเนินการต่อไปได้ บริษัทขอสงวนสิทธิ์ในการเรียกเก็บเงินเต็มจำนวนอีกครั้ง ก่อนหรือหลังจัดส่งสินค้า ในกรณีดังกล่าว บริษัทจะแจ้งให้คุณทราบทางอีเมลที่คุณใช้ในระหว่างชำระเงิน หากบริษัทไม่ได้รับอนุญาตจากผู้ออกบัตรเครดิตของคุณ พวกเขาจะติดต่อคุณเพื่อให้คุณชำระเงินกับทางบริษัทในอีกช่องทางหนึ่ง ทั้งนี้ หากราคาเพิ่มขึ้นจากตอนที่สั่งซื้อในตอนแรก บริษัทจะติดต่อผู้ถือบัตรภายใน&nbsp;7&nbsp;วันก่อนอนุญาตให้หักเงินจากบัตร</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">8.5&nbsp;คำสั่งซื้อไม่ถูกต้อง หากคุณพบข้อผิดพลาดในคำสั่งซื้อหลังจากที่คุณส่งคำสั่งซื้อแล้ว กรุณาติดต่อที่&nbsp;info@nextere.co.th&nbsp;และเราจะพยายามดำเนินการตามคำขอของคุณอย่างเต็มที่</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">8.6&nbsp;การปฏิเสธคำสั่งซื้อ เราอาจนำสินค้าออกจากเว็บไซต์ได้ทุกเมื่อ และ/หรือลบ หรือแก้ไขข้อมูลหรือเนื้อหาบนเว็บไซต์นี้ บริษัทจะพยายามอย่างเต็มที่เพื่อประมวลผลคำสั่งซื้อทั้งหมด แต่อาจมีบางสถานการณ์ที่จำเป็นต้องหยุดดำเนินการต่อซึ่งถือว่าเกิดขึ้นได้น้อยมาก หลังจากที่ส่งอีเมลยืนยันคำสั่งซื้อนั้นให้กับคุณ หากคำสั่งซื้อของคุณถูกยกเลิกและคุณได้ชำระเงินสำหรับคำสั่งซื้อนั้นเรียบร้อยแล้ว บริษัทจะคืนเงินให้กับคุณเต็มจำนวน เราและบริษัทไม่มีส่วนรับผิดชอบกับคุณหรือบุคคลอื่นสำหรับการถอนสินค้าออกจากเว็บไซต์ ไม่ว่าจะเป็นการขาย เพิกถอน แก้ไขเนื้อหาหรือข้อความบนเว็บไซต์นี้ หรือการปฏิเสธดำเนินการหรือยอมรับคำสั่งซื้อนั้น</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">9.&nbsp;การจัดส่ง</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">9.1&nbsp;คำสั่งซื้อของคุณจะสมบูรณ์ ณ วันที่มีการจัดส่งตามที่คาดการณ์ไว้ (และช่วงเวลา (ถ้ามี) ตามที่กำหนดไว้ในเอกสารยืนยัน ณ ที่อยู่จัดส่งที่คุณแจ้งในคำสั่งซื้อ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">9.2&nbsp;คุณจะได้รับแจ้งหากบริษัทไม่สามารถจัดส่งสินค้าภายในระยะเวลาที่กำหนดได้ ทั้งนี้ ตามขอบเขตของกฎหมายแล้ว เราไม่ต้องรับผิดชอบต่อความเสียหายหรือการสูญเสียอันเนื่องจากการส่งล่าช้า</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">9.3&nbsp;เมื่อมีการจัดส่งแล้ว คุณจะต้องลงชื่ออนุมัติการจัดส่งและคุณอาจติดต่อฝ่ายบริการลูกค้าที่อีเมล&nbsp;info@nextere.co.th&nbsp;หากมีข้อผิดพลาด ความบกพร่อง หรือความเสียหายที่เกิดกับสินค้า และคุณจะต้องจัดทำเอกสารส่งคืนมาพร้อมกับสินค้าให้กับเราตามที่ต้องการ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">9.4&nbsp;บริษัทอาจไม่สามารถจัดส่งสินค้าในบางพื้นที่ได้ อย่างเช่นในพื้นที่ภัยพิบัติทางธรรมชาติ เป็นต้น ในกรณีดังกล่าว บริษัทจะแจ้งให้คุณทราบเมื่อคุณทำการสั่งซื้อ บริษัทจะยกเลิกคำสั่งซื้อของคุณหรือจัดส่งสินค้าไปยังที่อยู่สำรองที่คุณแจ้งไว้ตอนที่สั่งซื้อสินค้า</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">9.5&nbsp;การจัดส่งจะอยู่ในบรรจุภัณฑ์ที่ได้มาตรฐาน หากคุณต้องการบรรจุภัณฑ์เฉพาะ คุณตกลงที่จะชำระค่าใช้จ่ายเพิ่มเติมที่เกิดขึ้นในภายหลัง</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">9.6&nbsp;ความเสี่ยงในสินค้าจะส่งผ่านมาให้คุณทันทีเมื่อยอมรับการจัดส่งแล้ว นอกจากว่าเกิดความล่าช้าในการจัดส่งเนื่องจากคุณละเมิดข้อภาระผูกพันตามข้อตกลง (อย่างเช่นข้อ&nbsp;9.8 )&nbsp;ในกรณีดังกล่าว ความเสี่ยงนั้นจะส่งต่อให้กับคุณ ณ วันจัดส่งสินค้าเดิมที่ควรมีการจัดส่งสินค้าไปแล้ว เมื่อส่งต่อความเสี่ยงมาที่คุณแล้ว เราจะไม่รับผิดชอบต่อการสูญเสีย หรือความเสียหายของสินค้าดังกล่าว การละเมิดข้อภาระผูกพันจะส่งผลต่อการสั่งซื้อสินค้าของคุณผ่านทางเว็บไซต์ในครั้งต่อไป</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">9.7&nbsp;หากคุณไม่พร้อมรับมอบสินค้า บริษัทอาจออกหนังสือแจ้งให้คุณทราบเกี่ยวกับการจัดส่งครั้งต่อไปหรือคุณอาจจะไปรับสินค้าด้วยตนเองกับผู้ให้บริการจัดส่งสินค้า</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">9.8&nbsp;หากการจัดส่งหรือการรับมอบสินค้าล่าช้าเนื่องจากคุณปฏิเสธ โดยไม่รับมอบสินค้าที่จัดส่งมา หรือคุณยังไม่รับมอบสินค้าหรือการจัดส่ง (ภายในสองสัปดาห์ตั้งแต่การจัดส่งครั้งแรก) จากผู้ให้บริการจัดส่งสินค้า บริษัทอาจดำเนินการในข้อใดข้อหนึ่งหรือทั้งสองข้อต่อไปนี้ (โดยไม่ส่งผลต่อสิทธิ์หรือการแก้ไขใด ๆ) ก) คิดค่าธรรมเนียมหรือค่าใช้จ่ายตามที่เห็นสมควร หรือ ข) ไม่จัดส่งหรือยกเลิกการจัดส่ง และบริษัทจะแจ้งให้คุณทราบทันทีเกี่ยวกับการยกเลิกข้อตกลงที่เกี่ยวข้อง ซึ่งบริษัทจะคืนเงินให้คุณหรือคืนให้บริษัทบัตรเครดิต หรือเข้าบัตรเดบิตของคุณตามจำนวนเงินที่คุณชำระภายใต้ข้อตกลงนี้ โดยมีการหักค่าดำเนินการ (รวมถึงค่าใช้จ่ายสำหรับการจัดส่ง และคืนสินค้า และเงินวางประกันตามที่กำหนดในข้อ&nbsp;9.8 (ก) ข้างต้น) คุณยอมรับว่าสินค้าดังกล่าวเป็นไปตามมาตรฐานทั่วไปและไม่ได้จัดทำขึ้นเพื่อตอบสนองข้อกำหนดเฉพาะ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">10.&nbsp;การยกเลิกโดยลูกค้า (ในกรณีที่คุณเปลี่ยนใจก่อนจัดส่งสินค้า) หากคุณต้องการยกเลิกคำสั่งซื้อ กรุณาติดต่อที่&nbsp;info@nextere.co.th&nbsp;โดยไม่มีค่าธรรมเนียมสำหรับการยกเลิก หากสินค้าภายใต้คำสั่งซื้อนั้นถูกจัดส่งไปแล้ว คุณอาจไม่สามารถยกเลิกได้และสินค้าดังกล่าวจะถูกส่งคืนให้กับบริษัทตามนโยบายการส่งคืนสินค้าและการคืนเงิน</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">11.&nbsp;นโยบายการส่งคืนสินค้าและการคืนเงิน</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">11.1&nbsp;หากคุณไม่พึงพอใจกับสินค้าที่คุณซื้อ คุณอาจส่งคืนสินค้าดังกล่าวให้กับบริษัทภายในสิบสี่ (14)&nbsp;วัน นับตั้งแต่วันที่ได้รับสินค้า สินค้าดังกล่าวต้องไม่ผ่านการใช้งานและต้องอยู่ในสภาพที่ขายต่อได้ ฉลาก และบรรจุภัณฑ์ต้องไม่ฉีกขาดหรือเสียหาย และสินค้าจะต้องอยู่ในบรรจุภัณฑ์เดิม</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">11.2&nbsp;คุณจะต้องส่งคืนสินค้าให้กับบริษัทตามเงื่อนไขที่มีลักษณะใกล้เคียงกับตอนที่คุณได้รับสินค้า หากสินค้าที่ส่งคืนให้กับบริษัทไม่อยู่ในสภาพที่ดี หรือขายต่อได้ บริษัทขอสงวนสิทธิ์ในการปฏิเสธสินค้าที่ส่งคืนนั้น และจะส่งสินค้ากลับให้คุณ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">11.3&nbsp;คุณอาจส่งคืนสินค้าผ่านไปรษณีย์ไทยทั่วประเทศหรือผู้ให้บริการจัดส่งรายอื่น โดยจะต้องส่งคืนสินค้ามาพร้อมกับแบบฟอร์มส่งคืนสินค้า บริษัทจะดำเนินการคืนเงินให้ทันทีหลังจากได้รับสินค้าตามรูปแบบการคืนเงินที่ระบุไว้ในแบบฟอร์มส่งคืน สินค้า</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">11.4&nbsp;ในบางกรณี นโยบายชำระเงิน/ การยกเลิก และการคืนเงินสำหรับสินค้าบางตัวอาจแตกต่างจากเงื่อนไขมาตรฐานทั่วไปที่ระบุไว้ในเอกสารนี้ ในกรณีดังกล่าว จะมีการระบุเงื่อนไขพิเศษในรายละเอียดสินค้า</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">12.&nbsp;สินค้าที่เสียหาย</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">12.1&nbsp;หากสินค้าที่คุณได้รับมีรอยตำหนิหรือมีปัญหา กรุณาติดต่อฝ่ายบริการลูกค้า เพื่อแจ้งหมายเลขคำสั่งซื้อของคุณ ชื่อ และที่อยู่ รวมถึงรายละเอียดสินค้าและเหตุผลที่ต้องส่งคืน รวมถึงแจ้งรายละเอียดว่าต้องการให้เราคืนเงินหรือต้องการเปลี่ยนสินค้า (เปลี่ยนสินค้าอย่างเดียว)</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">12.2&nbsp;เมื่อคุณรับมอบสินค้าแล้ว คุณจะต้องตรวจสอบสินค้าและคุณจะได้รับแจ้งสิทธิ์การเปลี่ยนสินค้าหรือการรับเงินคืน (ถ้ามี) ผ่านทางอีเมลโดยเร็วที่สุด</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">12.3&nbsp;หากสินค้าที่ส่งคืนไม่มีตำหนิ หรือปัญหา บริษัทอาจตัดสินใจซ่อมหรือเปลี่ยนสินค้าให้ใหม่ และ/หรืออาจขอให้คุณรับผิดชอบค่าใช้จ่ายในการจัดส่งและค่าซ่อมตามที่เห็นสมควรกับค่าธรรมเนียมและค่ าใช้จ่ายมาตรฐาน และหักค่าใช้จ่ายจากบัตรเครดิตหรือเดบิตของคุณ หรือตามรายละเอียดการชำระเงินที่คุณระบุไว้ตอนที่คุณส่งคำสั่งซื้อตามที่กฎหมายกำหนด ทั้งนี้จะไม่มีการรับผิดชอบต่อความสูญเสียหรือความเสียหายที่เกิดจากเหตุการณ์ดังกล่าว หากคุณมีคำถามเพิ่มเติม กรุณาติดต่อฝ่ายบริการลูกค้าที่หมายเลข&nbsp;02-121-4645&nbsp;วันจันทร์- ศุกร์ ตั้งแต่เวลา&nbsp;10.00 &ndash; 18.00&nbsp;น.</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">13.&nbsp;การปฏิเสธความรับผิดชอบ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">13.1&nbsp;หากคุณมีคูปองของขวัญ คูปองส่งเสริมการขาย หรือโค้ดส่วนลด (เรียกรวมกันว่า &ldquo;คูปอง&rdquo;) บุคคลอื่นอาจนำคูปองดังกล่าวมาใช้เพิ่มเติมนอกจากตัวคุณเองได้ และคุณสามารถโอนสิทธิ์ในคูปองดังกล่าวให้กับบุคคลอื่นหากได้รับอนุญาตตามเงื่อนไขและข้อกำหนดของคูปองดังกล่าว เงื่อนไขและข้อกำหนดอื่นอาจนำมาบังคับใช้กับการใช้คูปอง ซึ่งรายละเอียดจะระบุไว้ในคูปองดังกล่าว</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">13.2&nbsp;ในกรณีที่มีการทุจริต หรือพยายามหลอกลวง เราสามารถปิดบัญชีของคุณได้ และ/หรือใช้สิทธิ์ที่เราพึงมีกับคุณได้</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">13.3&nbsp;เราไม่รับผิดชอบต่อความสูญเสีย การขโมย หรือการใช้คูปองโดยไม่ได้รับอนุญาต</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">13.4&nbsp;บริษัทอาจระงับหรือยกเลิกคูปองที่ออกให้ตามที่เห็นสมควร และไม่ขอรับผิดชอบต่อความสูญเสียใด ๆ ที่เกิดจากการระงับหรือยกเลิกคูปองดังกล่าว</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">13.5&nbsp;สำหรับเงื่อนไขการใช้คูปอง ในบางกรณี บริษัทอาจแจกคูปองที่นำมาใช้กับเว็บไซต์นี้ได้เท่านั้น โดยบริษัทจะส่งคูปองให้กับคุณผ่านทางอีเมล หรือไปรษณีย์&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">ก) คูปองส่งเสริมการขายสามารถนำมาใช้งานภายในระยะเวลาที่กำหนดตามที่ระบุไว้ในคูปอง และสามารถใช้ได้ครั้งเดียว และไม่สามารถใช้ร่วมกับคูปองอื่นได้&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">ข) หากคำสั่งซื้อของคุณมีมูลค่าน้อยกว่าคูปองส่งเสริมการขาย จะไม่มีการคืนเงินหรือคืนเครดิตที่เหลือให้กับคุณ&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">ค) คูปองไม่สามารถแลกเปลี่ยนเป็นเงินสดได้&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">ง) หากคูปองมีมูลค่าไม่เพียงพอต่อคำสั่งซื้อของคุณ คุณอาจใช้วิธีการชำระเงินวิธีใดวิธีหนึ่งที่บริษัทยอมรับเพื่อชำระส่วนที่เหลือ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">14.&nbsp;การชดเชย&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">คุณตกลงที่จะชดเชย แก้ต่าง และไม่กระทำการใด ๆ อันส่งผลเสียต่อบริษัทและกรรมการ เจ้าหน้าที่ พนักงาน ที่ปรึกษา ตัวแทนของเนกซ์เทียร์ รวมถึงบุคคลอื่น และไม่เอาผิดกับบริษัทและบุคคลดังกล่าวในเรื่องข้อร้องเรียน ความรับผิดชอบ ค่าเสียหาย และ/หรือค่าใช้จ่ายของบุคคลอื่น (รวมถึงแต่ไม่จำกัดกับค่าธรรมเนียมทางกฎหมาย) อันเกิดจากการที่คุณใช้งานเว็บไซต์นี้ หรือละเมิดข้อกำหนดและเงื่อนไขนี้</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">15.&nbsp;การสื่อสาร</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">15.1&nbsp;บริษัทและเนกซ์เทียร์จะต้องปฏิบัติตามข้อภาระผูกพันภายใต้เงื่อนไขดังกล่าวด้วยทักษะและความความสามารถตามที่เห็นสมควร</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">15.2&nbsp;เราให้ความสำคัญกับความพึงพอใจของลูกค้า คุณอาจติดต่อเราได้ทุกเมื่อโดยใช้ข้อมูลที่อยู่ในข้อกำหนดและเงื่อนไขนี้ และเราจะพยายามแก้ไขปัญาหาให้กับคุณโดยเร็วที่สุด</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">15.3&nbsp;ในบางกรณี อีเมลของคุณอาจอยู่ในโปรแกรมคัดกรองสแปมของเราและอาจส่งมาไม่ถึงเรา หรือจดหมายของเราอาจส่งไม่ถึงคุณ หากคุณไม่ได้รับคำตอบจากเราภายในห้า (5)&nbsp;วันทำการ กรุณาสอบถามเราอีกครั้ง</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">16.&nbsp;เงื่อนไขที่อยู่นอกเหนือการควบคุม (เหตุสุดวิสัย)</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">16.1&nbsp;บริษัทจะไม่รับผิดชอบต่อความผิดใด ๆ การปิดล้อม หรือความล่าช้าตามข้อตกลงอันเนื่องจากเหตุการณ์ที่อยู่นอกเหนือการควบคุมของเรา ซึ่งรวมถึงแต่ไม่จำกัดกับ ก) การประท้วงหยุดงาน การปิดโรงงาน หรือการเคลื่อนไหวทางอุตสาหกรรม ข) ความไม่สงบ เหตุจลาจล การก่อการร้าย หรือภัยคุกคามจากการปะทะของการก่อการร้าย สงคราม (ไม่ว่ารัฐบาลจะประกาศหรือไม่ก็ตาม) หรือ คำสั่ง หรือการเตรียมเข้าสู่ภาวะสงคราม ค) ไฟไหม้ การระเบิดรุนแรง พายุ น้ำท่วม แผ่นดินไหว โรคระบาดหรือภัยพิบัติทางธรรมชาติ ง) การไม่สามารถใช้บริการขนส่งทางรถไฟ การจัดส่งผ่านทางเครื่องบิน รถยนต์ หรือในรูปแบบอื่น ๆ ทั้งของภาครัฐและภาคเอกชน จ) การไม่สามารถใช้เครือข่ายโทรคมนาคมของภาครัฐและเอกชน ฉ) กฎหมาย พระราชกฤษฎีกา การออกกฎหมายใหม่ ข้อบังคับ ข้อกำหนด หรือข้อห้ามของหน่วยงานรัฐบาล การประท้วงหยุดงาน การยุบกิจการ หรืออุบัติเหตุในการจัดส่งของการไปรษณีย์ หรือหน่วยงานขนส่งอื่น ๆที่เกี่ยวข้อง</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">16.2&nbsp;หากเกิดเหตุสุดวิสัยที่ทำให้บริษัทไม่สามารถจัดส่งสินค้า และดำเนินการต่อไปได้มากกว่าหนึ่งสัปดาห์นับตั้งแต่วันจัดส่งเดิม ทั้งคุณ และ/หรือบริษัทสามารถยกเลิกข้อตกลงหลังจากส่งหนังสือแจ้งยกเลิกได้ โดยไม่ต้องรับผิดชอบใด ๆ ยกเว้นการคืนเงินให้กับคุณสำหรับค่าสินค้าที่คุณชำระไป</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">16.3&nbsp;บริษัทขอสงวนสิทธิ์ในการตอบสนองต่อเหตุสุดวิสัยตามที่เห็นสมควร</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">17.&nbsp;การแจ้งให้ทราบ&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">หนังสือแจ้งภายใต้ข้อตกลงนี้จำเป็นต้องจัดทำเป็นลายลักษณ์อักษร ในรูปแบบจดหมาย หรือจัดส่งทางอีเมลให้กับอีกฝ่ายที่เกี่ยวข้อง มาตามที่อยู่ หรืออีเมลแอดเดรสที่ฝ่ายใดฝ่ายหนึ่งแจ้งให้อีกฝ่ายรับทราบ ก) หนังสือแจ้งที่ส่งผ่านทางไปรษณีย์จะถือว่าได้รับเป็นที่เรียบร้อยแล้วหลังจากผ่านไป สอง (2)&nbsp;วันทำการนับตั้งแต่วันที่จัดส่ง ณ ที่ทำการไปรษณีย์มายังที่อยู่ของผู้รับในประเทศไทย ข) หนังสือแจ้งที่จัดส่งทางอีเมลจะถือว่า จัดส่งเรียบร้อยแล้วภายในสอง (2)&nbsp;วันทำการนับตั้งแต่เวลาที่ส่งอีเมลนั้น และเพื่อพิสูจน์การจัดส่งดังกล่าว ควรมีหนังสือแจ้งหรืออีเมลที่ส่งมาตามที่อยู่ที่ถูกต้องหรือ การจัดส่งทางไปรษณีย์ หรือ มีบันทึกการส่งมอบหรือจัดส่งหรือยอมรับรายงานการจัดส่งดังกล่าวก็</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">18.&nbsp;สิทธิ์ในทรัพย์สินทางปัญญา</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">18.1&nbsp;เนื้อหาทั้งหมดที่เผยแพร่บนเว็บไซต์องเราซึ่งรวมถึงแต่ไม่จำกัดกับ เนื้อหา ภาพ ตราสัญลักษณ์ เครื่องหมายการค้า เครื่องหมายบริการ รูป คลิปเสียง สื่อดิจิตอลที่ดาวน์โหลดมา ชื่อแคมเปญ และข้อมูลที่ดัดแปลงล้วนเป็นทรัพย์สินของเนกซ์เทียร์และได้รับการคุ้มครองตามกฎหมายลิขสิทธิ์และเครื่องหมายการค้า และอนุสัญญาว่าด้วยทรัพย์สินทางปัญญาระดับโลก ซึ่งเนกซ์เทียร์และผู้ออกใบอนุญาตขอสงวนสิทธิ์ดังกล่าวทั้งหมด คุณสามารถจัดเก็บ พิมพ์ และแสดงเนื้อหาดังกล่าวได้ เพื่อใช้งานส่วนตัวเท่านั้น แต่คุณไม่ได้รับอนุญาตให้เผยแพร่ ถ่ายโอน จ่ายแจก หรือคัดลอกไม่ว่าด้วยวิธีการใดๆ ในเนื้อหาหรือสำเนาเนื้อหาที่คุณแจ้ง หรือที่แสดงไว้บนเว็บไซต์นี้ และคุณไม่ได้รับอนุญาตให้ใช้เนื้อหาดังกล่าวเพื่อจุดประสงค์ทางธุรกิจหรือทางการค้า</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">18.2&nbsp;คุณจำเป็นต้องทราบสถานะของเรา (และสถานะของซัพพลายเออร์ที่ระบุไว้) ในฐานะผู้จัดทำเนื้อหาบนเว็บไซต์ของเราตามที่ระบุไว้</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">18.3&nbsp;คุณต้องไม่ใช้เนื้อหาส่วนใดส่วนหนึ่งที่อยู่บนเว็บไซต์ของเราเพื่อจุดประสงค์ทางการค้า โดยไม่ได้รับการอนุญาตจากเราให้ดำเนินการดังกล่าว หากเนกซ์เทียร์รับทราบการละเมิดทรัพย์สินทางปัญญา เนกซ์เทียร์จะรีบดำเนินการตามกฎหมายทันที</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">18.4&nbsp;หากคุณพิมพ์ ทำสำเนาหรือดาวน์โหลดข้อมูลส่วนใดส่วนหนึ่งในเว็บไซต์นี้อันเป็นการฝ่าฝืนเงื่อนไขดังกล่าว สิทธ์ในการใช้เว็บไซต์ของคุณจะมีผลสิ้นสุดลงทันที และเรามีสิทธิ์ขอให้คุณส่งคืนหรือทำลายสำเนาเอกสารที่คุณจัดทำ คุณไม่ได้รับอนุญาตให้ดัดแปลง แปล ปรับเปลี่ยน แยก หรือสร้างงานใหม่จากเนื้อหาบนเว็บไซต์นี้ หรือเอกสารสนับสนุนที่แนบมาด้วย และ/หรือที่เนกซ์เทียร์ หรือผู้ออกใบอนุญาตของเราจัดทำขึ้น</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">19.&nbsp;การโฆษณาบนเว็บไซต์</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">เราปฏิบัติตามข้อบังคับ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">และข้อกำหนดที่เกี่ยวข้องกับเว็บไซต์นี้ตามที่สำนักงานคุ้มครองผู้บริโภคกำหนดอย่างเคร่งครัด</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">20.&nbsp;การละเว้น</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">หากเราไม่ใช้สิทธิ์หรือ ใช้สิทธิ์ล่าช้าตามเงื่อนไขและข้อกำหนด จะไม่ถือว่าเป็นการสละสิทธิ์ดังกล่าว ทั้งนี้ การสละสิทธิ์ในสถานการณ์ใด ๆ จะไม่ถือว่าเป็นการสละสิทธิ์กับเหตุการณ์ที่มีลักษณะเดียวกันหรือใกล้เคียงกันที่อาจเกิดขึ้นในอนาคต</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">21.&nbsp;การเป็นโมฆะบางส่วน&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">หากบทบัญญัติใด ๆ ในข้อกำหนดหรือข้อตกลงไม่สมบูรณ์ หรือไม่สามารถบังคับใช้ได้ภายในพื้นที่บริการ ความไม่สมบูรณ์ หรือการไม่สามารถบังคับใช้ได้ดังกล่าวจะไม่ส่งผลต่อบทบัญญัติส่วนที่เหลือไม่ว่าจะในลักษณะใดก็ตาม และยังคงมีผลโดยสมบูรณ์ตลอดระยะเวลาที่เงื่อนไขและข้อตกลงดังกล่าวมีผลโดยสมบูรณ์และนำมาบังคับใช้กับคุณ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">22.&nbsp;กฎหมายที่บังคับใช้</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">ข้อกำหนดและเงื่อนไข และข้อตกลงใด ๆ (และภาระผูกพันที่ไม่ได้อยู่ในสัญญาที่เกิดจากหรือที่เกี่ยวข้องกับเงื่อนไขหรือข้อตกลงดังกล่าว) จะบังคับใช้และตีความตามกฎหมายแห่งราชอาณาจักรไทย คุณและเราตกลงว่าจะหาข้อตกลงร่วมกันตามขอบเขตอำนาจของศาลไทย การสื่อสาร การจัดส่ง และการติดต่อ (รวมเรียกว่า &ldquo;เอกสาร&rdquo;) ระหว่างคุณกับเราจะจัดทำเป็นภาษาไทย</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">23.&nbsp;การแก้ไข</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">เราขอสงวนสิทธิ์ในการแก้ไขข้อกำหนดและเงื่อนไขนี้ได้ทุกเมื่อ เราจะแจ้งการเปลี่ยนแปลงเงื่อนไขบนเว็บไซต์ของเรา อย่างไรก็ตาม การใช้เว็บไซต์ในลักษณะต่อไปนี้ถือว่าเป็นการยอมรับเงื่อนไขใหม่ และเมื่อคุณส่งคำสั่งซื้อบนเว็บไซต์นี้ คุณยอมรับว่าคุณได้อ่านและเห็นชอบกับเงื่อนไขและข้อกำหนดที่บังคับใช้ ณ วันที่คุณส่งคำสั่งซื้อ ดังนั้นคุณจะต้องอ่านเงื่อนไขล่าสุดทุกครั้งเมื่อคุณส่งคำสั่งซื้อ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&nbsp;</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">24.&nbsp;เวลาจัดส่งสินค้าโดยประมาณ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">บริษัทขนส่งใช้เวลาจัดส่ง&nbsp;1-3&nbsp;วันทำการ (ไม่รวม เสาร์-อาทิตย์ และวันหยุดนักขัตฤกษ์)</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">ไม่รวมระยะเวลาการเตรียมและรอสินค้าสำหรับการสั่งซื้อสินค้าล่วงหน้า หรือสินค้าที่ยังไม่มีของจัดส่ง</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">***&nbsp;หมายเหตุ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&bull;&nbsp;บริษัทจะจัดส่งเฉพาะคำสั่งซื้อที่มีการชำระเงินสมบูรณ์แล้วเท่านั้น</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&bull;&nbsp;สินค้าจะถูกจัดส่งมายังที่อยู่ตามที่แจ้งไว้ในคำสั่งซื้อ และบริษัทขอสงวนสิทธิ์ในการเปลี่ยนแปลงที่อยู่จัดส่ง</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&bull;&nbsp;สำหรับจังหวัดยะลา ปัตตานี และนราธิวาส หมู่เกาะ และพื้นที่ห่างไกลอื่น ๆ เวลาจัดส่งอาจนานกว่าระยะเวลาที่ระบุไว้ และคุณจะได้รับแจ้งเวลาจัดส่งที่คาดว่าจะถึงเมื่อคุณได้รับอีเมลยืนยันคำสั่งซื้อ</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&bull;&nbsp;หากการจัดส่งใช้เวลานานกว่าที่ระบุไว้ (เนื่องจากเหตุสุดวิสัย) บริษัทจะแจ้งให้คุณทราบล่วงหน้าทางโทรศัพท์ อีเมล หรือในช่องทางอื่น</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px;"><span style="line-height: 107%; font-family: Prompt;">&bull;&nbsp;บริษัทจะดำเนินการจัดส่งสินค้าในวันทำการเท่านั้น ในช่วงเทศกาล หรือในช่วงที่มีการสั่งซื้อจำนวนมาก การจัดส่งอาจล่าช้ากว่าเวลาจัดส่งที่ที่กำหนด และบริษัทจะพยายามอย่างเต็มที่เพื่อจัดส่งสินค้าให้คุณภายในระยะเวลาที่เหมาะสม</span></span></p>
                    <p style='margin-top:0cm;margin-right:0cm;margin-bottom:8.0pt;margin-left:0cm;line-height:107%;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size: 14px; line-height: 107%; font-family: Prompt;">&nbsp;</span></p>
                </div>
            </div>
            <div class="modal-footer " style="justify-content: center;">
                <button id="btn-cookie-modal" type="button" class="btn btn-secondary col-11" data-bs-dismiss="modal" style="width: 120px;height: 40px; border-radius: 25px;">ยอมรับ</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#btn-cookie").click(function() {
            // $("#cookie").slideUp(500);
            $(".sywh-open-services").removeClass('mb-10');
        });
        $("#reset").click(function() {
            $("#compare").slideUp(500);
        });
        $("#test-btn").click(function() {
            $("#compare").slideDown(500);

        });
        // $("#btn-cookie-modal").click(function() {
        //     $("#cookie").slideUp(500);
        // });

    });
    $(".close-social").click(function() {
        $("#sy-whatshelp").hide();
    });
</script>

<script>
    window.fbAsyncInit = function() {
        FB.init({
                appId: '295555285694750',
                autoLogAppEvents: true,
                xfbml: true,
                version: 'v3.2'
            }

        );
    };
    (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];

            if (d.getElementById(id)) {
                return;
            }

            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js";
            fjs.parentNode.insertBefore(js, fjs);
        }

        (document, 'script', 'facebook-jssdk'));
</script>
<script>
    jQuery(function($) {
            $('a.sywh-open-services').click(function() {
                    if ($('.sywh-services').hasClass('active')) {
                        $('.sywh-services').removeClass('active');
                        $('#sy-whatshelp a.sywh-open-services').css({
                                'width': '200px',
                                '-moz-transition': 'width 0.3s ease-out',
                                '-webkit-transition': 'width 0.3s ease-out',
                                'transition': 'width 0.3s ease-out'
                            }

                        );

                        setTimeout(function() {
                                $('#sy-whatshelp a.sywh-open-services span').show();
                            }

                            , 300);

                        $('a.sywh-open-services i.fa-times').hide();
                        $('a.sywh-open-services i.fa-comments').show();
                        $('a.sywh-open-services').removeClass('show');
                        $('.sywh-services a:nth-child(1)').delay(0).fadeOut();
                        $('.sywh-services a:nth-child(2)').delay(50).fadeOut();
                        $('.sywh-services a:nth-child(3)').delay(100).fadeOut();
                        $('.sywh-services a:nth-child(4)').delay(150).fadeOut();
                        $('.sywh-services a:nth-child(5)').delay(200).fadeOut();
                        $(".close-social").show();
                    } else {
                        $('.sywh-services').addClass('active');
                        $('#sy-whatshelp a.sywh-open-services').css({
                                'width': '55px',
                                '-moz-transition': 'width 0.3s ease-out',
                                '-webkit-transition': 'width 0.3s ease-out',
                                'transition': 'width 0.3s ease-out'
                            }

                        );

                        setTimeout(function() {
                                $('#sy-whatshelp a.sywh-open-services span').hide();
                            }

                            , 300);
                        $('a.sywh-open-services i.fa-comments').hide();
                        $('a.sywh-open-services i.fa-times').show();
                        $('a.sywh-open-services').addClass('show');
                        $('.sywh-services a:nth-child(5)').delay(0).fadeIn();
                        $('.sywh-services a:nth-child(4)').delay(50).fadeIn();
                        $('.sywh-services a:nth-child(3)').delay(100).fadeIn();
                        $('.sywh-services a:nth-child(2)').delay(150).fadeIn();
                        $('.sywh-services a:nth-child(1)').delay(200).fadeIn();
                        $(".close-social").hide();
                    }
                }

            );
        }

    );
</script>