$(document).ready(function() {

   $('.insert-dotdotdot').dotdotdot({
   	ellipsis: '...', /* The HTML to add as ellipsis. */
   	wrap: 'letter', /* How to cut off the text/html: 'word'/'letter'/'children' */
   	watch: true /* Whether to update the ellipsis: true/'window' */
   });

    //select bootstrap .selectpicker
    $('.selectpicker').selectpicker('refresh');
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
    	$('.selectpicker').selectpicker('mobile');
    }

});

